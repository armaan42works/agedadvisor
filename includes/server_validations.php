<?php

    /*
     * To change this license header, choose License Headers in Project Properties.
     * To change this template file, choose Tools | Templates
     * and open the template in the editor.
     */
    /*
     * @author: Pramod Kumar
     * @Date: 25 July 2014
     * Validate change password form fields in admin section of change password
     */

    function change_password() {
        $fields_array = array('method' => 'REQUEST', 'rules' => array(
                'userid' => array('required' => true),
                'old_password' => array('required' => true),
                'new_password' => array('required' => true, 'minLength' => 6, 'maxLength' => 30),
                'confirm_password' => array('required' => true, 'minLength' => 6, 'maxLength' => 30)),
            'messages' => array(
                'userid' => array('required' => 'Please enter User Id'),
                'old_password' => array('required' => 'Please enter old password'),
                'new_password' => array('required' => 'Please enter new password'),
                'confirm_password' => array('required' => 'Please enter confirm password'))
        );
        return $fields_array;
    }

    function coupons() {
        $fields_array = array('method' => 'REQUEST', 'rules' => array(
                'client_type' => array('required' => TRUE),
                'type' => array('required' => TRUE),
                //'subtype' => array('required' => TRUE),
                'value' => array('required' => TRUE),
                'code' => array('required' => TRUE),
                'expire_date' => array('required' => TRUE)),
            'messages' => array(
                'client_type' => array('required' => 'Please select Targeted Client'),
                'type' => array('required' => 'Please select Promo Code'),
                //'subtype' => array('required' => 'Please select Coupon'),
                'value' => array('required' => 'Please enter Amount'),
                'code' => array('required' => 'Please enter Coupon Code'),
                'expire_date' => array('required' => 'Please select Expiration Date'))
        );
        return $fields_array;
    }

    function newsletter() {
        $fields_array = array('method' => 'REQUEST', 'rules' => array(
                'title' => array('required' => TRUE),
                'FCKeditor1' => array('required' => TRUE)),
            'messages' => array(
                'title' => array('required' => 'Please provide the Title'),
                'FCKeditor1' => array('required' => 'Please enter the message'))
        );
        return $fields_array;
    }

    function user() {
        $field_array = array('method' => 'REQUEST',
            'rules' => array(
                'name' => array('required' => TRUE),
                'address' => array('required' => TRUE),
                'country_id' => array('required' => TRUE),
                'state_id' => array('required' => TRUE),
                'zipcode' => array('required' => TRUE),
                'phone' => array('required' => TRUE)
            ),
            'messages' => array(
                'name' => array('required' => 'Please enter the name.'),
                'address' => array('required' => 'Please enter the address.'),
                'country_id' => array('required' => 'Please select the Country.'),
                'state_id' => array('required' => 'Please select the State.'),
                'zipcode' => array('required' => 'Please enter the zipcode.'),
                'phone' => array('required' => 'Please enter the Phone Number.')
        ));
        if (isset($business) && $business != '') {
            $field_array['rules']['business'] = array('required' => TRUE);
            $field_array['message']['business'] = array('required' => 'Please enter the Business.');
        }
        return $field_array;
    }

//Supplier valiadion

    function userSupplier() {
        $field_array = array('method' => 'REQUEST',
            'rules' => array(
                'first_name' => array('required' => TRUE),
                'email' => array('required' => TRUE),
                'phone' => array('required' => TRUE),
                'password' => array('required' => TRUE),
                'conform_password' => array('required' => TRUE),
                'company' => array('required' => TRUE),
                'trade' => array('required' => TRUE)
            ),
            'messages' => array(
                'first_name' => array('required' => 'Please enter your First Name.'),
                'email' => array('required' => 'Please enter your EmailId .'),
                'phone' => array('required' => 'Please enter your Phone Number.'),
                'password' => array('required' => 'Please enter Password.'),
                'conform_password' => array('required' => 'Please enter Conform Password.'),
                'company' => array('required' => 'Please enter Compony Name.'),
                'trade' => array('required' => 'Please enter the Trade Name.')
        ));
        return $field_array;
    }

//Customer valiadion

    function userCustomer() {
        $field_array = array('method' => 'REQUEST',
            'rules' => array(
                'first_name' => array('required' => TRUE),
                'user_name' => array('required' => TRUE),
                'password' => array('required' => TRUE),
                'confirm_password' => array('required' => TRUE)
            ),
            'messages' => array(
                'first_name' => array('required' => 'Please enter your First Name.'),
                'user_name' => array('required' => 'Please enter UserName.'),
                'password' => array('required' => 'Please enter Password.'),
                'confirm_password' => array('required' => 'Please enter Confirm Password.'),
        ));
        return $field_array;
    }

//Supplier login valiadion

    function supplierLogin() {
        $field_array = array('method' => 'REQUEST',
            'rules' => array(
                'user_id' => array('required' => TRUE),
                'password' => array('required' => TRUE)
            ),
            'messages' => array(
                'user_id' => array('required' => 'Please enter your Email Id.'),
                'password' => array('required' => 'Please enter your password.')
        ));
        return $field_array;
    }

    function userXLS() {
        $field_array = array('method' => 'REQUEST',
            'rules' => array(
                'name' => array('required' => TRUE),
                'address' => array('required' => TRUE),
                'zipcode' => array('required' => TRUE),
                'phone' => array('required' => TRUE)
            ),
            'messages' => array(
                'name' => array('required' => 'Please enter the name.'),
                'address' => array('required' => 'Please enter the address.'),
                'zipcode' => array('required' => 'Please enter the zipcode.'),
                'phone' => array('required' => 'Please enter the Phone Number.')
        ));
        if (isset($business) && $business != '') {
            $field_array['rules']['business'] = array('required' => TRUE);
            $field_array['message']['business'] = array('required' => 'Please enter the Business.');
        }
        return $field_array;
    }

    function sendnewsletter() {
        $fields_array = array('method' => 'REQUEST', 'rules' => array(
                'newsletter_id' => array('required' => TRUE),
                'subject' => array('required' => TRUE)),
            'messages' => array(
                'newsletter_id' => array('required' => 'Please choose a newsletter'),
                'subject' => array('required' => 'Please choose a subject'))
        );
        if ($_POST["receiver"] == 2) {
            $fields_array['rules']['receiver_selected'] = array('required' => TRUE);
            $fields_array['messages']['receiver_selected'] = array('required' => "Pleae enter email ids separated by comma");
        }
        return $fields_array;
    }

    function client_registration() {



        $fields_array = array('method' => 'REQUEST', 'rules' => array(
                'business' => array('required' => TRUE),
                'name' => array('required' => TRUE),
                'address' => array('required' => TRUE),
                'country_id' => array('required' => TRUE),
                'state_id' => array('required' => TRUE),
                'zipcode' => array('required' => TRUE),
                'phone' => array('required' => TRUE),
                'cellphone' => array('required' => TRUE),
                'email' => array('required' => TRUE),
                'confirmemail' => array('required' => TRUE),
                'password' => array('required' => TRUE),
                'confirmpassword' => array('required' => TRUE)
            ),
            'messages' => array(
                'business' => array('required' => 'Please enter your business name.'),
                'name' => array('required' => 'Please enter the name.'),
                'address' => array('required' => 'Please enter the address.'),
                'country_id' => array('required' => 'Please select the Country.'),
                'state_id' => array('required' => 'Please select the State.'),
                'zipcode' => array('required' => 'Please enter the zipcode.'),
                'phone' => array('required' => 'Please enter the Phone Number.'),
                'cellphone' => array('required' => 'Please enter the Cellphone.'),
                'email' => array('required' => 'Please enter the Email Address.'),
                'confirmemail' => array('required' => 'Please enter the confirm email field.'),
                'password' => array('required' => 'Please enter the password.'),
                'confirmpassword' => array('required' => 'Please enter the confirm password field.'))
        );
        if ($_POST["sp_refral"] != 2 || $_POST["client_refral"] != 2) {
            $fields_array['rules']['refferal_id'] = array('required' => TRUE);
            $fields_array['messages']['refferal_id'] = array('required' => "Pleae enter the refferal Id");
        }
        return $fields_array;
    }

    function message() {
        $fields_array = array('method' => 'REQUEST', 'rules' => array(
                'subject' => array('required' => TRUE),
                'FCKeditor1' => array('required' => TRUE)
            ),
            'messages' => array(
                'subject' => array('required' => 'Please enter the subject.'),
                'FCKeditor1' => array('required' => 'Please enter the message body.'))
        );
        return $fields_array;
    }

//validation on home key page

    function add_homekey() {
        $fields_array = array('method' => 'REQUEST', 'rules' => array(
                'meta_title' => array('required' => TRUE),
                'keyword' => array('required' => TRUE),
                'description' => array('required' => TRUE)
            ),
            'messages' => array(
                'meta_title' => array('required' => 'please enter a meta title'),
                'keyword' => array('required' => 'please enter the meta keywords'),
                'description' => array('required' => 'please enter the meta discription')
        ));
        return $fields_array;
    }

    function add_page() {
        $fields_array = array('method' => 'REQUEST', 'rules' => array(
                'page_title' => array('required' => TRUE),
                'url' => array('required' => TRUE),
                'type' => array('required' => TRUE),
                'meta_title' => array('required' => TRUE),
                'keyword' => array('required' => TRUE),
                'short_description' => array('required' => TRUE),
                'description' => array('required' => TRUE),
                'content' => array('required' => TRUE)
            ),
            'messages' => array(
                'page_title' => array('required' => 'Please enter the page title.'),
                'url' => array('required' => 'Please enter the page url.'),
                'type' => array('required' => 'Please choose a type'),
                'meta_title' => array('required' => 'please enter a meta title'),
                'keyword' => array('required' => 'please enter the meta keywords'),
                'short_description' => array('required' => 'please enter the short description'),
                'description' => array('required' => 'please enter the meta discription'),
                'content' => array('required' => 'Please enter the content of the page.'))
        );


        if ($_POST["type"] == 1 && $_FILES['image_path']['name'] == '' && isset($_POST['image_path'])) {

            $fields_array['rules']['image_path'] = array('required' => TRUE);
            $fields_array['messages']['image_path'] = array('required' => "Pleae choose a image");
        }

        return $fields_array;
    }

    function add_review() {
        $fields_array = array('method' => 'REQUEST', 'rules' => array(
                'supplier' => array('required' => TRUE),
                'url' => array('required' => TRUE),
                'meta_title' => array('required' => TRUE),
                'keyword' => array('required' => TRUE),
                'short_description' => array('required' => TRUE),
                'description' => array('required' => TRUE),
                'content' => array('required' => TRUE)
            ),
            'messages' => array(
                'supplier' => array('required' => 'Please enter the review title.'),
                'url' => array('required' => 'Please enter the page url dfhfgh.'),
                'meta_title' => array('required' => 'please enter a meta title'),
                'keyword' => array('required' => 'please enter the meta keywords'),
                'short_description' => array('required' => 'please enter the short description'),
                'description' => array('required' => 'please enter the meta discription'),
                'content' => array('required' => 'Please enter the content of the page.'))
        );

        if ($_POST["type"] == 1 && $_FILES['image_path']['name'] == '' && isset($_POST['image_path'])) {

            $fields_array['rules']['image_path'] = array('required' => TRUE);
            $fields_array['messages']['image_path'] = array('required' => "Pleae choose a image");
        }

        return $fields_array;
    }

    function reply_message() {
        $fields_array = array('method' => 'REQUEST', 'rules' => array(
                'FCKeditor1' => array('required' => TRUE)
            ),
            'messages' => array(
                'FCKeditor1' => array('required' => 'Please enter the message body.'))
        );
        return $fields_array;
    }

    function budget() {
        $fields_array = array('method' => 'REQUEST', 'rules' => array(
                'amount' => array('required' => TRUE)
            ),
            'messages' => array(
                'amount' => array('required' => 'Please enter the amount.'))
        );
        return $fields_array;
    }

    function services() {
        $fields_array = array('method' => 'REQUEST', 'rules' => array(
                'name' => array('required' => TRUE),
                'description' => array('required' => TRUE)
            ),
            'messages' => array(
                'name' => array('required' => 'Please enter the service name.'),
                'description' => array('required' => 'Please enter the service description.'))
        );
        return $fields_array;
    }

    function gallery() {
        $fields_array = array('method' => 'REQUEST', 'rules' => array(
                'title' => array('required' => TRUE),
                'file_type' => array('required' => TRUE)
            ),
            'messages' => array(
                'title' => array('required' => 'Please enter the image/video title'),
                'file_type' => array('required' => 'Please select a file tupe'))
        );
        return $fields_array;
    }

    function artwork() {
        $fields_array = array('method' => 'REQUEST', 'rules' => array(
                'description' => array('required' => TRUE)
            ),
            'messages' => array(
                'description' => array('required' => 'Please enter the logo description.'))
        );
        return $fields_array;
    }

    function slider() {
        $fields_array = array('method' => 'REQUEST', 'rules' => array(
                'title' => array('required' => TRUE),
                'description' => array('required' => TRUE)
            ),
            'messages' => array(
                'title' => array('required' => 'Please enter the logo title.'),
                'description' => array('required' => 'Please enter the logo description.'))
        );
        return $fields_array;
    }

    function b_price() {
        $fields_array = array('method' => 'REQUEST', 'rules' => array(
                'title' => array('required' => TRUE),
                'price' => array('required' => TRUE),
                'description' => array('required' => TRUE)
            ),
            'messages' => array(
                'title' => array('required' => 'Please enter price title.'),
                'price' => array('required' => 'Please enter the Ad space price.'),
                'description' => array('required' => 'Please enter the Ad description.'))
        );
        return $fields_array;
    }

    function m_price() {
        $fields_array = array('method' => 'REQUEST', 'rules' => array(
                'price' => array('required' => TRUE),
                'description' => array('required' => TRUE),
                'duration' => array('required' => TRUE)
            ),
            'messages' => array(
                'price' => array('required' => 'Please enter the Ad space price.'),
                'description' => array('required' => 'Please enter the Ad description.'),
                'duration' => array('required' => 'Please enter the duration.'))
        );
        return $fields_array;
    }

    function blog() {
        $fields_array = array('method' => 'REQUEST', 'rules' => array(
                'title' => array('required' => TRUE),
                'content' => array('required' => TRUE),
            ),
            'messages' => array(
                'title' => array('required' => 'Please enter blog title.'),
                'content' => array('required' => 'Please enter the blog content.')
            )
        );
        return $fields_array;
    }

    function comment() {
        $fields_array = array('method' => 'REQUEST', 'rules' => array(
                'comment' => array('required' => TRUE),
            ),
            'messages' => array(
                'comment' => array('required' => 'Please enter the comment.')
            )
        );
        return $fields_array;
    }

    function ads_masters() {
        $fields_array = array('method' => 'REQUEST', 'rules' => array(
                'title' => array('required' => TRUE),
                'description' => array('required' => TRUE),
                'space_id' => array('required' => TRUE),
            ),
            'messages' => array(
                'title' => array('required' => 'Please enter ad title.'),
                'description' => array('required' => 'Please enter the ad content.'),
                'space_id' => array('required' => 'Please select the space type.')
            )
        );
        return $fields_array;
    }  
   

    function forum() {

        $fields_array = array('method' => 'REQUEST', 'rules' => array(
                'title' => array('required' => TRUE),
                'content' => array('required' => TRUE),
            ),
            'messages' => array(
                'title' => array('required' => 'Please enter blog title.'),
                'content' => array('required' => 'Please enter the forum content.')
            )
        );

        return $fields_array;
    }

    function payble_amount() {

        $fields_array = array('method' => 'REQUEST', 'rules' => array(
                'amount' => array('required' => TRUE),
            ),
            'messages' => array(
                'amount' => array('required' => 'Payble amount is empty.'),
            )
        );

        return $fields_array;
    }

    function account_info() {

        $fields_array = array('method' => 'REQUEST', 'rules' => array(
                'address' => array('required' => TRUE),
            ),
            'messages' => array(
                'address' => array('required' => 'Please enter the address.'),
            )
        );

        return $fields_array;
    }

    function cat() {
        $fields_array = array('method' => 'REQUEST', 'rules' => array(
                'title' => array('required' => TRUE),
                'description' => array('required' => TRUE),
            ),
            'messages' => array(
                'title' => array('required' => 'Please enter the category title.'),
                'description' => array('required' => 'Please enter category description.'))
        );
        return $fields_array;
    }

    function add_positon() {
        $fields_array = array('method' => 'REQUEST', 'rules' => array(
                'job_title' => array('required' => TRUE),
                'interest_area' => array('required' => TRUE),
                'FCKeditor1' => array('required' => TRUE),
                'expire_date' => array('required' => TRUE),
            ),
            'messages' => array(
                'job_title' => array('required' => 'Please enter the job title.'),
                'interest_area' => array('required' => 'Please enter the industry of interest.'),
                'FCKeditor1' => array('required' => 'Please enter the description.'),
                'expire_date' => array('required' => 'Please enter the expire date.')
            )
        );
        return $fields_array;
    }

    function add_commision() {
        $fields_array = array('method' => 'REQUEST', 'rules' => array(
                'starting_commision' => array('required' => TRUE),
                'over_head' => array('required' => TRUE),
                'depreciation_coefficient' => array('required' => TRUE),
                'threshold_price' => array('required' => TRUE),
                'bonus' => array('required' => TRUE),
                'threshold_increment' => array('required' => TRUE),
                'threshold_count' => array('required' => TRUE)
            ),
            'messages' => array(
                'starting_commision' => array('required' => 'Please enter the starting commision.'),
                'over_head' => array('required' => 'Please enter the over head charges.'),
                'depreciation_coefficient' => array('required' => 'Please enter the depreciation coefficient.'),
                'commision_limit' => array('required' => 'Please enter the commision limit.'),
                'max_project' => array('required' => 'Please enter Maximum number of projects.'),
                'threshold_price' => array('required' => 'Please enter the threshold price.'),
                'bonus' => array('required' => 'Please enter the bonus amount.'),
                'threshold_increment' => array('required' => 'Please enter increment amount in %.'),
                'threshold_count' => array('required' => 'Please enter the threshold count in number.')
            )
        );
        return $fields_array;
    }

    function mail_template() {
        $fields_array = array('method' => 'REQUEST', 'rules' => array(
                'title' => array('required' => TRUE),
                'alias' => array('required' => TRUE),
                'subject' => array('required' => TRUE),
                'FCKeditor1' => array('required' => TRUE)
            ),
            'messages' => array(
                'title' => array('required' => 'Please enter the title.'),
                'alias' => array('required' => 'Please enter the alias.'),
                'subject' => array('required' => 'Please enter the subject.'),
                'FCKeditor1' => array('required' => 'Please enter the content.')
            )
        );
        return $fields_array;
    }

    function job_application() {
        $fields_array = array('method' => 'REQUEST', 'rules' => array(
                'name' => array('required' => TRUE),
                'address' => array('required' => TRUE),
                'c_country' => array('required' => TRUE),
                'c_state' => array('required' => TRUE),
                'zipcode' => array('required' => TRUE),
                'phone' => array('required' => TRUE),
                'email' => array('required' => TRUE),
                'cemail' => array('required' => TRUE),
                'password' => array('required' => TRUE),
                'cpassword' => array('required' => TRUE)
            ),
            'messages' => array(
                'name' => array('required' => 'Please enter the your name.'),
                'address' => array('required' => 'Please enter the address.'),
                'c_country' => array('required' => 'Please choose the country.'),
                'c_state' => array('required' => 'Please choose the state.'),
                'zipcode' => array('required' => 'Please enter the zip code.'),
                'phone' => array('required' => 'Please enter the phone number.'),
                'cellphone' => array('required' => 'Please enter the cellphone number.'),
                'email' => array('required' => 'Please enter the email.'),
                'cemail' => array('required' => 'Please enter confirm email.'),
                'password' => array('required' => 'Please enter the password.'),
                'cpassword' => array('required' => 'Please enter the confirm password.'),
                'coverletter' => array('required' => 'Please enter the cover letter.')
            )
        );

        if ($_FILES['resume'] == '') {
            $field_array['rules']['resume'] = array('required' => TRUE);
            $field_array['message']['resume'] = array('required' => 'Please upload your resume.');
        }
        return $fields_array;
    }

    function address() {
        $fields_array = array('method' => 'REQUEST', 'rules' => array(
                'address' => array('required' => TRUE),
            ),
            'messages' => array(
                'address' => array('required' => 'Please enter the address.'),
            )
        );
        return $fields_array;
    }

    function change_email() {
        $fields_array = array('method' => 'REQUEST', 'rules' => array(
                'email' => array('required' => true),
                'new_email' => array('required' => true),
                'confirm_email' => array('required' => true)),
            'messages' => array(
                'email' => array('required' => 'Please enter old admin email id.'),
                'new_email' => array('required' => 'Please enter admin new email id'),
                'confirm_email' => array('required' => 'Please confirm email id.'))
        );
        return $fields_array;
    }

?>