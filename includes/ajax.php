<?php
include("../application_top.php");
ini_set('display_errors', 1); 
session_start();
include_once("methods.php");

global $db;
switch ($_POST['action']) {
    // state list on front/admin
    case 'status' :
        //use this for status change
        echo status($table, $id, $status);
        break;
    case 'approval' :
        //use this for approved
        echo approval($table, $id, $approval);
        break;
    case 'verified' :
        //use this for approved
        echo verified($table, $id, $verified);
        break;
    case 'archive' :
        //use this for archive
        echo archive($table, $id, $archive);
        break;
    // Delete record in admin panel
    case "delete" :
        echo delete($table, $id);
        break;
    //Check whether the Email exist or not
    case "sendMail":
        echo sendMail($id);
        break;
    //Check whether the Email exist or not
    case "unquieEmail":
        echo unquieEmail($email);
        break;
    case "compareShortList":
        echo compareShortList($_POST['id'],$_POST['flag']);
        break;
    case "resetShortList":
        echo resetShortList();
        break;
    case "mailReport":
        echo mailReport($_POST['page_id'],$_POST['emailr']);
        break;
}

function status($table, $id, $status) {
    global $db;
    $changeStatus = $status == 1 ? 0 : 1;
    $UpdateSql = "UPDATE " . _prefix("$table") . " SET status = '$changeStatus' WHERE id= $id";
    $updateResult = $db->sql_query($UpdateSql);
    if ($updateResult) {
        $CurrentStatus = $changeStatus == 0 ? '<img src="' . ADMIN_IMAGE . 'deactivate.png" title="Inactive" />' : '<img src="' . ADMIN_IMAGE . 'activate.png" title="active"/>';
        echo "<a href='javascript:void(0);' id='$table-" . $id . "-" . $changeStatus . "' class='status'>$CurrentStatus</a>";
    }
}

function approval($table, $id, $approval) {
    echo $table;
    global $db;
    $changeStatus = $approval == 0 ? 1 : 1;
    $UpdateSql = "UPDATE " . _prefix("$table") . " SET approve_status = '$changeStatus' WHERE id= $id";
    $updateResult = $db->sql_query($UpdateSql);
    if ($updateResult) {
        $CurrentStatus = $approved == 1 ? 'Disapproved <img src="' . ADMIN_IMAGE . 'deactivate.png" title="Disapproved" />' : 'Approved <img src="' . ADMIN_IMAGE . 'activate.png" title="Approved"/>';
        echo "<a href='javascript:void(0);' id='$table--" . $id . "--" . $changeStatus . "' class='approval'>$CurrentStatus</a>";
    }
}

function verified($table, $id, $verified) {
    global $db;
    $changeStatus = $verified == 0 ? 1 : 1;
    $UpdateSql = "UPDATE " . _prefix("$table") . " SET approve_status = '$changeStatus' WHERE id= $id";
    $updateResult = $db->sql_query($UpdateSql);
    if ($updateResult) {
        $CurrentStatus = $verified == 1 ? 'Unverified<img src="' . ADMIN_IMAGE . 'deactivate.png" title="Unverified" />' : 'Verified <img src="' . ADMIN_IMAGE . 'activate.png" title="Verified"/>';
        echo "<a href='javascript:void(0);' id='$table--" . $id . "--" . $changeStatus . "' class='verified'>$CurrentStatus</a>";
    }
}

function archive($table, $id, $archive) {
    global $db;
    $changeStatus = $archive == 0 ? 1 : 1;
    $UpdateSql = "UPDATE " . _prefix("$table") . " SET archive = '$changeStatus' WHERE id= $id";
    $updateResult = $db->sql_query($UpdateSql);
    if ($updateResult) {
        $CurrentStatus = $archive == 0 ? ' <i style="font-size:20px;" title="Archieved" class="fa fa-archive"></i>' : '<img src="' . ADMIN_IMAGE . 'activate.png" title="Archive"/>';
        echo "<a href='javascript:void(0);' id='$table--" . $id . "--" . $changeStatus . "' class='archive'>$CurrentStatus</a>";
    }
}

function state($counrty) {
    echo stateList($counrty);
    die();
}

function delete($table, $id) {
    global $db;
    $updateSql = "UPDATE " . _prefix("$table") . " SET deleted = '1' WHERE id= $id";
    $updateResult = $db->sql_query($updateSql);
    if ($table == 'point_earning_setting') {
        $insert_data = "UPDATE " . _prefix("$table") . " SET referral_point='',referral_value = '', social_point = '',social_value = '', "
                . "min_value = '', client_point = '', client_value = '' WHERE id = $id";
        $db->sql_query($insert_data);
    }
}

function unquieEmail($email) {
    global $db;
    $checkEmail = "Select id from " . _prefix("users") . " where email like '$email'";
    $resCheckEmail = $db->sql_query($checkEmail);
    if ($db->sql_numrows($resCheckEmail) > 0) {
        return 0;
    } else {
        return json_encode($email == $email);
    }
}

function checkCaptcha($data) {
    require_once dirname(__FILE__) . '/captch/securimage.php';
    $securimage = new Securimage();
    if ($securimage->check($data) == false) {
        echo 0;
    } else {
        echo 1;
    }
}

function compareShortList($id,$flag) {
    if($flag==1){
        $p_id = $id;
        $_SESSION['session_compare_product'][$p_id]=  $id;       
        $_SESSION['shortlist_compare_count'] = count($_SESSION['session_compare_product']);
        echo $_SESSION['shortlist_compare_count'];
    }
    if($flag == 2){
        $p_id = $id;
        unset($_SESSION['session_compare_product'][$p_id]);
	$_SESSION['shortlist_compare_count'] = count($_SESSION['session_compare_product']);
	echo $_SESSION['shortlist_compare_count'];
    }
}

function resetShortList() {
     unset($_SESSION['session_compare_product']);
     unset($_SESSION['shortlist_compare_count']);   
     $_SESSION['shortlist_compare_count'] = 0;
    
}

function mailReport($page_id, $emailr) {
    global $db;
    if (!filter_var($emailr, FILTER_VALIDATE_EMAIL) === false) {    
        
        $facilityNameQry = "SELECT title from ad_products WHERE id = '$page_id'";
        $facilityName = $db->sql_query($facilityNameQry);
        
        while($title = $db->sql_fetchrow($facilityName)){
            $facility_title = $title['title'];
        }            
        
        
        $mailQuery    = "INSERT INTO ad_provider_comparison_report (email,pr_id,title) VALUES('$emailr','$page_id','$facility_title')";
        $exemailQuery = $db->sql_query($mailQuery);
        $last_id = $db->last_id();
        if ($last_id && isset($last_id)) {
            $sql_guery = "SELECT email,pr_id FROM ad_provider_comparison_report where id='$last_id' ";            
            $sql_guery_exe=$db->sql_query($sql_guery);
            $result = $db->sql_fetchrow($sql_guery_exe);
            $pr_id = $result['pr_id'];
            $email = $result['email'];                                            

            //sendmail($emailr, 'Link to print report!', '<p>Here is your download link to view and print the report for </p> <br> <strong>'.$facility_title.' : </strong><a href="https://www.agedadvisor.nz/Provider-Comparison-Report/report/'.md5($pr_id).'"> https://www.agedadvisor.nz/Provider-Comparison-Report/report/'.md5($pr_id).'</a><br><p>It will include contact information, star ratings, latest reviews and entry and fees information (where available). Should you have any questions, please do not hesitate to contact AgedAdvisor or the facility, directly.</p><br><p style="font-style:italic;">Please Note: The information contained in this report may include comments, reviews and historical information provided by residents and other parties, besides the provider. As such any data included may not be 100% complete or up to date, and therefore should be used as a guide only. We recommend visiting any facilities you are considering, and speaking.</p><img src="https://www.agedadvisor.nz/images/find_agedadvisor_retirement_villages_agedcare_logo.png"><br><p>Aged Advisor New Zealand  �  0800 AGED AD (2433 23)  �  Private Bag 4707, Christchurch 8140  �  https://www.agedadvisor.nz</p>');

                
            $to = $emailr;
            $headers  = "MIME-Version: 1.0\n";
            $headers .= "Content-type: text/html; charset=iso-8859-1\n";
            $headers .= 'From:info@agedadvisor.nz'."\n";	
            $email_subject_me = "Link to print report!";
            $email_txt = '<p>Here is your download link to view and print the report for </p> <br> <strong>'.$facility_title.' : </strong><a href="https://www.agedadvisor.nz/Provider-Comparison-Report/report/'.md5($pr_id).'"> https://www.agedadvisor.nz/Provider-Comparison-Report/report/'.md5($pr_id).'</a><br><p>It will include contact information, star ratings, latest reviews and entry and fees information (where available). Should you have any questions, please do not hesitate to contact AgedAdvisor or the facility, directly.</p><br><p style="font-style:italic;">Please Note: The information contained in this report may include comments, reviews and historical information provided by residents and other parties, besides the provider. As such any data included may not be 100% complete or up to date, and therefore should be used as a guide only. We recommend visiting any facilities you are considering, and speaking.</p><img src="https://www.agedadvisor.nz/images/find_agedadvisor_retirement_villages_agedcare_logo.png"><br><p>Aged Advisor New Zealand  �  0800 AGED AD (2433 23)  �  Private Bag 4707, Christchurch 8140  �  https://www.agedadvisor.nz</p>'; 
            $result = @mail($to, $email_subject_me, $email_txt, $headers); 

            if($result){                    
                echo "A download link has been sent to your email address : $emailr.";
            }else{
                echo "Server Error, please try after sometime.";
            }
        }
    } else { 
        echo("$emailr is not a valid email address!");         
    }    
}
?>