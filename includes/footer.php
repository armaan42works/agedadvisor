        <footer class="wow fadeInUp" data-wow-delay="0.5s">
           
            <div class="container">
                
                <div class="inner_footer">
                 
                    <div class="row">	
                 
                        <div class="col-md-9">
                         
                            <div class="left_footer_wrap">
                       
                                <div class="row">
                            
                                    <div class="col-md-4">
                                 
                                        <div class="repeat_footer_wrap float_list">
                                     
                                            <h3>North Island</h3>
                                     
                                            <ul>
                                        
                                                <li><a href="<?php echo HOME_PATH . 'search/for/149/Retirement-Villages-Rest-Homes-Aged-Care-Whangarei'; ?>">Whangarei</a></li>
                                        
                                                <li><a href="<?php echo HOME_PATH . 'search/for/158/Retirement-Villages-Rest-Homes-Aged-Care-New-Plymouth'; ?>">New Plymouth</a></li>

                                                <li><a href="<?php echo HOME_PATH . 'search/for/150/Retirement-Villages-Rest-Homes-Aged-Care-Auckland'; ?>">Auckland</a></li>

                                                <li><a href="<?php echo HOME_PATH . 'search/for/159/Retirement-Villages-Rest-Homes-Aged-Care-Wanganui'; ?>">Wanganui</a></li>

                                                <li><a href="<?php echo HOME_PATH .'search/for/152/Retirement-Villages-Rest-Homes-Aged-Care-Hamilton'; ?>">Hamilton</a></li>	

                                                <li><a href="<?php echo HOME_PATH . 'search/for/160/Retirement-Villages-Rest-Homes-Aged-Care-Feilding'; ?>">Feilding</a></li>

                                                <li><a href="<?php echo HOME_PATH . 'search/for/153/Retirement-Villages-Rest-Homes-Aged-Care-Tauranga'; ?>">Tauranga</a></li>

                                                <li><a href="<?php echo HOME_PATH . 'search/for/161/Retirement-Villages-Rest-Homes-Aged-Care-Palmerston-North'; ?>">Palmerston</a></li>

                                                <li><a href="<?php echo HOME_PATH . 'search/for/154/Retirement-Villages-Rest-Homes-Aged-Care-Rotorua'; ?>">Rotorua</a></li>

                                                <li><a href="<?php echo HOME_PATH . 'search/for/157/Retirement-Villages-Rest-Homes-Aged-Care-Napier'; ?>">Taupo</a></li>

                                                <li><a href="<?php echo HOME_PATH . 'search/for/162/Retirement-Villages-Rest-Homes-Aged-Care-Masterton'; ?>">Masterton</a></li>

                                                <li><a href="<?php echo HOME_PATH . 'search/for/156/Retirement-Villages-Rest-Homes-Aged-Care-Gisborne'; ?>">Gisborne</a></li>

                                                <li><a href="<?php echo HOME_PATH . 'search/for/163/Retirement-Villages-Rest-Homes-Aged-Care-Upper-Hutt'; ?>">Upper Hutt</a></li>

                                                <li><a href="<?php echo HOME_PATH . 'search/for/157/Retirement-Villages-Rest-Homes-Aged-Care-Napier'; ?>">Napier</a></li>

                                                <li><a href="<?php echo HOME_PATH . 'search/for/164/Retirement-Villages-Rest-Homes-Aged-Care-Lower-Hutt'; ?>">Lower Hutt</a></li>

                                            </ul>

                                        </div>

                                    </div>

                                    <div class="col-md-2">

                                        <div class="repeat_footer_wrap">
        
                                            <h3>South Island</h3>
            
                                            <ul>
            
                                                <li><a href="<?php echo HOME_PATH . 'search/for/166/Retirement-Villages-Rest-Homes-Aged-Care-Nelson'; ?>">Nelson</a></li>
                                        
                                                <li><a href="<?php echo HOME_PATH . 'search/for/167/Retirement-Villages-Rest-Homes-Aged-Care-Blenheim'; ?>">Blenheim</a></li>

                                                <li><a href="<?php echo HOME_PATH . 'search/for/168/Retirement-Villages-Rest-Homes-Aged-Care-Christchurch'; ?>">Christchurch</a></li>

                                                <li><a href="<?php echo HOME_PATH . 'search/for/169/Retirement-Villages-Rest-Homes-Aged-Care-Timaru'; ?>">Timaru</a></li>

                                                <li><a href="<?php echo HOME_PATH . 'search/for/170/Retirement-Villages-Rest-Homes-Aged-Care-Oamaru'; ?>">Oamaru</a></li>	

                                                <li><a href="<?php echo HOME_PATH . 'search/for/171/Retirement-Villages-Rest-Homes-Aged-Care-Dunedin'; ?>">Dunedin</a></li>

                                                <li><a href="<?php echo HOME_PATH . 'search/for/172/Retirement-Villages-Rest-Homes-Aged-Care-Invercargill'; ?>">Invercargill</a></li>

                                            </ul>

                                        </div>

                                    </div>

                                    <div class="col-md-2">
            
                                        <div class="repeat_footer_wrap">
                
                                            <h3>Facility Types</h3>
                    
                                            <ul>
                    
                                                <li><a href="<?php echo HOME_PATH . 'search/for/173/Retirement-Village-Apartment-1-bedroom'; ?>">Apartment 1 bedroom</a></li>
                            
                                                <li><a href="<?php echo HOME_PATH . 'search/for/174/Retirement-Village-Apartment-2-bedroom'; ?>">Apartment 2 bedroom</a></li>
                            
                                                <li><a href="<?php echo HOME_PATH . 'search/for/175/Retirement-Village-Apartment-3-bedroom'; ?>">Apartment 3 bedroom</a></li>
                            
                                                <li><a href="<?php echo HOME_PATH . 'search/for/176/Retirement-Village-Studio-Unit'; ?>">Studio Unit</a></li>
                            
                                                <li><a href="<?php echo HOME_PATH . 'search/for/177/Rest-home-care'; ?>">Rest home care</a></li>	
                            
                                                <li><a href="<?php echo HOME_PATH . 'search/for/179/Dementia-care'; ?>">Dementia care</a></li>
                            
                                                <li><a href="<?php echo HOME_PATH . 'search/for/180/Geriatric'; ?>">Geriatric</a></li>
                            
                                                <li><a href="<?php echo HOME_PATH . 'search/for/181/Psychogeriatric'; ?>">Psychogeriatric</a></li>
                            
                                            </ul>
                            
                                        </div>
                            
                                    </div>

                                    <div class="col-md-4">
                                        
                                        <div class="repeat_footer_wrap float_list">
                                            
                                            <h3>Information</h3>

                                            <ul>

                                                <li><a href="<?php echo HOME_PATH; ?>">Home</a></li>
            
                                                <li><a href="<?php echo HOME_PATH; ?>formal-complaints-hdc-dhb-moh">Voice a Complaint</a></li>
            
                                                <li><a href="<?php echo HOME_PATH; ?>about-us">About Us</a></li>
            
                                                <li><a href="<?php echo HOME_PATH; ?>sponsors-advertising">Sponsors & Advertising</a></li>
            
                                                <li><a href="<?php echo HOME_PATH; ?>articles">Articles</a></li>	

                                                <li><a href="<?php echo HOME_PATH; ?>contact-us">Contact Us</a></li>

                                                <li><a href="<?php echo HOME_PATH; ?>best-retirement-village-and-aged-care-awards">Best Facility Awards</a></li>

                                                <li><a href="<?php echo HOME_PATH ?>terms-and-conditions">Terms of Service & Privacy Policy</a></li>

                                                <li><a href="<?php echo HOME_PATH; ?>Compare-Retirement-Villages-Entry-Age-Costs-Fees-Conditions">Compare Villages</a></li>

                                                <li><a href="<?php echo HOME_PATH; ?>faq">FAQs</a></li>
                                            
                                                <li><a href="<?php echo HOME_PATH; ?>supplier/ranking">Compare Providers</a></li>

                                                <li><a href="<?php echo HOME_PATH; ?>reviews">Recent Reviews</a></li>

                                            </ul>
                                        
                                        </div>
                                    
                                    </div>
                        
                                </div>	
                            
                            </div>
                        
                        </div>
                        
                        <div class="col-md-3">
                        
                            <div class="repeat_footer_wrap right_footer">
                                
                                <h3>Contact us</h3>
                            
                                <p>
                            
                                    <span>
                                        
                                        <i class="fas fa-map-marker-alt"></i>
                                    
                                    </span>
                            
                                    Aged Advisor NZ Ltd, Private Bag 4707, Christchurch 8140
                            
                                </p> 

                                <p>
                            
                                    <span><i>&nbsp;</i></span> Office : 36A Sonter Drive, Wigram Business Park, Christchurch
                            
                                </p> 

                                <p>
                            
                                    <span><i class="far fa-envelope-open"></i></span>
                            
                                    <a href="mailto:info@listagram.com">info@listagram.com</a>	
                            
                                </p> 

                                <p>
                            
                                    <span><i class="fas fa-phone-volume"></i></span>
                            
                                    <a href="tel:44 078 767 595">+44 078 767 595</a>
                            
                                </p> 

                                <div class="social_media_icon">
                            
                                    <h3>Social Media</h3>	

                                    <a href="https://www.facebook.com/agedadvisor" target="_blank">
                                    
                                        <i class="fab fa-facebook-f"></i>
                                    
                                    </a>
                
                                    <a href="https://twitter.com/aged_nz" target="_blank">
                                    
                                        <i class="fab fa-twitter"></i>
                                    
                                    </a>
                
                                    <a href="javascript:void(0);"><i class="fab fa-pinterest"></i></a>
                
                                    <a href="javascript:void(0);"><i class="fab fa-google-plus-g"></i></a>
                
                                </div>

                            </div>
                    
                        </div>	
                    
                    </div>	
                        
                </div>

                <div class="bottom_footer">
                
                    <div class="bottom_footer_link">
                        
                        <a href="javascript:void(0);">Terms of Service & Privacy Policy</a>	
                            
                    </div>
                        
                    <div class="copyright">
                        
                        <p>
                            
                            <img src="img/copyright_icon.png" alt="img"> 2019 &copy; All Rights Reserved.
                        
                        </p>	
                            
                    </div>
                        
                </div>

            </div>
            
        </footer>

        <script type="text/javascript" src="<?php echo HOME_PATH; ?>includes/njs/jquery-3.3.1.min.js"></script>
        
        <script type="text/javascript" src="<?php echo HOME_PATH; ?>includes/njs/bootstrap.bundle.min.js"></script>
        
        <script type="text/javascript" src="<?php echo HOME_PATH; ?>includes/njs/bootstrap.min.js"></script>
        
        <script type="text/javascript" src="<?php echo HOME_PATH; ?>includes/njs/owl.carousel.js"></script>
        
        <script type="text/javascript" src="<?php echo HOME_PATH; ?>includes/njs/main.js"></script>
        
        <script type="text/javascript" src="<?php echo HOME_PATH; ?>includes/njs/wow.js"></script>

        <script>
    
            wow = new WOW({

                animateClass: 'animated',
            
                offset:       100,
    
                callback:     function(box) {
        
                  console.log("WOW: animating <" + box.tagName.toLowerCase() + ">")
        
                }
      
            });
            
            wow.init();
    
        </script>
       
    </body>

</html>