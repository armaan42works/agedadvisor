<?php

    /*
     * @version 1.0
     * @author CK
     * @date 1010-Feb-10013
     * @desc this class can be used to show a dynamic list has paging.
     * 		 e.g: You have a list of 100 record and you fetch that through
     * 				ajax but now you want to show these records by paging
     *   
     * 
     * 
     */
?>


<?php

    //---------------------------------------
    class AjaxPagingManager {

        private $base_url = ''; // The page we are linking to
        private $pgparam = 'page';
        private $getSrr = '';
        private $total_records = ''; // Total number of items (database results)
        private $per_page = 10; // Max number of items you want shown per page
        private $cur_page = 0; // The current page being viewed
        private $pagingStyle = SERIES_1103; // paging style
        private $pagingStr = ''; //
        public $pagename = '';
        public $extraParam = '';
        public $div = '';
        public $pageno = '';
        // for 1103_PAGING
        private $seriesRange = 5; // Number of "digit" links to show before/after the currently viewed page

        /**
         * Constructor
         *
         * @access	public
         * @param	array	initialization parameters
         * @param  pagingSize (Reocords per page)
         * @param pagename (php file name fetching ajax content)
         * @param div (html div id where to show result)
         * @param pageno
         * @param extraParam ( pass parameter to ajax php file) 
         */

        public function __construct($pagename, $div, $pageno = '', $extraParam = '', $pagingSize = 10, $pgparam = 'page') {
            $this->pagename = $pagename;
            $this->div = $div;
            $this->cur_page = $pageno;
            $this->extraParam = $extraParam;
            $this->pgparam = $pgparam;
            $this->initialize($pagingSize);
        }

        // --------------------------------------------------------------------

        /**
         * Initialize Preferences
         *
         * @access	private
         * @param	array	initialization parameters
         * @return	void
         */
        private function initialize($pagingSize) {

            if ((isset($_GET)) && (is_array($_GET))) {
                $getStr = '';
                foreach ($_GET as $key => $value) {
                    if (strstr($key, $this->pgparam)) {
                        continue;
                    }
                    $getStr .= "$key=$value&";
                }


                $this->getStr = ($getStr == '') ? "?" : '?' . $getStr;

                // make sure that pagenumber is integer
                if (!is_numeric($this->cur_page) OR ($this->cur_page == 0)) {
                    $this->cur_page = 1;
                }
            } else {
                $this->cur_page = 1;
            }

            $siteUrl = (($_SERVER['HTTPS'] != '') ? "https://" : "http://") . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];


            $this->base_url = $siteUrl;

            if ($pagingSize != '') {
                $this->per_page = $pagingSize;
            }
            // make sure that pagingsize is integer
            if (!is_numeric($this->per_page)) {
                $this->per_page = 10;
            }
        }

        public function getStart($pageNo,$per_page) {
            if(isset($per_page)){
                $this->per_page =   $per_page;
            }
            if ($pageNo > 0) {
                //first item to display on this page
                $this->start = ($pageNo - 1) * $this->per_page;
            } else {
                //if no page var is given, set start to 0
                $this->start = 0;
            }
            return $this->start;
        }

        public function getOffset($per_page) {
            if(isset($per_page)){
                return $per_page;
            }else{
                return $this->per_page;
            }
        }

        public function getPages($totalRecords,$per_page) {
            if(isset($per_page)){
                 $this->per_page = $per_page;
            }
            $totalPages = ceil($totalRecords / $this->per_page);
            return $totalPages;
        }

        public function doPaging($totalRecords, $pageNo) {
            if ($this->pagingStr != '') {
                return $this->pagingStr;
            }
            $this->total_records = $totalRecords;

            // If our item count or per-page total is zero there is no need to continue.
            if ($this->total_records == 0 OR $this->per_page == 0) {
                return '';
            }


            $this->pagingStr = $this->printPageNumbers('series', 'numbers', $pageNo);

            return $this->pagingStr;
        }

        // --------------------------------------------------------------------

        private function printPageNumbers($range = 'series', $type = 'numbers', $pageNo) {
            // find the total number of pages.
            $totalPages = ceil($this->total_records / $this->per_page);
            $paging = '';
            $this->cur_page = $pageNo;
            switch ($range) {


                case 'series':

                    if ($this->cur_page < $this->seriesRange) {
                        $seriesrange1 = ($this->seriesRange - $this->cur_page) + $this->seriesRange;
                    } else {
                        $seriesrange1 = $this->seriesRange;
                    }

                    if (($totalPages - $this->cur_page) < $this->seriesRange) {
                        $seriesrange10 = ($this->seriesRange - 1 - ($totalPages - $this->cur_page)) + $this->seriesRange;
                    } else {
                        $seriesrange10 = $this->seriesRange;
                    }

                    $from = ($this->cur_page - $seriesrange10 < 1) ? 1 : $this->cur_page - $seriesrange10;
                    $to = ($this->cur_page + $seriesrange1 > $totalPages) ? $totalPages : $this->cur_page + $seriesrange1;

                    if (($to - $from) >= ($this->seriesRange * 10)) {
                        // make sure that the total page number printed at any time is seriesrnage*10
                        if ($seriesrange10 > $seriesrange1) {
                            $from--;
                        } else {
                            $to--;
                        }
                    }

                    $prev = ($this->cur_page > 1) ? $this->cur_page - 1 : 1;
                    $next = ($this->cur_page < $totalPages) ? $this->cur_page + 1 : $totalPages;
                    if ($this->cur_page == 1) {
                        $paging .= "<span class=\"disabled\">&lt;&lt;</span>";
                        $paging .= "<span class=\"disabled\">Previous</span> ";
                    } else {
                        // Print last and previous page number links
                        $paging .= "<a href=\"javascript:ajaxPaging(pagename='$this->pagename',page='1',extra='$this->extraParam',div='$this->div')\" title=\"First Page\">&lt;&lt;</a> ";
                        $paging .= "<a href=\"javascript:ajaxPaging(pagename='$this->pagename',page='$prev',extra='$this->extraParam',div='$this->div')\"title=\"Previous Page\">Previous</a> ";
                    }

                    for ($i = $from; $i <= $to; $i++) {
                        switch ($type) {
                            case 'numbers':
                                if ($i != $this->cur_page) {
                                    $paging .= "<a href=\"javascript:ajaxPaging(pagename='$this->pagename',page='$i',extra='$this->extraParam',div='$this->div')\">$i</a> ";
                                } else {
                                    $paging .= "<span class=\"current\">$i</span>";
                                }
                                break;

                            case 'fromto':

                                $pageFrom = $this->per_page * ($i - 1) + 1;
                                $pageTo = $pageFrom + $this->per_page - 1;
                                if ($pageTo > $this->total_records) {
                                    $pageTo = $this->total_records;
                                }
                                if ($i != $this->cur_page) {
                                    $paging .= "<a href=\"$this->base_url$this->getStr$this->pgparam=$i\">$pageFrom-$pageTo</a> ";
                                } else {
                                    $paging .= "<span class=\"current\">$pageFrom-$pageTo</span> ";
                                }
                                break;
                        }
                    }

                    if ($this->cur_page == $totalPages) {
                        $paging .= "<span class=\"disabled\">Next </span>";
                        $paging .= "<span class=\"disabled\">&gt;&gt;</span> ";
                    } else {
                        $paging .= "<a href=\"javascript:ajaxPaging(pagename='$this->pagename',page='$next',extra='$this->extraParam',div='$this->div')\"title=\"Next Page\">Next</a>";
                        $paging .= "<a href=\"javascript:ajaxPaging(pagename='$this->pagename',page='$totalPages',extra='$this->extraParam',div='$this->div')\" title=\"Last Page\">&gt;&gt;</a> ";
                    }
                    break;
            } // End of main switch statement.

            return "<div class=\"pagination\">" . $paging . "</div>";
        }

        public function getPagingString($str = '') {
            if ($str == '') {
                $str = "Showing %d to %d of %d records";
            }

            $pageFrom = $this->per_page * ($this->cur_page - 1) + 1;
            $pageTo = $pageFrom + $this->per_page - 1;
            if ($pageTo > $this->total_records) {
                $pageTo = $this->total_records;
            }

            return sprintf($str, $pageFrom, $pageTo, $this->total_records);
        }

        public function set_base_url($url) {
            $this->base_url = $url;
        }

    }

// END Pagination Class
?>