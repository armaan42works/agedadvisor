<?php

    /*     * ********Constant input************ */
    define("ADMIN_EMAIL", "nigel@agedadvisor.co.nz");
    define("TITLE", "Aged Advisor");
    define("ADMIN_NAME", "Nigel");
    define('DEBUG_QUERY', FALSE);
    /*     * ********EOF DB Config*************************** */

    /*     * ********Twitter Apps key************* */
    define('YOUR_CONSUMER_KEY', 'CBgkztjAJ7AWrCY61PdeChW9P');
    define('YOUR_CONSUMER_SECRET', 'BNZ03C6xyg4ja1EulJguuAFaaIQFKNvyLF8JdmaYQzB21EvaK8');

    /*     * *******Version settings************************* */
    if ($_SERVER['HTTP_HOST'] == 'qa.ilmptech.com') {
        define("ADMIN_VERSION", "v4.3.5.0/");
        define("FRONT_VERSION", "v4.3.5.0/");
        /*         * ********Facebook Apps key************* */
        define('APPID', '410826909068365');
        define('APP_SECRET_KEY', 'ae7d46c7d7d91a7948fcbd20c8141e0c');
    } else {
        define("ADMIN_VERSION", "");
        define("FRONT_VERSION", "");
        define('APPID', '642050292578596'); //test4live.com
        define('APP_SECRET_KEY', 'dbc7ce4759a0cf7fafa8a9292e1f9e6e'); //test4live.com
    }
    /*     * ********BOF ADMIN PATH DEFINED************************ */
//$_SERVER['HTTP_HOST'] = '210.7.76.130:8029';
    define("ADMIN_PATH", $_SERVER['DOCUMENT_ROOT'] . "/" . ADMIN_VERSION . "admin/");
    define("HOME_PATH_URL", "http://" . $_SERVER['HTTP_HOST'] . "/" . ADMIN_VERSION . "admin/");
    define("MAIN_PATH", "http://" . $_SERVER['HTTP_HOST'] . "/" . ADMIN_VERSION . "admin/");
    define("INCLUDE_PATH", ADMIN_PATH . "include/");
    define("ADMIN_CSS", HOME_PATH_URL . "css/");
    define("ADMIN_IMAGE", HOME_PATH_URL . "img/");
    define("ADMIN_JS", HOME_PATH_URL . "js/");
    define("MODULE_PATH", ADMIN_PATH . "modules/");
    define("USER_PATH", MODULE_PATH . "users/");
    define("ACCOUNT_PATH", MODULE_PATH . "accounts/");
    define('END_TRANSACTION', FALSE);
    /*     * ********EOF PATH DEFINED************************ */

    /*     * ********BOF FRONT PAGE PATH DEFINED************************ */
    define("DOCUMENT_PATH", $_SERVER['DOCUMENT_ROOT'] . "/" . FRONT_VERSION);
    define("HOME_PATH", "http://" . $_SERVER['HTTP_HOST'] . "/" . FRONT_VERSION);
    define("INCLUDE_FILE", DOCUMENT_PATH . "includes/");
    define("MODULE_FRONT", DOCUMENT_PATH . "modules/");
    define("IMAGES", HOME_PATH . "images/");
    define("SCRIPT", HOME_PATH . "js/");
    define("CSS", HOME_PATH . "/style/");

    /*     * ********Email Id******************** */
    define('FROM', 'nigel@agedadvisor.co.nz');
    define('ADMINNAME', 'Aged Advisor');

    /*     * ******************************** */

    /*     * ********Paging constant******************** */
    define('PAGE_PER_NO', 10);
    define('SEARCH_PAGE_PER_NO', 5);
    define('COUPON_PER_NO', 3);

    /*     * ********BOF Global Array for Admin ************************ */
    $targetClient = array('0' => 'New Client', '1' => 'Existing Client');
    $promoCode = array('0' => 'Coupon', '1' => 'Gift Card');
    $couponType = array('0' => '$', '1' => '%');
    $clientType = array('0' => 'Client', '1' => 'Sales Person');
    $validate = array('0' => 'Pending email verification', '1' => 'Validated', '2' => 'Quoted');
    /* source for the da_user table */
    $source = array('0' => 'Google Search', '1' => 'Sales Person', '2' => 'Online Ads', '3' => 'Existing clients', '4' => 'Newspaper', '5' => 'Facebook', '6' => 'LinkedIn', '7' => 'Twitter', '8' => 'Other');
    $jobStatus = array('0' => 'N/A', '1' => 'Selected', '2' => 'Job Offered', '3' => 'Job Denied');
    /*     * ********EOF Global Array for Admin ************************ */
?>
