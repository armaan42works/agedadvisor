<?php

// Written by malcolm to Clean variables for X script attacks
function cleanthishere($var){
$var = str_replace("'", "", $var);
$var = str_replace('"', "", $var);
$var = str_replace('<', "", $var);
$var = str_replace('>', "", $var);
$var = str_replace('(', "", $var);
$var = str_replace(')', "", $var);

//HTML
$var = str_replace("&#39;", "", $var);
$var = str_replace("&#34;", "", $var);
$var = str_replace("&#60;", "", $var);
$var = str_replace("&#62;", "", $var);
$var = str_replace("&#63;", "", $var);
$var = str_replace("&#40;", "", $var);
$var = str_replace("&#41;", "", $var);

//Hex
$var = str_ireplace('%27', "", $var);
$var = str_ireplace('%22', "", $var);
$var = str_ireplace('%3C', "", $var);
$var = str_ireplace('%3E', "", $var);
$var = str_ireplace('%3F', "", $var);
$var = str_ireplace('%28', "", $var);
$var = str_ireplace('%29', "", $var);

$vartrimed=trim($var);
return $vartrimed;
}



//redirect to URL
function redirect_to($url) {
    //die($url);
    echo "<script>window.location.href='" . $url . "';</script>";
    exit;
}

//print well formatted array
function pr($arr) {
    echo '<pre>';
    print_r($arr);
    echo '</pre>';
}

//print well formatted array n die
function prd($arr) {
    echo '<pre>';
    print_r($arr);
    echo '</pre>';
    die();
}

// remove trailing char
function remove_last_char($str, $char) {
    if (substr($str, -1) == $char)
        return substr($str, 0, -1);
    else
        return $str;
}

// split url into different pieces
function split_url($url) {
    global $TW_URL;
    $TW_URL = explode('/', $url);
}

// active/inactive icons
function show_active_inactive_icon($status = true) {
    if ($status)
        return '<img src="' . PATH_IMG . 'icons/active.png" border="0" alt="active" title="active" />';
    else
        return '<img src="' . PATH_IMG . 'icons/inactive.png" border="0" alt="inactive" title="inactive" />';
}

// yes/no icons
function show_yes_no_icon($yes = true) {
    if ($yes)
        return '<img src="' . PATH_IMG . 'icons/yes.png" border="0" alt="yes" title="yes" />';
    else
        return '<img src="' . PATH_IMG . 'icons/no.png" border="0" alt="no" title="no" />';
}

function show_edit_delete_icon($edit = true) {
    if ($edit)
        return '<img src="' . PATH_IMG . 'icons/edit.png" border="0" alt="edit" title="edit" />';
    else
        return '<img src="' . PATH_IMG . 'icons/delete.png" border="0" alt="delete" title="delete" />';
}

//format date for mysql
function mysql_date() {
    return date('Y-m-d H:i:s');
}

//generate email link
function email_link($email) {
    return '<a href="mailto:' . $email . '">' . $email . '</a>';
}

//format date in mm-dd-yyyy
function format_date($date, $show_time = false) {
    if ($show_time)
        return date('m-d-Y H:i:s', strtotime($date));
    else
        return date('m-d-Y', strtotime($date));
}

// show error or msg
function show_message($status, $msg, $email) {

    if (empty($status))
        $status = $_GET['st'];
    if (empty($msg))
        $msg = $_GET['msg'];

    if ($status == 0) { //error
        return '<div id="error" class="error">
				<img src="' . PATH_IMG . 'icons/error.png" align="absmiddle">&nbsp;' . $msg . '<br>
			</div><br>';
        //echo '<span style="color:#f00;font-weight:bold">'.$_GET['msg'].'</span><br><br>';
    } elseif ($status == 1) { //msg success
        return '<div id="success" class="success">
				<img src="' . PATH_IMG . 'icons/success.png" align="absmiddle">&nbsp;' . $msg . "&nbsp;" . $email . '<br>
			</div><br>';
        //echo '<span style="color:#005F04;font-weight:bold">'.$_GET['msg'].'</span><br><br>';
    } elseif ($status == 2) { //msg
        return '<span style="font-size:12px;"><img src="' . PATH_IMG . 'checkmark.png">Email has been sent.</span>';
    }
}

// show common msg
function common_message($status, $msg) {
    if ($status == 0) { //error
        return '<div id="error" class="errorForm">
				<img src="' . IMAGES . 'error.png" align="absmiddle">&nbsp;' . $msg . '<br>
			</div>';
        //echo '<span style="color:#f00;font-weight:bold">'.$_GET['msg'].'</span><br><br>';
    } elseif ($status == 1) { //msg
        return '<div id="success" class="successForm">
				<img src="' . IMAGES . 'success.png" align="absmiddle">&nbsp;' . $msg . '<br>
			</div>';
        //echo '<span style="color:#005F04;font-weight:bold">'.$_GET['msg'].'</span><br><br>';
    }
}

// show common msg
function common_message_supplier($status, $msg) {
    if ($status == 0) { //error
        return '<div id="error" class="errorForm">
				<img src="' . IMAGES . 'error.png" align="absmiddle">&nbsp;' . $msg . '<br>
			</div>';
        //echo '<span style="color:#f00;font-weight:bold">'.$_GET['msg'].'</span><br><br>';
    } elseif ($status == 1) { //msg
        return '<div id="success" class="successForm">
				<i class="fa fa-check-square"></i>&nbsp;' . $msg . '<br>
			</div>';
        //echo '<span style="color:#005F04;font-weight:bold">'.$_GET['msg'].'</span><br><br>';
    }
}

//convert filesize from bytes to kb,mb,gb
function convert_file_size($to, $size) {
    if ($size <= 0)
        return 0;
    else {
        $to = strtolower($to);
        switch ($to) {
            case 'kb':
                $newsize = ceil($size / 1024);
                break;
            case 'mb':
                $newsize = ceil(($size / 1024) / 1024);
                break;
            case 'gb':
                $newsize = ceil((($size / 1024) / 1024) / 1024);
                break;
        }
        return $newsize . strtoupper($to);
    }
}

// resize image propotionally (not repalcing the target file)

function ak_img_thumb($target, $newcopy, $width, $height, $ext) { 
  $xoffset = 0;
    $yoffset = 0;
    
    
    
 list($org_width, $org_height) =  getimagesize($target); 


    $ext = strtolower($ext);
    if ($ext == "gif") {
        $img = imagecreatefromgif($target);
    } else if ($ext == "png") {
        $img = imagecreatefrompng($target);
    } else if ($ext == "tif") {
        $img = $target;
    } else {
        $img = imagecreatefromjpeg($target);
    }
    
      if ($org_width / $width > $org_height/ $height)
        {
            $xtmp = $org_width;
            $xratio = 1-((($org_width/$org_height)-($width/$height))/2);
            $org_width = $org_width * $xratio;
            $xoffset = ($xtmp - $org_width)/2;
        }
        elseif ($org_height/ $height > $org_width / $width)
        {
            $ytmp = $org_height;
            $yratio = 1-((($width/$height)-($org_width/$org_height))/2);
            $org_height = $org_height * $yratio;
            $yoffset = ($ytmp - $org_height)/2;
        }
        
        
        
        //echo "Width orig $w_orig  minus New width $new_width /2 = $new_offset_x<br>";
//echo " $ratio_of_thumb  ---- $ratio_of_orig ----    $tci, $img, 0, 0, ".$new_offset_x.", ".$new_offset_y.", $w, $h, $new_width, $new_height)";//exit;
    
   $img_n=imagecreatetruecolor ($width, $height);   
    imagecopyresampled($img_n, $img, 0, 0, $xoffset, $yoffset, $width, $height, $org_width, $org_height);
    imagejpeg($img_n, $newcopy, 80);
 }



function ak_img_resize($target, $newcopy, $w, $h, $ext) {
    list($w_orig, $h_orig) = getimagesize($target);
    $scale_ratio = $w_orig / $h_orig;
    if (($w / $h) > $scale_ratio) {
        $w = $h * $scale_ratio;
    } else {
        $h = $w / $scale_ratio;
    }
    $img = "";
    $ext = strtolower($ext);
    if ($ext == "gif") {
        $img = imagecreatefromgif($target);
    } else if ($ext == "png") {
        $img = imagecreatefrompng($target);
    } else if ($ext == "tif") {
        $img = $target;
    } else {
        $img = imagecreatefromjpeg($target);
    }
    $tci = imagecreatetruecolor($w, $h);
    // imagecopyresampled(dst_img, src_img, dst_x, dst_y, src_x, src_y, dst_w, dst_h, src_w, src_h)
    imagecopyresampled($tci, $img, 0, 0, 0, 0, $w, $h, $w_orig, $h_orig);
    imagejpeg($tci, $newcopy, 80);
}

// resize image propotionately
function resize_image($path, $imagename, $width, $height) {
    $filename = $path . $imagename;

    // Check if file exists
    //if (!file_exists($filename))
    if (!@getimagesize($filename)) {
        echo "Error occured while resizing (file does not exist)";
        die();
    }

    // Set a maximum height and width
    $size = getimagesize($filename);

    // Content type
    //header("Content-type: ".$size['mime']."");
    $content_type = $size['mime'];
    $type = str_replace("image/", "", $size['mime']);

    // Get new dimensions
    $width_orig = $size[0];
    $height_orig = $size[1];

    if (($width_orig < $width) && ($height_orig < $height)) {
        $width = ceil($width_orig);
        $height = ceil($height_orig);
    } else {
        if ($width && ($width_orig < $height_orig)) {
            $width = ceil(($height / $height_orig) * $width_orig);
        } else {
            $height = ceil(($width / $width_orig) * $height_orig);
        }
    }

    // Resample
    $image_p = imagecreatetruecolor($width, $height);
    switch ($type) {
        case'gif':
            $image = imagecreatefromgif($filename);
            break;
        case'pjpeg':
            $image = imagecreatefromjpeg($filename);
            break;
        case'jpeg':
            $image = imagecreatefromjpeg($filename);
            break;
        case'jpg':
            $image = imagecreatefromjpeg($filename);
            break;
        case'png':
            $image = imagecreatefrompng($filename);
            break;
    }
    imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);

    // Output
    ob_start();

    switch ($type) {
        case'gif':
            imagegif($image_p, $filename);
            break;
        case'pjpeg':
            imagejpeg($image_p, $filename);
            break;
        case'jpeg':
            imagejpeg($image_p, $filename);
            break;
        case'jpg':
            imagejpeg($image_p, $filename);
            break;
        case'png':
            imagepng($image_p, $filename);
            break;
    }
    ob_end_flush();
    imagedestroy($image_p);
}

//convert text to password old
/* function encrypt_password($str)
  {
  $plain=$str.SALT;
  $pwd=md5($plain);
  return $pwd;
  } */

//convert text to password new
function encrypt_password($str) {
    $salt = sha1(time());
    $salt_pwd = sha1("change-me--$str--");
    $pwd = sha1("change-me--" . $salt . $salt_pwd . "--");
    return $pwd;
}

//get current user
function get_login_user($key) {
    return get_user_info($_SESSION['user_id'], 'id');
}

//get user info
function get_user_info($id, $field) {
    global $db;
    $sql = 'select ' . $field . ' from ' . PREFIX . 'module_users where id=' . $id;
    list($result) = $db->sql_fetchrow($db->sql_query($sql));
    return $result;
}

// generate temporary password
function generate_password($length = 8, $level = 2) {
    list($usec, $sec) = explode(' ', microtime());
    srand((float) $sec + ((float) $usec * 100000));

    $validchars[1] = "0123456789abcdfghjkmnpqrstvwxyz";
    $validchars[2] = "0123456789abcdfghjkmnpqrstvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    $validchars[3] = "0123456789_!@#$%&*()-=+/abcdfghjkmnpqrstvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_!@#$%&*()-=+/";

    $password = "";
    $counter = 0;

    while ($counter < $length) {
        $actChar = substr($validchars[$level], rand(0, strlen($validchars[$level]) - 1), 1);

        // All character must be different
        if (!strstr($password, $actChar)) {
            $password .= $actChar;
            $counter++;
        }
    }

    return $password;
}

function send_email($to, $to_name, $from_name, $from, $subject, $message, $mail_type = '') {
    global $db;


$contenttype="text/plain; charset=UTF-8";
$content_type="text";
$strMessage = $message;
if (stristr($strMessage,"<table")!==FALSE){$contenttype="text/html; charset=UTF-8;";$content_type="html";}
if (stristr($strMessage,'style="')!==FALSE){$contenttype="text/html; charset=UTF-8;";$content_type="html";}
if (stristr($strMessage,'<p>')!==FALSE){$contenttype="text/html; charset=UTF-8;";$content_type="html";}
if (stristr($strMessage,'<b>')!==FALSE){$contenttype="text/html; charset=UTF-8;";$content_type="html";}
if (stristr($strMessage,'<br>')!==FALSE){$contenttype="text/html; charset=UTF-8;";$content_type="html";}
if (stristr($strMessage,'<img src')!==FALSE){$contenttype="text/html; charset=UTF-8;";$content_type="html";}
if (stristr($strMessage,'<a href="')!==FALSE){$contenttype="text/html; charset=UTF-8;";$content_type="html";}

if($contenttype=="text/html; charset:UTF-8;"){
	$strMessage=str_ireplace("\n",'<br>',$strMessage);
}



  $mail_to  = $to;
  $subject  = $subject;

  // Send by Normal Mail server
  
  $headers  = "MIME-Version: 1.0\r\n";

  $headers .= "Content-type: $contenttype\r\n";
  $headers .= "From: $from\r\n";
  if($bcc){ $headers .= "BCC: $bcc\r\n";}
  $headers .= "Sender: $from";
  //echo "here";
$result =   mail($mail_to, $subject, $message, $headers);


    if (!$result) {

        $email_sql = "INSERT INTO " . PREFIX . 'email_logs' . "(email_to, email_to_name, email_from, email_from_name, email_subject,email_content,email_date,email_status,email_type) values('" . $to . "','" . $to_name . "','" . $from . "','" . $from_name . "','" . $subject . "','" . $result . "',NOW(),'1','" . $mail_type . "')";
        $db->sql_query($email_sql);
        //echo 'Message was not sent.';
        //echo 'Mailer error: ' . $mail->ErrorInfo;
        return false;
    } else {

        $email_sql = "INSERT INTO " . PREFIX . 'email_logs' . "(email_to, email_to_name, email_from, email_from_name, email_subject,email_content,email_date,email_status,email_type) values('" . $to . "','" . $to_name . "','" . $from . "','" . $from_name . "','" . $subject . "','" . $message . "',NOW(),'0','" . $mail_type . "')";
        $db->sql_query($email_sql);
        //echo 'Message has been sent.';
        return true;
    }
}

function send_email_smtp($to, $to_name, $from_name, $from, $subject, $message, $mail_type = '') {
    global $db;


$contenttype="text/plain; charset=UTF-8";
$content_type="text";
$strMessage = $message;
if (stristr($strMessage,"<table")!==FALSE){$contenttype="text/html; charset=UTF-8;";$content_type="html";}
if (stristr($strMessage,'style="')!==FALSE){$contenttype="text/html; charset=UTF-8;";$content_type="html";}
if (stristr($strMessage,'<p>')!==FALSE){$contenttype="text/html; charset=UTF-8;";$content_type="html";}
if (stristr($strMessage,'<b>')!==FALSE){$contenttype="text/html; charset=UTF-8;";$content_type="html";}
if (stristr($strMessage,'<br>')!==FALSE){$contenttype="text/html; charset=UTF-8;";$content_type="html";}
if (stristr($strMessage,'<img src')!==FALSE){$contenttype="text/html; charset=UTF-8;";$content_type="html";}
if (stristr($strMessage,'<a href="')!==FALSE){$contenttype="text/html; charset=UTF-8;";$content_type="html";}

if($contenttype=="text/html; charset:UTF-8;"){
	$strMessage=str_ireplace("\n",'<br>',$strMessage);
}



  $mail_to  = $to;
  $subject  = $subject;

  // Send by Normal Mail server
  
  $headers  = "MIME-Version: 1.0\r\n";

  $headers .= "Content-type: $contenttype\r\n";
  $headers .= "From: $from\r\n";
  if($bcc){ $headers .= "BCC: $bcc\r\n";}
  $headers .= "Sender: $from";
  //echo "here";
$result =   mail($mail_to, $subject, $message, $headers);


    if (!$result) {

        $email_sql = "INSERT INTO " . PREFIX . 'email_logs' . "(email_to, email_to_name, email_from, email_from_name, email_subject,email_content,email_date,email_status,email_type) values('" . $to . "','" . $to_name . "','" . $from . "','" . $from_name . "','" . $subject . "','" . $result . "',NOW(),'1','" . $mail_type . "')";
        $db->sql_query($email_sql);
        //echo 'Message was not sent.';
        //echo 'Mailer error: ' . $mail->ErrorInfo;
        return false;
    } else {

        $email_sql = "INSERT INTO " . PREFIX . 'email_logs' . "(email_to, email_to_name, email_from, email_from_name, email_subject,email_content,email_date,email_status,email_type) values('" . $to . "','" . $to_name . "','" . $from . "','" . $from_name . "','" . $subject . "','" . $message . "',NOW(),'0','" . $mail_type . "')";
        $db->sql_query($email_sql);
        //echo 'Message has been sent.';
        return true;
    }
}

// encrypt text using md5
function password($password) {
    return md5($password);
}

// create form input field
//function create_input($type, $name, $id, $default_value, $extra=array(),$class='')
//function create_input($type,$id,$name,$readonly=false,$value='',$onclick='',$checked=false,$onchange='',$style='',$onblur='',$extra='')

function create_input($type, $name, $id, $default_value, $onchange = '', $style = '', $onblur = '', $class = '', $length = '', $size = '', $onclick = '', $disabled = '', $extra = '') {
    if (empty($type) || empty($name))
        return false;

    if (empty($id))
        $id = $name;

    //echo $extra;
    $html = '';
    //echo $type;
    switch ($type) {
        case 'text':
            $html = '<input type="' . $type . '" name="' . $name . '" id="' . $id . '" value="' . $default_value . '"';
            break;
        case 'password':
            $html = '<input type="' . $type . '" name="' . $name . '" id="' . $id . '" value="' . $default_value . '" class="' . $class . '" "' . $length . '" ';
            break;
        case 'submit':
            $html = '<input type="' . $type . '" name="' . $name . '" id="' . $id . '" value="' . $default_value . '" class="' . $class . '" ';
            break;
        case 'button':
            $html = '<input type="' . $type . '" name="' . $name . '" id="' . $id . '" value="' . $default_value . '" ';
            break;
        case 'textarea':
            $html = '<textarea name="' . $name . '" id="' . $id . '" ';
            break;
        case 'checkbox':
            $html = '<input type="' . $type . '" name="' . $name . '" id="' . $id . '" value="' . $default_value . '" class="' . $class . '" ';
            break;
        case 'radio':
            $html = '<input type="' . $type . '" name="' . $name . '" id="' . $id . '" value="' . $default_value . '"';
            break;
        case 'file':
            $html = '<input type="' . $type . '" name="' . $name . '" id="' . $id . '" value="' . $default_value . '" ';
            break;
        case 'hidden':
            $html = '<input type="' . $type . '" name="' . $name . '" id="' . $id . '" value="' . $default_value . '"  class="' . $class . '" ';
            break;
        default:
            return false;
            break;
    }

    /* if(isset($extra) && count($extra) > 0)
      {
      foreach($extra as $key=>$value)
      {
      $html .= $key.'='.'"'.$value.'" ';
      }
      } */

    if ($onchange != '')
        $html.='onchange="' . $onchange . '" ';

    if ($onclick != '')
        $html.='onClick="' . $onclick . '" ';

    if ($style != '')
        $html.='style="' . $style . '" ';

    if ($length != '')
        $html.= 'maxlength="' . $length . '" ';

    if ($size != '')
        $html.= 'size="' . $size . '" ';

    if ($class != '')
        $html.= 'class="' . $class . '" ';

    if ($onblur != '')
        $html.='onblur="' . $onblur . '" ';

    if ($disabled != '')
        $html.= 'disabled="' . $disabled . '" ';

    if ($type = 'radio') {
        if ($extra != '') {
            //echo $extra;
            $html.= ($extra == $default_value) ? ' checked ' : '';
        }
    }

    if ($type = 'checkbox') {
        if ($extra != '') {
            //echo ">>".$extra.$default_value;
            $html.= ($extra == $default_value) ? 'checked="checked" ' : '';
        }
    }


    $html .= '/>';

    if ($type == 'textarea') {
        $html .= $default_value . '</textarea>';
    }


    return $html;
}

function create_input_manual($type, $name, $id, $default_value, $onchange = '', $style = '', $onblur = '', $class = '', $length = '', $size = '', $onclick = '', $disabled = '', $extra = '') {
    if (empty($type) || empty($name))
        return false;

    if (empty($id))
        $id = $name;

    //echo $extra;
    $html = '';
    //echo $type;
    switch ($type) {
        case 'text':
            $html = '<input type="' . $type . '" name="' . $name . '" id="' . $id . '" readonly="readonly" value="' . $default_value . '"';
            break;
        default:
            return false;
            break;
    }
    $html .= '>';
    return $html;
}

//generate html textarea control
function html_textarea($id, $name, $class, $value, $rows = '', $cols = '', $onBlur = '', $style = '', $onclick = '') {

    if ($class == 'mceEditor') {
        if (!is_numeric($rows) || !is_numeric($cols))
            $html = '<textarea id="' . $id . '" name="' . $name . '" class="' . $class . '" cols="80" rows="30">';
        else
            $html = '<textarea id="' . $id . '" name="' . $name . '" class="' . $class . '" cols="' . $cols . '" rows="' . $rows . '">';
    }
    else {
        if (!is_numeric($rows) || !is_numeric($cols))
            $html = '<textarea id="' . $id . '" name="' . $name . '" class="' . $class . '">';
        else
            $html = '<textarea id="' . $id . '" name="' . $name . '" class="' . $class . '" cols="' . $cols . '" rows="' . $rows . '" onblur="' . $onBlur . '" onClick="' . $onclick . '" style="' . $style . '">';
    }
    $html.=$value;

    $html.='</textarea>';
    return $html;
}

//generated the select box on base of table and value from table dynamically
function html_select($table, $id, $name, $field_id, $field_value, $selected = '', $onchange = '', $first_blank = false, $class, $disabled = '') {

    global $db;

    $sql = 'select ' . $field_id . ',' . $field_value . ' from ' . PREFIX . $table . ' order by ' . $field_id;
    $result = $db->sql_query($sql);

    $html = '<select id = "' . $id . '" name = "' . $name . '" class="' . $class . '"';

    if ($onchange != '') {

        $html.='onchange = "' . $onchange . '"';
    }

    if ($disabled != '')
        $html.= 'disabled="' . $disabled . '"';

    $html.='>';

    if ($first_blank)
        $html .='<option value="">Please Select</option>';

    if ($db->sql_numrows($result) > 0) {

        while ($row = $db->sql_fetchrow($result)) {

            $html .='<option value="' . $row[$field_id] . '"';
            if ($selected == $row[$field_id])
                $html .='selected ';
            $html .='>' . $row[$field_value] . '</option>';
        }
    }else {
        $html .='<option>' . ERR3 . '</option>';
    }
    $html .='</select>';

    return $html;
}

//it's only for used the country drop down in my account/edit account section
//generated the select box on base of table and value from table dynamically
function country_select($table, $id, $name, $field_id, $field_value, $selected = '', $onchange = '', $first_blank = false, $class, $disabled = '') {

    global $db;

    $sql = 'select ' . $field_id . ',' . $field_value . ' from ' . PREFIX . $table . ' order by ' . $field_id;
    $result = $db->sql_query($sql);

    $html = '<select id = "' . $id . '" name = "' . $name . '" class="' . $class . '"';

    if ($onchange != '') {

        $html.='onchange = "' . $onchange . '"';
    }

    if ($disabled != '')
        $html.= 'disabled="' . $disabled . '"';

    $html.='>';

    if ($first_blank)
        $html .='<option value="">Please Select</option>';

    if ($db->sql_numrows($result) > 0) {

        while ($row = $db->sql_fetchrow($result)) {

            $html .='<option value="' . $row[$field_id] . '"';
            if ($selected == $row[$field_id])
                $html .='selected ';
            $html .='>' . $row[$field_id] . '(' . $row[$field_value] . ')' . '</option>';
        }
    }else {
        $html .='<option>' . ERR3 . '</option>';
    }
    $html .='</select>';

    return $html;
}

//generated the select box for admin only
function input_select($id, $name, $selected = '', $onchange = '', $first_blank = false, $class) {

    $html = '<select id = "' . $id . '" name = "' . $name . '" class="' . $class . '"';

    $html.='>';

    $html .= '<option value="L"';
    if ($selected == 'L')
        $html .= 'selected';
    $html .='>Active</option>';

    $html .= '<option value="D"';
    if ($selected == 'D')
        $html .= 'selected';
    $html .='>Inactive</option>';

    $html .='</select>';

    return $html;
}

//generate the static select box by passing the key or values;
function manual_select($id, $name, $selected, $input_key, $input_value, $class = '', $first_blank = false, $onchange = '', $disabled = '', $style = '') {


    $html = '<select id = "' . $id . '" name = "' . $name . '" class="' . $class . '"';

    if ($onchange != '') {
        $html.='onchange = "' . $onchange . '"';
    }
    if ($disabled != '')
        $html.= 'disabled="' . $disabled . '"';

    if ($style != '')
        $html.='style="' . $style . '" ';

    $html.='>';

    if ($first_blank)
        $html .='<option value="">Please Select</option>';

    if (is_array($input_value) && count($input_value) > 0) {

        for ($i = 0; $i < count($input_value); $i++) {

            if (is_array($input_key) && count($input_key) > 0) {

                for ($j = 0; $j <= $i; $j++) {
                    if ($i == $j) {
                        $html .='<option value="' . $input_key[$i] . '"';
                        if ($selected == $input_key[$i])
                            $html .='selected ';
                        $html .='>' . $input_value[$i] . '</option>';
                    }
                }
            }else {

                $html .='<option value="' . $input_value[$i] . '"';
                if ($selected == $input_value[$i])
                    $html .='selected ';
                $html .='>' . $input_value[$i] . '</option>';
            }
        }
    }
    $html .='</select>';
    return $html;
}

//clean up a string to be used in URL
//http://www.bala-krishna.com/how-to-clean-special-characters-from-php-string/
//modified by sourabh
function clear_url($string) {
    // Replace other special chars
    $specialCharacters = array(
        '#' => '',
        '$' => '',
        '%' => '',
        '&' => '',
        '@' => '',
        //'.' => '',
        '+' => '',
        '=' => '',
        '\\' => '',
        //'/' => '',
        '~' => '',
        '!' => '',
        '^' => '',
        '*' => '',
        '(' => '',
        ')' => '',
        //'_' => '-',
        '{' => '',
        '}' => '',
        '|' => '',
        ':' => '',
        '"' => '',
        '<' => '',
        '>' => '',
        '?' => '',
        ',' => '',
        "'" => '',
        ';' => '',
        ']' => '',
        '[' => ''
    );

    while (list($character, $replacement) = each($specialCharacters)) {
        $string = str_replace($character, '', $string);
    }

    // Remove all remaining other unknown characters
    $string = preg_replace('/[^a-zA-Z-_0-9\-\/.]/', ' ', $string);
    //$string = preg_replace('/^[\-]+/', '', $string);
    //$string = preg_replace('/[\-]+$/', '', $string);
    //$string = preg_replace('/[\-]{2,}/', ' ', $string);

    $string = str_replace(' ', '-', $string);
    return str_replace('---', '-', $string);
}

//generate seo friendly urls for each module
function generate_seo_url_new($module_name, $id, $seo_url) {
    //echo $module_name;
    //echo $seo_url;
    $seo_url = str_replace('---', '-', $seo_url);

    if (strtolower($module_name) == 'asset')
        $url = WEBSITE . $module_name . '/download/' . $id . '/' . trim(strtolower($seo_url));
    else {
        //if($id=='' || $id=='0')
        //	$url=WEBSITE.$module_name;
        //else
        $url = WEBSITE . trim(strtolower($seo_url));
    }
    //echo $url;
    //echo $module_name.','.$id.','.$seo_url.'='.$url.'<br>';
    return $url;
}

//generate link
function generate_link($module_text, $module_name, $module_op, $module_item_id, $module_target = '', $seo_url = '', $params = '') {
    global $db;
    if (trim($module_name) == '') { //external link
        $html = '<a href="' . trim($module_op) . '"';
    } else { //cms link
        if ($seo_url === true) { //generate seo url and supply it to function below
            //$sql="select seo_url from ".PREFIX."module_".trim($module_name)." where id=".trim($module_item_id);
            echo $sql = "select seo_url from " . PREFIX . "seo_url where module_name='" . trim($module_name) . "' and module_item_id=" . trim($module_item_id);
            //echo $sql.'<br>';
            list($seo_url) = $db->sql_fetchrow($db->sql_query($sql));
        }
        //$html='<a href="'.generate_seo_url(trim($module_name),trim($module_op),trim($module_item_id),trim($seo_url)).'"';
        $html = '<a href="' . generate_seo_url_new(trim($module_name), trim($module_item_id), trim($seo_url)) . '"';
    }

    if ($module_target != '')
        $html.=' target="' . trim($module_target) . '"';

    if ($params != '')
        $html.=$params;

    $html.='>' . trim($module_text) . '</a>';
    return $html;
}

//check if user logged in
function is_loggedin($userid = '') {
    if (is_numeric($userid)) {
        if ($userid == $_SESSION['user_id'])
            return true;
        else
            return false;
    }
    else {
        if (isset($_SESSION['username']) && is_numeric($_SESSION['user_id']))
            return true;
        else
            return false;
    }
}

//check if user is super admin/admin
function is_admin($super = false) {
    global $db;

    $admin_group_id = array(1, 2); //super admin and admin group ids

    if (is_loggedin()) {
        $result = get_user_group($_SESSION['user_id']);
        if ($result != false) {
            $row = $db->sql_fetchrow($result);
            if (in_array($row['id'], $admin_group_id)) {
                if ($super) {
                    if ($row['id'] == '1') //super admin
                        return true;
                    else
                        return false;
                } else
                    return true;
            } else
                return false;
        } else
            return false;
    } else
        return false;
}

// email validation
function validate_email($email) {
    if (trim($email) == '')
        return false;

    $regexp = "/^[^0-9][A-z0-9_]+([.][A-z0-9_]+)*[@][A-z0-9_]+([.][A-z0-9_]+)*[.][A-z]{2,4}$/";

    if (preg_match($regexp, $email)) {
        return true;
    } else {
        return false;
    }
}

//function to find how time passed by passing the date.
function calculate_days($start_date) {

    if (!empty($start_date)) {

        $current_time = strtotime(date('Y-m-d h:i:s'));
        $create_date = strtotime($start_date);
        $diff = abs($current_time - $create_date);
        $years = floor($diff / (365 * 60 * 60 * 24));
        $months = floor(($diff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
        $days = ceil(abs($current_time - $create_date) / 86400);

        if ($years) {
            return $years . " years";
        } else if ($months) {
            return $months . " months";
        } else if ($days) {
            return $days . " days";
        } else {
            return "0 days";
        }
    } else {
        return "0 days";
    }
}

//function to find state name by passing the state id
function find_state($state_id) {
    global $db;
    if ($state_id) {
        $sql = "select statename from " . PREFIX . "geo_states where id='" . $state_id . "'";
        list($result) = $db->sql_fetchrow($db->sql_query($sql));
        return $result;
    } else {
        return false;
    }
}

function ago($datefrom, $dateto = -1) {
    // Defaults and assume if 0 is passed in that
    // its an error rather than the epoch

    if ($datefrom == 0) {
        return "A long time ago";
    }
    if ($dateto == -1) {
        $dateto = time();
    }

    // Make the entered date into Unix timestamp from MySQL datetime field

    $datefrom = strtotime($datefrom);

    // Calculate the difference in seconds betweeen
    // the two timestamps

    $difference = $dateto - $datefrom;

    // Based on the interval, determine the
    // number of units between the two dates
    // From this point on, you would be hard
    // pushed telling the difference between
    // this function and DateDiff. If the $datediff
    // returned is 1, be sure to return the singular
    // of the unit, e.g. 'day' rather 'days'

    switch (true) {
        // If difference is less than 60 seconds,
        // seconds is a good interval of choice
        case(strtotime('-1 min', $dateto) < $datefrom):
            $datediff = $difference;
            $res = ($datediff == 1) ? $datediff . ' second ago' : $datediff . ' seconds ago';
            break;
        // If difference is between 60 seconds and
        // 60 minutes, minutes is a good interval
        case(strtotime('-1 hour', $dateto) < $datefrom):
            $datediff = floor($difference / 60);
            $res = ($datediff == 1) ? $datediff . ' minute ago' : $datediff . ' minutes ago';
            break;
        // If difference is between 1 hour and 24 hours
        // hours is a good interval
        case(strtotime('-1 day', $dateto) < $datefrom):
            $datediff = floor($difference / 60 / 60);
            $res = ($datediff == 1) ? $datediff . ' hour ago' : $datediff . ' hours ago';
            break;
        // If difference is between 1 day and 7 days
        // days is a good interval
        case(strtotime('-1 week', $dateto) < $datefrom):
            $day_difference = 1;
            while (strtotime('-' . $day_difference . ' day', $dateto) >= $datefrom) {
                $day_difference++;
            }

            $datediff = $day_difference;
            $res = ($datediff == 1) ? 'yesterday' : $datediff . ' days ago';
            break;
        // If difference is between 1 week and 30 days
        // weeks is a good interval
        case(strtotime('-1 month', $dateto) < $datefrom):
            $week_difference = 1;
            while (strtotime('-' . $week_difference . ' week', $dateto) >= $datefrom) {
                $week_difference++;
            }

            $datediff = $week_difference;
            $res = ($datediff == 1) ? 'last week' : $datediff . ' weeks ago';
            break;
        // If difference is between 30 days and 365 days
        // months is a good interval, again, the same thing
        // applies, if the 29th February happens to exist
        // between your 2 dates, the function will return
        // the 'incorrect' value for a day
        case(strtotime('-1 year', $dateto) < $datefrom):
            $months_difference = 1;
            while (strtotime('-' . $months_difference . ' month', $dateto) >= $datefrom) {
                $months_difference++;
            }

            $datediff = $months_difference;
            $res = ($datediff == 1) ? $datediff . ' month ago' : $datediff . ' months ago';

            break;
        // If difference is greater than or equal to 365
        // days, return year. This will be incorrect if
        // for example, you call the function on the 28th April
        // 2008 passing in 29th April 2007. It will return
        // 1 year ago when in actual fact (yawn!) not quite
        // a year has gone by
        case(strtotime('-1 year', $dateto) >= $datefrom):
            $year_difference = 1;
            while (strtotime('-' . $year_difference . ' year', $dateto) >= $datefrom) {
                $year_difference++;
            }

            $datediff = $year_difference;
            $res = ($datediff == 1) ? $datediff . ' year ago' : $datediff . ' years ago';
            break;
    }
    return $res;
}

function get_domn_data($url) {

    global $db;
    if (isset($url) && !empty($url)) {
        $sql = "SELECT * FROM " . PREFIX . "universities WHERE url='" . $url . "'";
        $result = $db->sql_query($sql);
        if ($db->sql_numrows($result) > 0) {
            return $db->sql_fetchrow($result);
        } else {
            return false;
        }
    } else {
        return false;
    }
}

function is_user_exit($user_id) {
    global $db;
    $sql = "SELECT id FROM " . PREFIX . "module_users
                  WHERE id = '" . $user_id . "' AND status = '1' LIMIT 1";
    $result = $db->sql_query($sql);
    if ($db->sql_numrows($result) > 0) {
        return true;
    } else {
        return false;
    }
}

function open_flash_chart_object_str($width, $height, $url, $use_swfobject = true, $base = '') {

    //
    // return the HTML as a string
    //
            return _ofc($width, $height, $url, $use_swfobject, $base);
}

function open_flash_chart_object($width, $height, $url, $use_swfobject = true, $base = '') {
    //
    // stream the HTML into the page
    //
            echo _ofc($width, $height, $url, $use_swfobject, $base);
}

function _ofc($width, $height, $url, $use_swfobject, $base) {
    //
    // I think we may use swfobject for all browsers,
    // not JUST for IE...
    //
            //$ie = strstr(getenv('HTTP_USER_AGENT'), 'MSIE');
    //
            // escape the & and stuff:
    //
            $url = urlencode($url);

    //
    // output buffer
    //
            $out = array();

    //
    // check for http or https:
    //
            if (isset($_SERVER['HTTPS'])) {
        if (strtoupper($_SERVER['HTTPS']) == 'ON') {
            $protocol = 'https';
        } else {
            $protocol = 'http';
        }
    } else {
        $protocol = 'http';
    }

    //
    // if there are more than one charts on the
    // page, give each a different ID
    //
            global $open_flash_chart_seqno;
    $obj_id = 'chart';
    $div_name = 'flashcontent';

    //$out[] = '<script type="text/javascript" src="'. $base .'js/ofc.js"></script>';

    if (!isset($open_flash_chart_seqno)) {
        $open_flash_chart_seqno = 1;
        $out[] = '<script type="text/javascript" src="' . WEBSITE . PATH_JS . 'chart-js/swfobject.js"></script>';
    } else {
        $open_flash_chart_seqno++;
        $obj_id .= '_' . $open_flash_chart_seqno;
        $div_name .= '_' . $open_flash_chart_seqno;
    }

    if ($use_swfobject) {
        // Using library for auto-enabling Flash object on IE, disabled-Javascript proof
        $out[] = '<div id="' . $div_name . '"></div>';
        $out[] = '<script type="text/javascript">';
        $out[] = 'var so = new SWFObject("' . $base . 'open-flash-chart.swf", "' . $obj_id . '", "' . $width . '", "' . $height . '", "9", "#FFFFFF");';
        //$out[] = 'so.addVariable("width", "' . $width . '");';
        //$out[] = 'so.addVariable("height", "' . $height . '");';
        $out[] = 'so.addVariable("data", "' . $url . '");';
        $out[] = 'so.addParam("allowScriptAccess", "sameDomain");';
        $out[] = 'so.write("' . $div_name . '");';
        $out[] = '</script>';
        $out[] = '<noscript>';
    }

    $out[] = '<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="' . $protocol . '://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=8,0,0,0" ';
    $out[] = 'width="' . $width . '" height="' . $height . '" id="ie_' . $obj_id . '" align="middle">';
    $out[] = '<param name="allowScriptAccess" value="sameDomain" />';
    $out[] = '<param name="movie" value="' . $base . 'open-flash-chart.swf?width=' . $width . '&height=' . $height . '&data=' . $url . '" />';
    $out[] = '<param name="quality" value="high" />';
    $out[] = '<param name="bgcolor" value="#FFFFFF" />';
    $out[] = '<embed src="' . $base . 'open-flash-chart.swf?data=' . $url . '" quality="high" bgcolor="#FFFFFF" width="' . $width . '" height="' . $height . '" name="' . $obj_id . '" align="middle" allowScriptAccess="sameDomain" ';
    $out[] = 'type="application/x-shockwave-flash" pluginspage="' . $protocol . '://www.macromedia.com/go/getflashplayer" id="' . $obj_id . '"/>';
    $out[] = '</object>';

    if ($use_swfobject) {
        $out[] = '</noscript>';
    }

    return implode("\n", $out);
}

/* * **********************************************
 * functions for generate random password length of 6.
  /********************************************* */

function Random_Password($length) {
    srand(date("s"));
    $possible_charactors = "abcdefghijklmnopqrstuvwxyz1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    $string = "";
    while (strlen($string) < $length) {
        $string .= substr($possible_charactors, rand() % (strlen($possible_charactors)), 1);
    }
    return($string);
}

function _prefix($table) {
    global $prefix;
    return $prefix . $table;
}

/*
 * function for creating options with Array
 */

function options($options, $selected = NULL) {
    $select = '';
    $option = '<option value="">---------------Select-------------------</option>';
    foreach ($options as $key => $value) {
        if ($selected != null) {
            $select = $selected == $key ? 'selected' : '';
        }
        $option .= '<option value="' . $key . '" ' . $select . '>' . $value . '</option>';
    }
    return $option;
}

/*
 * function for creating options with Array
 */

function options1($options, $selected = NULL) {
    $select = '';
    $option = '<option value="">---------------Select-------------------</option>';
    foreach ($options as $key => $value) {
        if ($selected != null) {
            $select = $selected == $key ? 'selected' : '';
        }
        $option .= '<option value="' . $key . '" ' . $select . '>' . $value . '</option>';
    }
    $option .= '<option value="0">Create New</option>';
    return $option;
}

// function for Service type drop down on home page
function optionshome($options, $selected = NULL) {
    $select = '';
    $option = '<option value="">--- Select Service type ---</option>';
    foreach ($options as $key => $value) {
        if ($selected != null) {
            $select = $selected == $key ? 'selected' : '';
        }
        $option .= '<option value="' . $key . '" ' . $select . '>' . $value . '</option>';
    }
    return $option;
}

// function for facility type drop down on home page
function optionshome1($options, $selected = NULL) {
    $select = '';
    $option = '<option value="">--- Select Services type ---</option>';
    foreach ($options as $key => $value) {
        if ($selected != null) {
            $select = $selected == $key ? 'selected' : '';
        }
        $option .= '<option value="' . $key . '" ' . $select . '>' . $value . '</option>';
    }
    return $option;
}

// function for facility type multiple selectbox
function optionsService($options, $selected = NULL) {
    $select = '';
    $option = '';
    foreach ($options as $key => $value) {
        if ($selected != null) {
            $select = $selected == $key ? 'selected' : '';
        }
        $option .= '<option value="' . $key . '" ' . $select . '>' . $value . '</option>';
    }
    return $option;
}

/*
 * function for city listing
 */

function cityList($selected = NULL) {
    $cityList = array();
    $sql_query = "select id, title from " . _prefix('cities') . " Where status = 1 AND deleted=0 ORDER BY title ASC";
    $res = mysqli_query($db->db_connect_id,$sql_query);
    while ($records = mysqli_fetch_assoc($res)) {
        $cityList[$records['id']] = $records['title'];
    }
//    if ($selected == NULL) {
//        $selected = 229;
//    }
    return options($cityList, $selected);
}

/*
 * function for suburb listing
 */

function suburbList($city = null, $selected = NULL) {
    if ($city != NULL) {
        $suburbList = array();
        $sql_query = "select id, title from " . _prefix('suburbs') . " where city_id='$city' AND status=1 AND deleted=0 ORDER BY title ASC";
        $res = mysqli_query($db->db_connect_id,$sql_query);
        while ($records = mysqli_fetch_assoc($res)) {
            $suburbList[$records['id']] = $records['title'];
        }
    } else {
        $suburbList = '';
    }
//    if ($selected == NULL) {
//        $selected = 229;
//    }
    return options1($suburbList, $selected);
}

/*
 * function for suburb listing
 */

function restCareList($suburb, $selected = NULL) {
    if ($selected != NULL) {
        $restCareList = array();
        $sql_query = "select id, title from " . _prefix('rest_cares') . " where suburb_id='$suburb' AND status=1 AND deleted=0 ORDER BY title ASC";
        $res = mysqli_query($db->db_connect_id,$sql_query);
        while ($records = mysqli_fetch_assoc($res)) {
            $restCareList[$records['id']] = $records['title'];
        }
    } else {
        $restCareList = '';
    }

    return options($restCareList, $selected);
}

/*
 * function for country listing
 */

function countryList($selected = NULL) {
    $countryList = array();
    $sql_query = "select id, name from " . _prefix('countries') . " ORDER BY name ASC";
    $res = mysqli_query($db->db_connect_id,$sql_query);
    while ($records = mysqli_fetch_assoc($res)) {
        $countryList[$records['id']] = $records['name'];
    }
    if ($selected == NULL) {
        $selected = 229;
    }
    return options($countryList, $selected);
}

function countryLists($selected = NULL) {
    $countryList = array();
    $sql_query = "select id, name from " . _prefix('countries') . " ORDER BY name ASC";
    $res = mysqli_query($db->db_connect_id,$sql_query);
    while ($records = mysqli_fetch_assoc($res)) {
        $countryList[$records['id']] = $records['name'];
    }
    return options($countryList, $selected);
}

/*
 * function for State Listing
 */

function stateList($country = null, $selected = null) {
    if ($country == null) {
        $country = 229;
    }
    $sql_query = "select id, name from " . _prefix('states') . " WHERE country_id ='$country' ORDER BY name ASC";
    $res = mysqli_query($db->db_connect_id,$sql_query);
    while ($records = mysqli_fetch_assoc($res)) {
        $stateList[$records['id']] = $records['name'];
    }
    return options($stateList, $selected);
}

/*
 * Get the logitude and latitude on the basis of zip code
 */

function getLnt($zipcode) {


//    $zipcode = "92604";
    $url = "http://maps.googleapis.com/maps/api/geocode/json?address=" . $zipcode . "&sensor=false";
    $details = file_get_contents($url);
    $result = json_decode($details, true);
    return $result['results'][0]['geometry']['location'];

//    echo "Latitude :" . $lat;
//    echo '<br>';
//    echo "Longitude :" . $lng;
//
//    $url = "http://maps.googleapis.com/maps/api/geocode/json?address=
//        " . urlencode($zip) . "&sensor=false";
//    $result_string = file_get_contents($url);
//    $result = json_decode($result_string, true);
//    $result1[] = $result['results'][0];
//    $result2[] = $result1[0]['geometry'];
//    $result3[] = $result2[0]['location'];
//    return $result3[0];
//    $val = getLnt('90001');
//    echo "Latitude: " . $val['lat'] . "<br>";
//    echo "Longitude: " . $val['lng'] . "<br>";
}

/*
 * Earned Points Setting
 */

function earnedPoints($type) {
    $data = '';
    $getPoints = "Select * from  " . _prefix("point_earning_setting") . " where type= '$type' && deleted = 0 && status = 1";
    $res = mysqli_query($db->db_connect_id,$getPoints);
    if (mysqli_num_rows($res) > 0) {
        $data = mysqli_fetch_assoc($res);
    }
    return $data;
}

function getPagination($count) {
    $paginationCount = floor($count / PAGE_PER_NO);
    $paginationModCount = $count % PAGE_PER_NO;
    if (!empty($paginationModCount)) {
        $paginationCount++;
    }
    return $paginationCount;
}

function getSearchPagination($count) {
    $paginationCount = floor($count / SEARCH_PAGE_PER_NO);
    $paginationModCount = $count % SEARCH_PAGE_PER_NO;
    if (!empty($paginationModCount)) {
        $paginationCount++;
    }
    return $paginationCount;
}

function get_pagination_link($paginationCount, $module, $data = '', $colspan = '') {
    $content = '';
    if ($paginationCount > 1) {
        $content .= '<tr><td colspan="' . $colspan . '">';
        $content .='<ul class="tsc_pagination tsc_paginationC tsc_paginationC01">
                    <li class="first link" id="first">
                        <a  href="javascript:void(0)" onclick="changePagination(\'0\',\'first\',\'' . $module . '\',\'' . $data . '\')">First</a>
                    </li>';
        for ($i = 0; $i < $paginationCount; $i++) {
            $content .='<li id="' . $i . '_no" class="link">
                            <a  href="javascript:void(0)" onclick="changePagination(\'' . $i . '\',\'' . $i . '_no\',\'' . $module . '\',\'' . $data . '\')">' . ($i + 1) . '
                          </a>
                    </li>';
        }
        $content .='<li class="last link" id="last">
                         <a href="javascript:void(0)" onclick="changePagination(\'' . ($paginationCount - 1) . '\',\'last\',\'' . $module . '\',\'' . $data . '\')">Last</a>
                    </li>
                    <li class="flash"></li>
                </ul>';
        $content .= '</td></tr>';
    }
    return $content;
}

//background-color:#FF0000;
function get_sp_pagination_link($paginationCount, $module, $data = '', $colspan = '', $selectedCount = 0) {
    $content = '';
    if ($paginationCount > 1) {
        $content .= '<tr><td colspan="' . $colspan . '">';
        $content .='<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center"><ul class="pagination pagination_nav">';
        if ($selectedCount == 0) {
            $content .='<li class="first link" id="first">
                        <a  href="javascript:void(0)" class="not-click">«</a>
                    </li>';
        } else {
            $content .='<li class="first link" id="first" data="' . $paginationCount . '">
                        <a  href="javascript:void(0)" onclick="changePagination(\'0\',\'first\',\'' . $module . '\',\'' . $data . '\')">«</a>
                    </li>';
        }
        for ($i = 0; $i < $paginationCount; $i++) {
            if ($i == $selectedCount) {
                $content .='<li  id="' . $i . '_no" class="link">
                            <a class="current-navi"  href="javascript:void(0)" >' . ($i + 1) . '
                          </a>
                    </li>';
            } else {
                $content .='<li id="' . $i . '_no" class="link">
                            <a  href="javascript:void(0)" onclick="changePagination(\'' . $i . '\',\'' . $i . '_no\',\'' . $module . '\',\'' . $data . '\')">' . ($i + 1) . '
                          </a>
                    </li>';
            }
        }
        if ($selectedCount == ($paginationCount - 1)) {
            $content .='<li class="last link" id="last">
                         <a href="javascript:void(0)" class="not-click">»</a>
                    </li>
                    <li class="flash"></li>
                </ul></div>';
        } else {
            $content .='<li class="last link" id="last">
                         <a href="javascript:void(0)" onclick="changePagination(\'' . ($paginationCount - 1) . '\',\'last\',\'' . $module . '\',\'' . $data . '\')">»</a>
                    </li>
                    <li class="flash"></li>
                </ul></div>';
        }

        $content .= '</td></tr>';
    }
    ?>

    <script>
        // $('#image').removeClass('required');

    </script>
    <?php
    return $content;
}

function get_search_pagination_link($paginationCount, $module, $data = '') {
    $content = '';
    if ($paginationCount > 0) {
        $content .='<ul class="pagination pagination_nav"><li class="page">Page 1 of ' . $paginationCount . '</li>
                    <li class="first link" id="first">
                        <a  href="javascript:void(0)" onclick="changePagination(\'0\',\'first\',\'' . $module . '\',\'' . $data . '\')">«</a>
                    </li>';
        for ($i = 0; $i < $paginationCount; $i++) {
            $content .='<li id="' . $i . '_no" class="link">
                            <a  href="javascript:void(0)" onclick="changePagination(\'' . $i . '\',\'' . $i . '_no\',\'' . $module . '\',\'' . $data . '\')">' . ($i + 1) . '
                          </a>
                    </li>';
        }
        $content .='<li class="last link" id="last">
                         <a href="javascript:void(0)" onclick="changePagination(\'' . ($paginationCount - 1) . '\',\'last\',\'' . $module . '\',\'' . $data . '\')">»</a>
                    </li>
                    <li class="flash"></li>
                </ul>';
    }
    ?>

    <script>
        // $('#image').removeClass('required');

    </script>
    <?php
    return $content;
}

/*
 * Get the subject and message of the email with the reference of the alias
 */

function emailTemplate($alias) {
    global $db;
    $sql_query = "SELECT subject, description FROM  " . _prefix("email_templates") . " where alias='$alias'";
    $res = $db->sql_query($sql_query);
    $records = $db->sql_fetchrowset($res);
    if (count($records)) {
        foreach ($records as $record) {

            $subject = $record['subject'];
            $description = $record['description'];
        }
    }
    $email_fields = array();
    $email_fields['subject'] = $subject;
    $email_fields['description'] = $description;

    return $email_fields;
}

/*
 * To Generate Client/salesperson unique id
 */

function randomId() {
    return strtotime(date('Ymdhis'));
}

/*
 *  Admin sends mail for verifying user
 */

function VerifyUser($id) {
    global $db;
    $mysql_query = "SELECT email, name, password FROM " . _prefix("users") . " WHERE id= '$id'";
    $res_mysql_query = $db->sql_query($mysql_query);
    $data = $db->sql_fetchrow($res_mysql_query);
    $emailTemplate = emailTemplate('send_password');
    if ($emailTemplate['subject'] != '') {
        $name = $data['name'];
        $to = $data['email'];
        $password = Random_Password(9);
        $updatePassword = "UPDATE " . _prefix("users") . " SET password ='$password' WHERE id= '$id'";
        $db->sql_query($updatePassword);
        $message = str_replace(array('{email}', '{name}', '{password}'), array($to, $name, $password), $emailTemplate['description']);
        $sendMail = sendmail($to, $emailTemplate['subject'], $message);
        if ($sendMail == true) {
            $unique_id = randomId();
            $updateSendMail = "UPDATE " . _prefix("users") . " SET validate = '1', unique_id = '$unique_id' WHERE id = '$id'";
            $db->sql_query($updateSendMail);
            $qoute_id = uniqueId(9);
            $updateQuoteMail = "UPDATE " . _prefix("quotes") . " SET validate = '1', unique_id = '$qoute_id' WHERE client_id = '$id'";
            $db->sql_query($updateQuoteMail);
            return true;
        } else {
            $quote_delete = "UPDATE " . _prefix("quotes") . " SET deleted = '1' WHERE client_id= '$id'";
            $db->sql_query($quote_delete);
            $user_delete = "UPDATE " . _prefix("users") . " SET validate = '3' WHERE id= '$id'";
            $db->sql_query($user_delete);
            return false;
        }
    }
}

function uniqueId($length) {
    global $db;
    srand(date("s"));
    $possible_charactors = "1234567890123456789012345678901234567890";
    $string = "";
    while (strlen($string) < $length) {
        $string .= substr($possible_charactors, rand() % (strlen($possible_charactors)), 1);
    }
    $sql_query = "SELECT * FROM " . _prefix("quotes") . " WHERE unique_id = '$string'";
    $res = $db->sql_query($sql_query);
    $num = $db->sql_numrows($res);
    if ($num > 0) {
        uniqueId($length);
    } else {

        return($string);
    }
}

function facebook_login() {
    global $db;
    $facebook = new Facebook(array(
        'appId' => APPID, // Facebook App ID
        'secret' => APP_SECRET_KEY, // Facebook App Secret
        'cookie' => false,
    ));
    $user = $facebook->getUser();

    if ($user) {
        try {
            $user_profile = $facebook->api('/me');
        } catch (FacebookApiException $e) {
            error_log($e);
            $user = null;
        }
        if (!empty($user_profile)) {
            # User info ok? Let's print it (Here we will be adding the login and registering routines)
            //print_r($user_profile);exit;
            $facebook_id = $user_profile['id'];
            $email = $user_profile['email'];
            $fname = $user_profile['first_name'];
            $lname = $user_profile['last_name'];
            $username = $fname . ' ' . $lname;
            // Check whether the facebook id exist or not
            $check_user = "SELECT id, first_name, last_name, email, user_type, validate FROM " . _prefix('users') . " WHERE facebook_id = '$facebook_id'";
            $res = $db->sql_query($check_user);
            $num = $db->sql_numrows($res);
            if ($num > 0) {
                // if facebook Id exists then set the Session
                $row = $db->sql_fetchrow($res);
                // if validate is 1 then redirect to registration page else to dashboard
                if ($row['validate'] == 1) {
                    $url = HOME_PATH . 'register';
                } else {
                    if ($row['user_type'] == 1) {
                        $_SESSION['username'] = $row['first_name'] . " " . $row['last_name'];
                        $_SESSION['userEmail'] = $row['email'];
                        $_SESSION['userId'] = $row['id'];
                        $_SESSION['id'] = $row['id'];
                        $_SESSION['SupUserName'] = $row['first_name'];
                    } else {
                        $_SESSION['csUserFullName'] = $row['first_name'] . " " . $row['last_name'];
                        $_SESSION['csEmail'] = $row['email'];
                        $_SESSION['csUserName'] = $row['first_name'];
                        $_SESSION['csId'] = $row['id'];
                    }
                    // if validation 0 exists then set the Session
//                    $_SESSION['userId'] = $row['id'];
//                    $_SESSION['name'] = $row['name'];
//                    $_SESSION['userType'] = $row['user_type'];
                    $url = HOME_PATH;
                }
                redirect_to($url);
            } else {
                // If the user is new and not rregistered
                $userData = array(
                    'first_name' => $fname,
                    'last_name' => $lname,
                    'facebook_id' => $facebook_id,
                    'email' => $email,
                    'validate' => 2,
                    'user_type' => 0,
                    'password' => Random_Password(6)
                );

                // Insert the details of user
                $insert_users = $db->insert(_prefix('users'), $userData);
                if ($insert_users) {
                    // Check the record with reference to facebook id
                    $check_user = "SELECT id, user_type,first_name, last_name, validate, email FROM " . _prefix('users') . " WHERE facebook_id = '$facebook_id'";
                    $res = $db->sql_query($check_user);
                    $row = $db->sql_fetchrow($res);
                    // if validate is 1 then redirect to registration page
                    if ($row['validate'] == 1) {
                        $url = HOME_PATH . 'user/register/' . md5($row['id']);
                    }
                    if ($row['user_type'] == 1) {
                        $_SESSION['username'] = $row['first_name'] . " " . $row['last_name'];
                        $_SESSION['userEmail'] = $row['email'];
                        $_SESSION['userId'] = $row['id'];
                        $_SESSION['id'] = $row['id'];
                        $_SESSION['SupUserName'] = $row['first_name'];
                    } else {
                        $_SESSION['csUserFullName'] = $row['first_name'] . " " . $row['last_name'];
                        $_SESSION['csEmail'] = $row['email'];
                        $_SESSION['csUserName'] = $row['first_name'];
                        $_SESSION['csId'] = $row['id'];
                    }

                    $url = HOME_PATH;
                    redirect_to($url);
                } else {
                    // if the user data not inserted rediect to login page
                    $url = HOME_PATH . 'login';
                    redirect_to($url);
                }
            }
        } else {
            # For testing purposes, if there was an error, let's kill the script
            die("There was an error.");
        }
    } else {
        # There's no active session, let's generate one
        $login_url = $facebook->getLoginUrl(array('scope' => 'email'));
        redirect_to($login_url);
    }
}

function get_dashboard_pagination($paginationCount, $id, $module, $submodule = null) {
    if ($submodule != null) {
        $submodule = "/$submodule";
    } else {
        $submodule = '';
    }
    ?>
    <ul class="pagination">
        <div class="prev"><a href="<?php echo HOME_PATH . $module . $submodule ?>"><i class="fa fa-angle-double-left"></i></a></div>
        <?php
        for ($i = 0; $i < $paginationCount; $i++) {
            $linkName = $i + 1;
            $page = $id + 1;
            $active = $linkName == ($page) ? 'active' : '';
            ?>
            <li class="<?php echo $active ?>"><a href="<?php echo HOME_PATH . $module . $submodule ?>/<?php echo $i == 0 ? '' : $i; ?>"><?php echo $linkName; ?></a></li>
            <?php
        }
        ?>
        <div class="next"><a href="<?php echo HOME_PATH . $module . $submodule ?>/<?php echo $paginationCount - 1 ?>"><i class="fa fa-angle-double-right"></i></a></div>
    </ul>
    <?php
    die();
}

function getAllAttachment($id) {
    global $db;
    $getattachment = "SELECT * FROM " . _prefix('attachment') . " WHERE message_id = '{$id}'";
    $resattachement = $db->sql_query($getattachment);
    $count = $db->sql_numrows($resattachement);
    $dataAttachment = $db->sql_fetchrowset($resattachement);
    if ($count > 0) {
        foreach ($dataAttachment as $attachment) {
            if (isset($attachment['filename']) && $attachment['filename'] != null && file_exists(ADMIN_PATH . 'files/message/' . $attachment['filename'])) {
                ?>
                <a href="<?php echo HOME_PATH_URL ?>download.php?type=message&file=<?php echo $attachment['filename']; ?>">Attachment</a><br><br>
                <?php
            }
        }
    }
}

function get_file_name($fileName) {

    $find1 = '_';
    $find2 = '.';
    $pos1 = strpos($fileName, $find1);
    $pos2 = strripos($fileName, $find2);
    $startIndex = min($pos1, $pos2);
    $length = abs($pos1 - $pos2);
    $file_name = substr($fileName, $startIndex + 1, $length - 1);
    return $file_name;
}

/*
 * function to sanitize image name
 */

function imgNameSanitize($string, $lenght) {
    $dotpos = strrpos($string, '.', -1);
    $ext = substr($string, $dotpos);
    // $ext= end(explode('.',$string));
    //  echo $dotpos;
    if ($dotpos > $lenght) {
        $name_ext = substr($string, 0, $lenght);
        $name_rep = str_replace(" ", "_", $name_ext);
        return $name_rep . $ext;
    } else {
        $name_rep = str_replace(" ", "_", $string);
        return $name_rep;
    }
}

/*
 * function for
 */

function spaceList($selected = NULL) {
    $spaceList = array();
    $sql_query = "select id, title from " . _prefix('space_prices') . " where status=1 AND 	deleted=0 ORDER BY id ASC";
    $res = mysqli_query($db->db_connect_id,$sql_query);
    while ($records = mysqli_fetch_assoc($res)) {
        $spaceList[$records['id']] = $records['title'];
    }

    return options($spaceList, $selected);
}

/*
 * function for suplier Listing
 */

function supplierList($selected = NULL) {
    $supplierList = array();
    $sql_query = "select id, first_name, last_name from " . _prefix('users') . " where user_type=1 AND deleted=0 AND validate = 2 AND status = 1 ORDER BY first_name ASC";
    $res = mysqli_query($db->db_connect_id,$sql_query);
    while ($records = mysqli_fetch_assoc($res)) {
        $supplierList[$records['id']] = $records['first_name'] . ' ' . $records['last_name'];
    }

    return options($supplierList, $selected);
}

function productList($supplier = null, $selected = NULL) {
    if ($supplier != NULL) {
        $productList = array();
        if ($selected != NULL) {
            $condition = "AND prd.id='$selected'";
        } else {
            $condition = '';
        }
        $sql_query = "select prd.id, prd.title from " . _prefix('products') . " AS prd LEFT JOIN " . _prefix('users') . " AS usr ON prd.supplier_id= usr.id WHERE prd.supplier_id= '$supplier' $condition ORDER BY prd.title ASC";
        $res = mysqli_query($db->db_connect_id,$sql_query);
        while ($records = mysqli_fetch_assoc($res)) {
            $productList[$records['id']] = $records['title'];
        }
    } else {
        $productList = '';
    }
//    if ($selected == NULL) {
//        $selected = 229;
//    }
    return options($productList, $selected);
}

/*
 * function for service category or facility type Listing
 */

function serviceCategory($selected = NULL) {
    $serviceCategory = array();
    $sql_query = "select id, name from " . _prefix('services') . " where deleted=0 AND status = 1 ORDER BY name ASC";
    $res = mysqli_query($db->db_connect_id,$sql_query);
    while ($records = mysqli_fetch_assoc($res)) {
        $serviceCategory[$records['id']] = $records['name'];
    }

    return optionshome($serviceCategory, $selected);
}

function serviceCategory1($selected = NULL) {
    $serviceCategory = array();
    $sql_query = "select id, name from " . _prefix('services') . " where deleted=0 AND status = 1 ORDER BY name ASC";
    $res = mysqli_query($db->db_connect_id,$sql_query);
    while ($records = mysqli_fetch_assoc($res)) {
        $serviceCategory[$records['id']] = $records['name'];
    }

    return optionshome1($serviceCategory, $selected);
}

/*
 *  Get slider images for home page
 */

function getSliderImage() {
    global $db;
    $getslider = "SELECT * FROM " . _prefix('sliders') . " WHERE status=1 && deleted= 0";
    $resslider = $db->sql_query($getslider);
    $count = $db->sql_numrows($resslider);
    $dataslider = $db->sql_fetchrowset($resslider);

    if ($count > 0) {
        $sliderImage = '';
        $counter = 1;
        foreach ($dataslider as $slider) {

            if (isset($slider['image']) && $slider['image'] != null && file_exists(DOCUMENT_PATH . 'admin/files/slider/' . $slider['image'])) {
                // pr(MAIN_PATH . '/files/slider/' . $slider['image']);
                $flag = ($counter == 1) ? ' active' : '';
                $sliderImage .= '<div class="item' . $flag . '" ><img class="img_height_ctn" src="' . MAIN_PATH . '/files/slider/' . $slider['image'] . '"  alt="slide Image"> </div> ';
            }
            $counter++;
        }
    }
    return $sliderImage;
}

/*
 *  Get logos  for home page
 */

function getLogoImage() {
    global $db;
    $getlogos = "SELECT * FROM " . _prefix('company_logos') . " WHERE status=1 && deleted= 0";
    $reslogos = $db->sql_query($getlogos);
    $count = $db->sql_numrows($resslider);
    $datalogos = $db->sql_fetchrowset($reslogos);
    //print_r($datalogos);
    if ($count > 0) {
        $logoImage = '';
        $counter = 1;
        foreach ($datalogos as $logo) {

            if (isset($logo['image']) && $logo['image'] != null && file_exists(DOCUMENT_PATH . 'admin/files/company_logo/' . $logo['image'])) {
//                    <img class="img-responsive" src="images/tmp/AIA.png" alt="" style="display:inline;">
                $logoImage .= '<a href=' . $logo['url'] . ' target="_blank"><img class="img-responsive" title="' . $logo['title'] . '" src="' . MAIN_PATH . '/files/company_logo/' . $logo['image'] . '"  alt="logo Image"></a>';
            }
            $counter++;
        }
    }
    return $logoImage;
}

/*
 *  Get artical  for home page listing
 */

function getArticles() {
    global $db;
    $getArticles = "SELECT * FROM " . _prefix('pages') . " WHERE status=1 && deleted= 0 ORDER BY id DESC LIMIT 8";
    $resArticles = $db->sql_query($getArticles);
    $count = $db->sql_numrows($resArticles);
    $dataArticles = $db->sql_fetchrowset($resArticles);

    if ($count > 0) {
        $logoImage = '';
        foreach ($dataArticles as $article) {
            $description = (strlen($article['page_title']) > 40) ? substr($article['page_title'], 0, 40) . '...' : $article['page_title'];
            $logoImage .= '<li><i class="fa fa-angle-right"></i> &nbsp;<a href="' . HOME_PATH . 'articles/view?id=' . md5($article['id']) . '">' . $description . '</a></li>';
        }
    }
    return $logoImage;
}

/*
 *  Get artical  for home page listing
 */

function getBlogs() {
    global $db;
    $getBlogs = "SELECT * FROM " . _prefix('blogs') . " WHERE status=1 && deleted= 0 ORDER BY id DESC LIMIT 8";
    $resBlogs = $db->sql_query($getBlogs);
    $count = $db->sql_numrows($resBlogs);
    $dataBlogs = $db->sql_fetchrowset($resBlogs);
    $blogs = '';
    if ($count > 0) {
        foreach ($dataBlogs as $blog) {
            $description = (strlen($blog['title']) > 40) ? substr($blog['title'], 0, 40) . '...' : $blog['title'];
            $blogs .= '<li><i class="fa fa-angle-right"></i><a href="' . HOME_PATH . 'forum/view?id=' . md5($blog['id']) . '">' . $description . '</a></li>';
        }
    }
    return $blogs;
}

/*
 *  Get artical  for home page listing
 */

//function getMetaKeys($module) {
//    die;
//    $model = '';
//    switch ($module) {
//        case '':
//            $query = "SELECT * FROM " . _prefix('home_keys') . " WHERE status=1 && deleted= 0 ";
//            break;
//        case 'product':
//            $query = "SELECT * FROM " . _prefix('products') . " WHERE status=1 && deleted= 0 ";
//            break;
//        case 'articles':
//            $query = "SELECT * FROM " . _prefix('pages') . " WHERE status=1 && deleted= 0 && md5(id) ='" . $_GET['id'] . "'";
//            break;
//        default:
//            $query = '';
//    }
//
//
//    global $db;
//    if ($query != '') {
//        $keys = $db->sql_query($query);
//        $count = $db->sql_numrows($keys);
//        $keyInfo = $db->sql_fetchrowset($keys);
//        $string = '';
//        if ($count > 0) {
//            foreach ($keyInfo as $data) {
//                $string .= '<meta name="keywords" content="' . $data['keyword'] . '"><meta name="description" content="' . $data['description'] . '">';
//            }
//        }
//        return $string;
//    } else {
//        return '';
//    }
//}

/*
 *  Get overall Rating
 * Argument: supplier id
 */

function overAllRating($uid) {
    global $db;
    $condition = " where fdbk.sp_id='" . $uid . "' ";
    $query = "SELECT fdbk.* , pds.title  FROM " . _prefix("feedbacks") . " AS fdbk   "
            . "LEFT JOIN " . _prefix("products") . " AS pds ON  fdbk.product_id= pds.id "
            . "$condition ORDER BY fdbk.created DESC";
    $res = $db->sql_query($query);
    $data = $db->sql_fetchrowset($res);

    $queryRating = "SELECT  `quality_of_care`, `caring_staff`, `responsive_management`, `trips_outdoor_activities`,"
            . " `indoor_entertainment`, `social_atmosphere`, `enjoyable_food`, `overall_rating` FROM " . _prefix("rating_score");
    $resRating = $db->sql_query($queryRating);
    $dataRating = $db->sql_fetchrowset($resRating);
    $QualityOfServiceWgt = $dataRating[0]['quality_of_care'];
    $cStaffWgt = $dataRating[0]['caring_staff'];
    $resManWgt = $dataRating[0]['responsive_management'];
    $tripWgt = $dataRating[0]['trips_outdoor_activities'];
    $indoorEntWgt = $dataRating[0]['indoor_entertainment'];
    $socialWgt = $dataRating[0]['social_atmosphere'];
    $foodWgt = $dataRating[0]['enjoyable_food'];
    $overAllRatingWgt = $dataRating[0]['overall_rating'];
    $TotalRating = 0;
    $weightSumation = $QualityOfServiceWgt + $cStaffWgt + $resManWgt + $tripWgt + $indoorEntWgt + $socialWgt + $foodWgt + $overAllRatingWgt;
    foreach ($data as $key => $record) {
        $TotalRating = $TotalRating + (($QualityOfServiceWgt * $record['quality_of_care']) + ($cStaffWgt * $record['caring_staff']) + ($resManWgt * $record['responsive_management']) + ($tripWgt * $record['trips_outdoor_activities']) + ($indoorEntWgt * $record['indoor_entertainment']) + ($socialWgt * $record['social_atmosphere']) + ($foodWgt * $record['enjoyable_food']) + ($overAllRatingWgt * $record['overall_rating']));
    }
    $Rating = ($TotalRating / ($weightSumation * count($data)));
    $overallRating = round($Rating);
    switch ($overallRating) {
        case 1:
            $all_Rating = '<i class="fa fa-star star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>';
            break;
        case 2:
            $all_Rating = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>';
            break;
        case 3:
            $all_Rating = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>';
            break;
        case 4:
            $all_Rating = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star"></i>';
            break;
        case 5:
            $all_Rating = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i>';
            break;
        default:
            $all_Rating = '<i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>';
            break;
    }
    return $all_Rating;
    die;
}

function serviceCategoryName($selected = NULL) {
    $serviceCategory = array();
    $sql_query = "select  name from " . _prefix('services') . " where deleted=0 AND status = 1 AND id='$selected' ORDER BY name ASC";
    $res = mysqli_query($db->db_connect_id,$sql_query);
    $records = mysqli_fetch_assoc($res);


    return $records['name'];
}

/*function upgradeproduct_logo($pro_id)
{
     global $db;
    $upgrade_pro="select id, supplier_id from ad_products where id={$pro_id}";
    $res = $db->sql_query($upgrade_pro);
    $up_prod = $db->sql_fetchrowset($res);
    $supp_id=$up_prod['supplier_id'];
    $up_plan="select id,plan_id from ad_pro_plans where pro_id={$pro_id}  AND supplier_id={$supp_id}";
    $up_query=$db->sql_query($up_plan);
    $up_product_list = $db->sql_fetchrowset($up_query);
    $up_product_list['id'];
    $pro_plan_id=$up_product_list['plan_id'];
    if($pro_plan_id>1)
    {
        $pl_id="select logo from ad_products";
        $pl_query= $db->sql_query($pl_id);
        $up_prod = $db->sql_fetchrowset($pl_query);
        $up_prod_logo=$up_prod['logo'];
    }

    return  $up_prod_logo;
}*/
?>