<?php
include("../application_top.php");
$started_at = microtime(true);
ini_set('display_errors', 1); 
error_reporting(E_ALL);


echo "<pre>";
$prev_month = date_create("last day of -1 month")->format('m'); 
(int)$prev_month;
$curr_year = (new DateTime)->format("Y");
(int)$curr_year;

$city_id_list = "SELECT DISTINCT city_id FROM ad_products WHERE deleted = 0 AND city_id > 0 AND city_id IS NOT NULL AND certification_service_type = 'Retirement Village'";
$queryresult = mysqli_query($db->db_connect_id,$city_id_list);


while($cityid = mysqli_fetch_array($queryresult)){   
    $city_id = $cityid['city_id']; 
        
    $query_rating = "SELECT plan_id, quick_url, product_id, avg(overall_rating) as final_rating FROM `ad_feedbacks` AS fdbk 
    LEFT JOIN ad_products AS pds ON fdbk.product_id= pds.id 
    LEFT JOIN ad_pro_plans AS plan ON fdbk.product_id= plan.pro_id
    WHERE fdbk.status = 1 AND fdbk.deleted = 0 AND YEAR(fdbk.created) = $curr_year AND MONTH(fdbk.created) = $prev_month AND current_plan = 1 
    AND city_id = $city_id
    GROUP BY product_id order by final_rating DESC, plan_id Desc";
    echo "<br>";

    $query_rating_res = $db->sql_query($query_rating);
    $count = $db->sql_numrows($query_rating_res);
    $data_rating = $db->sql_fetchrowset($query_rating_res);
    $i = 1;
    
    if($count){
        foreach ($data_rating as $key => $records) {                     
            echo $id_rv = $records['product_id'];  
            echo "<br>";
            echo $url_rv = "<a target='_blank' href='http://agedadvisor.nz/".$records['quick_url']."'>Link<a>";         
            echo "<br>";    
            echo $final_rating_rv = $records['final_rating'];        
            echo "<br>";   
            echo "Rank : " . $i;
            echo "<br>"; 
            echo "<br>";         
            echo $updateQuery = "UPDATE `ad_products` SET `rank_rating_city` = ".$i." WHERE `id` = {$records["product_id"]}";			
            echo "<br>"; 
            mysqli_query($db->db_connect_id,$updateQuery);    
            $data_id_rank_rating_nation = $records["product_id"]."-".$prev_month."-".$curr_year; 
            echo $updateQuery = "UPDATE `ad_data_history` SET `rank_rating_city` = ".$i." WHERE `id` = '$data_id_rank_rating_nation'";			    
            echo "<br>"; 
            mysqli_query($db->db_connect_id,$updateQuery);    
            $i++;
        }
    }
    
    echo "<br>";
    $query_reviews = "SELECT count(fdbk.id) as totalrev, product_id, quick_url FROM ad_feedbacks AS fdbk
    LEFT JOIN ad_products AS pds ON fdbk.product_id= pds.id 
    LEFT JOIN ad_pro_plans AS plan ON fdbk.product_id= plan.pro_id    
    WHERE fdbk.status = 1 AND fdbk.deleted = 0 AND YEAR(fdbk.created) = $curr_year AND MONTH(fdbk.created) = $prev_month AND current_plan = 1
    AND city_id = $city_id
    GROUP BY product_id ORDER BY totalrev DESC";
    echo "<br>";
    
    $query_reviews_res = $db->sql_query($query_reviews);
    $count = $db->sql_numrows($query_reviews_res);
    $data_reviews = $db->sql_fetchrowset($query_reviews_res);
    $j = 1;
    
    if($count){
        foreach ($data_reviews as $key => $records) {                     
            echo $id_rv = $records['product_id'];  
            echo "<br>";
            echo $url_rv = "<a target='_blank' href='http://agedadvisor.nz/".$records['quick_url']."'>Link<a>";         
            echo "<br>";    
            echo $final_reviews_rv = $records['totalrev'];        
            echo "<br>";   
            echo "Reviews Rank : " . $j;
            echo "<br>"; 
            echo "<br>";         
            echo $updateQuery = "UPDATE `ad_products` SET `rank_reviews_city` = ".$j." WHERE `id` = {$records["product_id"]}";			
            echo "<br>"; 
            mysqli_query($db->db_connect_id,$updateQuery);    
            $j++;
        }   
    }
    
    echo "<br>";
    $query_views = "SELECT SUM(visited_before) AS visited_before, prod_id, plan_id, quick_url FROM ad_analytics ana 
    LEFT JOIN ad_products AS pds ON ana.prod_id = pds.id     
    LEFT JOIN ad_pro_plans AS plan ON ana.prod_id = plan.pro_id    
    WHERE pds.deleted = 0 AND current_plan = 1 AND YEAR(ana.date) = $curr_year AND MONTH(ana.date) = $prev_month  AND city_id = $city_id  
    GROUP by prod_id ORDER BY visited_before DESC, plan_id DESC";   

    $query_views_res = $db->sql_query($query_views);
    $count = $db->sql_numrows($query_views_res);
    $data_views = $db->sql_fetchrowset($query_views_res);
    $k = 1;
    
    if($count){
        foreach ($data_views as $key => $records) {                     
            echo $id_rv = $records['prod_id'];  
            echo "<br>";
            echo $url_rv = "<a target='_blank' href='http://agedadvisor.nz/".$records['quick_url']."'>Link<a>";         
            echo "<br>";    
            echo $final_views_rv = $records['visited_before'];        
            echo "<br>";   
            echo "Rank : " . $k;
            echo "<br>"; 
            echo "<br>";         
            echo $updateQuery = "UPDATE `ad_products` SET `rank_page_view_city` = ".$k." WHERE `id` = {$records["prod_id"]}";			
            echo "<br>"; 
            mysqli_query($db->db_connect_id,$updateQuery);        
            $k++;
        }  
    }
    

    echo $qryTotalRatingCity = "SELECT plan_id, product_id, SUM(overall_rating) as total_rating_city FROM `ad_feedbacks` AS fdbk 
            LEFT JOIN ad_products AS pds ON fdbk.product_id = pds.id 
            LEFT JOIN ad_pro_plans AS plan ON fdbk.product_id = plan.pro_id 
            WHERE  fdbk.status = 1 AND fdbk.deleted = 0 AND current_plan = 1 AND  city_id ='$city_id' 
            GROUP BY product_id order by total_rating_city DESC, plan_id Desc";
    $resultTotalRatingCity = $db->sql_query($qryTotalRatingCity);    
    $rowTotalRatingCity = $db->sql_fetchrowset($resultTotalRatingCity);
    $m=1;
    
    
    foreach ($rowTotalRatingCity as $key => $records) {                     
        echo $id_rv = $records['product_id'];  
        echo "Rank : " . $m;
        echo "<br>"; 
        echo "total_rating_city : " . $records['total_rating_city']; 
        echo "<br>";         
        echo $updateQuery = "UPDATE `ad_products` SET `rank_total_rating_city` = ".$m." WHERE `id` = {$records["product_id"]}";			
        echo "<br>"; 
        mysqli_query($db->db_connect_id,$updateQuery);        
        $m++;
    }      
    
    $qryTotalReview_city = " SELECT count(fdbk.id) as totalrev_city, product_id FROM ad_feedbacks AS fdbk 
            LEFT JOIN ad_products AS pds ON fdbk.product_id = pds.id 
            LEFT JOIN ad_pro_plans AS plan ON fdbk.product_id = plan.pro_id 
            WHERE  fdbk.status = 1 AND fdbk.deleted = 0 AND current_plan = 1 AND city_id = '$city_id' 
            GROUP BY product_id ORDER BY totalrev_city DESC,plan_id DESC";
    $resultTotalReview_city=$db->sql_query($qryTotalReview_city);    
    $rowTotalReview_city=$db->sql_fetchrowset($resultTotalReview_city);
    $n=1;
    
    foreach ($rowTotalReview_city as $key => $records) {                     
        echo $id_trv = $records['product_id'];  
        echo "Rank : " . $n;
        echo "<br>"; 
        echo "<br>";         
        echo $updateQuery = "UPDATE `ad_products` SET `rank_total_reviews_city` = ".$n." WHERE `id` = {$records["product_id"]}";			
        echo "<br>"; 
        mysqli_query($db->db_connect_id,$updateQuery);        
        $n++;
    }  
    
}    

echo 'Cool, that only took ' . (microtime(true) - $started_at) . ' seconds!';