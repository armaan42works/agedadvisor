<?php
include("../application_top.php");
ini_set('display_errors', 1); 
global $db;
?>
<h1>.</h1>
<h1>AGED CARE LIST</h1>
<h2>FORMULA USED :  </h2>
<P>facility_ratio = num_revs/num_beds</p>
<P>rankingValue = avg_rating-( ( 1 -  facility_ratio - 0.5 ) / 0.95 )</P>
<h1>.</h1>

<?php
    $query_ranking = "SELECT prd.id, prd.quick_url, extra.no_of_room, extra.no_of_beds, (select MAX(fd.created) as max_date from ad_feedbacks fd where fd.product_id = prd.id and fd.status=1 and fd.deleted=0)as max_created    "            
            . " FROM " . _prefix("products") . " AS prd "            
            . " LEFT JOIN " . _prefix("extra_facility") . " AS extra ON extra.product_id=prd.id"
            . " WHERE certification_service_type = 'Aged Care'";

    $ranking_res = $db->sql_query($query_ranking);
    $count = $db->sql_numrows($ranking_res);
    $data = $db->sql_fetchrowset($ranking_res);   
    
    
    foreach ($data as $key => $record) {
        
        $id = $record['id'];
        $url = $record['quick_url'];
        $num_revs = overAllRatingProductCount($record['id']);
        $num_beds = $record['no_of_beds'];
        $num_rooms = $record['no_of_room'];
        //$num_beds = ($num_beds > 0) ? $num_beds : $num_rooms;
        $avg_rating = AvgRating($record['id']);       
           
        if( $num_beds >0 && $num_revs > 2){
            
            $data[$key]['num_revs'] = $num_revs;
            $data[$key]['avg_rating'] = $avg_rating;
            $data[$key]['facility_ratio'] = $facility_ratio = $num_revs/$num_beds;
            
            $datetime = new DateTime($record['max_created']);                      
            $now = new DateTime();       
            $interval = $datetime->diff($now);            
            $yrs = $interval->y;                                                                    
            
            if($yrs<1){
                $rankingValue = $avg_rating-((1-$facility_ratio-0.5)/0.95);
            }elseif($yrs > 1 && $yrs <2){
                $rankingValue = $avg_rating-((1-$facility_ratio-0.5)/0.95);
                $rankingValue = $rankingValue*.975;
            }else{
                $rankingValue = $avg_rating-((1-$facility_ratio-0.5)/0.95);
                $rankingValue = $rankingValue*.95;
            } 
            
            //$rankingValue = $avg_rating-(0.05/(1-$facility_ratio-0.5));
            $data[$key]['rank_score'] = $rankingValue;
           
            
        }else{  
            $data[$key]['num_revs'] = $num_revs;
            $data[$key]['avg_rating'] = $avg_rating;            
            $data[$key]['rank_score'] = "-99999999";            
        }
    }
 
    usort($data, function ($item1, $item2) {
        if ($item1['rank_score'] == $item2['rank_score']) return 0;
        return $item1['rank_score'] > $item2['rank_score'] ? -1 : 1;
    });
    
    $i=1;
    foreach ($data as $key => $record) {
        if($record['rank_score'] == "-99999999"){
            $data[$key]['ranking'] = NULL;
        }else{
            $data[$key]['ranking'] = $i;
            $i++;
        }                         
    }
    $j = 0;
    echo '<pre>';
    
    foreach ($data as $key => $record) {        
       $updateQuery = "UPDATE `ad_products` SET `rank` = {$record["ranking"]} WHERE `id` = {$record["id"]}";			
       mysqli_query($db->db_connect_id,$updateQuery);
       if($j<100){
            echo "<br>";
            echo "Facility ID : ".$record['id'] ;
            echo "<br>";
            echo "Facility URL : https://www.agedadvisor.nz/".$record['quick_url'];
            echo "<br>";
            echo "No of reviews : ".$record['num_revs'];
            echo "<br>";
            echo "No of beds : ".$record['no_of_beds'];
            echo "<br>";           
            echo "Avg Rating : ".$record['avg_rating'];
            echo "<br>";
            echo "Facility Ratio : ".$record['facility_ratio'];
            echo "<br>";
            echo "Formula Score : ". $record['rank_score'];
            echo "<br>";
            echo "Ranking : ".$record['ranking'];
            echo "<br>";
            echo "<br>";
            $j++;
       }
    }
    
    
    
    ?>

<h1>.</h1>
<h1>RETIREMENT VILLAGES LIST</h1>
<h2>FORMULA USED :  </h2>
<P>facility_ratio = num_revs/num_units</p>
<P>rankingValue = avg_rating -( ( 1 -  facility_ratio - 0.5 ) / 0.95 )</P> 
    
 <?php   
    
    
    $query_ranking = "SELECT prd.id, prd.quick_url, extra.no_of_room, extra.no_of_beds, (select MAX(fd.created) as max_date from ad_feedbacks fd where fd.product_id = prd.id and fd.status=1 and fd.deleted=0)as max_created  "            
            . " FROM " . _prefix("products") . " AS prd "            
            . " LEFT JOIN " . _prefix("extra_facility") . " AS extra ON extra.product_id=prd.id"
            . " WHERE certification_service_type = 'Retirement Village'";

    $ranking_res = $db->sql_query($query_ranking);
    $count = $db->sql_numrows($ranking_res);
    $data = $db->sql_fetchrowset($ranking_res);   
    
    foreach ($data as $key => $record) {        
        $id = $record['id'];
        $url = $record['quick_url'];
        $num_revs = overAllRatingProductCount($record['id']);
        $num_beds = $record['no_of_room'];        
        $avg_rating = AvgRating($record['id']);       
        
        if( $num_beds >0 && $num_revs > 2){            
            $data[$key]['num_revs'] = $num_revs;
            $data[$key]['avg_rating'] = $avg_rating;
            $data[$key]['facility_ratio'] = $facility_ratio = $num_revs/$num_beds;
            $datetime = new DateTime($record['max_created']);                      
            $now = new DateTime();       
            $interval = $datetime->diff($now);            
            $yrs = $interval->y;                                                                    
            
            if($yrs<1){
                $rankingValue = $avg_rating-((1-$facility_ratio-0.5)/0.95);
            }elseif($yrs > 1 && $yrs <2){
                $rankingValue = $avg_rating-((1-$facility_ratio-0.5)/0.95);
                $rankingValue = $rankingValue*.975;
            }else{
                $rankingValue = $avg_rating-((1-$facility_ratio-0.5)/0.95);
                $rankingValue = $rankingValue*.95;
            }
            //$rankingValue = $avg_rating-((1-$facility_ratio-0.5)/0.95);
            $data[$key]['rank_score'] = $rankingValue;                       
        }else{  
            $data[$key]['num_revs'] = $num_revs;
            $data[$key]['avg_rating'] = $avg_rating;
            $rankingValue = "-99999999";
            $data[$key]['rank_score'] = $rankingValue;            
        }
    }
    
    usort($data, function ($item1, $item2) {
        if ($item1['rank_score'] == $item2['rank_score']) return 0;
        return $item1['rank_score'] > $item2['rank_score'] ? -1 : 1;
    });
    
    $i=1;
    foreach ($data as $key => $record) {
        if($record['rank_score'] == "-99999999"){
            $data[$key]['ranking'] = NULL;
        }else{
            $data[$key]['ranking'] = $i;
            $i++;
        }                         
    }    
    $j=0;
    echo '<pre>'; //print_r($data);    
    foreach ($data as $key => $record) {
       $updateQuery = "UPDATE `ad_products` SET `rank` = {$record["ranking"]} WHERE `id` = {$record["id"]}";			
       mysqli_query($db->db_connect_id,$updateQuery);
       if($j<100){
            echo "<br>";
            echo "Facility ID : ".$record['id'] ;
            echo "<br>";
            echo "Facility URL : https://www.agedadvisor.nz/".$record['quick_url'];
            echo "<br>";
            echo "No of reviews : ".$record['num_revs'];
            echo "<br>";           
            echo "No of Units : ".$record['no_of_room'];
            echo "<br>";
            echo "Avg Rating : ".$record['avg_rating'];
            echo "<br>";
            echo "Facility Ratio : ".$record['facility_ratio'];
            echo "<br>";
            echo "Formula Score : ". $record['rank_score'];
            echo "<br>";
            echo "Ranking : ".$record['ranking'];
            echo "<br>";
            echo "<br>";
            $j++;
       }
 
    }
    
 die();   
    
    
    
    
    
    
    
    
    
    
    
    
    
    

$SelectQuery ="SELECT `id`,`title`,`latitude`,`longitude`,`address`,`address_suburb`,`address_city`,`zip` FROM `ad_products` WHERE `longitude` = 0 AND `deleted` = 0 LIMIT 9";

$SelectQueryResult = mysqli_query($db->db_connect_id,$SelectQuery);
$ij = 1;

while($info = mysqli_fetch_array($SelectQueryResult)){
    
	$address = $info["address"].'+'.$info["address_city"].'+'.$info["zip"].'+'."New+Zealand";
	$prepAddr = str_replace(' ','+',$address);
	$prepAddr = str_replace('  ','+',$prepAddr);
	$prepAddr = str_replace('++','+',$prepAddr);
	if(isset($info["latitude"]) && !empty($info["latitude"])){
		echo $info["latitude"];
		echo "<br>";

	}else{
		
		$geocode=file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.$prepAddr.'&sensor=false');
		
		//https://maps.googleapis.com/maps/api/geocode/json?address='.$prepAddr.'&key=AIzaSyCP0cad8aWAHQBU3v4OgYr-T2Q-Mwu4sGc
		echo $prepAddr;
		echo "<br>";
		$output= json_decode($geocode);
		$latitude = $output->results[0]->geometry->location->lat;
		$longitude = $output->results[0]->geometry->location->lng;

		if(isset($latitude) && !empty($latitude)){
			//$updateLatLongQuery = "UPDATE `ad_products` SET `longitude` = ".$longitude." , `latitude` = ".$latitude." WHERE `id` = {$info["id"]} AND ` IS NULL";
			
			//mysqli_query($db->db_connect_id,$updateLatLongQuery);

			echo "latitude longitude UPDATE".$ij;
			$ij++;
			echo "<br>";
		}
	}
}
echo "OK";
?>
