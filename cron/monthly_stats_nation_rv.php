<?php
include("../application_top.php");
$started_at = microtime(true);
ini_set('display_errors', 1); 
error_reporting(E_ALL);

echo "<pre>";
$prev_month = date_create("last day of -1 month")->format('m'); 
(int)$prev_month;
$curr_year = (new DateTime)->format("Y");
(int)$curr_year;

$query_rating = "SELECT plan_id, quick_url, product_id, avg(overall_rating) as final_rating FROM `ad_feedbacks` AS fdbk 
LEFT JOIN ad_products AS pds ON fdbk.product_id= pds.id 
LEFT JOIN ad_pro_plans AS plan ON fdbk.product_id= plan.pro_id
WHERE fdbk.status = 1 AND fdbk.deleted = 0 AND YEAR(fdbk.created) = $curr_year AND MONTH(fdbk.created) = $prev_month 
AND current_plan = 1 AND `certification_service_type` = 'Retirement Village' AND pds.deleted = 0 
GROUP BY product_id order by final_rating DESC, plan_id Desc";
echo "<br>";

$query_rating_res = $db->sql_query($query_rating);
$data_rating = $db->sql_fetchrowset($query_rating_res);
$i = 1;

foreach ($data_rating as $key => $records) {                     
    echo $id_rv = $records['product_id'];  
    echo "<br>";
    echo $url_rv = "<a target='_blank' href='http://agedadvisor.nz/".$records['quick_url']."'>Link<a>";         
    echo "<br>";    
    echo $final_rating_rv = $records['final_rating'];        
    echo "<br>";   
    echo "Rank : " . $i;
    echo "<br>"; 
    echo "<br>";         
    echo $updateQuery = "UPDATE `ad_products` SET `rank_rating_nation` = ".$i." WHERE `id` = {$records["product_id"]}";			
    echo "<br>"; 
    mysqli_query($db->db_connect_id,$updateQuery);  
    
    $data_id_rank_rating_nation = $records["product_id"]."-".$prev_month."-".$curr_year; 
    echo $updateQuery = "UPDATE `ad_data_history` SET `rank_rating_nation` = ".$i." WHERE `id` = '$data_id_rank_rating_nation'";			    
    echo "<br>"; 
    mysqli_query($db->db_connect_id,$updateQuery);    
    $i++;
}

echo $qryTotalRatingNation="SELECT plan_id, product_id, sum(overall_rating) as total_rating_nation FROM `ad_feedbacks` AS fdbk "
        . "LEFT JOIN ad_products AS pds ON fdbk.product_id= pds.id "
        . "LEFT JOIN ad_pro_plans AS plan ON fdbk.product_id= plan.pro_id "
        . "WHERE fdbk.status = 1 AND fdbk.deleted = 0 AND current_plan = 1 AND pds.deleted = 0 AND `certification_service_type` = 'Retirement Village' "
        . "GROUP BY product_id order by total_rating_nation DESC, plan_id DESC";

$resultTotalRatingNation=$db->sql_query($qryTotalRatingNation);
$countrating = $db->sql_numrows($resultTotalRatingNation);
$rowTotalRatingNation=$db->sql_fetchrowset($resultTotalRatingNation);
 
$m=1;  

foreach ($rowTotalRatingNation as $key => $records) {                     
    echo $id_rv = $records['product_id'];  
    echo "<br>";
    echo "Rank : " . $m;
    echo "<br>"; 
    echo "<br>";         
    echo $updateQuery = "UPDATE `ad_products` SET `rank_total_rating_nation` = ".$m." WHERE `id` = {$records["product_id"]}";			
    echo "<br>"; 
    mysqli_query($db->db_connect_id,$updateQuery);       
    $m++;
}
echo "<br>";
echo $qryTotalReview_nation="SELECT plan_id, count(fdbk.id) as totalrev_nation, product_id FROM ad_feedbacks AS fdbk "
        . "LEFT JOIN ad_products AS pds ON fdbk.product_id= pds.id "
        . "LEFT JOIN ad_pro_plans AS plan ON fdbk.product_id= plan.pro_id "
        . "WHERE  fdbk.status = 1 AND fdbk.deleted = 0 AND  current_plan = 1 AND pds.deleted = 0 AND `certification_service_type` = 'Retirement Village' "
        . "GROUP BY product_id ORDER BY totalrev_nation DESC,plan_id DESC";
$resultTotalReview_nation=$db->sql_query($qryTotalReview_nation);
$rowTotalReview_nation=$db->sql_fetchrowset($resultTotalReview_nation);

$n=1;  
foreach ($rowTotalReview_nation as $key => $records) {                         
    echo "Rank : " . $n;
    echo "<br>"; 
    echo "<br>";         
    echo $updateQuery = "UPDATE `ad_products` SET `rank_total_reviews_nation` = ".$n." WHERE `id` = {$records["product_id"]}";			
    echo "<br>"; 
    mysqli_query($db->db_connect_id,$updateQuery);       
    $n++;
}

echo "<br>";
echo $query_reviews = "SELECT count(fdbk.id) as totalrev, product_id, quick_url FROM ad_feedbacks AS fdbk
LEFT JOIN ad_products AS pds ON fdbk.product_id= pds.id 
LEFT JOIN ad_pro_plans AS plan ON fdbk.product_id= plan.pro_id    
WHERE fdbk.status = 1 AND fdbk.deleted = 0 AND YEAR(fdbk.created) = $curr_year AND MONTH(fdbk.created) = $prev_month 
AND current_plan = 1  AND `certification_service_type` = 'Retirement Village' AND pds.deleted = 0 
GROUP BY product_id ORDER BY totalrev DESC";
echo "<br>";
$query_reviews_res = $db->sql_query($query_reviews);
$data_reviews = $db->sql_fetchrowset($query_reviews_res);
$j = 1;
foreach ($data_reviews as $key => $records) {                     
    echo $id_rv = $records['product_id'];  
    echo "<br>";
    echo $url_rv = "<a target='_blank' href='http://agedadvisor.nz/".$records['quick_url']."'>Link<a>";         
    echo "<br>";    
    echo $final_reviews_rv = $records['totalrev'];        
    echo "<br>";   
    echo "Reviews Rank : " . $j;
    echo "<br>"; 
    echo "<br>";         
    echo $updateQuery = "UPDATE `ad_products` SET `rank_reviews_nation` = ".$j." WHERE `id` = {$records["product_id"]}";			
    echo "<br>"; 
    mysqli_query($db->db_connect_id,$updateQuery);           			        
    $j++;
}

echo "<br>";
echo $query_views = "SELECT SUM(visited_before) AS visited_before, prod_id, plan_id, quick_url FROM ad_analytics ana 
LEFT JOIN ad_products AS pds ON ana.prod_id = pds.id     
LEFT JOIN ad_pro_plans AS plan ON ana.prod_id = plan.pro_id    
WHERE pds.deleted = 0 AND current_plan = 1 AND YEAR(ana.date) = $curr_year AND MONTH(ana.date) = $prev_month  
AND `certification_service_type` = 'Retirement Village'  AND pds.deleted = 0 
GROUP by prod_id ORDER BY visited_before DESC, plan_id DESC";   

$query_views_res = $db->sql_query($query_views);
$data_views = $db->sql_fetchrowset($query_views_res);
$k = 1;
foreach ($data_views as $key => $records) {                     
    echo $id_rv = $records['prod_id'];  
    echo "<br>";
    echo $url_rv = "<a target='_blank' href='http://agedadvisor.nz/".$records['quick_url']."'>Link<a>";         
    echo "<br>";    
    echo $final_views_rv = $records['visited_before'];        
    echo "<br>";   
    echo "Rank : " . $k;
    echo "<br>"; 
    echo "<br>";         
    echo $updateQuery = "UPDATE `ad_products` SET `rank_page_view_nation` = ".$k." WHERE `id` = {$records["prod_id"]}";			
    echo "<br>"; 
    mysqli_query($db->db_connect_id,$updateQuery); 
    
    $data_id_views = $records["prod_id"]."-".$prev_month."-".$curr_year; 
    echo $updateQuery = "UPDATE `ad_data_history` SET `total_no_of_views` = ".$final_views_rv." WHERE `id` = '$data_id_views'";
    mysqli_query($db->db_connect_id,$updateQuery); 
    
    $k++;
}
 
echo $query_enquiry = "SELECT count(enq.id) AS enqTotal, prod_id, plan_id, quick_url FROM ad_enquiries enq 
LEFT JOIN ad_products AS pds ON enq.prod_id = pds.id     
LEFT JOIN ad_pro_plans AS plan ON enq.prod_id = plan.pro_id    
WHERE pds.deleted = 0 AND current_plan = 1 AND YEAR(time_of_enquiry) = $curr_year AND MONTH(time_of_enquiry) = $prev_month 
AND `certification_service_type` = 'Retirement Village'  AND pds.deleted = 0 
GROUP by prod_id ORDER BY enqTotal DESC, plan_id DESC";   

$query_enquiry_res = $db->sql_query($query_enquiry);
$data_enquiry = $db->sql_fetchrowset($query_enquiry_res);

foreach ($data_enquiry as $key => $records) {                     
    echo $id_rv = $records['prod_id'];  
    echo "<br>";
    echo $url_rv = "<a target='_blank' href='http://agedadvisor.nz/".$records['quick_url']."'>Link<a>";         
    echo "<br>";    
    echo $final_enq_rv = $records['enqTotal'];        
    echo "<br>";           
        
    $data_id_enq = $records["prod_id"]."-".$prev_month."-".$curr_year; 
    echo $updateQuery = "UPDATE `ad_data_history` SET `total_no_of_enq` = ".$final_enq_rv." WHERE `id` = '$data_id_enq'";    
    mysqli_query($db->db_connect_id,$updateQuery); 
}

echo 'Cool, that only took ' . (microtime(true) - $started_at) . ' seconds!';