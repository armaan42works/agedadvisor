<?php
include("../application_top.php");
error_reporting(E_ALL);


$prod_id = filter_input(INPUT_POST, 'facilityId');
$sup_id = filter_input(INPUT_POST, 'supplierId');
$cust_id = filter_input(INPUT_POST, 'customerId');
$nigelOnly = filter_input(INPUT_POST, 'nigelOnly');
$nigelOnly = intval($nigelOnly); 
if(!isset($prod_id)){ die(); }


$prev_month = date_create("last day of -1 month")->format('m'); 

(int)$prev_month;
$curr_year = (new DateTime)->format("Y");
(int)$curr_year;

$dateObj   = DateTime::createFromFormat('!m', $prev_month);
$monthName = $dateObj->format('F'); 

$second_last_month = date_create("last day of -2 month")->format('m'); 
$dateObj   = DateTime::createFromFormat('!m', $second_last_month);
$lastMonthName = $dateObj->format('F'); 

function ordinal($number) {
    $ends = array('th','st','nd','rd','th','th','th','th','th','th');
    if ((($number % 100) >= 11) && (($number % 100) <= 13)){
        return $number. 'th';
    }else{
        return $number. $ends[$number % 10];
    }
}
/*==============================================================================================*/

$upgraded_fac_avg_qry_rv = "SELECT AVG(rank_rating_nation) AS upgraded_avg_rank_rv FROM ad_products AS pro LEFT JOIN ad_pro_plans AS proplans ON pro.id = proplans.pro_id 
WHERE proplans.plan_id > 1 AND proplans.current_plan = 1 AND pro.deleted = 0 AND `certification_service_type` = 'Retirement Village'"; 
$upfac_res_rv = $db->sql_query($upgraded_fac_avg_qry_rv);    
$upfac_data_rv = $db->sql_fetchrowset($upfac_res_rv);

foreach ($upfac_data_rv as $key => $record) {
    $up_avg_rating_rv = ceil($record['upgraded_avg_rank_rv']);
}   

$upgraded_fac_avg_qry_ac = "SELECT AVG(rank_rating_nation) AS upgraded_avg_rank_ac FROM ad_products AS pro LEFT JOIN ad_pro_plans AS proplans ON pro.id = proplans.pro_id 
WHERE proplans.plan_id > 1 AND proplans.current_plan = 1 AND pro.deleted = 0 AND `certification_service_type` = 'Aged Care'"; 
$upfac_res_ac = $db->sql_query($upgraded_fac_avg_qry_ac);    
$upfac_data_ac = $db->sql_fetchrowset($upfac_res_ac);

foreach ($upfac_data_ac as $key => $record) {
    $up_avg_rating_ac = ceil($record['upgraded_avg_rank_ac']);
}   




$nonUpgraded_fac_avg_qry_rv = "SELECT AVG(rank_rating_nation) AS nonUpgraded_avg_rank_rv FROM ad_products AS pro LEFT JOIN ad_pro_plans AS proplans ON pro.id = proplans.pro_id 
WHERE proplans.plan_id = 1 AND proplans.current_plan = 1 AND pro.deleted = 0 AND `certification_service_type` = 'Retirement Village'"; 
$nonUpfac_res_rv = $db->sql_query($nonUpgraded_fac_avg_qry_rv);    
$nonUpfac_data_rv = $db->sql_fetchrowset($nonUpfac_res_rv);

foreach ($nonUpfac_data_rv as $key => $record) {
    $nonUp_avg_rating_rv = floor($record['nonUpgraded_avg_rank_rv']);
} 

$nonUpgraded_fac_avg_qry_ac = "SELECT AVG(rank_rating_nation) AS nonUpgraded_avg_rank_ac FROM ad_products AS pro LEFT JOIN ad_pro_plans AS proplans ON pro.id = proplans.pro_id 
WHERE proplans.plan_id = 1 AND proplans.current_plan = 1 AND pro.deleted = 0 AND `certification_service_type` = 'Aged Care'"; 
$nonUpfac_res_ac = $db->sql_query($nonUpgraded_fac_avg_qry_ac);    
$nonUpfac_data_ac = $db->sql_fetchrowset($nonUpfac_res_ac);

foreach ($nonUpfac_data_ac as $key => $record) {
    $nonUp_avg_rating_ac = floor($record['nonUpgraded_avg_rank_ac']);
}   


/*==============================================================================================*/



$upfac_enq_avg_rv = "SELECT AVG(total_no_of_enq) AS upfac_enq_avg FROM ad_data_history AS dh 
LEFT JOIN ad_pro_plans AS proplans ON dh.prod_id = proplans.pro_id 
LEFT JOIN ad_products AS prod ON dh.prod_id = prod.id
WHERE proplans.plan_id > 1 AND proplans.current_plan = 1 AND `certification_service_type` = 'Retirement Village' AND `month` = $prev_month"; 
$upfac_enq_res_rv = $db->sql_query($upfac_enq_avg_rv);    
$upfac_enq_data_rv = $db->sql_fetchrowset($upfac_enq_res_rv);

foreach ($upfac_enq_data_rv as $key => $record) {
    $upfac_enq_rv = ceil($record['upfac_enq_avg']);
}  


$nonUpfac_enq_avg_rv = "SELECT AVG(total_no_of_enq) AS nonUpfac_enq_avg FROM ad_data_history AS dh 
LEFT JOIN ad_pro_plans AS proplans ON dh.prod_id = proplans.pro_id 
LEFT JOIN ad_products AS prod ON dh.prod_id = prod.id 
WHERE proplans.plan_id = 1 AND proplans.current_plan = 1 AND `certification_service_type` = 'Retirement Village' AND `month` = $prev_month"; 
$nonUpfac_enq_res_rv = $db->sql_query($nonUpfac_enq_avg_rv);    
$nonUpfac_enq_data_rv = $db->sql_fetchrowset($nonUpfac_enq_res_rv);

foreach ($nonUpfac_enq_data_rv as $key => $record) {
    $nonUpfac_enq_rv = floor($record['nonUpfac_enq_avg']);
} 





$upfac_enq_avg_ac = "SELECT AVG(total_no_of_enq) AS upfac_enq_avg FROM ad_data_history AS dh 
LEFT JOIN ad_pro_plans AS proplans ON dh.prod_id = proplans.pro_id 
LEFT JOIN ad_products AS prod ON dh.prod_id = prod.id
WHERE proplans.plan_id > 1 AND proplans.current_plan = 1 AND `certification_service_type` = 'Aged Care' AND `month` = $prev_month"; 
$upfac_enq_res_ac = $db->sql_query($upfac_enq_avg_ac);    
$upfac_enq_data_ac = $db->sql_fetchrowset($upfac_enq_res_ac);

foreach ($upfac_enq_data_ac as $key => $record) {
    $upfac_enq_ac = ceil($record['upfac_enq_avg']);
}  



$nonUpfac_enq_avg_ac = "SELECT AVG(total_no_of_enq) AS nonUpfac_enq_avg FROM ad_data_history AS dh 
LEFT JOIN ad_pro_plans AS proplans ON dh.prod_id = proplans.pro_id 
LEFT JOIN ad_products AS prod ON dh.prod_id = prod.id 
WHERE proplans.plan_id = 1 AND proplans.current_plan = 1 AND `certification_service_type` = 'Aged Care' AND `month` = $prev_month"; 
$nonUpfac_enq_res_ac = $db->sql_query($nonUpfac_enq_avg_ac);    
$nonUpfac_enq_data_ac = $db->sql_fetchrowset($nonUpfac_enq_res_ac);

foreach ($nonUpfac_enq_data_ac as $key => $record) {
    $nonUpfac_enq_ac = floor($record['nonUpfac_enq_avg']);
} 


/*==============================================================================================*/
     

$upfac_view_avg_rv = "SELECT AVG(total_no_of_views) AS upfac_views_avg FROM ad_data_history AS dh 
LEFT JOIN ad_pro_plans AS proplans ON dh.prod_id = proplans.pro_id 
LEFT JOIN ad_products AS prod ON dh.prod_id = prod.id
WHERE proplans.plan_id > 1 AND proplans.current_plan = 1 AND `certification_service_type` = 'Retirement Village' AND `month` = $prev_month"; 
$upfac_view_res_rv = $db->sql_query($upfac_view_avg_rv);    
$upfac_view_data_rv = $db->sql_fetchrowset($upfac_view_res_rv);

foreach ($upfac_view_data_rv as $key => $record) {
    $upfac_view_rv = ceil($record['upfac_views_avg']);
}  


$nonUpfac_view_avg_rv = "SELECT AVG(total_no_of_views) AS nonUpfac_views_avg FROM ad_data_history AS dh 
LEFT JOIN ad_pro_plans AS proplans ON dh.prod_id = proplans.pro_id 
LEFT JOIN ad_products AS prod ON dh.prod_id = prod.id 
WHERE proplans.plan_id = 1 AND proplans.current_plan = 1 AND `certification_service_type` = 'Retirement Village' AND `month` = $prev_month"; 
$nonUpfac_view_res_rv = $db->sql_query($nonUpfac_view_avg_rv);    
$nonUpfac_view_data_rv = $db->sql_fetchrowset($nonUpfac_view_res_rv);

foreach ($nonUpfac_view_data_rv as $key => $record) {
    $nonUpfac_view_rv = floor($record['nonUpfac_views_avg']);
} 





$upfac_view_avg_ac = "SELECT AVG(total_no_of_views) AS upfac_view_avg FROM ad_data_history AS dh 
LEFT JOIN ad_pro_plans AS proplans ON dh.prod_id = proplans.pro_id 
LEFT JOIN ad_products AS prod ON dh.prod_id = prod.id
WHERE proplans.plan_id > 1 AND proplans.current_plan = 1 AND `certification_service_type` = 'Aged Care' AND `month` = $prev_month"; 
$upfac_view_res_ac = $db->sql_query($upfac_view_avg_ac);    
$upfac_view_data_ac = $db->sql_fetchrowset($upfac_view_res_ac);

foreach ($upfac_view_data_ac as $key => $record) {
    $upfac_view_ac = ceil($record['upfac_view_avg']);
}  


$nonUpfac_view_avg_ac = "SELECT AVG(total_no_of_views) AS nonUpfac_view_avg FROM ad_data_history AS dh 
LEFT JOIN ad_pro_plans AS proplans ON dh.prod_id = proplans.pro_id 
LEFT JOIN ad_products AS prod ON dh.prod_id = prod.id 
WHERE proplans.plan_id = 1 AND proplans.current_plan = 1 AND `certification_service_type` = 'Aged Care' AND `month` = $prev_month"; 
$nonUpfac_view_res_ac = $db->sql_query($nonUpfac_view_avg_ac);    
$nonUpfac_view_data_ac = $db->sql_fetchrowset($nonUpfac_view_res_ac);

foreach ($nonUpfac_view_data_ac as $key => $record) {
    $nonUpfac_view_ac = floor($record['nonUpfac_view_avg']);
} 



/*==============================================================================================*/
    

$query_ranking = "SELECT propla.plan_id,prd.id, prd.title, prd.supplier_id, prd.address_city, rank_rating_city, rank_rating_nation, rank_page_view_city, rank_page_view_nation, rank_reviews_city, rank_reviews_nation,rank_total_rating_city,rank_total_rating_nation,rank_total_reviews_city,rank_total_reviews_nation,certification_service_type, "
            . "(SELECT SUM(visited_before) FROM ad_analytics aa WHERE aa.prod_id = prd.id AND YEAR(date) = $curr_year AND MONTH(date) = $prev_month GROUP BY aa.prod_id) AS no_of_views, "
            . "(SELECT SUM(visited_before) FROM ad_analytics aa WHERE aa.prod_id = prd.id AND YEAR(date) = $curr_year AND MONTH(date) = $second_last_month GROUP BY aa.prod_id) AS no_of_views_last, "
            . "(SELECT COUNT(aenq.id) FROM ad_enquiries aenq WHERE aenq.prod_id = prd.id AND YEAR(time_of_enquiry) = $curr_year AND MONTH(time_of_enquiry) = $prev_month) AS no_of_enq, "
            . "(SELECT COUNT(aenq.id) FROM ad_enquiries aenq WHERE aenq.prod_id = prd.id AND YEAR(time_of_enquiry) = $curr_year AND MONTH(time_of_enquiry) = $second_last_month) AS no_of_enq_last, "
            . "(SELECT COUNT(aenq.facility_responded) FROM ad_enquiries aenq WHERE aenq.prod_id = prd.id AND YEAR(time_of_enquiry) = $curr_year AND MONTH(time_of_enquiry) = $prev_month AND facility_responded = 'Y') AS no_of_responses, "            
            . "(SELECT rank_rating_city FROM ad_data_history WHERE id = '$prod_id-$second_last_month-$curr_year') AS rank_rating_city_last, "            
            . "(SELECT rank_rating_nation FROM ad_data_history WHERE id = '$prod_id-$second_last_month-$curr_year') AS rank_rating_nation_last "   
            . " FROM " . _prefix("products") . " AS prd "
            . " LEFT JOIN ad_pro_plans as propla ON propla.pro_id = prd.id "
            . " WHERE prd.id = $prod_id AND current_plan = 1";           


$ranking_res = $db->sql_query($query_ranking);
$count = $db->sql_numrows($ranking_res);
$data = $db->sql_fetchrowset($ranking_res); 
if($count > 0){         
    $finalHtml = '';        
    $unsubscribe_url = "<a style='color:#949494; font-weight: bold;' href='https://agedadvisor.nz/unsubscribe-stats/?sp=".md5($sup_id)."' target='_blank'>unsubscribe</a>";
    $excel_link = "<a style='color: rgb(255, 255, 255); font-family: Verdana; background-color: rgb(241, 89, 34); display: inline-block; padding: 6px 12px;font-size: 14px; line-height: 1.42857; text-align: center; white-space: nowrap; vertical-align: middle; border: 1px solid transparent; border-radius: 4px;' href='https://agedadvisor.nz/monthly-facility-stats-report/?fc=".md5($prod_id)."' target='_blank'>Download Excel Report Here</a>";

    foreach ($data as $key => $record) {                  
//                echo 'plan_id : ' . 
            $plan_id = $record['plan_id'];
            echo "<br>";
            echo " id : " ; echo 
            $id = $record['id'];
            echo "<br>";
            echo " Title : " ; echo 
              $title= $record['title'];
            echo "<br>";
//                echo " no_of_enq : "  ; echo 
            
            $curr_month = $prev_month+1;
            $first_date_of_month = $curr_year.$curr_month.'01';
            

            $sqlTotalEnqu = "SELECT count(id)as enquirY FROM ad_enquiries WHERE prod_id ='$id' AND DATE(time_of_enquiry) <= $first_date_of_month";
            $resultTotalEnqu=mysqli_query($db->db_connect_id,$sqlTotalEnqu);
            $rowTotalEnqu=mysqli_fetch_array($resultTotalEnqu);

            $TotalEnqu=$rowTotalEnqu['enquirY'];


            $qryTotalView="SELECT sum(visited_before)as TotalView FROM ad_analytics WHERE prod_id ='$id' AND DATE(date) <= $first_date_of_month";
            $resultTotalView=mysqli_query($db->db_connect_id,$qryTotalView);
            $rowTotalView=mysqli_fetch_array($resultTotalView);
            //echo"<pre>";print_r($rowTotalView);die();
            $TotalView=$rowTotalView['TotalView'];



            $no_of_enq = (empty($record['no_of_enq']) || !isset($record['no_of_enq'])) ? 0 : $record['no_of_enq']; 
            $no_of_enq_last = $record['no_of_enq_last'];
            $no_of_enq_diff = $no_of_enq - $no_of_enq_last;
            $enq_compare_html = ($no_of_enq_diff > 0 ) ? '(Up '. $no_of_enq_diff . ' from '. $lastMonthName.')' : '(Down '. abs($no_of_enq_diff) . ' from '. $lastMonthName.')';
            if($no_of_enq_diff == 0){
                $enq_compare_html = '';
            }
            $total_enq_html=' - TOTAL to Date : '.$TotalEnqu;

            $no_of_responses = (empty($record['no_of_responses']) || !isset($record['no_of_responses'])) ? 0 : $record['no_of_responses'];                             
            $total_resp_html='';

            $no_of_views = (empty($record['no_of_views']) || !isset($record['no_of_views'])) ? 0 : $record['no_of_views']; 
            $no_of_views_last = $record['no_of_views_last'];
            $no_of_views_diff = $no_of_views - $no_of_views_last;
            $views_compare_html = ($no_of_views_diff > 0 ) ? '(Up '. $no_of_views_diff . ' from '. $lastMonthName.')' : '(Down '. abs($no_of_views_diff) . ' from '. $lastMonthName.')';
            if($no_of_views_diff == 0){
                $views_compare_html = '';
            }
            $total_view_html=' - TOTAL to Date : '.$TotalView;                                    
            $cert_type  = $record['certification_service_type']; 


            $rank_rating_city = (($record['rank_rating_city'] === NULL) || ($record['rank_rating_city'] < 1)) ? 'Not Ranked for '.$monthName.'' : ordinal($record['rank_rating_city']);                                
            $rank_rating_city_diff = $record['rank_rating_city'] - $record['rank_rating_city_last'];
            $rating_city_compare_html = ($rank_rating_city_diff > 0 ) ? '(Increased '. $rank_rating_city_diff .' position'. ' from '. $lastMonthName.')' : '(Dropped '. abs($rank_rating_city_diff) .' position'. ' from '. $lastMonthName.')';
            if($rank_rating_city_diff == 0){
                $rating_city_compare_html = '';
            }
//              
            $rank_rating_nation = (($record['rank_rating_nation'] === NULL) || ($record['rank_rating_nation'] < 1)) ? 'Not Ranked for '.$monthName.'' : ordinal($record['rank_rating_nation']);
            $rank_rating_nation_diff = $record['rank_rating_nation'] - $record['rank_rating_nation_last'];
            $rating_nation_compare_html = ($rank_rating_nation_diff > 0 ) ? '(Increased '. $rank_rating_nation_diff .' position'. ' from '. $lastMonthName.')' : '(Dropped '. abs($rank_rating_nation_diff).'position'. ' from '. $lastMonthName.')';
            if($rank_rating_nation_diff == 0){
                $rating_nation_compare_html = '';
            }


//              
            $rank_page_view_city = (($record['rank_page_view_city'] === NULL) || ($record['rank_page_view_city'] < 1)) ? 'Not Ranked for '.$monthName.'' : ordinal($record['rank_page_view_city']);
//                echo " rank_page_view_city : " ;  echo $rank_page_view_city ;
//                echo "<br>";
            $rank_page_view_nation = (($record['rank_page_view_nation'] === NULL) || ($record['rank_page_view_nation'] < 1)) ? 'Not Ranked for '.$monthName.'' : ordinal($record['rank_page_view_nation']);
//                echo " rank_page_view_nation : " ;  echo $rank_page_view_nation;
//                echo "<br>";
            $rank_reviews_city = (($record['rank_reviews_city'] === NULL) || ($record['rank_reviews_city'] < 1)) ? 'Not Ranked for '.$monthName.'' : ordinal($record['rank_reviews_city']);
//                echo " rank_reviews_city : " ;  echo $rank_reviews_city;
//                echo "<br>";
            $rank_reviews_nation = (($record['rank_reviews_nation'] === NULL) || ($record['rank_reviews_nation'] < 1)) ? 'Not Ranked for '.$monthName.'' : ordinal($record['rank_reviews_nation']);
//                echo " rank_reviews_nation : " ;  echo $rank_reviews_nation ;
//                echo "<br>";     
//                echo " supplier_id : " ;  echo $supplier_id = $record['supplier_id'];
//                echo "<br>";                
            $city = $record['address_city'];
//                echo "<br>";

        $Total_ratingCity_html  = (($record['rank_total_rating_city'] === NULL) || ($record['rank_total_rating_city'] < 1)) ? '' : ' ( All time rating rank : '.ordinal($record['rank_total_rating_city']).' ) ';            

        $Total_ratingNation_html= (($record['rank_total_rating_nation'] === NULL) || ($record['rank_total_rating_nation'] < 1)) ? '' : ' ( All time rating rank : '.ordinal($record['rank_total_rating_nation']).' ) ';

        $Total_reviewCity_html  = (($record['rank_total_reviews_city'] === NULL) || ($record['rank_total_reviews_city'] < 1)) ? '' : ' ( All time reviews rank : '.ordinal($record['rank_total_reviews_city']).' ) ';

        $Total_reviewNation_html= (($record['rank_total_reviews_nation'] === NULL) || ($record['rank_total_reviews_nation'] < 1)) ? '' : ' ( All time reviews rank : '.ordinal($record['rank_total_reviews_nation']).' ) ';



        if( $plan_id > 1 ){

            $upgradeNow = $upgradeavgviewHtml = $upgradeavgenqHtml = $avgrankHtml = '';                    

        }else{

            $upgradeNow = '<div style="font-family: Verdana, Geneva, sans-serif;">If you would like to upgrade, please click <a href="https://www.agedadvisor.nz/supplier/login" style="color: rgb(255, 255, 255); font-family: Verdana; background-color: rgb(241, 89, 34); display: inline-block; padding: 6px 12px; margin-bottom: 0px; font-size: 14px; line-height: 1.42857; text-align: center; white-space: nowrap; vertical-align: middle; background-image: none; border: 1px solid transparent; border-radius: 4px;"> UPGRADE NOW</a></div>';

            if(($upfac_view_rv > $no_of_views) && ($cert_type == 'Retirement Village')){

           //     $upgradeavgviewHtml = '<span style="font-size: 12px;font-style: italic;font-family: Verdana, Geneva, sans-serif;">*** Upgraded facilities receive on average '. $upfac_view_rv .' views</span>';   


            }else if(($upfac_view_ac > $no_of_views) && ($cert_type == 'Aged Care')){

            //    $upgradeavgviewHtml = '<span style="font-size: 12px;font-style: italic;font-family: Verdana, Geneva, sans-serif;">*** Upgraded facilities receive on average '. $upfac_view_ac .' views</span>';                           

            }
            else{
                $upgradeavgviewHtml = '';                        
            }



            if(($upfac_enq_rv > $no_of_enq) && ($cert_type == 'Retirement Village')){

            //    $upgradeavgenqHtml = '<span style="font-size: 12px;font-style: italic;font-family: Verdana, Geneva, sans-serif;">*** Upgraded facilities receive on average '. $upfac_enq_rv .' enquiries</span>';                        

            }else if (($upfac_enq_ac > $no_of_enq) && ($cert_type == 'Aged Care')){

            //    $upgradeavgenqHtml = '<span style="font-size: 12px;font-style: italic;font-family: Verdana, Geneva, sans-serif;">*** Upgraded facilities receive on average '. $upfac_enq_ac .' enquiries</span>';                        

            }else{
                $upgradeavgenqHtml = '';                  
            } 



            if(($up_avg_rating_rv < $nonUp_avg_rating_rv) && ($cert_type == 'Retirement Village')){

            //    $avgrankHtml = '<span style="font-size: 12px;font-style: italic;font-family: Verdana, Geneva, sans-serif;">*** Upgraded facilities have an average ranking in NZ of '. $up_avg_rating_rv .' compared to an average ranking of '. $nonUp_avg_rating_rv .' of non-upgraded</span>';                                    

            }else if (($up_avg_rating_ac < $nonUp_avg_rating_ac) && ($cert_type == 'Aged Care')) {

              //  $avgrankHtml = '<span style="font-size: 12px;font-style: italic;font-family: Verdana, Geneva, sans-serif;">*** Upgraded facilities have an average ranking in NZ of '. $up_avg_rating_ac .' compared to an average ranking of '. $nonUp_avg_rating_ac .' of non-upgraded</span>';                                    

            }else{
                $avgrankHtml = '';                        
            }

        }
        $avgrankHtml='';
        $upgradeavgviewHtml='';
        $upgradeavgenqHtml='';
        $container_start = '<div class="cont-start">';
        $facility_name = '<p><span style="font-family:Verdana"><strong> Facility Name : '.$title.'</strong></span></p>';                
        $compareText = '<p><span style="font-family:Verdana"><strong>See how you compare with others in your industry...</strong></span></p>';                
        $first_ul ='<ul class="first-ul-start">
            <li><span style="font-family:Verdana">Views : <strong>'.$no_of_views.'</strong> '.$views_compare_html.'<strong>'.$total_view_html.'</strong> </span></li>
            '.$upgradeavgviewHtml.'
            <li><span style="font-family:Verdana">Enquiries made : <strong>'.$no_of_enq.'</strong> '.$enq_compare_html.'<strong>'.$total_enq_html.'</strong> </span></li>
            '.$upgradeavgenqHtml.'
            <li><span style="font-family:Verdana">Responded to : <strong>'.$no_of_responses.'</strong> '.'<strong>'.$total_resp_html.'</strong></span></li>
        </ul>';                                
        $second_ul ='<ul class="second-ul-start">
            <li><span style="font-family:Verdana">Rating in '.$city.' : <strong>'.$rank_rating_city.'</strong>'.$rating_city_compare_html.$Total_ratingCity_html.'  </span></li>
            <li><span style="font-family:Verdana">Rating in NZ : <strong>'.$rank_rating_nation.'</strong> '.$rating_nation_compare_html.$Total_ratingNation_html.' </span></li>
            '.$avgrankHtml.'
            <li><span style="font-family:Verdana">Page Views in '.$city.' : <strong>'.$rank_page_view_city.'</strong> </span></li>
            <li><span style="font-family:Verdana">Page Views in NZ : <strong>'.$rank_page_view_nation.'</strong> </span></li>
            <li><span style="font-family:Verdana">Reviews in '.$city.' : <strong>'.$rank_reviews_city.'</strong> '.$Total_reviewCity_html.' </span></li>
            <li><span style="font-family:Verdana">Reviews in NZ : <strong>'.$rank_reviews_nation.'</strong> '.$Total_reviewNation_html.' </span></li>
        </ul>--------------------------------------------------------------------------------------------------------------------'; 
        $container_end = '</div>';                                

    }             
    $finalHtml = $container_start.$facility_name.$first_ul.$compareText.$second_ul.$finalHtml.$container_end;       

    $headers = "MIME-Version: 1.0\n";
    $headers .= "Content-type: text/html; charset=iso-8859-1\n";
    $headers .= 'From:   ';           
    $headers .= "reviews@agedadvisor.co.nz";                      
    $headers .= "\n";
    //$headers .= "Cc: reviews@agedadvisor.co.nz\r\n";            
    $fac_name = '';
    $emailTemplate = emailTemplate('facility_single_monthly_stats'); 
    if ($emailTemplate['subject'] != '') {                
        $message = str_replace(array('{message}','{fullname}','{month}','{year}','{upgradeNow}','{unsubscribe_url}'), array(trim($finalHtml), trim($fac_name), $monthName, $curr_year, $upgradeNow,$unsubscribe_url), stripslashes($emailTemplate['description']));                 
        $subject = str_replace('{month}',$monthName, $emailTemplate[ 'subject' ]);
        if($nigelOnly === 1){
            @mail( 'nigelmatthews77@gmail.com', $subject, $message, $headers );
            @mail( 'amitsingh9thjan@gmail.com', $subject, $message, $headers );
        }else{            
            if(!empty($cust_id)){
                $sqlCust = "SELECT * FROM ad_users WHERE id ='$cust_id'";
                $resultCustDet = mysqli_query($db->db_connect_id,$sqlCust);
                $rowCustRec = mysqli_fetch_array($resultCustDet);
                $fac_email = $rowCustRec['email'];
            }
            @mail( $fac_email, $subject, $message, $headers );     
        }  
           
        //@mail( 'jinandra.gupta@gmail.com', $subject, $message, $headers );   
        //@mail( $fac_email, $subject, $message, $headers ); 
        $dateTime = date('Y-m-d H:i:s');
        $datetimeSql = "UPDATE `ad_products` SET `last_monthy_report` = '$dateTime'  WHERE `id` = $prod_id"; 
        mysqli_query($db->db_connect_id,$datetimeSql); 
    }

}  
else{        
    $finalHtml = '';   
    $upgradeNow = '';
    $fac_name = '';
    $unsubscribe_url = "<a style='color:#949494; font-weight: bold;' href='https://agedadvisor.nz/unsubscribe-stats/?sp=".md5($sup_id)."' target='_blank'>unsubscribe</a>";
    $excel_link = "<a style='color: rgb(255, 255, 255); font-family: Verdana; background-color: rgb(241, 89, 34); display: inline-block; padding: 6px 12px;font-size: 14px; line-height: 1.42857; text-align: center; white-space: nowrap; vertical-align: middle; border: 1px solid transparent; border-radius: 4px;' href='https://agedadvisor.nz/monthly-facility-stats-report/?fc=".md5($prod_id)."' target='_blank'>Download Excel Report Here</a>";
    if(!empty($fac_email) && isset($fac_email) && $fac_email<> ''){
        $headers = "MIME-Version: 1.0\n";
        $headers .= "Content-type: text/html; charset=iso-8859-1\n";
        $headers .= 'From:   ';           
        $headers .= "reviews@agedadvisor.co.nz";                      
        $headers .= "\n";
        //$headers .= "Cc: reviews@agedadvisor.co.nz\r\n";            

        $emailTemplate = emailTemplate('facility_monthly_stats'); 
        if ($emailTemplate['subject'] != '') {                
            $message = str_replace(array('{message}','{fullname}','{month}','{year}','{upgradeNow}','{unsubscribe_url}'), array(trim($finalHtml), trim($fac_name), $monthName, $curr_year, $upgradeNow,$unsubscribe_url), stripslashes($emailTemplate['description']));                 
            $subject = str_replace('{month}',$monthName, $emailTemplate[ 'subject' ]);
            //@mail( 'nigelmatthews77@gmail.com', $subject, $message, $headers );                
            //@mail( 'amitsingh9thjan@gmail.com', $subject, $message, $headers );                
            //@mail( $fac_email, $subject, $message, $headers );               
        }
    }
}
    
    
