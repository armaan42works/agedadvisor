<?php
require_once("../application_top.php");
global $db;

$query = " SELECT enquirer.user_id,enquirer.prod_id,facility.title,enquirer.Supplier_id,enquirer.time_of_enquiry,";
$query.= " userdetails.first_name,userdetails.last_name,userdetails.email,userdetails.user_type,enquirer.id,enquirer.enq_specifics,enquirer.enquiry_link,enquirer.enquiry_paid_status";
$query.= " FROM " . _prefix("enquiries") . " AS enquirer";
$query.= " LEFT JOIN " . _prefix('users') ." AS userdetails ON enquirer.Supplier_id= userdetails.id";
$query.= " LEFT JOIN " . _prefix('products') ." AS facility ON enquirer.prod_id = facility.id";
$query.= " WHERE enquirer.taking_enquiries IS NULL AND enquirer.facility_followup_sent = 'n' AND userdetails.email != 'n/a' AND userdetails.email <> '' AND userdetails.user_type = 1 AND enquirer.enquiry_paid_status = 0 AND DATE(enquirer.time_of_enquiry) <= DATE_SUB(CURDATE(), INTERVAL 4 DAY)";
$query.= " GROUP BY enquirer.user_id, enquirer.prod_id";

$res = $db->sql_query($query);
$result = $db->sql_fetchrowset($res);   
$count = $db->sql_numrows($res);

    foreach ($result as $key => $record) {            
        $user_id = $record[0];               
        $prod_id = $record[1];        
        $facility = $record[2];        
        $supplier_id = $record[3]; 
        $date = $record[4];        
        $first_name = $record[5];
        $last_name = $record[6];
        $email = $record[7];       
        $id = $record[9]; 
        $enquiry_msg = $record[10]; 
        $enquiry_link = $record[11];
        $enquiry_paid_status = $record[12];        
        $enquiry_html_link = "<a style='background-color: #00ff00;border-color: #d23f0a;display: inline-block;padding: 6px 12px;margin-bottom: 0;font-size: 14px;font-weight: 400;line-height: 1.42857143;text-align: center;white-space: nowrap;vertical-align: middle;cursor: pointer;background-image: none;border: 1px solid transparent;border-radius: 4px;color: #fff;' href='".$enquiry_link."'>Click for contact details</a>";
        $fullname = $first_name . ' ' . $last_name;                        
        $random = bin2hex(openssl_random_pseudo_bytes(10));
        $random =  $random.'-'.md5($id);        
        $yes = "https://www.agedadvisor.nz/facility-followup-response/?response=YES&uniqid=".$random;
        $yes = "<a style='background-color: #f15922;border-color: #d23f0a;display: inline-block;padding: 6px 12px;margin-bottom: 0;font-size: 14px;font-weight: 400;line-height: 1.42857143;text-align: center;white-space: nowrap;vertical-align: middle;cursor: pointer;background-image: none;border: 1px solid transparent;border-radius: 4px;color: #fff;' href='".$yes."'> YES, we contacted the enquirer. </a>";
        $no = "https://www.agedadvisor.nz/facility-followup-response/?response=NO&uniqid=".$random;
        $no = "<a style='background-color: #f15922;border-color: #d23f0a;display: inline-block;padding: 6px 12px;margin-bottom: 0;font-size: 14px;font-weight: 400;line-height: 1.42857143;text-align: center;white-space: nowrap;vertical-align: middle;cursor: pointer;background-image: none;border: 1px solid transparent;border-radius: 4px;color: #fff;' href='".$no."'> NO, we have not contacted them. </a>";
        
            $headers = "MIME-Version: 1.0\n";
            $headers .= "Content-type: text/html; charset=iso-8859-1\n";
            $headers .= 'From:';           
            $headers .= "reviews@agedadvisor.co.nz";            
            $headers .= "\n";            
            $headers .= "bcc: amitsingh9thjan@gmail.com\r\n"; 
            $user_first_name = substr(trim($secresult[1]), 0, 3).'...';           
            $emailTemplate = emailTemplate('facility_followup_response');
            if ($emailTemplate['subject'] != '') {
                //echo $email;
                //echo "<br>";
                $message = str_replace(array('{fullname}', '{enquirer_name}', '{facility_name}', '{enquiry_msg}', '{date}', '{enquiry_html_link}', '{yes}', '{no}', '{id}'), array(trim($fullname), $user_first_name, $facility, $enquiry_msg, $date, $enquiry_html_link, $yes, $no, $id), stripslashes($emailTemplate['description']));
                //@mail( 'amitsingh9thjan@gmail.com', $emailTemplate[ 'subject' ], $message, $headers );
                @mail( $email, $emailTemplate[ 'subject' ], $message, $headers );
                $fields = array(
                            'facility_followup_sent' => "y"                            
                          );    
                $where = "where id=".$id."";           
                $update_result = $db->update(_prefix('enquiries'), $fields, $where);                         
            }
    }

