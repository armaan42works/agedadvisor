<?php
    /*
     * Objective  : Listing of all the files uploaded for a project
     * Filename   : file_details.php
     * Created By : Yogesh Chandra Joshi <yogesh.joshi@ilmp-tech.com> 
     * Created On : 28 August 2014
     * Modified On : 01 September 2014
     */
?>

<?php
    
    global $db;
    $id = $pathInfo['call_parts']['2'];
    $sql_query = "SELECT qu.id, qu.file_name, aq.filename, qa.filename  FROM " . _prefix("quotes") . " as qu "
            . "LEFT JOIN " . _prefix("admin_quotes") . " as aq ON qu.id = aq.quotes_id "
            . "LEFT JOIN " . _prefix("quote_attachment") . " as qa ON qa.quote_id = qu.id "
            . " where md5(qu.id) = '$id' ORDER BY qa.created, aq.created, qu.created  DESC";
    $res = $db->sql_query($sql_query);
    $num = $db->sql_numrows($res);
    $datas = $db->sql_fetchrowset($res);
    $quoteId = $datas[0]['id'];
    
    
    
     if (isset($_POST['submit']) && $submit == 'submit') {
        if ($_FILES['quoteFile']['error'] == 0) {

                $target_path = ADMIN_PATH . 'files/quote_files/';
                $date = new DateTime();
                $timestamp = $date->getTimestamp();
                $fileName = $timestamp . '_' . basename($_FILES['quoteFile']['name']);
                $target_path = $target_path . $fileName;
                move_uploaded_file($_FILES['quoteFile']["tmp_name"], $target_path);
               $_POST['filename']=$fileName;
            }
            
             $field= array(
            'filename' => $_POST['filename'],
            'quote_id'=>$quoteId
                 
                     
        );
            if($_FILES['quoteFile']['name']!=''){
                $insert_quotes = $db->insert(_prefix('quote_attachment'), $field);
            
            if ($insert_quotes) {
                // Message for insert
                $msg = common_message(1, constant('INSERT'));
                $_SESSION['msg'] = $msg;
                
               
            } 
            }
            
              $sql_query = "SELECT qu.id, qu.file_name, aq.filename, qa.filename  FROM " . _prefix("quotes") . " as qu "
            . "LEFT JOIN " . _prefix("admin_quotes") . " as aq ON qu.id = aq.quotes_id "
            . "LEFT JOIN " . _prefix("quote_attachment") . " as qa ON qa.quote_id = qu.id "
            . " where md5(qu.id) = '$id' ORDER BY qa.created, aq.created, qu.created  DESC" ;
    $res = $db->sql_query($sql_query);
    $num = $db->sql_numrows($res);
    $datas = $db->sql_fetchrowset($res);
           
     }
     
?>
<section id="body_container" class="Dashbord_main " >
    <ul class="breadcrumb">
        <li><a href="<?php echo HOME_PATH ?>user"><strong>HOME</strong></a></li>
        <li class=""><a href="<?php echo HOME_PATH ?>user/library">File library</a></li>
        <li class="last"><a href="javascript:void(0);">Details</a></li>
    </ul>
    <div class="messag_send"></div>
    <div class="message_container file_library">
        <div class="login_container chang_pass_main">
            
            <div><?php
                                if (isset($_SESSION['msg'])) {
                                    echo $_SESSION['msg'];
                                    unset($_SESSION['msg']);
                                }
                            ?></div>
            <div class="file_detail_upload">
                <form name="" id="file_details" enctype="multipart/form-data" action="" method="POST">
                    <span>Upload a document for project</span>                
                    <input name="quoteFile" type="file"  class="required quote_file"/>
                  <input name="submit" type="submit" value="submit" class="submit_btn" />
                </form>

            </div>

            <div class="file_detail_box">
                <span class="listing">List of all the uploaded documents</span>
                <?php
                    foreach ($datas as $records) {
                        foreach ($records as $val) {
                            $fileArray[] = $val;
                        }
                    }

                    $result = array_unique($fileArray);
                    

                    if (count($result) > 0) {

                        foreach ($result as $key => $value) {

                            if (isset($value) && $value != '' && file_exists(DOCUMENT_PATH . 'admin/files/quote_files/' . $value)) {
                                ?>  <div class="col">
                                    <span class=""><img src="<?php echo HOME_PATH ?>images/file_show.gif"/>  <a href="<?php echo HOME_PATH_URL ?>download.php?type=quote_files&file=<?php echo $value; ?>" class="getFile" id="<?php echo $value ?>"> <?php echo get_file_name($value); ?></a></span>

                                </div> 
                                <?php
                            }
                        }
                    }else {
                echo "<span class='norecord'>No Record found</span>";
            }
                    
                ?>
            </div>
        </div>
    </div>
</section>



<span><a href="<?php echo HOME_PATH_URL ?>download.php?file=<?php echo $dataGetAdminQuote['filename'] ?>" class="getFile" id="<?php echo $dataGetAdminQuote['filename'] ?>">Uploaded Document</a></span>

<script type="text/javascript">
    $(document).ready(function() {
        $('#file_details').validate();
    });


</script>
