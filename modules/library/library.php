<?php
    /*
     * Objective  : Listing of all the projects/quotes
     * Filename   : library.php
     * Created By : Yogesh Chandra Joshi <yogesh.joshi@ilmp-tech.com> 
     * Created On : 25 August 2014
     * Modified On : 01 September 2014
     */
?>

<?php
    global $db;
    
    $id = $_SESSION['userId'];
    $sql_query = "SELECT * FROM " . _prefix('quotes') . " WHERE client_id = '$id' ORDER BY created DESC";
    $res = $db->sql_query($sql_query);
    $num = $db->sql_numrows($res);
    $datas = $db->sql_fetchrowset($res);

?>
<section id="body_container" class="Dashbord_main " >
<ul class="breadcrumb">
 <li><a href="<?php echo HOME_PATH ?>user"><strong>HOME</strong></a></li>
 <li class="last"><a href="javascript:void(0);">File Library</a></li>
 </ul>
    <div class="messag_send"></div>
    <div class="message_container file_library">
    
        <?php
            if ($num > 0) {
                ?>
        <div class="login_container chang_pass_main">
        <?php
              
        foreach ($datas as $data) { ?>
     
           
            <div class="col">
                
                
               
                 <span class="label_text"><a href="<?php echo HOME_PATH.'user/fileDeails/'.md5($data['id']);?>" ><i class="fa fa-book fa-fw"></i>  <?php $str = $data['status'] == 1 ? 'P' : 'Q'; echo $str.$data['unique_id'];?> </a></span>
           
            
            </div> 
          <?php
        }
        ?></div>
            <?php
            }else {
                echo "<div class='norecord'>No Record found</div>";
            }
            ?>
        
    
</div>
</section>
