<?php
if (!session_id()) {
    session_start();
}

require_once("application_top.php");
global $db; 
require_once __DIR__ . '/facebook-sdk-v5/autoload.php';

$fb = new Facebook\Facebook([
  'app_id' => '1409054779407381',
  'app_secret' => 'e7dcb280e81129960a9a99d60b91c3f9',
  'default_graph_version' => 'v2.8'
  ]);


$helper = $fb->getRedirectLoginHelper();
if (isset($_GET['state'])) {   
    $helper->getPersistentDataHandler()->set('state', $_GET['state']);
}

try {
  //$accessToken = $helper->getAccessToken();
  $accessToken = $helper->getAccessToken();  
  $_SESSION['fb_access_token'] = $accessToken;
  echo "<br>";
} catch(Facebook\Exceptions\FacebookResponseException $e) {
  // When Graph returns an error
  echo 'Graph returned an error: ' . $e->getMessage();
  exit; 
} catch(Facebook\Exceptions\FacebookSDKException $e) {
  // When validation fails or other local issues
  echo 'Facebook SDK returned an error: ' . $e->getMessage();
  exit;
}
  

if (! isset($accessToken)) {
  if ($helper->getError()) {
    header('HTTP/1.0 401 Unauthorized');
    echo "Error: " . $helper->getError() . "\n";
    echo "Error Code: " . $helper->getErrorCode() . "\n";
    echo "Error Reason: " . $helper->getErrorReason() . "\n";
    echo "Error Description: " . $helper->getErrorDescription() . "\n";
  } else {
    header('HTTP/1.0 400 Bad Request');
    echo 'Bad request';
  }
  //exit;
}

try {
  // Get the \Facebook\GraphNodes\GraphUser object for the current user.
  // If you provided a 'default_access_token', the '{access-token}' is optional.
  $response = $fb->get('/me?fields=id,email,location,first_name,last_name,hometown', $accessToken);
} catch(\Facebook\Exceptions\FacebookResponseException $e) {
  // When Graph returns an error
  echo 'Graph returned an error: ' . $e->getMessage();
  exit;
} catch(\Facebook\Exceptions\FacebookSDKException $e) {
  // When validation fails or other local issues
  echo 'Facebook SDK returned an error: ' . $e->getMessage();
  exit;
}

$me = $response->getGraphUser();
if(!empty($me)){
    echo "<br>";
    $first_name = $me['first_name'];
    echo "<br>";
    $last_name = $me['last_name'];
    echo "<br>";
    $facebook_id = $me['id'];
    echo "<br>";
    $email = $me['email'];
    echo "<br>";    
    $validate = 2;
}else{
    header('Location: https://www.agedadvisor.nz/login');
}



$qry = "SELECT * FROM " . _prefix('users') . ' WHERE (validate=2 AND status=1 AND deleted= 0 AND facebook_id="' . $facebook_id . '" AND user_type=0)';
$result = $db->sql_query($qry);
$data = $db->sql_fetchrowset($result);
if ($db->sql_numrows($result) <= 0) {    
    
    $fields = array(
        'user_name' => trim($first_name),
        'facebook_id' => trim($facebook_id),
        'first_name' => trim($first_name),
        'last_name' => trim($last_name),
        'email' => (trim($email)),
        'validate' => trim($validate)                
    );
  
    $insert_result = $db->insert(_prefix('users'), $fields);
    $id = $db->last_id();
    
    $_SESSION['csUserName'] = $_SESSION['csUserFullName'] = $me['first_name'] . " " . $me['last_name'];
    $_SESSION['csEmail'] = $me['email'];    
    $_SESSION['csId'] = $id;
    $_SESSION['fbId'] = $me['id'];
          
}else{
   
    foreach ($data as $key => $record) { 
        
        $name = $record['first_name'].' '. $record['last_name'];              
        $id = $record['id'];       
        $email = $record['email'];       
        
        $_SESSION['csUserName'] = $_SESSION['csUserFullName'] = $name;
        $_SESSION['csEmail'] = $email;    
        $_SESSION['csId'] = $id;                
    }    
    
}



$user = $response->getGraphUser();
$graphNode = $response->getGraphNode();
//var_dump($me);

// Logged in
//echo '<h3>Access Token</h3>';
//echo $accessToken->getValue();


//
//// Validation (these will throw FacebookSDKException's when they fail)
//$tokenMetadata->validateAppId($config['app_id']);
//// If you know the user ID this access token belongs to, you can validate it here
////$tokenMetadata->validateUserId('123');
//$tokenMetadata->validateExpiration();

if (! $accessToken->isLongLived()) {
  // Exchanges a short-lived access token for a long-lived one
  try {
    $accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
  } catch (Facebook\Exceptions\FacebookSDKException $e) {
    echo "<p>Error getting long-lived access token: " . $e->getMessage() . "</p>\n\n";
    exit;
  }

  //echo '<h3>Long-lived</h3>';
  //var_dump($accessToken->getValue());
}



echo "</pre>";
// User is logged in with a long-lived access token.
// You can redirect them to a members-only page.
if($_SESSION['back_url'] == 'competition'){
    unset($_SESSION['back_url']);
    header('Location: https://www.agedadvisor.nz/view-competitions');
}elseif(stristr ($_SESSION['back_url'],'newmedia') == TRUE){
    $exploded = explode('-',$_SESSION['back_url']);
    $postfix =  $exploded[1];    
    unset($_SESSION['back_url']);    
    header('Location: https://www.agedadvisor.nz/new-media/'.$postfix);
}elseif($_SESSION['back_url'] == 'review'){
    header('Location: https://www.agedadvisor.nz/rating-supplier');
}else{
    unset($_SESSION['back_url']);
    header('Location: https://www.agedadvisor.nz/');
}