
<style>
span{font-family: "Neo Sans" !important;}
</style>

<div class="container">
    <div class="row articles_container">
        <div class="col-md-12 col-sm-12 col-xs-12 padding_left">
            <div class="container">
                <div class="row">
                    <?php
                    /*
                     * To change this license header, choose License Headers in Project Properties.
                     * To change this template file, choose Tools | Templates
                     * and open the template in the editor.
                     */
                    // to submit comment
                    if (isset($submit) && $submit == 'submit') {
                        if (isset($_SESSION['userId']) && !empty($_SESSION['userId'])) {
                            $userId = $_SESSION['userId'];
                        } else if (isset($_SESSION['csId']) && !empty($_SESSION['csId'])) {
                            $userId = $_SESSION['csId'];
                        }
                        if (isset($userId)) {
                            $blogId = isset($_GET[blogId]) ? $_GET['blogId'] : '';
                            $fields_array = comment();
                            $response = Validation::_initialize($fields_array, $_POST);
                            if (isset($response['valid']) && $response['valid'] > 0) {

                                $getMax = "SELECT max(order_by) as count from " . _prefix("blog_comments") . " WHERE status = 1 && deleted = 0 ";
                                $resMax = $db->sql_query($getMax);
                                $dataMax = mysqli_fetch_assoc($resMax);
                                $order = ($dataMax['count'] == NULL) ? 1 : $dataMax['count'] + 1;
                                $tmp = strip_tags($_POST["comment"], '&nbsp;');
                                $_POST['comment'] = trim($tmp, '&nbsp;') == '' ? '' : $_POST["comment"];
                                $fields = array(
                                    'blog_id' => $blog_id,
                                    'comment' => $_POST['comment'],
                                    'user_type' => 1,
                                    'commented_on' => date('Y-m-d H:i:s'),
                                    'commented_by' => $userId
                                );

                                $insert_result = $db->insert(_prefix('blog_comments'), $fields);
                                if ($insert_result) {
                                    // Message for insert
                                    $msg = common_message(1, constant('INSERT'));
                                    $_SESSION['msg'] = $msg;
                                    //ends here
                                    redirect_to(HOME_PATH . "forum/view?id=" . md5($blog_id));
                                }
                            } else {
                                $errors = '';
                                foreach ($response as $key => $message) {
                                    $error = true;
                                    $errors .= $message . "<br>";
                                }
                            }
                        } else {
                            // $msg = common_message(1, constant('LOGIN_FOR_COMMENT'));
                            $msg = 'Please login to add comment';
                            $_SESSION['msg'] = $msg;
                            redirect_to(HOME_PATH . 'login');
                        }
                    }


                    //end submit comment

                    //$id = $_REQUEST['id'];

                    $pathInfo = parse_path();                  
                    $id = $pathInfo['call_parts']['2'];  
                    global $db;
                    $getBlogs = "SELECT bg.*, CONCAT(IFNULL(user.first_name,''),' ',IFNULL(user.last_name,'')) AS author, count(cm.id) AS comment, cm.comment AS blog_comment, cm.commented_on AS blog_commented_on FROM " . _prefix("blogs") . " AS bg "
                            . "  LEFT JOIN " . _prefix("users") . " AS user ON user.id=bg.created_by "
                            . "  LEFT JOIN " . _prefix("blog_comments") . " AS cm ON cm.blog_id=bg.id "
                            . "  WHERE md5(bg.id)= '$id'"
                            . "  GROUP BY bg.id ORDER BY bg.id DESC LIMIT 1 ";
//    $getBlogs = "SELECT blog.*, concat(user.first_name,' ',user.last_name) AS posted_by FROM " . _prefix('blogs') . " AS blog LEFT JOIN " . _prefix('users') . " AS user ON blog.created_by=user.id  WHERE blog.status=1 && blog.deleted= 0 ORDER BY blog.id DESC LIMIT 8 ";
                    // $getBlogs = "SELECT * FROM " . _prefix('blogs') . " WHERE status=1 && deleted= 0 ORDER BY id DESC LIMIT 8";
                    $resBlogs = $db->sql_query($getBlogs);
                    $count = $db->sql_numrows($resBlogs);
                    $dataBlogs = $db->sql_fetchrowset($resBlogs);
                    $blogs1 = '';
                    if ($count > 0) {

					
					 foreach ($dataBlogs as $blog)
						 {
							 if(empty($blog['img_name']))
							 {
								 $blog['img_name']='default.png';
							  }
							  
							  
							
                            $blogs .= '<div class="col-md-8 col-lg-8 col-sm-12 col-xs-12 pull-left container_wrapper" >
                        <div class="posted_col" style="border:none;">
						<div class="headline">
                            <h2>' . $blog['title'] . '</h2>
							</div>
                          <a class="pull-left img_post" href="#"><img class="img-responsive" src="/admin/files/pages/image/'.$blog['img_name'].'"></a>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pdding_none">
                                <ul class="navbar-single">
                                    <li>Posted on: ' . $blog['created'] . '</li>
                                    <li>Posted by: ' . $blog['author'] . '</li>
                                    <li class="last">' . $blog['comment'] . ' Comment</li>
                                </ul>
                            </div>
                            <p class="p_text" style="padding-left:0px; padding-top:20px;">' . $blog['content'] . '</p>
                 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 navbar-single">
                 <h1 class="comment_h1">' . $blog['comment'] . ' Comment</h1>';
                            $getBlogscomment = "SELECT blog.*, concat(user.first_name,' ',user.last_name) AS posted_by FROM " . _prefix('blog_comments') . " AS blog LEFT JOIN " . _prefix('users') . " AS user ON blog.commented_by=user.id  WHERE md5(blog.blog_id)='$id' && blog.status= 1 && blog.deleted= 0 ORDER BY blog.id DESC LIMIT 8 ";
                            $resBlogscomment = $db->sql_query($getBlogscomment);
                            $count = $db->sql_numrows($resBlogscomment);
                            $Blogcomments = $db->sql_fetchrowset($resBlogscomment);
                            foreach ($Blogcomments as $blogcomment) {
                                $blogs .= '

                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-3 text-center comment_img"><img class="img-responsive" src="' . HOME_PATH . 'images/comment_img.jpg" alt="" style="display:inherit"></div>
                                <div class="col-lg-10 col-md-10 col-sm-10 col-xs-9 padding_left comment_box">
                                    <h3 class="hedding_h3">' . $blogcomment['posted_by'] . '</h3>
                                    <span class="sub_title">' . $blogcomment['commented_on'] . '</span>
                                    <p class="comment_p">' . $blogcomment['comment'] . ' </p>
                                </div>
                            ';
                            }

                            $blogs .= '</div><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <h4 class="comment_h4">Leave a Comment</h4>
                                <form class="form-group" action="" method="post" id="blog_comment_reply">
                                <input type="hidden" name="blog_id" value="' . $blog['id'] . '">
                                    <textarea name="comment" class="form-control post_mail required" placeholder="Post your comment..."></textarea>
                                     <input type="text" tabindex="11"  name="captcha" placeholder="Security Code" id="captcha-text" class="form-control input_fild">
                                  <div id="captcha-wrap" class="col-md-12 padding_none">
                                <img src="' . HOME_PATH . 'admin/captcha/captcha.php" alt="" id="captcha"/>
                                <img src="' . HOME_PATH . 'images/captcha_refresh.gif" alt="refresh captcha" id="refresh-captcha" />
                            </div>
                            <button type="submit" name="submit" value="submit" class="btn btn-danger inner_btn comment_btn pull-right">SUBMIT</button>
                                </form>
                            </div>
                        </div>
                    </div>';
                        }
                        echo $blogs;
                    }
					/*********************************end for blog view**********************/
					
					
                       /* foreach ($dataBlogs as $blog) {
                            $blogs .= '<div class="col-md-9 col-lg-9 col-sm-12 col-xs-12 pull-left container_wrapper" style="margin-top:45px;">
                        <div class="posted_col" style="border:none;">
                            <h2 class="col_h2" style="padding-left:0px; padding-bottom:15px;">' . $blog['title'] . '</h2>
<!---                           <a class="pull-left img_post" href="#"><img class="img-responsive" src="' . HOME_PATH . 'images/community_single_img.jpg"></a>---->
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pdding_none">
                                <ul class="navbar-single">
                                    <li>Posted on: ' . $blog['created'] . '</li>
                                    <li>dBy: ' . $blog['author'] . '</li>
                                    <li class="last">' . $blog['comment'] . ' Comment</li>
                                </ul>
                            </div>
                            <p class="p_text" style="padding-left:0px; padding-top:20px;">' . $blog['content'] . '</p>
                 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 navbar-single">
                 <h1 class="comment_h1">' . $blog['comment'] . ' Comment</h1>';
                            $getBlogscomment = "SELECT blog.*, concat(user.first_name,' ',user.last_name) AS posted_by FROM " . _prefix('blog_comments') . " AS blog LEFT JOIN " . _prefix('users') . " AS user ON blog.commented_by=user.id  WHERE md5(blog.blog_id)='$id' && blog.status= 1 && blog.deleted= 0 ORDER BY blog.id DESC LIMIT 8 ";
                            $resBlogscomment = $db->sql_query($getBlogscomment);
                            $count = $db->sql_numrows($resBlogscomment);
                            $Blogcomments = $db->sql_fetchrowset($resBlogscomment);
                            foreach ($Blogcomments as $blogcomment) {
                                $blogs .= '

                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-3 text-center comment_img"><img class="img-responsive" src="' . HOME_PATH . 'images/comment_img.jpg" alt="" style="display:inherit"></div>
                                <div class="col-lg-10 col-md-10 col-sm-10 col-xs-9 padding_left comment_box">
                                    <h3 class="hedding_h3">' . $blogcomment['posted_by'] . '</h3>
                                    <span class="sub_title">' . $blogcomment['commented_on'] . '</span>
                                    <p class="comment_p">' . $blogcomment['comment'] . ' </p>
                                </div>
                            ';
                            }

                            $blogs .= '</div><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <h4 class="comment_h4">Leave a Comment</h4>
                                <form class="form-group" action="" method="post" id="blog_comment_reply">
                                <input type="hidden" name="blog_id" value="' . $blog['id'] . '">
                                    <textarea name="comment" class="form-control post_mail required" placeholder="Post your comment..."></textarea>
                                     <input type="text" tabindex="11"  name="captcha" placeholder="Security Code" id="captcha-text" class="form-control input_fild">
                                  <div id="captcha-wrap" class="col-md-12 padding_none">
                                <img src="' . HOME_PATH . 'admin/captcha/captcha.php" alt="" id="captcha"/>
                                <img src="' . HOME_PATH . 'images/captcha_refresh.gif" alt="refresh captcha" id="refresh-captcha" />
                            </div>
                            <button type="submit" name="submit" value="submit" class="btn btn-danger inner_btn comment_btn pull-right">SUBMIT</button>
                                </form>
                            </div>
                        </div>
                    </div>';
                        }
                        echo $blogs;
                    }*/
                    ?>
                    <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12 pull-right padding_right article_innerbg">
                        <h2 class="hedding_tow">New Articles:</h2>
                        <ul class="list-unstyled right_nav article_nav">
                            <?php echo getArticles(); ?>
                            <div class="viewall"><a href="<?php echo HOME_PATH; ?>articles">View All</a></div>
                        </ul>
                        <h2 class="hedding_tow">Community<br>
                            Questions &amp; Answers</h2>
                        <ul class="list-unstyled right_nav article_nav">
                            <?php echo getBlogs(); ?>
                            <div class="viewall"><a href="<?php echo HOME_PATH; ?>forum">View All</a></div>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--MAIN BODY CODE ENDS-->
<script>
    //$('#blog_comment_reply').validate();
    var path = "<?php echo HOME_PATH; ?>admin/";
    $('#refresh-captcha').on('click', function() {
        $('#captcha-text').val("");
        $('#captcha').attr("src", path + "captcha/captcha.php?rnd=" + Math.random());
    });
    $('#blog_comment_reply').validate({
        rules: {
            captcha: {
                required: true,
                remote:
                        {
                            url: path + 'captcha/checkCaptcha.php',
                            type: "post",
                            data:
                                    {
                                        code: function()
                                        {
                                            return $('#captcha-text').val();
                                        }
                                    }
                        }
            }
        },
        messages: {
            captcha: {
                required: "Please enter the verifcation code.",
                remote: "Verication code incorrect, please try again."
            },
        }
    });
</script>
