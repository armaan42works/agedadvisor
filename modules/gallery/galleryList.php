    <script>
    $(document).ready(function() {


        setTimeout(function() {
            $("#success").hide('slow');
            $('#error').hide('slow');
        }, 5000);

        var page = 0;
        var li = '0_no';
        var data = '&id';
        var fancy = function(e) {
            $('.videoPlayer').fancybox(); 

        }
        changePagination(page, li, 'gallery', data, fancy);

        setTimeout(function() {
            
            $('input[class^="galley_image"]').on('click',function(){
           
            var img_id = $(this).attr('value');

            datastring='img_id='+img_id;

            var url='/modules/gallery/ajax_update_gallery.php';

            $.ajax({
                type:"POST",
                url: url,
                data:datastring,
                success:function(result){

               

                   $("#img_msg").removeClass( "hide").addClass( "alert alert-success");
                    $("#msg").html("Image selected as first image");
                     setTimeout(function(){
                      $("#img_msg").addClass("hide");
                 }, 2000);
                }
            });
            });
        }, 2000);

    });

</script>        

<div class="row">
    <div class="col-sm-offset-2 col-sm-6 success">
        <?php if (isset($error)) { ?>
            <div class="errorForm" id="error" style="display: block;">
                <img align="absmiddle" src="<?php echo HOME_PATH . '/images/error.png'; ?>">&nbsp;<?php echo $errors; ?>.<br>
            </div>
            <!--echo '<div style="color:#FF0000">' . $errors . '</div>';-->
            <?php
        }
        if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
            ?>           
            <?php echo $_SESSION['msg']; ?>.<br>           
            <?php
            unset($_SESSION['msg']);
        }
        ?>
    </div>
</div>
<div class="row">
	<div class="dashboard_container">
		<?php // prd($_SESSION); ?>
		<?php require_once 'includes/sp_left_navigation.php'; ?>
		<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
			<div class="dashboard_right_col">
				<div id="img_msg" class="hide">
					<div id="msg"></div>
				</div>
				<h2 class="hedding_h2"><i class="fa fa-picture-o"></i> <span>Gallery Manager</span></h2>
				<h4 class="logo_bottom_color font_size_16">Facility Name: <?php echo $_SESSION['proName']; ?></h4>
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 adbooking_container">
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="container_search" style="margin-top:10px; margin-bottom:6%;">
									<ul id="myTab" class="nav nav-tabs tab_navbar gallery_nav">
										<li class="active"><a href="#home" id="image" data-toggle="tab">Images</a></li>
										<!--<li ><a href="#ios" id="video" data-toggle="tab">Videos</a></li>-->
										<li class="pull-right">
											<div class="pull_right_sort text-right">
												<div class="form_group_bg form_group_bg_2"> 
													<a class="btn btn-danger center-block btn_search btn_pay pull-right"  href="<?php echo HOME_PATH; ?>supplier/addGallery">Add Image<?php if ($_SESSION['videoEnable'] == 1 && 1==2) { ?>/Video<?php } ?></a>
												</div>
											</div>
										</li>
									</ul>
									<div class="tab-content tab_container" style="padding:5px 0px;" id="pageData">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<style type="text/css">
    #fancybox-overlay{z-index: 999999999 !important;}
    #fancybox-wrap{z-index: 2147483647 !important;}
</style>



