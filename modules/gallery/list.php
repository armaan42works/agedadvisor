<?php
/*
 * Objective: Managing the artwork of supplier
 * Filename: artworkManager.php 
 * Created By : Vinod Kumar Singh <vinod.kumar@ilmp-tech.com>
 * Created On : 19 September 2014
 */
?>
<script type="text/javascript">
    $(document).ready(function() {
        var page = 0;
        var li = '0_no';
        var data = '&id=53';
        changePagination(page, li, 'gallery', data);
        $(".image").colorbox({
            width: '600px',
            height: '500px;'
        });

    });
</script>
<div class="dashboard_container">
    <?php require_once 'includes/sp_left_navigation.php'; ?>
    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12" id="group">
        <div class="dashboard_right_col  thumbail">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-md-8">
                    <h2 class="hedding_h2"><i class="fa fa-picture-o"></i> <span>Gallery Manager</span></h2>
                </div>
                <div class="col-md-3 pull-right" id="link">
                    <a href="<?php echo HOME_PATH; ?>supplier/addGallery">Add Image/Video</a>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left" id="pageData">
<!--                <table class="table table-striped table-hover">
                    <tbody  id="pageData">
                <?php // echo '<div id="pageData1"></div>'; ?>
                    </tbody>
                </table>-->
                <!---  <ul class="list-group">
                      <li class="list-group-item">
                          <div class="row">
                              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                  <span class="pull-left left_td">S no:</span>
                              </div>
                              <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                  <span class="pull-left right_td">Michel Johnson</span>
                              </div>
                          </div>
                      </li>
                      <li class="list-group-item">
                          <div class="row">
                              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                  <span class="pull-left left_td">Artwork description:</span>
                              </div>
                              <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                  <span class="pull-left right_td">Basic</span>
                              </div>
                          </div>
                      </li>
                      <li class="list-group-item">
                          <div class="row">
                              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                  <span class="pull-left left_td">Client expected date:</span>
                              </div>
                              <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                  <span class="pull-left right_td">DieSachbearbeiter, Schonhauer Allee 167c 10433 Berlin, Germany.</span>
                              </div>
                          </div>
                      </li>
                      <li class="list-group-item">
                          <div class="row">
                              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                  <span class="pull-left left_td">Client given artwork:</span>
                              </div>
                              <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                  <span class="pull-left right_td"><img class="img-responsive" src="Comman/images/map_2.png" alt=""/></span>
                              </div>
                          </div>
                      </li>
                      <li class="list-group-item">
                          <div class="row">
                              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                  <span class="pull-left left_td">Admin expected date:</span>
                              </div>
                              <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                  <span class="pull-left right_td">
                                      1. Text will be here<br>
                                      2. Text will be here<br>
                                      3. Text will be here
  
  
                                  </span>
                              </div>
                          </div>
                      </li>
  
                      <li class="list-group-item">
                          <div class="row">
                              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                  <span class="pull-left left_td">Admin given artwork:</span>
                              </div>
                              <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                  <span class="pull-left right_td">$350</span>
                              </div>
                          </div>
                      </li>
  
                      <li class="list-group-item">
                          <div class="row">
                              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                  <span class="pull-left left_td">Admin description:</span>
                              </div>
                              <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                  <span class="pull-left right_td"><img class="img-responsive" src="Comman/images/star.png" alt=""></span>
                              </div>
                          </div>
                      </li>
  
                      <li class="list-group-item">
                          <div class="row">
                              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                  <span class="pull-left left_td">Action:</span>
                              </div>
                              <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                  <span class="pull-left right_td">
                                      1. Text will be here<br>
                                      2. Text will be here<br>
                                      3. Text will be here<br>
  
                                  </span>
                              </div>
                          </div>
                      </li>
  
  
  
                  </ul>---->

<!--              <table class="table table-striped table-hover  table-bordered">
<thead>
<tr><th width="5%" align="left">S.No</th>
<th width="20%" align="left">Artwork Description</th>
<th width="15%" align="left">Client Expected Date</th>
<th width="15%" align="left">Client Given Artwork</th>
<th width="15%" align="left">Admin Expected Date</th>
<th width="15%" align="left">Admin Given Artwork</th>
<th width="20%" align="left">Admin Description</th>
<th width="10%" align="left">Action</th>
</tr>
</thead>
<tbody>
<tr style="margin-top: 10px;" class="row_7">
<td>1.</td>
<td>asdf asd</td>
<td>0000-00-00 00:00:00</td>
<td><img src="http://localhost/Aged_Advisor/admin//files/artwork/thumb_1410506204_download.jpg" title="View Image"></td>
<td>0000-00-00 00:00:00</td>
<td><img src="http://localhost/Aged_Advisor/admin//files/artwork/thumb_1410506204_download.jpg" title="View Image"><a href="http://localhost/Aged_Advisor/admin//download.php?type=artwork&amp;file=1410506204_download.jpg"><i class="fa fa-download"></i>&nbsp;Download</a></td>
<td>Description</td>
<td>Action</td>
</tr>
</tbody>
</table>-->

            </div>

        </div>

    </div>

</div>
<style type="text/css">

    #link{
        border: 2px solid rgb(22, 36, 194);
        padding: 3px;
        width: 170px;
        border-radius: 5px;
        font-weight: bold;
        /* background-color: graytext; */
        text-align: center;
    }
    #group{
        border: 1px solid green;
    }

</style>
