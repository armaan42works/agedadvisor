<!--<script>
$(function(){
    $('#container').html('<video id="vidJS" class="video-js vjs-default-skin" controls preload="auto" width="700" height="700" data-setup="{}"><source type="video/mp4" src="video.flv"></source></video>');
    var vidJS = videojs('vidJS', {techOrder:['flash','html5']}).ready(function(){
        $('#vidJS .vjs-big-play-button').show();
    });
});
</script>-->
<?php
require_once '../../includes/constant.php';
$image = $_GET['name'];
//$path = 'http://localhost/Aged_Advisor/';
?>
<head>
    <link href="<?php echo HOME_PATH; ?>js/video-js/video-js.css" rel="stylesheet" type="text/css"/>
    <!-- video.js must be in the <head> for older IEs to work. -->
    <script type="text/javascript" src="<?php echo HOME_PATH; ?>js/video-js/video.js"></script>

    <!-- Unless using the CDN hosted version, update the URL to the Flash SWF -->
    <script>
        videojs.options.flash.swf = "<?php echo HOME_PATH; ?>js/video-js/video-js.swf";
    </script>


</head>
<body>

    <video id="example_video_1" class="video-js vjs-default-skin" controls preload="none" width="640" height="264"
           poster="http://video-js.zencoder.com/oceans-clip.png"
           data-setup="{}">
        <source src="<?php echo HOME_PATH . 'admin/files/gallery/videos/' . $_GET['name']; ?>" type='video/mp4' />
        <source src="<?php echo HOME_PATH . 'admin/files/gallery/videos/' . $_GET['name']; ?>" type='video/mov' />
        <!--<source src="http://video-js.zencoder.com/oceans-clip.webm" type='video/webm' />-->
        <source src="<?php echo HOME_PATH . 'admin/files/gallery/videos/' . $_GET['name']; ?>" type='video/flv' />
        <track kind="captions" src="demo.captions.vtt" srclang="en" label="English"></track><!-- Tracks need an ending tag thanks to IE9 -->
        <track kind="subtitles" src="demo.captions.vtt" srclang="en" label="English"></track><!-- Tracks need an ending tag thanks to IE9 -->
    </video>
    <style type="text/css">
        .fancybox-overlay{z-index: 999999999 !important;}
        .fancybox-wrap{z-index: 2147483647 !important;}
    </style>