<?php
	$ab='<div style="width:100%; max-width:700px; border:1px solid #ccc; margin:0 auto; padding:15px; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
    	<div style="padding:0 15px 15px; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; width:100%;">
        	<a href="https://www.agedadvisor.nz/"><img src="https://www.agedadvisor.nz/images/find_agedadvisor_retirement_villages_agedcare_logo.png" style="float:left; max-width: 150px;" alt="" title="agedadvisor" /></a>
            <div style="clear:both"></div>
        </div>
        <div style="clear:both"></div>
        <div style="padding:10px 15px; text-align:center;  width:100%; border-top:1px solid #ccc; border-bottom:1px solid #ccc; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
        	<p style="font-size:13px; margin:0; font-family:Arial, Helvetica, sans-serif;">Security Note: AgedAdvisor will never ask you for your password via email</p>
        </div>
        <div style="clear:both"></div>
        <div style="padding: 25px 15px 10px; text-align:center;  width:100%; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
        	<p style="font-size:13px; margin:0; font-family:Arial, Helvetica, sans-serif;">To unsubscribe, or change the frequency that you receive this email, <a href="https://www.agedadvisor.nz/login">click here</a></p>
        </div>
        <div style="clear:both"></div>
        <div style="padding: 15px; text-align:center;  width:100%; background:url("https://www.agedadvisor.nz/images/old_age_retirement_care_services.jpg")no-repeat left; background-size:30%; border:1px solid #ccc; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
        	<div style="float:right; max-width: 65%;">
            	<h3 style="font-size:20px; margin: 0 0 10px; font-family:Arial, Helvetica, sans-serif;color: #888484;text-align: left;	">FIND, REVIEW & COMPARE REST HOMES	</h3>
                <button style="border: 1px solid #F15922; background: #F15922;     color: #fff; float:left; border-radius: 3px; font-size: 15px; letter-spacing: 2px; padding:5px 25px; cursor:pointer;">Find out</button>
            </div>
            <div style="clear:both"></div>
        </div>
        
        <!-- list boxe start -->
        <div style="clear:both"></div>
        <div style="width: 100%; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border-bottom:1px dotted #ccc; margin:30px 0; padding: 10px 0;">
        	<div style="width:25%; margin-right:5%; float:left;">
            	<img src="https://www.agedadvisor.nz/images/elderly_nursing_home_retirement.jpg" style="width:100%;"/>
            </div>
            <div style="width:70%; float:left;">
            	<h3 style="font-size:17px; margin: 0 0 10px; font-family:Arial, Helvetica, sans-serif;color: #F15922;text-align: left; border-bottom: 1px dashed #ccc;padding-bottom: 5px;">Lorem Ipsum is simply dummy text</h3>
                <p style="font-size:13px;color: #969696; line-height:18px; margin:0; font-family:Arial, Helvetica, sans-serif;"><strong style="color:#353535;">Average Rating:</strong> &nbsp;<span style="background: url("https://agedadvisor.nz/images/starbg.png")no-repeat 0px -19px;background-size: 80% auto; width: 104px;height: 14px;display: inline-block;"></span> </p><p style="font-size:13px;color: #969696; line-height:18px; margin:0; font-family:Arial, Helvetica, sans-serif;"><strong style="color:#353535;">Location:</strong> &nbsp;Henderson, RD 2.</p>
                <p style="font-size:13px;color: #969696; line-height:18px; margin:0; font-family:Arial, Helvetica, sans-serif;"><strong style="color:#353535;">Service Type:</strong> &nbsp;Geriatric, Rest home care.</p><p style="font-size:13px;color: #969696; line-height:18px; margin:0; font-family:Arial, Helvetica, sans-serif;"><strong style="color:#353535;">Supplier Name:</strong> &nbsp; Radius Residential Care Limited Radius Taupaki Gables.</p>
            </div>
        <div style="clear:both"></div>
        </div>
        <div style="clear:both"></div>
        <div style="width: 100%; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border-bottom:1px dotted #ccc; margin:30px 0; padding: 10px 0;">
            <div style="width:70%; margin-right:5%; float:left;">
            	<h3 style="font-size:17px; margin: 0 0 10px; font-family:Arial, Helvetica, sans-serif;color: #F15922;text-align: left; border-bottom: 1px dashed #ccc;padding-bottom: 5px;">Lorem Ipsum is simply dummy text</h3>
                <p style="font-size:13px;color: #969696; line-height:18px; margin:0; font-family:Arial, Helvetica, sans-serif;"><strong style="color:#353535;">Average Rating:</strong> &nbsp;<span style="background: url"https://agedadvisor.nz/images/starbg.png")no-repeat 0px -19px;background-size: 80% auto; width: 104px;height: 14px;display: inline-block;"></span> </p><p style="font-size:13px;color: #969696; line-height:18px; margin:0; font-family:Arial, Helvetica, sans-serif;"><strong style="color:#353535;">Location:</strong> &nbsp;Henderson, RD 2.</p>
                <p style="font-size:13px;color: #969696; line-height:18px; margin:0; font-family:Arial, Helvetica, sans-serif;"><strong style="color:#353535;">Service Type:</strong> &nbsp;Geriatric, Rest home care.</p><p style="font-size:13px;color: #969696; line-height:18px; margin:0; font-family:Arial, Helvetica, sans-serif;"><strong style="color:#353535;">Supplier Name:</strong> &nbsp; Radius Residential Care Limited Radius Taupaki Gables.</p>
            </div>
        	<div style="width:25%;float:left;">
            	<img src="https://www.agedadvisor.nz/images/elderly_nursing_home_retirement.jpg" style="width:100%;"/>
            </div>
        <div style="clear:both"></div>
        </div>
        <div style="clear:both"></div>
    
      
        <div style="margin-top:5px; width:100%;">
        	<p style="font-size:13px; font-family:Arial, Helvetica, sans-serif; text-align:center;">Our Sponsors - </p>
            <div style="background:#E4E4E4; padding:5px;">
            	<img src="https://www.agedadvisor.nz/admin/files/company_logo/1437036717_Home_of_Poi.png" alt="" title="" style="float:left; width:25%;padding: 10px; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; height: 67px;" />
            	<img src="https://www.agedadvisor.nz/admin/files/company_logo/1437104058_caxton_print.png" alt="" title="" style="float:left; width:25%;padding: 10px; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; height: 67px;" />
            	<img src="https://www.agedadvisor.nz/admin/files/company_logo/1439178368_BDO_logo_NZ.png" alt="" title="" style="float:left; width:25%;padding: 10px; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; height: 67px;" />
            	<img src="https://www.agedadvisor.nz/admin/files/company_logo/1425244504_Hearing_technology.png" alt="" title="" style="float:left; width:25%;padding: 10px; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; height: 67px;" />
        <div style="clear:both"></div>	
                
            </div>
        <div style="clear:both"></div>	
        </div> 
    </div>';

?>