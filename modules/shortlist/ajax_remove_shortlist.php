<?php

session_start();
include("../../application_top.php");
ini_set('display_errors', 1);
global $db;

$product_id = $_REQUEST['pro'];
$supplier_id = $_REQUEST['sp_id'];
$customer_username = $_REQUEST['user_name'];
$cookie_user_name = $_COOKIE['csUserFullName'];
$cs_userid = $_SESSION['csId'];

if (isset($cs_userid) && !empty($cs_userid)) {

    $del_shortlist = "DELETE FROM ad_myshortlist WHERE product_id='$product_id' AND supplier_id='$supplier_id' AND customer_user_id='$cs_userid' ";

    $del_shortlist_data = mysqli_query($db->db_connect_id, $del_shortlist);
    if ($del_shortlist_data) {
        $product_count = "SELECT COUNT(product_id) AS myshorltlist_count FROM ad_myshortlist WHERE customer_user_id='$cs_userid'";
        $product_count_query = mysqli_query($db->db_connect_id, $product_count);
        $shorltlist_count = mysqli_fetch_assoc($product_count_query);
        $myshorltlist_count = $shorltlist_count['myshorltlist_count'];
        echo $_SESSION['shortlist_count'] = $myshorltlist_count;
    }
} else {

    if (isset( $_COOKIE[ 'session_cookie_product' ] )) {
        
        $session_cookie_product = unserialize($_COOKIE['session_cookie_product'], ["allowed_classes" => false]);
        $_SESSION['session_cookie_product'] = $session_cookie_product;
        unset($_SESSION['session_cookie_product'][$supplier_id][$product_id]);
        unset($_COOKIE['session_cookie_product']);
        
        //If test_cookie does not exist then set test_cookie for 30 days
        setcookie( 'session_cookie_product', serialize($_SESSION['session_cookie_product']), time() + (86400 * 3), '/');
        
        $_SESSION['shortlist_cookie_count'] = count($_SESSION['session_cookie_product'][$supplier_id]);
    } else {
        unset($_SESSION['session_cookie_product'][$supplier_id][$product_id]);
        unset($_COOKIE['session_cookie_product']);
        
        $_SESSION['shortlist_cookie_count'] = count($_SESSION['session_cookie_product'][$supplier_id]);
        //If test_cookie does not exist then set test_cookie for 30 days
        setcookie('session_cookie_product', serialize($_SESSION['session_cookie_product']), time() + (86400 * 3), '/');
    }
    //$_SESSION['shortlist_cookie_count'] = count($_SESSION['session_cookie_product']);
    unset($_COOKIE['shortlist_cookie_count']);
    setcookie( 'shortlist_cookie_count', $_SESSION['shortlist_cookie_count'], time() + (86400 * 3), '/');
    
    echo $_SESSION['shortlist_cookie_count'];
}
?>
