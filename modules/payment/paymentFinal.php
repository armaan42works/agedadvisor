<?php
    $amount = 15;
?>
<div class="dashboard_container">
    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 padding_left">
        <div class="sidebar" id="sidebar">
            <!-- begin sidebar scrollbar -->
            <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 100%;">
                <div data-height="100%" data-scrollbar="true" style="overflow: hidden; width: auto; height: 100%;">
                    <!-- begin sidebar nav -->
                    <ul class="nav">
                        <li class="has-sub"><a href="daccountinformation.html" target="_self"><i class="fa fa-book"></i> <span>Account Information</span></a></li>
                        <li class="has-sub active"><a href="payment.html" target="_self"><i class="fa fa-money"></i> <span>Payment</span></a></li>
                        <li class="has-sub"><a href="#"><i class="fa fa-list-alt"></i> <span>Order Information</span></a></li>
                        <li class="has-sub"><a href="#"><i class="fa fa-file-text-o"></i> <span>Ad Booking Manager</span></a></li>
                        <li class="has-sub"><a href="#"><i class="fa fa-picture-o"></i> <span>Gallery Manager</span></a></li>
                        <li class="has-sub"><a href="#"><i class="fa fa-twitch"></i> <span>Feedback Center</span></a></li>
                        <li class="has-sub"><a href="#"><i class="fa fa-briefcase"></i> <span>Artwork Manager</span></a></li>
                        <li class="has-sub"><a href="#"><i class="fa fa-pencil-square-o"></i> <span>Service/Product Listing</span></a></li>
                    </ul>
                </div>
            </div>
            <!-- end sidebar scrollbar -->
        </div>
    </div>
    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
        <div class="dashboard_right_col">
            <h2 class="hedding_h2"><i class="fa fa-money"></i> <span>Payment</span></h2>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 payment_col  padding_left thumbnail">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                        <div class="pull_left_bg">
                            <h3 class="hedding_h3 payment_h3">Select Payment Mode</h3>
                            <form class="text-center form_payment payment_col2">
                                <label class="checkbox-inline" style="margin-top:18%;">
                                    <input type="radio" name="optionsRadiosinline" id="optionsRadios3" value="Business" style="width:100%;"><span>Debit card</span></label>
                                <label class="checkbox-inline input_radio">
                                    <input type="radio" name="optionsRadiosinline" id="optionsRadios4" value="Premium" style="width:100%;"><span>Credit Card</span></label>
                            </form>
                        </div>
                    </div>
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                        <div class="pull_left_bg" style="border:none; padding:2% 4% 10%;">
                            <form class="form_payment">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                                        <div class="form-group">
                                            <label class="label_text" for="name">Credit Card Number</label>
                                            <input id="name" class="form-control input_fild_1" type="text">
                                        </div>
                                        <div class="form-group">
                                            <label class="label_text" for="name">Name on this card</label>
                                            <input id="name" class="form-control input_fild_1" type="text">
                                        </div>
                                        <div class="form-group">
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padding_left  padding_right">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left padding_right">
                                                    <label class="label_text" for="name">Expiry</label>
                                                </div>
                                                <select class="form-control select_option_2 pull-left">
                                                    <option>Month</option>
                                                    <option>2</option>
                                                    <option>3</option>
                                                    <option>4</option>
                                                    <option>5</option>
                                                </select>
                                                <select class="form-control select_option_2 pull-left">
                                                    <option>Year</option>
                                                    <option>2011</option>
                                                    <option>2012</option>
                                                    <option>2013</option>
                                                    <option>2014</option>
                                                </select>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padding_left">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left">
                                                    <label class="label_text" for="name">CVV No.</label>
                                                </div>
                                                <input id="name" class="form-control input_fild_2 pull-left" value='' type="password" >
                                            </div>
                                        </div>
                                        <div class="form-group text-center total_bg">
                                            <span class="total_price">Total <?php echo $amount ?></span>
                                            <button type="submit" class="btn btn-danger center-block btn_search btn_pay" style="float:left; margin-bottom:0px;">Pay</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
