<style>
.google-map {margin:20px 0;border: 4px #e67e22 solid;width:100% !important;}
.map-infow h5{font-weight:bold;text-transform:capitalize;border-bottom:2px solid #F15922;color:#042E6F;}
.map-infow p {margin:0;}
</style> 
<div class="google-map-wrap" itemscope itemprop="hasMap" itemtype="http://schema.org/Map">
                <div id="google-map" class="google-map"></div><!-- #google-map -->
            </div>

                <?php /* === MAP DATA === */ ?>
                <?php  
                $ltd[] = array();
                $i=0;
                
                
                foreach($data as $info){
                    //if(isset($info["latitude"]) && isset($info["longitude"])){
                    $ltd[$i]["latitude"]=$info["latitude"];
                    $ltd[$i]["longitude"]=$info["longitude"];
                    $ltd[$i]["title"]=$info["title"];
                    $ltd[$i]["id"]=$info["id"];
                    $ltd[$i]["zip"]=$info["zip"];
                    $ltd[$i]["address"]=$info["address"];
                    $ltd[$i]["suburbTitle"]=$info["suburbTitle"];
                    $ltd[$i]["city"]=$info["city"];
                    $html='<h5>'.str_replace("'",'"',$info["title"]).'</h5>';
                    $html .='<p>Average Rating:'.OverAllNEWRatingProduct($info['id']).'</p>';
                    $html .= '<p>Total reviews: <i class="fa fa-users"></i>  '.str_replace("'",'"',overAllRatingProductCount($info['id'])).'</p>';
                    $ltd[$i]["html"]='<div class="map-infow">'.$html.'</div>';
                    $i++;
                    //}
                }   
                $locations = array();
                foreach($ltd as $lat){
                    $locations[] = array(
                    'google_map' => array(
                        'lat' => ''.$lat["latitude"].'',
                        'lng' => ''.$lat["longitude"].'',
                    ),
                    'location_address' => ''.$lat["city"].'',
                    'location_name'    => ''.$lat["html"].'',
                    'fulladdress' => $lat["address"].','.$lat["suburbTitle"].','.$lat["city"].' '.$lat["zip"].'New Zealand'
                   );
                }
                //echo "<pre>";
                
                
                //print_r($locations);
            ?>


                <?php /* === PRINT THE JAVASCRIPT === */ ?>

                <?php
                /* Set Default Map Area Using First Location */
                $map_area_lat = isset( $locations[0]['google_map']['lat'] ) ? $locations[0]['google_map']['lat'] : '';
                $map_area_lng = isset( $locations[0]['google_map']['lng'] ) ? $locations[0]['google_map']['lng'] : '';
                ?>

                <script type="text/javascript">
                jQuery(document).ready( function($) {

                    /* Do not drag on mobile. */
                    var is_touch_device = 'ontouchstart' in document.documentElement;
                    var map = new GMaps({
                        el: '#google-map',
                        lat: '<?php echo $map_area_lat; ?>',
                        lng: '<?php echo $map_area_lng; ?>',
                        scrollwheel: true,
                       // zoom:12,
                        draggable: ! is_touch_device
                    });
                    

                    /* Map Bound */
                    var bounds = [];
                    
var cnt = 0;
var locationIfOnePointOnTheMap;

                    <?php 
						$totalcount = count($locations); 
					?>

var totalCount = Number('<?php echo $totalcount;?>');

var checkAllDone = function(){
//alert(totalCount);
	if(cnt >= totalCount){
		if(totalCount == 1){
			map.setCenter(locationIfOnePointOnTheMap);
			map.setZoom(12);
		}
		else{
			map.fitLatLngBounds(bounds);
		}
	}
	else{
		setTimeout(checkAllDone, 1000);
	}

};

					<?php /* For Each Location Create a Marker. */
                    foreach( $locations as $location ){
                        $name = $location['location_name'];
                        $addr = $location['location_address'];
                        $map_lat = $location['google_map']['lat'];
                        $map_lng = $location['google_map']['lng'];
                        $map_full_add = $location['fulladdress'];
                        ?>
						

						(function(address){

							var latd;var longd;
							$.ajax({
								 url: "https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyAWkRDVedNbYuIzpKfxJC6EY89OKkCd3-c&address=" + address, 
								 success: function(result)
								 {
									if(result && result.results && result.results[0] && result.results[0].geometry && result.results[0].geometry.location && result.results[0].geometry.location.lat && result.results[0].geometry.location.lng){

										latd = result.results[0].geometry.location.lat;
										longd = result.results[0].geometry.location.lng;
										//alert(longd);

							/* Set Bound Marker */
						   // var latlng = new google.maps.LatLng(<?php echo $map_lat; ?>, <?php echo $map_lng; ?>);
										var latlng = new google.maps.LatLng(latd, longd);
										locationIfOnePointOnTheMap = latlng;
										bounds.push(latlng);
										//var iconBase = '<?php echo HOME_PATH;?>/images/orange_item.png';
										var iconBase = 'http://www.agedadvisor.nz/images/orange_item.png';
										/* Add Marker */ 
										map.addMarker({
											lat: latd,
											lng: longd,
											title: 'Click here for information',
											icon: iconBase,										
											infoWindow: {
												content: '<p><?php echo $name; ?></p>'
											}

										});
									}

									cnt++;
								},
								error: function(){
									cnt++;
								}
							});

						}('<?php echo $map_full_add;?>'));


							

						//if(cnt == tot){
							//alert('a');
//							map.fitLatLngBounds(bounds);
						//}

                    <?php 
						
					} //end foreach locations 
                    ?>

					checkAllDone();

                    /* Fit All Marker to map */
                    //map.fitLatLngBounds(bounds);
                    

                    /* Make Map Responsive */
                    var $window = $(window);
                    function mapWidth() {
                        var size = $('.google-map-wrap').width();
                        $('.google-map').css({width: '1140px', height: '400px'});
                    }
                    mapWidth();
                    $(window).resize(mapWidth);

                });


                </script>