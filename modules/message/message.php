<?php
    /*
     * Objective  : List of all the message
     * Filename : message.php
     * Created By : Sanket Khanna <sanket.khanna@ilmp-tech.com> 
     * Created On : 25 August 2014
     */
?>
<!----Dashbord_main START--------------------->
<section id="body_container" class="Dashbord_main">
    <ul class="breadcrumb">
        <li><a href="<?php echo HOME_PATH ?>user"><strong>Home</strong></a></li>
        <li class="last"><a href="javascript:void(0);">Message box</a></li>
    </ul>

    <div class="messag_send"></div>

    <!--message container code starts-->
    <div class="message_container">

<?php
    $msg_query = "SELECT dm.*, du.name FROM " . _prefix('messages') . " AS dm LEFT JOIN " . _prefix('users') . " as du ON dm.sender_id = du.id  WHERE (dm.receiver_id ={$_SESSION['userId']} || dm.sender_id ={$_SESSION['userId']}) && (dm.parent_message_id IS NULL || dm.parent_message_id = '0') ORDER BY dm.created DESC";
            $msgRes = $db->sql_query($msg_query);
            $msgDatas = $db->sql_fetchrowset($msgRes);
            $count = $db->sql_numrows($msgRes);
            if($count > 0){
                ?>
        <div class="row">
            <div class="coll box_1 coll_hedding">From</div>
            <div class="coll box_2 coll_hedding">Subject</div>
            <div class="coll box_3 coll_hedding">Date</div>
        </div>
        <?php
            
            foreach ($msgDatas as $msgData) {
                $getnom = "SELECT count(*) as total FROM " . _prefix("messages") . " WHERE parent_message_id = '{$msgData['id']}'";
                $resNom = $db->sql_query($getnom);
                $dataNom = $db->sql_fetchrow($resNom);
                $nom = $dataNom['total'];
                if ($nom > 0) {
                    $nom = ' (' . $nom . ')';
                } else {
                    $nom = '';
                }
                $read = $msgData['is_read_client'] == 0 ? '' : 'show_box';
                ?>
                <div class="row <?php echo $read; ?>"><a href="<?php echo HOME_PATH ?>user/message/inbox/<?php echo md5($msgData['id']); ?>">
                        <div class="coll box_1 "><strong><?php echo $msgData['name'] == '' ? constant('ADMINNAME') . $nom : $msgData['name'] . $nom ?></strong></div>
                        <div class="coll box_2 "><strong><?php echo $msgData['subject']; ?></strong></div>
                        <div class="coll box_3 "><strong><?php echo date('M d, Y', strtotime($msgData['created'])); ?></strong> </div></a>
                </div>
                <?php
            }
        ?>


        <ul class="pagination">
            <div class="prev"><a href="#"><i class="fa fa-angle-double-left"></i></a></div>
            <li class="active"><a href="#">1</a></li>
            <li><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">4</a></li>
            <li><a href="#">5</a></li>
            <li><a href="#">6</a></li>
            <div class="next"><a href="#"><i class="fa fa-angle-double-right"></i></a></div>
        </ul>
        <?php
            } else {
                echo "<div class='norecord'>No Record found</div>";
            }
    ?>
        
    </div>

</section>