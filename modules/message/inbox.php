<?php
    /*
     * Objective  : List of all the message
     * Filename : message.php
     * Created By : Sanket Khanna <sanket.khanna@ilmp-tech.com> 
     * Created On : 25 August 2014
     */
    require_once("./admin/fckeditor/fckeditor.php");
?>
<!----Dashbord_main START--------------------->
<section id="body_container" class="Dashbord_main">
    <ul class="breadcrumb">
        <li><a href="<?php echo HOME_PATH ?>user"><strong>Home</strong></a></li>
        <li><a href="<?php echo HOME_PATH ?>user/message">Message box</a></li>
        <li class="last"><a href="javascript:void(0);">Read</a></li>
    </ul>
    <?php
        $msgId = $pathInfo['call_parts']['3'];
        $getMail = "SELECT * FROM " . _prefix('messages') . " WHERE md5(id)= '$msgId'";
        $res = $db->sql_query($getMail);
        $msg = $db->sql_fetchrow($res);
    ?>

    <div class="messag_send"></div>

    <!--message container code starts-->
    <div class="message_container session_container">
        <div class="session_subject"><strong>Subject :</strong> <?php echo $msg['subject']; ?></div>
        <?php
            if (isset($submit) && $submit == 'Reply') {

                $fields_array = reply_message();
                $response = Validation::_initialize($fields_array, $_POST);
                if (isset($response['valid']) && $response['valid'] > 0) {
                    $body = $_POST["FCKeditor1"];
                    $fields = array('parent_message_id' => $msg['id'],
                        'subject' => $msg['subject'],
                        'body' => $body,
                        'quote_id' => $msg['quote_id'],
                        'email_id' => ADMINNAME,
                        'ip_address' => $_SERVER['REMOTE_ADDR'],
                        'sender_id' => $_SESSION['userId'],
                        'receiver_id' => 0,
                        'user_type' => 0,
                        'is_read_client' => 1);

                    $insert_result = $db->insert(_prefix('messages'), $fields);

                    $message_id = mysqli_insert_id($db->db_connect_id);
                    $updatefield = array(
                        'created' => date('Y-m-d H:i:s'),
                        'is_read_admin' => '0');
                    $where = " WHERE id = '{$msg['id']}' ";
                    $db->update(_prefix('messages'), $updatefield, $where);
                    $attachment = '';
                    if (isset($_FILES['file_name'])) {
                        for ($i = 0; $i < count($_FILES['file_name']['name']); $i++) {

                            if ($_FILES['file_name']['error'][0] == 0) {
                                $fileData = pathinfo($_FILES['file_name']["name"][$i]);
                                $target_path = DOCUMENT_PATH . '/admin/files/message/';
                                $filename = rand(1, 100000);
                                $date = new DateTime();
                                $timestamp = $date->getTimestamp();
                                $imageName = $filename . $timestamp . '_' . basename($_FILES['file_name']['name'][$i]);
                                $target_path = $target_path . $imageName;
                                $message_fields = array('message_id' => $message_id, 'filename' => $imageName);
                                if ($_FILES['file_name']['name'][$i] != "") {

                                    $insert_attachment = $db->insert(_prefix('attachment'), $message_fields);
                                }

                                $var = move_uploaded_file($_FILES['file_name']["tmp_name"][$i], $target_path);
                                $attachment = ADMIN_PATH . 'files/message/' . $imageName;
                            }
                        }
                    }
                    // $send = sendmail($email_id, $subject, $body, $attachment);
                    if ($insert_result) {
                        $msg = common_message(1, constant('SEND_MESSAGE'));
                        $_SESSION['msg'] = $msg;
                    }
                } else {
                    $errors = '';
                    foreach ($response as $key => $message) {
                        $error = true;
                        $errors .= $message . "<br>";
                    }
                }
            }
        ?>
        <form id='replyForm' name='replyForm' method='POST' enctype="multipart/form-data">
            <div class="sendMail">
                <?php
                    $oFCKeditor = new FCKeditor('FCKeditor1');
                    $oFCKeditor->BasePath = HOME_PATH . '/admin/fckeditor/';
                    $oFCKeditor->Height = '400px';
                    $oFCKeditor->Value = "";

                    $oFCKeditor->Create();
                ?>
                <div class="input text file_selected" >
                    <input type="file" name="file_name[]">
                </div>
                <div class="input text add" >
                    <a href="javascript:void(0);" class="addFile">+Add</a>
                </div>
                <div class="reply_button">
                    <input type="submit" value="Reply" name="submit" class="creat_account_btn"  style='float:left;'></div>
            </div>
        </form>
        <?php
            $getmail = "SELECT * FROM " . _prefix("messages") . " where md5(parent_message_id)='$msgId' order by id desc";
            $resMail = $db->sql_query($getmail);
            $numMail = $db->sql_numrows($resMail);
            $allMails = $db->sql_fetchrowset($resMail);
            if ($numMail > 0) {
                foreach ($allMails as $mail) {

                    if ($mail['is_read'] == 0 && $mail['receiver_id'] == $_SESSION['userId'] && !isset($submit) && $submit != 'Reply') {
                        $db->sql_query("UPDATE `da_messages` SET `is_read_client` = '1' WHERE `da_messages`.`id` ='{$mail['id']}'");
                    }
                    //rest of the emails
                    ?>
        <div class="massege_detels"><div class="session_from"><strong>FROM :</strong> <?php echo $mail['sender_id'] == 0 ? 'KA Analytics' : $_SESSION['name'] ?></div>
            <div class="session_from"><strong>TO :</strong> <?php echo stripslashes($mail['email_id']); ?></div>
            <div class="session_date"><strong>Date :</strong> <?php echo date('M d, Y H: i A', strtotime($mail['created'])); ?></div>
                        <div class="session_description"><?php echo stripslashes($mail['body']); ?></div></div>
                    <?php
                    getAllAttachment($mail['id']);
                }
                ?>

                <?php
            }

            $getMail1 = "SELECT dm.*, du.name FROM " . _prefix('messages') . " as dm LEFT JOIN " . _prefix('users') . " as du ON dm.sender_id = du.id   WHERE md5(dm.id)= '$msgId'";
            $res1 = $db->sql_query($getMail1);
            $msg1 = $db->sql_fetchrow($res1);
            if ($msg1['is_read'] == 0 && ($msg1['receiver_id'] == $_SESSION['userId'] || $msg1['sender_id'] == $_SESSION['userId'])) {
                $db->sql_query("UPDATE `da_messages` SET `is_read_client` = '1' WHERE `da_messages`.`id` ='{$msg1['id']}'");
            }
            //last
        ?>
        <div class="massege_detels"><div class="session_from"><strong>FROM :</strong> <?php echo $msg1['sender_id'] == 0 ? constant('ADMINNAME') : $msg1['name']; ?></div>
            <div class="session_from"><strong>TO :</strong> <?php echo $msg1['email_id']; ?></div>
            <div class="session_date"><strong>Date :</strong> <?php echo date('M d, Y H: i A', strtotime($msg1['created'])); ?></div>
            <div class="session_description"><?php echo stripslashes($msg1['body']); ?></div></div>
        <?php
            getAllAttachment($msg1['id']);
        ?>
        <div class="reply_button"><a href="javascript:void(0);" class="replydiv">Reply</a></div>
    </div>

</section>
<script>
    $(document).ready(function() {
        $('.sendMail').hide();
        $('.replydiv').click(function() {
            $('html, body').animate({scrollTop: 0}, 0);
            $(this).hide();
            $('.sendMail').show();
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.addFile').click(function() {
            var inputFile = '<div class="input text file_selected" ><input type="file" name="file_name[]" id="file_name"></div>';
            $('.add').before(inputFile);
        });
    })
</script>