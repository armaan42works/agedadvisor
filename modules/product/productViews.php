<?php
    if (!isset($_SESSION['AccountType']) || empty($_SESSION['AccountType']) || $_SESSION['AccountType'] == 1) {
        $_SESSION['msg'] = 'Please upgrade your account to use this service';
        redirect_to(HOME_PATH . 'supplier/payment');
    }
?>
<script type="text/javascript">
    $(document).ready(function() {
        setTimeout(function() {

            $("#success").hide('slow');
            $('#error').hide('slow');
        }, 5000);
        var page = 0;
        var li = '0_no';
        var data = '';
        changePagination(page, li, 'productviews', data);
//        $('#search_form').submit(function(e) {
//            $('#search_form').validate();
//            e.preventDefault();
//            var data = '&search_input=' + $.trim($('#search_input').val()) + '&search_type=' + $('#search_type').val();
//            changePagination(page, li, 'product', data);
//        });
    });
</script>
<div class="row">
    <div class="col-sm-offset-2 col-sm-6 success">
        <?php if (isset($error)) { ?>
                <div class="errorForm" id="error" style="display: block;">
                    <img align="absmiddle" src="<?php echo HOME_PATH . '/images/error.png'; ?>">&nbsp;<?php echo $errors; ?>.<br>
                </div>
                <!--echo '<div style="color:#FF0000">' . $errors . '</div>';-->
                <?php
            }
            if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
                ?>
                <?php echo $_SESSION['msg']; ?><br>
                <?php
                unset($_SESSION['msg']);
            }
        ?>
    </div>
</div>
<div class="container" style="padding:0px 3% 0">
    <div class="row">
        <div class="dashboard_container">
            <?php require_once 'includes/sp_left_navigation.php'; ?>
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                <div class="dashboard_right_col">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="col-md-8">
                            <h2 class="hedding_h2"><i class="fa fa-list-alt"></i> <span>Service/Product Views</span></h2>
                        </div>
                    </div>


<!--                    <div>
                        --------------------------- End here -------------------------
                        <?php
                            $search_array = array('title' => 'Title', 'facility_type' => 'Facility Type');
                        ?>
                        <div class="sublinks" height="40" valign="bottom">
                            <form id="search_form" method="post" action="">
                                <input type="text" name="search_input" id="search_input" value="<?php echo isset($search_input) ? $search_input : ''; ?>">
                                <select name="search_type" id="search_type">
                                    <?php
                                        $option = '<option value="">------ Choose Option-----</option>';
                                        foreach ($search_array as $key => $value) {
                                            $option .="<option value='$key'>$value</option>";
                                        }
                                        echo $option;
                                    ?>
                                </select>
                                <input type="submit" value="Search">
                            </form>
                        </div>
                    </div>-->


                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left scroll"   id="pageData" >
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
