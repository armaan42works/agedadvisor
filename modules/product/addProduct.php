<?php
//if ($_SESSION['AccountType'] == 1) {
//    redirect_to(HOME_PATH . 'supplier/payment');
//}
global $db;
$useId = $_SESSION['userId'];
$supplier_name = $_SESSION['username'];

//
//$sql_queryL = "SELECT member.* FROM " . _prefix("users") . " AS user"
//        . " Left join " . _prefix("membership_prices") . " AS member ON member.id=user.account_type WHERE user.id='$useId'";
//$resL = $db->sql_query($sql_queryL);
//$accountType = $db->sql_fetchrow($resL);

$query = "SELECT id, title,file,type FROM " . _prefix("galleries") . "   "
        . " where deleted = 0  AND user_id=" . $useId . " AND type=0  ORDER BY id";

$res = $db->sql_query($query);
$data = $db->sql_fetchrowset($res);
$image = '';
//include_once("./fckeditor/fckeditor.php");
if ($_SERVER['HTTP_X_FORWARDED_FOR']) {
    $IPaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
} else {
    $IPaddress = $_SERVER['REMOTE_ADDR'];
}

//$geoData = unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip=' . $IPaddress));
//$lat = $geoData['geoplugin_latitude'];
//$long = $geoData['geoplugin_longitude'];

/* Default value */
$lat = -41.211722;
$long = 175.517578;
$zip = 5792;
if (isset($submit) && $submit == 'Submit') {

    $fields_array = product();
    $response = Validation::_initialize($fields_array, $_POST);

//    if (isset($_POST['zip']) && !empty($_POST['zip']) && $_POST['geotag'] == 'no') {
//        $val = getLnt($_POST['zip']);
//        $latitude = $val['lat'];
//        $longitude = $val['lng'];
//    } else {
//        $geoData = unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip=' . $IPaddress));
//        $latitude = $geoData['geoplugin_latitude'];
//        $longitude = $geoData['geoplugin_longitude'];
//    }
    $latitude = $_POST['latitude'];
    $longitude = $_POST['longitude'];
    $ImageExt = array("jpg", "png", "jpeg", "gif");

    if ($_FILES['staff_image']['error'] == 0) {
        $ext = end(explode('.', $_FILES['staff_image']['name']));
        if (in_array($ext, $ImageExt)) {
            $file_tmp = $_FILES['staff_image']["tmp_name"];
            $target_pathL = ADMIN_PATH . 'files/product/staff/';
            $stf_image_name = uploadImageVideo($target_pathL, $_FILES['staff_mage']['iname'], $file_tmp);
        } else {
            $_SESSION['msg'] = common_message_supplier(0, "Please select a valid type of Image");
        }
    }
    if ($_FILES['management_image']['error'] == 0) {
        $ext = end(explode('.', $_FILES['management_image']['name']));
        if (in_array($ext, $ImageExt)) {
            $file_tmp = $_FILES['management_image']["tmp_name"];
            $target_pathL = ADMIN_PATH . 'files/product/management/';
            $mng_image_name = uploadImageVideo($target_pathL, $_FILES['management_image']['name'], $file_tmp);
        } else {
            $_SESSION['msg'] = common_message_supplier(0, "Please select a valid type of Image");
        }
    }
    if ($_FILES['activities_image']['error'] == 0) {
        $ext = end(explode('.', $_FILES['activities_image']['name']));
        if (in_array($ext, $ImageExt)) {
            $file_tmp = $_FILES['activities_image']["tmp_name"];
            $target_pathL = ADMIN_PATH . 'files/product/activity/';
            $act_image_name = uploadImageVideo($target_pathL, $_FILES['activities_image']['name'], $file_tmp);
        } else {
            $_SESSION['msg'] = common_message_supplier(0, "Please select a valid type of Image");
        }
    }


    if (isset($response['valid']) && $response['valid'] > 0) {
        $fields = array(
            'title' => trim($_POST['title']),
            'description' => trim($_POST['description']),
            'keyword' => trim($_POST['keyword']),
            'city_id' => trim($_POST['city']),
            'suburb_id' => trim($_POST['suburb']),
            'restcare_id' => trim($_POST['restcare']),
            'zip' => trim($_POST['zip']),
            'supplier_id' => $useId,
            'latitude' => $latitude,
            'longitude' => $longitude,
            'image' => $_POST['image'],
            'ext_url' => $ext_url,
            'youtube_video' => $youtube_video,
            'created' => date('Y-m-d h:i:s', time()),
            'staff_comment' => $staff_comment,
            'staff_image' => $stf_image_name,
            'management_comment' => $management_comment,
            'management_image' => $mng_image_name,
            'activity_comment' => $activities_comment,
            'activity_image' => $act_image_name
        );
        $extar_field = array(
            'sp_id' => $useId,
            'brief' => trim($_POST['brief']),
            'no_of_room' => trim($_POST['noOfRooms']),
            'no_of_beds' => trim($_POST['noOfBeds']),
            'extra_info1' => trim($_POST['extraInfo1']),
            'extra_info2' => trim($_POST['extraInfo2']),
            'extra_info3' => trim($_POST['extraInfo3']),
            'created' => date('Y-m-d h:i:s', time())
        );
        $image = $_POST['image'];
        $insert_result = $db->insert(_prefix('products'), $fields);
        $last_product_id = mysqli_insert_id();
        if ($insert_result) {

            if ($_POST['gImage'] != '') {
//                $imageArray = explode(',', $image);
                //Delete product images from pro_images
                $queryDel = "DELETE FROM " . _prefix("pro_images") . " WHERE product_id = '$pr_id'";
                $delete_service = mysqli_query($db->db_connect_id, $queryDel);
                //Inserting services type ids in pro_services table
                if ($delete_service) {
                    foreach ($image as $imageId) {
                        $image_field = array(
                            'supplier_id' => $useId,
                            'product_id' => $pr_id,
                            'gallery_id' => $imageId,
                            'modified ' => date('Y-m-d h:i:s', time())
                        );
                        // Message for insert
                        $insert_image = $db->insert(_prefix('pro_images'), $image_field);
                    }
                }
            }
            $queryDel = "DELETE FROM " . _prefix("pro_services") . " WHERE product_id = '$pr_id'";
            $delete_service = mysqli_query($db->db_connect_id, $queryDel);
            //Inserting services type ids in pro_services table
            foreach ($_POST['facility_type'] as $servise_id) {
                $services_field = array(
                    'supplier_id' => $useId,
                    'product_id' => $last_product_id,
                    'service_id' => $servise_id
                );
                // Message for insert
                $insert_service = $db->insert(_prefix('pro_services'), $services_field);
            }
            $addFacility = $addMoreFacility;
            $extar_field['product_id'] = $last_product_id;
            // Message for insert
            $insert_result_extra = $db->insert(_prefix('extra_facility'), $extar_field);
            if ($insert_result_extra) {
                if ($addFacility != "") {
                    $to = ADMIN_EMAIL;
                    $email = emailTemplate('supplier_add_facility');
                    if ($email['subject'] != '') {
                        $message = str_replace(array('{supplier_name}', '{addfacility}'), array($supplier_name, $addFacility), $email['description']);
                        $send = sendmail($to, $email['subject'], $message);
                    }
                }
                $msg = common_message_supplier(1, constant('INSERT_PRODUCT'));
                $_SESSION['msg'] = $msg;
                //ends here
                redirect_to(HOME_PATH . 'supplier/product-manager');
            }
        }
    } else {
        $errors = '';
        foreach ($response as $key => $message) {
            $error = true;
            $errors .= $message . "<br>";
        }
    }
}

if (key($_REQUEST) == 'edit') {
    $id = $_GET['id'];
    $sql_query = "SELECT pr.*,pr.id as pr_id, extinfo.*,extinfo.id as ex_id,sr.name as facility_name FROM " . _prefix("products") . " AS pr "
            . " Left join " . _prefix("services") . " AS sr ON pr.facility_type=sr.id"
            . " Left join " . _prefix("extra_facility") . " AS extinfo ON extinfo.product_id =pr.id"
            . " where md5(pr.id)='$id'";
    $res = $db->sql_query($sql_query);
    $records = $db->sql_fetchrowset($res);

    //Service ids for selected product
    $sql_service = "select service_id from " . _prefix("pro_services") . " where product_id=" . $records[0]['pr_id'];
    $res_service = $db->sql_query($sql_service);
    $records_service = $db->sql_fetchrowset($res_service);
    foreach ($records_service as $ids) {
        $service_ids[] = $ids['service_id'];
    }


    //images from gallery

    $getslider = "SELECT gallery.* FROM " . _prefix('pro_images') . " AS proImage "
            . "LEFT JOIN  " . _prefix('galleries') . " AS gallery ON gallery.id= proImage.gallery_id AND gallery.status=1 AND gallery.deleted=0 "
            . "  WHERE proImage.status=1 && proImage.deleted= 0 AND md5(proImage.product_id)='" . $id . "'";
    $resslider = $db->sql_query($getslider);
//    $count = $db->sql_numrows($resslider);
    $dataslider = $db->sql_fetchrowset($resslider);






    //prd($service_ids);
    if (count($records)) {
        foreach ($records as $record) {
            $ex_id = $record['ex_id'];
            $pr_id = $record['pr_id'];
            $title = $record['title'];
            $description = $record['description'];
            $keyword = $record['keyword'];
            $image = $record['image'];
            $facility = $record['facility_type'];
            $city = $record['city_id'];
            $suburb = $record['suburb_id'];
            $restcare = $record['restcare_id'];
            $ext_url = $record['ext_url'];
            $youtube_video = $record['youtube_video'];
            $zip = $record['zip'];
            $staff_comment = $record['staff_comment'];
            $management_comment = $record['management_comment'];
            $activities_comment = $record['activity_comment'];
            $staff_image = $record['staff_image'];
            $management_image = $record['management_image'];
            $activities_image = $record['activity_image'];
            $extraInfo1 = $record['extra_info1'];
            $extraInfo2 = $record['extra_info2'];
            $extraInfo3 = $record['extra_info3'];
            $brief = $record['brief'];
            $noOfRooms = $record['no_of_room'];
            $noOfBeds = $record['no_of_beds'];
            $long = $record['longitude'];
            $lat = $record['latitude'];


            if (empty($lat) && empty($long)) {
                $geoData = unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip=' . $IPaddress));
                $lat = $geoData['geoplugin_latitude'];
                $long = $geoData['geoplugin_longitude'];
            }
        }
    }
}

if (isset($submit) && $submit == 'Update') {
    $fields_array = artwork();
    $response = Validation::_initialize($fields_array, $_POST);
    if (isset($response['valid']) && $response['valid'] > 0) {

        $image = $_POST["gImage"];
//            if (isset($_POST['zip']) && !empty($_POST['zip'])) {
//                $val = getLnt($_POST['zip']);
//                $latitude = $val['lat'];
//                $longitude = $val['lng'];
//            }
//        if (isset($_POST['zip']) && !empty($_POST['zip']) && $_POST['geotag'] == 'no') {
//            $val = getLnt($_POST['zip']);
//            $latitude = $val['lat'];
//            $longitude = $val['lng'];
//        } else {
//            $geoData = unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip=' . $IPaddress));
//            $latitude = $geoData['geoplugin_latitude'];
//            $longitude = $geoData['geoplugin_longitude'];
//        }
        $latitude = $_POST['latitude'];
        $longitude = $_POST['longitude'];
        $ImageExt = array("jpg", "png", "jpeg", "gif");

        if ($_FILES['staff_image']['error'] == 0) {
            $ext = end(explode('.', $_FILES['staff_image']['name']));
            if (in_array($ext, $ImageExt)) {
                $file_tmp = $_FILES['staff_image']["tmp_name"];
                $target_pathL = ADMIN_PATH . 'files/product/staff/';
                $stf_image_name = uploadImageVideo($target_pathL, $_FILES['staff_image']['name'], $file_tmp);
            } else {
                $_SESSION['msg'] = "Please select a valid type of Image";
            }
        }
        if ($_FILES['management_image']['error'] == 0) {
            $ext = end(explode('.', $_FILES['management_image']['name']));
            if (in_array($ext, $ImageExt)) {
                $file_tmp = $_FILES['management_image']["tmp_name"];
                $target_pathL = ADMIN_PATH . 'files/product/management/';
                $mng_image_name = uploadImageVideo($target_pathL, $_FILES['management_image']['name'], $file_tmp);
            } else {
                $_SESSION['msg'] = "Please select a valid type of Image";
            }
        }
        if ($_FILES['activities_image']['error'] == 0) {
            $ext = end(explode('.', $_FILES['activities_image']['name']));
            if (in_array($ext, $ImageExt)) {
                $file_tmp = $_FILES['activities_image']["tmp_name"];
                $target_pathL = ADMIN_PATH . 'files/product/activity/';
                $act_image_name = uploadImageVideo($target_pathL, $_FILES['activities_image']['name'], $file_tmp);
            } else {
                $_SESSION['msg'] = "Please select a valid type of Image";
            }
        }
        if ($_FILES['staff_image']['name'] == "") {
            $stf_image_name = $staff_image;
        }
        if ($_FILES['management_image']['name'] == "") {
            $mng_image_name = $management_image;
        }
        if ($_FILES['activities_image']['name'] == "") {
            $act_image_name = $activities_image;
        }
        $fields = array(
            'title' => trim($_POST['title']),
            'description' => trim($_POST['description']),
            'keyword' => trim($_POST['keyword']),
            'city_id' => trim($_POST['city']),
            'suburb_id' => trim($_POST['suburb']),
            'restcare_id' => trim($_POST['restcare']),
            'ext_url' => trim($_POST['ext_url']),
            'youtube_video' => $_POST['youtube_video'],
            'zip' => trim($_POST['zip']),
            'latitude' => $latitude,
            'longitude' => $longitude,
            'modified' => date('Y-m-d h:i:s', time()),
            'staff_comment' => trim($_POST['staff_comment']),
            'staff_image' => $stf_image_name,
            'management_comment' => trim($_POST['management_comment']),
            'management_image' => $mng_image_name,
            'activity_comment' => trim($_POST['activities_comment']),
            'activity_image' => $act_image_name
        );
        $extar_field = array(
            'brief' => trim($_POST['brief']),
            'no_of_room' => trim($_POST['noOfRooms']),
            'no_of_beds' => trim($_POST['noOfBeds']),
            'extra_info1' => trim($_POST['extraInfo1']),
            'extra_info2' => trim($_POST['extraInfo2']),
            'extra_info3' => trim($_POST['extraInfo3']),
            'modified ' => date('Y-m-d h:i:s', time())
        );
        $addFacility = $addMoreFacility;

        $where = "where md5(id)= '$id'";
        $update_result = $db->update(_prefix('products'), $fields, $where);
        if ($update_result) {

            if ($_POST['gImage'] != '') {
//                $imageArray = explode(',', $image);
                //Delete product images from pro_images
                $queryDel = "DELETE FROM " . _prefix("pro_images") . " WHERE product_id = '$pr_id'";
                $delete_service = mysqli_query($db->db_connect_id, $queryDel);
                //Inserting services type ids in pro_services table
                if ($delete_service) {
                    foreach ($image as $imageId) {
                        $image_field = array(
                            'supplier_id' => $useId,
                            'product_id' => $pr_id,
                            'gallery_id' => $imageId,
                            'modified ' => date('Y-m-d h:i:s', time())
                        );
                        // Message for insert
                        $insert_image = $db->insert(_prefix('pro_images'), $image_field);
                    }
                }
            }

            //Delete service type having product_id of updated service/product
            $queryD = "DELETE FROM " . _prefix("pro_services") . " WHERE product_id = '$pr_id'";
            $delete_service = mysqli_query($db->db_connect_id, $queryD);
            //Inserting services type ids in pro_services table
            if ($delete_service) {
                foreach ($_POST['facility_type'] as $servise_id) {
                    $services_field = array(
                        'supplier_id' => $useId,
                        'product_id' => $pr_id,
                        'service_id' => $servise_id,
                        'modified ' => date('Y-m-d h:i:s', time())
                    );
                    // Message for insert
                    $insert_service = $db->insert(_prefix('pro_services'), $services_field);
                }
            }
            $extar_field['product_id'] = $pr_id;
            $extra_where = "where id= '$ex_id'";
            $insert_result_extra = $db->update(_prefix('extra_facility'), $extar_field, $extra_where);
            if ($insert_result_extra) {
                if ($addFacility != "") {
                    $to = ADMIN_EMAIL;
                    $email = emailTemplate('supplier_add_facility');
                    if ($email['subject'] != '') {
                        $message = str_replace(array('{supplier_name}', '{addfacility}'), array($supplier_name, $addFacility), $email['description']);
                        $send = sendmail($to, $email['subject'], $message);
                    }
                }
                $msg = common_message_supplier(1, constant('UPDATE_PRODUCT'));
                $_SESSION['msg'] = $msg;
                //ends here
                redirect_to(HOME_PATH . "supplier/product-manager");
            }
        }
    } else {
        $errors = '';
        foreach ($response as $key => $message) {
            $error = true;
            $errors .= $message . "<br>";
        }
    }
}

function uploadImageVideo($target_pathL, $file, $file_temp) {
    $date = new DateTime();
    $timestamp = $date->getTimestamp();
    $ext = strtolower(end(explode('.', $file)));
    $logoName = $timestamp . '_' . imgNameSanitize(basename($file), 20);
    $target_path = $target_pathL . $logoName;
    move_uploaded_file($file_temp, $target_path);
    if ($type == 0) {
        $target_path_thumb = $target_pathL . 'thumb_' . $logoName;
        ak_img_resize($target_path, $target_path_thumb, 50, 50, $ext);
    }
    return $logoName;
}
?>

<div class="row">
    <div class="col-sm-offset-2 col-sm-6 success">
        <?php if (isset($error)) { ?>
            <div class="errorForm" id="error" style="display: block;">
                <img align="absmiddle" src="<?php echo HOME_PATH . '/images/error.png'; ?>">&nbsp;<?php echo $errors; ?>.<br>
            </div>
            <!--echo '<div style="color:#FF0000">' . $errors . '</div>';-->
            <?php
        }
        if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
            ?>
            <div class="errorForm" id="error" style="display: block;">
                <img align="absmiddle" src="<?php echo HOME_PATH . '/images/error.png'; ?>">&nbsp;<?php echo $_SESSION['msg']; ?>.<br>
            </div>
            <?php
            unset($_SESSION['msg']);
        }
        ?>
    </div>
</div>
<div class="container" style="padding:0px 3% 0">
    <div class="row">
        <div class="dashboard_container">
            <?php require_once 'includes/sp_left_navigation.php'; ?>
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                <div class="dashboard_right_col">
                    <h2 class="hedding_h2"><i class="fa fa-pencil-square-o"></i> <span><?php echo (key($_REQUEST) == 'edit') ? 'Edit' : 'Add'; ?> Service/Product</span></h2>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 thumbnail adbooking_container">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <form name="addLogo" id="addLogo" action="" method="POST" class="form-horizontal form_payment" role="form" enctype="multipart/form-data">
                                    <input type="hidden" id="address" value=""/>
                                    <input type="hidden" id="latitude" name="latitude" value="<?php echo $lat; ?>"/>
                                    <input type="hidden" id="longitude" name="longitude" value="<?php echo $long; ?>" />
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 list_container">
                                            <div class="form-group" style="margin-bottom:6px;">
                                                <label for="title" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration">Product/Service Name <span class="redCol">*</span></label>
                                                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                                    <input id="title" name="title" maxlength="50" class="form-control input_fild_1 required" type="text" value="<?php echo isset($title) ? stripslashes($title) : ''; ?>">
                                                </div>
                                            </div>
                                            <div class="form-group" style="margin-bottom:6px;">
                                                <label for="description" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration">Description</label>
                                                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                                    <textarea rows="4" cols="50" maxlength="500" name="description" class="form-control input_fild_1" id="description"><?php
                                                        if ((isset($description)) && !empty($description)) {
                                                            echo stripslashes($description);
                                                        }
                                                        ?></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group" style="margin-bottom:6px;">
                                                <label for="keyword" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration">Keyword <span class="redCol">*</span></label>
                                                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                                    <input id="keyword" name="keyword" maxlength="20" class="form-control input_fild_1" type="text" value="<?php echo isset($keyword) ? stripslashes($keyword) : ''; ?>">
                                                </div>
                                            </div>
                                            <?php if ($_SESSION['youtubeEnable'] == 1) { ?>
                                                <div class="form-group" style="margin-bottom:6px;">
                                                    <label for="youtube_video" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration">YouTube Video(Embed Code)</label>
                                                    <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                                        <textarea id="youtube_video" name="youtube_video" class="form-control input_fild_1 " type="text"><?php echo isset($youtube_video) ? stripslashes($youtube_video) : ''; ?></textarea>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                            <?php if ($_SESSION['extUrlEnable'] == 1) { ?>
                                                <div class="form-group" style="margin-bottom:6px;">
                                                    <label for="ext_url" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration">External URL</label>
                                                    <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                                        <input id="ext_url" name="ext_url" type="url" class="form-control input_fild_1" type="text" value="<?php echo isset($ext_url) ? stripslashes($ext_url) : ''; ?>">
                                                    </div>
                                                </div>
                                            <?php } ?>
                                            <div class="form-group" style="margin-bottom:6px;">
                                                <label for="facility_type_id" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration">Facility Type <span class="redCol">* </span></label>
                                                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                                    <select id="facility_type_id" style="margin: 8px 0;" class="required form-control select_option_1 pull-left" name="facility_type[]" style="width:235px;" multiple="true"><?php echo serviceCatMultiple($service_ids); ?></select>
                                                </div>
                                            </div>
                                            <script type="text/javascript">
                                                $(document).ready(function() {
                                                    $('#facility_type_id').multiselect();
                                                })
                                            </script>
                                            <div class="form-group" style="margin-bottom:6px;">
                                                <label for="city" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration">City <span class="redCol">*</span></label>
                                                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                                    <select id="city" style="margin: 8px 0;" class="required form-control select_option_1 pull-left" name="city" style="width:235px;"><?php echo cityList($city); ?></select>
                                                </div>
                                            </div>
                                            <div class="form-group" style="margin-bottom:6px;">
                                                <label for="suburb" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration">Suburb</label>
                                                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                                    <select id="suburb" style="margin: 8px 0;" class="form-control select_option_1 pull-left" name="suburb" style="width:235px;"><?php echo suburbList($city, $suburb); ?></select>
                                                </div>
                                            </div>
<!--                                            <div class="form-group" style="margin-bottom:6px;">
                                                <label for="restcare" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration">Rest Care</label>
                                                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                                    <select id="restcare" style="margin: 8px 0;" class="form-control select_option_1 pull-left" name="restcare" style="width:235px;"><?php echo restCareList($suburb, $restcare); ?></select>
                                                </div>
                                            </div>-->
                                            <?php if ($_SESSION['facilityEnable'] == 1) { ?>

                                                <div class="form-group"  style="margin-bottom:6px;">
                                                    <label for="staff" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration">Facility Description </label>
                                                    <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 thumbnail">
                                                        <div class="col-md-12">
                                                            <label for="brief" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list ">Brief Description</label>
                                                            <textarea  name="brief" cols="50" maxlength="500" class="form-control input_fild_1"><?php
                                                                if ((isset($brief)) && !empty($brief)) {
                                                                    echo stripslashes($brief);
                                                                }
                                                                ?></textarea>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <label for="noOfRooms" class="col-lg-4 col-md-2 col-sm-10 col-xs-12 label_list text-left duration">No of Dwellings</label>
                                                            <div class="col-lg-7 col-md-8 col-sm-12 col-xs-12">
                                                                <input id="noOfRooms" name="noOfRooms" maxlength="5" class="form-control input_fild_1  " type="text" value="<?php echo isset($noOfRooms) ? stripslashes($noOfRooms) : ''; ?>">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <label for="noOfBeds" class="col-lg-4 col-md-2 col-sm-10 col-xs-12 label_list text-left duration">No of Beds</label>
                                                            <div class="col-lg-7 col-md-8 col-sm-12 col-xs-12">
                                                                <input id="noOfBeds" name="noOfBeds" maxlength="5" class="form-control input_fild_1  " type="text" value="<?php echo isset($noOfBeds) ? stripslashes($noOfBeds) : ''; ?>">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <label for="extraInfo1" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list ">Extra Info1</label>
                                                            <textarea id="extraInfo1"  name="extraInfo1" cols="50" maxlength="500" class="form-control input_fild_1"><?php
                                                                if ((isset($extraInfo1)) && !empty($extraInfo1)) {
                                                                    echo stripslashes($extraInfo1);
                                                                }
                                                                ?></textarea>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <label for="extraInfo2" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list ">Extra Info2</label>
                                                            <textarea id="extraInfo2"  name="extraInfo2" cols="50" maxlength="500" class="form-control input_fild_1"><?php
                                                                if ((isset($extraInfo2)) && !empty($extraInfo2)) {
                                                                    echo stripslashes($extraInfo2);
                                                                }
                                                                ?></textarea>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <label for="extraInfo3" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list ">Extra Info3</label>
                                                            <textarea id="extraInfo3"  name="extraInfo3" cols="50" maxlength="500" class="form-control input_fild_1"><?php
                                                                if ((isset($extraInfo1)) && !empty($extraInfo1)) {
                                                                    echo stripslashes($extraInfo1);
                                                                }
                                                                ?></textarea>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <label for="addMoreFacility" class="col-lg-4 col-md-2 col-sm-10 col-xs-12 label_list text-left duration">Add More Facility</label>
                                                            <div class="col-lg-7 col-md-8 col-sm-12 col-xs-12">
                                                                <input id="addMoreFacility" name="addMoreFacility" maxlength="150" class="form-control input_fild_1  " type="text" value="<?php echo isset($addMoreFacility) ? stripslashes($addMoreFacility) : ''; ?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                            <?php if ($_SESSION['staffEnable'] == 1) { ?>
                                                <div class="form-group"  style="margin-bottom:6px;">
                                                    <label for="staff" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration">Staff </label>
                                                    <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 thumbnail">
                                                        <div class="col-md-12">
                                                            <label for="comment" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list ">Comment</label>
                                                            <textarea  name="staff_comment" cols="50" maxlength="500" class="form-control input_fild_1"><?php
                                                                if ((isset($staff_comment)) && !empty($staff_comment)) {
                                                                    echo stripslashes($staff_comment);
                                                                }
                                                                ?></textarea>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <label for="image" class="col-lg-4 col-md-2 col-sm-10 col-xs-12 label_list text-left duration">Image</label>
                                                            <div class="col-lg-7 col-md-8 col-sm-12 col-xs-12">
                                                                <input name="staff_image" id="image" class="form-control_browse input_fild_img_browse" type="file">
                                                            </div>
                                                            <div class="col-md-12 text-center ">
                                                                <?php if (file_exists(DOCUMENT_PATH . 'admin/files/product/staff/' . $staff_image) && $staff_image != '' && key($_REQUEST) == 'edit') { ?>
                                                                    <img width="100px;" height="100px;" title="Staff Image" src="<?php echo MAIN_PATH . '/files/product/staff/' . $staff_image ?>"/>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php } if ($_SESSION['managementEnable'] == 1) { ?>
                                                <div class="form-group"  style="margin-bottom:6px;">
                                                    <label for="management" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration">Management </label>
                                                    <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 thumbnail">
                                                        <div class="col-md-12">
                                                            <label for="comment" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list ">Comment</label>
                                                            <textarea name="management_comment" cols="50" maxlength="500" class="form-control input_fild_1">
                                                                <?php
                                                                if ((isset($management_comment)) && !empty($management_comment)) {
                                                                    echo stripslashes($management_comment);
                                                                }
                                                                ?>
                                                            </textarea>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <label for="image" class="col-lg-4 col-md-2 col-sm-10 col-xs-12 label_list text-left duration">Image</label>
                                                            <div class="col-lg-7 col-md-8 col-sm-12 col-xs-12">
                                                                <input name="management_image" id="image" class="form-control_browse input_fild_img_browse" type="file">
                                                            </div>
                                                            <div class="col-md-12 text-center ">
                                                                <?php if (file_exists(DOCUMENT_PATH . 'admin/files/product/management/' . $management_image) && $management_image != '' && key($_REQUEST) == 'edit') { ?>
                                                                    <img width="100px;" height="100px;" title="Management Image" src="<?php echo MAIN_PATH . '/files/product/management/' . $management_image ?>"/>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php }if ($_SESSION['activityEnable'] == 1) { ?>
                                                <div class="form-group"  style="margin-bottom:6px;">
                                                    <label for="activities" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration">Activities </label>
                                                    <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 thumbnail">
                                                        <div class="col-md-12">
                                                            <label for="comment" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list ">Comment</label>
                                                            <textarea  name="activities_comment" cols="50" maxlength="500" class="form-control input_fild_1"><?php
                                                                if ((isset($activities_comment)) && !empty($activities_comment)) {
                                                                    echo stripslashes($activities_comment);
                                                                }
                                                                ?></textarea>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <label for="image" class="col-lg-4 col-md-2 col-sm-10 col-xs-12 label_list text-left duration">Image</label>
                                                            <div class="col-lg-7 col-md-8 col-sm-12 col-xs-12">
                                                                <input name="activities_image" id="image" class="form-control_browse input_fild_img_browse" type="file">
                                                            </div>
                                                            <div class="col-md-12 text-center ">
                                                                <?php if (file_exists(DOCUMENT_PATH . 'admin/files/product/activity/' . $activities_image) && $activities_image != '' && key($_REQUEST) == 'edit') { ?>
                                                                    <img width="100px;" height="100px;" title="Activity Image" src="<?php echo MAIN_PATH . '/files/product/activity/' . $activities_image ?>"/>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                            <div class="form-group" id="zipid" style="margin-bottom:6px;">
                                                <label for="zip" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration">Zip Code <span class="redCol">*</span></label>
                                                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                                    <input id="zip" name="zip" maxlength="4" class="form-control input_fild_1 required number" type="text" value="<?php echo isset($zip) ? stripslashes($zip) : ''; ?>">
                                                </div>
                                            </div>
                                            <div class="form-group" style="margin-bottom:6px;">
                                                <label   class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration">Geographical Presence </label>
                                                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                        <span class="pull-left right_td"><div id="map" style="width:350px;height:200px; margin-top:10px;"></div></span>
                                                    </div>
                                                </div>
                                            </div>


                                            <!--   <?php if (file_exists(DOCUMENT_PATH . 'admin/files/product/' . $image) && $image != '' && key($_REQUEST) == 'edit') { ?>
                                                                                                                                                                                                            <img width="100px;" height="100px;" title="feature" src="<?php echo MAIN_PATH . '/files/product/' . $image ?>"/>
                                            <?php } ?>
                                           </div>
                                       </div>-->
                                            <div class="form-group" style="margin-bottom:6px;">
                                                <label for="" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration">Upload Gallery image/video </label>
                                                <div id="hiddenField">
                                                    <input type="hidden" id="test" name="image">
                                                </div>
                                                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                                    <a href="<?php echo HOME_PATH; ?>popupfront.php?gImage&id=<?php echo $useId; ?>" id="openImage" style="color:#428BCA;" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration">Browse</a>
                                                </div>
                                                <div class="col-md-12 text-center ">
                                                    <?php foreach ($dataslider AS $gImage) { ?>
                                                        <?php if ($gImage['type'] == 0 && $gImage['file'] != '') { ?>
                                                            <img style="display:inline-block;width:100px;height:100px;"  title="<?php echo $gImage['title']; ?>" src="<?php echo MAIN_PATH . '/files/gallery/images/' . $gImage['file'] ?>"/>
                                                        <?php } else if ($gImage['type'] == 1) { ?>
                                                            <!--<img width="100px;" height="100px;" title="<?php echo $gImage['title']; ?>" src="<?php // echo MAIN_PATH . '/files/gallery/videos/video-thumb.png' . $gImage['file']          ?>"/>-->
                                                            <a style="display:inline-block;" class="videoPlayer" href="<?php echo HOME_PATH . 'modules/gallery/play_video.php?name=' . $gImage['title']; ?>">
                                                                <img style="width:100px;height:100px;"  title="<?php echo $gImage['title']; ?>" src="<?php echo MAIN_PATH . '/files/gallery/videos/video-thumb.png'; ?>"/>
                                                            </a>
                                                        <?php } ?>
                                                    <?php } ?>

                                                    <?php if (file_exists(DOCUMENT_PATH . 'admin/files/gallery/images/' . $image) && $image != '' && key($_REQUEST) == 'edit') { ?>
                                                        <img width="100px;" height="100px;" title="Image" src="<?php echo MAIN_PATH . '/files/gallery/images/' . $image ?>"/>
                                                    <?php } ?>
                                                </div>
                                            </div>


                                            <div class="form-group text-center total_bg" style="width:93%; padding:2% 0 0 0;">
                                                <div class="form-group text-center total_bg" style="width:93%; padding:2% 0 0 0;">
                                                    <?php if (key($_REQUEST) == 'edit') { ?>
                                                        <button type="submit" value="Update" name="submit" id="submit" class="btn btn-danger center-block btn_search btn_pay pull-right" style=" padding:6px 20px 6px; margin-bottom:2%;">UPDATE</button>

                                                    <?php } else { ?>
                                                        <button type="submit" value="Submit" name="submit" id="submit" class="btn btn-danger center-block btn_search btn_pay pull-right" style=" padding:6px 20px 6px; margin-bottom:2%;">SUBMIT</button>

                                                    <?php } ?>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>

                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<style>
    th,td,tr{border:0px!important; }
    .input{background-color:white;color: black;}
    label.error{
        color:red;
        font-size: 10px;

    }
    select.error{
        color:#000;
    }
    input.error{
        color:#000;}
    .redCol{
        color:red;
    }

</style>
<script type="text/javascript">
    $('#expectDate').datepicker({
        dateFormat: 'dd/mm/yy',
        changeMonth: true,
        changeYear: true,
        yearRange: '2010',
    });
    $(document).ready(function() {

        $(".videoPlayer").fancybox({
            'transitionIn': 'elastic',
            'transitionOut': 'elastic',
            'speedIn': 600,
            'speedOut': 200
        });
        $('.youtube').fancybox({
            openEffect: 'none',
            closeEffect: 'none',
            prevEffect: 'none',
            nextEffect: 'none',
            arrows: false,
            helpers: {
                media: {},
                buttons: {}
            }
        });
        $(".fancybox").fancybox();
        $("#openImage").fancybox({
            width: 400, // or whatever
            height: 400,
            autoSize: false
        });

        $('#city').change(function() {
            var cityId = $('#city').val();
            var path = '<?php echo HOME_PATH; ?>';
//            alert(cityId);
//            alert(path);
            $.ajax({
                url: path + 'ajaxFront.php?' + new Date().getTime(),
                data: 'action=getSuburb&cityId=' + cityId,
                type: "POST",
                cache: false,
                success: function(response) {
                    try {
                        $('#suburb').html(response);
                    } catch (e) {
                    }
                }
            })
        });
        $('#addLogo').validate({
            rules: {
                staff_image: {
                    accept: 'jpg,png,jpeg,gif'

                },
                management_image: {
                    accept: 'jpg,png,jpeg,gif'

                },
                activities_image: {
                    accept: 'jpg,png,jpeg,gif'

                }

            },
            messages: {
                staff_image: {
                    accept: "Image type jpg/png/jpeg/gif is allowed"
                },
                management_image: {
                    accept: "Image type jpg/png/jpeg/gif is allowed"
                },
                activities_image: {
                    accept: "Image type jpg/png/jpeg/gif is allowed"
                }
            },
            submitHandler: function(form) {
                $('#submit').attr('disabled', 'disabled');
                form.submit();

            }


        });
        $('#suburb').change(function() {
            var suburbId = $('#suburb').val();
            var path = '<?php echo HOME_PATH; ?>';
            $.ajax({
                url: path + 'ajaxFront.php?' + new Date().getTime(),
                data: 'action=getRestCare&suburbId=' + suburbId,
                type: "POST",
                cache: false,
                success: function(response) {
                    try {
                        $('#restcare').html(response);
                    } catch (e) {
                    }
                }
            })
        });
        $('.geotag').click(function() {
            var geotag = $(this).val();
            if (geotag == 'yes') {
                $('#zipid').hide();
                $('#zip').removeClass('required');
            }
            if (geotag == 'no') {
                $('#zipid').show();
                $('#zip').addClass('required');
            }
        });

    });
//<?php if (file_exists(DOCUMENT_PATH . 'admin/files/product/' . $image) && $image != '' && key($_REQUEST) == 'edit') { ?>
        $('#image').removeClass('required');
        //<?php } ?>
</script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
<script type="text/javascript">
    var map;
    var marker;
    var lat = <?php echo $lat; ?>;
    var lang = <?php echo $long; ?>;
    var myLatlng = new google.maps.LatLng(lat, lang);
    var geocoder = new google.maps.Geocoder();
    var infowindow = new google.maps.InfoWindow();
    function initialize() {
        var mapOptions = {
            zoom: 5,
            center: myLatlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        map = new google.maps.Map(document.getElementById("map"), mapOptions);
        marker = new google.maps.Marker({
            map: map,
            position: myLatlng,
            draggable: true
        });
        geocoder.geocode({'latLng': myLatlng}, function(results, status) {
            console.log(results);
            if (status == google.maps.GeocoderStatus.OK) {
                if (results[0]) {
                    $('#address').val(results[0].formatted_address);
                    $('#latitude').val(marker.getPosition().lat());
                    $('#longitude').val(marker.getPosition().lng());
                    infowindow.setContent(results[0].formatted_address);
                    infowindow.open(map, marker);
                }
            }
        });
        google.maps.event.addListener(marker, 'dragend', function() {

            geocoder.geocode({'latLng': marker.getPosition()}, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        $('#address').val(results[0].formatted_address);
                        $('#latitude').val(marker.getPosition().lat());
                        $('#longitude').val(marker.getPosition().lng());
                        var latlng = new google.maps.LatLng($('#latitude').val(), $('#longitude').val());
                        geocoder = new google.maps.Geocoder();
                        geocoder.geocode({'latLng': latlng}, function(results, status) {
                            if (status == google.maps.GeocoderStatus.OK) {
                                if (results[0]) {
                                    for (j = 0; j < results[0].address_components.length; j++) {
                                        if (results[0].address_components[j].types[0] == 'postal_code') {
                                            $('#zip').val(results[0].address_components[j].short_name);
                                        }
                                    }
                                }
                            }
                        });
                        infowindow.setContent(results[0].formatted_address);
                        infowindow.open(map, marker);
                    }
                }
            });
        });
    }

    google.maps.event.addDomListener(window, 'load', initialize);


</script>

