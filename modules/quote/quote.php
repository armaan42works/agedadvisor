<?php
    /*
     * Objective  : Client can request quote after login
     * Filename :quote.php
     * Created By : Yogesh Chandra Joshi <yogesh.joshi@ilmp-tech.com> 
     * Created On : 27 August 2014
     */
?>
<?php
    $recId = '';
    if (isset($pathInfo['call_parts']['2']) && $pathInfo['call_parts']['2'] != '') {
        $recId = $pathInfo['call_parts']['2'];
        $getQuoteDetail = "SELECT dq.id ,dq.project_desc, dq.file_name, dq.unique_id, dq.status, dq.coupon_id, daq.quotre_desc, daq.subtotal, "
                . "daq.total, dc.expire_date, dq.payment_status, daq.filename, dq.created "
                . "FROM " . _prefix('quotes') . " as dq "
                . "LEFT JOIN " . _prefix('admin_quotes') . " as daq ON daq.quotes_id = dq.id "
                . "LEFT JOIN " . _prefix('coupons') . " as dc ON dc.code = dq.coupon_id "
                . "WHERE md5(dq.id) = '$recId'";
        $resQuoteDetail = $db->sql_query($getQuoteDetail);
        $dataQuoteDetail = $db->sql_fetchrow($resQuoteDetail);
        $type = $dataQuoteDetail['status'] == 0 ? 'Q' : 'P';
    }

    if (isset($submit) && $submit == 'Submit') {
        $url = HOME_PATH . 'user/dashboard';

        if ($_FILES['uploadfile']['error'] == 0) {
            $target_path = ADMIN_PATH . 'files/quote_files/';
            $date = new DateTime();
            $timestamp = $date->getTimestamp();
            $fileName = $timestamp . '_' . basename($_FILES['uploadfile']['name']);
            $target_path = $target_path . $fileName;
        }
        $uniqueId = uniqueId(9);
        $fields = array(
            'client_id' => $_SESSION['userId'],
            'coupon_id' => $coupon_gift,
            'project_desc' => $description,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'validate' => '1',
            'unique_id' => $uniqueId);
        if (isset($_FILES['uploadfile']['name'])) {
            $fields['file_name'] = $fileName;
        }

        $insert_result = $db->insert(_prefix('quotes'), $fields);
        if ($insert_result) {
            if (isset($_FILES['uploadfile']['name'])) {
                move_uploaded_file($_FILES['uploadfile']["tmp_name"], $target_path);
                redirect_to($url);
            }
        }
    }

    if (isset($update) && $update == 'Update') {
        $url = HOME_PATH . 'user/dashboard';

        if ($_FILES['uploadfile']['error'] == 0) {
            $target_path = ADMIN_PATH . 'files/quote_files/';
            $date = new DateTime();
            $timestamp = $date->getTimestamp();
            $fileName = $timestamp . '_' . basename($_FILES['uploadfile']['name']);
            $target_path = $target_path . $fileName;
        }
        $fields = array(
            'project_desc' => $description,
            'ip_address' => $_SERVER['REMOTE_ADDR']
        );
        if (isset($_FILES['uploadfile']['name'])) {
            $fields['file_name'] = $fileName;
        }
        $where = " WHERE md5(id) = '$recId'";
        $insert_result = $db->update(_prefix('quotes'), $fields, $where);
        if ($insert_result) {
            if (isset($_FILES['uploadfile']['name'])) {
                move_uploaded_file($_FILES['uploadfile']["tmp_name"], $target_path);
            }
            redirect_to($url);
        }
    }
?>

<section  class="Dashbord_main">
    <ul class="breadcrumb">
        <li><a href="<?php echo HOME_PATH ?>user"><strong>HOME</strong></a></li>
        <li class="last"><a href="javascript:void(0);">Upload Project</a></li>
    </ul>
    <div class="messag_send"></div>
    <div class="message_container">
        <div class="login_container chang_pass_main">
            <div><?php
                    if (isset($_SESSION['msg'])) {
                        echo $_SESSION['msg'];
                        unset($_SESSION['msg']);
                    }
                ?></div>
            <?php
                $hide = '';
                $commonHide = 'display:none;';
                $sql_query_name = "SELECT name, business, country_id FROM " . _prefix('users') . " WHERE id='{$_SESSION['userId']}'";
                $resName = $db->sql_query($sql_query_name);
                $data = $db->sql_fetchrow($resName);
                if ($data['country_id'] != 229) {
                    $hide = "display:none";
                }
            ?>
            <form id="quoteForm" name="quoteForm" method="post" class="login_account request_form" enctype="multipart/form-data"> 
                <div class="col">
                    <label class="label_text">Project By :  </label> &nbsp; <?php echo $data['name']; ?>
                </div>
                <div class="col">
                    <label class="label_text">Company :  </label> &nbsp; <?php echo $data['business']; ?>
                </div>
                <?php
                    if (isset($recId) && $recId != '') {
                        ?>
                        <div class="col">
                            <label class="label_text">Project ID :  </label> &nbsp; <?php echo $type . $dataQuoteDetail['unique_id']; ?>
                        </div>
                        <div class="col">
                            <label class="label_text">Created On :  </label> &nbsp; <?php echo date('M d, Y', strtotime($dataQuoteDetail['created'])); ?>
                        </div>
                        <?php
                    }
                ?>
                <div class="col">
                    <label class="label_text">Project Description or Managerial Problem <span class="redCol">* </span> </label>
                    <textarea name="description" id="description" cols="100" rows="8" class="required textarea input_login input_width" ><?php echo stripslashes($dataQuoteDetail['project_desc']) ?></textarea>
                </div>
                <?php
                    if (isset($recId) && $recId != '') {
                        if (isset($dataQuoteDetail['file_name']) && $dataQuoteDetail['file_name'] != '' && file_exists(DOCUMENT_PATH . 'admin/files/quote_files/' . $dataQuoteDetail['file_name'])) {
                            ?>
                            <div class="col email showRecord" style="margin-top:20px;">
                                <label class="label_text">Uploaded Document : </label>
                                <span><a href='<?php echo HOME_PATH_URL ?>download.php?type=quote_files&file=<?php echo $dataQuoteDetail['file_name'] ?>'>Document</a></span>
                                <span><a href='javascript:void(0);' class='file_delete' id='<?php echo $dataQuoteDetail['id']; ?>'><img src="<?php echo ADMIN_IMAGE ?>li_delete.png" title="Delete Document" ></a></span>
                            </div>
                            <?php
                        } else {
                            ?>
                            <div class="col email showRecord" style="margin-top:20px;">
                                <label for="" class="label_text">Upload a file <br> (<span> Only word, excel, ppt or pdf file format is accepted
                                    </span>)</label>
                                <input type="file" id="uploadfile" name="uploadfile" style="overflow: hidden; text-overflow:ellipsis; white-space:nowrap; width: 100%;">
                            </div>
                            <?php
                        }
                        ?>
                        <div class="col">
                            <label class="label_text">Coupon/ Gift Certification Code :  </label> &nbsp; <?php echo $dataQuoteDetail['coupon_id'] != '' ? $dataQuoteDetail['coupon_id'] : 'N/A'; ?>
                        </div>
                        <div class="col">
                            <h3>Admin Quote</h3>
                        </div>
                        <div class="col">
                            <label class="label_text">Admin Quote :  </label> &nbsp; <?php echo $dataQuoteDetail['quotre_desc'] != '' ? $dataQuoteDetail['quotre_desc'] : 'N/A'; ?>
                        </div>
                        <?php
                        if (isset($dataQuoteDetail['filename']) && $dataQuoteDetail['filename'] != '' && file_exists(DOCUMENT_PATH . 'admin/files/' . $dataQuoteDetail['filename'])) {
                            ?>
                            <div class="col email showRecord" style="margin-top:20px;">
                                <label class="label_text">Uploaded Document : </label>
                                <a href='<?php echo HOME_PATH_URL ?>download.php?file=<?php echo $dataQuoteDetail['filename'] ?>'>Document</a>
                            </div>
                            <?php
                        }
                        ?>
                        <div class="col">
                            <label class="label_text">Subtotal :  </label> &nbsp; <?php echo $dataQuoteDetail['subtotal'] != '' ? '$' . $dataQuoteDetail['subtotal'] : 'N/A'; ?>
                        </div>
                        <div class="col">
                            <label class="label_text">Total :  </label> &nbsp; <?php echo $dataQuoteDetail['total'] != '' ? '$' . $dataQuoteDetail['total'] : 'N/A'; ?>
                        </div>
                        <div class="col">
                            <label class="label_text">Payment Status :  </label> &nbsp; <?php echo $dataQuoteDetail['payment_status'] == 0 ? 'Unpaid' : 'Paid'; ?>
                        </div>
                        <?php
                    } else {
                        ?>
                        <div class="col refferal cp_client_refferal" style="margin-top:20px;">
                            <label for="" class="label_text">Do you have a data/spreadsheet to upload ?</label>
                            <span class="radiobtn_area">
                                <input type="radio" name="data" id="0" value="0" class="input_text data" >
                                <label for="0" class="label_text">Yes</label>
                            </span>
                            <span class="radiobtn_area">
                                <input type="radio" name="data" id="1" value="1" class="input_text data" checked="checked">
                                <label for="1" class="label_text">No</label>
                            </span>
                        </div>

                        <?php if ($data['country_id'] == 229) {
                            ?>
                            <div class="col showRecord" style="margin-top:20px;<?php echo $commonHide; ?>">
                                <label for="" class="label_text">How would you like to submit your project data ?</label>
                                <span class="radiobtn_area">
                                    <input type="radio" name="submit_data" id="0" value="0" class="input_text submit_data" checked="checked">
                                    <label for="0" class="label_text">Electronically</label>
                                </span>
                                <span class="radiobtn_area">
                                    <input type="radio" name="submit_data" id="1" value="1" class="input_text submit_data">
                                    <label for="1" class="label_text">By Mail</label>
                                </span>
                            </div>
                            <div class="col showRecord" style="<?php echo $commonHide ?>">
                                <label class="label_text"><strong>Electronically</strong> <br>
                                    We respect your privacy and will safeguard your data.<br>
                                    All data is securely stored using 1024-bit encryption and SSL protection.
                                </label>
                            </div> 
                            <div class="col showRecord"  style="<?php echo $commonHide ?>">
                                <label class="label_text"><strong>By Mail</strong> <br>
                                    We recommend you use a traceable shipping method either by UPS, FdEx or US priority mail to ensure mail deliver.<br>
                                    We are not responsible for any lost or undelivered mail.
                                </label>
                            </div>
                        <?php }
                        ?>
                        <div class="col email showRecord" style="margin-top:20px;<?php echo $commonHide ?>">
                            <label for="" class="label_text">Upload a file <span class="redCol">* </span> <br> (<span> Only word, excel, ppt or pdf file format is accepted
                                </span>)</label>
                            <input type="file" id="uploadfile" name="uploadfile" style="overflow: hidden; text-overflow:ellipsis; white-space:nowrap; width: 100%;">
                        </div>
                        <?php
                        $sql_query = "SELECT address FROM " . _prefix("address") . " where id='1'";
                        $res = $db->sql_query($sql_query);
                        $records = $db->sql_fetchrowset($res);
                        $address = $records[0]['address'];
                        ?>
                        <div class="col mail" style="margin-top:20px;<?php echo $commonHide ?>">
                            <label for="" class="label_text"><?php echo $address; ?></label></div>
                        <div class="col">
                            <label class="label_text">Enter Coupon or Gift Certification Code</label> 
                            <input type="text" name="coupon_gift" id="coupon_gift"  class="input_width input_login" maxlength="50" size="50" value="" /> 

                        </div> 
                    <?php }
                ?>



                <div class="col">
                    <?php if (isset($recId) && $recId != '') {
                            ?>
                            <input type="submit" value="Update" name="update"class="creat_account_btn" >
                            <?php
                        } else {
                            ?>
                            <input type="submit" value="Submit" name="submit"class="creat_account_btn" >
                        <?php }
                    ?>

                </div>
            </form>
        </div>
    </div>
</section>


</section>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('#quoteForm').validate({
            rules: {
                coupon_gift: {
                    remote: {
                        url: '<?php echo HOME_PATH ?>admin/ajax.php',
                        type: 'POST',
                        data: {action: 'couponExist', type: '1', promocode: function() {
                                return $('#coupon_gift').val();
                            }
                        }
                    }
                }
            },
            messages: {
                coupon_gift: {
                    remote: "Coupon or Gift Certification Code entered not valid"
                }
            }

        });
        $('.data').click(function() {
            var data = $(this).val();
            if (data == 0) {
                $('.showRecord').show();
                $('input[name="uploadfile"]').rules('remove');
                $('input[name="uploadfile"]').rules('add', {
                    "extension": "doc|docx|xls|xlsx|ppt|pdf",
                    "required": true
                });
            } else {
                $('input[name="uploadfile"]').rules('remove');
                $('.showRecord').hide();
            }
        });
        $('.submit_data').click(function() {
            var sendType = $(this).val();
            if (sendType == 0) {
                $('.email').show();
                $('.mail').hide();
                $('input[name="uploadfile"]').rules('remove');
                $('input[name="uploadfile"]').rules('add', {
                    "extension": "doc|docx|xls|xlsx|ppt|pdf",
                    "required": true
                });
            } else {
                $('.email').hide();
                $('.mail').show();
                $('input[name="uploadfile"]').rules('remove');
            }
        });

        $('.file_delete').click(function() {
            var sure = confirm("Do you really want to delete this file?");
            if (sure) {
                if (sure) {
                    var id = $(this).attr('id');
                    $.ajax({
                        type: 'POST',
                        url: '<?php echo HOME_PATH ?>admin/ajax.php',
                        data: {action: 'deleteFile', id: id},
                        success: function(response) {
                            if (parseInt(response) == 1) {
                                window.location.reload();
                            }
                        }
                    });
                }
            }
        });
    });

</script>
<!----Dashbord_main END--------------------->