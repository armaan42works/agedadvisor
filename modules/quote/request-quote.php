<?php
    /*
     * Objective: Non-Registered Clients cans end there quotes to admin
     * filename : request-quote.php
     * Created By: Sanket Khanna <sanket.khanna@ilmp-tech.com>
     * Created On : 11 August 2014
     * Modified On : 01 September 2014
     */
    global $db;
    if (isset($_POST['submit_quote']) && $submit_quote == 'requestQuote') {
        $password = Random_Password(9);
        $fields1 = array(
            'name' => $name,
            'business' => $business,
            'industry' => $industry,
            'website' => $website,
            'phone' => $phone,
            'sms_alert' => $sms,
            'country_id' => $c_country,
            'state_id' => $c_state,
            'email' => $email,
            'user_type' => '0',
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'validate' => '0',
            'password' => $password
        );
        if ($_POST['sms'] == 0) {
            $fields1['country_code'] = $countryCode;
            $fields1['cellphone'] = $cellphone;
        } else {
            $fields1['country_code'] = "";
            $fields1['cellphone'] = "";
        }

        $insert_users = $db->insert(_prefix('users'), $fields1);

        $client_id = mysqli_insert_id($db->db_connect_id);

        if (isset($_FILES['uploadfile']['name'])) {


            if ($_FILES['uploadfile']['error'] == 0) {

                $target_path = ADMIN_PATH . 'files/quote_files/';
                $date = new DateTime();
                $timestamp = $date->getTimestamp();
                $fileName = $timestamp . '_' . basename($_FILES['uploadfile']['name']);
                $target_path = $target_path . $fileName;
                move_uploaded_file($_FILES['uploadfile']["tmp_name"], $target_path);
                $_POST['file_name'] = $fileName;
            }
        }


        $field2 = array(
            'service_id' => $service,
            'other' => $other_services,
            'data_analyze' => $data_analyze,
            'datasize' => $filesize,
            'project_desc' => $description,
            'validate' => '0',
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'client_id' => $client_id
        );
        if ($_POST['file_name'] != "") {
            $field2['file_name'] = $_POST['file_name'];
        }

        if ($_POST['budget'] == 0) {
            $field2['min_budget'] = $min_amount;
            $field2['max_budget'] = "";
        } elseif ($_POST['budget'] == 1) {
            $field2['min_budget'] = $min_amount;
            $field2['max_budget'] = $max_amount;
        } elseif ($_POST['budget'] == 2) {
            $field2['min_budget'] = "";
            $field2['max_budget'] = $max_amount;
        }

        if ($_POST['promo'] == 0) {
            $field2['coupon_id'] = $promocode;
        } elseif ($_POST['budget'] == 1) {
            $field2['coupon_id'] = $promocode;
        } elseif ($_POST['budget'] == 3) {

            $field2['coupon_id'] = "";
        }

        $insert_quotes = $db->insert(_prefix('quotes'), $field2);
       $id = mysqli_insert_id($db->db_connect_id);
        if ($insert_users && $insert_quotes) {
            $c_email = $email;
            $url = '<a href="' . HOME_PATH . 'validate/' . md5($id) . '"> Click Here</a>';
            $email = emailTemplate('validate_account');
            if ($email['subject'] != '') {
                $message = str_replace(array('{url}', '{name}'), array($url, $name), $email['description']);
                $send = sendmail($c_email, $email['subject'], $message);
                if ($send) {
                    $mysql_query = "UPDATE " . _prefix("users") . " SET email_send = '1' WHERE id = " . mysqli_insert_id($db->db_connect_id);
                    $db->sql_query($mysql_query);
                    $_SESSION['registerName'] = $name;
                    $_SESSION['registerEmail'] = $c_email;
                    $url = HOME_PATH . 'thank-you';
                    redirect_to($url);
                }
            }
        }
    }
?>
<section id="slider_container" class="request_quteo_slider">
    <div class="slider_bg">
        <div class="request_quteo_center">
            <h1> Request a <strong>free quote</strong></h1>
            <p>It’s quick, simple and free. We are committed to establishing long-term business relationships built on results.</p>

        </div>
        <div class="account_withUs">
            <div class="account_withUs_text">Do you have an account with us? </div> 
            <button type="button" class="account_WithUs_btn1" onclick="window.location.href = '<?php echo HOME_PATH . 'login' ?>'">YES</button>
            <button type="button" class="account_WithUs_btn2" id="noAcc">NO</button>
        </div>
    </div>
</section>

<section id="body_container">
    <div class="wrapper">

        <div class="main_box" id="scrollDiv">
            <!----left container STARTS------>
            <div class="left_container">
                <div class="login_container quote_container">
                    <h3>Request a free quote</h3>
                    <div class="clientView">
                        <form id="requestQuote" name="requestQuote" method="post" class="request_form" enctype="multipart/form-data">
                            <div class="left_col">
                                <div class="col">
                                    <label for="" class="label_text">Business Name <span style="color:#CC0000">*</span></label>
                                    <input type="text" name="business" id="business" class="input_text required">
                                </div>
                                <div class="col">
                                    <label for=""  class="label_text">Website</label>
                                    <input type="text" name="website" id="website" class="input_text">
                                </div>
                            </div>
                            <div class="right_col">
                                <div class="col">
                                    <label for="" class="label_text">Industry</label>
                                    <input type="text" name="industry" id="industry" class="input_text">
                                </div>
                            </div>
                            <div class="form_separator"></div>
                            <div class="left_col">
                                <div class="col">

                                    <label for="" class="label_text">Service you would like us to provide<br>
                                        your business<span style="color:#CC0000">*</span></label>
                                    <select name="service" id="service" class="input_text required">
                                        <?php
                                            // list of services
                                            $getServices = "SELECT id,name FROM " . _prefix("services") . " WHERE status = 1 && deleted = 0 ORDER BY order_by ASC";
                                            $resGetServices = $db->sql_query($getServices);
                                            $option = '<option value="">----------------------Select--------------------------</option>';
                                            $rows = $db->sql_fetchrowset($resGetServices);
                                            if (!empty($rows)) {
                                                foreach ($rows as $row) {
                                                    $option .="<option value='{$row['id']}'>{$row['name']}</option>";
                                                }
                                            }
                                            $option .='<option value="0">Other</option>';
                                            echo $option;
                                        ?>
                                    </select>
                                </div>
                                <div class="col other" style="display:none;">

                                    <input type="text" name="other_services" id="other_services" class="input_text">
                                </div>
                                <div class="col" style="margin-top:20px;">
                                    <label for="" class="label_text">Does your project include data to be analyzed?<span style="color:#CC0000">*</span></label>
                                    <span class="radiobtn_area">
                                        <input type="radio" name="data_analyze" id="yes" value="0"  class="input_text dataanalyze">
                                        <label for="yes" class="label_text">Yes</label>
                                    </span>
                                    <span class="radiobtn_area">
                                        <input type="radio" name="data_analyze" id="no" value="1"  class="input_text dataanalyze" checked="checked">
                                        <label for="no" class="label_text">No</label>
                                    </span>
                                </div>
                                <div class="col filesize" style="display:none;">
                                    <label for="" class="label_text">What is your file size?<span style="color:#CC0000">*</span></label>
                                    <div class="col_small">
                                        <span class="radiobtn_area">
                                            <input type="radio" name="filesize" id="small" value="0" class="input_text">
                                            <label for="small" class="label_text">Small</label>
                                        </span>
                                        <span class="radiobtn_area">
                                            <input type="radio" name="filesize" id="medium" value="1" class="input_text">
                                            <label for="medium" class="label_text">Medium</label>
                                        </span>
                                    </div>
                                    <div class="col_small">
                                        <span class="radiobtn_area">
                                            <input type="radio" name="filesize" id="large" value="2" class="input_text">
                                            <label for="large" class="label_text">Large</label>
                                        </span>
                                        <span class="radiobtn_area">
                                            <input type="radio" name="filesize" id="bigdata" value="3" class="input_text">
                                            <label for="bigdata" class="label_text">Big Data</label>
                                        </span>
                                    </div>
                                </div>
                                <div class="col" style="margin-top:20px;">
                                    <label for="" class="label_text">Budget<span style="color:#CC0000">*</span></label>
                                    <span class="radiobtn_area">
                                        <input type="radio" name="budget" id="lessthan" value="0" class="input_text budget" checked="checked">
                                        <label for="lessthan" class="label_text">less than</label>
                                    </span>
                                    <span class="radiobtn_area">
                                        <input type="radio" name="budget" id="between" value="1" class="input_text budget">
                                        <label for="between" class="label_text">between</label>
                                    </span>
                                    <span class="radiobtn_area">
                                        <input type="radio" name="budget" id="morethan" value="2" class="input_text budget">
                                        <label for="morethan" class="label_text">more than</label>
                                    </span>
                                </div>
                                <div class="col">
                                    <div style="float:left; width:43%;">
                                        <input type="text" name="min_amount" id="min_amount" class="input_small required number" style="width:78%;">
                                    </div>
                                    <label id="and" style="float:left; margin-right:4px; padding-top: 13px;display:none;">and</label>
                                    <div style="float:left; width:41%;">
                                        <input type="text" name="max_amount" id="max_amount" class="input_small" style="width:79%;display:none;">
                                    </div>
                                </div>
                            </div>  
                            <div class="right_col">
                                <div class="col">
                                    <label for="" class="label_text">Project Description or Managerial Problem<span style="color:#CC0000">*</span><br> (<span> Please describe your project/data or
                                            managerial problem with as much details as possible and we will reach out and help you
                                            meet your expectations for your business
                                        </span>)</label>
                                    <textarea name="description" id="description" class="textarea required"></textarea>
                                </div>
                                <div class="col" style="margin-top:20px;">
                                    <label for="" class="label_text">Upload a file (if any)<br> (<span> Only word, excel, ppt or pdf file format is accepted
                                        </span>)</label>
                                    <input type="file" id="uploadfile" name="uploadfile" style="overflow: hidden; text-overflow:ellipsis; white-space:nowrap; width: 100%;">
                                </div>
                                <div class="col" style="margin-top:20px;">
                                    <label for="" class="label_text">Do you have a promotional code?<span style="color:#CC0000">*</span></label>
                                    <div class="col_small col_small_new">
                                        <input type="radio" name="promo" id="coupon" value="0" class="input_text promo">
                                        <label for="coupon" class="label_text">I have a coupon code</label>
                                    </div>
                                    <div class="col_small col_small_new">
                                        <input type="radio" name="promo" id="giftcard" value="1" class="input_text promo">
                                        <label for="giftcard" class="label_text">I have a gift card</label>
                                    </div>
                                    <div class="col_small col_small_new">
                                        <input type="radio" name="promo" id="nocoupon" value="2" class="input_text promo" checked="checked">
                                        <label for="nocoupon" class="label_text">No</label>
                                    </div>
                                </div>
                                <div class="col promocode" style="display:none;">

                                    <input type="text" name="promocode" id="promocode" class="input_text">
                                </div>
                            </div>
                            <div class="form_separator"></div>
                            <h3 style="border:none; padding-top: 0px;">About yourself</h3>
                            <div class="left_col">
                                <div class="col">
                                    <label for="" class="label_text">First and last name </label>
                                    <input type="text" name="name" id="name" class="input_text required nameValidate">
                                </div>
                                <div class="col">
                                    <label for=""  class="label_text">Best phone number</label>
                                    <input type="text" name="phone" id="phone" class="input_text required phoneUS">
                                </div>
                                <div class="col">
                                    <label for=""  class="label_text">Country</label>
                                    <select name="c_country" id="c_country"  class="input_text countryClient required">
                                        <?php echo countryList(); ?>
                                    </select>
                                </div>
                                <div style="margin-top:20px;" class="col">
                                    <label class="label_text" for="">Would you like to receive quick alerts via SMS?</label>
                                    <span class="radiobtn_area">
                                        <input type="radio" class="input_text alert" value="0" id="yessms" name="sms">
                                        <label class="label_text" for="yessms">Yes</label>
                                    </span>
                                    <span class="radiobtn_area">
                                        <input type="radio" class="input_text alert" value="1" id="nosms" name="sms" checked="checked">
                                        <label class="label_text" for="nosms">No</label>
                                    </span>
                                </div>
                                <div class="col smsnumber" style="display:none;">
                                    <input type="text" name="countryCode" id="countryCode" class="input_small countrycode" style="width:13% !important;display:none;">
                                    <input type="text" name="cellphone" id="cellphone" class="input_small cellphone" style="width:50% !important;">
                                </div>
                                <div style="margin-top:20px;" class="col">

                                    <label class="" for="captcha">Captcha<span style="color:#CC0000">*</span></label>
                                    <div id="captcha-wrap">
                                        <img src="admin/captcha/captcha.php" alt="" id="captcha" /><br>
                                        Can't see Image ?<img src="<?php echo HOME_PATH ?>images/refresh.jpg" alt="refresh captcha" id="refresh-captcha" />
                                    </div>
                                </div>
                                <div class="col">
                                    <label class="" for="captcha">Enter the code shown above</label>
                                    <div class="col"><input class="input_text" id="captcha-text" name="captcha" type="text"></div>
                                    <label for="" class="label_text"><span class="code">We will contact you within 24 hours</span></label>
                                </div>
                            </div>
                            <div class="right_col">
                                <div class="col">
                                    <label for="" class="label_text">Best email contact </label>
                                    <input type="text" name="email" id="email" class="input_text required email">
                                </div>
                                <div class="col">
                                    <label for="" class="label_text">Confirm email </label>
                                    <input type="text" name="cemail" id="cemail" equalTo="#email" class="input_text required email" onPaste="return false">
                                </div>
                                <div class="col">
                                    <label for="" class="label_text">State</label>
                                    <select name="c_state" id="c_state"  class="input_text required">
                                        <?php echo stateList(); ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row" style="text-align:center;">
                                <button type="submit" class="submit_btn" value="requestQuote" name="submit_quote">Get a Free Quote</button>
                            </div>
                        </form>
                    </div>
                    <div style=" clear:both; width:100%;"></div>


                </div>


            </div>
            <!----right container STARTS------>
            <div class="right_container">
                <div class="about_col none willget">
                    <h1>What you will get?</h1>
                    <ul class="you_Get">
                        <li>An email and phone call from one of our  business analysts/consultants.
                        </li>
                        <li>An estimate cost for your project</li>
                        <li>A promotional code (Coupon or Gift Card)</li>
                        <li>A live online chat and/or in-person meeting</li>
                        <li>We respect your privacy and safeguard your data. All data is securely stored using 1024-bit encryption and SSL protection</li>
                    </ul>

                    <h1>Our Address</h1>
                    <img class="img" src="images/ourAdress_bg.png" alt="">
                    <span>New York City</span>
                    <p>102 Madison Ave, Second Floor 
                        New York, NY, 10016</p>

                    <h1 >Our Phone</h1>
                    <img class="img" src="images/our_phone_bgImg.png" alt="">
                    <p class="ourphon_text">Give Us a Call</p>
                    <span class="our_phon">212-260-1978l</span>

                    <h1>Our Skype ID</h1>
                    <img class="img" src="images/our_skypeId_bg_img.png" alt="">
                    <span>data.analytics_skype</span>

                </div>

            </div>
        </div>
    </div>
</section>
<?php
    $sql_budget = "SELECT amount FROM " . _prefix("budget") . " WHERE id= '1'";
    $res_budget = $db->sql_query($sql_budget);
    $data = $db->sql_fetchrow($res_budget);
?>
<script type="text/javascript">
   $(document).ready(function() {
        // Refresh Captcha
        $('#refresh-captcha').on('click', function() {
            $('#captcha').attr("src", "<?php echo HOME_PATH ?>admin/captcha/captcha.php?rnd=" + Math.random());
        });
        //Validation
        $('#requestQuote').validate({
            rules: {
                "captcha": {
                    "required": true,
                    "remote":
                            {
                                url: '<?php echo HOME_PATH ?>admin/captcha/checkCaptcha.php',
                                type: "post",
                                data:
                                        {
                                            code: function()
                                            {
                                                return $('#captcha-text').val();
                                            }
                                        }
                            }
                },
                "uploadfile": {
                    "extension": "doc|docx|xls|xlsx|ppt|pdf"
                },
                "email": {
                    "required": true,
                    "email": true,
                    "remote": {
                        url: '<?php echo HOME_PATH ?>admin/ajax.php',
                        type: 'POST',
                        data: {action: "unquieEmail", email: function() {
                                return $('#email').val();
                            }
                        }
                    }
                },
                "min_amount": {
                    "remote": {
                        url: '<?php echo HOME_PATH ?>admin/ajax.php',
                        type: 'POST',
                        data: {action: 'minBudget'}
                    }
                },
                "max_amount": {
                    "remote": {
                        url: '<?php echo HOME_PATH ?>admin/ajax.php',
                        type: 'POST',
                        data: {action: 'maxBudget'}
                    },
                    "min": function() {
                        return parseInt($('#min_amount').val());
                    }
                },
                "promocode": {
                    "remote": {
                        url: '<?php echo HOME_PATH ?>admin/ajax.php',
                        type: 'POST',
                        data: {action: 'couponExist', type: function() {
                              return  $("input[type='radio'][name='promo']:checked").val();
                            }}
                    }
                }
            },
            messages: {
                "captcha": {
                    "required": "Please enter the verifcation code.",
                    "remote": "Verication code incorrect, please try again."
                },
                "email": {
                    "required": "This field is required.",
                    "email": "Please enter a valid email address.",
                    "remote": "Email Id already exists."
                },
                "min_amount": {
                    "remote": "Minimun budget should be $<?php echo $data['amount']; ?>"
                },
                "max_amount": {
                    "remote": "Minimun budget should be $<?php echo $data['amount']; ?>"
                },
                "promocode": {
                    "remote": "Promotional Code is not valid"
                }
            }
        });
    });

</script>
<script type="text/javascript" src="<?php echo HOME_PATH ?>js/request-quote.js"></script>