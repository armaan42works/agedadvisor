<!----------------
Objective : Sales person apply for a Job
Filename : apply.php
Created By : Sanket Khanna <sanket.khanna@ilmp-tech.com>
Created On : 19 August 2014
----->
<?php
    if (isset($application) && $application == 'Send Application') {
        $fields_array = job_application();
        $response = Validation::_initialize($fields_array, $_POST);
        if (isset($response['valid']) && $response['valid'] > 0) {

            if (isset($_FILES['resume']['name'])) {


                if ($_FILES['resume']['error'] == 0) {

                    $target_path = ADMIN_PATH . 'files/jobs/';

                    $date = new DateTime();
                    $timestamp = $date->getTimestamp();
                    $fileName = $timestamp . '_' . basename($_FILES['resume']['name']);
                    $target_path = $target_path . $fileName;
                    move_uploaded_file($_FILES['resume']["tmp_name"], $target_path);
                    $_POST['resume'] = $fileName;
                }
            }



            $userData = array(
                'user_type' => 1,
                'name' => $name,
                'address' => $address,
                'email' => $email,
                'country_id' => $c_country,
                'state_id' => $c_state,
                'phone' => $phone,
                'cellphone' => $cellphone,
                'zipcode' => $zipcode,
                'validate ' => 0,
                'password' => md5($password),
                'ip_address' => $_SERVER['REMOTE_ADDR']
            );


            $insert_user = $db->insert(_prefix('users'), $userData);
            $sp_id = mysqli_insert_id($db->db_connect_id);
            $job_id = $pathInfo['call_parts']['2'];

            $jobsData = array(
                'sp_id' => $sp_id,
                'job_id' => $job_id,
                'coverletter' => $coverletter,
                'resumefile' => $fileName
            );

            $insert_job = $db->insert(_prefix('job_application'), $jobsData);

            if ($insert_job && $insert_user) {
                $c_email = $email;
                $url = '<a href="'.HOME_PATH.'validate/'.md5($password).'"> Click Here</a>';
                $email = emailTemplate('validate_account');
                if ($email['subject'] != '') {
                     $message = str_replace(array('{url}', '{name}'), array($url, $c_name), $email['description']);
                     $send = sendmail($c_email, $email['subject'], $message);
                    if($send){
                          $mysql_query = "UPDATE " . _prefix("users") . " SET email_send = '1' WHERE id = '$sp_id'";
                           $db->sql_query($mysql_query);
           }
        }
                // Message for insert
                $msg = common_message(1, constant('APPLICATION'));
                $_SESSION['msg'] = $msg;
                $url = HOME_PATH.'career';
                redirect_to($url);
                
            }
        } else {
            $errors = '';
            foreach ($response as $key => $message) {
                $error = true;
                $errors .= $message . "<br>";
            }
        }
    }
?>

<section id="slider_container" class="request_quteo_slider">
    <div class="slider_bg">
        <div class="request_quteo_center">
            <h1>Work with Us</h1>
            <p>Join today our great, professional and employee-friendly work environment with great work ethics</p>

        </div>

    </div>
</section>

<section id="body_container">
    <div class="wrapper">

        <div class="main_box">
            <!----left container STARTS------>
            <div class="left_container">
                
                <div class="login_container quote_container">
                    <div><?php
                                if (isset($_SESSION['msg'])) {
                                    echo $msg;
                                    unset($_SESSION['msg']);
                                }
                            ?></div>
                    <h3>Job Application Form</h3>
                    <div class="clientView career_container">
                        <form id="applyForm" class="request_form" name="applyForm" method="post" enctype="multipart/form-data">
                            <div class=" left_col left_coll2">
                                <div class="col">
                                    <span class="radiobtn_area" style="width:100%;">
                                        <input type="radio" id="exist" name="sales" class="sales input_text input_width">
                                        <label class="label_text" for="exist">Existing Sales Professional</label>
                                        <input type="radio" id="new" name="sales" class="sales input_text input_width" checked="checked">
                                        <label class="label_text" for="new">New Sales Professional Applicant</label>
                                </div>
                                <div class="col">
                                    <label class="label_text">Name<span style="color:#CC0000">*</span></label>
                                    <input type="text" name="name" id="name" class="sales input_text input_width" placeholder="Enter name" MaxLength="100">
                                </div>                            
                                <div class="col">
                                    <label class="label_text">Street Address<span style="color:#CC0000">*</span></label>
                                    <textarea id="address" name="address" class="textarea input_text input_width" placeholder="Enter adress" MaxLength="500"></textarea>
                                </div>

                                <div class="col">
                                    <label class="label_text">Country<span style="color:#CC0000">*</span></label>
                                    <select name="c_country" id="c_country"  class="input_text countryApply"><?php echo countryList(); ?></select>
                                </div>

                                <div class="col">
                                    <label class="label_text">State<span style="color:#CC0000">*</span></label>
                                    <select name="c_state" id="c_state" class="input_text input_width"><?php echo stateList(); ?></select>
                                </div>   

                                <div class="col">
                                    <label class="label_text">Zip Code<span style="color:#CC0000">*</span></label>
                                    <input type="text" name="zipcode" id="zipcode" class="input_text input_width" placeholder="Enter zipcode" MaxLength="10">
                                </div>

                                <div class="col">
                                    <label class="label_text">Best phone number<span style="color:#CC0000">*</span></label>
                                    <input type="text" name="phone" id="phone" class="input_text input_width" placeholder="Enter phone number" MaxLength="10">
                                </div>

                                <div class="col">
                                    <label class="label_text">Cell phone</label>
                                    <input type="text" name="cellphone" id="cellphone" class="input_text input_width" placeholder="Enter cellphone number" MaxLength="10">
                                </div>
                                <div class="col">
                                    <label class="label_text">Email Address<span style="color:#CC0000">*</span></label>
                                    <input type="text" name="email" id="email" class="input_text input_width" placeholder="Enter email" MaxLength="100"> 
                                </div>
                                <div class="col">
                                    <label class="label_text">Confirm email<span style="color:#CC0000">*</span></label>
                                    <input type="text" name="cemail" id="cemail" onPaste="return false" class="input_text input_width" placeholder="confirm email" MaxLength="100">
                                </div>
                                <div class="col">
                                    <label class="label_text">Password<span style="color:#CC0000">*</span></label>
                                    <input type="password" name="password" id="password" class="input_text input_width" placeholder="Enter password">
                                </div>
                                <div class="col">
                                    <label class="label_text">Confirm Password<span style="color:#CC0000">*</span></label>
                                    <input type="password" name="cpassword" id="cpassword" class="input_text input_width" placeholder="Confirm password">
                                </div>
                                <div class="col">   
                                    <label class="label_text">Cover letter (optional field)<br>
                                        <span>Resume (copy and paste your resume here)</span>
                                    </label>
                                    <textarea id="coverletter" name="coverletter" class="textarea input_text input_width" placeholder="Copy and paste your resume" maxlength="2000"></textarea>
                                </div>

                                <div class="col">
                                    <label class="label_text">Upload your resume<span style="color:#CC0000">*</span>
                                        <br><span>Only MS Word or Pdf file is accepted</span>
                                    </label>
                                    <input type="file" name="resume" id="resume"><br>
                                </div>
                                <div class="col">
                                    <label class="label_text" for="captcha">Captcha<span style="color:#CC0000">*</span></label>
                                    <div id="captcha-wrap">
                                        <img src="<?php echo HOME_PATH ?>admin/captcha/captcha.php" alt="" id="captcha" /><br>
                                        Can't see Image ?<img src="<?php echo HOME_PATH ?>images/refresh.jpg" alt="refresh captcha" id="refresh-captcha" />
                                    </div>
                                </div>
                                <div class="col">
                                    <label class="label_text" for="captcha">Enter the code shown above</label>
                                    <input class="input_text input_width" id="captcha-text" name="captcha" type="text" placeholder="Enter captcha text">
                                </div>
                                <div style="text-align:center;" class="row">
                                    <button id="application" name="application" value="Send Application" class="submit_btn" type="submit">Send Application</button>
                                </div> 

                            </div>

                        </form>
                    </div>


                </div>
                <div style=" clear:both; width:100%;"></div>


            </div>
            <div class="right_container">
                <div class="about_col none willget">
                    <h1>What you will get?</h1>
                    <ul class="you_Get">
                        <li> We pay way above the market compensation average through our commission model by 2-5 folds.                        </li>
                        <li>Get higher commission pay without the burden of excessive over time through our employee - friendly commission model</li>
                        <li>Earn additional commissions on your existing clients over time. </li>
                        <li>Earn points when you refer new clients and/or share us on social media.</li>
                        <li>Redeem earned points into payment.</li>
                        <li>Get bonus pay.</li>
                        <li>Get paid every month and on time.</li>
                        <li>Track in real-time earnings and pay outs in a very transparent way.</li>
                        <li>Get payment history in one place.</li>

                    </ul>



                </div>

            </div>

        </div>

    </div>
</div>
</section>
<script type="text/javascript">
    $(document).ready(function() {
        $('#refresh-captcha').on('click', function() {
            $('#captcha').attr("src", "<?php echo HOME_PATH ?>admin/captcha/captcha.php?rnd=" + Math.random());
        });

        $('.sales').click(function() {
            var type = $(this).attr('id');
            if (type == 'exist') {
                window.location.href = '<?php echo HOME_PATH ?>login';
            }
        });


        $('#applyForm').validate({
            rules: {
                "captcha": {
                    "required": true,
                    "remote":
                            {
                                url: '<?php echo HOME_PATH ?>admin/captcha/checkCaptcha.php',
                                type: "post",
                                data:
                                        {
                                            code: function()
                                            {
                                                return $('#captcha-text').val();
                                            }
                                        }
                            }
                },
                "name": {
                    "required": true,
                    "nameValidate": true
                },
                "address": {
                    "required": true
                },
                "c_country": {
                    "required": true
                },
                "c_state": {
                    "required": true
                },
                "zipcode": {
                    "required": true
                },
                "phone": {
                    "required": true,
                    "phoneUS": true
                },
                "cellphone": {
                    "phoneUS": true
                },
                "email": {
                    "required": true,
                    "email": true,
                    "remote": {
                        url: '<?php echo HOME_PATH ?>admin/ajax.php',
                        type: 'POST',
                        data: {action: "unquieEmail", email: function() {
                                return $('#email').val();
                            }
                        }
                    }

                },
                "cemail": {
                    "required": true,
                    "email": true,
                    "equalTo": "#email"
                },
                "password": {
                    "required": true,
                    "password_require": true
                },
                "cpassword": {
                    "required": true,
                    "password_require": true,
                    "equalTo": "#password"
                },
                "resume": {
                    "required": true,
                    "extension" : "pdf|doc|docx"
                }


            },
            messages: {
                "captcha": {
                    "required": "Please enter the verifcation code.",
                    "remote": "Verication code incorrect, please try again."
                },
                "email": {
                    "required": "This field is required.",
                    "email": "Please enter a valid email address.",
                    "remote": "Email Id already exists."
                }
            }

        });

        if ($('#c_country').val() == 229) {
            $('input[name="zipcode"]').rules('remove');
            $('input[name="zipcode"]').rules('add', {
                digits: true,
                required: true,
                maxlength: 5
            });
        }

        $('#c_country').on('change', function() {
            var coun_id = $(this).val();
            if (coun_id == 229) {
                $('input[name="zipcode"]').rules('remove');
                $('input[name="zipcode"]').rules('add', {
                    digits: true,
                    required: true,
                    maxlength: 5
                });
            } else {
                $('input[name="zipcode"]').rules('remove');
                $('input[name="zipcode"]').rules('add', {
                    required: true
                });
            }
        });


        $('.countryApply').on('change', function() {
            var country = $(this).val();
            if (country != 229) {
                $('#countryCode').show();
                $('#countryCode').val('');
            } else {
                $('#countryCode').hide();
                $('#countryCode').val('');
            }
            $.ajax({
                type: 'POST',
                url: '<?php echo HOME_PATH ?>admin/ajax.php',
                data: {action: 'stateList', country: country},
                success: function(response) {
                    $('#c_state').html($.trim(response));

                }
            });
        });

    });


</script>
