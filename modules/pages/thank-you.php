<?php
    /*
     * Objective : After Client Register will be redirected here
     * Filename : thank-you.php
     * Created By : Yogesh Joshi <yogesh.joshi@ilmp-tech.com>
     * Created On : 12 August 2014
     * Modified On : 14 August 2014
     */ 
?>
<section id="slider_container">
    <div class="slider_bg">
        <ul class="slider">
            <li><img src="<?php echo HOME_PATH; ?>images/slider_img.png" alt=""></li>
        </ul>

    </div>
</section>


<section id="body_container">
    <div class="wrapper">
        <div class="main_box">
            <!----left container STARTS------>
            <div class="left_container">
                <div class="about_col aboutus_container">
                    <p>Dear <?php echo $_SESSION['registerName']; ?></p>
                  <p>Thank you for submitting a quote request. You will hear back from us shortly.</p>
                  <p>Meanwhile, please login to your inbox at (<?php echo $_SESSION['registerEmail']; ?>) and click the link we sent you to verify your email. <strong>The link will expire in 4 hours</strong>. This step is necessary to validate your quote request. If our email is not delivered to your inbox, please check your spam folder.</p>
                  <p>We appreciate your business.<br>
                      KA Analytics & Technology
                  </p>
                </div>

            </div>
            <!----right container STARTS------>
            <div class="right_container">
                <div class="about_col none about-right">
                    <h3>News letter Sign Up</h3>
                    <p>Want to keep up to date with all our latest news and information? Enter your name and email below to be added to our mailing list.</p>
                    <div class="newletter_bg">
                        <h3>Join our Newsletter</h3>
                        <form id="form1" name="form1" method="post" class="joinus">
                            <input type="text" placeholder="Your Name">
                            <input type="password" placeholder="Your E-mail">
                            <a href="#" class="subscribe">Subscribe</a>
                        </form>






                    </div>




                </div>

            </div>
        </div>
    </div>
</section>