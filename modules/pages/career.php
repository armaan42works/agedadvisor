<!----------------
Objective : Listing of all the Active Jobs
Filename : career.php
Created By : Sanket Khanna <sanket.khanna@ilmp-tech.com>
Created On : 19 August 2014
----->
<script type="text/javascript">
    $(document).ready(function() {
        $('.caree_accord').click(function() {
            var job = $(this).attr('id');

            $('.caree_accord_body').each(function() {
                if ($(this).hasClass('' + job + '') && $(this).css('display') == 'none') {
                    $(this).slideDown();
                } else {
                    $(this).slideUp();
                }
            })
        });
    });

</script>
<section id="slider_container" class="request_quteo_slider">
    <div class="slider_bg">
        <div class="request_quteo_center">
            <h1>Work with Us</h1>
            <p>Join today our great, professional and employee-friendly work environment with great work ethics</p>

        </div>

    </div>
</section>

<section id="body_container">
    <div class="wrapper">

        <div class="main_box">
            <!----left container STARTS------>
            <div class="left_container left_new">
                <div class="login_container quote_container quotenew">
                    <div><?php
                                if (isset($_SESSION['msg'])) {
                                    echo $msg;
                                    unset($_SESSION['msg']);
                                }
                            ?></div>
                    <h3>Current Openings</h3>
                    <div class="clientView career_container">

                        <?php
                            global $db;
                            $sql_query = "SELECT * FROM " . _prefix("position") . " WHERE status = '1' && deleted = '0' && date(expire_date) >= '" . date('Y-m-d') . "'";
                            $res = $db->sql_query($sql_query);
                            $num = $db->sql_numrows($res);
                            $jobs = $db->sql_fetchrowset($res);
                            if ($num > 0) {
                                ?>
                                <div class="my_accordian">
                                    <?php
                                    $i = 1;
                                    foreach ($jobs as $job) {
                                        $start_ts = time();
                                        $end_ts = strtotime($job['expire_date']);
                                        $diff = $end_ts - $start_ts;
                                        $day = round($diff / 86400);
                                        ?>
                                        <div class="caree_accord" style="cursor:pointer;"  id="<?php echo $i; ?>"><?php echo strtoupper($job['job_title']) . ' ( Job Number : ' . $job['unique_id'] . ' )'; ?></div>
                                        <div class="caree_accord_body <?php echo $i; ?>" style=" display:none;">
                                            <p><strong>Job posted on : </strong><?php echo date('M d, Y, h:m a', strtotime($job['created'])); ?> ET</p>
                                            <p><strong>Job number : </strong><?php echo $job['unique_id']; ?></p>
                                            <p> <strong>Industry of interest : </strong><?php echo $job['interest_area']; ?></p>
                                            <p> <strong>Compensation : </strong><?php echo ($job['compensation'] != '') ? $job['compensation'] : 'N/A'; ?></p>
                                            <p><strong> Description : </strong></p>
                                            <ul class="Description">
                                                <li><?php echo stripslashes($job['description']); ?></li>
                                            </ul>
                                            <p class="applued_by"><strong>Apply by : </strong><?php echo date('d M, Y', strtotime($job['expire_date'])) . ' (' . ++$day . ' days left)' ?></p>
                                            <input type="submit" name="" value="Apply Now "   class="creat_account_btn"  onClick=" window.location.href = '<?php echo HOME_PATH ?>career/apply/<?php echo md5($job['id']) ?>'">
                                            <p class="creer_socil_link">
                                            <div class="share_icon">
                                                <span class="share"> Share</span> 
                                                <span class="f_bookk"><img src="<?php echo HOME_PATH ?>images/share_icon.png"/> </span>
                                                <span class="f_bookk"> <img src="<?php echo HOME_PATH ?>images/facebook_icon.png"/> </span>     
                                                <span class="f_bookk"><img src="<?php echo HOME_PATH ?>images/twitter_icon.png"/> </span>  
                                                <span class="f_bookk"> <img src="<?php echo HOME_PATH ?>images/linkdin.png"/> </span>     
                                                <span class="f_bookk"><img src="<?php echo HOME_PATH ?>images/print.png"/> </span>
                                                <span class="f_bookk"><img src="<?php echo HOME_PATH ?>images/message_icon.png"/> </span>

                                            </div>
                                            </p>

                                        </div>
                                        <?php
                                        $i++;
                                    }
                                    ?>
                                </div>
                                <?php
                            } else {
                                ?>
                                <p>Currently there are no openings</p>
                                <?php
                            }
                        ?>
                    </div>


                </div>
                <div style=" clear:both; width:100%;"></div>


            </div>


        </div>

    </div>
</div>
</section>
