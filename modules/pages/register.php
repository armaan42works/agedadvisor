<?php 
       /*
     * Objective: Client registeration
     * filename : register.php
     * Created By: Sanket Khanna <sanket.khanna@ilmp-tech.com>
     * Created On : 11 August 2014
     * Modified On : 01 September 2014
     */
    if (isset($_POST['captcha']) && $_POST['captcha']) {
        $fields = array(
            'business' => $cp_business,
            'industry' => $cp_Industry,
            'website' => $cp_website,
            'name' => $c_name,
            'email' => $c_email,
            'password' => md5($c_password),
            'source_id' => $c_source,
            'phone' => $c_number,
            'address' => $c_address,
            'zipcode' => $c_zipcode,
            'country_id' => $c_country,
            'state_id' => $c_state,
            'user_type' => '0',
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'validate' => '0');
        if ($_POST['sp_refferal'] == 0) {
            $fields['refferal_id'] = $cp_sales_id;
        } elseif ($_POST['cp_refferal'] == 2) {
            $fields['refferal_id'] = $cp_client_id;
        } else {
            $fields['refferal_id'] == '';
        }
        $insert_result = $db->insert(_prefix('users'), $fields);
        $id = mysqli_insert_id($db->db_connect_id);
        $url = '<a href="'.HOME_PATH.'validate/'.md5($id).'"> Click Here</a>';
        $email = emailTemplate('validate_account');
        if ($email['subject'] != '') {
            $message = str_replace(array('{url}', '{name}'), array($url, $c_name), $email['description']);
           $send = sendmail($c_email, $email['subject'], $message);
           if($send){
               $mysql_query = "UPDATE " . _prefix("users") . " SET email_send = '1' WHERE id = ".  mysqli_insert_id($db->db_connect_id);
               $db->sql_query($mysql_query);
               $_SESSION['registerName'] = $c_name;
               $_SESSION['registerEmail'] = $c_email;
                $url = HOME_PATH.'thank-you';
                redirect_to($url);
           }
        }
    }
    
    
    ?>
<section id="slider_container">
    <div class="slider_bg">
        <ul class="slider">
            <li><img src="images/slider_img.png" alt=""></li>
        </ul>

    </div>
</section>

<section id="body_container">
    <div class="wrapper">

        <div class="main_box">
            <!----left container STARTS------>
            <div class="left_container">
                <div class="login_container quote_container">
                    <h3>Welcome, Create your free client account today!</h3>

                    <div class="creat_account_bg">  
                        <button class="client_account_btn active account" id="clientAccount">Client Account</button>
                        <button class="sales_person_account_btn account" id="salesAccount" onclick="window.location.href = '<?php echo HOME_PATH; ?>career';">Sales Person Joins Here</button>
                    </div>
                    <div class="clientView">
                        <form id="clientForm" name="clientForm" method="post" class="login_account ragi_form request_form none" >
                            <div class="from_col1">
                                <div class="left_form">
                                    <div class="col">
                                        <label for="" class="label_text">Business Name <span style="color:#CC0000">*</span> </label>
                                        <input type="text" name="cp_business" id="cp_business" class="input_login required"value="" placeholder="Enter business name" MaxLength="100">
                                    </div>
                                    <div class="col">
                                        <label for="" class="label_text">Industry </label>
                                        <input type="text" name="cp_Industry" id="cp_industry" class="input_login"value="" placeholder="Enter industry name" MaxLength="100">
                                    </div>
                                    <div class="col">
                                        <label for="" class="label_text">Website </label>
                                        <input type="text" name="cp_website" id="cp_website" class="input_login"value="" placeholder="www.abc.com" MaxLength="100">
                                    </div>



                                </div>
                                <div class=" clientAccountright" style="height:328px !important;">


                                    <div class="col">
                                        <label for="" class="label_text">Are you referred by a salesperson?</label>
                                        <span class="radiobtn_area">
                                            <input type="radio" name="sp_refferal" id="0" value="0" class="input_text sp_reffer" checked="checked">
                                            <label for="0" class="label_text">Yes</label>
                                        </span>
                                        <span class="radiobtn_area">
                                            <input type="radio" name="sp_refferal" id="1" value="1" class="input_text sp_reffer">
                                            <label for="1" class="label_text">No</label>
                                        </span>
                                    </div>
                                    <div class="col cp_sales_id" style="position: relative;">
                                        <label for="" class="label_text">Enter your salesperson’s 10-digit ID code <span style="color:#CC0000">*</span> </label>
                                        <span style="float:left; color:#000000; font-size:12px; left:-31px; position:absolute; top:49px;">KAS:</span><input type="text" name="cp_sales_id" id="cp_sales_id" maxlength="10" class="input_login required"value="" placeholder="Enter 10-digit ID code" MaxLength="20">
                                    </div>
                                    <div class="col refferal cp_client_refferal" style="margin-top:20px;">
                                        <label for="" class="label_text">Are you referred by one of our clients?</label>
                                        <span class="radiobtn_area">
                                            <input type="radio" name="cp_refferal" id="0" value="2" class="input_text cp_reffer" checked="checked">
                                            <label for="0" class="label_text">Yes</label>
                                        </span>
                                        <span class="radiobtn_area">
                                            <input type="radio" name="cp_refferal" id="1" value="3" class="input_text cp_reffer">
                                            <label for="1" class="label_text">No</label>
                                        </span>
                                    </div>
                                    <div class="col refferal cp_client_id" style=" position: relative;">
                                        <label for="" class="label_text">Enter client’s 10-digit ID code <span style="color:#CC0000">*</span> </label>
                                        <span style="float:left; color:#000000; font-size:12px; left:-31px; position:absolute; top:49px;">KAC:</span><input type="text" name="cp_client_id" id="cp_client_id" maxlength="10" class="input_login"value="" placeholder="Enter 10-digit ID code" MaxLength="20">
                                    </div>

                                </div>
                            </div>
                            <div class="from_col1 none">
                                <h3 style="border:none;padding:0px;">About yourself</h3>
                                <div class="left_form">   
                                    <div class="col">
                                        <label for="" class="label_text">Name <span style="color:#CC0000">*</span> </label>
                                        <input type="text" name="c_name" id="c_name" class="input_login required nameValidate"value="" placeholder="Jhon Player" MaxLength="100">
                                    </div>

                                    <div class="col">
                                        <label for="" class="label_text">Email Address <span style="color:#CC0000">*</span> </label>
                                        <input type="text" name="c_email" id="c_email" class="input_login"value="" placeholder="abc@gmail.com" MaxLength="100">
                                    </div>
                                    <div class="col">
                                        <label for="" class="label_text">Confirm Email Address <span style="color:#CC0000">*</span> </label>
                                        <input type="text" name="cc_email" id="cc_email" equalTo='#c_email' class="input_login required email"value="" onPaste="return false" placeholder="abc@gmail.com" MaxLength="100">
                                    </div>
                                    <div class="col">
                                        <label for="" class="label_text">Password <span style="color:#CC0000">*</span> </label>
                                        <input type="password" name="c_password" id="c_password" class="input_login required password_require"value="" placeholder="Enter password">
                                    </div>
                                    <div class="col">
                                        <label for="" class="label_text">Confirm Password <span style="color:#CC0000">*</span> </label>
                                        <input type="password" name="cc_password" id="cc_password" equalTo='#c_password' class="input_login required password_require" value="" placeholder="Enter password">
                                    </div>
                                    <div class="col">
                                        <label for="" class="label_text">Where did you hear about us? <span style="color:#CC0000">*</span> </label>
                                        <select name="c_source" id="c_source" class="input_text countrySales required">
                                            <?php echo options($source); ?>
                                        </select>
                                    </div>
                                    <div class="col">
                                        <label class="" for="captcha">Captcha<span style="color:#CC0000">*</span></label>
                                        <div id="captcha-wrap">
                                            <img src="admin/captcha/captcha.php" alt="" id="captcha" /><br>
                                            Can't see Image ?<img src="<?php echo HOME_PATH ?>images/refresh.jpg" alt="refresh captcha" id="refresh-captcha" />
                                        </div>


                                    </div>
                                    <label class="" for="captcha">Enter the code shown above</label>
                                    <div class="col"><input class="input_text" id="captcha-text" name="captcha" type="text" placeholder="Enter captcha text"></div>

                                </div>
                                <div class=" clientAccountright">

                                    <div class="col">
                                        <label for="" class="label_text">Street Address <span style="color:#CC0000">*</span> </label>
                                        <textarea name="c_address" id="c_address" cols="30" rows="5" class="textarea required" placeholder="Enter address"></textarea>
                                    </div>

                                    <div class="col">
                                        <label for="" class="label_text">Country <span style="color:#CC0000">*</span> </label>
                                        <select name="c_country" id="c_country"  class="input_text countryClient required">
                                            <?php echo countryList(); ?>
                                        </select>
                                    </div>
                                    <div class="col">
                                        <label for="" class="label_text">State <span style="color:#CC0000">*</span> </label>
                                        <select name="c_state" id="c_state"  class="input_text required">
                                            <?php echo stateList(); ?>
                                        </select>
                                    </div>
                                    <div class="col">
                                        <label for="" class="label_text">Zip Code <span style="color:#CC0000">*</span> </label>
                                        <input type="text" name="c_zipcode" id="c_zipcode" class="input_login" value="" placeholder="Enter zipcode">
                                    </div>
                                    <div class="col">
                                        <label for="" class="label_text">Phone Number <span style="color:#CC0000">*</span> </label>
                                        <input type="text" name="c_number" id="c_number" class="input_login required phoneUS" value="" placeholder="Enter phone number">
                                    </div>


                                </div>
                                <div class="creat_account">
                                    <button type="button" id="c_button" class="creat_account_btn"  style="margin-top:35px;">JOIN NOW</button>
                                </div>
                                  <div class="sub_title">
                            By submitting this registration form, you certify that you agree to our <a href="#">Privacy Policy </a> and <a href="#">Terms of Service</a>. 
                        </div>
                            </div>
                        </form>


                    </div>
                    <div style=" clear:both; width:100%;"></div>
                    <div class="Salce_tView" style="display:none;">
                        <form id="form2" name="form2" method="post" class="login_account ragi_form">
                            <div class="col">
                                <input type="text" name="textfield" id="textfield" class="input_login" placeholder="User Name">
                            </div>
                            <div class="col">
                                <input type="password" name="password" id="password" class="input_login" placeholder="Email ID">
                            </div>
                            <div class="col">
                                <input type="password" name="password" id="password" class="input_login" placeholder="User id">
                            </div>
                        </form>
                        <div class=" clientAccountright ragi_form">
                            <div class="col">
                                <input type="password" name="password" id="password" class="input_login" placeholder="Password">
                            </div>
                            <div class="col">
                                <input type="password" name="password" id="password" class="input_login" placeholder="Mobile">
                            </div>
                            <div class="col">
                                <input type="password" name="password" id="password" class="input_login" placeholder="Address details">
                            </div>
                        </div>
                        <div class="creat_account">
                            <button type="button" id="click_new" class="creat_account_btn">CREATE ACCOUNT</button>
                        </div>
                      
                    </div>

                </div>


            </div>
            <!----right container STARTS------>
            <div class="right_container">
                <div class="about_col none signup_login willget_new">
                    <h1 style="margin-top:0px;">Why Us?</h1>
                    <ul class="you_Get">
                        <li>Turn your big data into useful and understandable information to optimize business and drive to success and growth
                        </li>
                        <li>Gain comprehensive business insights faster and easier to give your company a unique competitive lead in your industry</li>
                        <li>Improve and ease business decision-making process as this is critical to ensure business success</li>
                        <li>Work with a friendly and dynamic team of experts in data science, predictive analytics, advanced business statistics, algorithms, complex statistical analysis and science who make your data talk and are always ready to take your business to the next level </li>
                        <li>Empower your business through data analytics and get your data talk in a meaningful way</li>
                        <li>Share on social media and earn points.</li>


                    </ul>

                    <h1 style="margin-top:0px;">Benefits of creating a client account:</h1>
                    <ul class="you_Get">
                        <li>Track projects progress in real-time</li>
                        <li>Upload unlimited projects/data.</li>
                        <li>Get unlimited free quotes.</li>
                        <li>Get promo offers (coupons and gift cards)</li>
                        <li>Refer friends and earn points.</li>
                        <li>Share us/follow us on social media and earn points</li>
                        <li>Redeem earned points towards future projects and save</li>
                        <li>Access your file library to manage all your projects in one convenient place.</li>
                        <li>Communicate with ease (24/7) with our team using our user-friendly messaging features.</li>
                        <li>View payment history.</li>
                        <li>And a lot more.</li>
                    </ul>



                </div>

            </div>
        </div>
    </div>
</section>
<script type="text/javascript" src="<?php echo HOME_PATH ?>js/register.js"></script>
<script type="text/javascript">
    			$('#c_button').click(function(e) {
					e.preventDefault();
					$('#clientForm').submit();
				});
    			$('#click_new').click(function(e) {
					e.preventDefault();
					$('#form2').submit();
				});

</script>
