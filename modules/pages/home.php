<?php
//ini_set('display_errors', '1');
$ip = $_SERVER['REMOTE_ADDR']; // the IP address to query
$start_event_fetch_date = '';
$monthName = '';
$start_event_fetch_year = '';
$day_dff = '';
$city_name = '';


function cmp($a, $b) {
    //echo $a['id'];
    if ($a['ratingValue'] == $b['ratingValue']) {
        return 0;
    }
    return ($a['ratingValue'] < $b['ratingValue']) ? -1 : 1;
}
function show_image($image_enabled = FALSE, $id = "'none'", $certification_service_type = '', $url = '') {
    record_mtime("/modules/search/search.php function show_image()");

    $return = '';
    global $db;

    $review_friendly_badge = $response_time_badge = $response_rate_badge = '';

    $response_rate_query = "SELECT COUNT(NULLIF(id,0)) as total_enq, (SELECT COUNT(NULLIF(id,0)) FROM ad_enquiries WHERE DATE(time_of_enquiry) < DATE_SUB(CURDATE(), INTERVAL 7 DAY) and prod_id = $id AND facility_responded = 'y') AS replied FROM ad_enquiries WHERE DATE(time_of_enquiry) < DATE_SUB(CURDATE(), INTERVAL 7 DAY) and prod_id = $id";
    $response_rate_res = $db->sql_query($response_rate_query);
    $response_rate_data = $db->sql_fetchrowset($response_rate_res);


    if ($certification_service_type == 'Retirement Village') 
	{
        $query_reviews = "SELECT count(fdbk.id) as totalreview, fdbk.product_id,(extr.no_of_room) as totalRoom,"
                . "(count(fdbk.id)/(extr.no_of_room)) as cmost FROM ad_feedbacks AS fdbk LEFT JOIN ad_products AS pds ON fdbk.product_id= pds.id"
                . " LEFT JOIN ad_pro_plans AS plan ON fdbk.product_id= plan.pro_id inner join ad_extra_facility as extr on (extr.product_id=pds.id)"
                . " WHERE fdbk.status = 1 AND fdbk.deleted = 0 AND current_plan = 1 and pds.certification_service_type = 'Retirement Village' and (pds.deleted=0)"
                . " and (pds.status=1) GROUP BY product_id ORDER BY cmost DESC,plan_id DESC,product_id ASC LIMIT 50";
    } else 
	{

        $query_reviews = "SELECT count(fdbk.id) as totalreview, fdbk.product_id,(extr.no_of_beds) as totalbeds,"
                . "(count(fdbk.id)/(extr.no_of_beds)) as cmost FROM ad_feedbacks AS fdbk LEFT JOIN ad_products AS pds ON fdbk.product_id= pds.id "
                . "LEFT JOIN ad_pro_plans AS plan ON fdbk.product_id= plan.pro_id inner join ad_extra_facility as extr on (extr.product_id=pds.id)"
                . " WHERE fdbk.status = 1 AND fdbk.deleted = 0 AND current_plan = 1 and pds.certification_service_type != 'Retirement Village' "
                . "and (pds.deleted=0) and (pds.status=1) GROUP BY product_id ORDER BY cmost DESC,plan_id DESC,product_id asc LIMIT 50";
    }
     

    $qry_dayResp = "SELECT facility_name, ( (5 * (DATEDIFF(DATE(time_of_response),"
            . " DATE(time_of_enquiry)) DIV 7) + MID('0123444401233334012222340111123400012345001234550', 7 * WEEKDAY(DATE(time_of_enquiry)) + WEEKDAY(DATE(time_of_response)) + 1, 1) ) - (SELECT COUNT(*) FROM ad_holidays WHERE holiday_date between DATE(time_of_enquiry)"
            . " and DATE(time_of_response) ) )as dayResp FROM ad_enquiries where prod_id='$id'"
            . " and DATE(time_of_enquiry) < DATE_SUB(CURDATE(),INTERVAL 4 DAY) and facility_responded='y' and time_of_enquiry!='' and time_of_response!='' order BY time_of_enquiry desc";

    $res_dayResp = $db->sql_query($qry_dayResp);
    $count_resp = $db->sql_numrows($res_dayResp);
    $row_dayResp = $db->sql_fetchrowset($res_dayResp);
    if ($count_resp > 0) {
        foreach ($row_dayResp as $cResp) {
            $numResp[] = $cResp['dayResp'];
        }

        $avg_resp = round(array_sum($numResp) / count($numResp));

         
    }

    $trustqry = "SELECT "
            . " COUNT(CASE WHEN facility_responded = 'y' then 1 ELSE NULL END) as trusted_count,"
            . " COUNT(CASE WHEN facility_responded = 'n' then 1 ELSE NULL END) as nottrusted_count"
            . " FROM ad_enquiries"
            . " WHERE DATE(time_of_enquiry) < DATE_SUB(CURDATE(), INTERVAL 4 DAY)"
            . " and prod_id = $id ORDER by time_of_enquiry DESC LIMIT 4";

    $result_Resp = $db->sql_query($trustqry);
    $row_resp = $db->sql_fetchrowset($result_Resp);

    if ($row_resp[0]['nottrusted_count'] >= 3 && $row_resp[0]['trusted_count'] < 3) {
        $response_rate_badge = '<div class="nottrusted-responder1 most-reviewed-icon-size" style="margin-right:7px"> </div>';
    } else if ($row_resp[0]['trusted_count'] >= 3 && $row_resp[0]['nottrusted_count'] < 3) {
        $response_rate_badge = '<div class="trusted-responder most-reviewed-icon-size"> </div>';
    } else {
        $response_rate_badge = '';
    }

    $query = "SELECT gal.id, gal.title,gal.file,gal.type,prd.review_friendly FROM " . _prefix("galleries") . "  as gal "
            . " LEFT JOIN " . _prefix("products") . " AS prd on prd.id = gal.pro_id "
            . " where gal.deleted = 0  AND pro_id=" . $id . " AND gal.type=0  ORDER BY gal.id LIMIT 0,1";
    $res = $db->sql_query($query);
    $data = $db->sql_fetchrowset($res);
    if (isset($data[0])) {
        if ($data[0]['review_friendly'] == 1) {
            $review_friendly_badge = '<span class="review-friendly most-reviewed-icon-size"> </span>';
        }
        if ($data[0]['review_friendly'] == 2) {
            $review_friendly_badge = '<span class="review-unfriendly1 most-reviewed-icon-size"> </span>';
        }
    }


    if ($image_enabled && isset($data[0])) 
	{

        $record = $data[0];
        $pic_description = $data['title'];
        $fileImage = HOME_PATH . 'admin/files/gallery/images/thumb_' . $record['file'];
        $fileImageSrc = HOME_PATH . 'admin/files/gallery/images/thumb_' . $record['file'];
        $fileImageView = HOME_PATH . 'admin/files/gallery/images/' . $record['file'];

        if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/admin/files/gallery/images/thumb_' . $record['file'])) 
		{ 
			  
			  $return = '<a href="' . HOME_PATH . $url . '"><img alt="' . $certification_service_type . ' ' . $pic_description . '" title="' . $pic_description . '" src="' . $fileImageSrc . '" class="img-responsive" style="float:left; margin-right: 20px;margin-bottom:10px;"></a>';
			  
        } else {           
				 $return = '<a href="' . HOME_PATH . $url . '"><img alt="' . $certification_service_type . '" title="' . $certification_service_type . '" src="' . HOME_PATH .'/images/noimagedav1.png" class="img-responsive" style="float:left; margin-right: 20px;margin-bottom:10px;"></a>';
           
        }
    } else {
		
            $return = '<img alt="' . $certification_service_type . '" title="' . $certification_service_type . '" src="' . HOME_PATH .'/images/noimagedav1.png" class="img-responsive" style="float:left; margin-right: 20px;margin-bottom:10px;"></a>';         
    }
    return $return;
    record_mtime("/modules/search/search.php function show_image()");
}
function topFiveReview() {
    global $db;

    
    $query = "SELECT (COUNT(fd.product_id)/extf.no_of_beds) as review, prd.id,prd.title,prd.quick_url,prd.address_city,prd.address_suburb,extf.no_of_room,extf.no_of_beds,fd.overall_rating from ad_products as prd 
	LEFT JOIN ad_feedbacks as fd ON fd.product_id = prd.id 
	LEFT JOIN ad_extra_facility as extf ON prd.id=extf.product_id WHERE prd.deleted=0 AND extf.no_of_beds IS NOT NULL GROUP BY fd.product_id HAVING COUNT(fd.product_id) > 10 AND overall_rating > 4  ORDER BY overall_rating DESC  limit 10";
    
    $res = $db->sql_query($query);
    $data = $db->sql_fetchrowset($res);

    if (count($data) > 0) 
	{
        foreach ($data as $datares) {
            $star_rating = round(simlilarFacilityRating($datares["id"]));
            echo "<style type='text/css'>"
            . ".star-width-".$star_rating
            . "{ width: ".$star_rating."%;}"
            . "</style>";
            
            if (isset($datares['address_suburb']) &&!empty($datares['address_suburb'])) 
			{
                $addr_sububrb_htm =  $datares['address_suburb'];
            } else {
                $addr_sububrb_htm = '';
            }
            
            if (isset($datares['address_city']) &&!empty($datares['address_city'])) {
                $addr_city_htm = $datares['address_city'];
            } else {
                $addr_city_htm = '';
            }
            $showimg=show_image(1, $datares['id'], $datares['certification_service_type'], $datares['quick_url']);
			
            $resHTML .= '<div class="swiper-slide trending-place-item">
                                    <div class="trending-img">'.$showimg.'                                       
                                        <span class="trending-rating-orange">' . $star_rating . '%</span>
                                         
                                    </div>
                                    <div class="trending-title-box">
                                        <h4><a href="' . $datares["quick_url"] . '">' . ucwords(strtolower($datares["title"])) . '</a></h4>
                                        <div class="customer-review">
                                            <div class="rating-summary float-left">
                                                <div class="rating-result" title="' . $star_rating . '%">
                                                    <div class="star-inactive">
													<div style="width:'.$star_rating.'%;"
													
													class="star-active" >
													</div>
													</div>
                                                </div>
                                            </div>
                                            
                                        </div>
                                        <ul class="trending-address">
                                            <li><i class="ion-ios-location"></i>
                                                <p>'.$addr_sububrb_htm.'</p>
                                            </li>
                                            <li><i class="ion-ios-location"></i>
                                                <p>'.$addr_city_htm.'</p>
                                            </li>
                                           
                                        </ul>
                                       
                                    </div>
                                </div>';
        }
    } else {
        //echo "We have more than 5 records with more than five review";
        $query = "SELECT (COUNT(fd.product_id)/extf.no_of_beds) as review, prd.id,prd.title,prd.quick_url,extf.no_of_room,extf.no_of_beds from ad_products as prd LEFT JOIN ad_feedbacks as fd ON fd.product_id = prd.id LEFT JOIN ad_extra_facility as extf ON prd.id=extf.product_id WHERE extf.no_of_beds IS NOT NULL AND  prd.deleted=0 AND extf.no_of_beds <> 0 GROUP BY fd.product_id ORDER BY review HAVING COUNT(fd.product_id) > 5 DESC LIMIT 9 ";
        $res = $db->sql_query($query);
        $data = $db->sql_fetchrowset($res);
        //echo count($data).'awa';
        if (count($data) > 0) {
            foreach ($data as $datares) {   
                $resHTML .= '<li><i class="fa padding-right-10 fa-angle-right"></i><a href="' . $datares["quick_url"] . '">' . $datares["title"] . '</a></li>';
            }
        } else {
            //echo "we are going  to check the we have more than who have review and number of room";
            $query = "SELECT (COUNT(fd.product_id)/extf.no_of_room) as review, prd.id,prd.title,prd.quick_url,extf.no_of_room,extf.no_of_beds from ad_products as prd LEFT JOIN ad_feedbacks as fd ON fd.product_id = prd.id LEFT JOIN ad_extra_facility as extf ON prd.id=extf.product_id WHERE extf.no_of_room IS NOT NULL OR extf.no_of_room <> 0 GROUP BY fd.product_id  ORDER BY review DESC LIMIT 9";
            $res = $db->sql_query($query);
            $data = $db->sql_fetchrowset($res);
            if (count($data) >= 0) {
                foreach ($data as $datares) {
                    $resHTML .= '<li><i class="fa padding-right-10 fa-angle-right"></i><a href="' . $datares["quick_url"] . '">' . $datares["title"] . '</a></li>';
                }
            } else {
                //echo "we are going to get it through stars";
                $query = "SELECT AVG(fd.overall_rating) as review, prd.id,prd.title,prd.quick_url,extf.no_of_room,extf.no_of_beds from ad_products as prd LEFT JOIN ad_feedbacks as fd ON fd.product_id = prd.id LEFT JOIN ad_extra_facility as extf ON prd.id=extf.product_id GROUP BY fd.product_id ORDER BY review DESC LIMIT 9 ";
                $res = $db->sql_query($query);
                $data = $db->sql_fetchrowset($res);
                if (count($data) >= 0) {
                    foreach ($data as $datares) {
                    $resHTML .= '<li><i class="fa padding-right-10 fa-angle-right"></i><a href="' . $datares["quick_url"] . '">' . $datares["title"] . '</a></li>';
                    }
                }
            }
        }
    }
    $resHTML .= '</ul>';
    echo $resHTML;
}

function getWeekReviews() {
    global $db;
    $query = "SELECT fdbk.* , pds.title as pdtitle,pds.id as prId,pds.id AS pImage, sp.user_name as spUsername, sp.first_name AS spFName, sp.last_name AS spLName, cs.user_name AS csUsername, cs.first_name AS csFName, cs.last_name AS csLName ,pds.quick_url FROM " . _prefix("feedbacks") . " AS fdbk   "
    . "LEFT JOIN " . _prefix("products") . " AS pds ON  fdbk.product_id= pds.id "
    . "LEFT JOIN " . _prefix("users") . " AS sp ON  fdbk.sp_id= sp.id "
    . "LEFT JOIN " . _prefix("users") . " AS cs ON  fdbk.cs_id= cs.id "
    . "WHERE fdbk.abused_request=0 AND fdbk.abused=0 AND fdbk.week_review=1 AND fdbk.status=1 AND fdbk.deleted=0  ORDER BY fdbk.id DESC LIMIT 4";
    $res = $db->sql_query($query);
    $data = $db->sql_fetchrowset($res); //echo '<pre>',print_r($data),'</pre>';die;

    $queryRating = "SELECT  `quality_of_care`, `caring_staff`, `responsive_management`, `trips_outdoor_activities`,"
    . " `indoor_entertainment`, `social_atmosphere`, `enjoyable_food`, `overall_rating` FROM " . _prefix("rating_score");
    $resRating = $db->sql_query($queryRating);
    $dataRating = $db->sql_fetchrowset($resRating);
    //print_r($dataRating);
    $QualityOfServiceWgt = $dataRating[0]['quality_of_care'];
    $cStaffWgt = $dataRating[0]['caring_staff'];
    $resManWgt = $dataRating[0]['responsive_management'];
    $tripWgt = $dataRating[0]['trips_outdoor_activities'];
    $indoorEntWgt = $dataRating[0]['indoor_entertainment'];
    $socialWgt = $dataRating[0]['social_atmosphere'];
    $foodWgt = $dataRating[0]['enjoyable_food'];
    $overAllRatingWgt = $dataRating[0]['overall_rating'];
    $TotalRating = 0;
    $weightSumation = $QualityOfServiceWgt + $cStaffWgt + $resManWgt + $tripWgt + $indoorEntWgt + $socialWgt + $foodWgt + $overAllRatingWgt;
    $reviews = '';


    foreach ($data as $key => $record) {
        if ($record['feedback'] != '') {
            if (@getimagesize(DOCUMENT_PATH . 'admin/files/gallery/images/thumb_feedback_' . get_first_image_from_gallery($record['pImage']))) {
                $image = '<a href="' . HOME_PATH . $record['quick_url'] . '"><img src="' . HOME_PATH . 'admin/files/gallery/images/thumb_feedback_' . get_first_image_from_gallery($record['pImage']) . '" alt="product image" /></a>';
            } else {
                $image = '<a href="' . HOME_PATH . $record['quick_url'] . '"><img src="' . HOME_PATH . 'images/no_image_available.png" alt="" /></a>';
            }
            $TotalRating = $record['overall_rating'];

            $totratingpercent = $TotalRating * 20;
            $str = '<div class="star-inactive"><div class="star-active" style="width:' . $totratingpercent . '%;"></div></div>';
            $overallRating = round($TotalRating);
            switch ($overallRating) {
                case 1:
                    $all_Rating = '<i class="fa fa-star star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>';
                    break;
                case 2:
                    $all_Rating = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>';
                    break;
                case 3:
                    $all_Rating = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>';
                    break;
                case 4:
                    $all_Rating = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star"></i>';
                    break;
                case 5:
                    $all_Rating = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i>';
                    break;
                default:
                    $all_Rating = 'Not Rated';
                    break;
            }
            $commenter = ($record['anonymous'] == 1) ? '[Name withheld at users request]' : $record['csUsername'];
            // $feedback = (strlen($record['feedback']) > 60) ? substr($record['feedback'], 0, 60) . '...' : $record['feedback'];
            $pros = (strlen($record['pros']) > 20) ? substr($record['pros'], 0, 200) . '...' : $record['pros'];
            $reviews .= '<div class="col-md-2 col-sm-2 col-xs-12 review_thumb">' . $image . '</div>
                            <div class="col-md-10 col-sm-10 col-xs-12 pdding_right padding_left_col">
                                    <h3 class="hedding_three"><a href="' . HOME_PATH . $record['quick_url'] . '#reviews">' . $record['title'] . '</a></h3>
                                    <p class="m-0">' . $str . '</p>
                                    <p><span class="dark-text">Product/Service Name:&nbsp;<a href="' . HOME_PATH . $record['quick_url'] . '">' . substr($record['pdtitle'], 0, 60) . '</a><br/>
                                            <span class="dark-text">Commented By:&nbsp;' . $commenter . '</span>';
            if ($record['feedback_reply']) {
                $reviews .= '<br><span class="dark-text">Includes reply from ' . substr($record['pdtitle'], 0, 60) . '</span>';
            }

            $timeDiff = (strtotime(date("Y-m-d h:i:sa")) - strtotime($record['week_review_time'])) / (60 * 60 * 24);
            $reviews .= '</p><p class="margin_bottom">' . $pros . '</p></div><div class="espesar"></div>';
        }
    }
    //     usort($data, "cmp");
    if ($timeDiff > 7)
        $reviews = '';
    
    return $reviews;
}

function getReviews() 
{
    global $db;
    $query = "SELECT fdbk.* , pds.title as pdtitle,pds.id as prId,pds.id AS pImage, sp.user_name as spUsername, sp.first_name AS spFName, sp.last_name AS spLName, cs.user_name AS csUsername, cs.first_name AS csFName, cs.last_name AS csLName ,pds.quick_url,  pds.address_city FROM " . _prefix("feedbacks") . " AS fdbk   "
    . "LEFT JOIN " . _prefix("products") . " AS pds ON  fdbk.product_id= pds.id "
    . "LEFT JOIN " . _prefix("users") . " AS sp ON  fdbk.sp_id= sp.id "
    . "LEFT JOIN " . _prefix("users") . " AS cs ON  fdbk.cs_id= cs.id "
    . "WHERE fdbk.abused_request=0 AND fdbk.abused=0 AND fdbk.week_review=0 AND fdbk.status=1 AND fdbk.deleted=0  ORDER BY fdbk.id DESC LIMIT 14";

    $res = $db->sql_query($query);
    $data = $db->sql_fetchrowset($res); //echo '<pre>',print_r($data),'</pre>';die;
    
    $queryRating = "SELECT  `quality_of_care`, `caring_staff`, `responsive_management`, `trips_outdoor_activities`,"
    . " `indoor_entertainment`, `social_atmosphere`, `enjoyable_food`, `overall_rating` FROM " . _prefix("rating_score");
    $resRating = $db->sql_query($queryRating);
    $dataRating = $db->sql_fetchrowset($resRating);
    
    $QualityOfServiceWgt = $dataRating[0]['quality_of_care'];
    $cStaffWgt = $dataRating[0]['caring_staff'];
    $resManWgt = $dataRating[0]['responsive_management'];
    $tripWgt = $dataRating[0]['trips_outdoor_activities'];
    $indoorEntWgt = $dataRating[0]['indoor_entertainment'];
    $socialWgt = $dataRating[0]['social_atmosphere'];
    $foodWgt = $dataRating[0]['enjoyable_food'];
    $overAllRatingWgt = $dataRating[0]['overall_rating'];
    $TotalRating = 0;
    $weightSumation = $QualityOfServiceWgt + $cStaffWgt + $resManWgt + $tripWgt + $indoorEntWgt + $socialWgt + $foodWgt + $overAllRatingWgt;
    $reviews = '';

    foreach ($data as $key => $record) {
        $know_me_id = $record['know_me'];

        if ($know_me_id == 1) {
            $know_me_title = "Resident";
        }
        if ($know_me_id == 2) {
            $know_me_title = "Family member / Friend";
        }
        if ($know_me_id == 3) {
            $know_me_title = "Staff Member";
        }
        if ($know_me_id == 4) {
            $know_me_title = "Visitor";
        }
        if ($know_me_id == 0 || $know_me_id > 4) {
            $know_me_title = "Unspecified";
        }
        if ($record['visit_duration'] == 1) {
            $visit_duration = "< 10 days / visits";
        }
        if ($record['visit_duration'] == 2) {
            $visit_duration = "10 - 100 days / visits";
        }
        if ($record['visit_duration'] == 3) {
            $visit_duration = "> 100 days / visits";
        }
        if ($record['visit_duration'] == 0 || $record['visit_duration'] > 3) {
            $visit_duration = "Unspecified";
        }
        $last_visit = $record['last_visit'];

        $feedback_created = $record['created'];
        $feedback_date = date("Y-m-d", strtotime($feedback_created));
        $feedback_fetch_date = date("d", strtotime($feedback_date));
        $feedback_fetch_month = date("m", strtotime($feedback_date));
        $feedback_monthName = date("M", mktime(0, 0, 0, $feedback_fetch_month, 10));
        $feedback_fetch_year = date("Y", strtotime($feedback_date));
        
        if ($record['feedback'] != '') 
		{
             
            if (@getimagesize(DOCUMENT_PATH . 'admin/files/gallery/images/thumb_feedback_' . get_first_image_from_gallery($record['pImage']))) 
			{
                $image_path = HOME_PATH . 'admin/files/gallery/images/thumb_feedback_' . get_first_image_from_gallery($record['pImage']);
;
            } else {
                $image_path = HOME_PATH . 'images/no_image_available.png';
            }
           
            $TotalRating = $record['overall_rating'];
            $totratingpercent = $TotalRating * 20;
            
            echo "<style type='text/css'>"
            . ".star-width-".$totratingpercent
            . "{ width:".$totratingpercent."%;"
            . "} </style>";
            
            $str = '<div class="star-inactive">
			<div class="star-active star-width-' . $totratingpercent . '">
			</div>
			</div>';
            $overallRating = round($TotalRating);
             
            $commenter = ($record['anonymous'] == 1) ? '[Name withheld at users request]' : $record['csUsername'];
            // $feedback = (strlen($record['feedback']) > 60) ? substr($record['feedback'], 0, 60) . '...' : $record['feedback'];
            $pros = (strlen($record['pros']) > 20) ? substr($record['pros'], 0, 200) . '...' : $record['pros'];
            $reviews .= '<div class="item"><div class="pop_category_wrap reviews_slide_wrap"><div class="video_img reviews_slide_img"><div class="reviews_left_img"><img src="<?php echo HOME_PATH; ?>includes/img/yoga.jpg" alt="img">
			<div itemscope itemtype="http://schema.org/Product" class="swiper-slide coupon-content">
             <meta itemprop="name" content = "'.$record['pdtitle'].'"/>
              <meta itemprop="url" content = "' . HOME_PATH . $record['quick_url'] . '"/>
               <div  itemprop="review" itemscope itemtype="http://schema.org/Review">
                                    <div class="row" >
                                         
                                        <div class="col-md-12">
										<div class="coupon-owner">
                                               <h3 class="hedding_three">
		<a href="' . HOME_PATH . $record['quick_url'] . '#reviews">
		<span itemprop="name">' . $record['title'] . '</span>
		</a></h3>
		
		<p style="width:"'.$totratingpercent.'"%;">' . $str . '</p>
		 
		 
				<p class="review-by">By : <span itemprop="author">' . $commenter . '</span>
			                 <meta itemprop="datePublished" content="'.$feedback_date.'">on&nbsp;' . $feedback_monthName . ' ' . $feedback_fetch_year . '
							 ' . $know_me_title . ' / ' . $visit_duration . ' 
						<div itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating">
                        <meta itemprop="worstRating" content = "1"/>
                        <meta itemprop="ratingValue" content = "'.$overallRating.'"/>
                        <meta itemprop="bestRating" content = "5"/>
                        </div>	

 
							 
					 						
		</p>
                                            </div>
										</div>
										
                                       
                                    </div>
                                    <div class="row align-items-center">
                                         
                                        <div class="col-md-12">
                                            <div class="float-left">
                                                <p><span class="dark-text"> <a href="' . HOME_PATH . $record['quick_url'] . '">' . substr($record['pdtitle'], 0, 60) . ', ' . $record['address_city'] . '</a><br/>';
            if ($record['feedback_reply']) {
                $reviews .= '<br><span class="dark-text">Includes reply from ' . substr($record['pdtitle'], 0, 60) . '</span>';
            }


            $reviews .= '</p>
			<p class="margin_bottom">
			<span itemprop="description">' . $pros . '</span></p>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                </div>';
        }
    }
    return $reviews;
}

/* * *********************************end  of  code for get right banner according city name of current user************* */
?>

<main>
    
    <div class="main_pege_slider">
    
        <div class="owl-carousel owl-theme" id="main-slider">
        
            <div class="item">
            
                <img src="<?php echo HOME_PATH . 'includes/img/banner2.jpg'; ?>" alt="img">
                
            </div>
            
            <div class="item">
            
                <img src="<?php echo HOME_PATH . 'includes/img/banner.jpg'; ?>" alt="img">
                
            </div>
            
        </div>
        
        <div class="slider_overlay"></div>

        <div class="content_section_wrap">
        
            <div class="slider_content_inner">
            
                <div class="container">
                
                    <h1 class="wow fadeInUp" data-wow-delay="0.5s">Find, Review & Compare Rest Homes, Retirement Villages & Agedcare</h1>
                      
                    <div class="slider_search">
                    
                        <form class="wow fadeInUp" action="<?php echo HOME_PATH; ?>search/for" method="post" data-wow-delay="0.7s">
                        
                            <input type="text" placeholder="What are you looking for? " name="search">
                            
                            <button type="submit">Search</button>
                           
                        </form>
                        
                    </div>
                    
                </div>
                
            </div>
            
        </div>
        
    </div>

    <section class="middle_content_wrap wow fadeInUp" data-wow-delay="0.5s">
              
        <div class="container">
        
            <div class="popular_categories_wrap">
            
                <h2 class="inner_section_heading">Popular Categories</h2>

                <div class="popular_categories_inner_wrap">
                
                    <div class="row">
                    
                        <div class="col">
                        
                            <div class="pop_category_wrap" style="background-image:url('<?php echo HOME_PATH; ?>includes/img/cate_img.jpg');">
                            
                                <div class="pop_category_overlay">
                                
                                    <a href="javascript:void();">
                                        
                                        <h3>Villas</h3>
                                    
                                    </a>
                                 
                                </div>

                             
                            </div>
                          
                        </div>
                        
                        <div class="col">
                        
                            <div class="pop_category_wrap" style="background-image:url('<?php echo HOME_PATH; ?>includes/img/cate_img.jpg');">
                            
                                <div class="pop_category_overlay">
                                
                                    <a href="javascript:void();">
                                        
                                        <h3>Apartments</h3>
                                    
                                    </a>
                                 
                                </div>
                             
                            </div>
                          
                        </div>

                        <div class="col">
                        
                            <div class="pop_category_wrap" style="background-image:url('<?php echo HOME_PATH; ?>includes/img/cate_img.jpg');">
                            
                                <div class="pop_category_overlay">
                                
                                    <a href="javascript:void();">
                                        
                                        <h3>Rest Home</h3>
                                    
                                    </a>
                                 
                                </div>
                             
                            </div>
                          
                        </div>

                        <div class="col">
                        
                            <div class="pop_category_wrap" style="background-image:url('<?php echo HOME_PATH; ?>includes/img/cate_img.jpg');">
                            
                                <div class="pop_category_overlay">
                                
                                    <a href="javascript:void();">
                                        
                                        <h3>Hospital</h3>
                                    
                                    </a>
                                 
                                </div>
                             
                            </div>
                          
                        </div>

                        <div class="col">
                        
                            <div class="pop_category_wrap" style="background-image:url('<?php echo HOME_PATH; ?>includes/img/cate_img.jpg');">
                            
                                <div class="pop_category_overlay">
                                
                                    <a href="javascript:void();">
                                        
                                        <h3>Dementia</h3>
                                    
                                    </a>
                                 
                                </div>
                             
                            </div>
                          
                        </div>

                    </div>

                </div>

            </div>

        </div>

    </section>

    <section class="middle_content_wrap wow fadeInUp" data-wow-delay="0.5s">
    
        <div class="container">
        
            <div class="regions_wrap">
            
                <h2 class="inner_section_heading">Regions</h2>

                <div class="regions_inner_wrap">
                
                    <div class="owl-carousel owl-theme" id="regions_slider">
                  
                        <div class="item">
                        
                            <div class="pop_category_wrap" style="background-image:url('<?php echo HOME_PATH; ?>includes/img/img1.jpg');">
                        
                                <div class="pop_category_overlay">
                            
                                    <a href="javascript:void();"> 
                                
                                        <h3>China</h3>
                                    
                                        <p>Subheading Goes Here</p> 
                                  
                                    </a>
                                 
                                </div>
                             
                            </div>
                  
                        </div>
                   
                        <div class="item">
                    
                            <div class="pop_category_wrap" style="background-image:url('<?php echo HOME_PATH; ?>includes/img/img1.jpg');">
                          
                                <div class="pop_category_overlay">
                              
                                    <a href="javascript:void();">
                                        
                                        <h3>Dementia</h3>
                                
                                        <p>Subheading Goes Here</p> 
                                
                                    </a>
                                
                                </div>
                            
                            </div>
                    
                        </div>
                    
                    </div>
                    
                </div>
                
            </div>
            
        </div>
        
    </section>

    <section class="middle_content_wrap wow fadeInUp" data-wow-delay="0.5s">
    
        <div class="container">
        
            <div class="about_company_wrap">
            
                <h2 class="inner_section_heading">About AgedAdvisor</h2>
                
                <div class="about_company_wrap_inner">
                
                    <div class="row">
                    
                        <div class="col-md-7">
                        
                            <div class="about_img">
                            
                                <img src="<?php echo HOME_PATH; ?>includes/img/about_img.jpg" alt="img">
                               
                            </div>
                            
                        </div>
                        
                        <div class="col-md-5">
                        
                            <div class="about_text">
                            
                                <img src="<?php echo HOME_PATH; ?>includes/img/logo.png" alt="img">
                                
                                <p>Helping you find the right retirement or restcare facilities.The team at Aged Advisor are locals that have first hand experience with Retirement Villages, Rest Homes and Aged Care facilities, either through our line of work (providing services to residents within these facilities) or by having family or loved ones living there.</p> 

                                <a href="<?php echo HOME_PATH; ?>about-us">
                                    
                                    Explore AgedAdvisor <i class="fas fa-chevron-right"></i>
                                
                                </a>
                               
                            </div>
                            
                        </div>
                        
                    </div>
                    
                </div>
                
            </div>
            
        </div>
        
    </section>

    <section class="middle_content_wrap wow fadeInUp" data-wow-delay="0.5s">
    
        <div class="container">
        
            <div class="regions_wrap">
            
                <h2 class="inner_section_heading">Latest Village Videos</h2>
                
                <div class="regions_inner_wrap">
                
                    <div class="owl-carousel owl-theme" id="latest_village_videos_slider">
                  
                        <div class="item">
                      
                            <div class="pop_category_wrap">
                            
                                <div class="video_img">
                                
                                    <img src="<?php echo HOME_PATH; ?>includes/img/video_icon.jpg" alt="img">
                                
                                    <div class="play_icon">
                                
                                        <a><i class="fas fa-play"></i></a>
                                
                                    </div>
                             
                                </div>

                                <div class="video_slide_content">
                                    
                                    <h3>Heading Goes Here</h3>
                                
                                    <ul>
                                  
                                        <li><i class="fas fa-circle"></i> Fitness Cente</li>
                                  
                                        <li><i class="fas fa-circle"></i> Bicycle Hire</li>
                                  
                                        <li><i class="fas fa-circle"></i> Bar</li>
                                  
                                        <li><i class="fas fa-circle"></i> Room service</li>
                                  
                                        <li><a href="javascript:void(0);">Read More</a></li>
                                  
                                        <div class="clearfix"></div>
                                
                                    </ul>
                                
                                    <div class="video_location_wrap">
                                
                                        <a>
                                   
                                            <img src="<?php echo HOME_PATH; ?>includes/img/location.png" alt="img">
                                   
                                            <span>Auckland</span>
                                 
                                        </a>
                                
                                    </div>
                                
                                    <div class="video_review">
                                
                                        <span>
                                    
                                            <i class="fas fa-star"></i> 
                                      
                                            <i class="fas fa-star"></i> 
                                      
                                            <i class="fas fa-star"></i> 
                                      
                                            <i class="fas fa-star gray"></i> 
                                      
                                            <i class="fas fa-star gray"></i> 
                                  
                                        </span>
                                  
                                        <span class="review_count">(253 review)</span>
                                
                                    </div>
                             
                                </div>
                             
                            </div>
                  
                        </div>
              
                    </div>
                   
                </div>
                
            </div>
            
        </div>
        
    </section>

    <section class="middle_content_wrap wow fadeInUp" data-wow-delay="0.5s">

        <div class="container">
    
            <div class="top_rated_wrap">
        
                <h2 class="inner_section_heading">Top Rated Facilities</h2>
            
                <p class="inner_section_subheading">This would be Featured Facilities</p>

                
            
                <div class="regions_inner_wrap">
            
                    <div class="owl-carousel owl-theme" id="top_rated_facilities">
                
                        <div class="item">
                    
                            <div class="pop_category_wrap">
                        
                                <div class="video_img">
                            
                                    <img src="<?php echo HOME_PATH; ?>includes/img/img2.jpg" alt="img">
                                
                                    <div class="play_icon wishlist_facilites">
                                
                                        <a><i class="fas fa-heart"></i></a>
                                
                                    </div>
                             
                                </div>

                                <div class="video_slide_content">
                                
                                    <h3>Julia Wallace Retirement Village</h3>
                                    
                                    <div class="video_review">
                                    
                                        <span>
                                        
                                            <i class="fas fa-star"></i> 
                                        
                                            <i class="fas fa-star"></i> 
                                        
                                            <i class="fas fa-star"></i> 
                                        
                                            <i class="fas fa-star gray"></i> 
                                        
                                            <i class="fas fa-star gray"></i> 
                                    
                                        </span>
                                    
                                        <span class="review_count">(253 review)</span>
                                    
                                    </div>
                                    
                                    <div class="video_location_wrap">
                                    
                                        <a>
                                    
                                            <img src="<?php echo HOME_PATH; ?>includes/img/location.png" alt="img">
                                    
                                            <span>Clearview Park</span>
                                    
                                        </a>
                                    
                                    </div>
                                
                                </div>
                                
                            </div>
                    
                        </div>
              
                    </div>
                   
                </div>
                
            </div>
            
        </div>
        
    </section>

    <section class="middle_content_wrap wow fadeInUp" data-wow-delay="0.5s">
    
        <div class="container">
        
            <div class="top_rated_wrap">
            
                <h2 class="inner_section_heading">Compare Village fees and Pricing</h2>
                
                <p class="inner_section_subheading">Know someone that would like to place a review but doesn't have access to a computer?</p>

                <div class="compare_pricing_inner_wrap">
                
                    <a href="<?php echo HOME_PATH; ?>Compare-Retirement-Villages-Entry-Age-Costs-Fees-Conditions">View <i class="fas fa-chevron-right"></i></a>
                   
                </div>
                
            </div>
            
        </div>
        
    </section>

    <section class="middle_content_wrap wow fadeInUp" data-wow-delay="0.5s">
    
        <div class="container">
        
            <div class="latest_reviews_wrap">
            
                <h2 class="inner_section_heading">Latest Reviews</h2>
                
                <div class="regions_inner_wrap">
                
                    <div class="owl-carousel owl-theme" id="latest_reviews_slider">
                  
                    <div class="item">

                        <div class="pop_category_wrap reviews_slide_wrap">

                            <div class="video_img reviews_slide_img">
    
                                <div class="reviews_left_img">
        
                                    <img src="<?php echo HOME_PATH; ?>includes/img/yoga.jpg" alt="img">

                                </div>

                                <div class="review_right_text">

                                    <p>Yoga and Dance</p>
    
                                    <div class="video_location_wrap">
    
                                    <a>
    
                                        <img src="<?php echo HOME_PATH; ?>includes/img/location.png" alt="img">
        
                                        <span>Clearview Park</span>
        
                                    </a>
        
                                </div> 
        
                                <div class="video_review">
        
                                <span>
        
                                    <i class="fas fa-star"></i> 
            
                                    <i class="fas fa-star"></i> 
            
                                    <i class="fas fa-star"></i> 
            
                                    <i class="fas fa-star gray"></i> 
            
                                    <i class="fas fa-star gray"></i> 
            
                                </span>
            
                                </div> 
         
                            </div>
       
                        </div>

                        <div class="video_slide_content">
         
                            <span class="left_quote"><i class="fas fa-quote-left"></i></span>
         
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s. When an unknown printer took a galley of type and scrambled.</p>
         
                            <span class="right_quote"><i class="fas fa-quote-right"></i></span>
       
                        </div>

                        <div class="user_name_wrap">
          
                            <h4>John Smith</h4>
       
                        </div>

                    </div>
                    
                </div>
                
            </div>
            
        </div>
        
    </section>

    <section class="middle_content_wrap wow fadeInUp" data-wow-delay="0.5s">
    
        <div class="container">
        
            <div class="regions_wrap">
            
                <h2 class="inner_section_heading">Popular Categories</h2>

                <div class="regions_inner_wrap">
                
                    <div class="owl-carousel owl-theme" id="popular_catergories_slider">
                  
                        <div class="item">
                      
                            <div class="pop_category_wrap" style="background-image:url('<?php echo HOME_PATH; ?>includes/img/pop_cat.jpg');">
                            
                                <div class="pop_category_overlay">
                                
                                    <a href="javascript:void();"> 
                                    
                                        <h3>Heading Goes Here</h3>
                                    
                                        <p>Subheading Goes Here</p> 
                                  
                                    </a>
                                 
                                </div>
                             
                            </div>
                  
                        </div>
                    
                    </div>
                    
                </div>
                
            </div>
            
        </div>
        
    </section>

    <section class="middle_content_wrap wow fadeInUp" data-wow-delay="0.5s">
    
        <div class="container">
        
            <div class="regions_wrap">
            
                <h2 class="inner_section_heading">Top Rated Facilities</h2>

                <div class="regions_inner_wrap">
                
                    <div class="owl-carousel owl-theme" id="top_ratedslider2">
                  
                        <div class="item">
                      
                            <div class="pop_category_wrap" style="background-image:url('<?php echo HOME_PATH; ?>includes/img/top_rate.jpg');">
                            
                                <div class="pop_category_overlay">
                                
                                    <a href="javascript:void();"> 
                                    
                                        <h3>Heading Goes Here</h3>
                                    
                                        <p>Subheading Goes Here</p> 
                                  
                                    </a>
                                 
                                </div>
                             
                            </div>
                  
                        </div>
                
                    </div>
                
                </div>
                
            </div>
            
        </div>
        
    </section>

    <section class="middle_content_wrap wow fadeInUp" data-wow-delay="0.5s">
    
        <div class="container">
        
            <div class="regions_wrap">
            
                <h2 class="inner_section_heading">Trending Searches</h2>

                <div class="regions_inner_wrap">
                
                    <div class="owl-carousel owl-theme" id="trending_searches_slider">
                  
                        <div class="item">
                      
                            <div class="pop_category_wrap" style="background-image:url('<?php echo HOME_PATH; ?>includes/img/top_search.jpg');">
                            
                                <div class="pop_category_overlay">
                                
                                    <a href="javascript:void();"> 

                                        <h3>Heading Goes Here</h3>
    
                                        <p>Subheading Goes Here</p> 
    
                                    </a>
    
                                </div>
    
                            </div>
    
                        </div>
                    
                    </div>
                    
                </div>
                
            </div>
            
        </div>
        
    </section>

    <section class="middle_content_wrap wow fadeInUp" data-wow-delay="0.5s">
    
        <div class="container">
        
            <div class="regions_wrap">
            
                <h2 class="inner_section_heading">Hints & Tips Videos</h2>
                
                <div class="regions_inner_wrap">
                
                    <div class="owl-carousel owl-theme" id="hints_tips_videos_slider">

                        <div class="item">
    
                            <div class="pop_category_wrap">
        
                                <div class="video_img">
            
                                    <img src="<?php echo HOME_PATH; ?>includes/img/video_icon.jpg" alt="img">
                
                                    <div class="play_icon">
                
                                        <a><i class="fas fa-play"></i></a>
                    
                                    </div>
                    
                                </div>

                                <div class="video_slide_content">
                            
                                    <h3>Heading Goes Here</h3>
                                
                                    <ul>
                                
                                        <li><i class="fas fa-circle"></i> Fitness Cente</li>
    
                                        <li><i class="fas fa-circle"></i> Bicycle Hire</li>
    
                                        <li><i class="fas fa-circle"></i> Bar</li>
    
                                        <li><i class="fas fa-circle"></i> Room service</li>
    
                                        <li><a href="javascript:void(0);">Read More</a></li>
    
                                        <div class="clearfix"></div>
    
                                    </ul>
    
                                    <div class="video_location_wrap">
    
                                        <a>
        
                                            <img src="<?php echo HOME_PATH; ?>includes/img/location.png" alt="img">
            
                                            <span>Auckland</span>
            
                                        </a>
            
                                    </div>
            
                                    <div class="video_review">
            
                                        <span>
                
                                            <i class="fas fa-star"></i> 
                    
                                            <i class="fas fa-star"></i> 
                        
                                            <i class="fas fa-star"></i> 
                        
                                            <i class="fas fa-star gray"></i> 
                        
                                            <i class="fas fa-star gray"></i> 
                        
                                        </span>
                        
                                        <span class="review_count">(253 review)</span>
                        
                                    </div>
                        
                                </div>
                             
                            </div>
                  
                        </div>
              
                    </div>
                   
                </div>
                
            </div>
            
        </div>
        
    </section>

    <section class="middle_content_wrap wow fadeInUp" data-wow-delay="0.5s">
    
        <div class="container">
        
            <div class="top_rated_wrap">
            
                <h2 class="inner_section_heading">Download a Review Form</h2>
                
                <p class="inner_section_subheading">Know someone that would like to place a review but doesn't have access to a computer?</p>
                
                    <div class="compare_pricing_inner_wrap download_review_wrap">
                    
                    <a href="<?php echo HOME_PATH; ?>images/agedadvisor-Review-Sheet-A4-PRINT.pdf">Download <i class="fas fa-chevron-right"></i></a>
                
                </div>
                
            </div>
            
        </div>
        
    </section>

    <section class="middle_content_wrap wow fadeInUp" data-wow-delay="0.5s">
    
        <div class="container">
        
            <div class="top_rated_wrap popular_posts_wrap">
            
                <h2 class="inner_section_heading">Popular Posts</h2>
                  
                <div class="regions_inner_wrap">
                
                    <div class="owl-carousel owl-theme" id="popular_posts_slider">
                  
                        <div class="item">
                      
                            <div class="pop_category_wrap">
    
                                <div class="video_img">
        
                                    <img src="<?php echo HOME_PATH; ?>includes/img/img2.jpg" alt="img">
            
                                </div>

                                <div class="video_slide_content">
            
                                    <h3>Heading Goes Here</h3>
                
                                    <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.</p>
                                
                                </div>
                                
                            </div>
                
                        </div>

                    </div>

                </div>

            </div>

        </div>

    </section>

    <section class="logos_slider_wrap wow fadeInUp" data-wow-delay="0.5s">
    
        <div class="container">
        
            <div class="logos_inner_wrap">
            
                <?php echo getSponsorsLogo(); ?>

            </div>		
        </div>
    
    </section>

</main>