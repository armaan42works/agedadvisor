<!---------------
Objective: Provide form to send request to change password
filename : forget_password.php
Created By : Sanket Khanna <sanket.khanna@ilmp-tech.com>
Created : 14 August 2014
-->
<?php
    global $db;
    // getting the email id from forgot password page
      if(isset($submit) && $submit=='Submit') { 
       
         $id = $pathInfo['call_parts']['1'];
         $fields = array(
            'password' => md5($password),
            );
          $where = "where password = '$id'";
          $update_result = $db->update(_prefix('users'), $fields, $where); 
          
          if ($update_result) {
                // Message for insert
                $msg = common_message(1, constant('CHANGE_PASSWORD'));
                $_SESSION['msg'] = $msg;
                $url = HOME_PATH.'login';
                //ends here
                 redirect_to($url);
               
            }
      }
    ?>

<section id="slider_container">
<div class="slider_bg">
<ul class="slider">
<li><img src="<?php echo HOME_PATH; ?>images/slider_img.png" alt=""></li>
</ul>

</div>
</section>

<section id="body_container">
<div class="wrapper">

<div class="main_box">
<!----left container STARTS------>
<div class="left_container">
<div class="login_container">
<h3>Change Password</h3>
<form id="changePassword" name="forgetPassword" method="post" class="login_account request_form" style="width:60%; padding-left: 21%;">
<div class="col">
  <input type="password" name="password" id="password" class="input_login required password_require" placeholder="New Password">
</div>    
<div class="col">
  <input type="password" name="c_password" id="c_password" equalTo='#password' class="input_login required password_require" placeholder="Confirm New Password">
</div>
<div class="col">
    <input type="submit" name="submit" id="button" class="creat_account_btn" value="Submit"> 
</div>

</form>

</div>

</div>
<!----right container STARTS------>
<div class="right_container">
<div class="about_col none signup_login">
<h3>News letter Sign Up</h3>
<p>Want to keep up to date with all our latest news and information? Enter your name and email below to be added to our mailing list.</p>
<div class="newletter_bg">
<h3>Join our Newsletter</h3>
<form id="form1" name="form1" method="post" class="joinus">
<input type="text" placeholder="Your Name">
<input type="password" placeholder="Your E-mail">
<a href="#" class="subscribe">Subscribe</a>
</form>






</div>




</div>

</div>
</div>
</div>
</section>
<script type="text/javascript">
$(document).ready(function(){
   $('#changePassword').validate();
});


</script>
