<?php
       /*
     * Objective: Validation of account
     * filename : validate.php
     * Created By: Sanket Khanna <sanket.khanna@ilmp-tech.com>
     * Created On : 25 August 2014
     * Modified On : 01 September 2014
     */
    ?>
<section id="slider_container">
    <div class="slider_bg">
        <ul class="slider">
            <li><img src="<?php echo HOME_PATH; ?>images/slider_img.png" alt=""></li>
        </ul>

    </div>
</section>
<?php
    global $db;
    $id = $pathInfo['call_parts']['1'];
    $getData = "SELECT created, validate FROM " . _prefix("users") . " WHERE md5(id) = '$id'";
    $res = $db->sql_query($getData);
    $record = $db->sql_fetchrow($res);
    $time = date('Y-m-d H:i:s', strtotime($record['created']) + (60 * 60 * 4));
    $currentTime = date('Y-m-d H:i:s', strtotime(date('Ymdhis')));
    if ($record['validate'] == 0 && (strtotime($currentTime) <= strtotime($time))) {
        $uniqueId = randomId();
        $mysql_query = "UPDATE " . _prefix("users") . " SET validate = '1', unique_id= '$uniqueId' WHERE (md5(id) = '$id') && validate = '0'";
        $db->sql_query($mysql_query);
    $quoteId = uniqueId(9);
    $sql_update_quote = "SELECT id, password, name, email FROM " . _prefix("users") . " WHERE unique_id ='$uniqueId'";
    $resupdatequote = $db->sql_query($sql_update_quote);
    $num = $db->sql_numrows($resupdatequote);
    if ($num > 0) {
        $data = $db->sql_fetchrow($resupdatequote);
        $uniqueId = uniqueId(9);
        $salquoteId = "UPDATE " . _prefix("quotes") . " SET unique_id = '$uniqueId' Where client_id = '{$data['id']}'";
        $db->sql_query($salquoteId);
        if (strlen($data['password']) == 9) {
            $emailTemplate = emailTemplate('send_password');
            if ($emailTemplate['subject'] != '') {
                $name = $data['name'];
                $to = $data['email'];
                $message = str_replace(array('{email}', '{name}', '{password}'), array($to, $name, $data['password']), $emailTemplate['description']);
                $sendMail = sendmail($to, $emailTemplate['subject'], $message);
                if ($sendMail) {
                    $qoute_id = uniqueId(9);
                    $updateQuoteMail = "UPDATE " . _prefix("quotes") . " SET validate = '1', unique_id = '$qoute_id' WHERE client_id = '{$data['id']}'";
                    $db->sql_query($updateQuoteMail);
                } else {
                    $quote_delete = "UPDATE " . _prefix("quotes") . " SET deleted = '1' WHERE client_id= '{$data['id']}'";
                    $db->sql_query($quote_delete);
                }
            }
        }
    }
    } else {
        $expire = 1;
    }
    
?>
<section id="body_container">
    <div class="wrapper">
        <div class="main_box">
            <!----left container STARTS------>
            <div class="left_container">
                <div class="about_col aboutus_container">
                    <?php if($expire == 1){
                        echo "The link to validate account is expired. Please contact admin.";
                    } else {
                        ?>
                    <h3>Thanks for choosing KA Analytics & Technologies</h3>
                    <p>Your account is validated now.
                        <?php
                            if (strlen($data['password']) == 9) {
                                echo " We are sending you a password in the mail and please login with that password!! ";
                            }
                        ?>
                        <a href="<?php echo HOME_PATH ?>login">Click Here</a> for login</p>
                    <?php
                    } ?>
                    
                </div>

            </div>
            <!----right container STARTS------>
            <div class="right_container">
                <div class="about_col none about-right">
                    <h3>News letter Sign Up</h3>
                    <p>Want to keep up to date with all our latest news and information? Enter your name and email below to be added to our mailing list.</p>
                    <div class="newletter_bg">
                        <h3>Join our Newsletter</h3>
                        <form id="form1" name="form1" method="post" class="joinus">
                            <input type="text" placeholder="Your Name">
                            <input type="password" placeholder="Your E-mail">
                            <a href="#" class="subscribe">Subscribe</a>
                        </form>






                    </div>




                </div>

            </div>
        </div>
    </div>
</section>