<?php

    /* 
     * Objective  : Cleint can change the password
     * Filename : change_password.php
     * Created By : Yogesh Chandra Joshi <yogesh.joshi@ilmp-tech.com> 
     * Created On : 25 August 2014
 */
?>

<?php
    global $db;
 if(isset($submit) && $submit=='Submit') { 
     
     $id = $_SESSION["userId"] ;

if(count($_POST)>0) {
$sql_query = "SELECT *from "._prefix('users')."  WHERE id='" . $id . "'"; 
$res = $db->sql_query($sql_query);
 $records = $db->sql_fetchrowset($res);
 
  if (count($records)) {

if(md5($_POST["oldPassword"]) == $records[0]["password"]) {
    
     $fields = array(
            'password' => md5($newPassword),
            );
     $where = "where id = '$id'";
          $update_result = $db->update(_prefix('users'), $fields, $where); 
          if ($update_result) {
                // Message for insert
                $msg = common_message(1, constant('CHANGE_PASSWORD_SUCCESSFULLY'));
                $_SESSION['msg'] = $msg;
                
               
            }


} else{
     $msg = common_message(0, constant('OLD_PASSWORD'));
                $_SESSION['msg'] = $msg;
}
      }
      
}

 }
    ?>

<section id="body_container" class="Dashbord_main" >
      <ul class="breadcrumb">
 <li><a href="<?php echo HOME_PATH ?>user"><strong>HOME</strong></a></li>
 <li class="last"><a href="javascript:void(0);">Change Password</a></li>
 </ul>
   <div class="messag_send"></div>
   <div class="message_container">
<div class="login_container">
  
<div><?php
                                if (isset($_SESSION['msg'])) {
                                    echo $_SESSION['msg'];
                                    unset($_SESSION['msg']);
                                }
                            ?></div>
<form id="changePassword" name="changePassword" method="post" class="login_account request_form">
<div class="col">
     <label class="label_text">Old Password <span class="redCol">* </span> </label>  
    <input type="password" name="oldPassword" id="oldPassword" class="input_login required avoidfloat">
</div> 
<div class="col">
    <label class="label_text">New Password <span class="redCol">* </span> </label>
  <input type="password" name="newPassword" id="newPassword" class="input_login required password_require avoidfloat">
</div>    
<div class="col">
    <label class="label_text">Confirm Password <span class="redCol">* </span> </label>
  <input type="password" name="c_newPassword" id="c_newPassword" equalTo='#newPassword' class="input_login required password_require avoidfloat">
</div>
<div class="col">
    <input type="submit" name="submit" id="button" class="creat_account_btn" value="Submit"> 
</div>

</form>

</div>
   </div>
</section>
<script type="text/javascript">
$(document).ready(function(){
   $('#changePassword').validate() 
});
</script>