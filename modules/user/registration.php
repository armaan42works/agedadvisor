<!----------------
Objective : Client Registration Page
Filename : apply.php
Created By : Sanket Khanna <sanket.khanna@ilmp-tech.com>
Created On : 20 August 2014
----->
<?php
    $userId = $pathInfo['call_parts']['2'];
    if (isset($register) && $register == 'register') {

        $userData = array(
            'business' => $business,
            'industry' => $industry,
            'website' => $website,
            'name' => $name,
            'address' => $address,
            'country_id' => $c_country,
            'state_id' => $c_state,
            'phone' => $phone,
            'zipcode' => $zipcode,
            'validate ' => 2,
            'email' => $email
        );
        if (isset($password)) {
            $userData['password'] = md5($password);
        }
        $where = " Where md5(id) = '$userId'";
        $insert_user = $db->update(_prefix('users'), $userData, $where);
        unset($_SESSION['validate']);
        $getData = "SELECT * FROM " . _prefix('users') . " WHERE md5(id) = '$userId'";
        $res = $db->sql_query($getData);
        $data = $db->sql_fetchrow($res);
        $_SESSION['userId'] = $data['id'];
        $_SESSION['name'] = $data['name'];
        $_SESSION['userType'] = $data['user_type'];
        $url = HOME_PATH . 'user/dashboard';
        redirect_to($url);
    }

    $getData = "SELECT * FROM " . _prefix('users') . " WHERE md5(id) = '$userId'";
    $res = $db->sql_query($getData);
    $num = $db->sql_numrows($res);
    if ($num > 0) {
        $data = $db->sql_fetchrow($res);
        ?>
        <section id="slider_container" class="request_quteo_slider">
            <div class="slider_bg">
                <div class="request_quteo_center">
                    <h1>Register with Us</h1>


                </div>

            </div>
        </section>

        <section id="body_container">
            <div class="wrapper">

                <div class="main_box">
                    <!----left container STARTS------>
                    <div class="left_container">
                        <div class="login_container quote_container">
                            <h3>Registration Application Form</h3>
                            <div class="clientView career_container">
                                <form id="applyForm" class="request_form" name="applyForm" method="post" enctype="multipart/form-data">
                                    <div class=" left_col left_coll2">
                                        <div class="col">
                                            <label class="label_text">Business<span style="color:#CC0000">*</span></label>
                                            <input type="text" name="business" id="business" class="sales input_text input_width" value="<?php echo stripslashes($data['business']) ?>">
                                        </div>
                                        <div class="col">
                                            <label class="label_text">Industry</label>
                                            <input type="text" name="industry" id="industry" class="sales input_text input_width" value="<?php echo stripslashes($data['industry']) ?>">
                                        </div>
                                        <div class="col">
                                            <label class="label_text">Website</label>
                                            <input type="text" name="website" id="website" class="sales input_text input_width" value="<?php echo stripslashes($data['website']) ?>">
                                        </div>
                                        <div class="col">
                                            <label class="label_text">Name<span style="color:#CC0000">*</span></label>
                                            <input type="text" name="name" id="name" class="sales input_text input_width" value="<?php echo stripslashes($data['name']) ?>">
                                        </div> 
                                        <div class="col">
                                            <label class="label_text">Email Address<span style="color:#CC0000">*</span></label>
                                            <input type="text" name="email" id="email" class="sales input_text input_width" value="<?php echo stripslashes($data['email']) ?>">
                                        </div> 
                                        <div class="col">
                                            <label class="label_text">Street Address<span style="color:#CC0000">*</span></label>
                                            <textarea id="address" name="address" class="textarea input_text input_width"><?php echo stripslashes($data['address']); ?></textarea>
                                        </div>

                                        <div class="col">
                                            <label class="label_text">Country<span style="color:#CC0000">*</span></label>
                                            <select name="c_country" id="c_country"  class="input_text countryApply"><?php echo countryList($data['country_id']); ?></select>
                                        </div>

                                        <div class="col">
                                            <label class="label_text">State<span style="color:#CC0000">*</span></label>
                                            <select name="c_state" id="c_state" class="input_text input_width"><?php echo stateList($data['country_id'], $data['state_id']); ?></select>
                                        </div>   

                                        <div class="col">
                                            <label class="label_text">Zip Code<span style="color:#CC0000">*</span></label>
                                            <input type="text" name="zipcode" id="zipcode" class="input_text input_width" value="<?php echo $data['zipcode']; ?>">
                                        </div>

                                        <div class="col">
                                            <label class="label_text">Best phone number<span style="color:#CC0000">*</span></label>
                                            <input type="text" name="phone" id="phone" class="input_text input_width" value="<?php echo $data['phone']; ?>">
                                        </div>
                                        <?php
                                        if (strlen($data['password']) == 9 && $data['facebook_id'] == '' && $data['twitter_id'] == '') {
                                            ?>
                                            <div class="col">
                                                <label class="label_text">Password<span style="color:#CC0000">*</span></label>
                                                <input type="password" name="password" id="password" class="input_text input_width" value="">
                                            </div>
                                            <div class="col">
                                                <label class="label_text"> Confirm Password<span style="color:#CC0000">*</span></label>
                                                <input type="password" name="c_password" id="c_password" equalTo='#password' class="input_text input_width" value="">
                                            </div>
                                            <?php
                                        }
                                        ?>

                                        <div style="text-align:center;" class="row">
                                            <button id="application" name="register" value="register" class="submit_btn" type="submit">Register</button>
                                        </div> 
                                    </div>

                                </form>
                            </div>


                        </div>
                        <div style=" clear:both; width:100%;"></div>


                    </div>
                    <div class="right_container">
                        <div class="about_col none willget">




                        </div>

                    </div>

                </div>

            </div>
        </div>
        </section>
        <?php
    } else {
        $url = HOME_PATH . '/login';
        redirect_to($url);
    }
?>    


<script type="text/javascript">
    $(document).ready(function() {
        $('.sales').click(function() {
            var type = $(this).attr('id');
            if (type == 'exist') {
                window.location.href = '<?php echo HOME_PATH ?>login';
            }
        });


        $('#applyForm').validate({
            rules: {
                "name": {
                    "required": true,
                    "nameValidate": true
                },
                "business": {
                    "required": true
                },
                "address": {
                    "required": true
                },
                "c_country": {
                    "required": true
                },
                "c_state": {
                    "required": true
                },
                "zipcode": {
                    "required": true
                },
                "phone": {
                    "required": true,
                    "phoneUS": true
                },
                "email": {
                    "required": true,
                    "email": true
                }, "password": {
                    "required": true,
                    "password_require": true
                }, "c_password": {
                    "required": true,
                    "password_require": true
                }
            }
        });

        if ($('#c_country').val() == 229) {
            $('input[name="zipcode"]').rules('remove');
            $('input[name="zipcode"]').rules('add', {
                digits: true,
                required: true,
                maxlength: 5
            });
        }

        $('#c_country').on('change', function() {
            var coun_id = $(this).val();
            if (coun_id == 229) {
                $('input[name="zipcode"]').rules('remove');
                $('input[name="zipcode"]').rules('add', {
                    digits: true,
                    required: true,
                    maxlength: 5
                });
            } else {
                $('input[name="zipcode"]').rules('remove');
                $('input[name="zipcode"]').rules('add', {
                    required: true
                });
            }
        });


        $('.countryApply').on('change', function() {
            var country = $(this).val();
            if (country != 229) {
                $('#countryCode').show();
                $('#countryCode').val('');
            } else {
                $('#countryCode').hide();
                $('#countryCode').val('');
            }
            $.ajax({
                type: 'POST',
                url: '<?php echo HOME_PATH ?>admin/ajax.php',
                data: {action: 'stateList', country: country},
                success: function(response) {
                    $('#c_state').html($.trim(response));

                }
            });
        });

    });


</script>
