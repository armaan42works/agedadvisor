<?php
    /*
     * Objective  : Dashboard page for Client
     * Filename : dashboard.php
     * Created By : Sanket Khanna <sanket.khanna@ilmp-tech.com> 
     * Created On : 22 August 2014
     * Created On : 25 August 2014
     */
?>
<!----Dashbord_main START--------------------->
<section id="body_container" class="Dashbord_main">

    <div class="row">
        <div class="dashbord_tilles" >    
            <img  class="das_tile_img" src="<?php echo HOME_PATH ?>images/my_project_tile_img.png"/>
            <div class="das_til_heading">
                <h4>My Projects</h4>
                <p>List of all Projects</p>
            </div>
            <?php
    $sql_query = "SELECT * FROM " . _prefix('quotes') . " WHERE deleted = '0' && client_id = '{$_SESSION['userId']}'  ORDER BY id DESC LIMIT 0,5";
    $res = $db->sql_query($sql_query);
    $num = $db->sql_numrows($res);
    $datas = $db->sql_fetchrowset($res);
    if($num > 0){
        $i =1;
        ?>
            <ul>
                <?php
                    foreach($datas as $data){ 
                        $type = $data['status'] == 0 ? 'Q' : 'P';?>
                <li><a href="<?php echo HOME_PATH ?>user/quote/<?php echo md5($data['id']); ?>"><?php echo $type.$data['unique_id'].' - Added On '.date('M d, Y',strtotime($data['created'])); ?></a></li>        
                    <?php 
                        
                        if($i >= 4){
                            break;
                        }
                        $i++;
                    }
                    ?>
                <?php if($num >= 4){
                    ?>
                <span class="showmore"><a href="<?php echo HOME_PATH ?>user/project">Show More</a></span>
                <?php
                } ?>
                
            </ul>
            <?php
    } else {
             ?>
            <ul><li><div class='norecord'>No Record found</div></li></ul>
            <?php  
    }
                ?>
        </div>

        <div class="dashbord_tilles last" >
            <img class="myCoupn_tile_img" src="<?php echo HOME_PATH ?>images/mycoupn_tile_img.png"/>  
            <div class="myCoupn_til_heading">
                <h4>My Coupons/Gift Cards</h4>
                <p>Find all the valid coupons</p>
                <?php
             $getCoupon = "SELECT * FROM " . _prefix('coupons') . " AS DC WHERE "
                     . "DC.code NOT IN (SELECT coupon_id FROM " . _prefix('quotes') . " WHERE coupon_id IS NOT NULL) && "
            . "DC.client_type ='1' && date_format(DC.expire_date,'%Y-%m-%d') >= '" . date('Y-m-d') . "' ORDER BY DC.id DESC LIMIT 0,5";
             $resCoupon =  $db->sql_query($getCoupon);
             $num = $db->sql_numrows($resCoupon);
             $coupons = $db->sql_fetchrowset($resCoupon);
             ?>
            </div>
            <?php
                if($num > 0){
                    $i = 1;
                    ?>
            <ul class="mycoupn_li">
                <?php
                    foreach($coupons as $coupon){
                        ?>
                <li><a href="javascript:void(0);"><?php echo $coupon['code'].' - Expire On - '.date('M d, Y',strtotime($coupon['expire_date'])); ?></a></li>
                <?php
                    $i++;
                    if($i >= 4){
                        break;
                    }
                   
                    }
                     if($i >= 4){
                        ?><span class="showmore"><a href="<?php echo HOME_PATH ?>user/coupon">Show More</a></span><?php
                    }
                    ?>
                
            </ul>
            <?php
                } else {
                       ?>
            <ul><li><div class='norecord'>No Record found</div></li></ul>
            <?php
                }
                ?>
         </div>

    </div>
    <!----dashbord_row1 END--------------------->
    <!----dashbord_row2 START--------------------->
    <div class="row">
        <div class="dashbord_tilles">
            <img class="paymentReco_tile_img" src="<?php echo HOME_PATH ?>images/paymentReco_tile_img.png"/>  
            <div class="paymentReco_til_heading">
                <h4>Payment Records</h4>
                <p>Loreum ipsum is simply a dummy text</p>
            </div>
            <ul>
                <li><a href="javascript:void(0);">Project_Basic for UX Designer</a></li>
                <li><a href="javascript:void(0);">Project_10 steps to improve your wireframe</a></li>
                <li><a href="javascript:void(0);">Project_A Better Way To Request App Ratings</a></li>
                <li><a href="javascript:void(0);">Project_How To Speed Up Your WordPress Website</a></li>

                <span class="showmore"><a href="<?php echo HOME_PATH ?>user/payment">Show More</a></span>
            </ul>

        </div>


        <div class="dashbord_tilles last" >
            <img class="messageBox_tile_img" src="<?php echo HOME_PATH ?>images/messag_box_tile_img.png"/> 
            <div class="messageBox_til_heading">
                <h4>Message BOX (<?php echo $unread; ?> new messages)</h4>
                <p>Find all the conversation </p>
            </div>
            <?php
                $msg_query = "SELECT dm.*, du.name FROM " . _prefix('messages') . " AS dm LEFT JOIN " . _prefix('users') . " as du ON dm.sender_id = du.id  WHERE dm.receiver_id ={$_SESSION['userId']} ORDER BY dm.id DESC LIMIT 0,2";
                $res = $db->sql_query($msg_query);
                $msgs = $db->sql_fetchrowset($res);
                $num = $db->sql_numrows($res);
                if($num > 0){
                    $i = 1;
                    ?>
            <ul class="messagBox_li">
                <?php
                    foreach($msgs as $msg){
                     ?>
                <li><?php echo $msg['subject']; ?><br/>
                    By <?php echo $msg['name'] != '' ? $msg['name'] : 'KA Analytics'; ?><?php echo ' - '.date('M d, Y', strtotime($msg['created'])) ?></li>
                
                <?php
                    $i++;
                    if($i >= 4){
                        break;
                    }
                    }
                    if($i >= 4){
                        ?><span class="showmore"><a href="<?php echo HOME_PATH ?>user/message">Show More</a></span><?php
                    }
                    ?>

                
            </ul>
            <?php
                } else {
                    ?>
            <ul><li><div class='norecord'>No Record found</div></li></ul>
            <?php
                }
                ?>
            

        </div>

    </div>
    <!----dashbord_row2 END--------------------->
</section>
<!----Dashbord_main END--------------------->