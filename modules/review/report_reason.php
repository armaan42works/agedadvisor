
<?php
//require_once '../../includes/constant.php';
$cust_id = $_GET['cust_id'];
$abuse = $_GET['abuse'];
?>

<div class="row row_col">
    <div class="col-md-12" style="padding-top:4%;">
        <h3 class="pull-left col-lg-5 col-md-5 col-sm-12 col-xs-12" style="color: #f25822;"><i class="fa fa-list-alt"></i>&nbsp;Reason for reporting a review/comment</h3>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="">
		    <button onclick="window.history.go(-1); return false;" class="btn btn-info pull-right">&lt; Go Back</button>
	    </div>
        <p class="clearfix"></p>
        <div class="btn-default successForm success">
        </div><br>
        
        
        
        <form name="repoertReason" id="repoertReason" method="POST" action=""  class="form-horizontal" >
            <div class="form-group">
                

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><label for="reason">Reason&nbsp;<span class="redCol">* </span></label><br>
                    <textarea rows="4" width="100%"  maxlength="500" name="reason" class="required form-control" id="reason"><?php echo (isset($reason) && !empty($reason) ) ? stripslashes($reason) : ''; ?></textarea>
                </div>
            </div>        
            <input type="hidden" name="customer_id" id="customer_id" value="<?php echo $cust_id ?>" />

            <div class="form-group">
                <div class=" col-md-12 text-left">
                    <strong>Please refer to our Posting Guidelines below, before reporting a comment:</strong> Users have the freedom to post honest comments or reviews about experiences that they've had. They are posting their view, and therefore we recommend comments only be reported if;
<ul>
    <li>Individuals have been named.</li>
    <li>The comment clearly distorts facts.</li>
    <li>Generalisations have been made, eg. The use of words such as 'never' and 'always'.</li>
    <li>The comments are defamatory, illegal, offensive, threatening, discriminatory, blasphemous or contain bad language.</li></ul>

                </div>
                <div class=" col-md-12 text-center">
                    <input type="submit" value="Submit" name="submit" class="submit_btn btn btn-danger ">
                </div>
            </div>
        </form>
    </div>

<style>
    label.error{
        font-size:10px;
    }
</style>
<script>
	function goBack() {
    window.history.back()
	}

    $(document).ready(function() {
        $(".success").hide();
        $('#repoertReason').validate({
            rules: {
                reason: {
                    required: true,
                    minlength: 3,
                    maxlength: 500
                }
            },
            messages: {
                reason: {
                    required: "This field is required",
                    minlength: "Reason must be contain atleast 3 chars",
                    maxlength: "Reason should not be contain more than 500 chars"
                }
            }
        });
        $(".fancybox").fancybox();
        $("#repoertReason").submit(function() {
            var table = 'feedbacks';
            var id = '<?php echo $cust_id; ?>';
            var abuse = '<?php echo $abuse; ?>';
            var reason = $('#reason').val();
            if ($(this).validate().form()) {
                $.ajax({
                    type: 'POST',
                    url: '<?php echo HOME_PATH; ?>ajaxFront.php',
                    data: {action: 'abuse', id: id, table: table, abuse: abuse, reason: reason},
                    success: function(response) {
                        $('.success').html('<i class="fa fa-check-square"></i>&nbsp;Thank you for reporting the comment. We have now hidden the review/comment until our team moderates it.');
                        $(".success").show();
                        $(".form-group").hide();
                    }
                });
            }
            return false;
        });
    });
</script>