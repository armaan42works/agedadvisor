 
     <div class="call-to-action pad-tb-70" style="background-image: url(<?php echo HOME_PATH;?>/modules/pages/images/bg/memphis-colorful.png);height:250px;">
            <div class="overlay op-5"></div>
            <div class="container">
                <div class="row align-items-center  pad-top-80">
                    <div class="col-md-6 col-12">
                        <div class="breadcrumb-menu">
                          <h2 class="page-title" style="font-size: 30px; color:#141414;">Awards 2017</h2>
                        </div>
                    </div>
                    <div class="col-md-6 col-12">
                        <div class="breadcrumb-menu text-right sm-left">
                            <ul>
                                <li class="active"><a href="<?php echo HOME_PATH;?>">Home</a></li>
                                <li><a href="<?php echo HOME_PATH;?>awards-best-retirement-villages-aged-care-2017">Awards</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Breadcrumb section ends-->
        <!--Blog post starts-->
        <div class="blog-area section-padding pad-bot-40 mar-top-20">
            <div class="container">
                <div class="row">
				
				
				
                    <div class="col-md-12">
                        <div class="single-blog-item v3">
						 <div class="single-blog-item v3">
						 	 	 <div class="row">
					         <div class="col-md-12">
					     
                            <h1 style="text-align: center;margin: 0;padding: 0;display: block;font-family: Helvetica;font-size: 40px;font-style: normal;font-weight: bold;line-height: 125%;letter-spacing: -1px;color: #606060 !important;"><span style="color:#FF0000">Finalists &amp; Winners of 2017</span><br>
                               <span style="color:#696969">'Peoples Choice Awards'</span><br>
                                 <span style="color:#FF0000">for Best Retirement Villages and Best Aged Care.</span></h1>
                              </div>
                                   </div>
								    <div class="row">
					         <div class="col-md-12">
					       
                               <div class="text-center">
                             <img src="<?php echo HOME_PATH; ?>images/2017awards.jpg" alt="...">  
                                 </div> 
                              </div>
                                   </div>
					          <div class="row">
					 
					         <div class="col-md-8 offset-md-2">
                                    <div class="blog-content text-center pad-top-30">
                                   
                                       <h1 style="text-align: center;margin: 0;padding: 0;display: block;font-family: Helvetica;font-size: 40px;font-style: normal;font-weight: bold;line-height: 125%;letter-spacing: -1px;color: #606060 !important;"><span style="color:#FF0000">Thank you </span><span style="color:#696969">again...</span></h1>
                                         <p class="date"> 
                              
                                           ...for sharing your experiences. We have received nearly 2000 reviews and ratings for this years awards.

                                             Results of the Winners & Finalists for the 2017 "Peoples' Choice Awards" for Best Retirement or Lifestyle Villages & Aged Care Facilities:
                                            </p>
							                   <br>
                                div style="text-align: center;"><br>
<span style="color:#FF0000"><strong style="line-height:20.8px">WINNERS - NORTH ISLAND</strong></span><br>
<strong><a href="/search/aged-care/Remuera-Rise-care-Remuera-Auckland" target="_blank">Remuera Rise</a>, Auckland</strong><br>
<em>Best Aged Care upto 40 beds</em><br>
<br>
<strong><a href="/search/aged-care/Malyon-House-Mount-Manganui-Tauranga" target="_blank">Malyon House</a>, Tauranga</strong><br>
<em>Best Aged Care over 40 beds</em><br>
<br>
<strong><a href="/search/retirement-village/Meadowbank-Retirement-Village-Meadowbank-Auckland" target="_blank">Meadowbank Retirement Village</a>, Auckland</strong><br>
<em>Best Retirement / Lifestyle Village</em><br>
<br>
<br style="line-height: 20.8px;">
<span style="color:#FF0000"><strong style="line-height:20.8px">WINNERS - SOUTH ISLAND</strong></span><br>
<strong><a href="/search/aged-care/Cheviot-Rest-Home-Cheviot-Cheviot" target="_blank">Cheviot Rest Home</a>, Canterbury</strong><br>
<em>Best Aged Care upto 40 beds</em><br>
<br>
<strong><a href="/search/aged-care/Archer-Archer-Home-Beckenham-Christchurch" target="_blank">Archer Home</a>, Christchurch</strong><br>
<em>Best Aged Care over 40 beds</em><br>
<br style="line-height: 20.8px;">
<strong><a href="/search/retirement-village/Park-Lane-Retirement-Village-Arvida-Addington-Christchurch" target="_blank">Parklane Retirement Village</a>, Christchurch</strong><br>
<em>Best Retirement / Lifestyle Village</em><br>
<br>
[Not awarded for 2017]<br style="line-height: 20.8px;">
<em>Best Multi-Facility Provider (Nationwide)</em><br>
<br>
----------------------<br>
<br>
<span style="color:#FF0000"><strong>FINALISTS - NORTH ISLAND</strong></span><br>
<strong><a href="/search/aged-care/Lynton-Lodge-Hospital-Westmere-Auckland" target="_blank">Lynton Lodge Hospital</a>, Auckland</strong><br>
<strong><a href="/search/aged-care/Woodlands-Of-Feilding--Feilding" target="_blank">Woodlands of Fielding</a>, Fielding</strong><br>
<em>Best Aged Care upto 40 beds</em><br>
<br>
<strong><a href="/search/aged-care/Gracelands-Rest-Home-And-Village-Frimley-Hastings" target="_blank">Gracelands Rest Home</a>, Hastings</strong><br>
<strong><a href="/search/aged-care/Cromwell-House-Hospital-Epsom-Auckland" target="_blank">Cromwell House and Hospital</a>, Auckland</strong><br>
<strong><a href="/search/aged-care/Te-Wiremu-House-Te-Hapara-Gisborne" target="_blank">Te Wiremu House | Anglican Care Waiapu</a>, Gisborne</strong><br>
<strong><a href="/search/aged-care/Eversley-Rest-Home-And-Village-Mahora-Hastings" target="_blank">Eversley Rest Home</a>, Hastings</strong><br>
<em>Best Aged Care over 40 beds</em><br>
<br>
<strong><a href="/search/aged-care/Remuera-Rise-care-Remuera-Auckland" target="_blank">REMUERA RISE</a>, Auckland</strong><br>
<strong><a href="/search/retirement-village/Redwood-Retirement-Village--Rotorua" target="_blank">Bupa REDWOOD RETIREMENT VILLAGE</a>, Rotorua</strong><br>
<strong><a href="/search/retirement-village/Gracelands-Retirement-Village-Frimley-Hastings" target="_blank">GRACELANDS RETIREMENT VILLAGE</a>, Hastings</strong><br>
<strong><a href="/search/retirement-village/Bupa-The-Gardens-Retirement-Village-Rotorua-Rotorua" target="_blank">Bupa THE GARDENS RETIREMENT VILLAGE</a>, Rotorua</strong><br>
<strong><a href="/search/retirement-village/Maygrove-Village-Orewa-Auckland" target="_blank">MAYGROVE VILLAGE</a>, Orewa</strong><br>
<strong><a href="/search/retirement-village/Mt-Eden-Gardens-Mt-Eden-Auckland" target="_blank">MT EDEN GARDENS</a>, Auckland</strong><br>
<em>Best Retirement / Lifestyle Village</em><br>
<br>
<br>
<span style="color:#FF0000"><strong>FINALISTS - SOUTH ISLAND</strong></span><br>
<strong><a href="/search/aged-care/Waikiwi-Gardens-Rest-Home-Waikiwi-Invercargill" target="_blank">Waikiwi Gardens Rest Home</a>, Invercargill</strong><br>
<em>Best Aged Care upto 40 beds</em><br>
<br>
<strong><a href="/search/aged-care/Holmwood-Rest-Home-Rangiora-Rangiora" target="_blank">Holmwood Rest Home</a>, Rangiora</strong><br>
<strong><a href="/search/aged-care/Ernest-Rutherford-Retirement-Village-Stoke-Nelson" target="_blank">Ernest Rutherford</a>, Nelson</strong><br>
<strong><a href="/search/aged-care/Rannerdale-War-Veterans-Hospital-And-Home-Upper-Riccarton-Christchurch" target="_blank">Rannerdale War Veterans' Hospital and Home</a> Christchurch</strong><br>
<em>Best Aged Care over 40 beds</em><br>
<br>
<strong><a href="/search/retirement-village/Diana-Isaac-Retirement-Village-Mairehau-Christchurch" target="_blank">DIANA ISAAC RETIREMENT VILLAGE</a>, Christchurch</strong><br>
<strong><a href="/search/retirement-village/Charles-Upham-Retirement-Village-Waimakariri-Rangiora" target="_blank">CHARLES UPHAM RETIREMENT VILLAGE</a>, Rangiora</strong><br>
<strong><a href="/search/retirement-village/Anthony-Wilding-Retirement-Village-Aidanfield-Christchurch" target="_blank">ANTHONY WILDING RETIREMENT VILLAGE</a>, Christchurch</strong><br>
<strong><a href="/search/retirement-village/Ngaio-Marsh-Retirement-Village-Papanui-Christchurch" target="_blank">NGAIO MARSH RETIREMENT VILLAGE</a>, Christchurch</strong><br>
<strong><a href="/search/retirement-village/The-Russley-Village-Christchurch-Christchurch" target="_blank">THE RUSSLEY VILLAGE</a>, Christchurch</strong><br>

<em>Best Retirement / Lifestyle Village</em>&nbsp;<br>
&nbsp;</div>
 
                                    </div>
                                </div>
                                </div>		
				 		
			  
						 
						 
						 
						 
						 
							  <div class="row">
                             
                                <div class="col-md-8 offset-md-1">
                                        <h2>
                                    South Island Award Presentations....</h2> 
                                        <p>With our Aged Advisor head office based in Christchurch, we quietly presented the South Island Awards to the three Canterbury based facilities.y</p> 
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-10 offset-md-1">
								<a href="<?php echo HOME_PATH;?>search/aged-care/Cheviot-Rest-Home-Cheviot-Cheviot" target="_blank">
                             <img style="width: 100% !important;" src="<?php echo HOME_PATH;?>images/cheviot.jpg" alt="..."></a>
                                   <a   class="blog-cat btn v6 red">South Island Award Presentations</a>  
                                <div class="col-md-8 offset-md-2">
                                    <div class="blog-content text-center pad-top-30">
                                     
                                        <h3>Cheviot Resthome: Manager / Owner, Sue Coleman with Nigel Matthews (Aged Advisor) and their Awarda for Best Small Aged Care Facility - South Island (2 Years in a row).</h3>
                                        <!--<a href="single-news-three.html" class="btn v4">Know More</a>-->
                                    </div>
                                </div>
                            </div>
                        </div>
						<br>
                        <div class="single-blog-item v3">
						
							 <div class="single-blog-item v3">
							  <div class="row">
                             
                                <div class="col-md-8 offset-md-1">
                                        <h2>  South Island Awards...</h2> 
                                        <p>Christchurch currently has the most multiple Award-Winning Facilities.</p> 
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-10 offset-md-1">
								<a href="<?php echo HOME_PATH;?>search/aged-care/Archer-Archer-Home-Beckenham-Christchurch" target="_blank">
                                    <img  style="width: 100% !important;" src="<?php echo HOME_PATH;?>images/archer.jpg" alt="..."></a>
                                    <a   class="blog-cat btn v6 red">
                                    South Island Award Presentations</a>
                                </div>
                                <div class="col-md-8 offset-md-2">
                                    <div class="blog-content text-center pad-top-30">
									    <h3>Archer Home. Best Med/Large Aged Care Facility Award - South Island</h3>
                                        <p class="date">Graeme Mitchell, Village Manager (left) with team & residents delighted to receive their 2nd award from Nigel Matthews of Aged Advisor NZ</p>
                                    
                                        <!--<a href="single-news-three.html" class="btn v4">Know More</a>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    <br>
                        <div class="single-blog-item v3">
                            <div class="row">
                                <div class="col-md-10 offset-md-1">
								  
                                    <img  style="width: 100% !important;" src="<?php echo HOME_PATH;?>images/parklane.jpg" alt="..."> 
                                    <a  class="blog-cat btn v6 red">
                                       South Island Awards...</a>
                                </div>
                                <div class="col-md-8 offset-md-2">
                                    <div class="blog-content text-center pad-top-30">
									   <h3>Park Lane Retirement Village, Christchurch.
                                           Presentation of Best Retirement or Lifestyle Village Award - South Island.</h3>
                                        <p class="date">(left to right) Nigel Matthews (General Manager, AgedAdvisor) and Aleshia Rayner (Village Manager)</p>
                                     
                                        <!--<a href="single-news-three.html" class="btn v4">Know More</a>-->
                                    </div>
                                </div>
                            </div>
                        </div>
						                    <br>
					 
						
						                    <br>
						
							  <div class="row">
                             
                                <div class="col-md-8 offset-md-1">
                                        <h2>
                                     North Island Award Presentations...</h2> 
                                        <p>We were thrilled to be able to personally make presentations to the North Island winners (and several finalists) between 6th and 7th March. Congratulations to all of you.</p> 
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-10 offset-md-1">
								<a href="<?php echo HOME_PATH;?>search/aged-care/Remuera-Rise-care-Facility-Remuera-Auckland" target="_blank">
                                    <img  style="width: 100% !important;" src="<?php echo HOME_PATH;?>images/rrise.jpg" alt="..."></a>
                                    <a   class="blog-cat btn v6 red">
                                     
                                      North Island Award Presentations</a>
                                </div>
                                <div class="col-md-8 offset-md-2">
                                    <div class="blog-content text-center pad-top-30">
                                   
                                        <h3>Remuera Rise, Auckland.

                                     Presentation of Best Small Aged Care Facility Award - North Island.</h3>
                                         <p class="date">
                                      Jo Ellis (from Hearing Technology) presents certificate and award to Clinical Manager, Jennifer Ibanez (center) and Village Manager, Catherine Archer (right)</p>
                                        <!--<a href="single-news-three.html" class="btn v4">Know More</a>-->
                                    </div>
                                </div>
                            </div>
							
							
							
							
							                    <br>
							
							
							    <div class="row">
                                <div class="col-md-10 offset-md-1">
							 <a href="<?php echo HOME_PATH;?>search/aged-care/Remuera-Rise-care-Facility-Remuera-Auckland" target="_blank">
                                    <img  style="width: 100% !important;" src="<?php echo HOME_PATH;?>images/rrise2.jpg" alt="..."> </a>
                                    <a   class="blog-cat btn v6 red">
                                     
                                      North Island Award Presentations</a>
                                </div>
                                <div class="col-md-8 offset-md-2">
                                    <div class="blog-content text-center pad-top-30">
                                   
                                        <h3>Staff and Residents celebrating awards with afternoon tea. </h3>
                                         <p class="date"> </p>
                                        <!--<a href="single-news-three.html" class="btn v4">Know More</a>-->
                                    </div>
                                </div>
                            </div>
							
							
							
							                    <br>
							
							
							
							
							
							  <div class="row">
                                <div class="col-md-10 offset-md-1">
							 
                                    <img  style="width: 100% !important;" src="<?php echo HOME_PATH;?>images/meadowbank.jpg" alt="..."> 
                                    <a   class="blog-cat btn v6 red">
                                     
                                      North Island Award Presentations</a>
                                </div>
                                <div class="col-md-8 offset-md-2">
                                    <div class="blog-content text-center pad-top-30">
                                   
                                        <h3>Malyon House, Tauranga.

                                         Presentation of Best Med / Large Aged Care Facility Award - North Island.</h3>
                                         <p class="date">Some of the team and management with Village Manager, Amy Munro (center) and Nigel Matthews, Aged Advisor (far right)</p>
                                        <!--<a href="single-news-three.html" class="btn v4">Know More</a>-->
                                    </div>
                                </div>
                            </div>
							
							
							
							  <br>
							
						 
							
							
							
					 <div class="row">
					 <div class="col-md-12">
                        <a href="https://hearinglife.co.nz/">
						 <div class="text-center">
                            <img src="https://gallery.mailchimp.com/794c7fae79b8e528f8be29233/images/dbe8d60a-3e3f-4889-869b-275a54610dd2.jpg" alt="...">
                        </div></a>
                    </div>
				 
                    </div>
				 	 <div class="row">
					<div class="col-md-12">
					<a href="http://www.lawlink.co.nz/member-firms/">
                       <div class="text-center">
                            <img src="<?php echo HOME_PATH; ?>admin/files/pages/banner/1460551774_lawlink-logo.jpg" alt="...">  
                        </div></a>
                    </div>
                    </div>
					 <div class="row">
					 
					         <div class="col-md-8 offset-md-2">
                                    <div class="blog-content text-center pad-top-30">
                                   
                                        <h3>Special thanks to our Sponsors.</h3>
                                         <p class="date"> 
                              
Note: Aged Advisor winners are based on independent reviews and opinions from people that live, visit or work at the retirement/lifestyle villages and residential care facilities throughout New Zealand.

The team at Aged Advisor are thrilled to celebrate the exceptional organisations who offer independent living options or care for our older generation, and are proud to award them with the recognition they deserve.

Entry is now open for next years Awards. Make sure to <a  style="color: #6DC6DD;"href="<?php echo HOME_PATH; ?>rating-supplier">write your review.</a>
                            </p>
							<br>
                                        <a href="<?php echo HOME_PATH; ?>awards-best-retirement-villages-aged-care-2015-2016" class="btn v4">Click here to view previous years awards</a> 
                                    </div>
                                </div>
 
                    
                </div>		
							
							
							
							
							
                        </div>
				
						
                    </div>
                </div>
             
            </div>
      
    </div>
    </div>
    
 
