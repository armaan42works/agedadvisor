 
        <div class="call-to-action pad-tb-70" style="background-image: url(<?php echo HOME_PATH;?>/modules/pages/images/bg/memphis-colorful.png);height:250px;">
            <div class="overlay op-5"></div>
            <div class="container">
                <div class="row align-items-center  pad-top-80">
                    <div class="col-md-6 col-12">
                        <div class="breadcrumb-menu">
						<h2 class="page-title" style="font-size: 30px; color:#141414;">Awards 2019</h2>
                            
                        </div>
                    </div>
                    <div class="col-md-6 col-12">
                        <div class="breadcrumb-menu text-right sm-left">
                            <ul>
                                <li class="active"><a href="<?php echo HOME_PATH;?>">Home</a></li>
                                <li><a href="<?php echo HOME_PATH;?>awards">Awards</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Breadcrumb section ends-->
        <!--Blog post starts-->
        <div class="blog-area section-padding pad-bot-40 mar-top-20">
            <div class="container">
                <div class="row">
				
				
				
                    <div class="col-md-12">
                        <div class="single-blog-item v3">
						 <div class="single-blog-item v3">
						 	 	 <div class="row">
					         <div class="col-md-12">
					     
                            <h1 style="text-align: center;margin: 0;padding: 0;display: block;font-family: Helvetica;font-size: 40px;font-style: normal;font-weight: bold;line-height: 125%;letter-spacing: -1px;color: #606060 !important;"><span style="color:#FF0000">Finalists &amp; Winners of 2019</span><br>
                               <span style="color:#696969">'Peoples Choice Awards'</span><br>
                                 <span style="color:#FF0000">for Best Retirement Villages and Best Aged Care.</span></h1>
                              </div>
                                   </div>
								    <div class="row">
					         <div class="col-md-12">
					       
                               <div class="text-center">
                             <img src="<?php echo HOME_PATH; ?>images/2017awards.jpg" alt="...">  
                                 </div> 
                              </div>
                                   </div>
					          <div class="row">
					 
					         <div class="col-md-8 offset-md-2">
                                    <div class="blog-content text-center pad-top-30">
                                   
                                       <h1 style="text-align: center;margin: 0;padding: 0;display: block;font-family: Helvetica;font-size: 40px;font-style: normal;font-weight: bold;line-height: 125%;letter-spacing: -1px;color: #606060 !important;"><span style="color:#FF0000">Thank you </span><span style="color:#696969">again...</span></h1>
                                         <p class="date"> 
                              
                                           ...for sharing your experiences. We have received nearly 2000 reviews and ratings for this years awards.

                                             Results of the Winners & Finalists for the 2017 "Peoples' Choice Awards" for Best Retirement or Lifestyle Villages & Aged Care Facilities:
                                            </p>
							                   <br>
                                div style="text-align: center;"><br>
<span style="color:#FF0000"><strong style="line-height:20.8px">WINNER - BEST PROVIDER NATIONWIDE</strong></span><br>

<em>Best Multi-Facility / Group Provider (Nationwide)</em><br>
<strong><a href="/search/for/75000/Retirement-Villages-Rest-Homes-Aged-Care-RYMAN+VILLAGES" target="_blank">RYMAN HEALTHCARE</a>, New Zealand</strong><br>
<br style="line-height: 20.8px;">
----------------------<br>
<br>




<span style="color:#FF0000"><strong style="line-height:20.8px">WINNERS - NORTH ISLAND</strong></span><br>
<em>Best Aged Care upto 40 beds</em><br>
<strong><a href="/search/aged-care/Cook-Street-Nursing-Care-Centre-West-End-Palmerston-North" target="_blank">Cook Street Nursing Care Centre</a>, Palmerston North</strong><br>
<br>
<em>Best Aged Care over 40 beds</em><br>
<strong><a href="/search/aged-care/Malyon-House-Mount-Manganui-Tauranga" target="_blank">Malyon House</a>, Mt Maunganui</strong><br>
<br>
<em>Best Large Retirement / Lifestyle Village</em><br>
<strong><a href="/search/retirement-village/Maygrove-Village-Orewa-Auckland" target="_blank">MAYGROVE VILLAGE</a>, Orewa</strong><br>
<br>
<br style="line-height: 20.8px;">
<span style="color:#FF0000"><strong style="line-height:20.8px">WINNERS - SOUTH ISLAND</strong></span><br>
<em>Best Aged Care upto 40 beds</em><br>
<strong><a href="/search/aged-care/Cheviot-Rest-Home-Cheviot-Cheviot" target="_blank">Cheviot Rest Home</a>, Canterbury</strong><br>
<br>
<em>Best Aged Care over 40 beds</em><br>
<strong><a href="/search/aged-care/Redroofs-Lifecare-Maori-Hill-Dunedin" target="_blank">Redroofs Lifecare</a>, Dunedin</strong><br>
<br style="line-height: 20.8px;">

<em>Best Small Retirement / Lifestyle Village</em><br>
<strong><a href="/search/retirement-village/Archer-Archer-Lifestyle-Village-Beckenham-Christchurch" target="_blank">Archer Lifestyle Village</a>, Christchurch</strong><br>
<br>

<em>Best Large Retirement / Lifestyle Village</em><br>
<strong><a href="/search/retirement-village/Summerset-At-Wigram-Retirement-Village-Wigram-Christchurch" target="_blank">SUMMERSET AT WIGRAM Village</a>, Christchurch</strong><br>
<br>

----------------------<br>
<br>
<span style="color:#FF0000"><strong>FINALISTS - NORTH ISLAND</strong></span><br>
<em>Best Aged Care upto 40 beds</em><br>
<strong><a href="/search/aged-care/Pohlen-Hospital--Matamata" target="_blank">Pohlen Hospital</a>, Matamata</strong><br>
<strong><a href="/search/aged-care/Concord-House-Rest-Home-Greenlane-Auckland" target="_blank">Concord House Rest Home</a>, Auckland</strong><br>


<br>
<em>Best Aged Care over 40 beds</em><br>
<strong><a href="/search/aged-care/Greenvalley-Rest-Home-Glenfield-Auckland" target="_blank">Greenvalley Rest Home</a>, Auckland</strong><br>
<strong><a href="/search/aged-care/Summerset-In-The-Bay-Greenmeadows-Napier" target="_blank">Summerset in the Bay</a>, Napier</strong><br>
<strong><a href="/search/aged-care/Heretaunga-Rest-Home-And-Village-Silverstream-Upper-Hutt" target="_blank">Heretaunga Rest Home</a>, Upper Hutt</strong><br>


<br>
<em>Best Large Retirement / Lifestyle Village</em><br>
<strong><a href="/search/retirement-village/Highfield-Country-Estate-Te-Awamutu-Te-Awamutu" target="_blank">HIGHFIELD COUNTRY ESTATE</a>, Te Awamutu</strong><br>
<strong><a href="/search/retirement-village/Kiri-Te-Kanawa-Retirement-Village-Taruheru-Gisborne" target="_blank">KIRI TE KANAWA RETIREMENT VILLAGE</a>, Gisborne</strong><br>
<strong><a href="/search/retirement-village/Shona-Mcfarlane-Retirement-Village-Avalon-Lower-Hutt" target="_blank">SHONA MCFARLANE RETIREMENT VILLAGE</a>, Lower Hutt</strong><br>
<strong><a href="/search/retirement-village/Carmel-Country-Estate-Retirement-Village-Ohauiti-Tauranga" target="_blank">CARMEL COUNTRY ESTATE RETIREMENT VILLAGE</a>, Tauranga</strong><br>
<!--<strong><a href="/search/retirement-village/St-Andrews-Village-Glendowie-Auckland" target="_blank">ST ANDREWS VILLAGE</a>, Auckland</strong><br>-->
<br>
<br>
<span style="color:#FF0000"><strong>FINALISTS - SOUTH ISLAND</strong></span><br>
<em>Best Aged Care upto 40 beds</em><br>
<strong><a href="/search/aged-care/Aspiring-Enliven-Care-Centre--Wanaka" target="_blank">Aspiring Enliven Care Centre</a>, Wanaka</strong><br>
<strong><a href="/search/aged-care/Waikiwi-Gardens-Rest-Home-Waikiwi-Invercargill" target="_blank">Waikiwi Gardens Rest Home</a>, Invercargill</strong><br>
<br>
<em>Best Aged Care over 40 beds</em><br>
<strong><a href="/search/aged-care/Archer-Archer-Home-Beckenham-Christchurch" target="_blank">Archer Home</a>, Christchurch</strong><br>
<br>
<em>Best Small Retirement / Lifestyle Village</em>&nbsp;<br>
<strong><a href="/search/retirement-village/Edith-Cavell-retirement-Village-Sumner-Christchurch" target="_blank">EDITH CAVELL RETIREMENT VILLAGE</a>, Christchurch</strong><br>
<strong><a href="/search/retirement-village/Windsorcare-Retirement-Village-Shirley-Christchurch" target="_blank">WINDSORCARE Village</a>, Christchurch</strong><br>
<strong><a href="/search/retirement-village/Rosebank-Retirement-Village--Ashburton" target="_blank">ROSEBANK RETIREMENT VILLAGE</a>, Ashburton</strong><br><br>

<em>Best Large Retirement / Lifestyle Village</em>&nbsp;<br>
<strong><a href="/search/retirement-village/Diana-Isaac-Retirement-Village-Mairehau-Christchurch" target="_blank">DIANA ISAAC RETIREMENT VILLAGE</a>, Christchurch</strong><br>
<strong><a href="/search/retirement-village/Alpine-View-Lifestyle-Village-Burwood-Christchurch" target="_blank">ALPINE VIEW LIFESTYLE VILLAGE</a>, Christchurch</strong><br>
<strong><a href="/search/retirement-village/The-Russley-Village-Russley-Christchurch" target="_blank">THE RUSSLEY VILLAGE</a>, Christchurch</strong><br>
<!--<strong><a href="/search/retirement-village/Brooklands-Retirement-Village-Mosgiel-Dunedin" target="_blank">BROOKLANDS RETIREMENT VILLAGE</a>, Mosgiel</strong><br>-->


&nbsp;</div>
                            <h2 class="null" style="margin: 0;padding: 0;display: block;font-family: Helvetica;font-size: 26px;font-style: normal;font-weight: bold;line-height: 125%;letter-spacing: -.75px;text-align: left;color: #404040 !important;">Photos to come...</h2>

 
                                    </div>
                                </div>
                                </div>		
				 		
			   
                        <div class="single-blog-item v3">
						
							 
					 <div class="row">
					 <div class="col-md-12">
                        <a href="https://hearinglife.co.nz/">
						 <div class="text-center">
                            <img src="https://gallery.mailchimp.com/794c7fae79b8e528f8be29233/images/dbe8d60a-3e3f-4889-869b-275a54610dd2.jpg" alt="...">
                        </div></a>
                    </div>
				 
                    </div>
				 	 <div class="row">
					<div class="col-md-12">
					<a href="https://www.homeofpoi.com/us/">
                       <div class="text-center">
                            <img src="<?php echo HOME_PATH; ?>images/homeofpoi-logo.png" alt="...">  
                        </div></a>
                    </div>
                    </div>
					 <div class="row">
					 
					         <div class="col-md-8 offset-md-2">
                                    <div class="blog-content text-center pad-top-30">
                                   
                                        <h3>Special thanks to our Sponsors.</h3>
                                         <p class="date"> 
                              
Note: Aged Advisor winners are based on independent reviews and opinions from people that live, visit or work at the retirement/lifestyle villages and residential care facilities throughout New Zealand.

The team at Aged Advisor are thrilled to celebrate the exceptional organisations who offer independent living options or care for our older generation, and are proud to award them with the recognition they deserve.

Entry is now open for next years Awards. Make sure to <a  style="color: #6DC6DD;"href="<?php echo HOME_PATH; ?>rating-supplier">write your review.</a>
                            </p>
							<br>
                                        <a href="<?php echo HOME_PATH; ?>awards-best-retirement-villages-aged-care-2017" class="btn v4">Click here to view previous years awards</a> 
                                    </div>
                                </div>
 
                    
                </div>		
							
							
							
							
							
                        </div>
				
						
                    </div>
                </div>
             
            </div>
      
    </div>
    </div>
    
 
