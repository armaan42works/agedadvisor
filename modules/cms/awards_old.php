
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns:fb="https://www.facebook.com/2008/fbml" xmlns:og="https://opengraph.org/schema/"> <head>
        
<meta property="og:title" content="AWARDS Page for Amit..."/>
<meta property="fb:page_id" content="43929265776" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />        
    	<!-- NAME: 1 COLUMN -->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>AWARDS Page for Amit...</title>
        
    <style type="text/css">
		body,#bodyTable,#bodyCell{
			height:100% !important;
			margin:0;
			padding:0;
			width:100% !important;
		}
		table{
			border-collapse:collapse;
		}
		img,a img{
			border:0;
			outline:none;
			text-decoration:none;
		}
		h1,h2,h3,h4,h5,h6{
			margin:0;
			padding:0;
		}
		p{
			margin:1em 0;
			padding:0;
		}
		a{
			word-wrap:break-word;
		}
		.ReadMsgBody{
			width:100%;
		}
		.ExternalClass{
			width:100%;
		}
		.ExternalClass,.ExternalClass p,.ExternalClass span,.ExternalClass font,.ExternalClass td,.ExternalClass div{
			line-height:100%;
		}
		table,td{
			mso-table-lspace:0pt;
			mso-table-rspace:0pt;
		}
		#outlook a{
			padding:0;
		}
		img{
			-ms-interpolation-mode:bicubic;
		}
		body,table,td,p,a,li,blockquote{
			-ms-text-size-adjust:100%;
			-webkit-text-size-adjust:100%;
		}
		#templatePreheader,#templateHeader,#templateBody,#templateFooter{
			min-width:100%;
		}
		#bodyCell{
			padding:20px;
		}
		.mcnImage{
			vertical-align:bottom;
		}
		.mcnTextContent img{
			height:auto !important;
		}
		body,#bodyTable{
			background-color:#F2F2F2;
		}
		#bodyCell{
			border-top:0;
		}
		#templateContainer{
			border:0;
		}
		h1{
			color:#606060 !important;
			display:block;
			font-family:Helvetica;
			font-size:40px;
			font-style:normal;
			font-weight:bold;
			line-height:125%;
			letter-spacing:-1px;
			margin:0;
			text-align:left;
		}
		h2{
			color:#404040 !important;
			display:block;
			font-family:Helvetica;
			font-size:26px;
			font-style:normal;
			font-weight:bold;
			line-height:125%;
			letter-spacing:-.75px;
			margin:0;
			text-align:left;
		}
		h3{
			color:#606060 !important;
			display:block;
			font-family:Helvetica;
			font-size:18px;
			font-style:normal;
			font-weight:bold;
			line-height:125%;
			letter-spacing:-.5px;
			margin:0;
			text-align:left;
		}
		h4{
			color:#808080 !important;
			display:block;
			font-family:Helvetica;
			font-size:16px;
			font-style:normal;
			font-weight:bold;
			line-height:125%;
			letter-spacing:normal;
			margin:0;
			text-align:left;
		}
		#templatePreheader{
			background-color:#FFFFFF;
			border-top:0;
			border-bottom:0;
		}
		.preheaderContainer .mcnTextContent,.preheaderContainer .mcnTextContent p{
			color:#606060;
			font-family:Helvetica;
			font-size:11px;
			line-height:125%;
			text-align:left;
		}
		.preheaderContainer .mcnTextContent a{
			color:#606060;
			font-weight:normal;
			text-decoration:underline;
		}
		#templateHeader{
			background-color:#FFFFFF;
			border-top:0;
			border-bottom:0;
		}
		.headerContainer .mcnTextContent,.headerContainer .mcnTextContent p{
			color:#606060;
			font-family:Helvetica;
			font-size:15px;
			line-height:150%;
			text-align:left;
		}
		.headerContainer .mcnTextContent a{
			color:#6DC6DD;
			font-weight:normal;
			text-decoration:underline;
		}
		#templateBody{
			background-color:#FFFFFF;
			border-top:0;
			border-bottom:0;
		}
		.bodyContainer .mcnTextContent,.bodyContainer .mcnTextContent p{
			color:#606060;
			font-family:Helvetica;
			font-size:15px;
			line-height:150%;
			text-align:left;
		}
		.bodyContainer .mcnTextContent a{
			color:#6DC6DD;
			font-weight:normal;
			text-decoration:underline;
		}
		#templateFooter{
			background-color:#FFFFFF;
			border-top:0;
			border-bottom:0;
		}
		.footerContainer .mcnTextContent,.footerContainer .mcnTextContent p{
			color:#606060;
			font-family:Helvetica;
			font-size:11px;
			line-height:125%;
			text-align:left;
		}
		.footerContainer .mcnTextContent a{
			color:#606060;
			font-weight:normal;
			text-decoration:underline;
		}
	@media only screen and (max-width: 480px){
		body,table,td,p,a,li,blockquote{
			-webkit-text-size-adjust:none !important;
		}

}	@media only screen and (max-width: 480px){
		body{
			width:100% !important;
			min-width:100% !important;
		}

}	@media only screen and (max-width: 480px){
		td[id=bodyCell]{
			padding:10px !important;
		}

}	@media only screen and (max-width: 480px){
		table[class=mcnTextContentContainer]{
			width:100% !important;
		}

}	@media only screen and (max-width: 480px){
		.mcnBoxedTextContentContainer{
			max-width:100% !important;
			min-width:100% !important;
			width:100% !important;
		}

}	@media only screen and (max-width: 480px){
		table[class=mcpreview-image-uploader]{
			width:100% !important;
			display:none !important;
		}

}	@media only screen and (max-width: 480px){
		img[class=mcnImage]{
			width:100% !important;
		}

}	@media only screen and (max-width: 480px){
		table[class=mcnImageGroupContentContainer]{
			width:100% !important;
		}

}	@media only screen and (max-width: 480px){
		td[class=mcnImageGroupContent]{
			padding:9px !important;
		}

}	@media only screen and (max-width: 480px){
		td[class=mcnImageGroupBlockInner]{
			padding-bottom:0 !important;
			padding-top:0 !important;
		}

}	@media only screen and (max-width: 480px){
		tbody[class=mcnImageGroupBlockOuter]{
			padding-bottom:9px !important;
			padding-top:9px !important;
		}

}	@media only screen and (max-width: 480px){
		table[class=mcnCaptionTopContent],table[class=mcnCaptionBottomContent]{
			width:100% !important;
		}

}	@media only screen and (max-width: 480px){
		table[class=mcnCaptionLeftTextContentContainer],table[class=mcnCaptionRightTextContentContainer],table[class=mcnCaptionLeftImageContentContainer],table[class=mcnCaptionRightImageContentContainer],table[class=mcnImageCardLeftTextContentContainer],table[class=mcnImageCardRightTextContentContainer]{
			width:100% !important;
		}

}	@media only screen and (max-width: 480px){
		td[class=mcnImageCardLeftImageContent],td[class=mcnImageCardRightImageContent]{
			padding-right:18px !important;
			padding-left:18px !important;
			padding-bottom:0 !important;
		}

}	@media only screen and (max-width: 480px){
		td[class=mcnImageCardBottomImageContent]{
			padding-bottom:9px !important;
		}

}	@media only screen and (max-width: 480px){
		td[class=mcnImageCardTopImageContent]{
			padding-top:18px !important;
		}

}	@media only screen and (max-width: 480px){
		td[class=mcnImageCardLeftImageContent],td[class=mcnImageCardRightImageContent]{
			padding-right:18px !important;
			padding-left:18px !important;
			padding-bottom:0 !important;
		}

}	@media only screen and (max-width: 480px){
		td[class=mcnImageCardBottomImageContent]{
			padding-bottom:9px !important;
		}

}	@media only screen and (max-width: 480px){
		td[class=mcnImageCardTopImageContent]{
			padding-top:18px !important;
		}

}	@media only screen and (max-width: 480px){
		table[class=mcnCaptionLeftContentOuter] td[class=mcnTextContent],table[class=mcnCaptionRightContentOuter] td[class=mcnTextContent]{
			padding-top:9px !important;
		}

}	@media only screen and (max-width: 480px){
		td[class=mcnCaptionBlockInner] table[class=mcnCaptionTopContent]:last-child td[class=mcnTextContent]{
			padding-top:18px !important;
		}

}	@media only screen and (max-width: 480px){
		td[class=mcnBoxedTextContentColumn]{
			padding-left:18px !important;
			padding-right:18px !important;
		}

}	@media only screen and (max-width: 480px){
		td[class=mcnTextContent]{
			padding-right:18px !important;
			padding-left:18px !important;
		}

}	@media only screen and (max-width: 480px){
		table[id=templateContainer],table[id=templatePreheader],table[id=templateHeader],table[id=templateBody],table[id=templateFooter]{
			max-width:600px !important;
			width:100% !important;
		}

}	@media only screen and (max-width: 480px){
		h1{
			font-size:24px !important;
			line-height:125% !important;
		}

}	@media only screen and (max-width: 480px){
		h2{
			font-size:20px !important;
			line-height:125% !important;
		}

}	@media only screen and (max-width: 480px){
		h3{
			font-size:18px !important;
			line-height:125% !important;
		}

}	@media only screen and (max-width: 480px){
		h4{
			font-size:16px !important;
			line-height:125% !important;
		}

}	@media only screen and (max-width: 480px){
		table[class=mcnBoxedTextContentContainer] td[class=mcnTextContent],td[class=mcnBoxedTextContentContainer] td[class=mcnTextContent] p{
			font-size:18px !important;
			line-height:125% !important;
		}

}	@media only screen and (max-width: 480px){
		table[id=templatePreheader]{
			display:block !important;
		}

}	@media only screen and (max-width: 480px){
		td[class=preheaderContainer] td[class=mcnTextContent],td[class=preheaderContainer] td[class=mcnTextContent] p{
			font-size:14px !important;
			line-height:115% !important;
		}

}	@media only screen and (max-width: 480px){
		td[class=headerContainer] td[class=mcnTextContent],td[class=headerContainer] td[class=mcnTextContent] p{
			font-size:18px !important;
			line-height:125% !important;
		}

}	@media only screen and (max-width: 480px){
		td[class=bodyContainer] td[class=mcnTextContent],td[class=bodyContainer] td[class=mcnTextContent] p{
			font-size:18px !important;
			line-height:125% !important;
		}

}	@media only screen and (max-width: 480px){
		td[class=footerContainer] td[class=mcnTextContent],td[class=footerContainer] td[class=mcnTextContent] p{
			font-size:14px !important;
			line-height:115% !important;
		}

}	@media only screen and (max-width: 480px){
		td[class=footerContainer] a[class=utilityLink]{
			display:block !important;
		}

}</style>        <script type="text/javascript">
            var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
            document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
            </script>
            <script type="text/javascript">
            try {
                var _gaq = _gaq || [];
                _gaq.push(["_setAccount", "UA-329148-88"]);
                _gaq.push(["_setDomainName", ".campaign-archive.com"]);
                _gaq.push(["_trackPageview"]);
                _gaq.push(["_setAllowLinker", true]);
            } catch(err) {console.log(err);}</script>
                    <link rel="stylesheet" href="https://us11.campaign-archive2.com/css/archivebar-desktop.css" mc:nocompile/> 
                    <script type="text/javascript" src="https://us11.campaign-archive2.com/js/archivebar-desktop-plugins.js" mc:nocompile>
                    </script> <script src="https://downloads.mailchimp.com/ZeroClipboard.min.js" mc:nocompile></script> 
            <!--<script type="text/javascript">
            $(document).ready(function() {
                ZeroClipboard.setDefaults({ moviePath: "//downloads.mailchimp.com/ZeroClipboard.07.13.swf", trustedDomains: ['us11.campaign-archive2.com']});
                var clip = new ZeroClipboard( $('#copyURL') );
                clip.setHandCursor(true);
                clip.on('complete', function(client, args) {alert('Copied "'+args.text+'" to your clipboard.');});
                clip.on('mouseover', function (client) { $('#copyURL').addClass('hover'); });
                clip.on('mouseout', function (client) { $('#copyURL').removeClass('hover'); });

                $('li.more > a').click(function(){
                    var toToggle = $($(this).attr('href'));
                    if(toToggle.is(':visible')){
                        toToggle.slideUp('fast');
                        $(this).removeClass('is-active');
                        if ($('#awesomebar').find('.is-active').length < 1){
                            $('#awesomebar').removeClass('sub-active');
                        }
                    } else {
                        toToggle.slideDown('fast');
                        $(this).addClass('is-active');
                        $('#awesomebar').addClass('sub-active');
                    }
                    return false;
                });

            });
        </script>-->   
                    
    <script type="text/javascript">
    function incrementFacebookLikeCount() {
        var current = parseInt($('#campaign-fb-like-btn span').html());
        $('#campaign-fb-like-btn span').fadeOut().html(++current).fadeIn();
    }

    function getUrlParams(str) {
        var vars = {}, hash;
        if (!str) return vars;
        var hashes = str.slice(str.indexOf('?') + 1).split('&');
        for(var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            vars[hash[0]] = hash[1];
        }
        return vars;
    }
    
    function setupSocialSharingStuffs() {
        var numSocialElems = $('a[rel=socialproxy]').length;
        var numSocialInitialized = 0;
        var urlParams = getUrlParams(window.document.location.href);
        var paramsToCopy = {'e':true, 'eo':true};
        $('a[rel=socialproxy]').each(function() {
            var href = $(this).attr('href');
            var newHref = decodeURIComponent(href.match(/socialproxy=(.*)/)[1]);
            // for facebook insanity to work well, it needs to all be run against just campaign-archive
            newHref = newHref.replace(/campaign-archive(\d)/gi, 'campaign-archive');
            var newHrefParams = getUrlParams(newHref);
            for(var param in urlParams) {
                if ((param in paramsToCopy) && !(param in newHrefParams)) {
                    newHref += '&' + param + '=' + urlParams[param];
                }
            }
            $(this).attr('href', newHref);
            if (href.indexOf('facebook-comment') !== -1) {
                $(this).fancyZoom({"zoom_id": "social-proxy", "width":620, "height":450, "iframe_height": 450});
            } else {
                $(this).fancyZoom({"zoom_id": "social-proxy", "width":500, "height":200, "iframe_height": 500});
            }
            numSocialInitialized++;
                    });
    }
	if (window.top!=window.self){
        $(function() {
          var iframeOffset = $("#archive", window.parent.document).offset();
          $("a").each(function () {
              var link = $(this);
              var href = link.attr("href");
              if (href && href[0] == "#") {
                  var name = href.substring(1);
                  $(this).click(function () {
                      var nameElement = $("[name='" + name + "']");
                      var idElement = $("#" + name);
                      var element = null;
                      if (nameElement.length > 0) {
                          element = nameElement;
                      } else if (idElement.length > 0) {
                          element = idElement;
                      }
         
                      if (element) {
                          var offset = element.offset();
                          var height = element.height();
                          //3 is totally arbitrary, but seems to work best.
                          window.parent.scrollTo(offset.left, (offset.top + iframeOffset.top - (height*3)) );
                      }
         
                      return false;
                  });
              }
          });
        });
    }
</script>  <script type="text/javascript">
            $(document).ready(function() {
                setupSocialSharingStuffs();
            });
        </script> </head> <body id="archivebody"> <!--<div id="awesomewrap"> <div id="awesomeshare"> <div id="zclipwrap"> <label for="shortURL">Short URL</label> <span id="copyURL" data-clipboard-target="shortURL"> <input id="shortURL" name="shorturl" style="margin-top:6px;" type="text" value="http://eepurl.com/bRwWdz" class="av-text"> <span>Copy</span> </span> <div id="zero_clipboard"></div> </div> <ul id="awesomesocial"> <li class="fbk"> <a class="fbk-like" id="campaign-fb-like-btn" rel="socialproxy" href="/?socialproxy=http%3A%2F%2Fus11.campaign-archive1.com%2Fsocial-proxy%2Ffacebook-like%3Fu%3D794c7fae79b8e528f8be29233%26id%3D803baf68c8%26url%3Dhttp%253A%252F%252Fus11.campaign-archive2.com%252F%253Fu%253D794c7fae79b8e528f8be29233%2526id%253D803baf68c8%26title%3DAWARDS%2520Page%2520for%2520Amit..." title="Like AWARDS Page for Amit... on Facebook">Facebook <b>0 <i>likes</i></b></a> </li> <li class="twt"> <a title="Share on Twitter" target="_blank" href="http://twitter.com/share?url=http%3A%2F%2Feepurl.com%2FbRwWdz">Twitter <b>0 <i>tweets</i></b></a> </li> <li class="ggl"> <a title="+1 on Google Plus" id="campaign-gpo-btn" href="http://us11.campaign-archive1.com/?u=794c7fae79b8e528f8be29233&amp;id=803baf68c8&amp;socialproxy=http%3A%2F%2Fus11.campaign-archive2.com%2Fsocial-proxy%2Fgoogle-plus-one%3Fu%3D794c7fae79b8e528f8be29233%26id%3D803baf68c8%26url%3Dhttp%253A%252F%252Fus11.campaign-archive1.com%252F%253Fu%253D794c7fae79b8e528f8be29233%2526id%253D803baf68c8%26title%3DAWARDS%2520Page%2520for%2520Amit..." class="twitter-share-button" rel="socialproxy">Google +1</a> </li> </ul> </div> <ul id="awesomebar"> <li> <a href="http://eepurl.com/bvbiqj" title="Subscribe to List" target="_blank">Subscribe</a> </li> <li id="toggleshare" class="more"><a href="#awesomeshare">Share</a></li> <li><a href="http://us11.campaign-archive2.com/home/?u=794c7fae79b8e528f8be29233&amp;id=332b49f859" title="View Past Issues" target="_blank">Past Issues</a></li> <li class="float-r"><a class="rss" target="_blank" href="http://us11.campaign-archive2.com/feed?u=794c7fae79b8e528f8be29233&amp;id=332b49f859" title="subscribe to the email archive feed">RSS</a></li> <li class="float-r more"><a href="#translate">Translate</a> <ul id="translate"> <li> <a rel="nofollow" title="English" href="http://translate.google.com/translate?hl=auto&amp;langpair=auto|en&amp;u=http%3A%2F%2Fus11.campaign-archive2.com%2F%3Fu%3D794c7fae79b8e528f8be29233%26id%3D803baf68c8%26e%3D">English</a> </li><li> <a rel="nofollow" title="العربية" href="http://translate.google.com/translate?hl=auto&amp;langpair=auto|ar&amp;u=http%3A%2F%2Fus11.campaign-archive2.com%2F%3Fu%3D794c7fae79b8e528f8be29233%26id%3D803baf68c8%26e%3D">العربية</a> </li><li> <a rel="nofollow" title="Afrikaans" href="http://translate.google.com/translate?hl=auto&amp;langpair=auto|af&amp;u=http%3A%2F%2Fus11.campaign-archive2.com%2F%3Fu%3D794c7fae79b8e528f8be29233%26id%3D803baf68c8%26e%3D">Afrikaans</a> </li><li> <a rel="nofollow" title="белару�?ка�? мова" href="http://translate.google.com/translate?hl=auto&amp;langpair=auto|be&amp;u=http%3A%2F%2Fus11.campaign-archive2.com%2F%3Fu%3D794c7fae79b8e528f8be29233%26id%3D803baf68c8%26e%3D">белару�?ка�? мова</a> </li><li> <a rel="nofollow" title="българ�?ки" href="http://translate.google.com/translate?hl=auto&amp;langpair=auto|bg&amp;u=http%3A%2F%2Fus11.campaign-archive2.com%2F%3Fu%3D794c7fae79b8e528f8be29233%26id%3D803baf68c8%26e%3D">българ�?ки</a> </li><li> <a rel="nofollow" title="català" href="http://translate.google.com/translate?hl=auto&amp;langpair=auto|ca&amp;u=http%3A%2F%2Fus11.campaign-archive2.com%2F%3Fu%3D794c7fae79b8e528f8be29233%26id%3D803baf68c8%26e%3D">català</a> </li><li> <a rel="nofollow" title="中文（简体）" href="http://translate.google.com/translate?hl=auto&amp;langpair=auto|zh-CN&amp;u=http%3A%2F%2Fus11.campaign-archive2.com%2F%3Fu%3D794c7fae79b8e528f8be29233%26id%3D803baf68c8%26e%3D">中文（简体）</a> </li><li> <a rel="nofollow" title="中文（�?體）" href="http://translate.google.com/translate?hl=auto&amp;langpair=auto|zh-TW&amp;u=http%3A%2F%2Fus11.campaign-archive2.com%2F%3Fu%3D794c7fae79b8e528f8be29233%26id%3D803baf68c8%26e%3D">中文（�?體）</a> </li><li> <a rel="nofollow" title="Hrvatski" href="http://translate.google.com/translate?hl=auto&amp;langpair=auto|hr&amp;u=http%3A%2F%2Fus11.campaign-archive2.com%2F%3Fu%3D794c7fae79b8e528f8be29233%26id%3D803baf68c8%26e%3D">Hrvatski</a> </li><li> <a rel="nofollow" title="Česky" href="http://translate.google.com/translate?hl=auto&amp;langpair=auto|cs&amp;u=http%3A%2F%2Fus11.campaign-archive2.com%2F%3Fu%3D794c7fae79b8e528f8be29233%26id%3D803baf68c8%26e%3D">Česky</a> </li><li> <a rel="nofollow" title="Dansk" href="http://translate.google.com/translate?hl=auto&amp;langpair=auto|da&amp;u=http%3A%2F%2Fus11.campaign-archive2.com%2F%3Fu%3D794c7fae79b8e528f8be29233%26id%3D803baf68c8%26e%3D">Dansk</a> </li><li> <a rel="nofollow" title="eesti&nbsp;keel" href="http://translate.google.com/translate?hl=auto&amp;langpair=auto|et&amp;u=http%3A%2F%2Fus11.campaign-archive2.com%2F%3Fu%3D794c7fae79b8e528f8be29233%26id%3D803baf68c8%26e%3D">eesti&nbsp;keel</a> </li><li> <a rel="nofollow" title="Nederlands" href="http://translate.google.com/translate?hl=auto&amp;langpair=auto|nl&amp;u=http%3A%2F%2Fus11.campaign-archive2.com%2F%3Fu%3D794c7fae79b8e528f8be29233%26id%3D803baf68c8%26e%3D">Nederlands</a> </li><li> <a rel="nofollow" title="Suomi" href="http://translate.google.com/translate?hl=auto&amp;langpair=auto|fi&amp;u=http%3A%2F%2Fus11.campaign-archive2.com%2F%3Fu%3D794c7fae79b8e528f8be29233%26id%3D803baf68c8%26e%3D">Suomi</a> </li><li> <a rel="nofollow" title="Fran&ccedil;ais" href="http://translate.google.com/translate?hl=auto&amp;langpair=auto|fr&amp;u=http%3A%2F%2Fus11.campaign-archive2.com%2F%3Fu%3D794c7fae79b8e528f8be29233%26id%3D803baf68c8%26e%3D">Fran&ccedil;ais</a> </li><li> <a rel="nofollow" title="Deutsch" href="http://translate.google.com/translate?hl=auto&amp;langpair=auto|de&amp;u=http%3A%2F%2Fus11.campaign-archive2.com%2F%3Fu%3D794c7fae79b8e528f8be29233%26id%3D803baf68c8%26e%3D">Deutsch</a> </li><li> <a rel="nofollow" title="&Epsilon;&lambda;&lambda;&eta;&nu;&iota;&kappa;ή" href="http://translate.google.com/translate?hl=auto&amp;langpair=auto|el&amp;u=http%3A%2F%2Fus11.campaign-archive2.com%2F%3Fu%3D794c7fae79b8e528f8be29233%26id%3D803baf68c8%26e%3D">&Epsilon;&lambda;&lambda;&eta;&nu;&iota;&kappa;ή</a> </li><li> <a rel="nofollow" title="हिन�?दी" href="http://translate.google.com/translate?hl=auto&amp;langpair=auto|hi&amp;u=http%3A%2F%2Fus11.campaign-archive2.com%2F%3Fu%3D794c7fae79b8e528f8be29233%26id%3D803baf68c8%26e%3D">हिन�?दी</a> </li><li> <a rel="nofollow" title="Magyar" href="http://translate.google.com/translate?hl=auto&amp;langpair=auto|hu&amp;u=http%3A%2F%2Fus11.campaign-archive2.com%2F%3Fu%3D794c7fae79b8e528f8be29233%26id%3D803baf68c8%26e%3D">Magyar</a> </li><li> <a rel="nofollow" title="Gaeilge" href="http://translate.google.com/translate?hl=auto&amp;langpair=auto|ga&amp;u=http%3A%2F%2Fus11.campaign-archive2.com%2F%3Fu%3D794c7fae79b8e528f8be29233%26id%3D803baf68c8%26e%3D">Gaeilge</a> </li><li> <a rel="nofollow" title="Indonesia" href="http://translate.google.com/translate?hl=auto&amp;langpair=auto|id&amp;u=http%3A%2F%2Fus11.campaign-archive2.com%2F%3Fu%3D794c7fae79b8e528f8be29233%26id%3D803baf68c8%26e%3D">Indonesia</a> </li><li> <a rel="nofollow" title="íslenska" href="http://translate.google.com/translate?hl=auto&amp;langpair=auto|is&amp;u=http%3A%2F%2Fus11.campaign-archive2.com%2F%3Fu%3D794c7fae79b8e528f8be29233%26id%3D803baf68c8%26e%3D">íslenska</a> </li><li> <a rel="nofollow" title="Italiano" href="http://translate.google.com/translate?hl=auto&amp;langpair=auto|it&amp;u=http%3A%2F%2Fus11.campaign-archive2.com%2F%3Fu%3D794c7fae79b8e528f8be29233%26id%3D803baf68c8%26e%3D">Italiano</a> </li><li> <a rel="nofollow" title="日本語" href="http://translate.google.com/translate?hl=auto&amp;langpair=auto|ja&amp;u=http%3A%2F%2Fus11.campaign-archive2.com%2F%3Fu%3D794c7fae79b8e528f8be29233%26id%3D803baf68c8%26e%3D">日本語</a> </li><li> <a rel="nofollow" title="ភាសា�?្មែរ" href="http://translate.google.com/translate?hl=auto&amp;langpair=auto|km&amp;u=http%3A%2F%2Fus11.campaign-archive2.com%2F%3Fu%3D794c7fae79b8e528f8be29233%26id%3D803baf68c8%26e%3D">ភាសា�?្មែរ</a> </li><li> <a rel="nofollow" title="한국어" href="http://translate.google.com/translate?hl=auto&amp;langpair=auto|ko&amp;u=http%3A%2F%2Fus11.campaign-archive2.com%2F%3Fu%3D794c7fae79b8e528f8be29233%26id%3D803baf68c8%26e%3D">한국어</a> </li><li> <a rel="nofollow" title="македон�?ки&nbsp;јазик" href="http://translate.google.com/translate?hl=auto&amp;langpair=auto|mk&amp;u=http%3A%2F%2Fus11.campaign-archive2.com%2F%3Fu%3D794c7fae79b8e528f8be29233%26id%3D803baf68c8%26e%3D">македон�?ки&nbsp;јазик</a> </li><li> <a rel="nofollow" title="بهاس ملايو" href="http://translate.google.com/translate?hl=auto&amp;langpair=auto|ms&amp;u=http%3A%2F%2Fus11.campaign-archive2.com%2F%3Fu%3D794c7fae79b8e528f8be29233%26id%3D803baf68c8%26e%3D">بهاس ملايو</a> </li><li> <a rel="nofollow" title="Malti" href="http://translate.google.com/translate?hl=auto&amp;langpair=auto|mt&amp;u=http%3A%2F%2Fus11.campaign-archive2.com%2F%3Fu%3D794c7fae79b8e528f8be29233%26id%3D803baf68c8%26e%3D">Malti</a> </li><li> <a rel="nofollow" title="Norsk" href="http://translate.google.com/translate?hl=auto&amp;langpair=auto|no&amp;u=http%3A%2F%2Fus11.campaign-archive2.com%2F%3Fu%3D794c7fae79b8e528f8be29233%26id%3D803baf68c8%26e%3D">Norsk</a> </li><li> <a rel="nofollow" title="Polski" href="http://translate.google.com/translate?hl=auto&amp;langpair=auto|pl&amp;u=http%3A%2F%2Fus11.campaign-archive2.com%2F%3Fu%3D794c7fae79b8e528f8be29233%26id%3D803baf68c8%26e%3D">Polski</a> </li><li> <a rel="nofollow" title="Portugu&ecirc;s" href="http://translate.google.com/translate?hl=auto&amp;langpair=auto|pt&amp;u=http%3A%2F%2Fus11.campaign-archive2.com%2F%3Fu%3D794c7fae79b8e528f8be29233%26id%3D803baf68c8%26e%3D">Portugu&ecirc;s</a> </li><li> <a rel="nofollow" title="Portugu&ecirc;s&nbsp;-&nbsp;Portugal" href="http://translate.google.com/translate?hl=auto&amp;langpair=auto|pt-PT&amp;u=http%3A%2F%2Fus11.campaign-archive2.com%2F%3Fu%3D794c7fae79b8e528f8be29233%26id%3D803baf68c8%26e%3D">Portugu&ecirc;s&nbsp;-&nbsp;Portugal</a> </li><li> <a rel="nofollow" title="Rom&acirc;nă" href="http://translate.google.com/translate?hl=auto&amp;langpair=auto|ro&amp;u=http%3A%2F%2Fus11.campaign-archive2.com%2F%3Fu%3D794c7fae79b8e528f8be29233%26id%3D803baf68c8%26e%3D">Rom&acirc;nă</a> </li><li> <a rel="nofollow" title="Ру�?�?кий" href="http://translate.google.com/translate?hl=auto&amp;langpair=auto|ru&amp;u=http%3A%2F%2Fus11.campaign-archive2.com%2F%3Fu%3D794c7fae79b8e528f8be29233%26id%3D803baf68c8%26e%3D">Ру�?�?кий</a> </li><li> <a rel="nofollow" title="Espa&ntilde;ol" href="http://translate.google.com/translate?hl=auto&amp;langpair=auto|es&amp;u=http%3A%2F%2Fus11.campaign-archive2.com%2F%3Fu%3D794c7fae79b8e528f8be29233%26id%3D803baf68c8%26e%3D">Espa&ntilde;ol</a> </li><li> <a rel="nofollow" title="Kiswahili" href="http://translate.google.com/translate?hl=auto&amp;langpair=auto|sw&amp;u=http%3A%2F%2Fus11.campaign-archive2.com%2F%3Fu%3D794c7fae79b8e528f8be29233%26id%3D803baf68c8%26e%3D">Kiswahili</a> </li><li> <a rel="nofollow" title="Svenska" href="http://translate.google.com/translate?hl=auto&amp;langpair=auto|sv&amp;u=http%3A%2F%2Fus11.campaign-archive2.com%2F%3Fu%3D794c7fae79b8e528f8be29233%26id%3D803baf68c8%26e%3D">Svenska</a> </li><li> <a rel="nofollow" title="עברית" href="http://translate.google.com/translate?hl=auto&amp;langpair=auto|iw&amp;u=http%3A%2F%2Fus11.campaign-archive2.com%2F%3Fu%3D794c7fae79b8e528f8be29233%26id%3D803baf68c8%26e%3D">עברית</a> </li><li> <a rel="nofollow" title="Lietuvių" href="http://translate.google.com/translate?hl=auto&amp;langpair=auto|lt&amp;u=http%3A%2F%2Fus11.campaign-archive2.com%2F%3Fu%3D794c7fae79b8e528f8be29233%26id%3D803baf68c8%26e%3D">Lietuvių</a> </li><li> <a rel="nofollow" title="latviešu" href="http://translate.google.com/translate?hl=auto&amp;langpair=auto|lv&amp;u=http%3A%2F%2Fus11.campaign-archive2.com%2F%3Fu%3D794c7fae79b8e528f8be29233%26id%3D803baf68c8%26e%3D">latviešu</a> </li><li> <a rel="nofollow" title="sloven�?ina" href="http://translate.google.com/translate?hl=auto&amp;langpair=auto|sk&amp;u=http%3A%2F%2Fus11.campaign-archive2.com%2F%3Fu%3D794c7fae79b8e528f8be29233%26id%3D803baf68c8%26e%3D">sloven�?ina</a> </li><li> <a rel="nofollow" title="slovenš�?ina" href="http://translate.google.com/translate?hl=auto&amp;langpair=auto|sl&amp;u=http%3A%2F%2Fus11.campaign-archive2.com%2F%3Fu%3D794c7fae79b8e528f8be29233%26id%3D803baf68c8%26e%3D">slovenš�?ina</a> </li><li> <a rel="nofollow" title="�?рп�?ки" href="http://translate.google.com/translate?hl=auto&amp;langpair=auto|sr&amp;u=http%3A%2F%2Fus11.campaign-archive2.com%2F%3Fu%3D794c7fae79b8e528f8be29233%26id%3D803baf68c8%26e%3D">�?рп�?ки</a> </li><li> <a rel="nofollow" title="தமிழ�?" href="http://translate.google.com/translate?hl=auto&amp;langpair=auto|ta&amp;u=http%3A%2F%2Fus11.campaign-archive2.com%2F%3Fu%3D794c7fae79b8e528f8be29233%26id%3D803baf68c8%26e%3D">தமிழ�?</a> </li><li> <a rel="nofollow" title="ภาษาไทย" href="http://translate.google.com/translate?hl=auto&amp;langpair=auto|th&amp;u=http%3A%2F%2Fus11.campaign-archive2.com%2F%3Fu%3D794c7fae79b8e528f8be29233%26id%3D803baf68c8%26e%3D">ภาษาไทย</a> </li><li> <a rel="nofollow" title="Türkçe" href="http://translate.google.com/translate?hl=auto&amp;langpair=auto|tr&amp;u=http%3A%2F%2Fus11.campaign-archive2.com%2F%3Fu%3D794c7fae79b8e528f8be29233%26id%3D803baf68c8%26e%3D">Türkçe</a> </li><li> <a rel="nofollow" title="Filipino" href="http://translate.google.com/translate?hl=auto&amp;langpair=auto|tl&amp;u=http%3A%2F%2Fus11.campaign-archive2.com%2F%3Fu%3D794c7fae79b8e528f8be29233%26id%3D803baf68c8%26e%3D">Filipino</a> </li><li> <a rel="nofollow" title="украї�?н�?ька" href="http://translate.google.com/translate?hl=auto&amp;langpair=auto|uk&amp;u=http%3A%2F%2Fus11.campaign-archive2.com%2F%3Fu%3D794c7fae79b8e528f8be29233%26id%3D803baf68c8%26e%3D">украї�?н�?ька</a> </li><li> <a rel="nofollow" title="Tiếng&nbsp;Việt" href="http://translate.google.com/translate?hl=auto&amp;langpair=auto|vi&amp;u=http%3A%2F%2Fus11.campaign-archive2.com%2F%3Fu%3D794c7fae79b8e528f8be29233%26id%3D803baf68c8%26e%3D">Tiếng&nbsp;Việt</a> </li> </ul> </li> </ul> </div>-->
        <body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0" style="margin: 0;padding: 0;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #F2F2F2;height: 100% !important;width: 100% !important;">
        <center>
            <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;margin: 0;padding: 0;background-color: #F2F2F2;height: 100% !important;width: 100% !important;">
                <tr>
                    <td align="center" valign="top" id="bodyCell" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;margin: 0;padding: 20px;border-top: 0;height: 100% !important;width: 100% !important;">
                        <!-- BEGIN TEMPLATE // -->
                        <table border="0" cellpadding="0" cellspacing="0" width="600" id="templateContainer" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;border: 0;">
                            <tr>
                                <td align="center" valign="top" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                    <!-- BEGIN PREHEADER // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="600" id="templatePreheader" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;min-width: 100%;background-color: #FFFFFF;border-top: 0;border-bottom: 0;">
                                        <tr>
                                        	<td valign="top" class="preheaderContainer" style="padding-top: 9px;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
    <!--<tbody class="mcnTextBlockOuter">
        <tr>
            <td valign="top" class="mcnTextBlockInner" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                
                <table align="left" border="0" cellpadding="0" cellspacing="0" width="366" class="mcnTextContentContainer" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                    <tbody><tr>
                        
                        <td valign="top" class="mcnTextContent" style="padding-top: 9px;padding-left: 18px;padding-bottom: 9px;padding-right: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #606060;font-family: Helvetica;font-size: 11px;line-height: 125%;text-align: left;">
                        
                            
                        </td>
                    </tr>
                </tbody></table>
                
                <table align="right" border="0" cellpadding="0" cellspacing="0" width="197" class="mcnTextContentContainer" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                    <tbody><tr>
                        
                        <td valign="top" class="mcnTextContent" style="padding-top: 9px;padding-right: 18px;padding-bottom: 9px;padding-left: 18px;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #606060;font-family: Helvetica;font-size: 11px;line-height: 125%;text-align: left;">
                        
                            <a href="http://us11.campaign-archive2.com/?u=794c7fae79b8e528f8be29233&id=803baf68c8&e=[UNIQID]" target="_blank" style="word-wrap: break-word;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #606060;font-weight: normal;text-decoration: underline;">View this email in your browser</a>
                        </td>
                    </tr>
                </tbody></table>
                
            </td>
        </tr>
    </tbody>-->
</table></td>
                                        </tr>
                                    </table>
                                    <!-- // END PREHEADER -->
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                    <!-- BEGIN HEADER // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="600" id="templateHeader" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;min-width: 100%;background-color: #FFFFFF;border-top: 0;border-bottom: 0;">
                                        <tr>
                                            <td valign="top" class="headerContainer" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td valign="top" class="mcnTextBlockInner" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                
                <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnTextContentContainer">
                    <tbody><tr>
                        
                        <td valign="top" class="mcnTextContent" style="padding-top: 9px;padding-right: 18px;padding-bottom: 9px;padding-left: 18px;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #606060;font-family: Helvetica;font-size: 15px;line-height: 150%;text-align: left;">
                        
                            <h1 style="text-align: center;margin: 0;padding: 0;display: block;font-family: Helvetica;font-size: 40px;font-style: normal;font-weight: bold;line-height: 125%;letter-spacing: -1px;color: #606060 !important;"><span style="color:#FF0000">Finalists &amp; Winners of 2017</span><br>
<span style="color:#696969">'Peoples Choice Awards'</span><br>
<span style="color:#FF0000">for Best Retirement Villages and Best Aged Care.</span></h1>

                        </td>
                    </tr>
                </tbody></table>
                
            </td>
        </tr>
    </tbody>
</table></td>
                                        </tr>
                                    </table>
                                    <!-- // END HEADER -->
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                    <!-- BEGIN BODY // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="600" id="templateBody" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;min-width: 100%;background-color: #FFFFFF;border-top: 0;border-bottom: 0;">
                                        <tr>
                                            <td valign="top" class="bodyContainer" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnImageBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
    <tbody class="mcnImageBlockOuter">
            <tr>
                <td valign="top" style="padding: 9px;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnImageBlockInner">
                    <table align="left" width="100%" border="0" cellpadding="0" cellspacing="0" class="mcnImageContentContainer" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                        <tbody><tr>
                            <td class="mcnImageContent" valign="top" style="padding-right: 9px;padding-left: 9px;padding-top: 0;padding-bottom: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                
                                    
                                        <img align="left" alt="" src="/images/2017awards.jpg" width="564" style="max-width: 629px;padding-bottom: 0;display: inline !important;vertical-align: bottom;border: 0;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" class="mcnImage">
                                    
                                
                            </td>
                        </tr>
                    </tbody></table>
                </td>
            </tr>
    </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td valign="top" class="mcnTextBlockInner" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                
                <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnTextContentContainer">
                    <tbody><tr>
                        
                        <td valign="top" class="mcnTextContent" style="padding-top: 9px;padding-right: 18px;padding-bottom: 9px;padding-left: 18px;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #606060;font-family: Helvetica;font-size: 15px;line-height: 150%;text-align: left;">
                        
                            <h1 style="text-align: center;margin: 0;padding: 0;display: block;font-family: Helvetica;font-size: 40px;font-style: normal;font-weight: bold;line-height: 125%;letter-spacing: -1px;color: #606060 !important;"><span style="color:#FF0000">Thank you </span><span style="color:#696969">again...</span></h1>

                        </td>
                    </tr>
                </tbody></table>
                
            </td>
        </tr>
    </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td valign="top" class="mcnTextBlockInner" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                
                <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnTextContentContainer">
                    <tbody><tr>
                        
                        <td valign="top" class="mcnTextContent" style="padding: 9px 18px;line-height: 150%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #606060;font-family: Helvetica;font-size: 15px;text-align: left;">
                        
                            <span style="line-height:20.8px"><strong>...</strong>for sharing your experiences. We have received nearly 2000 reviews and ratings for this years awards.<br>
<br>
<strong>Results</strong> of the Winners &amp; Finalists for the 2017 &quot;Peoples' Choice Awards&quot; for Best Retirement or Lifestyle Villages &amp; Aged Care Facilities:</span><br style="line-height: 20.8px;">
<span style="line-height:20.8px">&nbsp;</span>

<div style="text-align: center;"><br>
<span style="color:#FF0000"><strong style="line-height:20.8px">WINNERS - NORTH ISLAND</strong></span><br>
<strong><a href="/search/aged-care/Remuera-Rise-care-Remuera-Auckland" target="_blank">Remuera Rise</a>, Auckland</strong><br>
<em>Best Aged Care upto 40 beds</em><br>
<br>
<strong><a href="/search/aged-care/Malyon-House-Mount-Manganui-Tauranga" target="_blank">Malyon House</a>, Tauranga</strong><br>
<em>Best Aged Care over 40 beds</em><br>
<br>
<strong><a href="/search/retirement-village/Meadowbank-Retirement-Village-Meadowbank-Auckland" target="_blank">Meadowbank Retirement Village</a>, Auckland</strong><br>
<em>Best Retirement / Lifestyle Village</em><br>
<br>
<br style="line-height: 20.8px;">
<span style="color:#FF0000"><strong style="line-height:20.8px">WINNERS - SOUTH ISLAND</strong></span><br>
<strong><a href="/search/aged-care/Cheviot-Rest-Home-Cheviot-Cheviot" target="_blank">Cheviot Rest Home</a>, Canterbury</strong><br>
<em>Best Aged Care upto 40 beds</em><br>
<br>
<strong><a href="/search/aged-care/Archer-Archer-Home-Beckenham-Christchurch" target="_blank">Archer Home</a>, Christchurch</strong><br>
<em>Best Aged Care over 40 beds</em><br>
<br style="line-height: 20.8px;">
<strong><a href="/search/retirement-village/Park-Lane-Retirement-Village-Arvida-Addington-Christchurch" target="_blank">Parklane Retirement Village</a>, Christchurch</strong><br>
<em>Best Retirement / Lifestyle Village</em><br>
<br>
[Not awarded for 2017]<br style="line-height: 20.8px;">
<em>Best Multi-Facility Provider (Nationwide)</em><br>
<br>
----------------------<br>
<br>
<span style="color:#FF0000"><strong>FINALISTS - NORTH ISLAND</strong></span><br>
<strong><a href="/search/aged-care/Lynton-Lodge-Hospital-Westmere-Auckland" target="_blank">Lynton Lodge Hospital</a>, Auckland</strong><br>
<strong><a href="/search/aged-care/Woodlands-Of-Feilding--Feilding" target="_blank">Woodlands of Fielding</a>, Fielding</strong><br>
<em>Best Aged Care upto 40 beds</em><br>
<br>
<strong><a href="/search/aged-care/Gracelands-Rest-Home-And-Village-Frimley-Hastings" target="_blank">Gracelands Rest Home</a>, Hastings</strong><br>
<strong><a href="/search/aged-care/Cromwell-House-Hospital-Epsom-Auckland" target="_blank">Cromwell House and Hospital</a>, Auckland</strong><br>
<strong><a href="/search/aged-care/Te-Wiremu-House-Te-Hapara-Gisborne" target="_blank">Te Wiremu House | Anglican Care Waiapu</a>, Gisborne</strong><br>
<strong><a href="/search/aged-care/Eversley-Rest-Home-And-Village-Mahora-Hastings" target="_blank">Eversley Rest Home</a>, Hastings</strong><br>
<em>Best Aged Care over 40 beds</em><br>
<br>
<strong><a href="/search/aged-care/Remuera-Rise-care-Remuera-Auckland" target="_blank">REMUERA RISE</a>, Auckland</strong><br>
<strong><a href="/search/retirement-village/Redwood-Retirement-Village--Rotorua" target="_blank">Bupa REDWOOD RETIREMENT VILLAGE</a>, Rotorua</strong><br>
<strong><a href="/search/retirement-village/Gracelands-Retirement-Village-Frimley-Hastings" target="_blank">GRACELANDS RETIREMENT VILLAGE</a>, Hastings</strong><br>
<strong><a href="/search/retirement-village/Bupa-The-Gardens-Retirement-Village-Rotorua-Rotorua" target="_blank">Bupa THE GARDENS RETIREMENT VILLAGE</a>, Rotorua</strong><br>
<strong><a href="/search/retirement-village/Maygrove-Village-Orewa-Auckland" target="_blank">MAYGROVE VILLAGE</a>, Orewa</strong><br>
<strong><a href="/search/retirement-village/Mt-Eden-Gardens-Mt-Eden-Auckland" target="_blank">MT EDEN GARDENS</a>, Auckland</strong><br>
<em>Best Retirement / Lifestyle Village</em><br>
<br>
<br>
<span style="color:#FF0000"><strong>FINALISTS - SOUTH ISLAND</strong></span><br>
<strong><a href="/search/aged-care/Waikiwi-Gardens-Rest-Home-Waikiwi-Invercargill" target="_blank">Waikiwi Gardens Rest Home</a>, Invercargill</strong><br>
<em>Best Aged Care upto 40 beds</em><br>
<br>
<strong><a href="/search/aged-care/Holmwood-Rest-Home-Rangiora-Rangiora" target="_blank">Holmwood Rest Home</a>, Rangiora</strong><br>
<strong><a href="/search/aged-care/Ernest-Rutherford-Retirement-Village-Stoke-Nelson" target="_blank">Ernest Rutherford</a>, Nelson</strong><br>
<strong><a href="/search/aged-care/Rannerdale-War-Veterans-Hospital-And-Home-Upper-Riccarton-Christchurch" target="_blank">Rannerdale War Veterans' Hospital and Home</a> Christchurch</strong><br>
<em>Best Aged Care over 40 beds</em><br>
<br>
<strong><a href="/search/retirement-village/Diana-Isaac-Retirement-Village-Mairehau-Christchurch" target="_blank">DIANA ISAAC RETIREMENT VILLAGE</a>, Christchurch</strong><br>
<strong><a href="/search/retirement-village/Charles-Upham-Retirement-Village-Waimakariri-Rangiora" target="_blank">CHARLES UPHAM RETIREMENT VILLAGE</a>, Rangiora</strong><br>
<strong><a href="/search/retirement-village/Anthony-Wilding-Retirement-Village-Aidanfield-Christchurch" target="_blank">ANTHONY WILDING RETIREMENT VILLAGE</a>, Christchurch</strong><br>
<strong><a href="/search/retirement-village/Ngaio-Marsh-Retirement-Village-Papanui-Christchurch" target="_blank">NGAIO MARSH RETIREMENT VILLAGE</a>, Christchurch</strong><br>
<strong><a href="/search/retirement-village/The-Russley-Village-Christchurch-Christchurch" target="_blank">THE RUSSLEY VILLAGE</a>, Christchurch</strong><br>

<em>Best Retirement / Lifestyle Village</em>&nbsp;<br>
&nbsp;</div>

<hr><div style="text-align: center;"><span style="line-height:20.8px">Special thanks to our Sponsors.</span></div>

                        </td>
                    </tr>
                </tbody></table>
                
            </td>
        </tr>
    </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnImageBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
    <tbody class="mcnImageBlockOuter">
            <tr>
                <td valign="top" style="padding: 9px;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnImageBlockInner">
                    <table align="left" width="100%" border="0" cellpadding="0" cellspacing="0" class="mcnImageContentContainer" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                        <tbody><tr>
                            <td class="mcnImageContent" valign="top" style="padding-right: 9px;padding-left: 9px;padding-top: 0;padding-bottom: 0;text-align: center;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                
                                    <a href="http://hearingtech.co.nz" title="" class="" target="_blank" style="word-wrap: break-word;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                        <img align="center" alt="" src="https://gallery.mailchimp.com/794c7fae79b8e528f8be29233/images/dbe8d60a-3e3f-4889-869b-275a54610dd2.jpg" width="300" style="max-width: 300px;padding-bottom: 0;display: inline !important;vertical-align: bottom;border: 0;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" class="mcnImage">
                                    </a>
                                    <br><br><br>
                                    
                                    <a href="http://www.lawlink.co.nz/member.php" title="" class="" target="_blank" style="word-wrap: break-word;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                        <img align="center" alt="" src="/admin/files/pages/banner/1460551774_lawlink-logo.jpg" style="padding-bottom: 0;display: inline !important;vertical-align: bottom;border: 0;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" class="mcnImage">
                                    </a>
                                                                        <hr>
                                    
                                    
                                    
                                
                            </td>
                        </tr>
                    </tbody></table>
                </td>
            </tr>
    </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td valign="top" class="mcnTextBlockInner" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                
                <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnTextContentContainer">
                    <tbody><tr>
                        
                        <td valign="top" class="mcnTextContent" style="padding: 9px 18px;line-height: 150%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #606060;font-family: Helvetica;font-size: 15px;text-align: left;">
                        
                            <div style="text-align: left;"><span style="line-height:20.8px">Note: Aged Advisor winners are based on&nbsp;independent reviews and&nbsp;opinions from people that live, visit or work at the retirement/lifestyle villages and residential care facilities throughout New Zealand.<br>
<br>
The team at Aged Advisor are thrilled to celebrate the exceptional organisations who offer independent living options or care for our older generation, and are proud to award them with the recognition they deserve.</span><br>
<br>
Entry is now open for next years Awards. Make sure to <a href="/rating-supplier">write your review</a>.</div>

                        </td>
                    </tr>
                </tbody></table>
                
            </td>
        </tr>
    </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
    <tbody class="mcnDividerBlockOuter">
        <tr>
            <td class="mcnDividerBlockInner" style="min-width: 100%;padding: 10px 18px;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                <table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-top-width: 2px;border-top-style: solid;border-top-color: #EAEAEA;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                    <tbody><tr>
                        <td style="text-align: center;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                            <span><a href="/awards-best-retirement-villages-aged-care-2015-2016">Click here to view previous years awards</a></span>
                        </td>
                    </tr>
                </tbody></table>
<!--            
                <td class="mcnDividerBlockInner" style="padding: 18px;">
                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
            </td>
        </tr>
    </tbody>
</table></td>
                                        </tr>
                                    </table>
                                    <!-- // END FOOTER -->
                                </td>
                            </tr>
                        </table>
                        <!-- // END TEMPLATE -->
                    </td>
                </tr>
            </table>
        </center>
    </body>    </body> </html>
