<?php
    $sql_query = "SELECT * FROM " . _prefix("faqs") . " where status=1 AND deleted=0 order by order_by DESC";
    $res = $db->sql_query($sql_query);
    $records = $db->sql_fetchrowset($res);
    $c=1;
?>
<style type="text/css">
    .faqlistdiv{margin-top: -1px;margin-bottom: 0;}
    #faqContainer:first-child{margin-top: 3% !important;}
    #faqContainer .faqlistdiv:last-child{margin-bottom: 5% !important;}
	.btn {
  background-color: #5f5b5b;
  border: none;
  color: white;
  padding: 11px 17px;
  font-size: 16px;
  cursor: pointer;
}
.btn:hover {
  background-color: #f15922;
  color: #fff;
  text-decoration: none;
}
</style>

<div class="call-to-action pad-tb-70" style="background-image: url(<?php echo HOME_PATH;?>/modules/pages/images/bg/memphis-colorful.png); height:250px;">
            <div class="overlay op-5 green"></div>
            <div class="container">
                <div class="row align-items-center  pad-top-80">
                    <div class="col-md-6 col-12">
                        <div class="breadcrumb-menu">
                            <h2 class="page-title" style="font-size: 30px; color:#141414;">Supplier FAQs</h2>
                        </div>
                    </div>
                    <div class="col-md-6 col-12">
                        <div class="breadcrumb-menu text-right sm-left">
                            <ul>
                                <li class="active"><a href="<?php echo HOME_PATH;?>">Home</a></li>
                                <li><a href="<?php echo HOME_PATH;?>faq">FAQ</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		
	<!--MAIN BODY CODE ENDS-->
<script type="text/javascript">$(".showit").click(function(){$(this).next(".show_details").slideToggle("slow")});</script>

  
    <!--Page Wrapper starts-->
      
         
        <!--FAQ Section starts-->
        <div class="list-details-section faq-section pad-bot-90">
            <div class="container">
                <div class="row">
                   
                    <div class="col-lg-12">
                        <!--Listing Details starts-->
                        <div class="list-details-wrap">
                            <div id="booking" class="list-details-section">
                            
								<?php $i=0; foreach ($records as $key => $record) { $c=$c+1;?>
                                <div id="accordion1_<?php echo $c; ?>" role="tablist">
								
                                    <div class="card">
                                        <div class="card-header" role="tab" id="headingOne_<?php echo $c; ?>">
                                            <a role="button" data-toggle="collapse" data-parent="#accordion1_<?php echo $c; ?>" href="#collapseOne_<?php echo $c; ?>" aria-expanded="true" aria-controls="collapseOne_<?php echo $c; ?>">
                                                <?php echo $record['title']; ?>
                                            </a>
                                        </div>
                                        <div id="collapseOne_<?php echo $c; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne_<?php echo $c; ?>">
                                            <div class="card-body">
                                        <?php echo $record['content']; ?>
                                            </div>
                                        </div>
                                    </div>
								
                               
                           
                                </div>
									<?php } ?>
                            </div>
                          
                           
                       
                           
                        </div>
						
						 
                        <!--FAQ Section starts-->
                    </div>
					
                </div>
            </div>
        </div>
   
  
<style>
h2.page-title.a {
    color: black !important;
}
</style>


