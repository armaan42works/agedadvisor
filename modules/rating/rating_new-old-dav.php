<?php
ini_set('display_errors', '1');
require_once ('modules/fblogin/facebook-sdk-v5/autoload.php');

$new_password = true;

$fb = new Facebook\Facebook([
        'app_id' => '1409054779407381', // Replace {app-id} with your app id
        'app_secret' => 'e7dcb280e81129960a9a99d60b91c3f9',
        'default_graph_version' => 'v2.8'
    ]);

$helper = $fb->getRedirectLoginHelper();

$permissions = ['email', 'user_location', 'public_profile']; // Optional permissions
$loginUrl = $helper->getLoginUrl('https://www.agedadvisor.nz/facebook-login/', $permissions);
$loginUrl = htmlspecialchars($loginUrl);
$_SESSION['back_url'] = 'review';

global $db;
$add_subject = '';

$dodgy = FALSE;

if (ISSET($_SERVER['HTTP_USER_AGENT'])) {
    $ua = $_SERVER['HTTP_USER_AGENT'];
    if (preg_match('/\bmsie [5-6]\./i', $ua) && !preg_match('/\b(opera)/i', $ua)) {
        $dodgy = TRUE;
    }
}

setcookie('back_url', $_SERVER['REQUEST_URI'], time() + 3600);
$showmsg = 0;
if (isset($_REQUEST['resp']) && !empty($_REQUEST['resp']) && isset($_REQUEST['enq']) && !empty($_REQUEST['enq']) && isset($_REQUEST['prId']) && !empty($_REQUEST['prId'])) {
    $response = $_REQUEST['resp'];
    $enqId = base64_decode($_REQUEST['enq']);
    $prodId = base64_decode($_REQUEST['prId']);

    $fields = array(
        'third_followup_sent' => "y",
        'chosen_a_facility' => "yes",
        'chosen_facility_id' => $prodId
    );
    $where = "where id=" . $enqId . "";
    $update_result = $db->update(_prefix('enquiries'), $fields, $where);

    $showmsg = 1;
}

if (isset($_REQUEST['resp']) && !empty($_REQUEST['resp']) && isset($_REQUEST['enq']) && !empty($_REQUEST['enq']) && isset($_REQUEST['prId']) && !empty($_REQUEST['prId'])) {
    if ($_REQUEST['prId'] == "NULL") {
        $response = $_REQUEST['resp'];
        $enqId = base64_decode($_REQUEST['enq']);
        $prodId = $_REQUEST['prId'];
        $fields = array(
            'third_followup_sent' => "y",
            'chosen_a_facility' => "chosen other facility",
            'chosen_facility_id' => 99999
        );
        $where = "where id=" . $enqId . "";
        $update_result = $db->update(_prefix('enquiries'), $fields, $where);
        $showmsg = 1;
    }
}

function get_userid($email = '', $phone = '', $uname = '') {
    global $db;
    $userid = '';
    $sql_query = "SELECT id FROM ad_users where (email  LIKE '$email' OR user_name LIKE '$uname') && password='' ";    
    $res = mysqli_query($db->db_connect_id,$sql_query);    
    
    $records = mysqli_fetch_assoc($res);
    $rows = mysqli_num_rows($res);
    if ($rows > 0) {
        $userid = $records['id'];
    }
    return $userid;
    
}

function countDigits($text) {
    return count(array_filter(str_split($text), 'is_numeric'));
}

if (isset($_POST['country']) && $_POST['country']) {

    if ($_POST['phone'] == '' && $_POST['email'] == '') {
        $errors = 'You must provide an Email address or Phone number for verification.';
        if ($_POST['overall_rating'] < 0.5) {
            $errors .= "<br>You must select an Overall Rating below. <a href=\"#overall\">Click here to go to it</a>";
        }
    } else if ($_POST['overall_rating'] < 0.5) {
        $errors = "You must select an Overall Rating below. <a href=\"#overall\">Click here to go to it</a>";
    } else {
        if (1 == 1) {// Always do it
            $first_name = cleanthishere($_POST['first_name']);
            $yes_no_planto = cleanthishere($_POST['yes_no_planto']);
            $who_did_you_tell = cleanthishere($_POST['who_did_you_tell']);
            $what_response = cleanthishere($_POST['what_response']);
            $Nantionality = cleanthishere($_POST['nationality']);
            $why_choose_check = implode(",", $_POST['why_choose']);
            $why_choose1 = cleanthishere($_POST['why_choose1']);
            $why_choose2 = cleanthishere($_POST['why_choose2']);
            if(ISSET($_POST['initials_staff'])){ $initials_staff = cleanthishere($_POST['initials_staff']);}else{$initials_staff='';}

            if (!empty($why_choose1)) {
                $why_choose_check .= ",$why_choose1";
            }
            if (!empty($why_choose2)) {
                $why_choose_check .= ",$why_choose2";
            }
            
            $why_choose = $why_choose_check;
            $last_name = cleanthishere($_POST['last_name']);
            $email = cleanthishere($_POST['email']);
            $phone = cleanthishere($_POST['phone']);
            
            if (strlen($phone) > 1 && countDigits($phone) < 3) {
                $dodgy = TRUE;
            }

            if ($first_name && $first_name == $last_name) {
                $dodgy = TRUE;
            }

            $password = cleanthishere($_POST['password']);
            $user_name = cleanthishere($_POST['user_name']);
            $reviewer_TYPE = '';
            $country = cleanthishere($_POST['country']);
            $email_updates_facility = cleanthishere($_POST['email_updates_facility']);
            if ($email_updates_facility == 'yes') {
                $email_updates_facility = 1;
            } else {
                $email_updates_facility = 0;
            }

            // echo "About to save account";exit;
            if (ISSET($_SERVER['REMOTE_ADDR'])) {
                $ip_address = $_SERVER['REMOTE_ADDR'];
            } else {
                $ip_address = "unknown";
            }

            $deleted = 0;
            if ($dodgy) {
                $deleted = 1;
            }

            $existing_user_no_password = get_userid($email, $phone, $user_name);
            $new_password = TRUE;
            if (!$existing_user_no_password && ISSET($_POST['email']) && $_POST['email']) {
                $testemail = cleanthishere($_POST['email']);                
                $qry = "select id from " . _prefix('users') . "  where email LIKE '$testemail' ORDER BY ID DESC"; // In case of 2 users found only grab the first entry 
                record_time($qry);

                $result = $db->sql_query($qry);
                if (mysqli_num_rows($result) > 0) {
                    $dataemail = $db->sql_fetchrow($result);
                    $userId = $existing_user_no_password = $dataemail['id'];                    
                    $_SESSION['csId'] = $userId;
                    $new_password = FALSE; // do not update the password
                    // Do not bother with validating an email address or phone
                    //Log them in
                    $qry = "select * from " . _prefix('users') . " where id= '$userId' ";
                    record_time($qry);
                    $result = $db->sql_query($qry);
                    record_time($qry);
                    $data = $db->sql_fetchrow($result);
                    if ($db->sql_numrows($result) > 0) {                        
                        $_SESSION = array();
                        $_SESSION['csUserFullName'] = $data['first_name'] . " " . $data['last_name'];
                        $_SESSION['csEmail'] = $data['email'];
                        $_SESSION['csUserName'] = $data['user_name'];
                        $_SESSION['csId'] = $data['id'];
                        $user_type = 0;
                    }                    
                }
            }

            if ($existing_user_no_password) {                
                //Just update the user ie giving them a password and update details
                //Update as agreed to terms and cons    for //echo "<h1>$userId</h1>";
                //update current_plan fields of current supplier to 0
                if ($new_password) {                    
                    $update_fields = array(
                        'first_name' => trim($first_name),
                        'last_name' => trim($last_name),
                        'email' => trim($email),
                        'phone' => trim($phone),
                        'password' => md5(trim($password)),                        
                        'user_name' => trim($user_name),
                        'reviewer_TYPE' => $reviewer_TYPE,                        
                        'user_type' => '0',
                        'deleted' => $deleted,
                        'validate' => '2',
                        'agree_terms' => '1',
                        'ip_address' => $ip_address,
                        'country_id' => $country,
                        'email_updates_facility' => $email_updates_facility
                    );
                } else {                    
                    $update_fields = array(
                        'first_name' => trim($first_name),
                        'last_name' => trim($last_name),
                        'email' => trim($email),
                        'phone' => trim($phone),                        
                        'reviewer_TYPE' => $reviewer_TYPE,                         
                        'user_type' => '0',
                        'deleted' => $deleted,
                        'validate' => '2',
                        'agree_terms' => '1',
                        'ip_address' => $ip_address,
                        'country_id' => $country,
                        'email_updates_facility' => $email_updates_facility
                    );
                }

                $where = "where id= '$existing_user_no_password' ";
                $insert_result = $db->update(_prefix('users'), $update_fields, $where);                 
            }// end update user
            else {// they are definitely new
                $fields = array(
                    'first_name' => trim($first_name),
                    'last_name' => trim($last_name),
                    'email' => trim($email),
                    'phone' => trim($phone),
                    'password' => md5(trim($password)),
                    //'trade' => trim($trade),
                    'user_name' => trim($user_name),
                    'reviewer_TYPE' => $reviewer_TYPE,
                    //'zipcode' => trim($zipcode),
                    'user_type' => '0',
                    'validate' => '2',
                    'deleted' => $deleted,
                    'agree_terms' => '1',
                    'ip_address' => $ip_address,
                    'country_id' => $country,
                    'email_updates_facility' => $email_updates_facility
                );
                //print_r($fields);
                $insert_result = $db->insert(_prefix('users'), $fields);
                $id = $db->last_id();
                //echo "About to save account".$insert_result;exit;
            }// end insert new user
            if ($insert_result && $new_password) {

                // Do not bother with validating an email address or phone
                //Log them in
                $qry = "select * from " . _prefix('users') . ' where (validate=2 AND status=1 AND user_name="' . trim($user_name) . '" AND password="' . md5($password) . '" AND user_type=0)';
                record_time($qry);
                $result = $db->sql_query($qry);
                record_time($qry);
                $data = $db->sql_fetchrow($result);
                if ($db->sql_numrows($result) > 0) {
                    /* To destroy the session */
                    //  session_unset();
                    //  session_destroy();
                    $_SESSION = array();
                    $_SESSION['csUserFullName'] = $data['first_name'] . " " . $data['last_name'];
                    $_SESSION['csEmail'] = $data['email'];
                    $_SESSION['csUserName'] = $data['user_name'];
                    $_SESSION['csId'] = $data['id'];
                    $user_type = 0;
                } // if result           
            }
        }
    }
}


// Above will log them in unless an error with their input

$needs_search_facility = FALSE;
$needs_login = FALSE;
$productId = base64_decode($prId);

if (ISSET($_POST['restcareid'])) {
    $restcareid = cleanthishere($_POST['restcareid']);
    $productId = $restcareid;
}
if (ISSET($_POST['supplierId'])) {
    $supplierId = cleanthishere($_POST['supplierId']);
}


if (!ISSET($supplierId)) {
    $supplierId = base64_decode($spId);
}
if (isset($_SESSION['userId']) && !empty($_SESSION['userId'])) {
    $userId = $_SESSION['userId'];
} else if (isset($_SESSION['csId']) && !empty($_SESSION['csId'])) {
    $userId = $_SESSION['csId'];
} else {
    $needs_login = TRUE;
//    $msg = 'Please login to add a review';
//   $_SESSION['msg'] = $msg;
//    $time = time() + 7 * 24 * 3600;
//    setcookie('back_url', $_SERVER['REQUEST_URI'], $time);
//    redirect_to(HOME_PATH . 'login');
}

//echo "$userId";
// test user for dodgyness
$they_have_agreed_terms_previously = FALSE;
$they_have_agreed_newsletter_previously = FALSE;
$querys = " SELECT first_name,last_name,phone,deleted,agree_terms,email_updates_facility FROM " . _prefix("users")
        . " WHERE id =" . cleanthishere($userId) . "";
record_time($querys);
$planres = $db->sql_query($querys);
record_time($querys);
$dataplans = $db->sql_fetchrow($planres);

$first_name = $dataplans['first_name'];
$last_name = $dataplans['last_name'];
$phone = $dataplans['phone'];
$deleted = $dataplans['deleted'];

//echo "<h1>$userId</h1>";
if ($dataplans['agree_terms']) {
    $they_have_agreed_terms_previously = TRUE;
}
if ($dataplans['email_updates_facility']) {
    $they_have_agreed_newsletter_previously = TRUE;
}
if (substr($phone, 0, 1) == '8') {
    $dodgy = TRUE;
}
if (strlen($phone) > 1 && countDigits($phone) < 3) {
    $dodgy = TRUE;
}
if ($deleted == 1) {
    $dodgy = TRUE;
}
if ($first_name && $first_name == $last_name) {
    $dodgy = TRUE;
}
if ($user_name == $first_name . '_' . $last_name) {
    $dodgy = TRUE;
}
if (stristr($first_name . '_' . $last_name, 'XY')) {
    $dodgy = TRUE;
}
if (stristr($first_name . '_' . $last_name, 'XZ')) {
    $dodgy = TRUE;
}
if (stristr($first_name . '_' . $last_name, 'ZX')) {
    $dodgy = TRUE;
}

if (!ISSET($restcareid)) {
    $restcareid = $productId;
}

$id = $productId;
$querys = " SELECT title,supplier_id FROM " . _prefix("products")
        . " WHERE id =" . cleanthishere($restcareid) . "";
record_time($querys);
$planres = $db->sql_query($querys);
record_time($querys);
$dataplans = $db->sql_fetchrow($planres);
$facility_title = $dataplans['title'];
$supplierId = $dataplans['supplier_id']; // incase it is missing

if (!$facility_title) {
    $needs_search_facility = TRUE;
}


//echo "<small>userId = $userId  ,  supplierId = $supplierId  ,  restcareid = $restcareid</small>";

$id = $productId;
$querys = " SELECT title,quick_url FROM " . _prefix("products")
        . " WHERE id =" . cleanthishere($restcareid) . "";
record_time($querys);
$planres = $db->sql_query($querys);
record_time($querys);
$dataplans = $db->sql_fetchrow($planres);
$facility_title = $dataplans['title'];
$quick_url = $dataplans['quick_url'];


$qry = "select usersdata.id,usersdata.company,usersdata.last_name, usersdata.user_name , usersdata.email,usersdata.email_reviews_facility,usersdata.email_reviews_head_office,usersdata.agree_terms from " . _prefix('users') . ' AS usersdata,' . _prefix('pro_plans') . ' AS pro_plan  where usersdata.id=pro_plan.supplier_id AND pro_plan.supplier_id =' . cleanthishere($supplierId);
record_time($qry);

$result = $db->sql_query($qry);
record_time($qry);
$data = $db->sql_fetchrowset($result);

foreach ($data as $key => $record) {
    $data_head_office = $record;
}

$qry2 = "select email from " . _prefix('pro_extra_info') . ' where  pro_id=' . cleanthishere($restcareid);
record_time($qry2);
$result2 = $db->sql_query($qry2);
record_time($qry2);
$data2 = $db->sql_fetchrowset($result2);
foreach ($data2 as $key => $record) {
    $data_facility = $record;
}
//print_r($data_facility);


$qryDuration = "select id, title from " . _prefix('durations') . ' where  status=1 AND deleted=0';
record_time($qryDuration);
$resultDuration = $db->sql_query($qryDuration);
record_time($qryDuration);
$durationVisit = $db->sql_fetchrowset($resultDuration);

$qryPlace = "select id, title from " . _prefix('choose_place') . ' where  status=1 AND deleted=0';
record_time($qryPlace);
$resultPlace = $db->sql_query($qryPlace);
record_time($qryPlace);
$choosePlace = $db->sql_fetchrowset($resultPlace);


$qryKnowMe = "select id, title from " . _prefix('know_me') . ' where  status=1 AND deleted=0 ORDER BY order_by';
record_time($qryKnowMe);
$resultKnowMe = $db->sql_query($qryKnowMe);
$knowMe = $db->sql_fetchrowset($resultKnowMe);
record_time($qryKnowMe);

$qryProduct = "select id,title, description from " . _prefix('products') . ' where deleted=0 AND id=' . $productId . '';
record_time($qryProduct);
$resultProduct = $db->sql_query($qryProduct);
record_time($qryProduct);
$product_res = $db->sql_fetchrowset($resultProduct);

/* * *************** To feche nationality ******************* */
$qryoriginity = "select id, origin_name from  ad_originity ORDER BY `id` ASC";
record_time($qryoriginity);
$resultOriginity = $db->sql_query($qryoriginity);
record_time($qryoriginity);
$origin_res = $db->sql_fetchrowset($resultOriginity);

//prd($qryProduct);
if (isset($_POST['submit2']) && $_POST['submit2'] == 'submit') {
    if (isset($userId) && $overall_rating > 0) {        
        // CAPTCHA removed by Malcolm    
        if (isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response'])) {
            $secret = '6Ldc7iAUAAAAAJKYL3I8xHTHxQCgcTyNLl24NNey';
            //get verify response data
            $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret=' . $secret . '&response=' . $_POST['g-recaptcha-response']);
            $responseData = json_decode($verifyResponse);
            
            if ($responseData->success) {
                $errors = "";
                $queryRating = "SELECT  `quality_of_care`, `caring_staff`, `responsive_management`, `trips_outdoor_activities`,"
                        . " `indoor_entertainment`, `social_atmosphere`, `enjoyable_food`, `overall_rating` FROM " . _prefix("rating_score");
                record_time($queryRating);
                $resRating = $db->sql_query($queryRating);
                record_time($queryRating);
                $dataRating = $db->sql_fetchrowset($resRating);
                $QualityOfServiceWgt = $dataRating[0]['quality_of_care'];
                $cStaffWgt = $dataRating[0]['caring_staff'];
                $resManWgt = $dataRating[0]['responsive_management'];
                $tripWgt = $dataRating[0]['trips_outdoor_activities'];
                $indoorEntWgt = $dataRating[0]['indoor_entertainment'];
                $socialWgt = $dataRating[0]['social_atmosphere'];
                $foodWgt = $dataRating[0]['enjoyable_food'];
                $overAllRatingWgt = $dataRating[0]['overall_rating'];
                $TotalRating = 0;
                $weightSumation = $QualityOfServiceWgt + $cStaffWgt + $resManWgt + $tripWgt + $indoorEntWgt + $socialWgt + $foodWgt + $overAllRatingWgt;
               
                $TotalRating = (($QualityOfServiceWgt * $quality_of_care) + ($cStaffWgt * $caring_staff) + ($resManWgt * $responsive_management) + ($tripWgt * $trips_outdoor_activities) + ($indoorEntWgt * $indoor_entertainment) + ($socialWgt * $social_atmosphere) + ($foodWgt * $enjoyable_food) + ($overAllRatingWgt * $overall_rating));
                $Rating = ($TotalRating / ($weightSumation));
                $overallRating = round($Rating);

                if (isset($_SESSION['csId']) && !empty($_SESSION['csId'])) {
                    $customerId = $_SESSION['csId'];
                } else if (isset($_SESSION['userId']) && !empty($_SESSION['userId'])) {
                    $customerId = $_SESSION['userId'];
                } else {
                    $customerId = $supplierId;
                }

                $fields = array(
                    'cs_id' => $customerId,
                    'sp_id' => $supplierId,
                    'product_id' => $productId,
                    'title' => $title,
                    'know_me' => $knowMeId,
                    'visit_duration' => $visitDuration,
                    'last_visit' => $lastVisit,
                    'recommended' => $recommended,
                    'who_did_you_tell' => $who_did_you_tell,
                    'what_response' => $what_response,
                    'yes_no_planto' => $yes_no_planto,
                    'feedback' => trim($feedback),
                    'pros' => trim($pros),
                    'cons' => trim($cons),
                    'quality_of_care' => trim($quality_of_care),
                    'caring_staff' => trim($caring_staff),
                    'responsive_management' => trim($responsive_management),
                    'trips_outdoor_activities' => trim($trips_outdoor_activities),
                    'indoor_entertainment' => trim($indoor_entertainment),
                    'social_atmosphere' => trim($social_atmosphere),
                    'enjoyable_food' => trim($enjoyable_food),
                    'overall_rating' => trim($overall_rating),
                    'anonymous' => trim($anonymous),
                    'avg_rating' => trim($overallRating),
                    'how_did_you_hear' => trim($how_did_you_hear),
                    'nationality' => trim($nationality),
                    'why_choose' => trim($why_choose),
                    'initials_staff' => trim($initials_staff),
                    'created' => date('Y-m-d H:i:s', time())
                );


                if (!is_numeric($productId) || $productId < 1 || $supplierId < 1 || !is_numeric($supplierId) || $dodgy) {                    
                    redirect_to(HOME_PATH . $quick_url . '#reviews');
                    exit;
                }
                
                $insert_result = $db->insert(_prefix('feedbacks'), $fields);
                $row_id = mysqli_insert_id($db->db_connect_id);

                if ($insert_result) {
                    //Update as agreed to terms and cons    for //echo "<h1>$userId</h1>";
                    $updateagreed = "UPDATE " . _prefix("users") . " SET agree_terms ='1' WHERE id= '$userId'";
                    $db->sql_query($updateagreed);
                    /*                     * *** approve reviews after 24 hrs  *** */
                    $querys = "SELECT status, created FROM " . _prefix("feedbacks") . " WHERE product_id =" . $productId . " AND id=" . $row_id . " AND is_manual = 0 AND deleted = 0 order by created desc limit 0,1";
                    record_time($querys);
                    $mod_review = $db->sql_query($querys);
                    record_time($querys);
                    $dataplans = $db->sql_fetchrow($mod_review);
                    $status = $dataplans['status'];
                    $created = date('Y-m-d H:i:s', strtotime($dataplans['created']) + 1 * 10 * 10);
                    $current_time = date('Y-m-d H:i:s', time());

                    if ($current_time >= $created) {
                        $status = array(
                            'status' => '1',
                        );
                        $where = "where product_id =" . $productId . " AND id=" . $row_id . " AND is_manual = 0 AND deleted = 0";
                        $update_result = $db->update(_prefix('feedbacks'), $status, $where);
                    }
                    record_time("Send emails");

                    $to = 'nigelmatthews@ihug.co.nz';                    
                    $approval_url = HOME_PATH . 'review_status.php?id=' . $row_id . '&status=1&pid=' . $productId . '';
                    $subject = 'For Approval';
                    $message = 'Facility Name : ' . $facility_title . "<br>" . '
                                Posted on : ' . date('Y/m/d h:i:s', time()) . "<br>$facility_email<br><br>TITLE : $title<br><br>" . '
                                The Positives : ' . $pros . "<br><br>" . '
                                The Negatives : ' . $cons . "<br><br>" . '
                                Suggestions or General Comment : ' . $feedback . "<br><br>" . '
                                To approve the review <a href="' . $approval_url . '" target="_blank">Click Here</a>' . "<br><br>" . ' 
                                <p><strong>Please Note : This review will not appear online until it has been approved for release by the Aged Advisor Approval Team. This can take up to 24 hours.</strong></p>';
                    $headers = 'From: webmaster@example.com' . "\r\n" .
                            'Reply-To: webmaster@example.com' . "\r\n" .
                            'X-Mailer: PHP/' . phpversion();

                    sendmail($to, $subject, $message, '', $headers);
                    

                    $facility_email = $data_facility['email'];

                    $message = 'Facility Name : ' . $facility_title . "<br>" . '
                                Posted on : ' . date('Y/m/d h:i:s', time()) . "<br>$facility_email<br><br>TITLE : $title<br><br>" .
                                '<p><strong>Please Note : This review will not appear online until it has been approved for release by the Aged Advisor Approval Team. This can take up to 24 hours.</strong></p>
                                The Positives : ' . $pros . "<br><br>" . '
                                The Negatives : ' . $cons . "<br><br>" . '
                                Suggestions or General Comment : ' . $feedback . "<br><br>" . '
                                To see the full review (including ratings), please login to Aged Advisor' . "<br>" . '
                                http://agedadvisor.nz/supplier/login' . "<br><br>" . 'Once logged in, you can also post a reply (NOTE: Upgraded plans only).';
                                // feedback from aged advisor client email

                    $email_subject = 'Aged Advisor - Thanks, your review has now been received and is pending approval ...';
                    $bcc = 'nigelmatthews@ihug.co.nz'; //$_SESSION['csEmail'];
                    

                    if ($data_head_office['agree_terms']) {
                        $are_they_agree = "Even though they have agreed to terms and conditions.<br>";
                    } else {
                        $are_they_agree = 'And have <b>NOT</b> yet agreed to terms and conditions.<br>';
                    }

                    if ($data_head_office['email_reviews_facility'] && $data_head_office['agree_terms'] && $facility_email) {
                        sendmail($facility_email, $email_subject, $message, '', $bcc);
                        $xmessage = "This email was also sent to Facility $facility_email <br>";
                    } else {
                        $xmessage = "NOT sent to Facility $facility_email <br>";
                        $add_subject = "!!! NO EMAIL FOR TO: !!! - ";
                    }

                    $head_office_email = $data_head_office['email'];
                } else {
                    $head_office_email = '';
                }
                if ($data_head_office['email_reviews_head_office'] && $data_head_office['agree_terms'] && $head_office_email) {
                    sendmail($head_office_email, $email_subject, $xmessage . $message, '', $bcc);
                    $xmessage = $xmessage . "This email was also sent to Head Office $head_office_email <br><br>";
                } else {
                    $xmessage = $xmessage . "NOT sent to Head Office $head_office_email <br>" . $are_they_agree . "<br>";
                }

                // Now send a copy to Nigel
                $admin_email = 'nigelmatthews@ihug.co.nz'; // testing 
                //$admin_email='office@homeofpoi.com';// testing 

                sendmail($admin_email, $add_subject . "ADMIN COPY " . $email_subject, $xmessage . $message, '', $bcc);

                $msg = common_message_supplier(1, constant('CLIENT_FEEDBACK'));

                // Redirect to the facility page#reviews
                // IF EMAIL FOR REVIEWER SEND THEM AN EMAIL THANKS AND TELL FRIENDS
                if (ISSET($_SESSION['csEmail']) && stristr($_SESSION['csEmail'], '@')) {// Yes send an email
                    $email_subject = 'Aged Advisor - Thanks, your review has now been posted ...';
                    $message = "Hi " . $_SESSION['csUserFullName'] . "<br><br>";
                    $message .= "<b>Thank you for taking the time to place a review</b>. This will help others looking for a provider to make more of an informed decision.<br><br>
                                Please <b>forward this on to your family and friends</b> so they too can share their thoughts and experiences.<br><br>
                                <strong>Please Note : This review will not appear online until it has been approved for release by the Aged Advisor Approval Team. This may take up to 24 hours.</strong><br><br>
                                ---------------------------------------------------------------------------------------<br><br>";
                    $message .= 'Facility Name : ' . $facility_title . "<br>" . '
                                Posted on : ' . date('Y/m/d h:i:s', time()) . "<br><br>TITLE : $title<br><br>" . '
                                The Positives : ' . $pros . "<br><br>" . '
                                The Negatives : ' . $cons . "<br><br>" . '
                                Suggestions or General Comment : ' . $feedback . "<br><br>";
                    $message .= "To see the full review (including ratings) please click here " . HOME_PATH . $quick_url . '#reviews' . "<br><br>
                                ---------------------------------------------------------------------------------------<br><br>";
                    $message .= "We would love for you to post a story or share an experience about Retirement Life. Please login and send your story through to us.<br><br>
                                Your username is : " . $_SESSION['csUserName'] . " ";
                    if ($anonymous) {
                        $message .= "[<b>Withheld at your request</b>]";
                    }
                    $message .= "<br><br>
                                If you need to retrieve your password then you can click here http://www.agedadvisor.nz/forgotPassword and simply enter your email address.<br><br>
                                Please note, posting a review means that you agree to our sites terms and conditions. These can be found at http://www.agedadvisor.nz/terms_and_conditions<br><br>";
                    if ($overall_rating < 3) {
                        $message .= "---------------------------------------------------------------------------------------<br><br>Note: It would appear that the facility you have reviewed could have done better in one or more areas.<br><br>
                                    If you've approached the facility directly to sort out the situation and you're still not happy OR you prefer not to contact the facility (for whatever reason) then we would like to make sure that you're aware of the following options that are also open to you;<br><br>
                                    <strong>Making an anonymous complaint...</strong><br>
                                    Advising HealthCert of dishonest / concerning situations in the health sector.<br>
                                    Freephone 0800 424 888 (Health Integrity Line) to supply anonymous information.<br>
                                    http://www.health.govt.nz/about-ministry/contact-us<br><br>
                                    <strong>Making a formal complaint...</strong><br>
                                    Health & Disability Commissioner<br>
                                    Freephone 0800 11 22 33<br>
                                    http://www.hdc.org.nz/complaints/making-a-complaint/online-complaint-form<br><br>
                                    <strong>OR...If you require the help of an advocate...</strong><br>
                                    Freephone 0800 555 050 or local phone numbers are available from<br>
                                    http://advocacy.hdc.org.nz/find-an-advocate<br><br>
                                    <strong>For more information on how the HDC's Compliant Process works...</strong><br>
                                    http://www.hdc.org.nz/media/250376/hdc%20guide%20for%20complainants.pdf<br><br>
                                    Note: During 2014, 104 complaints that related to 83 certified aged residential care facilities were recorded. Of these complaints, HealthCERT received 37 (38%) directly, 19 (20%) through the Health and Disability Commissioner’s Office and the remaining 48 (42%) through district health board Health of Older People Portfolio Managers.<br><br>
                                    ------------------------------------<br><br>
                                    <strong>Additional options to consider:</strong><br>
                                    Community Law Services offer free legal advice.<br>
                                    http://communitylaw.org.nz/<br><br>
                                    Age Concern offices around NZ offer free advice in addressing Elder Abuse & Neglect.<br>
                                    http://www.ageconcern.org.nz/<br><br>
                                    TVNZ's FairGo Programme - 09 916 7288<br>
                                    http://tvnz.co.nz/fair-go";
                        $message .= "<br><br>";
                    }

                    $message .= "Regards,<br><br>
                                Reviews / Accounts Team<br>
                                Aged Advisor (NZ) Ltd<br>
                                <br>
                                PS. Know someone that would like to place a review but doesn't have access to a computer?<br><a target=\"_blank\" href=\"http://www.agedadvisor.nz/images/agedadvisor_Review_Sheet_A4_PRINT.pdf\">Download a printable FREEPOST form here</a>";

                    $bcc = 'nigelmatthews@ihug.co.nz';                    

                    sendmail($_SESSION['csEmail'], $email_subject, $message, '', $bcc);
                }// End of SENDING THE REVIEWER THE EMAIL

                record_time("Send emails");

                if (1 == 0 && ($_SERVER['REMOTE_ADDR'] == "203.97.201.204" || $_SERVER['REMOTE_ADDR'] == "125.236.218.32") && ISSET($GLOBALS['process_time_start'])) {// Its Malcolm
                    echo "<hr><h4>Only viewable at Malcolm's Home to show Process Times, otherwise goes onto the facility page.</h4>";                    
                    foreach ($GLOBALS['process_time_start'] as $name => $time) {
                        $start = 0;
                        $end = 0;
                        if (ISSET($time['start'])) {
                            $start = $time['start'];
                        }
                        if (ISSET($time['end'])) {
                            $end = $time['end'];
                        }
                        echo "<b>$name = </b>" . number_format($end - $start, 4) . " seconds.<br>";
                    }// end for each
                    exit;
                }

                if (!$new_password) {// we only logged them in via the email address so now log them out again
                    $_SESSION = array();
                    //        ECHO 'LOG THENM OUT <a href="'.HOME_PATH .$quick_url.'#reviews">Continue</a>';exit;      
                    //session_destroy();
                }

                //            redirect_to(HOME_PATH . 'reviews');
                redirect_to(HOME_PATH . $quick_url . '#reviews');
            } else {
                $errors = 'Robot verification failed, please try again.';
            }
        } else {
            $errors = 'Please click on the reCAPTCHA box.';
        }
    } else {
        //$msg = 'Please login to add your review.';//
        //$_SESSION['msg'] = $msg;
        ///if(!isset($userId)){$error="You must enter at least an Email or a Phone number<br>";}
        //if($overall_rating<0.5){$error="You must select an Overall Rating below<br>";}
        //redirect_to(HOME_PATH . 'login');
    }
}
?>
<script type="text/javascript">
    $(document).ready(function()
    {
        var limit = 3;
        $('.single-checkbox').on('change', function(evt) {
            if ($(this).siblings(':checked').length >= limit) {
                this.checked = false;
                alert("maximum 3 selection allowed");
            }
        });
    });
</script>

<?php if($needs_search_facility){ ?>
<script type="text/javascript">
    $('#search').validate();
    $(function() {
        $('#cityLable').focus(function() {
            $(this).val('');
            $("#city,#cityid,#suburbid,#restcareid,#supplierId").val('');
        })
        $('#cityLable').focusout(function() {
            //restcareid
            var restcare_id = $("#restcareid").val();
            if (restcare_id == '' ) {
                $(this).val('');
                $("#city,#cityid,#suburbid,#restcareid,#supplierId").val('');
            }
        })
        
        var offset = $('#cityLable').offset();
        var cf = $(document).find('#cityLable');
        if (cf.length > 0){
            var autoL = offset.left + 'px';
        }
        var path = '<?php echo HOME_PATH; ?>';
        $("#cityLable").autocomplete({
            source: function(request, response) {
                $.ajax({
                    minLength: 3,
                    method: "post",
                    url: path + 'ajaxFront.php/' + new Date().getTime(),
                    data: {
                        'action': 'getSupplierProList', 'maxRows': 12,
                        'name': request.term
                    },
                    success: function(result) {
                        var data = $.parseJSON(result);
                        if (data === '') {
                            return {
                            label: '',
                                    value: '',
                                    // title = location,restcare=supplier product                                                       value: item.title + ', '+item.suburb+ ', '+item.restCare,
                                    cityId: '',
                                    suburbId: '',
                                    restCareId: ''
                            };
                        }
                        response($.map(data, function(item) {
                            var restCare = (item.restCare === null) ? '' : item.restCare;
                            var suburb = (item.suburb === null) ? '' : ', ' + item.suburb;
                            var location = (item.cty === null) ? '' : ', ' + item.cty;
                            var servType = '';
                            if (item.servType == "Aged Care"){
                                servType = '[AC]';
                            } else if (item.servType == "Retirement Village"){
                                servType = '[RV]';
                            } else{
                                servType = '[HS]';
                            }
                            return {
                                label: restCare + '' + suburb + '' + location + '' + servType,
                                value: restCare + '' + suburb + '' + location + '' + servType,
                                // title = location,restcare=supplier product                                                       value: item.title + ', '+item.suburb+ ', '+item.restCare,
                                cityId: item.id,
                                suburbId: item.suburbId,
                                restCareId: item.restCareId,
                                supplierId: item.supplierId
                            };
                        }));
                    },
                    error: function(e) {
                        alert('inside error');
                    }
                });
            },
            select: function(event, ui) {
                $this = $(this);
                setTimeout(function() {
                    $('#cityid').val(ui.item.cityId);
                    $('#suburbid').val(ui.item.suburbId);
                    $('#restcareid').val(ui.item.restCareId);
                    $('#supplierId').val(ui.item.supplierId);
                    $('#cityLable').val(ui.item.label);
                    $('#facilityType').focus();
                    $this.blur();
                }, 1);
                //return false;
            },
            open: function(event, ui) {
                if ($('#guidlines').css('display') == 'none'){
                    //var autoT =  628 + 'px';
                    var autoT = 510 + 'px';
                } else{
                    var autoT = 725 + 'px';
                }
                $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
                $('ul.ui-widget-content').css({"z-index": "9999", "display": "block", "top": autoT, "left": autoL});
            },
            close: function() {
                $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
            }
        });
    });
</script>

<?php } ?>

<?php if (isset($errors)) { ?>
    <div class="container">
        <div class="col-md-12" style="margin-top: 10px;">
            <div class="alert alert-danger text-center"><a href="#" class="close" data-dismiss="alert">&times;</a><?php echo  $errors ?></div>
        </div>
    </div>
<?php } ?>

    <script src='https://www.google.com/recaptcha/api.js'></script>
    <!--MAIN BODY CODE STARTS-->
    <div class="container">
        <div class="row articles_container">
            <?php if ($showmsg) { ?>
                <div class="row">
                    <div class="col-md-12" style="margin-top: 20px;">
                        <div class="alert alert-success text-center" style="margin-bottom:0px;"><a href="#" class="close" data-dismiss="alert">&#215</a><strong>Message!&nbsp;&nbsp;</strong>Thank you for your response.</div>
                    </div>
                </div>
            <?php } ?>

            <script type="text/javascript">
                // $('#search').validate();                
                $(function() {
                    $('#cityLable1').focus(function() {
                        $(this).val('');
                    });
                    $('#cityLable1').focusout(function() {
                        var restcare_id = $("#restcareid1").val();
                        if (restcare_id && restcare_id != 'undefined') {
                            //window.location.href = "https://agedadvisor.nz/rating-supplier?resp=YES&enq="+dataeid+"&prId="+datapid;
                        } else{
                            $(this).val('');
                            $("#restcareid1").val('');
                        }
                    });
                    var offset = $('#cityLable1').offset();
                    var cf = $(document).find('#cityLable1');
                    if (cf.length > 0){
                        var autoL = offset.left + 'px';
                    }
                    var path = '<?php echo HOME_PATH; ?>';
                    $("#cityLable1").autocomplete({
                        source: function(request, response) {
                            $.ajax({
                                minLength: 3,
                                    method: "post",
                                    url: path + 'ajaxFront.php/' + new Date().getTime(),
                                    data: {
                                    'action': 'getSupplierProList',
                                            'maxRows': 12,
                                            'name': request.term
                                    },
                                    success: function(result) {
                                        var data = $.parseJSON(result);
                                        if (data === '') {
                                            return {
                                            label: '',
                                                    value: '',
                                                    // title = location,restcare=supplier product                                                       value: item.title + ', '+item.suburb+ ', '+item.restCare,
                                                    //  cityId: '',
                                                    //suburbId: '',
                                                    restCareId: ''
                                            };
                                        }
                                        response($.map(data, function(item) {
                                            var restCare = (item.restCare === null) ? '' : item.restCare;
                                            var suburb = (item.suburb === null) ? '' : ', ' + item.suburb;
                                            var location = (item.cty === null) ? '' : ', ' + item.cty;
                                            var servType = '';
                                            if (item.servType == "Aged Care"){
                                                servType = '[AC]';
                                            } else if (item.servType == "Retirement Village"){
                                                servType = '[RV]';
                                            } else{
                                                servType = '[HS]';
                                            }
                                            return {
                                                label: restCare + '' + suburb + '' + location + '' + servType,
                                                value: restCare + '' + suburb + '' + location + '' + servType,
                                                // title = location,restcare=supplier product                                                       value: item.title + ', '+item.suburb+ ', '+item.restCare,
                                                cityId: item.id,
                                                suburbId: item.suburbId,
                                                restCareId: item.restCareId,
                                                supplierId: item.supplierId
                                            };
                                        }));
                                    },
                                    error: function(e) {
                                        alert('inside error');
                                    }
                            });
                        },
                        select: function(event, ui) {
                            $this = $(this);
                            setTimeout(function() {
                                $('#restcareid1').val(ui.item.restCareId);
                                var datapid = btoa(ui.item.restCareId);
                                var dataeid = $("#getenqid").val();
                                if (ui.item.restCareId && ui.item.restCareId != 'undefined') {
                                    window.location.href = "https://agedadvisor.nz/rating-supplier?resp=YES&enq=" + dataeid + "&prId=" + datapid;
                                }
                            }, 1000)
                        },
                        open: function(event, ui) {
                            if ($('#guidlines').css('display') == 'none'){
                                //var autoT =  628 + 'px';
                                var autoT = 360 + 'px';
                            } else{
                                var autoT = 725 + 'px';
                            }
                            //  $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
                            //  $('ul.ui-widget-content').css({"z-index": "9999", "display": "block", "top": autoT, "left": autoL});
                        },
                        close: function() {
                            $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
                        }
                    });
                });
            </script> 


            <?php $d=$_GET['prId']; $k=$_GET['enq'];  if($d=='NULL' and $k!='NULL'){   ?>
                <?php 
                if (isset($_GET['enq'])) {
                    $get_enqid = $_GET["enq"];
                } 
                ?>
                <br/>
                <div id="srh"><h2 style="color: #e67e22;">Search and select Facility you have chosen </h2></div>
                <div>
                    <input type="text" class="form-control input-lg ui-autocomplete-input ui-widget required" required autocomplete="off" name="cityLable1" id="cityLable1" onfocus="this.value = '';"  placeholder="Start typing facility name here and then select from displayed list">
                </div>
                <br/>
                <input type="hidden" value="<?php echo $get_enqid?>" id="getenqid">
                <input type="hidden" value="" id="restcareid1">
            <?php }else{ ?>


                <style>
                    .col-md-12.col-sm-12.col-xs-12.padding_none {
                        top: 0px;
                    } 
                </style>

                <h1 style="padding-left:0px; margin-bottom: 5px;" class="theme_color_inner">Write a Review to Rate/Vote on a Facility</h1>
                <div style="margin-top:0;" class="col-md-12 col-sm-12 col-xs-12 registration_container">
                    <form action="<?php echo HOME_PATH .'rating-supplier';?>" id='feedbackForm' method="POST" name='feedbackForm'>              
                        <!--<h4>Simply add your review below using one of the following methods.</h4>-->
                        <?php if($needs_login){ ?>
                    <p><!--If logging in using your <a href="login-twitter.php?redirect=review">Twitter</a> or <a href="<?php echo $loginUrl?>">Facebook</a> details, please do so now before writing your review (otherwise any comments made will not be saved). NOTE: We never display your photo or contact details.<br>-->
                            If you wish to, you can log in using  <a href="login-twitter.php"><img style="width: 141px;height: 29px;" src="<?php echo HOME_PATH; ?>images/sign_in_with_twitter.png" alt=""></a>&nbsp;<a href="<?php echo  $loginUrl ?>"><img style="width: 141px;height: 29px;" src="<?php echo HOME_PATH; ?>images/sign_in_with_facebook.png" alt=""></a></p>
                        <!--    <li>If you have an account with us already, please <a href="login">login here</a> before writing your review.</li>
                            <li>OR we will create an account for you when you post your review.  -->

                        <?php } else { ?>
                        <p style="height:52px;">You are posting this review using your existing login.</p>
                        <?php  } ?>
                        <div id="show" class="show-guid">First time placing a review? <input type="checkbox" id="checkguidlines" class="checkguildlines1"></div>
                        <!--end of  code show and div on checkbox--> 
                        <div id="guidlines" class="our-guidlines">   
                            <p><strong>Our Posting Guidelines:</strong> We believe you should have the freedom to post comments or reviews about experiences that you've had, so that others can hear about the good and the bad... so when you post your thoughts, please keep to the following;
                            <ul>
                                <li>Don't name individuals. Keep it generic. eg. &quot;the resident&quot;, &quot;the family member&quot;, &quot;the staff member&quot;, &quot;the manager&quot;.</li>
                                <li>Stick to the facts. If you've had a great experience, then tell us. If you've had the opposite - then let us know also.</li>
                                <li>Avoid using words such as 'never', 'always'. If something happened several times then say that.  If it happened twice - then it happened twice, and once is only once.</li>
                                <li>This is a family-based site so do not post any material that is defamatory, illegal, offensive, threatening, discriminatory, blasphemous or contains bad language.</li>
                            </ul>
                            </p>
                        </div> 

                        <!--************************code for show message on based on days***********-->
                        <?php
                        if (isset($_SESSION['csId']) && !empty($_SESSION['csId'])) {
                            $customerId = $_SESSION['csId'];
                        } else if (isset($_SESSION['userId']) && !empty($_SESSION['userId'])) {
                            $customerId = $_SESSION['userId'];
                        } else {
                            $customerId = $supplierId;
                        }

                        if (isset($id) && !empty($id)) {
                            $cur_date = date("Y-m-d");
                            $feedback_count_day = "SELECT *  from ad_feedbacks where `cs_id`={$customerId} and `product_id` ={$id} and `deleted`='0' and `status`='1' order by created desc limit 1 ";
                            $result_feedback = $db->sql_query($feedback_count_day);
                            $datas = $db->sql_fetchrowset($result_feedback);
                            if ($db->sql_numrows($result_feedback) > 0) {
                                foreach ($datas as $data_result) {
                                    $last_date_time = $data_result['created'];
                                    $feedlast_date = date("Y-m-d", strtotime($last_date_time));
                                    $date1 = new DateTime($feedlast_date);
                                    $date2 = new DateTime($cur_date);
                                    $interval = $date1->diff($date2);
                                    $interval_days = $interval->days;
                                    if ($interval_days <= 90) {
                                        echo '<div class="less_ninty alert alert-success text-center">
                                            <a href="#" class="close" data-dismiss="alert">×</a>
                                           Thanks for the review. Unfortunately, it appears you have already placed a review on this facility within the last 90 days. We require that any followup reviews have at least a 6 month gap from the previous time, to keep it balanced. You may post a review after a minimum of 90 days if the star ratings you gave last time have changed. Please come back to post your review after the 90 day minimum timeframe is up. Regards, Aged Advisor Reviews Team. 
                                          </div>';
                                    }
                                    if ($interval_days > 90 and $interval_days <= 180) {
                                        echo "<div class='more_ninty alert alert-success text-center'>
                                        <a href='#' class='close' data-dismiss='alert'>×</a>
                                      Welcome back. It appears that you placed a review on this facility within the last 3 - 6 months. If there's been any changes then your review will be posted.
                                       </div>";
                                    }
                                }
                            }
                        }
                        ?>

                        <?php if (empty($id)) {  ?>
                            <div id="ajax-data">

                            </div>
                        <?php } ?>
                            <!--*********************end of code for show  message on based on days*****-->    
                            <div class="col business_name" style='margin-bottom: 20px;margin-top:0px;'>
                                <div class='rating_error' style="color: #ff0000;font-family: 'PT Sans';font-size: 12px;"></div></label>

                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 padding_none" style="margin-top:0%;">
                                <hr>
                                <?php if(!$needs_search_facility){?>
                                    <input type="hidden" name="restcareid" id="restcareid" value="<?php echo  $restcareid ?>"/>
                                    <input type="hidden" name="supplierId" id="supplierId" value="<?php echo  $supplierId ?>"/>

                                    <h2>Facility Name : <strong><?php echo $product_res[0]['title'] ?></strong></h2>
                                    <h4>Owner Name : <?php echo $data_head_office['company'] . ' ' . $data['last_name']; ?></h4>
                                <?php }else{ ?>
                                    <h4 class="comment_h4">Facility Name &nbsp;<span  class="redCol small" style="color:red;">*</span></h4>
                                    <!--  <div class="form-group">
                                                            <select class="form-control select_option_1 input_fild pull-left supplier required" name="supplierId" style="margin:0px 0 8px;"><?php // echo supplierList($supplier_id); ?></select>
                                                        </div> -->
                                    <div class="form-groups" style="">
                                        <!--<label for="name" class="label_text ">City, suburb, retirement village or rest care</label>-->
                                        <input type="hidden" name="cityid" id="cityid">
                                        <input type="hidden" name="suburbid" id="suburbid">
                                        <input type="hidden" name="restcareid" id="restcareid"/>
                                        <input type="hidden" name="supplierId" id="supplierId"/>
                                        <div class="error"></div>

                                        <!--for display facility name--> 
                                        <?php
                                            if (isset($_REQUEST['action']) == "rat") {
                                                $facility_name = mysqli_query($db->db_connect_id,"select title from  ad_products where id='" . $_REQUEST['prId'] . "' AND supplier_id='" . $_REQUEST['spId'] . "'");
                                                while ($facility_name1 = mysqli_fetch_array($facility_name)) {
                                                    $facility_name2 = $facility_name1['title'];
                                                }
                                        ?>
                                            <!--<input type="text" class="form-control input-lg ui-autocomplete-input ui-widget required" required autocomplete="off" name="cityLable" id="cityLable" onfocus="this.value = '';"  placeholder="Start typing facility name here and then select from displayed list">-->
                                                <input type="text" value="<?php echo $facility_name2; ?>" class="form-control" name="cityLable" id="cityLable" disabled>
                                                <!-- end  of code show facility name-->      
                                        <?php } else {  ?>
                                                <input type="text" class="form-control input-lg ui-autocomplete-input ui-widget required" required autocomplete="off" name="cityLable" id="cityLable" onfocus="this.value = '';"  placeholder="Start typing facility name here and then select from displayed list">
                                        <?php } ?>
                                    </div>

                                <?php } ?>               
                                <hr>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <h4 class="comment_h4">Feedback Title &nbsp;<span  class="redCol small" style="color:red;">*</span></h4>
                                        <div class="form-group">
                                            <input type="text" placeholder="Write feedback title ..." class="form-control input_fild_1 required" name="title" maxlength="50" id="title"  value="<?php if(ISSET($_POST['title'])){echo cleanthishere($_POST['title']);}?>"/>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <h4 class="comment_h4">The Positives&nbsp;</h4>
                                        <div class="form-group">
                                            <textarea placeholder="Post your feedback..." class="form-control post_mail" name="pros" minlength="3" maxlength="500" id="pros"  ><?php if(ISSET($_POST['pros'])){echo cleanthishere($_POST['pros']);}?></textarea>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <h4 class="comment_h4">The Negatives&nbsp;</h4>
                                        <div class="form-group">
                                            <textarea placeholder="Post your feedback..." class="form-control post_mail" name="cons" minlength="3" maxlength="500" id="cons"  ><?php if(ISSET($_POST['cons'])){echo cleanthishere($_POST['cons']);}?></textarea>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <h4 class="comment_h4">Suggestions to Management or General Comment&nbsp;<span  class="redCol small" style="color:red;">*</span></h4>
                                        <div class="form-group">
                                            <textarea placeholder="Any comments or suggestions that you believe management should hear..." class="form-control post_mail" name="feedback" maxlength="500"  > <?php if(ISSET($_POST['feedback'])){echo cleanthishere($_POST['feedback']);}?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12">
                                <h4 class="comment_h4" style="margin-bottom: 0px;margin-top: 20px;">Quality of Care</h4>
                                <input type="number" class="rating" min=0 max=5 step=1  data-size="xs" name='quality_of_care' id='quality_of_care' value=' <?php if(ISSET($_POST['quality_of_care'])){echo cleanthishere($_POST['quality_of_care']);}?>'>
                                       <h4 class="comment_h4" style="margin-bottom: 0px;margin-top: 20px;">Knowledgeable / Caring Staff</h4>
                                       <input type="number" class="rating" min=0 max=5 step=1 data-size="xs"  name='caring_staff' id='caring_staff' value=' <?php if(ISSET($_POST['caring_staff'])){echo cleanthishere($_POST['caring_staff']);}?>'>
                                       <h4 class="comment_h4" style="margin-bottom: 0px;margin-top: 20px;">Responsive Management</h4>
                                    <input type="number" class="rating" min=0 max=5 step=1 data-size="xs" name='responsive_management' id='responsive_management' value=' <?php if(ISSET($_POST['responsive_management'])){echo cleanthishere($_POST['responsive_management']);}?>'></td>
                                <h4 class="comment_h4" style="margin-bottom: 0px;margin-top: 20px;">Trips/Outdoor Activities</h4>
                                <input type="number" class="rating" min=0 max=5 step=1 data-size="xs"  name='trips_outdoor_activities' id='trips_outdoor_activities' value=' <?php if(ISSET($_POST['trips_outdoor_activities'])){echo cleanthishere($_POST['trips_outdoor_activities']);}?>'>
                            </div>
                            <div class="col-sm-6 col-xs-12">
                                <h4 class="comment_h4" style="margin-bottom: 0px;margin-top: 20px;">Indoor Entertainment</h4>                                                   
                                <input type="number" class="rating" min=0 max=5 step=1 data-size="xs"  name='indoor_entertainment' id='indoor_entertainment' value=' <?php if(ISSET($_POST['indoor_entertainment'])){echo cleanthishere($_POST['indoor_entertainment']);}?>'>
                                <h4 class="comment_h4" style="margin-bottom: 0px;margin-top: 20px;">Social Atmosphere</h4>
                                <input type="number" class="rating" min=0 max=5 step=1 data-size="xs"  name='social_atmosphere' id='social_atmosphere' value=' <?php if(ISSET($_POST['social_atmosphere'])){echo cleanthishere($_POST['social_atmosphere']);}?>'>
                                       <h4 class="comment_h4" style="margin-bottom: 0px;margin-top: 20px;">Enjoyable Food</h4>
                                <input type="number" class="rating" min=0 max=5 step=1 data-size="xs"  name='enjoyable_food' id='enjoyable_food' value=' <?php if(ISSET($_POST['enjoyable_food'])){echo cleanthishere($_POST['enjoyable_food']);}?>'>
                                       <a name="overall"></a><h4 class="comment_h4" style="margin-bottom: 0px;margin-top: 20px;">OVERALL RATING (* Required)</h4>
                                <input type="number" class="rating" min=0 max=5 step=1 data-size="xs"  name='overall_rating' id='overall_rating' value=' <?php if(ISSET($_POST['overall_rating'])){echo cleanthishere($_POST['overall_rating']);}?>' required>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 panel panel-info" id="raised_issue" style=" margin-top:2%;display: none">
                                It would appear that this facility could have done better in one or more areas. Please help us understand if the facility has had an opportunity to address your concern.<br> 
                                <span>Did you raise this with anyone?</span><br>
                                <input id="clickyes" type="radio" name="yes_no_planto" style="width:auto;" value="yes"  <?php  if(ISSET($_POST['yes_no_planto']) && $_POST['yes_no_planto']=='yes'){echo 'checked';}?>><label for="clickyes"  style="font-weight:normal;" >&nbsp;Yes</label><br>
                                <input id="clickno" type="radio" name="yes_no_planto" style="width:auto;" value="no"  <?php  if(ISSET($_POST['yes_no_planto']) && $_POST['yes_no_planto']=='no'){echo 'checked';}?>><label for="clickno"  style="font-weight:normal;" >&nbsp;No</label><br>
                                <input id="clickplanto" type="radio" name="yes_no_planto" style="width:auto;" value="plan to"  <?php  if(ISSET($_POST['yes_no_planto']) && $_POST['yes_no_planto']=='plan to'){echo 'checked';}?>><label for="clickplanto"  style="font-weight:normal;" >&nbsp;Plan to</label><br>
                                <div id="yes_raised_issue" style=" margin-top:2%;display: none">Who did your raise this with? ie RN, Clinical or Facility Manager, Facility Owner, Advocate, DHB rep etc.
                                    <div class="form-group">
                                        <input type="text" class="form-control input_fild" id="who_did_you_tell" name="who_did_you_tell" placeholder="Who did you raise this with?" value=" <?php  if(ISSET($_POST['who_did_you_tell'])){echo cleanthishere($_POST['who_did_you_tell']);}else{echo "";}?>">
                                    </div>
                                    Did you get a response?
                                    <div class="form-group">
                                        <textarea placeholder="What was the response (if any)?" class="form-control post_mail" name="what_response" minlength="0" maxlength="500" id="what_response"  ><?php if(ISSET($_POST['what_response'])){echo cleanthishere($_POST['what_response']);}?></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12" style=" margin-top:2%;">
                                <h4 class="comment_h4">Would you recommend this facility to a friend?</h4>
                                <div class="form-group radio-new"  style="margin-left: 25px;">
                                    <input id="yes" type="radio" name="recommended" value="1"<?php if(ISSET($_POST['recommended']) && $_POST['recommended']=='1'){echo " checked";}?>><label for="yes"  style="font-weight:normal;" >&nbsp;Yes  </label><br> <input id="no" type="radio" name="recommended" value="0"<?php if(ISSET($_POST['recommended']) && $_POST['recommended']=='0'){echo " checked";}?>><label for="no"   style="font-weight:normal;" >&nbsp;No </label>
                                    <br> <input id="not_sure" type="radio" name="recommended" value="2"<?php if(ISSET($_POST['recommended']) && $_POST['recommended']=='2'){echo " checked";}?>><label for="not_sure"   style="font-weight:normal;" >&nbsp;Not sure</label>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <h4 class="comment_h4" style="margin-top: 33px;">How do you know about this place?</h4>

                                <div class="form-group radio-new"  style="margin-left: 25px;">
                                    <?php foreach ($knowMe as $key => $record) { ?>
                                        <input type="radio" name="knowMeId" id="<?php echo 'visits-' . $record['id']; ?>" value="<?php echo $record['id']; ?>"<?php if(ISSET($_POST['knowMeId']) && $_POST['knowMeId']==$record['id']){echo " checked";}?>><label style="font-weight:normal;" for="<?php echo 'visits-' . $record['id']; ?>"><?php echo '&nbsp;' . $record['title']; ?></label></br>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <h4 class="comment_h4">When did you last live at / visit this facility?</h4>
                                <div class="form-group radio-new"  style="margin-left: 25px;">
                                    <input id="12-months" type="radio" name="lastVisit" value="1"<?php if(ISSET($_POST['lastVisit']) && $_POST['lastVisit']=='1'){echo " checked";}?>><label for="12-months"  style="font-weight:normal;" >&nbsp;Within the last 12 months</label><br/>
                                    <input id="1-3years" type="radio" name="lastVisit" value="2"<?php if(ISSET($_POST['lastVisit']) && $_POST['lastVisit']=='2'){echo " checked";}?>><label for="1-3years"   style="font-weight:normal;" >&nbsp;1 - 3 years ago</label><br/>
                                    <input id="3+years" type="radio" name="lastVisit" value="3"<?php if(ISSET($_POST['lastVisit']) && $_POST['lastVisit']=='3'){echo " checked";}?>><label for="3+years"   style="font-weight:normal;" >&nbsp;More than 3 years ago</label>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12 sbm" style="margin-top:2%;">
                                <h4 class="comment_h4">How long have you lived at / visited this facility for?</h4>
                                <div class="form-group radio-new"   style="margin-left: 25px;">
                                    <?php foreach ($durationVisit as $key => $record) { ?>
                                        <div class="in-flx">
                                            <input type="radio" name="visitDuration" id="<?php echo 'durations-' . $record['id']; ?>"  value="<?php echo $record['id']; ?>"<?php if(ISSET($_POST['visitDuration']) && $_POST['visitDuration']==$record['id']){echo " checked";}?>><label style="font-weight:normal;"  for="<?php echo 'durations-' . $record['id']; ?>"><?php echo '&nbsp;' . $record['title']; ?></label>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12"  style=" margin-top:2%;">
                                <h4 class="comment_h4">Would you like your first name / username withheld from being displayed?</h4>
                                <div class="form-group radio-new"  style="margin-left: 25px;">
                                    <input id="uname" type="radio" name="anonymous" value="1"<?php if(ISSET($_POST['anonymous']) && $_POST['anonymous']=='1'){echo " checked";}?>><label for="uname"  style="font-weight:normal;" >&nbsp;Yes  </label>
                                    <br> <input id="noname" type="radio" name="anonymous" value="0" <?php if(ISSET($_POST['anonymous']) && $_POST['anonymous']=='0'){echo " checked";}?><?php if(!ISSET($_POST['anonymous'])){echo " checked";}?>><label for="noname"   style="font-weight:normal;" >&nbsp;No </label>
                                </div>
                            </div>


                            <div class="col-md-12 col-sm-12 col-xs-12"  style=" margin-top:5%;">
                                <h4 class="comment_h4">How did you hear about Aged Advisor?</h4>
                                <select size="1" name="how_did_you_hear" id="how_did_you_hear" class="input_fild required form-control" ><option value="">Please select*</option>
                                    <optgroup label="Online">
                                        <option value="Google" <?php if(ISSET($_POST['how_did_you_hear']) && $_POST['how_did_you_hear']=='Google'){echo "selected";}?>>Google</option>
                                        <option value="Another website" <?php if(ISSET($_POST['how_did_you_hear']) && $_POST['how_did_you_hear']=='Another website'){echo "selected";}?>>Another website</option>
                                        <option value="Facebook" <?php if(ISSET($_POST['how_did_you_hear']) && $_POST['how_did_you_hear']=='Facebook'){echo "selected";}?>>Facebook</option>
                                        <option value="Twitter" <?php if(ISSET($_POST['how_did_you_hear']) && $_POST['how_did_you_hear']=='Twitter'){echo "selected";}?>>Twitter</option>
                                    </optgroup>
                                    <optgroup label="People / Places">
                                        <option value="Form in letterbox" <?php if(ISSET($_POST['how_did_you_hear']) && $_POST['how_did_you_hear']=='Form in letterbox'){echo "selected";}?>>Form in letterbox</option>
                                        <option value="Letterbox Booklet Mailer" <?php if(ISSET($_POST['how_did_you_hear']) && $_POST['how_did_you_hear']=='Letterbox Booklet Mailer'){echo "selected";}?>>Letterbox Booklet Mailer</option>
                                        <option value="Active Retirees Insert" <?php if(ISSET($_POST['how_did_you_hear']) && $_POST['how_did_you_hear']=='Active Retirees Insert'){echo "selected";}?>>Active Retirees Insert</option>
                                        <option value="Family member" <?php if(ISSET($_POST['how_did_you_hear']) && $_POST['how_did_you_hear']=='Family member'){echo "selected";}?>>Family member</option>
                                        <option value="Friend" <?php if(ISSET($_POST['how_did_you_hear']) && $_POST['how_did_you_hear']=='Friend'){echo "selected";}?>>Friend</option>
                                        <option value="Health Care Professional"<?php if(ISSET($_POST['how_did_you_hear']) && $_POST['how_did_you_hear']=='Health Care Professional'){echo "selected";}?> >Health Care Professional</option>
                                        <option value="From the village/facility" <?php if(ISSET($_POST['how_did_you_hear']) && $_POST['how_did_you_hear']=='From the village/facility'){echo "selected";}?>>From the village/facility</option>
                                    </optgroup>
                                    <optgroup label="Organisations">
                                        <option value="Aged Concern" <?php if(ISSET($_POST['how_did_you_hear']) && $_POST['how_did_you_hear']=='Aged Concern'){echo "selected";}?>>Aged Concern</option>
                                        <option value="BowlsNZ" <?php if(ISSET($_POST['how_did_you_hear']) && $_POST['how_did_you_hear']=='BowlsNZ'){echo "selected";}?>>BowlsNZ</option>
                                        <option value="Greypower" <?php if(ISSET($_POST['how_did_you_hear']) && $_POST['how_did_you_hear']=='Greypower'){echo "selected";}?>>Greypower</option>
                                        <option value="Hearing Technology" <?php if(ISSET($_POST['how_did_you_hear']) && $_POST['how_did_you_hear']=='Hearing Technology'){echo "selected";}?>>Hearing Technology</option>
                                        <option value="MSPD Canterbury" <?php if(ISSET($_POST['how_did_you_hear']) && $_POST['how_did_you_hear']=='MSPD Canterbury'){echo "selected";}?>>MSPD Canterbury</option><option value="MS Wellington" <?php if(ISSET($_POST['how_did_you_hear']) && $_POST['how_did_you_hear']=='MS Wellington'){echo "selected";}?>>MS Wellington</option>
                                        <option value="MGS Missions Trip" <?php if(ISSET($_POST['how_did_you_hear']) && $_POST['how_did_you_hear']=='MGS Missions Trip'){echo "selected";}?>>MGS Missions Trip</option>
                                        <option value="Positive Ageing Expo" <?php if(ISSET($_POST['how_did_you_hear']) && $_POST['how_did_you_hear']=='Positive Ageing Expo'){echo "selected";}?>>Positive Ageing Expo</option>
                                        <option value="RV Residents Assoc. NZ" <?php if(ISSET($_POST['how_did_you_hear']) && $_POST['how_did_you_hear']=='RV Residents Assoc. NZ'){echo "selected";}?>>RV Residents Assoc. NZ</option>
                                        <option value="MOH Strategy" <?php if(ISSET($_POST['how_did_you_hear']) && $_POST['how_did_you_hear']=='MOH Strategy'){echo "selected";}?>>MOH Strategy</option>
                                        <option value="Other Community group" <?php if(ISSET($_POST['how_did_you_hear']) && $_POST['how_did_you_hear']=='Other Community group'){echo "selected";}?>>Other Community group</option>
                                    </optgroup>
                                    <optgroup label="Other Media">
                                        <option value="Newspaper"<?php if(ISSET($_POST['how_did_you_hear']) && $_POST['how_did_you_hear']=='Newspaper'){echo "selected";}?> >Newspaper</option>
                                        <option value="Magazine" <?php if(ISSET($_POST['how_did_you_hear']) && $_POST['how_did_you_hear']=='Magazine'){echo "selected";}?>>Magazine</option>
                                        <option value="TV" <?php if(ISSET($_POST['how_did_you_hear']) && $_POST['how_did_you_hear']=='TV'){echo "selected";}?>>TV</option>
                                        <option value="Radio" <?php if(ISSET($_POST['how_did_you_hear']) && $_POST['how_did_you_hear']=='Radio'){echo "selected";}?>>Radio</option>
                                        <option value="Other"<?php if(ISSET($_POST['how_did_you_hear']) && $_POST['how_did_you_hear']=='Other'){echo "selected";}?> >Other</option>
                                    </optgroup>

                                </select>
                            </div>



                            <!-- *******for check user data** -->
                            <?php  

                            if (isset($_SESSION['csId']) && !empty($_SESSION['csId'])) {
                                $customerId = $_SESSION['csId'];
                            } else if (isset($_SESSION['userId']) && !empty($_SESSION['userId'])) {
                                $customerId = $_SESSION['userId'];
                            } else {
                                $customerId = $supplierId;
                            }

                            if(isset($id)){
                                $check_user="select * from ad_feedbacks where cs_id={$customerId} AND product_id={$id}";
                            }
                            
                            if(isset($prodd_id))
                            {
                                $check_user="select * from ad_feedbacks where cs_id={$customerId} AND product_id={$id}";
                            }
                            
                            $product_detail_new=mysqli_query($db->db_connect_id,$check_user); 
                            if (mysqli_num_rows($product_detail_new) > 0) 
                            {
                                if($new_ques_ans= mysqli_fetch_array($product_detail_new)){
                                    $new_ans=$new_ques_ans['why_choose'];
                                    $why_choose_value=explode(",",$new_ans);
                                    $why_choose_f=$why_choose_value[0];
                                    $why_choose_s=$why_choose_value[1];
                                    $why_choose_t=$why_choose_value[2];
                                    $why_choose_for=$why_choose_value[3];
                                    $why_choose_fiv=$why_choose_value[4];
                                    $why_choose_array_val=array($why_choose_f,$why_choose_s,$why_choose_t);
                                    $new_ans_nationality=$new_ques_ans['nationality'];
                                }
                            ?>
                            <!-- fetching data of new question ans if user and prod already exist-->
                            <div class="col-md-12 col-sm-12 col-xs-12"  style="margin-top:2%;">
                                <h4 class="comment_h4"> In choosing a place, what’s most important to you? (Select up to 3)</h4>
                                <div class="form-group"  style="margin-left: 25px;">
                                    <?php foreach ($choosePlace as $key => $record) { ?>
                                        <input type="checkbox" name="why_choose[]"  class="single-checkbox" id= "chk" value="<?php echo $record['title']; ?>"<?php if (in_array($record['title'], $why_choose_array_val)) { echo "checked";} ?> >

                                        <label style="font-weight:normal;"  for="<?php echo 'choose-place' . $record['id']; ?>"><?php echo '&nbsp;' . $record['title']; ?></label></br>
                                    <?php } ?>

                                    <p>if other mention here </p>
                                    <div class="col-md-12 col-sm-12 col-xs-12"  style="margin-top:2%;">

                                    <input type="text" class="form-control"  placeholder="others" name="why_choose1" id="" value="<?php if (isset($why_choose_for) && !empty($why_choose_for)) { echo $why_choose_for; } ?>" >
                                        <input type="text" class="form-control"  placeholder="others" name="why_choose2" id= "" value="<?php if (isset($why_choose_fiv) && !empty($why_choose_fiv)) { echo $why_choose_fiv; } ?>" >
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12"  style="margin-top:2%;">
                                <h4 class="comment_h4">Ethnicity</h4>

                                <select name="nationality" class="form-control" id="country">
                                    <?php if ($new_ans_nationality == 1) { ?>
                                        <option value="1">New Zealander</option>
                                    <?php } if ($new_ans_nationality == 4) { ?>
                                        <option value="4">Maori/Pacific</option>
                                    <?php } if ($new_ans_nationality != 1 && $new_ans_nationality != 4) {
                                            $originaty_name = mysqli_query($db->db_connect_id,"select origin_name from ad_originity where id={$new_ans_nationality}");
                                            if ($originty_name_val = mysqli_fetch_array($originaty_name)) {
                                                $orig_name = $originty_name_val['origin_name'];
                                            }
                                    ?>
                                        <option value="<?php echo $new_ans_nationality; ?>"><?php echo $orig_name; ?></option>  
                                    <?php } ?>
                                    <?php foreach($origin_res as $key => $record) { ?>
                                        <option value="<?php echo $record['id']; ?>"><?php echo $record['origin_name']; ?></option>
                                    <?php } ?>
                                    </select>
                                </div>
                                <?php } else { ?>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <h4 class="comment_h4"> In choosing a place, what’s most important to you? (Select up to 3)</h4>
                                    <div class="form-group"  style="margin-left: 25px;">
                                    <?php foreach ($choosePlace as $key => $record) { ?>
                                            <input type="checkbox" name="why_choose[]"  class="single-checkbox" id= "chk" value="<?php echo $record['title']; ?>"><label style="font-weight:normal;"  for="<?php echo 'choose-place' . $record['id']; ?>"><?php echo '&nbsp;' . $record['title']; ?></label></br>
                                    <?php } ?>
                                        <p>if other mention here </p>
                                        <input type="text"   class="form-control"  placeholder="others" name="why_choose1" id="">
                                        <input type="text"   class="form-control"  placeholder="others" name="why_choose2" id= "">
                                        <h4 class="comment_h4">Ethnicity <small>(This is to help others of a similar ethnicity)</small></h4>
                                        <select name="nationality" class="form-control" id="country">
                                            <option value="">Please Select</option>
                                                <?php foreach ($origin_res as $key => $record) { ?>
                                                <option value="<?php echo $record['id']; ?>"><?php echo $record['origin_name']; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                </div>
                                <?php } ?>
                                        <!--<input type="checkbox"  class="single-checkbox" name="imp_for_place" value="location">Locations 
                                        <input type="checkbox"  class="single-checkbox"  name="imp_for_place" value="staffing">staffing
                                        <input type="checkbox"   class="single-checkbox" name="imp_for_place" value="homely-feel">homely-feel 
                                        <input type="checkbox"   class="single-checkbox"  name="imp_for_place" value="social-atomoshpher" > social-atomoshpher
                                        <input type="checkbox"  class="single-checkbox"  name="imp_for_place" value="Care">Continuum of Care <br>
                                        <input type="checkbox"  class="single-checkbox"  name="imp_for_place" value="Modern-Decor">Modern Decor
                                        <input type="checkbox"  class="single-checkbox"  name="imp_for_place" value="Management">Management 
                                        <input type="checkbox"  class="single-checkbox"   name="imp_for_place" value="VariedActivities" >Varied Activities
                                        <input type="checkbox"   class="single-checkbox" name="imp_for_place" value="Food" >Enjoyable Food
                                        <input type="checkbox"  class="single-checkbox"  name="imp_for_place" value="bathroom">Private Bathroom
                                        <input type="text"   class="single-checkbo"  placeholder="others" name="imp_for_place" id="">
                                        <input type="text   class="single-checkox"  placeholder="others" name="imp_for_place" id=""> -->                    
                                    <div class="col-md-12 col-sm-12 col-xs-12"  style="margin-top:2%;">
                                        <?php if($needs_login){ ?>
                                        <h4 class="comment_h4">To ensure we only post authentic reviews, please confirm your details below.</h4>
                                        <p>NOTE: We never display or share contact details.</p>
                                        <div class="col-sm-12">
                                            <input type="text" class="form-control input_fild required alphaNumCsF margin-top-20" id="first_name"  name="first_name" maxlength="50" placeholder="First Name*" value="<?php if(ISSET($_POST['first_name'])){echo cleanthishere($_POST['first_name']); }else{ echo "First Name*";}?>"  onfocus="this.value = '';" >
                                        </div>
                                        <div class="col-sm-12">
                                            <input type="text" class="form-control input_fild alphaNumCsL margin-top-20" id="last_name"  name="last_name" placeholder="Last Name*" value="<?php if(ISSET($_POST['last_name'])){echo cleanthishere($_POST['last_name']);}else{echo "Last Name*";}?>"  onfocus="this.value = '';" >
                                        </div>

                                        <div class="clearfix"></div>
                                        <div class="col-sm-12">
                                            <p>&nbsp;</p>
                                            <p><span class="color-red">*</span> For verification purposes please enter at least a valid email address <b>OR</b> your phone number.</p>
                                        </div> 
                                        <div class="col-md-12">  
                                            <label for="email" class="margin-top-20">Your Email</label>
                                            <input type="text" class="form-control input_fild" id="email"  name="email" placeholder="Your Email"  value="<?php if(ISSET($_POST['email'])){echo cleanthishere($_POST['email']);}?>">
                                        </div>
                                        <div class="col-md-12">
                                            <label for="phone"  class="margin-top-20">AND/OR Your Phone No.</label>
                                            <input type="text" class="form-control input_fild phonePattern" id="phone" name="phone"   maxlength="19"  placeholder="AND/OR Your Phone No."  value="<?php if(ISSET($_POST['phone'])){echo cleanthishere($_POST['phone']);}?>">
                                        </div>
                                        <div id="create_user_passwords">

                                            <p>&nbsp;<br>Please provide a User Name - We recommend using a first name and up to three numbers.<br>(This will be displayed, unless you have requested it to be withheld. It must be 6-30 characters in length.)</p>

                                            <div class="col-md-12 col-sm-12">
                                                <label for="user_name" class="margin-top-20">User Name* (Do not use an email address)</label>
                                                <input type="text" placeholder="User Name* (Do not use an email address)" name="user_name" id="username"  class="form-control input_fild noSpace" value=""  onfocus="this.value = '';" >
                                            </div>
                                            <div class="col-md-12 col-sm-12">
                                                <label for="password" class="margin-top-20">Password*</label>     
                                                <input type="password" class="form-control input_fild required" id="password" name="password"  value="" >
                                            </div>
                                            <div class="col-md-12 col-sm-12">
                                                <label for="confirm_password" class="margin-top-20">Confirm Password*</label>        
                                                <input type="password" class="form-control input_fild required" id="confirm_password" name="confirm_password" value="">
                                            </div>
                                        </div>
                                        <?php  }else{ ?>
                                            <input type="hidden" id="already_logged_in" name="already_logged_in"  value="true">
                                        <?php } ?>



                                        <?php if(ISSET($_GET['r']) && $_GET['r'])  {// From Email followup   ?>
                                            <input type="hidden" id="how_did_you_hear" name="how_did_you_hear"  value="6 Month Referral Email">
                                        <?php  }else{  ?>
                                        <p>&nbsp;</p>

                                        <?php } ?>

                                        <p>&nbsp;</p>

                                        <div class="col-sm-12">
                                            <?php if(!$they_have_agreed_newsletter_previously){ ?> 
                                            <label>
                                                <input type="checkbox" class="input_fild" id="email_updates_facility"  name="email_updates_facility" value="yes" <?php if(ISSET($_POST['email_updates_facility']) && $_POST['email_updates_facility']=='yes'){echo "checked";}?>> 
                                                Receive our Newsletter.
                                            </label>
                                            <?php }else{
                                                echo '<input type="hidden" name="email_updates_facility" value="yes">';                                
                                            }?>
                                        </div>
                                        <div class="col-sm-12">
                                            <label>
                                                <input type="checkbox" class="input_fild required" id="agree"  name="agree" value="yes" <?php if(ISSET($_POST['agree']) && $_POST['agree']=='yes'){echo "checked";}?>> 
                                                       I agree to your <a href="<?php echo HOME_PATH ?>terms-and-conditions" target="_blank" class="color-green">Terms and Conditions</a> <span class="color-red">*</span>
                                            </label>              
                                        </div>         
                                    </div>
                                    <input type="hidden" name="country" value="157">
                                    <input type="hidden" name="reviewer_TYPE" value="">

                                    <div class="col-md-12 col-sm-12 col-xs-12"  style="margin-bottom:5%; margin-top:2%;">

                                        <div class="row">

                                            <div style="margin-top:10px;" class="col-md-4">
                                                <div class="g-recaptcha" data-sitekey="6Ldc7iAUAAAAANMNq065X8wNJOepTkdzXO0pa57C"></div>                   
                                            </div>
                                            <div class="col-md-4" style="margin-top:30px;">
	                                            <span id="initials_dots">...</span>&nbsp;&nbsp;<input style="display: none;width:60px;" type="text" name="initials_staff" id="initials_input" value="">
	                                            
                                                <button name='submit1' id="submitButton" value='submit' class="btn btn-danger inner_btn comment_btn " type="submit">Post your Review</button>
                                                <button style="display: none" type="button" id="processing_update_details_button" disabled="disabled" class="btn btn-danger">Processing Now <span> <img src="../../assets/img/72.gif"> </span></button>                        
                                                <input type="hidden" name="submit2" value="submit" />
                                            </div>
                                        </div>                    
                                    </div>
                                </form>
                            </div>
                            <?php }?>
                        </div>
                    </div>
                    <!--MAIN BODY CODE ENDS-->
                    <style>
                        input,textarea{
                            color:black;
                            font-size: 15px;
                        } label.error{
                            color:red;
                            font-size: 15px;

                        }
                        .redCol{
                            color:red;
                        }
                    </style>
                    <script type="text/javascript">                            

                        $(document).ready(function() {
                            var path = "<?php echo HOME_PATH; ?>admin/";
                            jQuery.validator.addMethod("phonePattern", function(value, element) {
                                return this.optional(element) || /^[0-9()\+\- ]+$/.test(value);
                            }, "Enter valid phone number.");
                            
                            jQuery.validator.addMethod("alphaNumeric", function(value, element) {
                                return this.optional(element) || /^[0-9a-zA-Z]+$/.test(value);
                            }, "Only  Alphanumeric.");
                            
                            jQuery.validator.addMethod("alphaNumCsF", function(value, element) {
                                return this.optional(element) || /^[a-zA-Z\'\.\-\ ]+$/.test(value);
                            }, "Please enter valid first name");
                            
                            jQuery.validator.addMethod("alphaNumCsL", function(value, element) {
                                return this.optional(element) || /^[a-zA-Z\'\.\-\ ]+$/.test(value);
                            }, "Please enter valid last name");
                            
                            jQuery.validator.addMethod("noSpace", function(value, element) {
                                return value.indexOf(" ") < 0 && value != "";
                            }, "Space is not allowed");
                            
                            setTimeout(function() {
                                $("#success").hide('slow');
                                $('#error').hide('slow');
                            }, 350000);
                            
                            //$('#blog_comment_reply').validate();
                            var path = "<?php echo HOME_PATH; ?>admin/";
                            $('#feedbackForm').validate({
                                rules: {
                                    phone: {
                                        minlength: 9,
                                        maxlength: 15
                                    },
                                    password: {
                                        required: true,
                                        minlength: 6,
                                        maxlength: 20
                                    },
                                    confirm_password: {
                                        equalTo: "#password"
                                    },
                                    agree: {
                                        required: true,
                                    },
                                    how_did_you_hear: {
                                        required: true,
                                    },
                                    email: {
                                        email: true
                                    },
                                    user_name: {
                                        required: true,
                                        minlength: 6,
                                        maxlength: 30,
                                        remote: {
                                            url: path + 'ajax.php',
                                            type: 'POST',
                                            data: {
                                                action: "unquieUsername", username: function() {
                                                    return $('#username').val();
                                                }
                                            }
                                        }
                                    },
                                    feedback:
                                    {
                                        required: true,
                                        maxlength: 500
                                    },
                                    captcha: {
                                        required: true,
                                        remote:
                                        {
                                            url: path + 'captcha/checkCaptcha.php',
                                            type: "post",
                                            data:
                                            {
                                                code: function()
                                                {
                                                    return $('#captcha-text').val();
                                                }
                                            }
                                        }
                                    }
                                },
                                messages: {
                                    zipcode: {
                                        required: "This field is required.",
                                        minlength: "Zipcode must be contain at least 3 chars.",
                                        maxlength: "Zipcode must be contain maximum 10 chars."
                                    },
                                    phone: {
                                        remote: "Phone number already in use. Please use other phone number.",
                                        minlength: "Phone must contain at least 9 chars.",
                                        maxlength: "Phone should not be more than 15 characters."
                                    },
                                    password: {
                                    required: " This field is required.",
                                        minlength: "Password must be contain at least 6 chars.",
                                        maxlength: "Password must be contain maximum 20 chars."
                                    },
                                    agree: {
                                        required: "You must agree<br>",
                                    },
                                    how_did_you_hear: {
                                        required: " This field is required."
                                    },
                                    confirm_password: {
                                        equalTo: " Enter Confirm Password Same as Password."
                                    },
                                    email: {
                                        email: "Please enter a valid email address.",
                                        remote: "Email address already in use. Please use other email."
                                    },
                                    user_name: {
                                        required: "This field is required.",
                                        remote: "User name already in use. Please use other User name.",
                                        minlength: "User name  must be contain at least 6 chars.",
                                        maxlength: "User name  must be contain maximum 30 chars."
                                    },
                                    feedback: {
                                        required: "This field is required.",
                                        maxlength: "Feedback should not be more than 500 characters "
                                    }
                                },
                                submitHandler: function (form) {
                                    // do other things for a valid form
                                    $('#processing_update_details_button').show();
                                    $('#submitButton').hide();
                                    $(this).hide();                                
                                    setTimeout(function() {
                                        $('#submitButton').show();
                                        $('#processing_update_details_button').hide();
                                    }, 50000);
                                    form.submit();
                                }
                            });
                            <?php if($needs_search_facility=='OFF'){ ?>
                            $('.supplier').on('change', function () {
                                var id = $(this).val();
                                $.ajax({
                                    type: 'POST',
                                    url: 'https://agedadvisor.nz/ajaxFront.php',
                                    data: {
                                        action: 'getProduct',
                                        supplierId: id
                                    },
                                    success:function (response) {
                                            $('#product').html(response);
                                    }
                                });
                            });                                
                            <?php } ?>                                    
                            /* for open a    nd hide div on click on the checkbox------*/
                            $(".our-guidlines").hide();
                            $(".checkguildlines1").click(function() {
                                if ($(this).is(":checked")) {
                                    $(".our-guidlines").show();
                                } else {
                                    $(".our-guidlines").hide();
                                }
                            });    
                            /* foropen and hide d    iv  on click on the checkbox------*/ 
                            // MALCOLM's Jquery code forch    ecking email
                            function refreshFunction() {
                                var e = $("#email").val();
                                if (validateEmail(e)) {
                                    $( "#create_user_passwords" ).load(  "/modules/rating/find_email_user.php",{testemail:e}, function( response, status, xhr ) { if ( status == "error" ) {    var msg = "Sorry but there was an error: "; $( "#create_user_passwords" ).html( msg + xhr.status + " " + xhr.statusText );  } }); 
                                } 
                            }
                            

                            function validateEmail(sEmail) {
                                var filter = /^\S+@\S+\.\S+$/;
                                if (filter.test(sEmail)) {
                                    return true;
                                } else {
                                    return false;
                                }
                            }

                            $("#email").on('input',function(e) {e.preventDefault(), refreshFunction(); });
                        });


                        $("#initials_dots").click(function()     {
							$("#initials_dots").hide();
							$("#initials_input").show().focus();
                        });


                        $("#clickyes").change(function()     {
                            if ($(this).is(':checked')){
                                $('#yes_raised_issue').slideDown('slow');
                            }
                        });
                        $("#clickno").change(function()     {
                            if ($(this).is(':checked')){
                                $('#yes_raised_issue').slideUp('slow');
                            }
                        });

                        $("#clickplanto").change(function() {
                            if ($(this).is(':checked')){
                                $('#yes_raised_issue').slideUp('slow');
                            }
                        });

                    $("#overall_rating").change(function() {
                        
                        if ($("#quality_of_care").val() < 2 && $("#quality_of_care").val() > 0){$('#raised_issue').slideDown('slow'); }
                        if ($("#caring_staff").val()<2 && $("#caring_staff").val()>0){$('#raised_issue').slideDown('slow'); }
                        if ($("#responsive_management").val()<2 && $("#responsive_management").val()>0){$('#raised_issue').slideDown('slow'); }
                        if ($("#trips_outdoor_activities").val()<2 && $("#trips_outdoor_activities").val()>0){$('#raised_issue').slideDown('slow'); }
                        if ($("#indoor_entertainment").val()<2 && $("#indoor_entertainment").val()>0){$('#raised_issue').slideDown('slow'); }
                        if ($("#social_atmosphere").val()<2 && $("#social_atmosphere").val()>0){$('#raised_issue').slideDown('slow'); }
                        if ($("#enjoyable_food").val()<2 && $("#enjoyable_food").val()>0){$('#raised_issue').slideDown('slow'); }
                        if ($("#overall_rating").val    ()<3 && $("#overall_rating").val()>0){$('#raised_issue').slideDown('slow'); }
                    });

                    </script>
                    <!--<script>
                        (fun    ction() {
                            var cx = '004227034257929506210:nqmdrcpyzkw';
                            var gcse = document.createElement('script');
                        gcse.type = 'text/javascript';
                            gcse.async = true;
                            gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
                                    '//www.google.com/cse/cse.js?cx=' + cx;
                            var s = document.getElementsByTagName('script')[0];
                            s.parentNode.insertBefore(gcse, s);
                        })();
                    </script>
                    <gcse:searchbox></gcse:searchbox>-->
                    <style>
                        .form-group input[type=checkbox] {

                            width: auto !important;
                            height: auto !important;
                            zoom:0;
                        }

                    </style>
                    <script type="text/javascript">
                            $(document).ready(function(){
                                $('#restcareid').each(function() {
                                    var elem =$(this);
                                });
                            });
                    </script>
                    <script type="text/javascript">
                        $(document).ready(function(){
                            var user_id = '<?php echo $customerId; ?>';
                            url = '/modules/rating/ajax-review-msg.php';
                            $('#cityLable').focusout(function() {
                                var product_id = $("#restcareid").attr('value');
                                <?php if (empty($id)) { ?>
                                if (product_id == '' || product_id == null)
                                {
                                    //$("#ajax-data").hide();
                                }
                                else
                                {
                                    var prod_id = product_id;
                                    datstring = 'user_id=' + user_id + '&product_id=' + prod_id;
                                        $.ajax({
                                            type:"POST",
                                            url:url,
                                            data:datstring,
                                            success:function(result)
                                            {
                                                $("#ajax-data").html(result);
                                            }
                                        });                           
                                }
                                <?php } ?>
                            });
                        });
                    </script>