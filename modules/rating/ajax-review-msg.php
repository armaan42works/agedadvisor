<?php
ini_set('display_errors', '1');
require_once '../../application_top.php';
global $db;

$user_id=$_REQUEST['user_id']; 
$product_id=$_REQUEST['product_id'];
$cur_date=date("Y-m-d");

$feedback_count_day = "SELECT *  from ad_feedbacks where `cs_id`={$user_id} and `product_id` ={$product_id} and `deleted`='0' and `status`='1' order by created desc limit 1 ";
$result_feedback = mysqli_query($db->db_connect_id, $feedback_count_day);
if(mysqli_num_rows($result_feedback)>0){
   while($datas = mysqli_fetch_array($result_feedback))
   {
                        $last_date_time=$datas['created']; 
                        $feedlast_date= date("Y-m-d",strtotime($last_date_time));
                        $date1 = new DateTime($feedlast_date);
                        $date2 = new DateTime($cur_date);
                        $interval = $date1->diff($date2);
                        $interval_days=$interval->days ;
                       if($interval_days <= 90){
                         echo '<div class="less_ninty alert alert-success text-center">
                         <a href="#" class="close" data-dismiss="alert">×</a>
                             <p>Thanks for the review. Unfortunately, it appears you have already placed a review on this facility within the last 90 days. We require that any followup reviews have at least a 6 month gap from the previous time, to keep it balanced. You may post a review after a minimum of 90 days if the star ratings you gave last time have changed. Please come back to post your review after the 90 day minimum timeframe is up.</p><p> Regards, Aged Advisor Reviews Team.</p> </div>'; 
                            
                          }
                        if($interval_days >90 and $interval_days <=180){
                         echo " <div class='less_ninty alert alert-success text-center'>
                         <a href='#' class='close' data-dismiss='alert'>×</a>
                        <p>Welcome back. It appears that you placed a review on this facility within the last 3 - 6 months. If there's been any changes then your review will be posted.</p> </div";
                          }
    }
}
?>