<link rel="stylesheet" type="text/css" href="<?php echo HOME_PATH;?>assets/plugins/bootstrap/css/bootstrap.min.css"> 
<div class="clearfix"></div> <!--Listing Details Hero starts-->
 <div class="listing-details-wrapper bg-h" style="background-image: url(<?php echo HOME_PATH; ?>images/single-listing/single-list-1.jpg);">
           
            <div class="container">
                <div class="row">
                    <div class="col-md-12" style="padding:0;">
                        <div class="list-details-title v1 op-7" style="background-color:#143b78; padding-left:15px; padding-right:15px;">
                            <div class="row">
                                <div class="col-lg-6 col-md-7 col-sm-12">
                                    <div class="single-listing-title float-left">
                                       <p><a class="btn v6 red"><?php echo $certification_service_type?> Profile</a>  <?php  if($logo_enable==1 && $logo!=''){ ?>
            <img style="margin-top: 7px;" src="<?php echo HOME_PATH; ?>/admin/files/product/logo/thumb_<?php echo $logo?>" class="profile-heading-img" alt="<?php echo $certification_service_type?> <?php echo isset($title) ? stripslashes($title) : ''; ?> Logo" title="<?php echo $certification_service_type?> <?php echo isset($title) ? stripslashes($title) : ''; ?> Logo">
            <?php  } else { 
                if(!file_exists(HOME_PATH . '/admin/files/product/logo/thumb_' . $logo) && file_exists(DOCUMENT_PATH . 'admin/files/user/' . $provider_logo) && $provider_logo != '' &&  $plan_id_of_facility!=1 && $plan_id_of_facility!=''){ ?>            
            <img style="margin-top: 7px;" src="<?php echo HOME_PATH; ?>admin/files/user/thumb_<?php echo $provider_logo?>" class="profile-heading-img" alt="<?php echo $certification_service_type?> <?php echo isset($title) ? stripslashes($title) : ''; ?> Logo" title="<?php echo $certification_service_type?> <?php echo isset($title) ? stripslashes($title) : ''; ?> Logo">
            <?php  } }?></p>
                                       <h2 class="shortlist-heading" itemprop="name"><i class="icofont-tick-boxed"></i>
                        <?php echo isset($title) ? stripslashes($title) : ''; ?>                       
                        <?php if( in_array($plan_type_id, $bronze_plan) ) { ?>
                            <div style="display: inline-block; color: #cd7f32">BRONZE</div>
                        <?php } else if( in_array($plan_type_id, $gold_plan) ) { ?>
                            <div style="display: inline-block; color: #FFD700">GOLD</div>
                        <?php } else if( in_array($plan_type_id, $silver_plan) ) { ?>
                            <div style="display: inline-block; color: #C0C0C0">SILVER</div>
                        <?php } ?>
                   
                   <?php if($logo_enable && $award_text){?>
                        <span class="pull-left " style="color: #042E6F;padding-bottom: 0px;margin: 0 0 0px;font-size: 15px;">
                            <span class="glyphicon glyphicon-certificate" aria-hidden="true"></span> 
                            <?php echo $award_text?></span>  
                    <?php }?>
                   
                   
                   
                    <?php  if(isset($_SESSION['searchid']) && is_numeric($_SESSION['searchid'])){
                        $sid=$_SESSION['searchid']; ?>
                        <!--<a href="<?php echo HOME_PATH ?>search/search/<?php echo $sid?>">&lt; Return to Search</a> &nbsp; -->
                    <?php  } ?>
                      
                    <?php if(isset($_SESSION['csUserName']) && !empty($_SESSION['csUserName'])) { ?>
                      
                        <?php if(isset($_SESSION['session_product'][$record['supplier_id'][[$record['pr_id']]]])) {?>
                           <a id="shortlist" class="addto_shortlist hide">Add to shortlist</a>
                           <a id="remove_shortlist" class="remove_shortlist">Remove from shortlist</a> 
                        <?php  }else { ?>
                           <a id="shortlist" class="addto_shortlist">Add to shortlist</a>
                           <a id="remove_shortlist" class="remove_shortlist hides">Remove from shortlist</a>
                        <?php  } ?>
                         </h2>
                      
                    <?php } else { ?>
                        
                        <?php if(isset($_COOKIE[ 'session_cookie_product' ])){ 
                            $session_cookie_product_arr = unserialize($_COOKIE['session_cookie_product'], ["allowed_classes" => false]);
                        ?>
                            <?php if(isset($session_cookie_product_arr[$record['supplier_id']][$record['pr_id']])) {?>
                               <a id="shortlist" class="addto_shortlist" style="display: none">Add to shortlist</a> 
                               <a id="remove_shortlist" class="remove_shortlist">Remove from shortlist</a>
                            <?php  } else{?>
                               <a id="shortlist" class="addto_shortlist">Add to shortlist</a>
                               <a id="remove_shortlist" class="remove_shortlist" style="display: none">Remove from shortlist</a> 
                            <?php  } ?>  
                        <?php } else { ?>
                            <?php if(isset($_SESSION['session_cookie_product'][$record['supplier_id']][$record['pr_id']])) {?>
                               <a id="shortlist" class="addto_shortlist" style="display: none">Add to shortlist</a> 
                               <a id="remove_shortlist" class="remove_shortlist">Remove from shortlist</a>
                            <?php  } else{?>
                               <a id="shortlist" class="addto_shortlist">Add to shortlist</a>
                               <a id="remove_shortlist" class="remove_shortlist" style="display: none">Remove from shortlist</a> 
                            <?php  } ?>  
                        <?php } ?>
                        </h2>                    
                    <?php } ?> 
                                       <p><?php  if(isset($address) && !empty($address)) { ?><span itemprop="streetAddress"><i class="ion-ios-location"></i> <?php echo $address?></span> <?php  } ?>
                                            <?php  if(isset($address_suburb) && !empty($address_suburb)) { ?><span itemprop="addressLocality"><?php echo $address_suburb?></span> <?php  } ?>
                                            <?php  if(isset($address_city) && !empty($address_city)) { ?><span itemprop="addressCountry">, <?php echo $address_city?>, <?php echo $zip?></span><?php  } ?>
                                            <span>New Zealand</span>											
											</p>
											 <div class="add-review" style="color:#fff;" itemprop="aggregateRating" itemscope="" itemtype="http://schema.org/AggregateRating">
                        <meta itemprop="bestRating" content="5"/>
                        <meta itemprop="worstRating" content="0"/>
                        <span style="display:none;" itemprop="itemReviewed"><?php echo $title?></span>
                        Avg. Rating:
                       <?php if(simlilarFacilityRating($record['pr_id'])==0)
                        {?>
                            Not Rated
                            <span class="text-hide" itemprop="ratingValue">0</span>
                        <?php }
                        else
                        {?>
                       <?php echo OverAllNEWRatingProduct($record['pr_id']);?><?php echo simlilarFacilityRating($record['pr_id'])?>%
                        <?php } ?>
                       <?php if (overAllRatingProductCount($record['pr_id']) != '') { ?>
               
               
                <a href="<?php echo HOME_PATH; ?>search/viewReview/<?php echo $record['pr_id']; ?>/Reviews-for-<?php  echo str_ireplace('search/', '', $quick_url);?>"><i class="icofont icofont-users mr-3"></i><span itemprop="reviewCount"><?php echo overAllRatingProductCount($record['pr_id']); ?></span></a>
                <?php } else { ?>
                <i class="icofont icofont-users mr-5"></i><span itemprop="reviewCount"><?php echo overAllRatingProductCount($record['pr_id']); ?></span>
                <?php } ?>
                
                <?php $reviewlink='<a class="pdl-15" href="'.HOME_PATH . 'rating?spId=' .base64_encode($record['supplier_id']) . '&prId=' .base64_encode($record['pr_id']).'">Add Review</a>';
                echo $reviewlink;
               
               // if(ISSET($review_friendly)){ echo " <br>".$review_friendly;}
                ?>
                <?php 
                    $sup_id= $record['supplier_id'];
                    $opt_email=mysqli_query($db->db_connect_id, "select * from ad_users where id='$sup_id'");
                    while($opt_email1=mysqli_fetch_array($opt_email))
                    {
                       $opt_email2=$opt_email1['email_updates_facility'];
                    }
                ?>
                <!-- supplier id -->
                
                <input type="hidden" id="supp_messid" value="<?php echo $record['supplier_id'];?>"/>
                <input type="hidden" id="pro_fac_id" value="<?php echo  $record['pr_id'];?>"/>
                <input type="hidden" id="customer_user_name" value="<?php echo $_SESSION['csUserName']?>"/>
                <!-- close here supplier id -->
                </div>
				<p> 
                    <?php if($rank == NULL){ ?>
                        <a style="color:#fff;" href="javascript:void(0)">Not Ranked Yet</a>
                        <i class="icofont icofont-question-circle" data-toggle="tooltip" title="Facilities that are 'not ranked' must have a minimum of 3 reviews to give a ranking. AgedAdvisor rankings also take into account the number of dwellings or beds the facility has. The higher the number of reviews to no. of dwellings / beds, then the higher the ranking accuracy."></i>
                    <?php }else{ ?>
                        <a style="color:#fff;" href="javascript:void(0)" ># <?php echo $rank;?>  of  <?php echo $certification_service_type_count;?> in New Zealand.</a>
                    <?php } ?>                                                                                     
                </p>
                                         
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-5 col-sm-12">
                                    <div class="list-details-btn text-right sm-left">
                                        <div class="save-btn">
                                            <a href="#" class="btn v3 white"><i class="ion-heart"></i> Save</a>
                                        </div>
                                        <div class="share-btn">
                                            <a href="#" class="btn v3 white"><i class="ion-android-share-alt"></i> Share</a>
                                            <ul class="social-share">
                                                <li class="bg-fb"><a href="#"><i class="ion-social-facebook"></i></a></li>
                                                <li class="bg-tt"><a href="#"><i class="ion-social-twitter"></i></a></li>
                                                <li class="bg-ig"><a href="#"><i class="ion-social-instagram"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
       
		<!--Listing Details Hero ends-->
        <!--Listing Details Info starts-->
         <div class="list-details-section section-padding">
            <div class="container">
			 	
                <div class="row">
                    <div class="col-lg-8 col-md-12">             
				
                        <div id="list-menu" class="list_menu">
                            <ul class="list-details-tab fixed_nav">                                
                            
                        <li class="nav-item active" id="Description"><a href="#overview" class="active">Description</a></li>
						 <?php if($opt_email2==1){ ?>                                
								<li id="Questions" class="nav-item"><a href="#price">Questions</a></li>
                                <?php  } ?>
                                <li class="nav-item" id="Contact1"><a href="#Contact">Contact</a></li>
								
								 <?php  if($certification_service_type=='Aged Care'){?>
                                <li class="nav-item" id="dhbDetails1"><a href="#dhbDetails">DHB Audits</a></li>
                                <?php  } ?>
                                <?php  if($certification_service_type=='Retirement Village'){?>
                                <li class="nav-item" id="otherDetails1"><a href="#otherDetails">Entry Conditions</a></li>
                                <?php  } ?> 
                                
                                <li class="nav-item"><a href="#reviews">Reviews</a></li>
                                <li class="nav-item"><a href="#add_review">Add Review</a></li>
                            </ul>
                        </div>
                        <!--Listing Details starts-->
                        <div class="list-details-wrap">
                            <div id="overview" class="list-details-section">                               
                                <div class="overview-content">
								<?php                     
                    if(in_array($plan_type_id, $silver_plan) || in_array($plan_type_id, $gold_plan)){
                        if ($brief){
                            echo "<p itemprop='description'>".stripslashes($brief)."</p>";
                        } 
                    }                    
                ?>
				
				 <h3>Facility services offered</h3>
				 
				  
                <ul class="listing-features"><?php
                    $i = 1;
                    foreach ($facilityType as $record) {
                        echo '<li>' . $record['name'] . '</li>';$i+=1;
                    }
                    ?>
                </ul>
                                    <p class="mar-bot-10"> <?php if( $noOfRooms || $noOfBeds || $description || $description_our_version ){ ?>
                                        <h3>
                                            <i class="icofont icofont-file-text"></i>
                                            <?php echo $certification_service_type?> Information 
                                            <small>&nbsp;&nbsp;<?php echo $last_update?></small>
                                        </h3>
                                        <p>This facility is owned by <?php echo $company?>.</p>
                                        <p>
                                            <?php                                              
                                            if( $plan_type_id == 1 ){
                                                echo $description_our_version;
                                            } else if( in_array($plan_type_id, $bronze_plan) || in_array($plan_type_id, $silver_plan) || in_array($plan_type_id, $gold_plan) ) {
                                                if(strlen($description)<100 && strlen($description_our_version)>50){
                                                echo $description_our_version;                                                
                                            }else{ 
                                                echo $description;                                                
                                            }
                                            } else {
                                                echo $description_our_version;
                                            }
                                            /*
                                            if(strlen($description)<100 && strlen($description_our_version)>50){
                                                echo $description_our_version;                                                
                                            }else{ 
                                                echo $description;                                                
                                            }*/
                                            ?>
                                        </p>
                                        <p>
                                            <?php  if($noOfRooms){?> 
                                                No. of Units 
                                                <?php echo $noOfRooms?><br>
                                            <?php  } ?>
                                            <?php  if($noOfBeds){?>
                                                No. of Beds <?php echo $noOfBeds?>
                                            <?php  } ?>
                                        </p>
                                    <?php  } ?>
                                    <?php  if($staff_comment && $staff_enable){?>
                                        <a name="staff"></a>
                                        <h3>
                                            <?php echo $certification_service_type ?> Staff
                                        </h3>
                                        <p><?php echo $staff_comment ?></p>
                                    <?php  } ?>
                                    <?php  if($management_comment && $management_enable){ ?>
                                        <a name="management"></a>
                                        <h3><?php echo $certification_service_type?> Management</h3>
                                        <p><?php echo $management_comment?></p>
                                    <?php } ?>
                                    <?php if($activities_comment && $activity_enable){ ?>
                                        <a name="activities"></a>
                                        <h3><?php echo $certification_service_type?> Activities</h3>
                                        <p><?php echo $activities_comment?></p>
                                    <?php } ?>
                                    <a href="#enquire" data-target="#userEnquiryForm" data-toggle="modal" id="enquirebutton2">
                                        <?php 
                                        if(!empty($vacancy2)) { 
                                            echo  $vacancy2 ;                                            
                                        } else { 
                                            echo "Enquire about availability";                                            
                                        } ?> 
                                    </a>                     
                                    <?php if($opt_email2==1) { ?>
                                        <a href="#Questions" id="decriptionQuestionLink">Ask a Question</a> 
                                    <?php } ?></p>
                                </div>
                                
                            </div>
							<?php 
							
							if( count($data[0]) > 0 ){ ?>
                            <div id="gallery" class="list-details-section">
                                <h4>Gallery </h4>
                                <!--Carousel Wrapper-->
                                <div id="carousel-thumb" class="carousel slide carousel-fade carousel-thumbnails list-gallery" data-ride="carousel">
                                    <!--Slides-->
                                    <div class="carousel-inner" role="listbox">
									<?php  
									$gal_active=0;
									$gal_thumb_html='';
									foreach ($data as $key => $record) 
									{
										
                         $pic_description=$record['title'];
                         $fileImage =  HOME_PATH.$imagepath.'/thumb_' . $record['file'];
                         $fileImageSrc = HOME_PATH .$imagepath.'/thumb_' . $record['file'];
                         $fileImageView = HOME_PATH .$imagepath.'/' . $record['file'];
                         //$_SERVER['DOCUMENT_ROOT']
                         $_SERVER['DOCUMENT_ROOT'].'/'.$imagepath.'/'.$record['file'];
                          if(file_exists($_SERVER['DOCUMENT_ROOT'].'/'.$imagepath.'/'.$record['file']))
						  { 
							$gal_thumb_html.='<li data-target="#carousel-thumb" data-slide-to="'.$gal_active.'"><img class="img-fluid d-block w-100" src="'.$fileImageView.'"></li>';
					  ?>
                         
						  <div class="carousel-item <?php if($gal_active=="0"){echo "active";}?>">
                                            <img class="d-block w-100" src="<?php echo $fileImageView;?>" alt="slide">
                                        </div>
										
                         <?php } 
						 $gal_active++;
						 } ?>
                                        
                                         
                                    </div>
                                    <!--Controls starts-->
                                    <a class="carousel-control-prev" href="#carousel-thumb" role="button" data-slide="prev">
                                        <span class="ion-arrow-left-c" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="carousel-control-next" href="#carousel-thumb" role="button" data-slide="next">
                                        <span class="ion-arrow-right-c" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                    <!--Controls ends-->
                                    <ol class="carousel-indicators  list-gallery-thumb">
									<?php echo $gal_thumb_html;?>
                                         
                                    </ol>
                                </div>
                                <!--/.Carousel Wrapper-->
                            </div>
							 <?php } ?>
							 <?php if($opt_email2==1){ ?>
                            <div id="price" class="list-details-section">
                              <h2><i class="icofont icofont-question-circle"></i> Questions and Answers</h2>
                                        
                                       <?php  if(isset($_SESSION['csUserName']))
                                             {
                                               if(isset($product_id))
                                               { 
                                                   $question=mysqli_query($db->db_connect_id, "select * from ad_question_ans where product_id='$product_id'");
                                                   while($ques_answer=mysqli_fetch_array($question))
                                                   {    
                                                    $ques_time=$ques_answer['qustion_time'];
                                                    $ques_answer=$ques_answer['answer_time'];
                                                    $ques_answer1=date("d-M-Y , g:i A",strtotime($ques_answer));
                                                    $ques_time1=date("d-M-Y , g:i A",strtotime($ques_time));
                                                    ?>
                                                    <p><strong>Q:-&nbsp; <?php echo $ques_answer['question_title'];?> </strong></p>
                                                    <p><strong>Date &nbsp; <?php echo $ques_time1;?> </strong></p>
                                                    <p><strong>Ans:-</strong><?php echo $ques_answer['answer'];?> </p>
                                                    <p><strong>Date:-</strong><?php echo $ques_answer1;?> </p>
                                             <?php }}} ?>
                                              
                                             <?php if(isset($_SESSION['SupUserName'])){   
                                                $_SESSION['SupUserName'];
                                                $_SESSION['userId'];
                                                $question=mysqli_query($db->db_connect_id, "select * from ad_question_ans where supplier_id={$_SESSION['userId']} AND product_id='$product_id' ");
                                                while($ques_answer=mysqli_fetch_array($question))
                                                   {
                                                         $q_id=$ques_answer['id'];
                                                         echo '<p><strong>Q:-&nbsp; '. $ques_answer["question_title"].'</strong></p>';
                                                        if(!empty($ques_answer['answer']))
                                                        {
                                                            echo '<p><strong>Ans:-</strong> '.$ques_answer["answer"].' </p>';
                                                        }
                                                        else
                                                        { 
                                                           echo '<form method="post" id="frm'.$q_id.'">
                                                           <textarea class="form-control" rows="3" name="description_name" id="ques_des"></textarea><br/>                                                           <input type="hidden" id="ans_id"   name="'.$q_id.'" value="'.$q_id.'"/>
                                                           <input type="button" class="ans_button" name="" id="qb'.$q_id.'"  value="Submit"/>
                                                           </form>';
                                                        }
                                                  } 
                                               }
                                               ?>
                                               
                                           <?php if(!isset($_SESSION['csUserName'])){ ?>
                                               <p>Want to ask a Question? Simply<a href="<?php echo HOME_PATH?>login"> [Log in]</a> or <a href="<?php echo HOME_PATH?>register">[Create an account]</a></p>
                                           <?php  }else{ ?>
                                            <a data-toggle="collapse" href="#askquestion" id="questionanchor">Ask a Question</a>  
                                            <form id="askquestion" class="question-form collapse" method="post">
                                            <input type="hidden" name="user_name" class="user_name1" value="<?php echo $_SESSION['csUserName'];?>" id="quesuname">
                                            <label for="exampleInputEmail1">Title</label>
                                            <input type="text" class="form-control" id="que_title" name="title" >
                                            <label for="exampleInputEmail1">Description</label>
                                            <textarea class="form-control" rows="3" name="description_name" id="ques_des"></textarea><br/> 
                                            <input type="button" class="btn btn-danger" name="" id="ques_button" value="Submit">
                                            </form> 
                                            <?php  } ?>
                                            <span id="message_login"></span>
                                
                            </div>
							<?php }?>
							<div id="Contact" class="list-details-section <?php if(count($data[0]) > 0 ){ ?>mar-top-80<?php }?>">
							<h3><i class="icofont icofont-map"></i> Address</h3>
							 <div class="row"> 
							 <div class="col-md-6">     
                                        <?php  if(isset($first_name) && isset($last_name)  && !empty($first_name)  && !empty($last_name)) {?><p><?php echo $first_name?> <?php echo $last_name?><br><?php  } ?>
                                            <?php  if(isset($position) && !empty($position)) { ?><span><?php echo $position?></span><br><?php  } ?>
                                            <?php  if(isset($phone) && !empty($phone)) { ?> 
                                            <section class="action action-show-number">                                        
                                                <a class="button standard action-call phone-list" id="show_no">
                                                    <i class="icofont icofont-phone"></i><span>Show number</span>
                                                </a>
                                                <a class="button standard action-call phone-list" id="hide_no" style="display:none;">
                                                    <i class="icofont icofont-phone"></i><span>Hide number</span>
                                                </a>
                                                <p class="numbers-container">                                            
                                                    <a href="tel:<?php echo $phone?>" class="button">
                                                        <span><i class="ion-ios-telephone"></i><?php echo $phone?></span>
                                                    </a>                                            
                                                </p>
                                            </section>
                                            <?php  } ?>
											</div> <div class="col-md-6">     
                                            <?php  if(isset($title) && !empty($title)) { ?><strong><?php echo isset($title) ? stripslashes($title) : ''; ?></strong><br><?php  } ?>
                                            <?php  if(isset($address) && !empty($address)) { ?><span itemprop="streetAddress"><i class="ion-ios-location"></i> <?php echo $address?></span><br><?php  } ?>
                                            <?php  if(isset($address_suburb) && !empty($address_suburb)) { ?><span itemprop="addressLocality"><?php echo $address_suburb?></span><br><?php  } ?>
                                            <?php  if(isset($address_city) && !empty($address_city)) { ?><span itemprop="addressCountry"><?php echo $address_city?>,<?php echo $zip?></span><br><?php  } ?>
                                            <span>New Zealand</span>
                                            &nbsp;<button id="click_showmap">Show Map</button></p>
							</div>
							<div id="showmap" class="col-sm-12">
                                    
                                    <?php 
                                        $address = $map_address; 
                                        $prepAddr = str_replace(' ','+',$address);
                                        //echo $prepAddr;
                                    ?>
                                    <div class="clearfix"></div>
                                    </div>
							</div>
							<div class="col-sm-12">
                                        <span><a href="#enquire" data-target="#userEnquiryForm" data-toggle="modal" id="enquirebutton3"><?php if(!empty($vacancy2)) { echo  $vacancy2 ;} else { echo "Enquire about availability";} ?> </a>&nbsp;&nbsp;&nbsp;
                                        
                                        <a data-toggle="collapse" href="#contact-tab" id="sendamessage">Send a message</a></span>
                                        
                                        <p id="supp_message_result" class="supp-message-c"></p>
                                        <div class="contact-tab panel-collapse collapse" id="contact-tab">
       
                                        <form method="post" id="frm_message" name="frm_mess">
                                            <input type="name" pattern=".{3,}" id="cust_name" name="cust_name" placeholder="Your Name" required title="Three or more characters">
                                            <input type="email" id="cust_email" name="cust_email" placeholder="Email" required>
                                            <input type="tel" id="cust_mob" name="cust_mob" placeholder="Mobile">
                                            <input type="hidden" name="supp_id" value="<?php echo $supp_id1?>"/>                                           
                                            <textarea rows="3"  value="" id="supp_message" name="supp_message" placeholder="Your Message"></textarea>
                                            <input type="submit" class="btn btn-danger" id="send_message1" value="Submit"> 
                                        </form>
                                        <button style="display: none" type="button" id="contact_query_sent" disabled="disabled" class="btn btn-danger full-width">Sending Now<span> <img src="../../assets/img/72.gif"> </span></button>
                                        <button style="display: none" type="button" id="contact_query_after_sent" disabled="disabled" class="btn btn-danger full-width">Message Sent!</button>
                                                                                
                                        </div>
                                    </div>
							</div>
							
                           
							
							 <?php  if($certification_service_type=='Aged Care'){?>
                                <div id="dhbDetails" class="list-details-section mar-top-10">
                                    <h3>DHB Audit Details</h3>
                                    <a href="#Audits">Further Rest Home Certification and Audits Information</a>.
                                    
                                        <p><strong><?php echo isset($title) ? stripslashes($title) : ''; ?></strong><br><?php  if($dhb_name){?><?php echo $dhb_name?><br><?php  }?>
                                    <?php  if($certification_period=='12a'){?>Audit Certification period: <strong>12 months</strong> (<font color='grey'>due to new manager employed</font>)<br><?php }?>    
                                    <?php  if($certification_period=='12b'){?>Audit Certification period: <strong>12 months</strong> (<font color='grey'>due to new ownership</font>)<br><?php }?>    
                                    <?php  if($certification_period=='12c'){?>Audit Certification period: <strong>12 months</strong> (<font color='grey'>due to new level of care introduced</font>)<br><?php }?>    
                                    <?php  if($certification_period=='12d'){?>Audit Certification period: <strong>12 months</strong> (<font color='grey'>due to being a new facility</font>)<br><?php }?>    
                                    <?php  if($certification_period=='12'){?>Audit Certification period: <strong><?php echo $certification_period?> months</strong> (<font color='red'>Poor</font>)<br><?php }?>    
                                    <?php  if($certification_period=='24'){?>Audit Certification period: <strong><?php echo $certification_period?> months</strong> (<font color='orange'>Fair</font>)<br><?php }?>    
                                    <?php  if($certification_period=='36'){?>Audit Certification period: <strong><?php echo $certification_period?> months</strong> (<font color='blue'>Good</font>)<br><?php }?>    
                                    <?php  if($certification_period=='48'){?>Audit Certification period: <strong><?php echo $certification_period?> months</strong> (<font color='green'>Very Good</font>)<br><?php }?>    
                                    <?php  if($certification_period=='60'){?>Audit Certification period: <strong><?php echo $certification_period?> months</strong> (<font color='green'>Excellent</font>)<br><?php }?>    
                                    <?php  if($certificate_license_end_date && $certificate_license_end_date!='0000-00-00'){?>Certificate renewal date: <strong><?php echo date('jS F, Y',strtotime($certificate_license_end_date))?></strong><br><?php }?>
                                    <?php  if($current_auditor){?>Auditor: <?php echo $current_auditor?><br>
                                    <?php  if($audit_url){?><a href="<?php echo $audit_url?>" target="_blank">Click here to view latest audit findings</a>
                                    <?php  } }?>
                                    </p><div class="headline"></div>
                                    <h3>Why we have Rest Home Audits </h3> 
                                    <p id="Audits">All rest homes and aged residential care facilities are certified and audited to ensure they:<br>

<ul><li>provide safe, appropriate care for their residents</li>
<li>meet the standards set out in the Health and Disability Services (Safety) Act 2001.</li></ul>
<h3>Types of audit and when they happen</h3>

<strong>Certification / Surveillance Audits</strong><br>

<b>Certification audits</b> happen every 1–4 years. After the audit, rest homes are certified for a set period of time (the exact length depends on how well the rest home performed at the certification audit). Once this time is up, the rest home must be re-audited and its certification renewed.<br>
<br>
An unannounced spot audit (also called a surveillance audit) happens around the middle of a rest home’s certification period. The spot audit ensures progress has been made on outstanding areas identified in the earlier certification audit and that standards haven’t slipped.<br>
<br>
In addition to audits, rest homes have to report to their DHB on how they are addressing issues found at audit. These improvements are then verified at the next audit event.<br>
<br>
<strong>Other types of audit</strong><br>
<br>
Provisional audits happen when a provider purchases a certified rest home from another provider.<br>
<br>
Partial provisional audits happen when a provider wants to add services to their certificate (eg, a rest home adding hospital-level care), when a new rest home is built, or when a provider adds capacity or reconfigures their services (eg, builds a new wing, upgrades rooms).  Before 2014, audits for adding capacity or reconfiguring services were referred to as verification audits.<br>
<br>
<strong>Ministry inspections</strong><br>
<br>
Rest homes may have unannounced inspections by the Ministry under the Health and Disability Services (Safety) Act 2001 in the event of a serious complaint.<br>
<br>
<strong>DHB issues-based audits</strong><br>
<br>
DHBs can conduct issues-based audits under the Aged Related Residential Care Contract. For information on these audits please contact the relevant DHB.</p>
                                    <div class="clearfix"></div>
                                </div>
                                <?php  } ?>
							
							
                           
							 <?php  if($certification_service_type=='Retirement Village'){?>
							 <div class="clearfix"></div>
                                <div id="otherDetails" class="list-details-section mar-top-10 contact-form">
                                    <section class="col-sm-12" style="border-bottom: 1px solid rgba(255, 255, 255, 0.48);margin-top: 15px; padding-bottom: 15px; text-align: center;">
                                        <a href="https://www.agedadvisor.nz/Compare-Retirement-Villages-Entry-Age-Costs-Fees-Conditions?city=<?php echo $city_id?>&sort=mostReviews&amt=150000" target="blank">See how this compares with other facilities!</a>
                                    </section>
                                    
                                    <section class="col-sm-12" style="border-bottom: 1px solid rgba(255, 255, 255, 0.48);">
                                        <section class="col-sm-6">                                                    
                                            <section  class="provider-heading-rating" style="margin-top: 15px;margin-bottom: 8px;">Fee model <i class="icofont icofont-question-circle" data-original-title="ORA/LTO - Occupational Rights Agreement / Licence To Occupy is the most popular model currently used in NZ, where you pay an upfront amount and then receive this back less a deferred fee. Other models include Outright Purchase or Rental." data-toggle="tooltip" title=""></i></section>
                                        </section>
                                        <section class="col-sm-6">                                                    
                                            <section  class="provider-heading-rating" style="margin-top: 15px;margin-bottom: 8px;"><?php echo $fee_model?></section>
                                        </section>
                                    </section> 

                                    <section  class="col-sm-12" style="padding-top: 5px;padding-bottom: 5px;border-bottom: 1px solid rgba(255, 255, 255, 0.48);">
                                        <section class="col-sm-6">
                                            <section  class="provider-main-minmum-age">Minimum Age of Entry <i class="icofont icofont-question-circle" data-original-title="Each village normally has a minimum age to be a resident. The age stated is based on a single person. Where a 'couple' (two people) has one person over the minimum age and another under, then we recommend you contact the provider directly" data-toggle="tooltip" title=""></i> </section>
                                        </section>
                                        <section class="col-sm-6">
                                            <section  class="provider-main-minmum-age"><?php echo $min_age?></section>
                                        </section>
                                    </section>

                                     

                                    <section  class="col-sm-12" style="border-bottom: 1px solid rgba(255, 255, 255, 0.48);">
                                        <section class="col-sm-6">
                                            <section  class="provider-heading-name" style="height: 86px;">Lowest Known Price</section>
                                        </section>
                                        <section class="col-sm-6">
                                            <section  class="provider-heading-name"><?php echo $lowest_known_price?></section>
                                            <section  class="provider-heading-name"><?php echo $lowest_known_price_dwellings?></section>
                                        </section>
                                    </section>

                                    <section  class="col-sm-12" style="border-bottom: 1px solid rgba(255, 255, 255, 0.48);">
                                        <section class="col-sm-6">
                                            <section  class="provider-heading-name" style="height: 86px;">Highest Known Price</section>
                                        </section>
                                        <section class="col-sm-6">
                                            <section  class="provider-heading-name" ><?php echo $highest_known_price?> </section>
                                             <section  class="provider-heading-name" ><?php echo $highest_known_price_dwellings?> </section>
                                        </section>                                                
                                    </section>

                                    <section  class="col-sm-12" style="border-bottom: 1px solid rgba(255, 255, 255, 0.48);">
                                        <section class="col-sm-6">
                                            <section  class="provider-main-capital" style="height: 61px;">Capital Gain <i class="icofont icofont-question-circle" data-original-title="Who receives the increase between the purchase price of the property and the sale price on exiting. Most retirement villages do not pass on any capital gain, however some do." data-toggle="tooltip" title=""></i> </section>
                                        </section>
                                        <section class="col-sm-6">
                                            <section  class="provider-main-capital" style="height: 61px;"> <?php echo $capital_gain?></section>
                                        </section>                                                 
                                    </section>
                                    <section  class="col-sm-12" style="border-bottom: 1px solid rgba(255, 255, 255, 0.48);">
                                        <section class="col-sm-6">
                                            <section  class="provider-main-capital" style="height: 61px;"> Capital Loss (if any) <i class="icofont icofont-question-circle" data-original-title="Who is liable for any loss in capital value on sale of unit." data-toggle="tooltip" title=""></i></section>
                                        </section>
                                        <section class="col-sm-6">
                                            <section  class="provider-main-capital" style="height: 61px;"> <?php echo $capital_loss?></section>
                                        </section>                                                 
                                    </section>

                                    <section  class="col-sm-12" style="border-bottom: 1px solid rgba(255, 255, 255, 0.48);">
                                        <section class="col-sm-6">
                                            <section  class="provider-section-capital" style="padding-top: 5px;padding-bottom: 5px;min-height: 86px;">
                                                Maintenance Fees Starting from <i class="icofont icofont-question-circle" data-original-title="This is the maintenance that will apply Weekly" data-toggle="tooltip" title=""></i>                                                        
                                                <div class="clearfix"></div>
                                                <section  class="provider-section-drop-list" style="display:none;">
                                                    <section  style="height: 60px">Yearly Maintenance Cost</section>
                                                </section>
                                            </section>
                                        </section>
                                        <section class="col-sm-6">
                                            <section  class="provider-section-capital"><?php echo $maintenance_fees_starting_from?></section>
                                            <section><?php echo $period?></section>
                                                <div class="clearfix"></div>
                                                <section  class="provider-section-drop-list" style="display:none;">
                                                    <section  style="height: 60px">Yearly Maintenance Cost</section>
                                                </section>
                                            </section>
                                        </section>
                                    </section> 

                                    <section  class="col-sm-12" style="border-bottom: 1px solid rgba(255, 255, 255, 0.48);">
                                        <section class="col-sm-6">
                                            <section  class="provider-heading-name" style="height: 60px;">Maintenance Fee Increases?</section>
                                        </section>
                                        <section class="col-sm-6">
                                            <section  class="provider-heading-name" style="height: 60px;"><?php echo $maintenance_fee_increases?></section>
                                        </section>                                                  
                                    </section>

                                    <section  class="col-sm-12" style="border-bottom: 1px solid rgba(255, 255, 255, 0.48);">
                                        <section class="col-sm-6">
                                            <section  class="provider-footer-facilties" style="height: 61px;">Extra Costs/Fees</section>
                                        </section>
                                        <section class="col-sm-6">
                                            <section  class="provider-footer-facilties" style="height: 61px;"><?php echo $extra_costs_fee?></section>
                                        </section>                                                
                                    </section>         
                                    <section  class="col-sm-12" style="border-bottom: 1px solid rgba(255, 255, 255, 0.48);">
                                        <section class="col-sm-6">
                                            <section  class="provider-footer-facilties" style="height: 61px;"> No. of dwellings in village <i class="icofont icofont-question-circle" data-original-title="This is the total number of studio apartments, apartments, villas or townhouses that this village comprises of." data-toggle="tooltip" title=""></i></section>
                                        </section>
                                        <section class="col-sm-6">
                                            <section  class="provider-footer-facilties" style="height: 61px;"><?php echo $dwellings_in_village?></section>
                                        </section>                                                
                                    </section>

                                    <section  class="col-sm-12" style="border-bottom: 1px solid rgba(255, 255, 255, 0.48);">
                                        <section class="col-sm-6">
                                            <section  class="provider-footer-star" style="height: 61px;">Data current as of</section>
                                        </section>
                                        <section class="col-sm-6">
                                            <section  class="provider-footer-star" style="height: 61px;"><?php echo $data_current?></section>
                                        </section>                                                
                                    </section>

                                    <section  class="col-sm-12">
                                        <section class="col-sm-6">
                                            <section  class="provider-footer-providers" >Data supplied by</section>
                                        </section>
                                        <section class="col-sm-6">
                                            <section  class="provider-footer-providers" ><?php echo $data_supplied_by?></section>
                                        </section>                                                
                                    </section>   
                                    <p>&nbsp;</p>                                    
                                </div>                                
                                <?php  } ?> 
								
                            <div id="reviews" class="list-details-section mar-top-10 ">
                               
									  
									  <div class="headline"><a name="reviews"></a><h3>Reviews / Feedback for <?php echo $title?></h3>
                                      <div class="pull-right"><?php echo $reviewlink;?></div>
                                 <!--**************** code for thanks for review***********-->
                                      <div class="col-md-12 col-sm-12 col-xs-12 container_wrapper">
                                 <?php 
                                      $curr_date = date('Y-m-d H:i:s');
                                  if(isset($_SESSION['csUserName']) && isset($proId) && isset($supp_id1)){
                                        $cs_username= $_SESSION['csUserName'];  
                                        $visited_query="select MAX(created)  from ad_feedbacks where sp_id={$supp_id1} AND cs_id={$_SESSION['csId']} AND product_id={$proId}";
                                        $visit_query=mysqli_query($db->db_connect_id, $visited_query);
                                        if(mysqli_num_rows($visit_query)>0){
                                        while($visited_query_dt=mysqli_fetch_array($visit_query))
                                        {
                                           $visit_facility_review_dt=$visited_query_dt['MAX(created)'];
                                        }
        
                                            if($visit_facility_review_dt!='' && $visit_facility_review_dt!=NULL ){
                                             
                                            $dteStart = new DateTime($curr_date); 
                                            $dteEnd   = new DateTime($visit_facility_review_dt); 
                                             $dteDiff1  = $dteStart->diff($dteEnd); 
                                           
                                              $date_diff_hr= $dteDiff1->format("%H"); 
                                              $date_diff_day= $dteDiff1->format("%D"); 
                                       
                                       if($date_diff_hr<24 && $date_diff_day<1){
                                     ?>
                                   <div class="row">
                                  <div class="alert alert-success text-center" id="post_review">
                                  <a href="#" class="close" data-dismiss="alert">×</a>
                                     Thank you for posting this review. Can you comment on any other facilities? <a href="<?php echo HOME_PATH ?>rating-supplier"> Click here to post another review now.</a>
                                </div></div>
                                  <?php   }}}}?>
                                  <?php
                                    if(isset($_SESSION['csUserName']) && isset($proId) && isset($supp_id1)){
                                       $visit_query_data="select MAX(created)  from ad_feedbacks where sp_id={$supp_id1} AND cs_id={$_SESSION['csId']} AND product_id={$proId}";
                                        $visiting_query=mysqli_query($db->db_connect_id, $visit_query_data);
                                        if(mysqli_num_rows($visiting_query)>0){
                                        while($visited_query_dtt=mysqli_fetch_array($visiting_query))
                                        {
                                           $visit_facility_review_dtt=$visited_query_dtt['MAX(created)'];
                                        } 
                                         if($visit_facility_review_dtt=='' && $visit_facility_review_dtt==NULL ){
        
                                  ?>
                                 <div class="row">
                                    <div class="alert alert-success text-center" id="visited_facility">
                                     <a href="#" class="close" data-dismiss="alert">×</a>
                                     <p>Have you visited this facility? Help others who are looking for a place by posting a review on <a href="<?php echo HOME_PATH ?>rating?spId=<?php echo base64_encode($supp_id1)?>&prId=<?php echo base64_encode($proId)?>"><?php echo $title?></a> or <a href="<?php echo HOME_PATH ?>rating-supplier">review a different facility now.</a></p>
                                  </div>
                                   </div>
                                  <?php }} } 
                                  else {  if(!isset($_SESSION['csUserName'])){?>
                                  <div class="row">
                                    <div class="alert alert-success text-center" id="visited_facility">
                                    <a href="#" class="close" data-dismiss="alert">×</a>
                                     Have you visited this facility? Help others who are looking for a place by posting a review on <a href="<?php echo HOME_PATH ?>rating?spId=<?php echo base64_encode($supp_id1)?>&prId=<?php echo base64_encode($proId)?>"><?php echo $title?></a> or <a href="<?php echo HOME_PATH ?>rating-supplier">review a different facility now.</a>
                                  </div></div>
                                 <?php  }} ?>                                  
                             </div>
                            </div> 
							
								
								
								<div id="reviews" class="list-details-section pad-top-10">
                                 
                                <div class="review-box">
                                    <ul class="review_wrap">
                                         
                                        <?php checkfunc(md5($product_id)); ?>	
                                    </ul>
                                </div>
                            </div>
							<?php if(count($similarFacility)>1){ ?>
							<!--Similar Listing Starts-->
                        <div class="similar-listing">
                            <div class="similar-listing-title">
                                <h3>You might also like...</h3>
                            </div>
                            <div class="swiper-container similar-list-wrap">
                                <div class="swiper-wrapper">
								
								<?php
 foreach($similarFacility as $sFac){
                                    $fID = $sFac['id'];
                                    $fQuick_url = $sFac['quick_url'];
                                    $fTitle = $sFac['title'];
                                    $no_of_review= $sFac['no_of_review'];
                                    if(isset($sFac['file']) && !empty($sFac['file'])){
                                        $fImageSet = $_SERVER['DOCUMENT_ROOT'].'/admin/files/gallery/images/thumb_'.$sFac['file'];                                                                                
                                    }
                                    if(isset($sFac['facilitylogo']) && !empty($sFac['facilitylogo'])){
                                        $fLogo = $_SERVER['DOCUMENT_ROOT'].'/admin/files/product/logo/thumb_'.$sFac['facilitylogo'];
                                    }
                                    if(isset($sFac['headofclogo']) && !empty($sFac['headofclogo'])){
                                        $hLogo = $_SERVER['DOCUMENT_ROOT'].'/admin/files/user/thumb_'.$sFac['headofclogo'];
                                    }
                                    if (file_exists($fImageSet)) {
                                        $fImage = HOME_PATH. 'admin/files/gallery/images/'.$sFac['file'];   
                                    }elseif (file_exists($fLogo)) {
                                        $fImage = HOME_PATH. 'admin/files/product/logo/'.$sFac['facilitylogo'];
                                    }else{
                                        $fImage = HOME_PATH. 'admin/files/user/'.$sFac['headofclogo'];
                                    }
                                    $fSuburb = $sFac['address_suburb'];
                                    $fCity = $sFac['address_city'];
                                    $fOverall_rating = simlilarFacilityRating($fID);
                                    if ( isset($fImage) && !empty($fImage) ) 
									{
                                        $response_query = "SELECT `id`, `facility_responded` FROM ad_enquiries WHERE prod_id = $fID AND facility_followup_sent = 'y' ORDER BY time_of_enquiry DESC LIMIT 3";
                                        $response_query_res = mysqli_query($db->db_connect_id,  $response_query );
                                        $no_response_count = 0;
                                        while($response_query_data = mysqli_fetch_array($response_query_res)){
                                            if($response_query_data[ 'facility_responded' ] == 'n'){
                                                $no_response_count++;
                                            }
                                        }
                                        if($no_response_count>2){
                                            $fImage = '';
                                        }                                        
                                    $similarFacilityHtml = '<div class="swiper-slide similar-item">
                                        <img src="'.$fImage.'" alt="...">
                                        <div class="similar-title-box">
                                            <h5><a href="'.HOME_PATH. $fQuick_url.'">'.ucwords(strtolower($fTitle)).'</a></h5>
                                            
                                        </div>
                                        <div class="customer-review">
                                            <div class="rating-summary">
                                                 <div class="star-inactive user-icon1">
                                                                        <div class="star-active" style="width:'.$fOverall_rating.'%;"></div>                                                                     
                                                                    </div>
																	<div class="pull-right price-amt" style="color:#f15922;">'.round($fOverall_rating).'% <i class="icofont icofont-users"></i> '.$no_of_review.'  </div>
                                            </div>
                                            <p><i class="ion-ios-location"></i> '.$fSuburb.', '.$fCity.'</p>
                                        </div>
                                    </div> ';
                                    }else {
                                        $similarFacilityHtml = '';
                                    }
                                        echo $similarFacilityHtml;      
                                }								
								?>
                                                                  
                                  
                                 
                                </div>
                            </div>
                            <div class="slider-btn v3 similar-next"><i class="ion-arrow-right-c"></i></div>
                            <div class="slider-btn v3 similar-prev"><i class="ion-arrow-left-c"></i></div>
                        </div>
							<?php }?>
                        <!--Similar Listing ends-->

 <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                           <?php //include('add_breadcrumbs.php');?>
                            <p>&nbsp;</p>
                            <div class="well well-sm">
                                <p>Know someone that would like to place a review but doesn't have access to a computer?<br><a href="http://www.agedadvisor.nz/images/agedadvisor_Review_Sheet_A4_PRINT.pdf">Download a printable FREEPOST form here</a></p>
                            </div>                
                        </div>
						
                                 
                            </div>
                             
                        </div>
                        <!--Listing Details ends-->
                        <!--Similar Listing starts-->
                         
                        <!--Similar Listing ends-->
                    </div>
                    <div class="col-lg-4 col-md-12">
                        <div class="listing-sidebar">
						  <div class="reviewing-top">
                        <ul><?php
                            echo '<li><p>Quality of Care<span class="star-inactive"><span class="star-active" style="width:'.$quality_of_care.'%;"></span></span></p></li><li><p>Caring Staff<span class="star-inactive"><span class="star-active" style="width:'.$caring_staff.'%;"></span></span></p></li><li><p>Responsive Mgmt<span class="star-inactive"><span class="star-active" style="width:'.$responsive_management.'%;"></span></span></p></li><li><p>Outdoor Activity<span class="star-inactive"><span class="star-active" style="width:'.$trips_outdoor_activities.'%;"></span></span></p></li><li><p>Indoor Entertainment<span class="star-inactive"><span class="star-active" style="width:'.$indoor_entertainment.'%;"></span></span></p></li><li><p>Social Atmosphere<span class="star-inactive"><span class="star-active" style="width:'.$social_atmoshphere.'%;"></span></span></p></li><li><p>Enjoyable Food<span class="star-inactive"><span class="star-active" style="width:'.$enjoyable_food.'%;"></span></span></p></li>';
                        ?>
                        </ul>
                    </div>  
                    
                    <?php 
                        $vacancy_qyery= "select * from ad_products where id='$pr_id'" ; 
                        $vacancy=mysqli_query($db->db_connect_id, $vacancy_qyery);
                        while( $vacancy1=mysqli_fetch_array($vacancy))
                        {
                             $vacancy2=$vacancy1['vacancy'];
                             if(stristr($vacancy2, 'Not')){$show_enquiry_form=false;}
                             $ext_van_link=$vacancy1['vacancy_external_link'];
                        } 
                    ?>                                                              
                        <a href="<?php if($show_enquiry_form){echo '#enquire';}else{echo '#';}?>" data-target="#userEnquiryForm" data-toggle="modal"  id="enquirebutton" class="btn-block btn btn-danger" style="font-size:16px;font-weight: bold;">
                        <?php if(!empty($vacancy2)) { echo  $vacancy2 ;} else { echo "Enquire about availability";} ?> <i class="icofont icofont-question-circle" style="height:auto; line-height: normal;margin-right:-40px;" data-toggle="tooltip" title="Click to send an enquiry. Most facilities respond within 2 working days. If a facility is not responding (usually due to being full) then we will let you know or the button will have been deactivated."></i></a>
                    <div class="review-container">
                        <?php echo $review_friendly_badge?><?php echo $response_time_badge?><?php echo $response_rate_badge?><?php echo $most_review_badge?>
                    </div>
						</div>
                        <div class="listing-sidebar">                                                            
                       <?php					
								if($youtube_video_enable && $youtube_video){?>
								<div class="sidebar-widget info">
                 <div class="headline hidden-xs"><h3><?php echo $title?> Videos</h3></div>
                <div class="responsive-video"><a name="video"></a>
                
                <?php if(stristr($youtube_video, 'tube')){// YouTUBE VIDEO
                    ?><iframe width="100%" src="<?php echo $youtube_video?>" frameborder="0" allowfullscreen></iframe> 
                <?php }else if(stristr($youtube_video, 'vimeo')){// VIMEO VIDEO
	             // Fix for incorrect link to vimeo
	             $youtube_video=str_ireplace('https://vimeo.com/', 'https://player.vimeo.com/video/', $youtube_video) ;  

                ?><iframe src="<?php echo $youtube_video?>" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>                
                <?php }?>
                </div>   </div>            
                <?php  } ?>
                                
                      <div class="sidebar-widget left-map hidden-xs">          
                         
                        <?php if ($pid!=1){?><div id="map" class="map"></div><?php }?>
                        <?php echo $map_address_title;?>                           
					 <?php  						
							 if($ext_url_enable && $ext_url){
                                                               
                            //Fix url for missing http://
                            $vis_ext_url=$ext_url;
                            if(stristr($vis_ext_url, '/')){
                             $vis_ext_url = strstr($vis_ext_url, '/', true);
                            }
							
                            if(!stristr($ext_url, 'http')){$ext_url='http://'.$ext_url;}                           
                       ?>                      
						
						<div class="address">
                                    <span class="ion-android-globe"></span>
                                    <p><a href="<?php echo  $ext_url?>" target="_blank"><?php echo $vis_ext_url;?></a></p>
                                </div>
						
                       <?php } ?>
					    </div>
                           
                                
                                  <div class="panel-group" id="enquire">
                            
                            <div class="panel panel-default"  >
                                    <div class="panel-heading">
                                    
                                     <?php 
                                        $vacancy_qyery= "select * from ad_products where id='$pr_id'" ; 
                                        $vacancy=mysqli_query($db->db_connect_id, $vacancy_qyery);
                                        while( $vacancy1=mysqli_fetch_array($vacancy))
                                        {
                                             $vacancy2=$vacancy1['vacancy'];
                                             $ext_van_link=$vacancy1['vacancy_external_link'];
                                        }
                                        
//                if(empty($vacancy2) ||  $vacancy2=="Enquire about availability" || $vacancy2=="Vacancy soon"){ 
                                        if(1==1){ ?>
                                        <h4 class="panel-title"> 
                                           
                                            <a data-target="#userEnquiryForm" data-toggle="modal">
                                                <?php 
                                                    if(!empty($vacancy2)) { 
                                                        echo  $vacancy2 ;                                                        
                                                    } else { 
                                                        echo "Enquire about availability";
                                                        
                                                    } 
                                                ?> 
                                            </a>
                                        </h4> 
                                          <?php } else {?>
                                    
                                    <h4 class="panel-title">
                                        <?php if(!empty($ext_van_link)){?>
                                    <a  href="<?php echo $ext_van_link;?>" id="send-link"><?php  echo  $vacancy2;?></a> 
                                         <?php }else { echo  '';}?>
                                    </h4>
                                    <?php }?> 
 

                                       </div>
                                    <div id="collapse2" class="panel-collapse collapse">
                                        <div class="panel-body">
                                                <div id="vacanncy_form" class="vacanncy-txt-c"></div> 
                                        </div>
                                        <!-- end of vacancy code close here -->
                                        
                                    </div>
                                </div>
                                    
                              <?php 
                                    $appointment_qyery= "select * from ad_products where id='$pr_id'" ; 
                                    $appointmentday=mysqli_query($db->db_connect_id, $appointment_qyery);
                                     while( $appointmentday1=mysqli_fetch_array($appointmentday))
                                      {
                                          $appointment_day1=trim($appointmentday1['appointment_day']);
                                          $app = explode(",",$appointment_day1);
                                          $app = str_replace(' ', '', $app);
                                      } 
                                      $to_day =  new DateTime(); 
                                      $today = $to_day->format('Y-m-d');
                                       if (count(array_filter($app))>0)
									   {
                                        
                               ?>

                              <div class="panel panel-default" >
                                    <div class="panel-heading">
                                        <h4 class="panel-title"> <a data-toggle="collapse" href="#collapse1" id="requestviewinganchor">Request a Viewing</a> </h4>
                                    </div>
                                    <div id="collapse1" class="panel-collapse collapse">
                                        <div class="panel-body">
                                              <?php 
                                                    if(isset($_SESSION['csUserName']))
                                                    {     $cust_name= $_SESSION['csUserName'];
                                                    
                                                        $customer_detail=mysqli_query($db->db_connect_id, "select * from ad_users where user_name='$cust_name'");
                                                        while($cutomer_detail_data=mysqli_fetch_array($customer_detail))
                                                        {
                                                             $cust_detailid=$cutomer_detail_data['id'];
                                                        }
                                                    } 
                                                ?>
                                            <div id="message_appointment" class="vacanncy-txt-c"></div>
                                            <form method="post" name="req_form" id="req_form">
                                                <input type="hidden" value="<?php echo $pr_id ;?>" id="prod_id" name="prod_name"/>
                                                <input type="hidden" value="<?php echo $cust_detailid;?>" id="enquirer_id" name="customer_name"/>
                                                
                                                
                                                  <select name="day" class="app_day" id="app_day">
                                                    <option value="">Select Appointment Day</option>
                                                  <?php  foreach ($app as $appday) { 

                                                    
                                                    if(strtotime($appday) > strtotime($today) ){
                                                          
                                                          $timestamp = strtotime($appday);
                                                          $day = date('D', $timestamp);
                                                          $finalDayDate = $day.','. $appday;
                                                          echo '<option value="'.$finalDayDate.'">'.$finalDayDate.'</option>';
                                                      }
                                                  }

                                                  ?>
                                                  </select>
                                                  
                                                  <select  name="time" id="app_time" class="app_time">
                                                  <option value="">Select Time</option>
                                                  <option value="09:00am">09:00 am</option>
                                                  <option value="09:30am">09:30 am</option>
                                                  <option value="10:00am">10:00 am</option>
                                                  <option value="10:30am">10:30 am</option>
                                                  <option value="11:00am">11:00 am</option>
                                                  <option value="11:30am">11:30 am</option>
                                                  <option value="12:00pm">12:00 pm</option>
                                                  <option value="12:30pm">12:30 pm</option>
                                                  <option value="01:00pm">01:00 pm</option>
                                                  <option value="01:30pm">01:30 pm</option>
                                                  <option value="02:00pm">02:00 pm</option>
                                                  <option value="02:30pm">02:30 pm</option>
                                                  <option value="03:00pm">03:00 pm</option>
                                                  <option value="03:30pm">03:30 pm</option>
                                                  </select>
                                                  
                                                   <select  name="sat_time" id="app_sattime" class="app_sattime">
                                                  <option value="">Select Time</option>
                                                  <option value="09:00am">09:00 am</option>
                                                  <option value="09:30am">09:30 am</option>
                                                  <option value="10:00am">10:00 am</option>
                                                  <option value="10:30am">10:30 am</option>
                                                  <option value="11:00am">10:00 am</option>
                                                  <option value="11:30am">11:30 am</option>
                                                  </select>
                                                 
                                                <input type="text" id="req_uname" class="req_uname" placeholder="Name" required/>
                                                <input type="email" value="" placeholder="Email" name="req_uemail" id="req_uemail" required/> 
                                                <!-- <textarea rows="5"  value="" placeholder="Private massage" style="width:100%"></textarea>-->
                                                
                                              <input type="button" id="req_query" class="btn btn-danger full-width" value="Submit">
                                                <div class="clearfix"></div>
                                            </form>
                                           <!-- <button type="submit" id="req_query" class="btn btn-danger full-width">Submit</button>-->                                            
                                        </div>                                        
                                    </div>
                                </div>
                                <?php  } ?>
                              
                                <div class="clearfix"></div>
                            </div>
                            
							<?php if($data_count>0 ||  $data_count_event>0){?>
                                 
                            <div class="sidebar-widget">
                                <div class="business-time">
                                    <div class="business-title">
                                        <h6><i class="ion-android-alarm-clock"></i> Upcoming Events</h6>                                        
                                    </div>
                                     <div class="upcoming-events mt-15">
                            <div class="event-blog-container">
                                
                                    
                  <?php foreach($fetch_eventdate_res as $fetch_eventdate){              
                    $fetch_eventdate_start_event=$fetch_eventdate['start_event_date_time'];
                    $fetch_eventdate_end_event = $fetch_eventdate['end_event_date_time'];
                    $event_date= date("Y-m-d",strtotime($fetch_eventdate_start_event));
                    $start_event_fetch_date = date("d", strtotime($event_date));
                    $dayof_date=date("D",strtotime($event_date));
                    $start_event_fetch_month = date("m", strtotime($event_date));
                    $monthName = date("M", mktime(0, 0, 0, $start_event_fetch_month, 10));
                    $start_event_fetch_year = date("Y", strtotime($event_date));
                    ?>
                        <div class="event-blog-list-box design-2 right-date">
                            <div class="event-blog-date-box">
                                <div class="event-blog-date-month">
                                    <p><?php echo $dayof_date; ?>, <?php echo $start_event_fetch_date;?> <?php echo $monthName;?>, <?php echo $start_event_fetch_year;?></p>
                                </div>
                            </div>
                            <div class="event-blog-cont-box">
                                <div class="event-blog-cont-box-in">
                                    <ul class="event-list">
                     
                <?php 
                     /*$date_detail_data="SELECT  * FROM ad_add_event WHERE supplier_id='$supp_id1' AND  start_event_date_time >'$event_date' AND start_event_date_time < date_add('$event_date', INTERVAL 1 DAY)";*/

                        $date_detail_data="SELECT * FROM ad_add_event WHERE organizer_type ='$supp_id1' AND  organizer_id='$supp_id1' AND  showto_facility='-1'  AND status='1' AND deleted='0' AND  start_event_date_time >'$event_date' AND start_event_date_time < date_add('$event_date', INTERVAL 1 DAY)";
                        $event_detail_query = $db->sql_query($date_detail_data);
                        $event_detail_res = $db->sql_fetchrowset($event_detail_query);
                    foreach($event_detail_res as $event_detail)
                    {   $fetch_eventdate_title = $event_detail['event_title'];
                        $fetch_eventdate_start_event = $event_detail['start_event_date_time'];
                        $fetech_start_event_time = date('g:ia', strtotime($fetch_eventdate_start_event));
                        $fetch_eventdate_end_event = $event_detail['end_event_date_time'];
                        $fetech_end_event_time = date('g:ia', strtotime($fetch_eventdate_end_event));
                        $fetch_eventdate_id = $event_detail['id'];
                        $fetch_eventdate_event_description = $event_detail['event_description'];
                        $event_organizer_email = $event_detail['event_organizer_email'];
                        $event_organizer_name = $event_detail['event_organizer_name'];
                        $event_location = $event_detail['event_location'];
                        $event_location_query=mysqli_query($db->db_connect_id, "SELECT `title` FROM ad_cities WHERE `id`= $event_location");
                        while($event_location = mysqli_fetch_array($event_location_query)){
                            $event_location_city = $event_location['title'];
                        }
                    ?>         
                        <li>
                            <button class="addtocalendar atc-style-icon atc-style-menu-wb" id="atc_icon_calbw1" data-on-button-click="atcButtonClick" data-on-calendar-click="atcCalendarClick">
                                <i class="atcb-link" tabindex="1" id="atc_icon_calbw1_link">
                                    <img src="https://addtocalendar.com/static/cal-icon/cal-bw-01.png" width="32">
                                </i>
                            <var class="atc_event">
                                    <var class="atc_date_start"><?php echo $fetch_eventdate_start_event?></var>
                                    <var class="atc_date_end"><?php echo $fetch_eventdate_end_event?></var>
                                    <var class="atc_timezone">Pacific/Auckland</var>
                                    <var class="atc_title"><?php echo $fetch_eventdate_title?></var>
                                    <var class="atc_description"><?php echo $fetch_eventdate_event_description?></var>
                                    <var class="atc_location"><?php echo $event_location_city ?></var>
                                    <var class="atc_organizer"><?php echo $event_organizer_name?></var>
                                    <var class="atc_organizer_email"><?php echo $event_organizer_email?></var>
                                </var>
                            </button>
                            <p>
                                <a target="_blank" href="<?php echo HOME_PATH; ?>event-view/<?php echo  $fetch_eventdate_id;?>/<?php echo  $fetch_eventdate_title;?>"><?php echo $fetech_start_event_time;?>&nbsp;-&nbsp;<?php echo $fetech_end_event_time;?>
                                    <span><?php echo $fetch_eventdate_title;?></span>
                                </a>
                           
                            </p>
                        </li>
                        
                        <?php }
                        
                        $date_detail_data_admin="SELECT  * FROM ad_add_event WHERE organizer_type ='admin_2' AND status='1' AND deleted='0' AND showto_facility='1' AND start_event_date_time >'$event_date' AND start_event_date_time < date_add('$event_date', INTERVAL 1 DAY)";

                     $event_detail_query_admin = $db->sql_query($date_detail_data_admin);
                     $event_detail_res_admin = $db->sql_fetchrowset($event_detail_query_admin);
                    foreach($event_detail_res_admin as $event_detail_admin)
                    {   $fetch_eventdate_title_admin = $event_detail_admin['event_title'];
                        $fetch_eventdate_start_event_admin = $event_detail_admin['start_event_date_time'];
                        $fetech_start_event_time_admin = date('g:ia', strtotime($fetch_eventdate_start_event_admin));
                        $fetch_eventdate_end_event_admin = $event_detail_admin['end_event_date_time'];
                        $fetech_end_event_time_admin = date('g:ia', strtotime($fetch_eventdate_end_event_admin));
                        $fetch_eventdate_id_admin = $event_detail_admin['id'];
                        $fetch_eventdate_event_description_admin = $event_detail_admin['event_description'];
                        $event_organizer_email_admin = $event_detail_admin['event_organizer_email'];
                        $event_organizer_name_admin = $event_detail_admin['event_organizer_name'];
                        $event_location_admin = $event_detail_admin['event_location'];
                        $event_location_query_admin=mysqli_query($db->db_connect_id, "SELECT `title` FROM ad_cities WHERE `id`= $event_location");
                        while($event_location_admin = mysqli_fetch_array($event_location_query_admin)){
                            $event_location_city_admin = $event_location_admin['title'];
                        }
                    ?>         
                        <li>
                            <button class="addtocalendar atc-style-icon atc-style-menu-wb" id="atc_icon_calbw1" data-on-button-click="atcButtonClick" data-on-calendar-click="atcCalendarClick">
                                <i class="atcb-link" tabindex="1" id="atc_icon_calbw1_link">
                                    <img src="https://addtocalendar.com/static/cal-icon/cal-bw-01.png" width="32">
                                </i>
                            <var class="atc_event">
                                    <var class="atc_date_start"><?php echo $fetch_eventdate_start_event_admin?></var>
                                    <var class="atc_date_end"><?php echo $fetch_eventdate_end_event_admin?></var>
                                    <var class="atc_timezone">Pacific/Auckland</var>
                                    <var class="atc_title"><?php echo $fetch_eventdate_title_admin?></var>
                                    <var class="atc_description"><?php echo $fetch_eventdate_event_description_admin?></var>
                                    <var class="atc_location"><?php echo $event_location_city_admin ?></var>
                                    <var class="atc_organizer"><?php echo $event_organizer_name_admin?></var>
                                    <var class="atc_organizer_email"><?php echo $event_organizer_email_admin?></var>
                                </var>
                            </button>
                            <p>
                                <a target="_blank" href="<?php echo HOME_PATH; ?>event-view/<?php echo  $fetch_eventdate_id_admin;?>/<?php echo  $fetch_eventdate_title_admin;?>"><?php echo $fetech_start_event_time_admin;?>&nbsp;-&nbsp;<?php echo $fetech_end_event_time_admin;?>
                                    <span><?php echo $fetch_eventdate_title_admin;?></span>
                                </a>
                           
                            </p>
                        </li>

                        <?php } /*******************code of evebt admin list***************/?>

                    </ul>
                 </div>
              </div>
              <div class="clearfix"></div>
           </div>
        <?php  } ?>
                    <div class="clearfix"></div>
                                
                   <!-- <div class="next-days hide">-->
                    <div class="next-days">
               
                    <?php foreach($fetch_nexteventsdate_res as $fetch_nexteventdate)
                    {              
                        $fetch_nexteventdate_start_event=$fetch_nexteventdate['start_event_date_time'];
                        $fetch_nexteventdate_end_event = $fetch_nexteventdate['end_event_date_time'];
                        $nextevent_date= date("Y-m-d",strtotime($fetch_nexteventdate_start_event));
                        $start_nextevent_fetch_date = date("d", strtotime($nextevent_date));
                        $nextdayof_date=date("D",strtotime($nextevent_date));
                        $start_nextevent_fetch_month = date("m", strtotime($nextevent_date));
                        $nextmonthName = date("M", mktime(0, 0, 0, $start_nextevent_fetch_month, 10));
                        $start_nextevent_fetch_year = date("Y", strtotime($nextevent_date));
                    ?>
                        <div class="event-blog-list-box design-2 right-date">
                            <div class="event-blog-date-box">
                                <div class="event-blog-date-month">
                                    <p><?php echo $dayof_date; ?>, <?php echo $start_nextevent_fetch_date;?> <?php echo $nextmonthName;?>, <?php echo $start_nextevent_fetch_year;?></p>
                                </div>
                            </div>
                            <div class="event-blog-cont-box">
                                <div class="event-blog-cont-box-in">
                                    <ul class="event-list">
                     
                <?php 
                     //$next_date_detail_data="SELECT  ad_add_event.* FROM ad_add_event WHERE start_event_date_time >'$event_date' AND start_event_date_time < date_add('$nextevent_date', INTERVAL 1 DAY)";
                       $next_date_detail_data="SELECT  * FROM ad_add_event WHERE organizer_type ='$supp_id1' AND  organizer_id='$supp_id1' AND   showto_facility='-1' AND status='1' AND deleted='0' AND  start_event_date_time >'$nextevent_date' AND start_event_date_time < date_add('$nextevent_date', INTERVAL 1 DAY)";


                     $next_event_detail_query = $db->sql_query($next_date_detail_data);
                     $next_event_detail_res = $db->sql_fetchrowset($next_event_detail_query);
                    foreach($next_event_detail_res as $next_event_detail)
                    {   $next_fetch_eventdate_title = $next_event_detail['event_title'];
                        $next_fetch_eventdate_start_event = $next_event_detail['start_event_date_time'];
                    $next_fetech_start_event_time = date('g:ia', strtotime($next_fetch_eventdate_start_event));
                        $next_fetch_eventdate_end_event = $next_event_detail['end_event_date_time'];
                        $next_fetech_end_event_time = date('g:ia', strtotime($next_fetch_eventdate_end_event));
                        $next_fetch_eventdate_id = $next_event_detail['id'];
                        $next_fetch_eventdate_event_description = $next_event_detail['event_description'];
                        $next_event_organizer_email = $next_event_detail['event_organizer_email'];
                        $next_event_organizer_name = $next_event_detail['event_organizer_name'];
                        $next_event_location = $next_event_detail['event_location'];
        $next_event_location_query=mysqli_query($db->db_connect_id, "SELECT `title` FROM ad_cities WHERE `id`= $next_event_location");
                        while($next_event_location = mysqli_fetch_array($next_event_location_query)){
                            $next_event_location_city = $next_event_location['title'];
                        }?> 
                        <li>
                            <button class="addtocalendar atc-style-icon atc-style-menu-wb" id="atc_icon_calbw1" data-on-button-click="atcButtonClick" data-on-calendar-click="atcCalendarClick">
                                <i class="atcb-link" tabindex="1" id="atc_icon_calbw1_link">
                                   <img src="https://addtocalendar.com/static/cal-icon/cal-bw-01.png" width="32">
                                </i>
                            <var class="atc_event">
                                    <var class="atc_date_start"><?php echo $next_fetch_eventdate_start_event?></var>
                                    <var class="atc_date_end"><?php echo $next_fetch_eventdate_end_event?></var>
                                    <var class="atc_timezone">Pacific/Auckland</var>
                                    <var class="atc_title"><?php echo $next_fetch_eventdate_title?></var>
                                    <var class="atc_description"><?php echo $next_fetch_eventdate_event_description?></var>
                                    <var class="atc_location"><?php echo $next_event_location_city ?></var>
                                    <var class="atc_organizer"><?php echo $next_event_organizer_name?></var>
                                    <var class="atc_organizer_email"><?php echo $event_organizer_email?></var>
                                </var>
                            </button>
                            <p>
                                <a target="_blank" href="<?php echo HOME_PATH; ?>event-view/<?php echo  $next_fetch_eventdate_id;?>/<?php echo  $next_fetch_eventdate_title;?>"><?php echo $next_fetech_start_event_time;?>&nbsp;-&nbsp;<?php echo $next_fetech_end_event_time;?>
                                    <span><?php echo $next_fetch_eventdate_title;?></span>
                                </a>
                           
                            </p>
                        </li>
                        
                        <?php } ?>

                          <?php 
                       
                     $next_date_detail_data= "SELECT  * FROM ad_add_event WHERE organizer_type ='admin_2' AND  showto_facility='1' AND status='1' AND deleted='0'    AND  start_event_date_time >'$nextevent_date' AND start_event_date_time < date_add('$nextevent_date', INTERVAL 1 DAY)";
                     $next_event_detail_query = $db->sql_query($next_date_detail_data);
                     $next_event_detail_res = $db->sql_fetchrowset($next_event_detail_query);
                    foreach($next_event_detail_res as $next_event_detail)
                    {   $next_fetch_eventdate_title = $next_event_detail['event_title'];
                        $next_fetch_eventdate_start_event = $next_event_detail['start_event_date_time'];
                    $next_fetech_start_event_time = date('g:ia', strtotime($next_fetch_eventdate_start_event));
                        $next_fetch_eventdate_end_event = $next_event_detail['end_event_date_time'];
                        $next_fetech_end_event_time = date('g:ia', strtotime($next_fetch_eventdate_end_event));
                        $next_fetch_eventdate_id = $next_event_detail['id'];
                        $next_fetch_eventdate_event_description = $next_event_detail['event_description'];
                        $next_event_organizer_email = $next_event_detail['event_organizer_email'];
                        $next_event_organizer_name = $next_event_detail['event_organizer_name'];
                        $next_event_location = $next_event_detail['event_location'];
        $next_event_location_query=mysqli_query($db->db_connect_id, "SELECT `title` FROM ad_cities WHERE `id`= $next_event_location");
                        while($next_event_location = mysqli_fetch_array($next_event_location_query)){
                            $next_event_location_city = $next_event_location['title'];
                        }?> 
                        <li>
                            <button class="addtocalendar atc-style-icon atc-style-menu-wb" id="atc_icon_calbw1" data-on-button-click="atcButtonClick" data-on-calendar-click="atcCalendarClick">
                                <i class="atcb-link" tabindex="1" id="atc_icon_calbw1_link">
                                   <img src="https://addtocalendar.com/static/cal-icon/cal-bw-01.png" width="32">
                                </i>
                            <var class="atc_event">
                                    <var class="atc_date_start"><?php echo $next_fetch_eventdate_start_event?></var>
                                    <var class="atc_date_end"><?php echo $next_fetch_eventdate_end_event?></var>
                                    <var class="atc_timezone">Pacific/Auckland</var>
                                    <var class="atc_title"><?php echo $next_fetch_eventdate_title?></var>
                                    <var class="atc_description"><?php echo $next_fetch_eventdate_event_description?></var>
                                    <var class="atc_location"><?php echo $next_event_location_city ?></var>
                                    <var class="atc_organizer"><?php echo $next_event_organizer_name?></var>
                                    <var class="atc_organizer_email"><?php echo $event_organizer_email?></var>
                                </var>
                            </button>
                            <p>
                                <a target="_blank" href="<?php echo HOME_PATH; ?>event-view/<?php echo  $next_fetch_eventdate_id;?>/<?php echo  $next_fetch_eventdate_title;?>"><?php echo $next_fetech_start_event_time;?>&nbsp;-&nbsp;<?php echo $next_fetech_end_event_time;?>
                                    <span><?php echo $next_fetch_eventdate_title;?></span>
                                </a>
                           
                            </p>
                        </li>
                        
                        <?php }  /***********************end of code for next event for admin****************/ ?> 
                          
                    </ul>
                 </div>
              </div>
              <div class="clearfix"></div>
           </div>
        <?php } ?>
                              </div>
                                 </div>
                                <?php 
                               
                                $datetime1 = new DateTime($event_date);
                                $datetime2 = new DateTime($nextevent_date);
                                $interval = $datetime1->diff($datetime2);
                                $interval_day=$interval->format('%a%'); 
                                if($interval_day>=1){
                                ?>
                                <div class="show-more">
                                    <span>Show more</span>
                                </div>
                                <?php } ?>
								 	
                            </div>
                                </div>
                            </div>
							 <?php }?>
							 
							 <?php echo getRightBannerNew1($city_name); ?>
							 <!-- -->
							 <?php if(!empty($associatedFacilityType)) { ?>
							 <div class="sidebar-widget">
							  <div class="business-title">
							   <a name="images"></a>                                
                                <h4>Associated Facility</h4>                                        
                                    </div>
									
									<?php if(count($associatedFacilityType)){                                   
                                foreach($associatedFacilityType as $sFac){        
                                    $fID = $sFac['id'];
                                    $fQuick_url = $sFac['quick_url'];
                                    $fTitle = $sFac['title'];
                                    $no_of_review= $sFac['no_of_review'];
                                    if(isset($sFac['file']) && !empty($sFac['file'])){            
                                        $fImageSet = $_SERVER['DOCUMENT_ROOT'].'/admin/files/gallery/images/thumb_'.$sFac['file'];                
                                        $fImage = 'thumb_'.$sFac['file'];
                                    }
                                    $fSuburb = $sFac['address_suburb'];
                                    $fCity = $sFac['address_city'];
                                    $fOverall_rating = simlilarFacilityRating($fID);   
									 
									
									$similarFacilityHtml = '<div class="trending-place-item">';
									//if($fImage)
								if(file_exists($_SERVER['DOCUMENT_ROOT'].'/admin/files/gallery/images/'.$fImage))
									{
										$similarFacilityHtml .= '<div class="trending-img">
                                                    <img src="'.HOME_PATH. 'admin/files/gallery/images/'.$fImage.'" alt="" title="" />                                                  
                                                </div>';
									}
                                                
												
                                                $similarFacilityHtml .= '<div class="trending-title-box">
                                                    <h4><a href="single-listing-one.html">'.$fTitle.'</a></h4>
                                                    <div class="customer-review">
                                                        <div class="rating-summary float-left">
                                                             <div class="star-inactive user-icon1">
                                            <div class="star-active" style="width:'.$fOverall_rating.'%;"></div>         
                                                                                         
                                           
                                            </div>
                                                        </div>
                                                        <div class="review-summury float-right">
                                                            <p><a href="#">'.$no_of_review.' Reviews</a></p>
                                                        </div>
                                                    </div>
                                                    <ul class="trending-address">
                                                        <li><i class="ion-ios-location"></i>
                                                            <p>'.$fSuburb.', '.$fCity.'</p>
                                                        </li>
                                                        
                                                    </ul>
                                                     
                                                </div>
                                            </div>';									                       
							echo $similarFacilityHtml;
                                        
                                }
                            }?>
                                
                            </div>
							<?php }?>
							 <!-- -->
                            
                             
                    </div>
                </div>
            </div>
        </div>
        <!--Listing Details Info ends-->
		<span class="scrolltotop"><i class="ion-arrow-up-c"></i></span>
		
		<div id="jobVacancyModal" class="modal fade" role="dialog" style="z-index:99999">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Job Vacancy Confirmation</h5>
                </div>
                <div class="modal-body">
                    <p>This appears to be an employment request and will not be seen by the correct person.</p>
                    <p>If you are looking for work, click “Yes, I am looking for a job.”.</p>
                    <p>If this is an enquiry about availability then click “No. I’m not looking for a job.” to send.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-primary" id="sendUserEnquery">No. I’m not looking for a job.</button>
                    <button type="button" data-dismiss="modal" class="btn" id="openCantactTab">Yes, I am looking for a job.</button>
                </div>
            </div>
        </div>
    </div>
	
	<div id="userEnquiryForm" class="modal fade" role="dialog" style="z-index:9999">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Enquire about availability</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top:-21px;">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <?php 
                    if(isset($_SESSION['csUserName']) || isset($_SESSION['userId']))
                    {   
                        $cust_name = $_SESSION['csUserName'];
                        $cust_user_id = $_SESSION['userId'];
                        if(!empty($cust_name)){
                            $customer_detail = mysqli_query($db->db_connect_id, "select * from ad_users where user_name='$cust_name'");
                        } else {
                            $customer_detail = mysqli_query($db->db_connect_id, "select * from ad_users where id='$cust_user_id'");
                        }
                        
                        while($cutomer_detail_data=mysqli_fetch_array($customer_detail))
                        {
                             $cust_detailid = $cutomer_detail_data['id'];
                             $cust_username = $cutomer_detail_data['first_name']." ".$cutomer_detail_data['last_name'];
                             $cust_email = $cutomer_detail_data['email'];
                             $cust_phone = $cutomer_detail_data['phone'];
                        }
                    } 
                ?>
                <!-- div id="vacanncy_form" class="vacanncy-txt-c"></div -->
                <form method="post" name="vacancy1_form" id="vacancy1_form">
                    <?php if($show_enquiry_form){?>
                    <div class="modal-body">
                        <input type="hidden" value="<?php echo $pr_id ;?>" id="prod_id" name="prod_id"/>
                        <input type="hidden" value="<?php echo $cust_detailid;?>" id="enquirer_id" name="enquirer_id"/>
                        <input type="hidden" id="vacancy-button" class="vacancy-button" value="<?php if(!empty($vacancy2)){echo $vacancy2;} else { echo "Enquire about availability";} ?>"/>
                        <input type="text" value="<?php echo $cust_username;?>" pattern=".{3,}" id="vacanncy_uname" name="enquirer_name" placeholder="Name" required title="Three or more characters"/>
                        <p style="font-size: 14px;color:#868686; margin-bottom:20px;"><a id="enquirejob" href="#sendamessage">NOTE: For work related enquiries, click here.</a></p>
                        <input type="email" value="<?php echo $cust_email;?>" placeholder="Email" name="enquirer_email" id="vacanncy_uemail" required/>
                        <input type="tel" value="<?php echo $cust_phone; ?>" placeholder="Mobile/Phone" name="enquirer_tel" id="vacanncy_tel" required/>  
                        <textarea style="margin-bottom: 0px;" rows="4" cols="38" id="enq_specifics" name="enq_specifics" placeholder="Enquiry Specifics (if any)"/></textarea>

                        <input id="opt_in" style="width: 15px;margin-bottom: 0px;-moz-appearance: checkbox" type="checkbox" name="opt_in" checked="checked">
                        <span style="font-size: 13px;color:#868686; margin-bottom:20px;"> Please email me any updates.</span>
                        <div class="clearfix"></div>
                        <!--button type="submit" id="vacanncy_query" class="btn btn-danger full-width">Submit</button>
                        <button style="display: none" type="button" id="vacanncy_query_sent" disabled="disabled" class="btn btn-danger full-width">Sending Now<span> <img src="../../assets/img/72.gif"> </span></button>
                        <button style="display: none" type="button" id="vacanncy_query_after_sent" disabled="disabled" class="btn btn-danger full-width">Message Sent!</button -->
                       
                    </div>
                    <div class="modal-footer">
                        <button type="submit" id="vacanncy_query" class="btn btn-primary">Submit</button>
                        <button style="display: none" type="button" id="vacanncy_query_sent" disabled="disabled" class="btn btn-danger">Sending Now<span> <img src="../../assets/img/72.gif"> </span></button>
                        <button style="display: none" type="button" id="vacanncy_query_after_sent" disabled="disabled" class="btn btn-danger">Message Sent!</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                    <?php }?>
                </form>
            </div>
        </div>
    </div>
	
	 <?php if(count($associatedFacilityType) || count($similarFacility)){ ?> 
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content blue-light-gradient">                            
                <div class="provider-compair-heading">
                    <h2><i class="icofont icofont-hand-o-right" style="margin-right: 30px;"></i>Your enquiry is being sent .... <i class="icofont icofont-times-circle" aria-hidden="true"style="float: right; cursor: pointer;" data-dismiss="modal"></i></h2>
                </div>
                <div class="provider-compair-form">                                                                
                        <div class="form-group">
                            <p><strong>We can also send your enquiry to these facilities below.</strong> Simply untick the ones you're not interested in and click &quot;Send" or simply click &quot;No thanks&quot;.</p>

                        </div>
                    <form name="addon-fac" id="addon-facility">
                    <?php if(count($associatedFacilityType)){                                                                     
                    foreach($associatedFacilityType as $sFac){
                        $fID = $sFac['id'];
                        $sID = $sFac['supplier_id'];
                        $fQuick_url = $sFac['quick_url'];
                        $fTitle = $sFac['title'];                                                                    
                        $fSuburb = $sFac['address_suburb'];
                        $fCity = $sFac['address_city'];  
                        $no_of_enquiry= $sFac['no_of_enquiry'];
                        if( $no_of_enquiry > 0 ){
                            $enq_count = '';
                            $enq_count_msg = '<span><b>Enquiry previously sent</b><span>';
                        } else {
                            $enq_count = 'checked="checked"';
                            $enq_count_msg = '';
                        }
                        $associatedFacilityHtml = '<div class="tab-detail-content assoc" style="padding-top:15px; padding-bottom:15px;background: rgba(255,255,255,.8);">                                                                                                    
                                                        <div class="col-sm-12 col-md-12 col-xs-12">
                                                            <div class="col-sm-1 col-md-1 col-xs-1" style="padding-left: 0px;"> 
                                                                <div class="checkbox checkbox-success">
                                                                    <input type="checkbox" style="margin-top:15px;height:30px;width:37px;" name="extra_facilities_id[]" value="'.$fID.'" '.$enq_count.' />
                                                                    <label for="checkbox3"></label>
                                                                </div>                                                                                                                                                                                                                        
                                                            </div>
                                                            <div class="col-sm-11 col-md-11 col-xs-11">
                                                                <a href="'.HOME_PATH. $fQuick_url.'" target="_blank">                                                                                                        
                                                                    <span class="tital-color">'.$fTitle.'</span>
                                                                    <span>'.$fSuburb.', '.$fCity.'</span>                                                                                                                
                                                                </a>
                                                                '.($enq_count_msg!=''?$enq_count_msg:'').'
                                                            </div>
                                                        </div>                                                                                                                                                                                                                                                                                                                                                                                                              
                                                        <div class="clearfix"></div>                                                                                                
                                                    </div>';                                                                                  
                    echo $associatedFacilityHtml;
                    }}
                    if(count($similarFacility)){
                        foreach($similarFacility as $sFac){
                            $fID = $sFac['id'];
                            $sID = $sFac['supplier_id'];
                            $fQuick_url = $sFac['quick_url'];
                            $fTitle = $sFac['title'];
                            $fSuburb = $sFac['address_suburb'];
                            $fCity = $sFac['address_city'];                                                                        
                            $no_of_enquiry= $sFac['no_of_enquiry'];
                            if( $no_of_enquiry > 0 ){
                                $enq_count = '';
                                $enq_count_msg = '<span><b>Enquiry previously sent</b><span>';
                            } else {
                                $enq_count = 'checked="checked"';
                                $enq_count_msg = '';
                            }
                            $similarFacilityHtml = '<div class="tab-detail-content simil" style="padding-top:15px; padding-bottom:15px;background: rgba(255,255,255,.8);">                                                                                                    
                                                        <div class="col-sm-12 col-md-12 col-xs-12">
                                                            <div class="col-sm-1 col-md-1 col-xs-1" style="padding-left: 0px;">                                                                                                            
                                                                <div class="checkbox checkbox-success">
                                                                    <input type="checkbox" style="margin-top:15px;height:30px;width:37px;" name="extra_facilities_id[]" value="'.$fID.'" '.$enq_count.'/>
                                                                    <label for="checkbox3"></label>
                                                                </div> 
                                                            </div>
                                                            <div class="col-sm-11 col-md-11 col-xs-11">
                                                                <a href="'.HOME_PATH. $fQuick_url.'" target="_blank">                                                                                                        
                                                                    <span class="tital-color">'.$fTitle.'</span>
                                                                    <span>'.$fSuburb.', '.$fCity.'</span></a>
                                                                    <span>';
                                                                     if(simlilarFacilityRating($fID)==0)
{$similarFacilityHtml=$similarFacilityHtml.'Not Rated
<span class="text-hide" itemprop="ratingValue">0</span>';
}
else
{
$similarFacilityHtml=$similarFacilityHtml. OverAllNEWRatingProduct($fID);//.simlilarFacilityRating($fID).'%';
} 
if (overAllRatingProductCount($fID) != '') {$similarFacilityHtml=$similarFacilityHtml.' <a target="_blank" href="'.HOME_PATH.'search/viewReview/'.$fID.'/Reviews-for-'.str_ireplace('search/', '', $fQuick_url).'"><span itemprop="reviewCount">'.overAllRatingProductCount($fID).' Reviews</span></a>';}
$similarFacilityHtml=$similarFacilityHtml.'                                                
                                                            '.($enq_count_msg!=''?$enq_count_msg:'').'
                                                            </div>
                                                        </div>                                                                                                                                                                                                                                                                                                            
                                                        <div class="clearfix"></div>                                                                                                
                                                    </div>';                                                                        
                                echo $similarFacilityHtml;      
                        }}?> 
                        <input type="hidden" value="" name="usname" class="usname"/>
                        <input type="hidden" value="" name="usphone" class="usphone"/>
                        <input type="hidden" value="" name="usmail" class="usmail"/>                                                                                                                                        
                        <input type="hidden" value="" name="enq_specs" id="enq_specs"/>
                        <div style='height:23px;'>&nbsp;</div>
                        <button type="button" class="btn btn-info" id="close-report" data-dismiss="modal" style="float:right;" id="cancel-button">No thanks</button> 
                        <button type="submit" class="btn btn-danger" id="submit-report" style="margin-right:15px; float:right;">Send to ticked facilities</button>
                        <div class="form-group" style="display:inline-block; float: right;">                                                                                                        
                            <p style="display: none" id="sendingmsg" class="btn btn-danger full-width">Sending Now<span> 
                                    <img src="../../assets/img/72.gif"> </span>
                                </p>
                                <p style="display: none" id="sentmsg" class="btn btn-danger full-width">Enquiry Sent!</p>                                                                                                        
                        </div>                                                                   
                        <div style='height:35px;'>&nbsp;</div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <?php } ?>  
		
		<script type="text/javascript">

$(document).ready(function(e) { 
    
    $('[data-toggle="tooltip"]').tooltip();
    
    $('#show_no').click(function(){
        var prod_id=$("#prod_id").val();
        var suppId = $('#supp_messid').val();
        $('#hide_no').show();
        $(this).hide();
        $('.numbers-container').show();
        $.ajax({
            type: "post",
            url: '<?php echo HOME_PATH ?>cron/ajax_show_no_analytics.php',
            data:{prId : prod_id,supp_id : suppId },
            success: function(result) 
            {}
        });
    });
    $('#hide_no').click(function(){
        $('#show_no').show();
        $(this).hide();
        $('.numbers-container').hide();
    });
    
    $('#datetimepicker1').datetimepicker({
    dayOfWeekStart : 1,
    lang:'en',
    inline:false,
    startDate:  new Date(),
    minDate:0
});  
    
    <?php if( count($data) > 0 ){ ?>
        $('.slider1').bxSlider({
            mode: 'fade',
            adaptiveHeight: true,
            captions: true,
            slideWidth: 750
        });
    <?php } ?>
    
    

    var count = 0;
        
    $('#requestlink,#contacttabrequestlink').click(function(e) {
        
        count++;
        
        if(count%2==0){
            $('#datetimepicker1').datetimepicker('hide');
            
        }else{
            $('#datetimepicker1').datetimepicker('show');
            
        }
    });
    
    $('.tab-detail div').not(':eq(0)').hide();
    
    $('.tab-button li:nth-child(1)').addClass('current-button');
    
    $('.tab-button li').click(function(){
        $(this).addClass('current-button');
        $(this).siblings().removeClass('current-button');
        var tab_index = $(this).index();
        
        $('.tab-detail div.tab-detail-content').eq(tab_index).show();
        $('.tab-detail div.tab-detail-content').eq(tab_index).children().show();
        $('.tab-detail div.tab-detail-content').eq(tab_index).siblings().hide();
        $('.tab-detail div.tab-detail-content').eq(tab_index).siblings().children().hide();
        
    });
    $(".show-more").click(function(){
        $(".next-days").slideToggle();
        
        $(".show-more span").text(function(i, text){
          return text === "Show more" ? "Show less" : "Show more";
        })
        
    });
 
 <?php if ($pid!=1){?>   
        var map;
        var contactMap;
        $(document).ready(function(){
           map = new GMaps({
            div: '#map',
            lat: -43.3744881,
            lng: 172.4662705,
            zoom: <?php echo $map_address_zoom?>
          });
          
    <?php if($map_address!=="New Zealand"){?>
            GMaps.geocode({
              address: '<?php echo $map_address?>',
              callback: function(results, status){
                if(status=='OK'){
                  var latlng = results[0].geometry.location;
                  map.setCenter(latlng.lat(), latlng.lng());
                  map.addMarker({
                    lat: latlng.lat(),
                    lng: latlng.lng()
                  });
                }
              }
            });
    <?php } ?>
    
    });<?php } ?>
});
   
</script>
              
<script type="text/javascript">
    $(document).ready(function() {     
        var link_url='<?php echo $ext_van_link; ?>';
        var vacancy_value='<?php echo $vacancy2;?>';        
        var page = 0;
        var li = '0_no';
        var data = '&id=<?php echo md5($product_id); ?>';       
        $('.read_more').click(function() {
            $(this).siblings('.feedback').toggle();
            $(this).text() == 'Hide' ? $(this).text('Read more') : $(this).text('Hide');           
            $(this).parent().parent().next('div.reads_more').slideToggle("slow", function() {             
            });
        });
    
    var sessionCookieShortList = $(document).find("#sessionCookieShortList").length;
    var sessionshort = $(document).find("#sessionShortList").length;
    if(sessionshort>0){
        $('#sessionShortList').html(<?php echo $_SESSION['shortlist_count']; ?>);
    }
    if(sessionCookieShortList>0){
        $('#sessionCookieShortList').html(<?php echo  $_SESSION['shortlist_cookie_count']; ?>);
    }
    
   $("#shortlist").click(function(){
         
      var suppId = $('#supp_messid').val();
      var prodId = $('#pro_fac_id').val();
       $.ajax({
        type: "POST",
        url: "<?php echo HOME_PATH ?>modules/search/shortlist.php",
        data:{spId : suppId,prId : prodId},
        success: function(result) 
        {   
            var sessionCookieShortList = $(document).find("#sessionCookieShortList").length;
            var sessionshort = $(document).find("#sessionShortList").length;
            if($.isNumeric(result)){
                <?php if(isset($_SESSION['csUserName'])){ ?>
                    if(sessionshort>0){
                        $('#sessionShortList').html(result);
                        $('#remove_shortlist').show();
                        $('#shortlist').hide();
                    }else{
                        $('#sessionShortList').html(<?php echo $_SESSION['shortlist_count']; ?>);
                        $('#remove_shortlist').show();
                        $('#shortlist').hide();
                    }
                <?php } else { ?>
                    if(sessionCookieShortList>0){
                        $('#sessionCookieShortList').html(result);
                        $('#remove_shortlist').show();
                        $('#shortlist').hide();
                    }else{
                        $('#sessionCookieShortList').html(<?php echo $_SESSION['shortlist_cookie_count']; ?>);
                        $('#remove_shortlist').show();
                        $('#shortlist').hide();
                    }
                <?php } ?>
            }
        }
     });      
    });
    
    $(".remove_shortlist").click(function(){
        var sp_id1=$("#supp_messid").val();
        var pro1=$("#pro_fac_id").val();
        var user1=$("#customer_user_name").val();
        var datastring="pro=" + pro1 +"&sp_id="+ sp_id1 + "&user_name=" + user1 ;
        var sessionCookieShortList = $(document).find("#sessionCookieShortList").length;
        var sessionshort = $(document).find("#sessionShortList").length;
        $.ajax({ 
            type:"POST",
            url:'<?php echo HOME_PATH ?>modules/shortlist/ajax_remove_shortlist.php', 
            data: datastring,
            success: function(result)
            {
                if($.isNumeric(result)){
                    <?php if(isset($_SESSION['csUserName'])){ ?>
                        if(sessionshort>0){
                            $('#sessionShortList').html(result);
                            $('#remove_shortlist').show();
                            $('#shortlist').hide();
                        }else{
                            $('#sessionShortList').html(<?php echo $_SESSION['shortlist_count']; ?>);
                            $('#remove_shortlist').show();
                            $('#shortlist').hide();
                        }
                    <?php } else { ?>
                        if(sessionCookieShortList>0){
                            $('#sessionCookieShortList').html(result);
                            $('#remove_shortlist').hide();
                            $('#shortlist').show();
                        }else{
                            $('#sessionCookieShortList').html(<?php echo $_SESSION['shortlist_cookie_count']; ?>);
                            $('#remove_shortlist').hide();
                            $('#shortlist').show();
                        }
                    <?php } ?>
                    
                }            
              }
        });
    });
    
    $("#decriptionQuestionLink").click(function(){
        $("#Questions").trigger('click');
    });

        $("#ContactLink").click(function(){
        $("#Contact").trigger('click');
    });
    
        $("#dhbDetailsLink").click(function(){
        $("#dhbDetails").trigger('click');
    });
    
        $("#DescriptionLink").click(function(){
        $("#Description").trigger('click');
    });



    $("#frm_message").submit(function(event){
        $("#send_message1").hide();
        $("#contact_query_sent").show();
        $.ajax({
            type: "post",
            url: '<?php echo HOME_PATH ?>/modules/search/ajaxsend_message.php?prod_id=<?php echo $pr_id; ?>',
            data: $(this).serialize(),
            success: function(result) 
            {
               $("#supp_message_result").html(result);
               if(result == "Your message has been successfully sent"){
                    $("#frm_message").hide('slow');   
                    $("#frm_message").find('#send_message1').hide('slow');   
                    $("#contact_query_after_sent").show();
                    $("#contact_query_sent").hide(); 
               }else{
                   $("#send_message1").show();
                   $("#contact_query_sent").hide();                             
                }
               setTimeout(function(){                  
                   $("#contact_query_after_sent").hide();
                   $("#send_message1").show();
                   $("#supp_message_result").html('');
                },70000);  
            }            
        });
        event.preventDefault();         
    });
    
    $("#ques_button").click(function(){
        var cusomer_username=$("#quesuname").val();
        var questitle=$("#que_title").val();
        var ques_des=$("#ques_des").val();
        var supp_id=$("#supp_messid").val();
        datastring="cusomer_username=" + cusomer_username + "&questitle=" +  questitle +   "&ques_des=" + ques_des +"&supp_id=" + supp_id  ;
        
        if(cusomer_username.length<0 || cusomer_username=='')
        {
            $("#message_login").html("Please <a href='<?php echo HOME_PATH ?>/login'>log<a> in to to view questions and answers on this facility.");
            var a =$(document).find('.question-form collapse').length;            
            $("a").fadeout();
        }
        else
        { 
        $.ajax({
            type: "post",
            url: '<?php echo HOME_PATH ?>/modules/search/ajaxquestion_answer.php?prod_id=<?php echo $pr_id; ?>',
            data: datastring,
            success: function(result) 
            {
               $("#message_login").html(result);
               var a =$(document).find('.question-form collapse').length;           
               $("a").hide();   
            }
            
         });
        }
    });
    
    $(".ans_button").click(function(){
        
      var frm_id= $(this).parent().attr("id");
      var q_id=$(this).siblings('#ans_id').attr('value');
       var ans=$(this).siblings('#ques_des').attr('value');
       
      datastring="frm_id=" + frm_id + "&q_id=" +  q_id + "&ans=" + ans; 
       
            $.ajax({
            type: "post",
            url: '<?php echo HOME_PATH ?>/modules/search/ajax_answer.php',
            data: datastring,
            success: function(result) 
            {
               
            }            
        });
    });  
    
    $("#v1").hide();
    $("#vacanncy_link").hide();
    $("#v-form").click(function(){
        $("#v1").show();
        $("#vacanncy_link").hide();
    });
    $("#v-link").click(function(){
      $("#vacanncy_link").show();
      $("#v1").hide();
    });
    
    $('#addon-facility').submit(function(event){ 
        var checked = $('#addon-facility input[type=checkbox]:checked').length;
        if(checked<=0){
            alert("Please select at least one facility!");
            return false;
        }else{                   
            $("#submit-report, #close-report").hide();
            $("#sendingmsg").show();
            $.ajax({
                type: "post",
                url: '<?php echo  HOME_PATH ?>/modules/search/extra_vacancy_send.php', 
                data: $("#addon-facility").serialize(),
                success: function(result)  
                {                  
                    $("#sendingmsg").hide();
                    $("#sentmsg").show();
                    setTimeout(function(){
                        $('#myModal').modal('hide');
                        $("#sentmsg").hide();
                        $("#submit-report, #close-report").show();
                        location.reload();
                    },2000)                
                }
            });
        }
        event.preventDefault();        
    }); 
    
    $("#vacancy1_form").submit(function(event){                
        $("#vacanncy_query").hide();
        $("#vacanncy_query_sent").show();
        
        var opt_in;
        if($('#opt_in').is(':checked')){
          opt_in = 1;
        }else{
          opt_in = 0;  
        }                       
        var vacanncy_uname=$("#vacanncy_uname").val();
        var vacancy_uemail=$("#vacanncy_uemail").val();
        var vacanncy_tel=$("#vacanncy_tel").val();                 
        var enq_specifics = $("#enq_specifics").val();
        
        $.ajax({
            type: "post",
            url: '<?php echo  HOME_PATH ?>/modules/search/ajax_check_send_enq.php', 
            data: $("#vacancy1_form").serialize(),
            success: function(resultenq)  
            {               
                var resdata = $.parseJSON(resultenq);
                var resenqdata = resdata.data; 
                //alert(resenqdata);
                if(resenqdata != 'ProdNotFound'){
                    
                    if (confirm("You have already sent an enquiry to this provider on the "+resenqdata+". Would you still like to send this enquiry?")) {
                        setTimeout(function(){
                            
                        },10000);    
                        $.ajax({
                            type: "post",
                            url: '<?php echo  HOME_PATH ?>/modules/search/ajax_vacancy.php', 
                            data: $("#vacancy1_form").serialize(),
                            success: function(result)  
                            {                                                       
                                if(result){                  
                                    //alert(result);
                                    if(result == 2){
                                        $('#jobVacancyModal').modal({
                                            backdrop: 'static',
                                            keyboard: false
                                        }).on('click', '#sendUserEnquery', function(e) {
                                            $.ajax({
                                                type: "post",
                                                url: '<?php echo  HOME_PATH ?>/modules/search/ajax_vacancy.php?test=0', 
                                                data: $("#vacancy1_form").serialize(),
                                                success: function(result)  
                                                {                                                       
                                                    if(result){
                                                        if($("#collapse2").find('.usname').length > 0 ){
                                                            $(".usname").val(vacanncy_uname);
                                                            $(".usmail").val(vacancy_uemail);
                                                            $(".usphone").val(vacanncy_tel);                        
                                                            $("#enq_specs").val(enq_specifics);
                                                        }                                                        
                                                        $("#vacanncy_form").html("Great! Your enquiry has been sent."); 
                                                        $("#vacanncy_query_after_sent").show();
                                                        $("#vacanncy_query_sent").hide();
                                                        $('#userEnquiryForm').modal('hide');
                                                        //$('#myModal').modal('show');
                                                        $('#myModal').modal({
                                                            backdrop: 'static',
                                                            keyboard: false
                                                        });
                                                        $('#vacancy1_form').get(0).reset();  
                                                    }else{
                                                        $("#vacanncy_form").html(result);
                                                        $("#vacanncy_query").show();
                                                        $("#vacanncy_query_sent").hide();                             
                                                    }
                                                    
                                                    if($("#collapse2").find('.usname').length > 0 ){
                                                        $(".usname").val(vacanncy_uname);
                                                        $(".usmail").val(vacancy_uemail);
                                                        $(".usphone").val(vacanncy_tel);                        
                                                        $("#enq_specs").val(enq_specifics);
                                                    }

                                                    $("#vacanncy_form").html("Great! Your enquiry has been sent."); 
                                                    $("#vacanncy_query_after_sent").show();
                                                    $("#vacanncy_query_sent").hide(); 
                                                    $('#userEnquiryForm').modal('hide');
                                                    //$('#myModal').modal('show');
                                                    $('#myModal').modal({
                                                        backdrop: 'static',
                                                        keyboard: false
                                                    }); 
                                                    $('#vacancy1_form').get(0).reset();  
                                                    
                                                    setTimeout(function(){
                                                        $("#vacanncy_form").html('');
                                                        $("#vacanncy_query_after_sent").hide();
                                                        $("#vacanncy_query").show();
                                                    },3000); 
                                                }
                                            });
                                        }).on('click', '#openCantactTab', function(e) {
                                            $("#vacanncy_query").show();
                                            $("#vacanncy_query_sent").hide();    
                                            $('#userEnquiryForm').modal('hide');
                                            $('#jobVacancyModal').modal('hide');
                                            $("#Contact").trigger('click');   
                                            $("#sendamessage").trigger('click');   
                                            setTimeout(function() {$("#cust_name").focus();},500);
                                            $("#collapse2").removeClass( "in" );  
                                            return;
                                        });
                                    }  
                                }else{
                                    $("#vacanncy_form").html(result);
                                    $("#vacanncy_query").show();
                                    $("#vacanncy_query_sent").hide();                             
                                }                                   
                            }
                        });
                    }  else {
                        $("#vacanncy_query").show();
                        $("#vacanncy_query_sent").hide();       
                        return;
                    }
                } else {
                    $.ajax({
                        type: "post",
                        url: '<?php echo  HOME_PATH ?>/modules/search/ajax_vacancy.php', 
                        data: $("#vacancy1_form").serialize(),
                        success: function(result)  
                        {                                                       
                            if(result){                  
                                //alert(result);
                                if(result == 2){
                                    $('#jobVacancyModal').modal({
                                        backdrop: 'static',
                                        keyboard: false
                                    }).on('click', '#sendUserEnquery', function(e) {
                                        $.ajax({
                                            type: "post",
                                            url: '<?php echo  HOME_PATH ?>/modules/search/ajax_vacancy.php?test=0', 
                                            data: $("#vacancy1_form").serialize(),
                                            success: function(result)  
                                            {                                                       
                                                if(result){
                                                    if($("#collapse2").find('.usname').length > 0 ){
                                                        $(".usname").val(vacanncy_uname);
                                                        $(".usmail").val(vacancy_uemail);
                                                        $(".usphone").val(vacanncy_tel);                        
                                                        $("#enq_specs").val(enq_specifics);
                                                    }
                                                                                                                           
                                                    $("#vacanncy_form").html("Great! Your enquiry has been sent."); 
                                                    $("#vacanncy_query_after_sent").show();
                                                    $("#vacanncy_query_sent").hide();     
                                                    $('#userEnquiryForm').modal('hide');
                                                    //$('#myModal').modal('show');
                                                    $('#myModal').modal({
                                                        backdrop: 'static',
                                                        keyboard: false
                                                    });
                                                    $('#vacancy1_form').get(0).reset();  
                                                }else{
                                                    $("#vacanncy_form").html(result);
                                                    $("#vacanncy_query").show();
                                                    $("#vacanncy_query_sent").hide();                             
                                                }
                                                
                                                if($("#collapse2").find('.usname').length > 0 ){
                                                    $(".usname").val(vacanncy_uname);
                                                    $(".usmail").val(vacancy_uemail);
                                                    $(".usphone").val(vacanncy_tel);                        
                                                    $("#enq_specs").val(enq_specifics);
                                                }

                                                $("#vacanncy_form").html("Great! Your enquiry has been sent."); 
                                                $("#vacanncy_query_after_sent").show();
                                                $("#vacanncy_query_sent").hide();   
                                                $('#userEnquiryForm').modal('hide');
                                                //$('#myModal').modal('show');
                                                $('#myModal').modal({
                                                    backdrop: 'static',
                                                    keyboard: false
                                                });
                                                $('#vacancy1_form').get(0).reset(); 
                                                
                                                setTimeout(function(){
                                                    $("#vacanncy_form").html('');
                                                    $("#vacanncy_query_after_sent").hide();
                                                    $("#vacanncy_query").show();
                                                },3000); 
                                            }
                                        });
                                    }).on('click', '#openCantactTab', function(e) {
                                        $("#vacanncy_query").show();
                                        $("#vacanncy_query_sent").hide();       
                                        $('#userEnquiryForm').modal('hide');
                                        $('#jobVacancyModal').modal('hide');
                                        $("#Contact").trigger('click');   
                                        $("#sendamessage").trigger('click');   
                                        setTimeout(function() {$("#cust_name").focus();},500);
                                        $("#collapse2").removeClass( "in" );  
                                        return;
                                    });
                                }                

                                if(result == 3){
                                    $("#vacanncy_query_after_sent").show();
                                    $('#userEnquiryForm').modal('hide');
                                    $("#vacanncy_query_sent").hide();       
                                    return;
                                }
                                
                            }else{
                                $("#vacanncy_form").html(result);
                                $("#vacanncy_query").show();
                                $("#vacanncy_query_sent").hide();                             
                            }   
                        }
                    });
                }
            }
        });       
        event.preventDefault();        
    });    
    $("#app_sattime").hide();
    $(".app_day").change(function(){
    var checkdate=$(this).val(); 
    var find_day=checkdate.split(','); 
    var app_findday=find_day[0];       
   if(app_findday=="Mon"|| "Tue" || "Wed" || "Thu" || "Fri")
   {    
       $("#app_time").show();
       $("#app_sattime").hide();      
       
   }
     if(app_findday=="Sat")
    {
          $("#app_sattime").show();
           $("#app_time").hide();
    }
  }); 
     
 }); 
 
 

window.addEventListener('load',function(){
 
 jQuery('#enquirebutton').click(function(){
 gtag('event','click',{
 'event_category':'enquiry',
 'event_label':jQuery('h2.headline.shortlist-heading[itemprop="name"]').text().split('FINALIST')[0].trim()
 })
 });
 jQuery('#requestviewinganchor').click(function(){ 
 gtag('event','click',{
 'event_category':'enquiry',
 'event_label':jQuery('h2.headline.shortlist-heading[itemprop="name"]').text().split('FINALIST')[0].trim()
 });
 });
 jQuery('#enquirebutton2').click(function(){
 gtag('event','click',{
 'event_category':'enquiry',
 'event_label':jQuery('h2.headline.shortlist-heading[itemprop="name"]').text().split('FINALIST')[0].trim()
 })
 });
 jQuery('#enquirebutton3').click(function(){
 gtag('event','click',{
 'event_category':'enquiry',
 'event_label':jQuery('h2.headline.shortlist-heading[itemprop="name"]').text().split('FINALIST')[0].trim()
 })
 });
 });
 
 
 
     $("#enquirebutton").click(function(){
          $("#collapse2").addClass( "in" );
          setTimeout(function() {$("#vacanncy_uname").focus();},500);
          
	});    
     $("#enquirebutton2").click(function(){
          $("#collapse2").addClass( "in" );
          setTimeout(function() {$("#vacanncy_uname").focus();},500);
          
	});    
     $("#enquirebutton3").click(function(){
          $("#collapse2").addClass( "in" );
          setTimeout(function() {$("#vacanncy_uname").focus();},500);
          
	});    
	var type = window.location.hash.substr(1);
    if(type=="enquire"){
          $("#collapse2").addClass( "in" );
          setTimeout(function() {$("#vacanncy_uname").focus();},500);
	};    
    $("#sendamessage").click(function(){
        $(document).find('#frm_message').show('slow');   
        $(document).find('#send_message1').show('slow');   
    });    

    $("#enquirejob").click(function(){
        $('#userEnquiryForm').modal('hide');
        $("#Contact").trigger('click');   
        $("#sendamessage").trigger('click');   
        setTimeout(function() {$("#cust_name").focus();},500);
        $("#collapse2").removeClass( "in" );  
    });    

$( "#click_showmap" ).click(function() {
  $("#showmap").html('<iframe src="https://www.google.com/maps/embed/v1/search?q=<?php echo $prepAddr?>&key=AIzaSyDkggLcQVkN0AF-fx-RNUOqeduvD6BgYRo" width="100%" height="300" frameborder="0" class="border-0" allowfullscreen></iframe><div class="clearfix"></div>');
  $( "#click_showmap" ).hide();
});

</script>
      <!--=== End Content Part ===-->
<style>
.headline a.pull-right.add-to-shortlist {
	display: block;
}

.headline a.pull-right.add-to-shortlist.hide {
	display: none
}

.headline a.remove_shortlist {
	float: right;
	display: block;
	cursor: pointer;
}

.headline a.addto_shortlist {
	float: right;
	display: block;
	cursor: pointer;
}

.headline a.remove_shortlist.hides {
	display: none;
}

.headline a.pull-right.add-to-shortlist.flt-l-ml-10 {
	float: left;
	margin-left: 10px;
}

.headline a.remove_shortlist.flt-r {
	float: right;
}

.headline a.pull-right.add-to-shortlist.ml-10 {
	margin-left: 10px;
}

.border-0 {
	border: none;
}

.supp-message-c {
	text-align: center;
	color: #F00;
}

.vacanncy-txt-c {
	text-align: center;
	color: #FF0000;
}

.next-days.hide {
	display: none;
}

.mt-20-b-25-pdl-0 {
	margin: 20px 0px 25px;
	padding-left: 0px;
}

.dis-inline {
	display: inline;
}
</style>
</div>