<script type="text/javascript" src="<?php echo HOME_PATH; ?>js/bootstrap-multiselect.js"></script>
 <script type="text/javascript" src="//maps.google.com/maps/api/js?key=AIzaSyDkggLcQVkN0AF-fx-RNUOqeduvD6BgYRo"></script> 
<style>
.star-active {
    background: url(https://www.agedadvisor.nz//images/starbg.png) 0 -21px no-repeat;
    background-size: 91px;
 
}
 
.star-inactive {
    background-image: url(https://www.agedadvisor.nz//images/starbg.png);
    background-repeat: no-repeat;
    background-size: 91px;
    background-position: 0 0;
    display: inline-block;
    height: 20px;
    width: 92px;
}
    .vm-pagination ul > li
    {
        display:inline-block;
        padding:0px 10px;
    }
    .tooltips:hover:after {
        content:attr(tip);
        position: absolute;
        display: inline;
        padding: 6px 10px;
        margin-left: 5px;
        max-width: 400px;
        background: #111;
        background: rgba(0,0,0,.8);
        border-radius: 5px;
        color: #FFF;
        font-size: 14px;
        font-weight: normal;
        text-shadow: 0 1px 0 #000;
    }
    .fa-question-circle {    
        width: 40px;    
        text-align: left;    
        font-size: 16px;
        color: #E98B39;
        transition: all .3s;
    }  
	
	input[type="checkbox"], input[type="radio"] {
    margin-right: 0px;
	vertical-align: middle;
}
</style>
<?php
$showmsg = 0;
if (isset($_REQUEST['resp']) && !empty($_REQUEST['resp']) && isset($_REQUEST['enq']) && !empty($_REQUEST['enq'])) {
    $response = $_REQUEST['resp'];
    if ($response == "NO") {
        $response = "No, changed our minds";
    }
    if ($response == "NOTYET") {
        $response = "Still looking / deciding";
    }

    $enqId = $_REQUEST['enq'];
    $enqIds = explode('-', $enqId);

    foreach ($enqIds as $id) {
        $enqId = $id;
        $fields = array(
            'third_followup_sent' => "y",
            'chosen_a_facility' => $response,
            'chosen_facility_id' => NULL
        );
        $where = "where id=" . $enqId . "";
        $update_result = $db->update(_prefix('enquiries'), $fields, $where);
        $showmsg = 1;
    }
}

$city_description_title = '';
$city_description = '';
$city_keyword = '';

function ListserviceCategoryNames($fac_string) {

    global $db;
    $name = '';
    $query = "SELECT name  FROM " . _prefix("services") . " WHERE id IN (" . $fac_string . ") ";
    //echo $query;exit;
    $res = $db->sql_query($query);
    $data = $db->sql_fetchrowset($res);
    foreach ($data as $key => $value) {
        $name .= $value['name'] . " or ";
    }// end for each
    $name = rtrim($name, "or ");

    return $name;
}

function ListsuburbNames($sub_string) {

    $sub_string = str_ireplace(',', ', ', $sub_string);

    return $sub_string;
}

function only_one_result($restCare) {
    //echo $restCare;
    if ($restCare) {
        global $db;
        $query = "SELECT quick_url  FROM " . _prefix("products") . " WHERE title = '" . $restCare . "' OR id='" . $restCare . "' ";
        //echo $query;exit;
        $res = $db->sql_query($query);
        $data = $db->sql_fetchrowset($res);
        foreach ($data as $key => $value) {
            $quick_url = $value['quick_url'];
            $url = HOME_PATH . $quick_url;
            redirect_to($url);
            exit;
        }// end for each
    }// end if $restCare
    return $restCare;
}

// end function

function extend_url($facilityType, $filter2, $suburb = '') {
    $default = 'Retirement-Villages-Rest-Homes-Aged-Care-';
    $count = 0;
    if ($facilityType) {


        $default = '';
        global $db;
        $query = "SELECT id,description "
                . " FROM " . _prefix("services");

        $res = $db->sql_query($query);
        $data = $db->sql_fetchrowset($res);


        //    $countData = count($data);
        foreach ($data as $key => $value) {
            $services_id = $value['id'];
            $services_description = $value['description'];
            //echo "LOOK $services_id - $services_description in $facilityType<br>";
            if ($count < 3 && stristr(',' . $facilityType . ',', ',' . $services_id . ',')) {
                $default = $default . $services_description . " ";
                $count = $count + 1;
            }
        }
        $default .= '-';
        //exit;
    }

    if (stristr($filter2, 'rate')) {
        $default = 'Best-top-rated-' . $default;
    }
    if (stristr($filter2, 'review')) {
        $default = 'Most-reviews-' . $default;
    }

    if ($suburb) {
        if (strlen($suburb) >= 50) {
            $suburb = substr($suburb, 0, 49);
        }

        $default .= '-' . str_ireplace(',', '-', $suburb) . '-';
    }
    // Convert to url
    $default = trim(cleanthishere($default));
    $default = str_ireplace('  ', '-', $default);
    $default = str_ireplace(' ', '-', $default);
    $default = str_ireplace('---', '-', $default);
    $default = str_ireplace('--', '-', $default);
    $default = str_ireplace('+', '', $default);
    $default = str_ireplace(',', '', $default);
    $default = str_ireplace(')', '', $default);
    $default = str_ireplace('(', '', $default);



    return $default;
}

// end function extend url

function array_to_string($a) {
    $return = '';
    if (is_array($a)) {
        while (list ($key, $val) = each($a))
            $return = $return . $val . ',';
        $return = rtrim($return, ",");
    }
    return $return;
}

function string_to_array($a) {
    $return_array = array();
    if (!is_array($a)) {
        $return_array = explode(",", $a);
    }
    return $return_array;
}

function grab_all_prids_that_are_in_facility_types_array($facilityType) {
    $return = '';
//print_r($facilityType);

    while (list ($key, $val) = each($facilityType)) {
        $list = '';
        if ($val) {
            $list = grab_all_prids_with($val);
        }
        if ($list) {
            $return = $return . $list . ',';
        }
    }// end while
    $return = rtrim($return, ",");
    return $return;
}

function grab_all_prids_with($service_id) {
    $return = '';
    global $db;
    //default
    $query = "SELECT product_id FROM " . _prefix("pro_services") . "   "
            . " where deleted = 0  AND status = 1 AND service_id=" . $service_id;

    if ($service_id == '18') {
        $query = "SELECT id AS product_id FROM " . _prefix("products") . "   "
                . " where deleted = 0  AND certification_service_type='Home Care' ";
    }

    if ($service_id == '19') {
        $query = "SELECT id AS product_id FROM " . _prefix("products") . "   "
                . " where deleted = 0  AND certification_service_type='Aged Care' ";
    }

    if ($service_id == '20') {
        $query = "SELECT id AS product_id FROM " . _prefix("products") . "   "
                . " where deleted = 0  AND certification_service_type='Retirement Village' ";
    }

    $res = $db->sql_query($query);
    $data = $db->sql_fetchrowset($res);
    //print_r($data);echo $query;
    foreach ($data as $key => $record) {

        $return = $return . $record['product_id'] . ",";
    }
    $return = rtrim($return, ",");
    //echo "$service_id ={" .$return."}";
    return $return;
}

function show_image($image_enabled = FALSE, $id = "'none'", $certification_service_type = '', $url = '') {
    record_mtime("/modules/search/search.php function show_image()");

    $return = '';
    global $db;

    $review_friendly_badge = $response_time_badge = $response_rate_badge = '';

    $response_rate_query = "SELECT COUNT(NULLIF(id,0)) as total_enq, (SELECT COUNT(NULLIF(id,0)) FROM ad_enquiries WHERE DATE(time_of_enquiry) < DATE_SUB(CURDATE(), INTERVAL 7 DAY) and prod_id = $id AND facility_responded = 'y') AS replied FROM ad_enquiries WHERE DATE(time_of_enquiry) < DATE_SUB(CURDATE(), INTERVAL 7 DAY) and prod_id = $id";
    $response_rate_res = $db->sql_query($response_rate_query);
    $response_rate_data = $db->sql_fetchrowset($response_rate_res);

//foreach($response_rate_data as $data_rate){    
//    if($data_rate['total_enq'] > 0){
//        $replied_enq = $data_rate['replied'];   
//        $total_enq = $data_rate['total_enq'];   
//        $response_rate = ($replied_enq/$total_enq)*100;   
//        $response_rate_round = round( $response_rate, 1, PHP_ROUND_HALF_UP);
//        if($response_rate_round > 79){
//            $response_rate_badge = '<div class="trusted-responder" style="margin-right:7px"> </div>';
//        }        
//    }
//}


    if ($certification_service_type == 'Retirement Village') 
	{
        $query_reviews = "SELECT count(fdbk.id) as totalreview, fdbk.product_id,(extr.no_of_room) as totalRoom,"
                . "(count(fdbk.id)/(extr.no_of_room)) as cmost FROM ad_feedbacks AS fdbk LEFT JOIN ad_products AS pds ON fdbk.product_id= pds.id"
                . " LEFT JOIN ad_pro_plans AS plan ON fdbk.product_id= plan.pro_id inner join ad_extra_facility as extr on (extr.product_id=pds.id)"
                . " WHERE fdbk.status = 1 AND fdbk.deleted = 0 AND current_plan = 1 and pds.certification_service_type = 'Retirement Village' and (pds.deleted=0)"
                . " and (pds.status=1) GROUP BY product_id ORDER BY cmost DESC,plan_id DESC,product_id ASC LIMIT 50";
    } else 
	{

        $query_reviews = "SELECT count(fdbk.id) as totalreview, fdbk.product_id,(extr.no_of_beds) as totalbeds,"
                . "(count(fdbk.id)/(extr.no_of_beds)) as cmost FROM ad_feedbacks AS fdbk LEFT JOIN ad_products AS pds ON fdbk.product_id= pds.id "
                . "LEFT JOIN ad_pro_plans AS plan ON fdbk.product_id= plan.pro_id inner join ad_extra_facility as extr on (extr.product_id=pds.id)"
                . " WHERE fdbk.status = 1 AND fdbk.deleted = 0 AND current_plan = 1 and pds.certification_service_type != 'Retirement Village' "
                . "and (pds.deleted=0) and (pds.status=1) GROUP BY product_id ORDER BY cmost DESC,plan_id DESC,product_id asc LIMIT 50";
    }
    $result_reviews = $db->sql_query($query_reviews);
    $count = $db->sql_numrows($result_reviews);
    $data_reviews = $db->sql_fetchrowset($result_reviews);
    $i = 0;
    if (count($data_reviews) > 0) {
        foreach ($data_reviews as $data_reviews_val) {
            if ($data_reviews_val['product_id'] == $id) {
                switch (true) {

                    case $i < 5 :
                        $most_review_badge = '<span class="most-reviewed-top5a most-reviewed-icon-size"> </span>';
                        break;

                    case $i < 10:
                        $most_review_badge = '<span class="most-reviewed-top10a most-reviewed-icon-size"> </span>';
                        break;

                    case $i < 25:
                        $most_review_badge = '<span class="most-reviewed-top25a most-reviewed-icon-size"> </span>';
                        break;

                    case $i < 50:
                        $most_review_badge = '<span class="most-reviewed-top50a most-reviewed-icon-size"> </span>';
                        break;

                    default :
                        $most_review_badge = '';
                        break;
                }
            }
            $i++;
        }
    }


    /*
      $response_duration_query = "SELECT time_of_enquiry, facility_responded FROM ad_enquiries WHERE DATE(time_of_enquiry) < DATE_SUB(CURDATE(), INTERVAL 7 DAY) AND prod_id = $id ORDER BY time_of_enquiry DESC LIMIT 1";
      $response_duration_res = $db->sql_query($response_duration_query);
      $response_duration_data = $db->sql_fetchrowset($response_duration_res);

      foreach($response_duration_data as $data_duration){

      if($data_duration['facility_responded'] === 'y'){
      $response_time_badge = '<div class="response-time" style="margin-right:7px"> </div>';
      }
      }
     */

    $qry_dayResp = "SELECT facility_name, ( (5 * (DATEDIFF(DATE(time_of_response),"
            . " DATE(time_of_enquiry)) DIV 7) + MID('0123444401233334012222340111123400012345001234550', 7 * WEEKDAY(DATE(time_of_enquiry)) + WEEKDAY(DATE(time_of_response)) + 1, 1) ) - (SELECT COUNT(*) FROM ad_holidays WHERE holiday_date between DATE(time_of_enquiry)"
            . " and DATE(time_of_response) ) )as dayResp FROM ad_enquiries where prod_id='$id'"
            . " and DATE(time_of_enquiry) < DATE_SUB(CURDATE(),INTERVAL 4 DAY) and facility_responded='y' and time_of_enquiry!='' and time_of_response!='' order BY time_of_enquiry desc";

    $res_dayResp = $db->sql_query($qry_dayResp);
    $count_resp = $db->sql_numrows($res_dayResp);
    $row_dayResp = $db->sql_fetchrowset($res_dayResp);
    if ($count_resp > 0) {
        foreach ($row_dayResp as $cResp) {
            $numResp[] = $cResp['dayResp'];
        }

        $avg_resp = round(array_sum($numResp) / count($numResp));

        switch (true) {
            case ($avg_resp > 0 && $avg_resp < 2 ) :
                $response_time_badge = '<span class="response-time-one1 most-reviewed-icon-size"> </span>';
                break;
            case ($avg_resp > 1 && $avg_resp < 3):
                $response_time_badge = '<span class="response-time-two1 most-reviewed-icon-size"> </span>';
                break;
            case ($avg_resp > 2 && $avg_resp < 4):
                $response_time_badge = '<span class="response-time-three1 most-reviewed-icon-size"> </span>';
                break;
            case ($avg_resp > 3 && $avg_resp < 5):
                $response_time_badge = '<span class="response-time-four1 most-reviewed-icon-size"> </span>';
                break;
            case ($avg_resp > 4):
                $response_time_badge = '<span class="response-time-five1 most-reviewed-icon-size"> </span>';
                break;
            default :
                $response_time_badge = '<span class="response-time1 most-reviewed-icon-size"> </span>';
                break;
        }
    }

    $trustqry = "SELECT "
            . " COUNT(CASE WHEN facility_responded = 'y' then 1 ELSE NULL END) as trusted_count,"
            . " COUNT(CASE WHEN facility_responded = 'n' then 1 ELSE NULL END) as nottrusted_count"
            . " FROM ad_enquiries"
            . " WHERE DATE(time_of_enquiry) < DATE_SUB(CURDATE(), INTERVAL 4 DAY)"
            . " and prod_id = $id ORDER by time_of_enquiry DESC LIMIT 4";

    $result_Resp = $db->sql_query($trustqry);
    $row_resp = $db->sql_fetchrowset($result_Resp);

    if ($row_resp[0]['nottrusted_count'] >= 3 && $row_resp[0]['trusted_count'] < 3) {
        $response_rate_badge = '<div class="nottrusted-responder1 most-reviewed-icon-size" style="margin-right:7px"> </div>';
    } else if ($row_resp[0]['trusted_count'] >= 3 && $row_resp[0]['nottrusted_count'] < 3) {
        $response_rate_badge = '<div class="trusted-responder most-reviewed-icon-size"> </div>';
    } else {
        $response_rate_badge = '';
    }

    $query = "SELECT gal.id, gal.title,gal.file,gal.type,prd.review_friendly FROM " . _prefix("galleries") . "  as gal "
            . " LEFT JOIN " . _prefix("products") . " AS prd on prd.id = gal.pro_id "
            . " where gal.deleted = 0  AND pro_id=" . $id . " AND gal.type=0  ORDER BY gal.id LIMIT 0,1";
    $res = $db->sql_query($query);
    $data = $db->sql_fetchrowset($res);
    if (isset($data[0])) {
        if ($data[0]['review_friendly'] == 1) {
            $review_friendly_badge = '<span class="review-friendly most-reviewed-icon-size"> </span>';
        }
        if ($data[0]['review_friendly'] == 2) {
            $review_friendly_badge = '<span class="review-unfriendly1 most-reviewed-icon-size"> </span>';
        }
    }


    if ($image_enabled && isset($data[0])) 
	{

        $record = $data[0];
        $pic_description = $data['title'];
        $fileImage = HOME_PATH . 'admin/files/gallery/images/thumb_' . $record['file'];
        $fileImageSrc = HOME_PATH . 'admin/files/gallery/images/thumb_' . $record['file'];
        $fileImageView = HOME_PATH . 'admin/files/gallery/images/' . $record['file'];

        if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/admin/files/gallery/images/thumb_' . $record['file'])) 
		{

            $logo_image_pro = upgradeproduct_logos($id);
            if ($logo_image_pro == '' || empty($logo_image_pro)) 
			{
               // $logo = '';
            } 
			else 
			{
                //$logo = '<div id="pro_logo" class="product_logo"><img src="https://agedadvisor.nz/admin/files/product/logo/thumb_' . $logo_image_pro . '"/></div>';
            }
            /*$return = '<div class="col-xs-12 col-sm-4 col-md-12" style="padding:0">' . $logo . '
              <div class="review-container"><a href="' . HOME_PATH . $url . '"><img alt="' . $certification_service_type . ' ' . $pic_description . '" title="' . $pic_description . '" src="' . $fileImageSrc . '" class="img-responsive" style="float:left; margin-right: 20px;margin-bottom:10px;"></a>' . $review_friendly_badge . $response_time_badge . $response_rate_badge . $most_review_badge . '</div></div>';*/
			  
			  $return = '<div class="col-xs-12 col-sm-4 col-md-12" style="padding:0">' . $logo . '
              <div class="review-container"><a href="' . HOME_PATH . $url . '"><img alt="' . $certification_service_type . ' ' . $pic_description . '" title="' . $pic_description . '" src="' . $fileImageSrc . '" class="img-responsive" style="float:left; margin-right: 20px;margin-bottom:10px;"></a></div></div>';
			  
        } else {
			
           /* if ($review_friendly_badge != '' || $response_time_badge != '' || $response_rate_badge != '' || $most_review_badge != '') {/*
				
                /*
				NO IMAGE AREA -DAVINDER
				$return = '<div class="col-xs-12 col-sm-4 col-md-12"><div class="association-img review-container">' . $review_friendly_badge . $response_time_badge . $response_rate_badge . $most_review_badge . '</div></div>';*/
				 $return = '<div class="col-xs-12 col-sm-4 col-md-12" style="padding:0"><div class="review-container"><a href="' . HOME_PATH . $url . '"><img alt="' . $certification_service_type . '" title="' . $certification_service_type . '" src="' . HOME_PATH .'/images/noimagedav.png" class="img-responsive" style="float:left; margin-right: 20px;margin-bottom:10px;"></a></div></div>';
           /* }*/
        }
    } else {
        /*if ($review_friendly_badge != '' || $response_time_badge != '' || $response_rate_badge != '' || $most_review_badge != '') {*/
            $return = '<div class="col-xs-12 col-sm-4 col-md-12" style="padding:0"><div class="review-container"><a href="' . HOME_PATH . $url . '"><img alt="' . $certification_service_type . '" title="' . $certification_service_type . '" src="' . HOME_PATH .'/images/noimagedav.png" class="img-responsive" style="float:left; margin-right: 20px;margin-bottom:10px;"></a></div></div>';
        /*}*/
    }
    return $return;
    record_mtime("/modules/search/search.php function show_image()");
}

function cmp($a, $b) {

    if ($a['ratingValue'] == $b['ratingValue']) {
        return 0;
    }
    return ($a['ratingValue'] < $b['ratingValue']) ? -1 : 1;
}

//prd($_POST);


global $db;
// Is it saved?
// Check if in URL
//$pathInfo = parse_path();
$sid = '';
if (ISSET($pathInfo['call_parts']['2'])) {
    $sid = $pathInfo['call_parts']['2'];
}// WE GOT THE SEARCH ID
$sid = cleanthishere($sid);
if (!is_numeric($sid)) {
    $sid = '';
}



if (!$sid) {
    $proSuburb = '';
    if (ISSET($cityid) && $cityid) {
        $cityid = mysqli_escape_string($db->db_connect_id, $cityid);
    } else {
        $cityid = '';
    }
    if (ISSET($cityId) && $cityId) {
        $cityid = mysqli_escape_string($db->db_connect_id, $cityId);
    } else {
        $cityid = '';
    }
    if (ISSET($providers) && $providers) {
        $providers = mysqli_escape_string($db->db_connect_id, $providers);
    } else {
        $providers = '';
    }
//	if(ISSET($proSuburb)){$proSuburb=mysqli_escape_string($db->db_connect_id, $proSuburb);}else{$proSuburb='';}
    if (ISSET($restCare) && $restCare) {
        $restCare = mysqli_escape_string($db->db_connect_id, $restCare);
    } else {
        $restCare = '';
    }
    if (ISSET($cityLable) && $cityLable) {
        $cityLable = mysqli_escape_string($db->db_connect_id, $cityLable);
    } else {
        $cityLable = '';
    }
    if (ISSET($facilityType) && $facilityType) {
        $facilityType = mysqli_escape_string($db->db_connect_id, array_to_string($facilityType));
    } else {
        $facilityType = '';
    }
    if (ISSET($suburbType) && $suburbType) {
        $suburbType = mysqli_escape_string($db->db_connect_id, array_to_string($suburbType));
        $proSuburb = $suburbType;
    } else {
        $suburbType = '';
    }
    if (ISSET($filter1) && $filter1) {
        $filter1 = mysqli_escape_string($db->db_connect_id, $filter1);
    } else {
        $filter1 = '';
    }
    if (ISSET($filter2) && $filter2) {
        $filter2 = mysqli_escape_string($db->db_connect_id, $filter2);
    } else {
        $filter2 = '';
    }
//echo "citylabel = $cityLable, facility type = ".$facilityType.", filter1 = $filter1, filter2 = $filter2<hr>";exit;
    record_mtime("/modules/search/search.php find existing search");

// Find an existing search
    $queryseid = "SELECT id,search FROM " . _prefix("searches") . " WHERE search LIKE '$cityLable' AND facility_type LIKE '$facilityType' AND
	    sort_by LIKE '$filter1' AND sort_by2 LIKE '$filter2' AND city LIKE '$city' ";
    
    if ($cityid) {
        $queryseid .= " AND cityid = '$cityid' ";
    }
    $queryseid .= " AND proSuburb LIKE '$proSuburb' AND 
	    restCare LIKE '$restCare' ";
    $queryseid .= "AND provider LIKE '$providers' ";
    $queryseid .= "ORDER BY id ASC LIMIT 0,1";
    //echo $queryseid;exit;
    $reseid = $db->sql_query($queryseid);
    $datae = $db->sql_fetchrowset($reseid);



    if (isset($datae[0])) {
        $recorde = $datae[0];
        $sid = $recorde['id'];
        $search = urlencode(trim($recorde['search']));
        if ($sid) {
            $url = HOME_PATH . 'search/for/' . $sid . '/' . extend_url($facilityType, $filter2, $proSuburb) . $search;
            //echo $url;exit;
            redirect_to($url);
            exit;
        }
    }
    $userData = array(
        'search_query' => '',
        'search' => $cityLable,
        'facility_type' => $facilityType,
        'sort_by' => $filter1,
        'sort_by2' => $filter2,
        'cityName' => '',
        'city' => $city,
        'cityid' => $cityid,
        'proSuburb' => $proSuburb,
        'restCare' => $restCare,
        'provider' => $providers
    );

    // Insert the details of user
    $insert_query = $db->insert(_prefix('searches'), $userData);
    if ($insert_query) {
        $sid = mysqli_insert_id();
        // if validate is 1 then redirect to registration page
        if ($sid) {
            $search = urlencode(trim($cityLable));
            $url = HOME_PATH . 'search/for/' . $sid . '/' . extend_url($facilityType, $filter2, $proSuburb) . $search;
            //echo $url;exit;
            redirect_to($url);
        }
    }// end if inserted
    record_mtime("/modules/search/search.php find existing search");
} else if ($sid) {
    // GRAB THE SID QUERY    
    $_SESSION['searchid'] = $sid;
    $querysid = "SELECT * FROM " . _prefix("searches") . " WHERE id='$sid' ";
    $resid = $db->sql_query($querysid);
    $data = $db->sql_fetchrowset($resid);



    if (isset($data[0])) {

        $record = $data[0];

        //$query=$record['search_query'];
        $cityLable = trim($record['search']);
        $facilityType_string = $record['facility_type'];
        $filter1 = $record['sort_by'];
        $filter2 = $record['sort_by2'];
        //$query=$record['cityName'];
        $city = $record['city'];
        $cityid = trim($record['cityid']);
        $proSuburb = $record['proSuburb'];
        $restCare = only_one_result($record['restCare']);
        $providers = $record['provider'];

        if (!$cityid) {
            $sql = "select id,title from ad_cities ";
            $result = mysqli_query($db->db_connect_id, $sql) or die("$sql<br>Error number:" . mysqli_errno() . "");
            if ($result) {
                $found = false;
                while ($myrow = mysqli_fetch_row($result)) {
                    $city_id = $myrow[0];
                    $city_title = trim($myrow[1]);
                    if (!$found && stristr($cityLable, $city_title)) {
                        $found = $city_id;
                    }
                    //echo "$cityLable, $city_title<br>";
                    //echo".";
                }
            }
            //echo "$cityLable, $city_title, $found";
            if ($found) {
                $status = array(
                    'cityid' => "$found",
                );
                $where = "where id = '" . $sid . "' ";
                $update_result = $db->update(_prefix('searches'), $status, $where);
                $cityid = $found;
            }
        }
    } else {
        // Can not find save query
        $url = HOME_PATH . 'search/for';
        //echo $url;exit;
        redirect_to($url);
    }
}
//echo "citylabel = $cityLable, facility type = ".$facilityType_string.", filter1 = $filter1, filter2 = $filter2<hr>";

$facilityType = string_to_array($facilityType_string);
$suburbType = string_to_array($proSuburb);


// Create the query
$con = '';
if ($cityid) {
//echo $cityid;	
    $sql = "select description_title,description,title from ad_cities WHERE id= '$cityid'";
    $result = mysqli_query($db->db_connect_id, $sql) or die("$sql<br>Error number:" . mysqli_errno() . "");
    if ($result) {
        while ($myrow = mysqli_fetch_row($result)) {
            $city_description_title = $myrow[0];
            $city_description = $myrow[1];
            $city_keyword = $myrow[2];
        }
    }



    $cityCon = ' prd.city_id= ' . $cityid . ' ';
}
if ($providers) {
    $providers = trim($providers);
    $sql = "select description_title,description from ad_users WHERE trim(company) = '$providers'";
    $result = mysqli_query($db->db_connect_id, $sql) or die("$sql<br>Error number:" . mysqli_errno() . "");
    if ($result) {
        while ($myrow = mysqli_fetch_row($result)) {
            $provider_description_title = $myrow[0];
            $provider_description = $myrow[1];
        }
    }
}
//prd($proSuburb);
//if ($proSuburb) {
//    $suburbCon = "AND prd.address_suburb LIKE '%" . $proSuburb . "%'";
//}
if ($restCare) {
    $restcareCon = "AND prd.title LIKE '%" . $restCare . "%' ";
}
if ($cityid || $restCare) {
    $con = "AND ( $cityCon $restcareCon )";
} else if ($cityid == '' && $cityLable) {
    $con = "AND (concat(city.title, ', ' , prd.address_suburb, ', ', prd.title) LIKE '%" . $cityLable . "%' OR concat(city.title, ', ' , prd.address_suburb) LIKE '%" . $cityLable . "%' OR concat(prd.address_city, ', ' , prd.address_suburb, ', ' , prd.title) LIKE '%" . $cityLable . "%' OR concat(prd.address_city, ', ' , prd.address_suburb) LIKE '%" . $cityLable . "%' OR prd.title LIKE '%" . $cityLable . "%' OR   city.title LIKE '%" . $cityLable . "%'   OR (sp.company LIKE '%" . $cityLable . "%' AND sp.deleted = 0 AND sp.user_type = 1) )";
} else if ($facilityType_string) {
    $con = "AND ( prd.facility_type IN ($facilityType_string) )";
    //echo "here [$cityid]=='' && [$cityLable] [$restCare] [$filter1] [$filter2] [$facilityType_string]";exit;
    //redirect_to(HOME_PATH);
} else {
    $con = "AND ( prd.id='' )";
//    echo "here [$cityid]=='' && [$cityLable] [$restCare] [$filter1] [$filter2] [$facilityType_string]";exit;
    redirect_to(HOME_PATH);
}


$allFacilityType_pr_ids = grab_all_prids_that_are_in_facility_types_array($facilityType);

$sub_string = implode("','", $suburbType);



if ($allFacilityType_pr_ids !== '') {
    $condition = " where prd.deleted = 0  $con AND ( prd.id IN ($allFacilityType_pr_ids) )";
} else {
    $condition = " where prd.deleted = 0  $con ";
}
$condition_without_suburb = $condition;
if ($sub_string) {
    $condition .= " AND ( prd.address_suburb IN ('$sub_string') ) ";
//echo $condition;	
}

if (empty($filter2) AND empty($filter2)) {
    $filter2 = 'rankAsc';
}
if (!empty($filter1)) {
    $order = $filter1;
} else if (!empty($filter2) && $filter2 != 'rateDesc') {
    $order = $filter2;
} else {
    $order = '';
}

if (!empty($filter2) && $filter2 == 'reviewDesc') {
    $query_review = "SELECT DISTINCT prd.id,prd.review_friendly,prd.supplier_id,(select count(feedback) from ad_feedbacks fd where fd.product_id = prd.id and fd.status=1 and fd.deleted=0) as no_of_review,sp.company AS companyname, sp.last_name AS spLName, prd.title  ,prd.address_city AS city, prd.address_suburb AS suburbTitle,  prd.description, prd.image, prd.status  ,prd.latitude,pein.certification_service_type,memp.image_enable, prd.longitude,prd.quick_url ,prd.award_text,extra.no_of_room,extra.no_of_beds, (select MAX(fd.created) as max_date from ad_feedbacks fd where fd.product_id = prd.id and fd.status=1 and fd.deleted=0)as max_created  "
            . " FROM " . _prefix("products") . " AS prd "
            . " LEFT JOIN " . _prefix("services") . " AS srv ON srv.id=prd.facility_type "
            . " LEFT JOIN " . _prefix("users") . " AS sp ON  prd.supplier_id= sp.id "
            . " LEFT JOIN " . _prefix("cities") . " AS city ON city.id=prd.city_id "
            . " LEFT JOIN " . _prefix("suburbs") . " AS suburb ON suburb.id=prd.suburb_id "
            . " Left join " . _prefix("pro_plans") . " AS prpn ON prpn.pro_Id =prd.id AND prpn.current_plan = 1 "
            . " Left join " . _prefix("membership_prices") . " AS memp ON prpn.plan_Id =memp.id"
            . " LEFT JOIN " . _prefix("pro_extra_info") . " AS pein ON pein.pro_id=prd.id"
            . " LEFT JOIN " . _prefix("extra_facility") . " AS extra ON extra.product_id=prd.id"
            . " " . $condition . " ORDER BY  no_of_review desc  LIMIT 0,300  "; //" LIMIT {$start},{$limit}"; 
    $res_review = $db->sql_query($query_review);
    $data = $db->sql_fetchrowset($res_review);
    $count = $db->sql_numrows($res_review);
    $totalRatingVillage = 0; $noOfRatingCount = 0;
    
    foreach ($data as $key => $record) {
        $data[$key]['ratingValue'] = OverAllNEWRatingProduct($record['id']);
        $data[$key]['no_of_review'] = $record['no_of_review'];

        $num_revs = overAllRatingProductCount($record['id']);
        $avg_rating = AvgRating($record['id']);
        if( $avg_rating > 0 ){
            $totalRatingVillage = $totalRatingVillage + $avg_rating;
            $noOfRatingCount++;
        }
            
        if ($record['certification_service_type'] == 'Aged Care') {
            $num_beds = $record['no_of_beds'];
        } elseif ($record['certification_service_type'] == 'Retirement Village') {
            $num_beds = $record['no_of_room'];
        } else {
            $num_beds = ($num_beds > $num_rooms) ? $num_beds : $num_rooms;
        }

        if ($num_beds > 0 && $num_revs > 2) {
            $data[$key]['avg_rating'] = $avg_rating;
            $data[$key]['facility_ratio'] = $facility_ratio = $num_revs / $num_beds;

            $datetime = new DateTime($record['max_created']);
            $now = new DateTime();
            $interval = $datetime->diff($now);
            $yrs = $interval->y;

            if ($yrs < 1) {
                $rankingValue = $avg_rating - ((1 - $facility_ratio - 0.5) / 0.95);
            } elseif ($yrs > 1 && $yrs < 2) {
                $rankingValue = $avg_rating - ((1 - $facility_ratio - 0.5) / 0.95);
                $rankingValue = $rankingValue * .975;
            } else {
                $rankingValue = $avg_rating - ((1 - $facility_ratio - 0.5) / 0.95);
                $rankingValue = $rankingValue * .95;
            }

            $data[$key]['rank_score'] = $rankingValue;
        } else {
            $data[$key]['rank_score'] = "-99999999";
        }
    }
    if( $noOfRatingCount > 0 ){
        $totalOverageVillageRating = ($totalRatingVillage / $noOfRatingCount);
    }
    $overageVillageRating = round($totalOverageVillageRating);
    if(isset($overageVillageRating) || $overageVillageRating>0 || !empty($overageVillageRating)){
    	$ratingOverageVillage = $totalOverageVillageRating;
    }else{
    	$ratingOverageVillage = 0;
    }  
    
    usort($data, function ($item1, $item2) {
        if ($item1['rank_score'] == $item2['rank_score'])
            return 0;
        return $item1['rank_score'] > $item2['rank_score'] ? -1 : 1;
    });

    $i = 1;
    foreach ($data as $key => $record) {
        if ($record['rank_score'] == "-99999999") {
            $data[$key]['ranking'] = NULL;
        } else {
            $data[$key]['ranking'] = $i;
            $i++;
        }
    }

    usort($data, function ($item1, $item2) {
        if ($item1['no_of_review'] == $item2['no_of_review'])
            return 0;
        return $item1['no_of_review'] > $item2['no_of_review'] ? -1 : 1;
    });
}
elseif (!empty($filter2) && $filter2 == 'rateDesc') {
    $query1 = "SELECT DISTINCT prd.id,prd.review_friendly,prd.supplier_id,(select AVG(fd.overall_rating) as over_rating from ad_feedbacks fd where fd.product_id = prd.id and fd.status=1 and fd.deleted=0)as overall_rating,sp.company AS companyname, sp.last_name AS spLName, prd.title  ,prd.address_city AS city, prd.address_suburb AS suburbTitle,  prd.description, prd.image, prd.status  ,prd.latitude,pein.certification_service_type,memp.image_enable, prd.longitude,prd.quick_url, prd.award_text,extra.no_of_room,extra.no_of_beds, (select MAX(fd.created) as max_date from ad_feedbacks fd where fd.product_id = prd.id and fd.status=1 and fd.deleted=0)as max_created "
            . " FROM " . _prefix("products") . " AS prd "
            . " LEFT JOIN " . _prefix("services") . " AS srv ON srv.id=prd.facility_type "
            . " LEFT JOIN " . _prefix("users") . " AS sp ON  prd.supplier_id= sp.id "
            . " LEFT JOIN " . _prefix("cities") . " AS city ON city.id=prd.city_id "
            . " LEFT JOIN " . _prefix("suburbs") . " AS suburb ON suburb.id=prd.suburb_id "
            . " Left join " . _prefix("pro_plans") . " AS prpn ON prpn.pro_Id =prd.id AND prpn.current_plan = 1 "
            . " Left join " . _prefix("membership_prices") . " AS memp ON prpn.plan_Id =memp.id"
            . " LEFT JOIN " . _prefix("pro_extra_info") . " AS pein ON pein.pro_id=prd.id"
            . " LEFT JOIN " . _prefix("extra_facility") . " AS extra ON extra.product_id=prd.id"
            . " " . $condition . " ORDER BY  overall_rating desc  LIMIT 0,300  "; //" LIMIT {$start},{$limit}"; 
    //echo $query1;

    $res1 = $db->sql_query($query1);
    $data = $db->sql_fetchrowset($res1);
    $count = $db->sql_numrows($res1);
    $totalRatingVillage = 0; $noOfRatingCount = 0;
    
    foreach ($data as $key => $record) {
        $data[$key]['ratingValue'] = OverAllNEWRatingProduct($record['id']);
        $num_revs = overAllRatingProductCount($record['id']);
        $avg_rating = AvgRating($record['id']);
        if( $avg_rating > 0 ){
            $totalRatingVillage = $totalRatingVillage + $avg_rating;
            $noOfRatingCount++;
        }
        if ($record['certification_service_type'] == 'Aged Care') {
            $num_beds = $record['no_of_beds'];
        } elseif ($record['certification_service_type'] == 'Retirement Village') {
            $num_beds = $record['no_of_room'];
        } else {
            $num_beds = ($num_beds > $num_rooms) ? $num_beds : $num_rooms;
        }

        if ($num_beds > 0 && $num_revs > 2) {
            $data[$key]['avg_rating'] = $avg_rating;
            $data[$key]['facility_ratio'] = $facility_ratio = $num_revs / $num_beds;
            $datetime = new DateTime($record['max_created']);
            $now = new DateTime();
            $interval = $datetime->diff($now);
            $yrs = $interval->y;

            if ($yrs < 1) {
                $rankingValue = $avg_rating - ((1 - $facility_ratio - 0.5) / 0.95);
            } elseif ($yrs > 1 && $yrs < 2) {
                $rankingValue = $avg_rating - ((1 - $facility_ratio - 0.5) / 0.95);
                $rankingValue = $rankingValue * .975;
            } else {
                $rankingValue = $avg_rating - ((1 - $facility_ratio - 0.5) / 0.95);
                $rankingValue = $rankingValue * .95;
            }

            $data[$key]['rank_score'] = $rankingValue;
        } else {
            $data[$key]['rank_score'] = "-99999999";
        }
    }
    
    if( $noOfRatingCount > 0 ){
        $totalOverageVillageRating = ($totalRatingVillage / $noOfRatingCount);
    }
    $overageVillageRating = round($totalOverageVillageRating);
    if(isset($overageVillageRating) || $overageVillageRating>0 || !empty($overageVillageRating)){
    	$ratingOverageVillage = $totalOverageVillageRating;
    }else{
    	$ratingOverageVillage = 0;
    }
    
    usort($data, function ($item1, $item2) {
        if ($item1['rank_score'] == $item2['rank_score'])
            return 0;
        return $item1['rank_score'] > $item2['rank_score'] ? -1 : 1;
    });

    $i = 1;
    foreach ($data as $key => $record) {
        if ($record['rank_score'] == "-99999999") {
            $data[$key]['ranking'] = NULL;
        } else {
            $data[$key]['ranking'] = $i;
            $i++;
        }
    }

    usort($data, function ($item1, $item2) {
        if ($item1['overall_rating'] == $item2['overall_rating'])
            return 0;
        return $item1['overall_rating'] > $item2['overall_rating'] ? -1 : 1;
    });
} elseif (!empty($filter2) && $filter2 == 'rankAsc') {

    $query_ranking = "SELECT DISTINCT prd.id,prd.review_friendly,prd.supplier_id,sp.company AS companyname, sp.last_name AS spLName, prd.title,prd.address,prd.zip,prd.address_city AS city, prd.address_suburb AS suburbTitle,  prd.description, prd.image, prd.status  ,prd.latitude,pein.certification_service_type,memp.image_enable, prd.longitude,prd.quick_url ,prd.award_text,extra.no_of_room,extra.no_of_beds, (select MAX(fd.created) as max_date from ad_feedbacks fd where fd.product_id = prd.id and fd.status=1 and fd.deleted=0)as max_created  "
            . " FROM " . _prefix("products") . " AS prd "
            . " LEFT JOIN " . _prefix("users") . " AS sp ON  prd.supplier_id= sp.id "
            . " LEFT JOIN " . _prefix("cities") . " AS city ON city.id=prd.city_id "
            . " LEFT JOIN " . _prefix("suburbs") . " AS suburb ON suburb.id=prd.suburb_id "
            . " Left join " . _prefix("pro_plans") . " AS prpn ON prpn.pro_Id =prd.id AND prpn.current_plan = 1 "
            . " Left join " . _prefix("membership_prices") . " AS memp ON prpn.plan_Id =memp.id"
            . " LEFT JOIN " . _prefix("pro_extra_info") . " AS pein ON pein.pro_id=prd.id"
            . " LEFT JOIN " . _prefix("extra_facility") . " AS extra ON extra.product_id=prd.id"
            . " " . $condition . "  ORDER BY prd.id LIMIT 0,300 "; //" LIMIT {$start},{$limit}"; */   

    $ranking_res = $db->sql_query($query_ranking);
    $count = $db->sql_numrows($ranking_res);
    $data = $db->sql_fetchrowset($ranking_res);
    $totalRatingVillage = 0; $noOfRatingCount = 0;
    
    foreach ($data as $key => $record) {
        $num_revs = overAllRatingProductCount($record['id']);
        $avg_rating = AvgRating($record['id']);
        if( $avg_rating > 0 ){
            $totalRatingVillage = $totalRatingVillage + $avg_rating;
            $noOfRatingCount++;
        }
        //$num_beds = ($num_beds > $num_rooms) ? $num_beds : $num_rooms;

        if ($record['certification_service_type'] == 'Aged Care') {
            $num_beds = $record['no_of_beds'];
        } elseif ($record['certification_service_type'] == 'Retirement Village') {
            $num_beds = $record['no_of_room'];
        } else {
            $num_beds = ($num_beds > $num_rooms) ? $num_beds : $num_rooms;
        }

        if ($num_beds > 0 && $num_revs > 2) {
            $data[$key]['avg_rating'] = $avg_rating;
            $data[$key]['facility_ratio'] = $facility_ratio = $num_revs / $num_beds;
            $datetime = new DateTime($record['max_created']);
            $now = new DateTime();
            $interval = $datetime->diff($now);
            $yrs = $interval->y;

            if ($yrs < 1) {
                $rankingValue = $avg_rating - ((1 - $facility_ratio - 0.5) / 0.95);
            } elseif ($yrs > 1 && $yrs < 2) {
                $rankingValue = $avg_rating - ((1 - $facility_ratio - 0.5) / 0.95);
                $rankingValue = $rankingValue * .975;
            } else {
                $rankingValue = $avg_rating - ((1 - $facility_ratio - 0.5) / 0.95);
                $rankingValue = $rankingValue * .95;
            }

            //$rankingValue = $avg_rating-((1-$facility_ratio-0.5)/0.95);

            $data[$key]['rank_score'] = $rankingValue;
        } else {
            $data[$key]['rank_score'] = "-99999999";
        }
    }
    
    if( $noOfRatingCount > 0 ){
        $totalOverageVillageRating = ($totalRatingVillage / $noOfRatingCount);
    }
    $overageVillageRating = round($totalOverageVillageRating);
    if(isset($overageVillageRating) || $overageVillageRating>0 || !empty($overageVillageRating)){
    	$ratingOverageVillage = $totalOverageVillageRating;
    }else{
    	$ratingOverageVillage = 0;
    }
    
    //echo '<pre>'; print_r($data);
    usort($data, function ($item1, $item2) {
        if ($item1['rank_score'] == $item2['rank_score'])
            return 0;
        return $item1['rank_score'] > $item2['rank_score'] ? -1 : 1;
    });

    $i = 1;
    foreach ($data as $key => $record) {
        if ($record['rank_score'] == "-99999999") {
            $data[$key]['ranking'] = NULL;
        } else {
            $data[$key]['ranking'] = $i;
            $i++;
        }
    }
    //echo '<pre>'; print_r($data);
} else {
    $query = "SELECT DISTINCT prd.id,prd.review_friendly,prd.supplier_id,sp.company AS companyname, sp.last_name AS spLName, prd.title  ,prd.address,prd.zip,prd.address_city AS city, prd.address_suburb AS suburbTitle,  prd.description, prd.image, prd.status  ,prd.latitude,pein.certification_service_type,memp.image_enable, prd.longitude,prd.quick_url ,prd.award_text,extra.no_of_room,extra.no_of_beds, (select MAX(fd.created) as max_date from ad_feedbacks fd where fd.product_id = prd.id and fd.status=1 and fd.deleted=0)as max_created    "
            . " FROM " . _prefix("products") . " AS prd "
            . " LEFT JOIN " . _prefix("users") . " AS sp ON  prd.supplier_id= sp.id "
            . " LEFT JOIN " . _prefix("cities") . " AS city ON city.id=prd.city_id "
            . " LEFT JOIN " . _prefix("suburbs") . " AS suburb ON suburb.id=prd.suburb_id "
            . " Left join " . _prefix("pro_plans") . " AS prpn ON prpn.pro_Id =prd.id AND prpn.current_plan = 1 "
            . " Left join " . _prefix("membership_prices") . " AS memp ON prpn.plan_Id =memp.id"
            . " LEFT JOIN " . _prefix("pro_extra_info") . " AS pein ON pein.pro_id=prd.id"
            . " LEFT JOIN " . _prefix("extra_facility") . " AS extra ON extra.product_id=prd.id"
            . " " . $condition . " ORDER BY  prd.title $order LIMIT 0,300 "; //" LIMIT {$start},{$limit}"; 
    $res = $db->sql_query($query);
    $data = $db->sql_fetchrowset($res);
    $count = $db->sql_numrows($res);
    $totalRatingVillage = 0; $noOfRatingCount = 0;
    
    foreach ($data as $key => $record) {
        $data[$key]['ratingValue'] = OverAllNEWRatingProduct($record['id']);

        $num_revs = overAllRatingProductCount($record['id']);
        $avg_rating = AvgRating($record['id']);
        if( $avg_rating > 0 ){
            $totalRatingVillage = $totalRatingVillage + $avg_rating;
            $noOfRatingCount++;
        }
        if ($record['certification_service_type'] == 'Aged Care') {
            $num_beds = $record['no_of_beds'];
        } elseif ($record['certification_service_type'] == 'Retirement Village') {
            $num_beds = $record['no_of_room'];
        } else {
            $num_beds = ($num_beds > $num_rooms) ? $num_beds : $num_rooms;
        }

        if ($num_beds > 0 && $num_revs > 2) {
            $data[$key]['avg_rating'] = $avg_rating;
            $data[$key]['facility_ratio'] = $facility_ratio = $num_revs / $num_beds;

            $datetime = new DateTime($record['max_created']);
            $now = new DateTime();
            $interval = $datetime->diff($now);
            $yrs = $interval->y;

            if ($yrs < 1) {
                $rankingValue = $avg_rating - ((1 - $facility_ratio - 0.5) / 0.95);
            } elseif ($yrs > 1 && $yrs < 2) {
                $rankingValue = $avg_rating - ((1 - $facility_ratio - 0.5) / 0.95);
                $rankingValue = $rankingValue * .975;
            } else {
                $rankingValue = $avg_rating - ((1 - $facility_ratio - 0.5) / 0.95);
                $rankingValue = $rankingValue * .95;
            }

            $data[$key]['rank_score'] = $rankingValue;
        } else {
            $data[$key]['rank_score'] = "-99999999";
        }
    }
    
    if( $noOfRatingCount > 0 ){
        $totalOverageVillageRating = ($totalRatingVillage / $noOfRatingCount);
    }
    $overageVillageRating = round($totalOverageVillageRating);
    if(isset($overageVillageRating) || $overageVillageRating>0 || !empty($overageVillageRating)){
    	$ratingOverageVillage = $totalOverageVillageRating;
    }else{
    	$ratingOverageVillage = 0;
    }
    
    usort($data, function ($item1, $item2) {
        if ($item1['rank_score'] == $item2['rank_score'])
            return 0;
        return $item1['rank_score'] > $item2['rank_score'] ? -1 : 1;
    });

    $i = 1;

    foreach ($data as $key => $record) {
        if ($record['rank_score'] == "-99999999") {
            $data[$key]['ranking'] = NULL;
        } else {
            $data[$key]['ranking'] = $i;
            $i++;
        }
    }
    if ($order == "asc") {

        function sortByTitle($a, $b) {
            return strcmp(strtolower($a["title"]), strtolower($b["title"]));
        }

    }
    if ($order == "desc") {

        function sortByTitle($a, $b) {
            return strcmp(strtolower($b["title"]), strtolower($a["title"]));
        }

    }
    usort($data, 'sortByTitle');
}

/* * *********************************pagination code for the search product************** */
$start = 0;
$limit = 25;
if (isset($_REQUEST['id']) && !empty($_REQUEST['id'])) {
    $id = $_REQUEST['id'];
    $start = ($id - 1) * $limit;
} else {
    $id = 1;
}
$total = ceil($count / $limit);
/* * ********************************end of code of pagination for the search product********** */

if ($proSuburb) {// They have selected a suburb so get all of them
//now grab all suburbs
    $query = "SELECT prd.address_suburb AS suburbTitle ,extra.no_of_room,extra.no_of_beds "
            . " FROM " . _prefix("products") . " AS prd "
            . " LEFT JOIN " . _prefix("users") . " AS sp ON  prd.supplier_id= sp.id "
            . " LEFT JOIN " . _prefix("cities") . " AS city ON city.id=prd.city_id "
            . " LEFT JOIN " . _prefix("suburbs") . " AS suburb ON suburb.id=prd.suburb_id "
            . " Left join " . _prefix("pro_plans") . " AS prpn ON prpn.pro_Id =prd.id AND prpn.current_plan = 1 "
            . " Left join " . _prefix("membership_prices") . " AS memp ON prpn.plan_Id =memp.id"
            . " LEFT JOIN " . _prefix("pro_extra_info") . " AS pein ON pein.pro_id=prd.id"
            . " LEFT JOIN " . _prefix("extra_facility") . " AS extra ON extra.product_id=prd.id"
            . " " . $condition_without_suburb . " LIMIT 0,300 "; //" LIMIT {$start},{$limit}"; 
// Save query here
//print_r($pathInfo['call_parts'][1]);exit;
    $res = $db->sql_query($query);
    $data_suburbs = $db->sql_fetchrowset($res);
}


//echo count($data, COUNT_RECURSIVE);
//if(count($data, COUNT_RECURSIVE)==49){
if ($count == 1) {
    $facilityid = $data[0][0]; //['title'];
    //echo $facilityid;
    only_one_result($facilityid);
}

$countData = $db->sql_numrows($res);

$suburb_array = array();
if ($proSuburb) {// They have selected a suburb so get all of them
//echo "FINDING ALL SUBURBS";
    foreach ($data_suburbs as $key => $value) {
        if ($value['suburbTitle']) {
            $suburb_array[$value['suburbTitle']] = trim(ucwords($value['suburbTitle']));
        }
    }
}

foreach ($data as $key => $value) {

    if ($value['suburbTitle'] && !$proSuburb) {
        $suburb_array[$value['suburbTitle']] = $value['suburbTitle'];
    }

    $resultSet[] = array($value['title'], $value['latitude'], $value['longitude']);
    /* update facility review sataus if admin doesn't response for approval mail.  */

    $querys = " SELECT status, created FROM " . _prefix("feedbacks")
            . " WHERE product_id =" . $value['id'] . " AND is_manual = 0 AND deleted = 0 order by created desc limit 0,1";

    $mod_review = $db->sql_query($querys);
    $dataplans = $db->sql_fetchrow($mod_review);
    $status = $dataplans['status'];
    $created = date('Y-m-d H:i:s', strtotime($dataplans['created']) + 24 * 60 * 60);
    $current_time = date('Y-m-d H:i:s', time());

    if ($current_time >= $created) {

        $status = array(
            'status' => '1',
        );
        $where = "where product_id = " . $value['id'] . " AND is_manual = 0 AND deleted = 0";
        $update_result = $db->update(_prefix('feedbacks'), $status, $where);
        //echo '<span style="color:red;">review auto updated</span> '. $created .' <br/>';
    }
}



$latLangJson = json_encode($resultSet);
$suburb_array = array_unique($suburb_array);
asort($suburb_array);
//print_r($suburb_array);
?>

<script type="text/javascript">
    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
        $('#cityLable').focus(function () {
            $(this).val('');
            $("#city,#cityid,#suburbid,#restcareid, #restCare, #porCity, #proSuburb").val('');
        });
        $('.feedback_comment').hide();
        $('.view_comment').click(function () {
            $(this).parent().next('div.feedback_comment').slideToggle("slow", function () {
            });
        });
        $("input:checkbox").change(function () {
            var value = $(this).val();
            var n = $("input:checked").length;
            if (n > 3) {
                alert('Please select no more than 3 products/services to compare.');
                $(this).prop("checked", false);
            } else if ($(this).prop("checked") == true) {
                var id = 'productList-' + $(this).val();
                $("#" + id).val(value);
            } else if ($(this).prop("checked") == false) {
                var id = 'productList-' + $(this).val();
                $("#" + id).val('');
            }
        });
        $('#filter1').change(function () {
            $('#search').submit();
        });
        $('#filter2').change(function () {
            $('#search').submit();
        });

        $('input.checkbox_check').click(function () {
            $(".show_details").hide();
            if ($('input.checkbox_check').attr(':checked'))
                ;
            {
                var n = $("input:checked").length;
                if (n > 1) {
                    $(this).parent().next().show();
                }
            }
        });


        $('.compareButton').click(function () {
            var length = 0;
            $('#productForm input[type=hidden]').each(function () {
                if ($(this).val() != '') {
                    length = length + 1;
                }
            });
            if (length == 0) {
                alert('Please select products/services to compare');
            } else if (length == 1) {
                alert('Please select more product/services to compare');
            } else {
                $('#productForm').submit();
            }
        });
        var path = '<?php echo HOME_PATH; ?>';
        var offset = $('#cityLable').offset();
        var autoL = offset.left;
        var autoT = parseInt(offset.top) + parseInt(47);
        $("#cityLable").autocomplete({
            source: function (request, response) {
                $.ajax({
                    minLength: 3,
                    method: "post",
                    url: path + 'ajaxFront.php/' + new Date().getTime(),
                    data: {
                        'action': 'cities',
                        'maxRows': 12,
                        'name': request.term
                    },
                    success: function (result) {
                        var data = $.parseJSON(result);
                        response($.map(data, function (item) {
                            var suburb = (item.proSuburb == null) ? '' : ', ' + item.proSuburb;
                            var restCare = (item.restCare == null) ? '' : ', ' + item.restCare;
                            return {
                                label: item.title + '' + suburb + '' + restCare,
                                value: item.title + '' + suburb + '' + restCare,
                                cityName: item.title,
                                cityId: item.id,
                                restCare: item.restCare,
                                porCity: item.porCity,
                                proSuburb: item.proSuburb,
                                providers: item.providers
                            }
                        }));
                    }
                });
            },
            select: function (event, ui) {
                $this = $(this);
                setTimeout(function () {

                    $('#cityName').val(ui.item.cityName);
                    $('#cityid').val(ui.item.cityId);
                    $('#restCare').val(ui.item.restCare);
                    $('#porCity').val(ui.item.porCity);
                    $('#proSuburb').val(ui.item.proSuburb);
                    $('#cityLable').val(ui.item.label);
                    $('#providers').val(ui.item.providers);
                    $('#facilityType').focus();
                    $this.blur();
                    $('#search').submit();
                }, 1);


            },
            open: function (event, ui) {
                $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
                $('ul.ui-widget-content').css({
                    "z-index": "9999999",
                    "display": "block",
                    "top": autoT,
                    "left": autoL
                });
            },
            close: function () {
                $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
            }
        });
    });</script>

<form id="productForm"  name="productForm" action="<?php echo HOME_PATH . 'compare'; ?>" method="post">
                        <?php foreach ($data as $key => $record) { ?>
        <input type = "hidden" name = "productList[]" id = "productList-<?php echo $record['id']; ?>" value = ""/>
<?php }
?>
</form>

<?php if ($showmsg) { ?>
    <div class="row">
        <div class="col-md-12" style="margin-top: 20px;">
            <div class="alert alert-success text-center" style="margin-bottom:0px;"><a href="#" class="close" data-dismiss="alert">&#215</a><strong>Message!&nbsp;&nbsp;</strong>Thank you for your response.</div>
        </div>
    </div>
<?php } ?>

<div class="page-wrapper">
   
	 
        <div class="filter-wrapper style1 mar-top-100 half-map">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-7 col-md-12">
					
						<form id="search"  class="clearfix" name="search"  action="<?php echo HOME_PATH . 'search/for'; ?>" method="post">
                        <div class="form-group filter-group">
                            <div class="row">
                    
            <div class="col-md-12 col-md-offset-3 margin-top-20">
                <!--<h2>Search again</h2>-->
                <h2><?php $fac_string = implode(',', $facilityType);
                 $sub_string = implode(',', $suburbType); ?><?php echo $fac_string ? ListserviceCategoryNames($fac_string) . ' ' : 'Retirement Villages, Rest Homes and/or Aged Care facilities' ?><?php echo $sub_string ? ' in ' . ListsuburbNames($sub_string) . ' ' : '' ?><?php echo!empty($cityLable) ? ', ' . $cityLable : ' in ' . $cityName; ?><?php if (!$cityLable && !$cityName) {
    echo "New Zealand";
} ?>.</h2>
                <div class="input-group margin-bottom-40">
                    <input type="hidden" name="cityName" id="cityName" value=""><?php // echo $city;              ?>
                    <input type="hidden" name="city" id="city" value="<?php echo $city; ?>">
                    <input type="hidden" name="cityid" id="cityid" value="<?php echo $cityid; ?>">
                    <input type="hidden" name="proSuburb" id="proSuburb" value="<?php echo $proSuburb; ?>">
                    <input type="hidden" name="restCare" id="restCare" value="<?php echo $restCare; ?>">
                    <input type="hidden" name="providers" id="providers" value="<?php echo $providers; ?>">
                    <input type="text" style="width: 86%;" class="filter-input ui-autocomplete-input ui-widget" required autocomplete="off" value="<?php echo $cityLable; ?>" name="cityLable" id="cityLable"  placeholder="City, suburb or name of retirement village or rest care">
                    <span class="input-group-btn"><button style="margin-top: 26px;
                    height: 49px;" class="btn btn-danger" type="submit">SEARCH</button></span>
                </div>                
            </div>
     
                                <div class="col-md-6 col-12">
								<?php if (count($suburb_array) > 1) { ?>
								 <label for="suburbname" class="label_text" style="font-size: 18px; color: #000;">Filter by Suburb:</label>
								  <select class="filter-input a"  style="scroll-behavior: smooth;" name="suburbType[]" multiple="true" id="suburbType">
                                  <?php echo optionsService($suburb_array, $proSuburb); ?>
                                     </select>
                                     
						
						     <script type="text/javascript">
                            $(document).ready(function () {
                                $('#suburbType').multiselect();
                            })
                            </script>
			 
                                </div>
								
								 <?php } ?>
							 
							 <style>
							 .nice-select.filter-input.a {
                             display: none;
                                  }
                               button.multiselect.dropdown-toggle.btn.btn-default.filter-input {
                                border: 1px solid #ddd;
                                 margin-top: 25px;
                                  border-radius: 3px;
                                        width: 100%;
                                      padding: 10px 20px;
                                       height: 50px;
                                      width: 354px;
                                  background-color: WHITE;
                                        }
		
                                        .mar-top-10 {
                                            margin-top: 44px !important;
                                            }		</style>
                                <div class="col-md-6  col-12 text-right sm-left">
                                  <label for="name" style="font-size: 18px; color: #000;">Filter by facility type:</label>
                    <select class="filter-input a" name="facilityType[]" multiple="true" id="facilityType">
                 <?php echo serviceCatMultiple($facilityType); ?>
                    </select>
                    <script type="text/javascript">
                        $(document).ready(function () {
                            $('#facilityType').multiselect();
                        })
                    </script>
                                </div>	 
								 
							 
                                <div class="col-md-6  col-12">
                                    <div class="filter-sub-menu style1">
									
									
									  <select  id="filter2" name="filter2" title="Sort Result"   class="filter-input">
                        <option value="">Sort Result</option>
                        <option value="asc"  <?php echo isset($filter2) && ($filter2 == 'asc') ? 'selected' : ''; ?>>Name Ascending</option>
                        <option value="desc"  <?php echo isset($filter2) && ($filter2 == 'desc') ? 'selected' : ''; ?>>Name Descending</option>
                        <option value="rateDesc"  <?php echo isset($filter2) && ($filter2 == 'rateDesc') ? 'selected' : ''; ?>>Top Rated</option>
                        <option value="reviewDesc"  <?php echo isset($filter2) && ($filter2 == 'reviewDesc') ? 'selected' : ''; ?>>Top Reviews</option>
                        <option value="rankAsc"  <?php echo isset($filter2) && ($filter2 == 'rankAsc') ? 'selected' : ''; ?>>Top Ranking</option>
                    </select>
									
                         
                                    </div>
                                </div>
                                <div class="col-md-6  col-12 text-right sm-left">
                                    <button type="submit" class="btn v1 mar-top-10">UPDATE</button>
                                </div>
                            </div>
			 
      
            <div class="col-md-2 col-sm-4 col-xs-7">
                <div class="form-group">
                    <?php if ($countData > 0) { ?><label for="update" class="label_text">Compare&nbsp;facilities:</label><br>

                        <button class="btn btn-warning compareButton" id="compareButton" type="button" style="margin-top: 0px;">Compare</button>
                    <?php } ?>
                </div>
            </div>
                        </div>
						</form>
                        <div class="row mar-top-25 pad-bot-30 align-items-center">
                            <div class="col-lg-4 col-sm-4 col-12">
                                 
                            </div>
                            <div class="col-lg-8 col-sm-8 col-12">
                                <div class="item-element res-box  text-right xs-left">
                                    <p>Showing <span><?php echo $count; ?></span> Listings</p>
                                </div>
                            </div>
                        </div>
					<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12 community_wrapper">	
						<?php if ($city_keyword && $city_description_title && $city_description) { ?>
            <div class="row articles_container"> 
                <div class="col-md-12">
                    <div class="association-section">
                        <div class="associate-content-box">
                            <h1 style="margin-left: 0px;"><?php echo  $city_description_title ?></h1>
                            <p><?php echo  str_ireplace($city_keyword, "<strong>" . $city_keyword . "</strong>", $city_description); ?>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>

            <?php if ($provider_description_title && $provider_description) { ?>
            <div class="row articles_container"> 
                <div class="col-md-12">
                    <div class="association-section">
                        <div class="associate-content-box margin-left-right">
                            <h1 style="margin-left: 0px;"><?php echo  $provider_description_title ?></h1>
                            <p><?php echo  $provider_description ?></p>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>     
	    </div>	
			</div>	
			<!--START LISTING-->
			 <div itemprop="aggregateRating" itemscope="" itemtype="http://schema.org/AggregateRating">
            <meta itemprop="bestRating" content="5">
            <meta itemprop="worstRating" content="0">
            <span style="display:none;" itemprop="itemReviewed">
                <?php 
                $fac_string = implode(',', $facilityType);
                $sub_string = implode(',', $suburbType); 
                echo $fac_string ? ListserviceCategoryNames($fac_string) . ' ' : 'Retirement Villages, Rest Homes and/or Aged Care facilities';
                echo $sub_string ? ' in ' . ListsuburbNames($sub_string) . ' ' : '';
                echo !empty($cityLable) ? ', ' . $cityLable : ' in ' . $cityName; 
                if (!$cityLable && !$cityName) {
                    echo "New Zealand";
                } 
                ?>.
            </span>
            <span class="text-hide" content="<?php echo round($ratingOverageVillage,1); ?>" data-rating="<?php echo $ratingOverageVillage; ?>" itemprop="ratingValue"><?php echo round($ratingOverageVillage,1); ?></span>
            <span class="text-hide"  itemprop="reviewCount"><?php echo $noOfRatingCount; ?></span>
        </div>
		
                        <div class="item-wrapper">
                            <div class="tab-content">
                                <div id="grid-view" class="tab-pane active  product-grid">
                                    <div class="row">
									
									 <?php
                                    // MAKE SURE NO DUPLICATIONS

                                    if (count(array_filter($data)) > 0) {

                                        record_mtime("/modules/search/search.php display search results line 1358");

                                        $i = 0;
                                        // LIMIT {$start},{$limit}"; 
                                        $output = array_slice($data, $start, $limit);
                                        foreach ($output as $key => $record) 
										{
                                            $latest_positive_feedback_query = "SELECT * FROM `ad_feedbacks` WHERE product_id = {$record["id"]} AND overall_rating > 3.5  AND pros <> '' and created > date_sub(now(), interval 12 month)
                                             ORDER BY `ad_feedbacks`.`created` DESC limit 2";
                                            ?>

							<div class="col-xl-6 col-lg-12" id="mapping_<?php echo ++$i; ?>">
                                            <div class="trending-place-item">
                                                <div class="trending-img">
												<?php echo  show_image($record['image_enable'], $record['id'], $record['certification_service_type'], $record['quick_url']) ?>
												<?php
                                        $h_num = 4; // not upgraded
                                        if ($record['image_enable']) {
                                            //$h_num = 2;
											$h_num = 4;
                                        }
                                        ?>                             
                                                </div>
                                                <div class="trending-title-box">
                                                   <h<?php echo  $h_num ?>><?php
                                        echo '<a href="' . HOME_PATH . $record['quick_url'] . '">' . ucwords(strtolower($record['title'])) . "</a>";
                                        //echo  $record['certification_service_type'];
                                        if ($record['certification_service_type'] == "Home Services") {
                                            echo '<div class="hs"> </div>';
                                        } if ($record['certification_service_type'] == "Aged Care") {
                                            echo '<div class="ac"> </div>';
                                        } if ($record['certification_service_type'] == "Retirement Village") {
                                            echo '<div class="rv"> </div>';
                                        }
                                        ?></h<?php echo  $h_num ?>>
                                                    <div class="customer-review">
                                                        <div class="rating-summary float-left">
														<?php $ab=$record['id']/100; 
															   $ab;
															
															?>  
                                                            <div class="rating-result" title="	<?php
															    
															echo $ab;
															
															?>%">
															<?php echo OverAllNEWRatingProduct($record['id']); ?>
															  
                                                            </div>
                                                        </div>
                                                        <div class="review-summury float-right">
                                                            <p> <a href="<?php echo  HOME_PATH . $record['quick_url'] ?>#reviews"><?php echo overAllRatingProductCount($record['id']); ?> Reviews<a> </p>
                                                        </div>
                                                    </div>
												           <p> 
                                         <?php if ($record['ranking'] == NULL) { ?>
                                                <a style="    color: #f15922;" href="javascript:void(0)">Not Ranked Yet</a>
                                                <i class="fa fa-question-circle" data-toggle="tooltip" title="Facilities that are 'not ranked' must have a minimum of 3 reviews to give a ranking. AgedAdvisor rankings also take into account the number of dwellings or beds the facility has. The higher the number of reviews to no. of dwellings / beds, then the higher the ranking accuracy."></i>
                                                 <?php } else { ?>
                                                <a  style="    color: #f15922;" href="javascript:void(0)" ># <?php echo  $record['ranking']; ?>  of  <?php echo  $count; ?> facilities. </a>
                                                <!--<br>
                                                <a href="javascript:void(0)">Avg Rating : <?php echo  $record['avg_rating'] ?></a>
                                                <br> 
                                                <a href="javascript:void(0)">Facility Ratio : <?php echo  $record['facility_ratio'] ?>
                                                <br>                                
                                                <a href="javascript:void(0)" >Rank Score :  <?php echo  $record['rank_score']; ?></a>-->
                    <?php } ?>                                                                                     
                                        </p>
                                                    <ul class="trending-address">
                                                        <li><i class="ion-ios-location"></i>
                                                            <p><?php echo $record['city'];
                                                    echo!empty($record['suburbTitle']) ? ', ' . $record['suburbTitle'] : ''; ?></p>
                                                        </li>
                                                        <li><i class="ion-ios-home"></i>
                                                            <p><?php echo productServices($record['id']); ?></p>
                                                        </li>
                                                        <li> 
                           
                                                        </li>
                                                    </ul>
                                                    <div class="trending-bottom pad-bot-30">
                                                        <div class="trend-left float-left">
                                                    
                                                            <li><input type="Checkbox" name="product[]" id="product-<?php echo $record['id']; ?>" value="<?php echo $record['id']; ?>" class="checkbox_check"><label style="font-weight:normal; font-size:15px;" for="product-<?php echo $record['id']; ?>"> Check to Compare</label></li>
															
															<li class="show_details form-group"  id="show_details" style="display: none">
                                            <button class="btn btn-warning compareButton" id="compareButton" type="button" style="margin-top: 0px;">Compare</button>
                                        </li>

                                                        </div>
                                                        <div class="trend-right float-right" >
                                                         <a style="color: #f15922;" href="<?php echo HOME_PATH . 'rating?spId=' . base64_encode($record['supplier_id']) . '&prId=' . base64_encode($record['id']) ?>">Add Review</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
										

               
                <!-- End Inner Results -->
    <?php
    }
    $i++;

    record_mtime("/modules/search/search.php display search results line 1358");
} else {
    echo '<div class="alert alert-warning" role="alert"><h3>No record Found</h3>Can&#39;t find the facility you&#39;re after? Try typing in the first few letter of the facilities name or try searching under city or suburb. Otherwise <a href="' . HOME_PATH . 'contact_us' . '" title="Contact us">contact us</a> and we&#39;ll try to find it for you.</div>';
}
?> 
                                        
                                    </div>
                                </div>
                                 
                                <!--pagination starts-->
                                <div class="post-nav nav-res pad-bot-80">
                                    <div class="row">
                                        <div class="col-md-8 offset-md-2  col-xs-12 ">
                                            <div class="page-num text-center">
                                                <ul>
												<?php
if ($id > 1) {
    echo "<li><a href='?id=" . ($id - 1) . "'> Previous</a></li>";
}

for ($i = 1; $i <= $total; $i++) {
    if ($i == $id) {
        echo "<li class='active'><a>" . $i . "<a/></li>";
    } else {
        echo "<li><a href='?id=" . $i . "'>" . $i . "</a></li>";
    }
}
if ($id != $total) {
    echo "&nbsp;&nbsp;<a href='?id=" . ($id + 1) . "'>Next<a>";
}
?>

                                                    
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--pagination ends-->
                            </div>
                        </div>
                    </div>
                    <div class="explore__map-side">
                        <div id="map_right_listing"></div>
                    </div>
                </div>
            </div>
        </div>
        <!--Listing section ends-->
        <!-- Scroll to top starts-->
        <span class="scrolltotop"><i class="ion-arrow-up-c"></i></span>
        <!-- Scroll to top ends-->
    </div>
	

 
<!--=== End Search Block Version 2 ===-->
 
 

<?php
//echo $upgrade = upgradeproduct_logo($record['id']);
if (ISSET($cityLable) && $cityLable) {
    $city_searched = $cityLable;
} else {
    $city_searched = $cityName;
}
?>

<script>
    fbq('track', 'SearchPage', {
        <city>: <<?php echo  $city_searched ?>>});
</script>
<?php
if (ISSET($last_map_script)) {
    echo $last_map_script;
}
?>
<!-- NEW CSSs--->
<!--<script type="text/javascript" src="<?php echo HOME_PATH;?>includes/js/plugin.js"></script>   --> 
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD_8C7p0Ws2gUu7wo0b6pK9Qu7LuzX2iWY&amp;libraries=places&amp;callback=initAutocomplete"></script>
<script type="text/javascript" src="<?php echo HOME_PATH; ?>includes/js/markerclusterer.js"></script>
<script type="text/javascript" src="<?php echo HOME_PATH; ?>includes/js/maps.js"></script>
<script type="text/javascript" src="<?php echo HOME_PATH; ?>includes/js/infobox.min.js"></script>


<script type="text/javascript" src="<?php echo HOME_PATH;?>includes/js/main.js"></script>
<!-- END NEW CSSs--->