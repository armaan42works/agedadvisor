 <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.15&amp;sensor=false&amp;libraries=places"></script>
<?php

function cmp($a, $b) {
    if ($a['ratingValue'] == $b['ratingValue']) {
        return 0;
    }
    return ($a['ratingValue'] < $b['ratingValue']) ? -1 : 1;
}

global $db;
$con = '';
if (!empty($cityid)) {
    $cityCon = ' prd.city_id= ' . $cityid . ' ';
}
if (!empty($cityid) && !empty($cityName)) {
    $cityLable = $cityName;
}
if (!empty($suburbid)) {
    $suburbCon = ' AND prd.suburb_id= ' . $suburbid . ' ';
}
if (!empty($restcareid)) {
    $restcareCon = ' AND prd.restcare_id= ' . $restcareid . ' ';
}

if (!empty($cityid) || !empty($suburbid) || !empty($restcareid)) {
    $con = "AND ( $cityCon  $suburbCon  $restcareCon )";
}

if (empty($cityid) && !empty($cityLable)) {
    $con = "AND ( prd.city_id =0 )";
}

if (empty($cityid) && empty($cityLable) && empty($restcareid) && empty($facilityType)) {
    $con = "AND ( prd.city_id =0 )";
}

//prd($con);
//
//if (!empty($city) && !empty($suburbid) && !empty($restcareid)) {
//    $con = "(prd.city_id= '$cityid' AND prd.suburb_id='$suburbid' AND prd.restcare_id='$restcareid')";
//} elseif (!empty($city) && !empty($suburbid) && empty($restcareid)) {
//    $con = "(prd.city_id= '$cityid' AND prd.suburb_id='$suburbid')";
//} elseif (!empty($city) && empty($suburbid) && !empty($restcareid)) {
//    $con = "(prd.city_id= '$cityid' AND prd.restcare_id='$restcareid')";
//}elseif (!empty($city) && empty($suburbid) && !empty($restcareid)) {
//    $con = "(prd.city_id= '$cityid' AND prd.restcare_id='$restcareid')";
//} else {
//    $con = "(prd.city_id= '$cityid')";
//}
//$searchCondition =(!empty($suburbid)&&!empty($restcareid))?:;
$allFacilityType = implode(", ", $facilityType);
if (!isset($allFacilityType)) {
    $allFacilityType = $facilityType;
}
if (isset($facilityType) && $facilityType != '') {
    $condition = " where prd.deleted = 0  $con AND ( proService.service_id IN ($allFacilityType))";
} else {
    $condition = " where prd.deleted = 0  $con ";
}
if (empty($filter2) AND empty($filter2)) {
    $filter2 = 'ASC';
}
if (!empty($filter1)) {
    $order = $filter1;
} else if (!empty($filter2) && $filter2 != 'rateDesc') {
    $order = $filter2;
} else {
    $order = '';
}
//$order = !empty($filter1) ? $filter1 : $filter2;
//$query = "SELECT prd.id, prd.title,city.title AS city, prd.description, prd.image, srv.name, prd.status ,prd.latitude, prd.longitude "
//        . " FROM " . _prefix("products") . " AS prd "
//        . " Left join " . _prefix("services") . " AS srv ON srv.id=prd.facility_type "
//        . " Left join " . _prefix("cities") . " AS city ON city.id=prd.city_id "
//        . " " . $condition . " ORDER BY  prd.title $order ";
//
//$res = $db->sql_query($query);
//$count = $db->sql_numrows($res);
//$i = 1;
//if (isset($_POST['pageId']) && !empty($_POST['pageId'])) {
//    $id = $_POST['pageId'];
//    $i = $i + SEARCH_PAGE_PER_NO * $id;
//} else {
//    $id = '0';
//    $i = 1;
//}
//$pageLimit = SEARCH_PAGE_PER_NO * $id;
if (!empty($filter2) && $filter2 == 'rateDesc') {

    $query1 = "SELECT prd.id,prd.supplier_id,'' AS rateDesc,sp.first_name AS spFName, sp.last_name AS spLName, prd.title  ,city.title AS city, prd.description, prd.image,prd.status  ,prd.latitude, prd.longitude,prd.quick_url "
            . " FROM " . _prefix("products") . " AS prd "
            . "LEFT JOIN " . _prefix("users") . " AS sp ON  prd.supplier_id= sp.id "
            . " Left join " . _prefix("cities") . " AS city ON city.id=prd.city_id "
            . " " . $condition . " ORDER BY  prd.title $order ";

    $res1 = $db->sql_query($query1);
    $data = $db->sql_fetchrowset($res1);
//    prd($data);

    foreach ($data as $key => $record) {
        $data[$key]['ratingValue'] = overAllRatingProduct($record['id']);
    }
    usort($data, "cmp");
} else {
    $query = "SELECT prd.id,prd.supplier_id,sp.first_name AS spFName, sp.last_name AS spLName, prd.title  ,city.title AS city,"
            . " prd.description, prd.image, prd.status  ,prd.latitude, prd.longitude,prd.quick_url "
            . " FROM " . _prefix("products") . " AS prd "
            . "LEFT JOIN " . _prefix("users") . " AS sp ON  prd.supplier_id= sp.id "
            . " Left join " . _prefix("cities") . " AS city ON city.id=prd.city_id "
            .  $condition . " ORDER BY  prd.title $order ";

    $res = $db->sql_query($query);
    $data = $db->sql_fetchrowset($res);
//    prd($query);
}

$countData = $db->sql_numrows($res);
//    $countData = count($data);
foreach ($data as $key => $value) {
    $resultSet[] = array($value['title'], $value['latitude'], $value['longitude']);
}
$latLangJson = json_encode($resultSet);
?>

<script type="text/javascript">
    $(function() {
        $('#cityLable').focus(function() {
            $(this).val('');
            $("#city,#cityid,#suburbid,#restcareid, #restCare, #porCity, #proSuburb").val('');
        });
        $('.feedback_comment').hide();
        $('.view_comment').click(function() {
            $(this).parent().next('div.feedback_comment').slideToggle("slow", function() {
            });
        });
        $("input:checkbox").change(function() {
            var value = $(this).val();
            var n = $("input:checked").length;
            if (n > 3) {
                alert('Please select up to 3 products/services to compare.');
                $(this).prop("checked", false);
            } else if ($(this).prop("checked") == true) {
                var id = 'productList-' + $(this).val();
                $("#" + id).val(value);
            } else if ($(this).prop("checked") == false) {
                var id = 'productList-' + $(this).val();
                $("#" + id).val('');
            }
        });
        $('#filter1').change(function() {
            $('#search').submit();
        });
        $('#filter2').change(function() {
            $('#search').submit();
        });
        $('#compareButton').click(function() {
            var length = 0;
            $('#productForm input[type=hidden]').each(function() {
                if ($(this).val() != '') {
                    length = length + 1;
                }
            });
            if (length == 0) {
                alert('Please select products/services to compare');
            } else if (length == 1) {
                alert('Please select the more products/services to compare');
            } else {
                $('#productForm').submit();
            }
        });
        var path = '<?php echo HOME_PATH; ?>';
        var offset = $('#cityLable').offset();
        var autoL = offset.left;
        var autoT = parseInt(offset.top) + parseInt(47);
        $("#cityLable").autocomplete({
            source: function(request, response) {
                $.ajax({
                    minLength: 3,
                    method: "post",
                    url: path + 'ajaxFront.php/' + new Date().getTime(),
                    data: {
                        'action': 'cities',
                        'maxRows': 12,
                        'name': request.term
                    },
                    success: function(result) {
                        var data = $.parseJSON(result);
                        response($.map(data, function(item) {
                            var suburb = (item.proSuburb == null) ? '' : ', ' + item.proSuburb;
                            var restCare = (item.restCare == null) ? '' : ', ' + item.restCare;
                            return {
                                label: item.title + '' + suburb + '' + restCare,
                                value: item.title + '' + suburb + '' + restCare,
                                // value: item.title + ', '+item.suburb+ ', '+item.restCare,
                                cityName: item.title,
                                cityId: item.id,
                                restCare: item.restCare,
                                porCity: item.porCity,
                                proSuburb: item.proSuburb
                            }
                        }));
                    }
                });
            },
            select: function(event, ui) {
                $('#cityName').val(ui.item.cityName);
                $('#cityid').val(ui.item.cityId);
                $('#restCare').val(ui.item.restCare);
                $('#porCity').val(ui.item.porCity);
                $('#proSuburb').val(ui.item.proSuburb);
                $('#cityLable').val(ui.item.label);
                $('#facilityType').focus();
                $('#search').attr('action', 'search/search');
            },
            open: function(event, ui) {
                $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
                $('ul.ui-widget-content').css({
                    "z-index": "9999999",
                    "display": "block",
                    "top": autoT,
                    "left": autoL
                });
            },
            close: function() {
                $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
            }
        });
    });</script>

<!--MAIN BODY CODE STARTS-->
<div class="container">
    <div class="row articles_container">
        <div class="col-md-12 col-sm-12 col-xs-12 padding_none">

            <!---left section starts-->
            <div style="display:none;" id="mapData" class="mapData"><?php echo $latLangJson; ?></div>

            <form id="productForm"  name="productForm" action="<?php echo HOME_PATH . 'compare'; ?>" method="post">
                <?php foreach ($data as $key => $record) { ?>
                    <input type = "hidden" name = "productList[]" id = "productList-<?php echo $record['id']; ?>" value = ""/>
<?php }
?>
            </form>

            <form id="search"  name="search" action="<?php echo isset($facilityType) ? '' : 'search/search'; ?>" method="post">
                <div class="col-md-9 col-lg-9 col-sm-12 col-xs-12 pull-left search_wrapper" style="padding-left:0px;">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 padding_left">
                        <div class="form-group">
                            <label for="name" class="label_text">City, suburb or name of retirement village</label>
                            <input type="hidden" name="cityName" id="cityName">
                                        <input type="hidden" name="cityid" id="cityid">
                                        <input type="hidden" name="restCare" id="restCare">
                                        <input type="hidden" name="porCity" id="porCity">
                                        <input type="hidden" name="proSuburb" id="proSuburb">
                            <input type="text" class="form-control input-lg ui-autocomplete-input ui-widget"  autocomplete="off" value="<?php echo $cityLable; ?>" name="cityLable" id="cityLable"  placeholder="City, suburb or name of retirement village or rest care">
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 padding_left">
                        <div class="form-group">
                            <label for="name" class="label_text">Facility type</label>
                            <select class="form-control input-lg" id="facilityType" multiple="multiple" name="facilityType" >
<?php echo serviceCatMultiple($facilityType); ?>
                            </select>
                        </div>
                    </div>
                    <script type="text/javascript">
                        $(document).ready(function() {
                            $('#facilityType').multiselect();
                        })
                    </script>
                    <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 pull-right">
                        <div class="form-group">
                            <button class="btn btn-warning center-block btn_search" type="submit">SEARCH</button>
                        </div>
                    </div>
                    <!--</form>-->

                    <h1 style="padding-left:5px; margin-top:10px; font-size: 12px; color: #FF0000;" class="theme_color_inner pull-left"><?php echo $countData; ?> Result(s) found for &nbsp;“<?php echo $facilityType ? serviceCategoryName($facilityType) . ' facility ' : ' All facilities ' ?><?php echo!empty($cityLable) ? ' in ' . $cityLable : ' in ' . $cityName; ?>”</h1>
<?php if ($countData > 0) { ?>
                        <div class="pull-right col-md-5 col-sm-12 col-xs-12 text-right" style="margin-top:0px; margin-bottom: 10px;">
                            <div class="form-group" style="margin:0px;">
                                <!--<label for="firstname" class="control-label label_text display_results" style="float:left;">Sort by :</label>-->
                                <div class="col-sm-12 col-xs-12 padding_right">
                                    <div class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration padding_right">
                                        <label for="name" class="label_text" style="padding-top:10px;">SORT BY</label></div>
                                    <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 padding_none pull-right">    
                                        <select  id="filter2" name="filter2" title="Sort Result"   class="form-control">
                                            <option value="">Sort Result</option>
                                            <option value="asc"  <?php echo isset($filter2) && ($filter2 == 'asc') ? 'selected' : ''; ?>>Name Ascending</option>
                                            <option value="desc"  <?php echo isset($filter2) && ($filter2 == 'desc') ? 'selected' : ''; ?>>Name Descending</option>
                                            <option value="rateDesc"  <?php echo isset($filter2) && ($filter2 == 'rateDesc') ? 'selected' : ''; ?>>Top Rated</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

<?php } ?>
                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 pull-left container_search" style="margin-top:10px;">
                        <ul id="myTab" class="nav nav-tabs tab_navbar">
                            <li class="active"><a href="#home" data-toggle="tab">List View</a></li>
                            <li><a onclick="loadMap();" href="#ios" data-toggle="tab">Map View</a></li>
                            <li class="pull-right">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-right pull_right_sort text-right">
                                    <div class="form-group form_group_bg form_group_bg_2">
                                        <?php if ($countData > 0) { ?>
                                            <button class="btn btn-warning center-block btn_search" id="compareButton" type="button" style="margin-top: 10px;">Compare</button>
<?php } ?>
                                    </div>
                                </div>
                            </li>
                        </ul>
                        <div id="myTabContent" class="tab-content tab_container">
                            <div class="tab-pane fade in active" id="home">
                                <?php
                                if (count(array_filter($data)) > 0) {
                                    foreach ($data as $key => $record) {
                                        $cityLast = empty($cityLast) ? $record['city'] : $cityLast;
                                        ?>
                                        <div class="posted_col tab_posted">
                                            <?php
                                            $pathCheck = DOCUMENT_PATH . 'admin/files/gallery/images/' . $record['image'];
                                            if (@getimagesize($pathCheck)) {
                                                ?>
                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 col_lg_bg_1">
                                                    <a href="<?php echo HOME_PATH .$record['quick_url']; ?>" class="pull-left img_post"><img src="<?php echo HOME_PATH . 'admin/files/gallery/images/' . $record['image'] ?>" class="img-responsive"></a>
                                                </div>
        <?php } ?>

                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 pdding_none search_col">
                                                <h2 class="col_h2"><?php echo '<a href=' . HOME_PATH .$record['quick_url'] . ">" . $record['title'] . "</a>"; ?></h2>
                                                <p style="margin:0;"><?php echo overAllRatingProduct($record['id']); ?></p>
                                                <div class="posted"><span>Service Type: <?php echo productServices($record['id']); ?></span><br><span>Location: <?php
                                                        echo $record['city'];
                                                        echo!empty($record['suburbTitle']) ? ', ' . $record['suburbTitle'] : '';
                                                        ?></span><br/><span>Supplier Name: <?php echo $record['spFName'] . " " . $record['spLName']; ?></span></div>
                                                <p><?php
                                                    echo substr($record['description'], 0, 60);
                                                    echo strlen($record['description']) > 60 ? '...' : ''
                                                    ?></p>
                                                <span class="text-left"><a target="_blank" href="<?php echo HOME_PATH . 'rating?spId=' . base64_encode($record['supplier_id']) . '&prId=' . base64_encode($record['id']) ?>">Add Review/Rate</a></span> |
                                                <span class="text-left"><a href="<?php echo HOME_PATH; ?>search/viewReview?id=<?php echo md5($record['id']) ?>">Read Reviews for this facility <i class="fa fa-users"></i>&nbsp;<?php echo overAllRatingProductCount($record['id']); ?></a></span>


                                            </div>
                                            <p class="pull-right"><input type="Checkbox" name="product[]" id="product-<?php echo $record['id']; ?>" value="<?php echo $record['id']; ?>" class="pull-right"><label style="font-weight:normal;" for="product-<?php echo $record['id']; ?>"> Check to Compare&nbsp;</label></p>
                                        </div>
                                        <?php
                                        $i++;
                                    }
                                    ?>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 displaying">
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 pull-left display_results"></div>
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 pull-right display_results"><a title="Go to Top" class="pull-right" href="javascript:window.scrollTo(0,300)"><i style="font-size: 30px; padding-left: 50px;" class="fa fa-level-up"></i></a></div>
                                    </div>
                                    <?php
                                } else {
                                    echo '<div class="posted_col tab_posted"><table class="table table-bordered table_bord"><tbody><tr><td class="text-center">Can&#39;t find the facility you&#39;re after? Try typing in the first few letter of the facilities name or try searching under city or suburb. Otherwise <a href="' . HOME_PATH . 'contact_us' . '" title="Contact us">contact us</a> and we&#39;ll try to find it for you.</td></tr></tbody></table></div>';
                                }
                                ?>
                            </div>
                            <div class="tab-pane fade" id="ios">
<?php if (count(array_filter($data)) > 0) { ?>

                                    <div id="myMap" class="posted_col tab_posted img-responsive map_search">

                                    </div>

                                    <?php
                                } else {
                                    echo '<div class="posted_col tab_posted"><table class="table table-bordered table_bord"><tbody><tr><td class="text-center">No record Found</td></tr></tbody></table></div>';
                                }
                                ?>
                            </div>
                            <div class="tab-pane fade" id="jmeter">
                                <p>jMeter is an Open Source testing software. It is 100% pure
                                    Java application for load and performance testing.</p>
                            </div>
                            <div class="tab-pane fade" id="ejb">
                                <p>Enterprise Java Beans (EJB) is a development architecture
                                    for building highly scalable and robust enterprise level
                                    applications to be deployed on J2EE compliant
                                    Application Server such as JBOSS, Web Logic etc.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <?php
            $today = date('Y-m-d');
            if ((isset($cityid) && !empty($cityid)) AND (isset($cityName) && !empty($cityName))) {
                $typeCondition = " AND (" . (!empty($cityid) ? "`city_id` = $cityid " : '') . " " . (!empty($cityName) ? "  OR city_name LIKE  '%" . $cityName . "%' " : '') . " ) ";
            } else if ($cityLable) {
                $typeCondition = " AND city_name LIKE   '%" . $cityLable . "%' ";
            } else if ($facilityType) {
                $typeCondition = ' AND `service_category_id` =  ' . $facilityType . ' ';
            } else {
                $typeCondition = ' ';
            }
            $query = "SELECT title, image FROM  " . _prefix("ads_masters") . "  WHERE `from_date` <='" . $today . "' AND `to_date` >= '" . $today . "' AND  `approve_status` =1 AND `status` = 1 AND `deleted` = 0 " . $typeCondition . " order by rand() limit 5";
            $res = $db->sql_query($query);
            $data = $db->sql_fetchrowset($res);
//                        prd($query);
            ?>

            <!---Right section starts-->
            <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12 pull-right pdding_none article_innerbg">
                <div class="right_box">
                    <?php
                    foreach ($data as $key => $record) {
                        $file = DOCUMENT_PATH . '/admin/files/ads_image/' . $record['image'];
                        if (file_exists($file)) {
                            ?>
                            <div class="col_1 text-center"><img title="<?php echo $record['title']; ?>" style="width: 254px;height: 250px;" class="img-responsive" src="<?php echo HOME_PATH; ?>admin/files/ads_image/<?php echo $record['image']; ?>" alt="ads image"></div>
                            <?php
                        }
                    }
                    ?>
                </div>
            </div>

        </div>
    </div>
</div>
<!--MAIN BODY CODE ENDS-->

<!--<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>-->


<?php if ($countData > 0) { ?>

    <style>
        #myMap {min-height:275px; height:auto; width: 680px!important;}
    </style>
    <script type="text/javascript">

        var geocoder;
        var map;
        var address = '<?php echo!empty($cityName) ? $cityName : (!empty($cityLable) ? $cityLable : $cityLast); ?>' + ', New Zealand';
        function initialize() {
            geocoder = new google.maps.Geocoder();
            var latlng = new google.maps.LatLng(-41.211722, 175.517578);
            var myOptions = {
                zoom: 5,
                center: latlng,
                mapTypeControl: true,
                mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU},
                navigationControl: true,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            map = new google.maps.Map(document.getElementById("myMap"), myOptions);
            if (geocoder) {
                geocoder.geocode({'address': address}, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (status != google.maps.GeocoderStatus.ZERO_RESULTS) {
                            map.setCenter(results[0].geometry.location);

                            var infowindow = new google.maps.InfoWindow(
                                    {content: '<b>' + address + '</b>',
                                        size: new google.maps.Size(500, 273)
                                    });

                            var marker = new google.maps.Marker({
                                position: results[0].geometry.location,
                                map: map,
                                title: address
                            });
                            google.maps.event.addListener(marker, 'click', function() {
                                infowindow.open(map, marker);
                            });

                        } else {
                            alert("No results found");
                        }
                    } else {
                        alert("Geocode was not successful for the following reason: " + status);
                    }
                });
            }
        }
        /*       
         var places = jQuery.parseJSON($('#mapData').html());
         //var places = document.getElementById('mapData').innerHTML;
         //var places = [
         //['Bhubaneswar', 20.2960587, 85.8245398],
         //['Cuttack', 20.462521, 85.8829895],
         //['Puri', 19.8133822, 85.8314655],
         //['Konark', 19.8920686, 86.091184],
         //['Sambalpur', 21.466222, 83.9751639]
         //];
         //
         var mapOptions = {
         zoom: 5,
         center: new google.maps.LatLng(-35.960223, 159.785156)
         }
         var map = new google.maps.Map(document.getElementById('myMap'), mapOptions);
         function initialize() {
         setMarkers(map, places);
         }
         
         function setMarkers(map, locationsDemo) {
         var locations = jQuery.parseJSON($('#mapData').html());
         if (($('#mapData').length > 0) && ($('#mapData').html() != 'null')) {
         for (var i = 0; i < locations.length; i++) {
         var place = locations[i];
         var myLatLng = new google.maps.LatLng(place[1], place[2]);
         var marker = new google.maps.Marker({
         position: myLatLng, content: "$425K",
         title: place[0]
         });
         //                    marker.setContent(place[0]);
         marker.setMap(map);
         }
         }
         }
         google.maps.event.addDomListener(window, 'load', initialize);
         google.maps.event.trigger(map, 'resize');
         $(document).ready(function() {
         initialize();
         google.maps.event.trigger(map, 'resize');
         });*/
        function loadMap() {
            initialize();
            google.maps.event.trigger(map, 'resize');
            $("#myMap").animate({width: '680',
                right: '0',
            }, 280, function() {
                google.maps.event.trigger(map, 'resize');
            });
        }
    </script>
<?php } else { ?>
    <script type="text/javascript">
        function loadMap() {
        }
    </script>

    <style>
        #myMap {height: 100%; width: 100%;}
    </style>
    <?php
}
//$url = 'https://www.google.co.in/search?q=android%20site:test4live.com/';
////$url = 'https://www.google.co.in';
////$result = file_get_contents($url);
//
//
////  Initiate curl
//$ch = curl_init();
//// Disable SSL verification
//curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
//// Will return the response, if false it print the response
//curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//// Set the url
//curl_setopt($ch, CURLOPT_URL,$url);
//// Execute
//$result=curl_exec($ch);
//// Closing
//curl_close($ch);
//pr($result);
//var_dump(json_encode($result, true));
//prd($result);
// Will dump a beauty json :3
?>

