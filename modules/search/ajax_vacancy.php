<?php
if(!isset($_SESSION)){
	session_start();
}
include( "../../application_top.php" );

global $db;$add_subject='';

//$ip = $_SERVER['REMOTE_ADDR'];


function planType($pr_id) {
    global $db;
    $querys = " SELECT member.price , member.title,pln.plan_id as planId, pln.plan_start_date AS expiry   FROM " . _prefix("pro_plans") . " AS pln   "
            . " LEFT JOIN " . _prefix("membership_prices") . " AS member ON member.id=pln.plan_id "
            . " WHERE pln.pro_id =" . $pr_id . " AND pln.current_plan = 1 AND pln.status = 1 AND pln.deleted = 0 ORDER BY pln.id DESC";
    $planres = mysqli_query($db->db_connect_id, $querys);
    $plndata = mysqli_fetch_assoc($planres);
    return $plndata;
    exit();
}

function email_admin_error($error){
	
    $to = 'nigel@agedadvisor.co.nz';
    $headers = "MIME-Version: 1.0\n";
    $headers .= "Content-type: text/html; charset=iso-8859-1\n";
    $headers .= 'From:   ';            
    $headers .= "server@agedadvisor.co.nz";
    $headers .= "\n";
//    $headers .= "Cc: reviews@agedadvisor.co.nz\r\n";
        $result  = @mail( $to, 'User ID=0 found in enquiry table', $error, $headers );
}

function generatePassword( $length = 12, $strength = 6 ) {
    $vowels     = 'aeuy';
    $consonants = 'bdghjmnpqrstvz';
    if ( $strength > 1 ) {
        $consonants .= 'BDGHJLMNPQRSTVWXZ';
    }
    if ( $strength > 2 ) {
        $vowels .= "AEUY";
    }
    if ( $strength > 4 ) {
        $consonants .= '23456789';
    }
    if ( $strength > 8 ) {
        $consonants .= '@#$%';
    }
    $password = '';
    $alt      = time() % 2;
    for ( $i = 0; $i < $length; $i++ ) {
        if ( $alt == 1 ) {
            $password .= $consonants[ ( rand() % strlen( $consonants ) ) ];
            $alt = 0;
        } else {
            $password .= $vowels[ ( rand() % strlen( $vowels ) ) ];
            $alt = 1;
        }
    }
    return $password;
}

function get_userid( $email = '', $phone = '', $uname = '' ) {
    global $db;
    $userid = '';
    $email = trim($email);
    $phone = trim($phone);
    
    if ( $phone ) {
        $sql_query = "select id from ad_users where phone = '$phone' ";
    }
    if ( $email && $email != 'n/a' ) {
        $sql_query = "select id  from ad_users where email  = '$email'  ";
    }
    
    $res     = mysqli_query($db->db_connect_id,  $sql_query );
    $records = mysqli_fetch_assoc( $res );
    $rows    = mysqli_num_rows( $res );
    if ( $rows > 0 ) {
        $userid = $records[ 'id' ];
        $_SESSION['userId'] = $userid;
    }
    return $userid;
}
function get_count_of_enquiries($userId, $prod_id, $count_for_days = 30){
    // this function will check how many enquiries a user did in past $count_for_days -- Techorator
    global $db;
    $count = 0;
    $userId = htmlspecialchars($userId);
 
    $sql = "select count(distinct prod_id) count from ad_enquiries where user_id = {$userId} and time_of_enquiry between date_sub(now(), interval {$count_for_days} day) and now() and prod_id != {$prod_id}";    
    $result = mysqli_query($db->db_connect_id, $sql);
    
    if($result){        
        $count = mysqli_fetch_assoc($result);
        $count = $count['count'];
    }
    if($count > 0){
        // if user already done enquires in past $count_for_days for different facilities this message will be returned and 
        // will append in email which is to be sent to facility owner
        return "<p><strong> Note: The enquirer has also requested infromation from {$count} other facilities near you.</strong></p>";
    }
    else
    {
        return "";
    }
}

function get_facility_name( $prod_id ) {
    global $db;
    $title     = '';
    $sql_query = "select title from ad_products where id = '$prod_id' ";
    $res       = mysqli_query($db->db_connect_id,  $sql_query );
    $records   = mysqli_fetch_assoc( $res );
    $rows      = mysqli_num_rows( $res );
    if ( $rows > 0 ) {
        $title = $records[ 'title' ];
    }
    return $title;
}

function get_facility_supplier_id( $prod_id ) {
    global $db;
    $$supplier_id = '';
    $sql_query    = "select supplier_id from ad_products where id = '$prod_id' ";
    $res          = mysqli_query($db->db_connect_id,  $sql_query );
    $records      = mysqli_fetch_assoc( $res );
    $rows         = mysqli_num_rows( $res );
    if ( $rows > 0 ) {
        $supplier_id = $records[ 'supplier_id' ];
    }
    return $supplier_id;
}

function get_taking_enquiry( $prod_id ) {
    global $db;
    $$supplier_id = '';
    $sql_query    = "select taking_enquiries from ad_enquiries where prod_id = '$prod_id' limit 1";
    $res          = mysqli_query($db->db_connect_id,  $sql_query );
    $records      = mysqli_fetch_assoc( $res );
    $rows         = mysqli_num_rows( $res );
    if ( $rows > 0 ) {
        if( 2 == $records[ 'taking_enquiries' ] ){
            $t_enq = TRUE;
        }else{
            $t_enq = FALSE;
        }
    }
    return $t_enq;
}

//$con = mysqli_connect( "localhost", "agedadvisor", "f3Cdc7~3" ) or die( "error in connection" );
//mysqli_select_db( "new_agedadvisor_live", $con );

$enquirer_id        = trim($_REQUEST[ 'enquirer_id' ]);
$uname              = trim($_REQUEST[ 'enquirer_name' ]);
$uemail             = trim($_REQUEST[ 'enquirer_email' ]);
$prId               = trim($_REQUEST[ 'prod_id' ]);
$vacanncy_tel       = trim($_REQUEST[ "enquirer_tel" ]);
$opt_in             = $_REQUEST[ "opt_in" ];
$msg                = $_REQUEST[ "enq_specifics" ];
$valid_mobile_count = strlen( trim( $vacanncy_tel ) );
if(filter_var($uemail, FILTER_VALIDATE_EMAIL) ){
    $valid_email = 1;
}
//$valid_email        = preg_match( "^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $uemail );
//var_dump($uemail);
//var_dump($valid_email); 


if(ISSET($_GET['test'])){}
else if($msg){
	$job_request=FALSE;
	if(stristr($msg,'employ')){$job_request=TRUE;}
	if(stristr($msg,'job')){$job_request=TRUE;}
	if(stristr($msg,'work')){$job_request=TRUE;}
	if(stristr($msg,'position')){$job_request=TRUE;}
	if(stristr($msg,'careg')){$job_request=TRUE;}
	if(stristr($msg,'carer')){$job_request=TRUE;}
	if(stristr($msg,'career')){$job_request=TRUE;}
 
 if($job_request){
	 // WARNING they looking for a job
	 echo "2";
	 //echo "This appears to be an employment request and will not be seen by the correct person. Click here if this is not the case.";
	 exit;
 }	
	
}




if ( $uemail == '' ) {
    $valid_email = 1;
    $uemail      = 'n/a';
}

if(!empty($enquirer_id )){
    $founduser = $enquirer_id;    
}else{
    $founduser = get_userid( $uemail, $vacanncy_tel );
}

if ( !$founduser ) {
    $newuname = str_ireplace( ' ', '_', $uname );
    $newuname = $newuname . generatePassword( 4 );
    $found    = FALSE;
    $count    = "";
    while ( !$found ) {
        if ( get_userid( '', '', $newuname . $count ) ) {
            $found = FALSE;
            $count = $count + 1;
        } else {
            $found    = TRUE;
            $newuname = "$newuname$count";
        }
    }
    $cut        = explode( ' ', $uname );
    $first_name = $cut[ 0 ];
    $last_name  = '';
    if ( ISSET( $cut[ 1 ] ) ) {
        $last_name = $cut[ 1 ];
    }
    if ( ISSET( $cut[ 2 ] ) ) {
        $last_name = $cut[ 2 ];
    }
    if ( ISSET( $cut[ 3 ] ) ) {
        $last_name = $cut[ 3 ];
    }
        
    $fields        = array(
        'first_name' => $first_name,
        'last_name' => $last_name,
        'email' => $uemail,
        'phone' => $vacanncy_tel,
        'password' => '',
        'user_name' => $newuname,
        'reviewer_TYPE' => 6,
        'user_type' => '0',
        'deleted' => '0',
        'validate' => '',
        'agree_terms' => '',
        'email_updates_facility' => $opt_in 
    );
    $insert_result = $db->insert( _prefix( 'users' ), $fields );
    $founduser     = $db->last_id();                
    $_SESSION['userId'] = $founduser;
}


if ( $opt_in == 1 ) {
    $update_fields = array(
         'email_updates_facility' => $opt_in 
    );
    $where = "where id= '$founduser' ";
    $insert_result = $db->update( _prefix( 'users' ), $update_fields, $where );
}

 $not_taking_enquiry = get_taking_enquiry($prId);  

if($not_taking_enquiry){
    
$reason = "NOT TAKING ENQUIRIES";    
$facility_name  = get_facility_name( $prId );
$supplier_id    = get_facility_supplier_id( $prId );
$today          = date( "Y-m-d H:i:s" );
$fields         = array(
    'user_id' => trim( $founduser ),
    'prod_id' => trim( $prId ),
    'Supplier_id' => trim( $supplier_id ),
    'facility_name' => trim( $facility_name ),
    'time_of_enquiry' => $today,
    'enq_specifics' => trim( $msg )
    );

$possible_spam=FALSE;

$plans = planType($prId);
$plan='FREE';
if(ISSET($plans['title']) && $plans['title']){$plan=$plans['title'];}

if(!$founduser){$error='Following enquiry has been marked as DELETED as user_id=0 or blank.'."\n".json_encode($fields);  email_admin_error($error);
$fields         = array(
    'user_id' => trim( $founduser ),
    'prod_id' => trim( $prId ),
    'Supplier_id' => trim( $supplier_id ),
    'facility_name' => trim( $facility_name ),
    'time_of_enquiry' => $today,
    'enq_specifics' => trim( $msg ),
    'deleted' => '1'
);
echo "3";exit;	
	
}
//email_admin_error($vacanncy_tel);

if($vacanncy_tel && strlen($vacanncy_tel)==11 && substr($vacanncy_tel, 0,1)=='8'){ // Do not send NIgel the error email
	
	$fields         = array(
    'user_id' => trim( $founduser ),
    'prod_id' => trim( $prId ),
    'Supplier_id' => trim( $supplier_id ),
    'facility_name' => trim( $facility_name ),
    'time_of_enquiry' => $today,
    'enq_specifics' => trim( $msg ),
    'deleted' => '1'
);
	
echo "3";exit;	
	
}


$insert_result  = $db->insert( _prefix( 'enquiries' ), $fields );
$enq_last_id    = $db->last_id();
$tracking_pixel_id = md5((int)$enq_last_id);

$enquiry_link = "https://www.agedadvisor.nz/extra-facility/?sq=".md5((int)trim( $founduser ))."&ug=".md5((int)trim( $prId ))."&sf=".md5((int)$enq_last_id);
$update_fields = array(
    'enquiry_link' => $enquiry_link,
    'taking_enquiries' => 2 
);

$where = "where id= '$enq_last_id' ";
$insert_result = $db->update( _prefix( 'enquiries' ), $update_fields, $where );

$userQuery = "SELECT `first_name`,`last_name`,`email` FROM `ad_users` WHERE id = '$founduser'";
$userQueryRes = mysqli_query($db->db_connect_id, $userQuery);

while($user = mysqli_fetch_array($userQueryRes)){    
    $first_name = $user['first_name'];
    $last_name = $user['last_name'];
    $useremail = $user['email'];            
    $username = $first_name.' '.$last_name; 
}

$addressQuery ="SELECT `title`,`address_suburb`,`address_city`,`zip`, `latitude`, `longitude` FROM `ad_products` WHERE id = '$prId'";
$addressQueryRes = mysqli_query($db->db_connect_id, $addressQuery);

while($addData = mysqli_fetch_array($addressQueryRes)){        
    $suburb = $addData['address_suburb'];
    $city = $addData['address_city'];
    $zip = $addData['zip'];        
    $lat = $addData['latitude'];
    $long = $addData['longitude']; 
    $title = $addData['title']; 
}
              
$link_to_facility_in_area = "<a style='background-color: #F15922;border-color: #d23f0a;display: inline-block;padding: 6px 12px;margin-bottom: 0;font-size: 14px;font-weight: 400;line-height: 1.42857143;text-align: center;white-space: nowrap;vertical-align: middle;cursor: pointer;background-image: none;border: 1px solid transparent;border-radius: 4px;color: #fff;' href='https://www.agedadvisor.nz/other-facilities/?sub=".$suburb."&pid=".$prid."&city=".$city."&zip=".$zip."&lat=".$lat."&long=".$long."&uid=".md5($founduser)."'>Click to view other facilities in the area</a>";  


if(!$possible_spam){
    
    $to       = $useremail;
    $headers  = "MIME-Version: 1.0\n";
    $headers .= "Content-type: text/html; charset=iso-8859-1\n";
    $headers .= 'From:   ';            
    $headers .= "reviews@agedadvisor.co.nz";
    $headers .= "\n";
    $headers .= "Cc: reviews@agedadvisor.co.nz\r\n";
    $emailTemplate = emailTemplate( 'Enq-availability-reason' );   
    if ( $emailTemplate[ 'subject' ] != '' ) {       
        $message    = str_replace( array(
            '{name}',            
            '{facility_name}', 
            '{reason}',
            '{link_to_facility_in_area}',
            '{plan}' 
        ), array(
            $username,            
            $title,
            $reason,
            $link_to_facility_in_area,
            $plan
        ), stripslashes( $emailTemplate[ 'description' ] ) );                
        $result  = @mail( $to, $emailTemplate[ 'subject' ] . ' ' . $title, $message, $headers );
    }
    if ( $result ) {
        echo '1';
    } else {
        echo '<font color="#FF0000">Server Error!</font>';
    } 
    } 
    exit();    
}





$facility_name  = get_facility_name( $prId );
$supplier_id    = get_facility_supplier_id( $prId );
$today          = date( "Y-m-d H:i:s" );
$fields         = array(
    'user_id' => trim( $founduser ),
    'prod_id' => trim( $prId ),
    'Supplier_id' => trim( $supplier_id ),
    'facility_name' => trim( $facility_name ),
    'time_of_enquiry' => $today,
    'enq_specifics' => trim( $msg )
);

$possible_spam=FALSE;



if($vacanncy_tel && strlen($vacanncy_tel)==11 && substr($vacanncy_tel, 0,1)=='8'){ // Do not send NIgel the error email
$fields         = array(
    'user_id' => trim( $founduser ),
    'prod_id' => trim( $prId ),
    'Supplier_id' => trim( $supplier_id ),
    'facility_name' => trim( $facility_name ),
    'time_of_enquiry' => $today,
    'enq_specifics' => trim( $msg ),
    'deleted' => '1'
);
	
echo "3";exit;	
	
}

if(!$founduser){$error='Following enquiry has been marked as DELETED as user_id=0 or blank.'."\n".json_encode($fields);  email_admin_error($error);
$fields         = array(
    'user_id' => trim( $founduser ),
    'prod_id' => trim( $prId ),
    'Supplier_id' => trim( $supplier_id ),
    'facility_name' => trim( $facility_name ),
    'time_of_enquiry' => $today,
    'enq_specifics' => trim( $msg ),
    'deleted' => '1'
);
	
echo "3";exit;	
}



$insert_result  = $db->insert( _prefix( 'enquiries' ), $fields );
$enq_last_id    = $db->last_id();
$tracking_pixel_id = md5((int)$enq_last_id);

$enquiry_link = "https://www.agedadvisor.nz/extra-facility/?sq=".md5((int)trim( $founduser ))."&ug=".md5((int)trim( $prId ))."&sf=".md5((int)$enq_last_id);
$update_fields = array(
    'enquiry_link' => $enquiry_link 
);
$where = "where id= '$enq_last_id' ";
$insert_result = $db->update( _prefix( 'enquiries' ), $update_fields, $where );

$headofficeemail = '';
$pro_query      = "select ad_products.*,ad_users.email AS headofficeemail ,ad_users.first_name AS headofficefname, ad_users.last_name AS headofficelname , ad_users.email_reviews_facility  , ad_users.email_reviews_head_office  , ad_users.agree_terms  , ad_users.phone AS headofficephone from ad_products,ad_users where ad_products.id='$prId' AND ad_users.id = ad_products.supplier_id ";
$pro_name_query = mysqli_query($db->db_connect_id,  $pro_query );
while ( $product_detail = mysqli_fetch_array( $pro_name_query ) ) {
    $headofficeemail           = $product_detail[ 'headofficeemail' ];
    $headofficefname           = $product_detail[ 'headofficefname' ];
    $headofficelname           = $product_detail[ 'headofficelname' ];
    $email_reviews_facility    = $product_detail[ 'email_reviews_facility' ];
    $email_reviews_head_office = $product_detail[ 'email_reviews_head_office' ];
    $agree_terms               = $product_detail[ 'agree_terms' ];
    $headofficephone           = $product_detail[ 'headofficephone' ];
    $product_name              = $product_detail[ 'title' ];
    $supplier_id               = $product_detail[ 'supplier_id' ];
    $product_url               = $product_detail[ 'quick_url' ];
    
    $quick_url                 = "agedadvisor.nz/" . $product_url;
    $user_name                 = "select  * from ad_pro_extra_info where id='$prId' ";
    $user_name1                = mysqli_query($db->db_connect_id,  $user_name );
    while ( $user_fullname = mysqli_fetch_array( $user_name1 ) ) {
        $user_fname = $user_fullname[ 'first_name' ];
        $user_lname = $user_fullname[ 'last_name' ];
        $user_email = $user_fullname[ 'email' ];
        
        // Incase no email recorded
       if($user_email==''){$add_subject='!!! NO EMAIL FOR TO: !!! - ';} 
        
        if ( $user_fname ) {
            $user_name = $user_fname;
        } else {
            $user_name = $product_name . ' Admin';
        }
    }
    
    $upgrade2 = "select  * from ad_pro_plans where pro_id='$prId' AND current_plan=1";
    $upgrade1 = mysqli_query($db->db_connect_id,  $upgrade2 );
    while ( $upgraded = mysqli_fetch_array( $upgrade1 ) ) {
        $upgrade = $upgraded[ 'plan_id' ];
        if ( $upgrade > 1 && $upgrade !='' ) {
            $hasupgraded = TRUE;
        } else {
            $hasupgraded = FALSE;
        }
    }
}

if( !empty($headofficeemail) ){
    $headofficeemailArrStr = explode(",", $headofficeemail);
    foreach( $headofficeemailArrStr as $headofficeemailArrStrVal ){
        if( !empty($headofficeemailArrStrVal) && filter_var($headofficeemailArrStrVal, FILTER_VALIDATE_EMAIL) ){
            $valid_facility_email = 1;
        }
    }
}

$response_query = "SELECT `id`,`taking_enquiries`, `facility_responded` FROM ad_enquiries WHERE prod_id = $prId AND facility_followup_sent = 'y' ORDER BY time_of_enquiry DESC LIMIT 3";
$response_query_res = mysqli_query($db->db_connect_id,  $response_query );
$no_response_count = 0;

while($response_query_data = mysqli_fetch_array($response_query_res)){
    if($response_query_data[ 'facility_responded' ] == 'n'){
        $no_response_count++;
    }     
}

$fQuery     = "SELECT `id`,`certification_service_type`,`facility_type`,`address_suburb`,`address_city`,`zip` ,`facility_link` FROM `ad_products` WHERE id = '$prId'";
$fQueryresL = mysqli_query($db->db_connect_id,  $fQuery );
while($fc = mysqli_fetch_array($fQueryresL)){
    $fpro_id                     = $fc[ 'pro_id' ];
    $fCertification_service_type = $fc[ 'certification_service_type' ];
    $fFacility_type              = $fc[ 'facility_type' ];
    $fAddress_suburb             = $fc[ 'address_suburb' ];
    $fAddress_city               = $fc[ 'address_city' ];
    $fZip                        = $fc[ 'zip' ];
    $other_facility_link         = $fc[ 'facility_link' ];
}

$similarFacilityQuery     = 'SELECT COUNT(distinct fd.id) as count_feedback , pr.id, pr.quick_url,pr.title, pr.address_suburb, '
                        . "pr.address_city, fd.overall_rating,gall.file,gall.type, pr.latitude, pr.longitude, SQRT(POW(69.1 * (pr.latitude - ('$lat')), 2) + POW(69.1 * (('$long') - pr.longitude) * COS(pr.latitude / 57.3), 2)) "
                        . 'AS `distance`, (select aprod.certification_service_type from ad_enquiries enq left join ad_products aprod on aprod.id=enq.prod_id where enq.user_id='.$founduser.' order by enq.id desc limit 1) as prod_cert_type '
                        . 'FROM `ad_products` AS pr '
                        . "LEFT JOIN `ad_pro_plans` AS prdp ON pr.id = prdp.pro_id "
                        . 'LEFT JOIN `ad_feedbacks` AS fd ON pr.id = fd.product_id AND fd.status = 1 AND fd.deleted = 0 '
                        . 'LEFT JOIN `ad_galleries` as gall ON pr.id = gall.pro_id '
                        . 'WHERE ad_pro_plans.current_plan != 1 AND ad_pro_plans.status = 1 AND (`address_suburb` = "' . $fAddress_suburb . '" OR `address_city` = "' . $fAddress_city . '" OR `zip` = "' . $fZip . '") '
                        . 'AND gall.type=0 AND gall.file IS NOT NULL AND pr.id NOT IN("'.$prId.'") AND pr.deleted=0 '
                        . 'AND pr.certification_service_type = "'.$fCertification_service_type.'" '
                        . 'GROUP BY pr.id HAVING count_feedback >= 3 ORDER BY distance, fd.overall_rating DESC LIMIT 5';





$similarFacilityQueryResult = mysqli_query($db->db_connect_id,  $similarFacilityQuery );
if ( mysqli_num_rows( $similarFacilityQueryResult ) <= 0 ) {
    $lat                  = -41.211722;
    $long                 = 175.517578;
    $similarFacilityQuery = "SELECT fd.feedback, COUNT(distinct fd.id) as count_feedback , pr.id, pr.quick_url,pr.title, pr.address_suburb, "
                        . "pr.address_city,pr.deleted,pr.latitude, pr.longitude, "
                        . "SQRT(POW(69.1 * (pr.latitude - ('$lat')), 2) + POW(69.1 * (('$long') - pr.longitude) * COS(pr.latitude / 57.3), 2)) AS `distance`, "
                        . "fd.overall_rating,gall.file,gall.type, "
                        . "(select aprod.certification_service_type from ad_enquiries enq left join ad_products aprod on aprod.id=enq.prod_id where enq.user_id=".$founduser." order by enq.id desc limit 1) as prod_cert_type "
                        . "FROM `ad_products` AS pr "
                        . "LEFT JOIN `ad_pro_plans` AS prdp ON pr.id = prdp.pro_id "
                        . "LEFT JOIN `ad_feedbacks` AS fd ON pr.id = fd.product_id AND fd.status = 1 AND fd.deleted = 0 "
                        . "LEFT JOIN `ad_galleries` as gall ON pr.id = gall.pro_id "
                        . "WHERE prdp.current_plan != 1 AND prdp.plan_id != 1 AND prdp.status = 1 AND pr.certification_service_type = '".$fCertification_service_type."'  "
                        . "GROUP BY pr.id HAVING distance < 500 and distance > 0  AND count_feedback >= 3 AND pr.deleted = 0 AND gall.type=0 AND gall.file IS NOT NULL "
                        . "ORDER BY distance LIMIT 5";
}




$similarFacilityQueryresL = mysqli_query($db->db_connect_id,  $similarFacilityQuery );

$you_may_also_like = '';
while($sFac = mysqli_fetch_array($similarFacilityQueryresL)){
    $fID        = $sFac[ 'id' ];
    $fQuick_url = $sFac[ 'quick_url' ];
    $fTitle     = $sFac[ 'title' ];
    $fcount_feedback     = $sFac[ 'count_feedback' ];
    if ( isset( $sFac[ 'file' ] ) && !empty( $sFac[ 'file' ] ) ) {
        $fImageSet = $_SERVER[ 'DOCUMENT_ROOT' ] . '/admin/files/gallery/images/thumb_' . $sFac[ 'file' ];
        $fImage    = 'thumb_' . $sFac[ 'file' ];
    }
    $fSuburb         = $sFac[ 'address_suburb' ];
    $fCity           = $sFac[ 'address_city' ];
    $fOverall_rating = simlilarFacilityRating( $fID );
    if ( file_exists( $fImageSet ) ) {
        $similarFacility = '<img src="' . HOME_PATH . 'admin/files/gallery/images/' . $fImage . '" alt="" title="" />
                            <div class="facility-rating">

                                <a href="' . HOME_PATH . $fQuick_url . '"><span class="prcnt-icon">' . round( $fOverall_rating ) . '% rating from '.$fcount_feedback.' reviews</span><br/>' . " " . $fTitle . '<br/>
                                <span>' . $fSuburb . ', ' . $fCity . '</span>
                                <div class="star-inactive">
                                    <div class="star-active" style="width:' . $fOverall_rating . '%;"></div>
                                </div>
                                </a>
                        </div>
                        <br>

                        ';
    } else {
        $you_may_also_like = '';
    }
    $you_may_also_like = $you_may_also_like . $similarFacility;
}


$_SESSION['enquirer_id']= $founduser;


if ( $no_response_count > 2) {     
    if ( $valid_email == 1 && $uemail != 'n/a' ) {
        
        $to       = $uemail;
        $headers  = "MIME-Version: 1.0\n";
        $headers .= "Content-type: text/html; charset=iso-8859-1\n";
        $headers .= 'From:   ';
        $headers .= "reviews@agedadvisor.co.nz";
        $headers .= "\n";
        $headers .= "Cc: reviews@agedadvisor.co.nz\r\n";

        $emailTemplate = emailTemplate( 'unresponsive_facilities_enquirer' );                       
        $emailTemplate_provider = emailTemplate( 'unresponsive_facilities_provider' );                       
// enquirer
        if ( $emailTemplate[ 'subject' ] != '' ) {
            $message = str_replace( array(
                '{username}',
                '{facility_url}',
                '{name}',
                '{facility_name}',
                '{email}',
                '{mobile}',
                '{plan_id}' 
            ), array(
                $user_name,
                $quick_url,
                $uname,
                $product_name,
                $uemail,
                $vacanncy_tel,
                $upgrade 
            ), stripslashes( $emailTemplate[ 'description' ] ) );
                
            $message = str_replace( '{you-may-also-like}', $you_may_also_like, $message );
            //$result = @mail( $to, $emailTemplate[ 'subject' ] . ' ' . $facility_name . ' at ' . date( "g:i a d.m.y" ), $message, $headers );
        if(!$possible_spam){    
            @mail( $to, $emailTemplate[ 'subject' ] . ' ' . $facility_name . ' at ' . date( "g:i a d.m.y" ), $message, $headers ); }
        }
            
// Provider Email            
        if ( $emailTemplate_provider[ 'subject' ] != '' ) {
            $message = str_replace( array(
                '{headofficefname}',
                '{link_to_facility}',
                '{facility_url}',
                '{name}',
                '{facility_name}',
                '{email}',
                '{mobile}',
                '{plan_id}' 
            ), array(
                $headofficefname,
                $enquiry_link,
                $quick_url,
                $uname,
                $product_name,
                $uemail,
                $vacanncy_tel,
                $upgrade 
            ), stripslashes( $emailTemplate_provider[ 'description' ] ) );
                
            $message = str_replace( '{you-may-also-like}', $you_may_also_like, $message );
            //$result = @mail( $to, $emailTemplate[ 'subject' ] . ' ' . $facility_name . ' at ' . date( "g:i a d.m.y" ), $message, $headers );
            if(!$possible_spam){      
                @mail( $headofficeemail, $emailTemplate[ 'subject' ] . ' ' . $facility_name . ' at ' . date( "g:i a d.m.y" ), $message, $headers );                   
            }                                      
        }
    }


    if ( $valid_facility_email == 1 ) {
        
        $to      = $headofficeemail;
        $headers = "MIME-Version: 1.0\n";
        $headers .= "Content-type: text/html; charset=iso-8859-1\n";
        $headers .= 'From:   ';
        $headers .= "reviews@agedadvisor.co.nz";
        $headers .= "\n";
        $headers .= "Cc: $user_email\r\n";
        $emailTemplate = emailTemplate( 'unresponsive_facilities_enquirer' );                       

        if ( $emailTemplate[ 'subject' ] != '' ) {
            $message = str_replace( array(
                '{username}',
                '{facility_url}',
                '{name}',
                '{facility_name}',
                '{email}',
                '{mobile}',
                '{plan_id}' 
            ), array(
                $user_name,
                $quick_url,
                $uname,
                $product_name,
                $uemail,
                $vacanncy_tel,
                $upgrade 
            ), stripslashes( $emailTemplate[ 'description' ] ) );

                $fQuery     = "SELECT `id`,`certification_service_type`,`facility_type`,`address_suburb`,`address_city`,`zip` ,`facility_link` FROM `ad_products` WHERE id = '$prId'";
                $fQueryresL = mysqli_query($db->db_connect_id,  $fQuery );                
                while($fc = mysqli_fetch_array($fQueryresL)){
                    $fpro_id                     = $fc[ 'pro_id' ];
                    $fCertification_service_type = $fc[ 'certification_service_type' ];
                    $fFacility_type              = $fc[ 'facility_type' ];
                    $fAddress_suburb             = $fc[ 'address_suburb' ];
                    $fAddress_city               = $fc[ 'address_city' ];
                    $fZip                        = $fc[ 'zip' ];
                    $other_facility_link         = $fc[ 'facility_link' ];
                }                              
            $message = str_replace( '{you-may-also-like}', $you_may_also_like, $message );            
            if(!$possible_spam){    
                @mail( $to, $emailTemplate[ 'subject' ] . ' ' . $facility_name . ' at ' . date( "g:i a d.m.y" ), $message, $headers );                 
            }
        }
    }
        
                    $to      = 'nigel@agedadvisor.co.nz';
                    $headers = "MIME-Version: 1.0\n";
                    $headers .= "Content-type: text/html; charset=iso-8859-1\n";
                    $headers .= 'From:   ';
                    if ( $uemail == 'n/a' ) {
                        $headers .= "reviews@agedadvisor.co.nz";
                    } else {
                        $headers .= "$uemail";
                    }                
                    $headers .= "\n";                     
                    $emailTemplate = emailTemplate( 'Enq-availability' );
                    $link_to_facility = "<a style='background-color: #F15922;border-color: #d23f0a;display: inline-block;padding: 6px 12px;margin-bottom: 0;font-size: 14px;font-weight: 400;line-height: 1.42857143;text-align: center;white-space: nowrap;vertical-align: middle;cursor: pointer;background-image: none;border: 1px solid transparent;border-radius: 4px;color: #fff;' href='https://www.agedadvisor.nz/extra-facility/?sq=".md5((int)trim( $founduser ))."&ug=".md5((int)trim( $prId ))."&sf=".md5((int)$enq_last_id)."'>Click for contact details</a>";
                    if ( $emailTemplate[ 'subject' ] != '' ) {
                        $message    = str_replace( array(
                            '{username}',
                            '{facility_url}',
                            '{name}',
                            '{facility_name}',
                            '{email}',
                            '{mobile}',
                            '{msg}',
                            '{link_to_facility}',
                            '{plan_id}' 
                        ), array(
                            $user_name,
                            $quick_url,
                            $uname,
                            $product_name,
                            $uemail,
                            $vacanncy_tel,
                            $msg,
                            $link_to_facility,
                            $upgrade 
                        ), stripslashes( $emailTemplate[ 'description' ] ) );
                    $fQuery     = "SELECT `manager`,`email`,`phone`,first_name,last_name,position FROM `ad_pro_extra_info` WHERE pro_id='$prId'";
                    $fQueryresL = mysqli_query($db->db_connect_id,  $fQuery );                    
                    while($fc = mysqli_fetch_array($fQueryresL)){
                        $facilitymanager = $fc[ 'manager' ];
                        $facilityemail   = $fc[ 'email' ];
                        $facilityphone   = $fc[ 'phone' ];
                        $first_name   = $fc[ 'first_name' ];
                        $last_name   = $fc[ 'last_name' ];
                        $position   = $fc[ 'position' ];
                    }
                    $message = str_replace( array(
                        '{first_name}',
                        '{last_name}',
                        '{position}',
                        '{facilitymanager}',
                        '{facilityemail}',
                        '{facilityphone}',
                        '{headofficeemail}',
                        '{headofficefname}',
                        '{headofficelname}',
                        '{email_reviews_facility}',
                        '{email_reviews_head_office}',
                        '{agree_terms}',
                        '{headofficephone}' 
                    ), array(
                        $first_name,
                        $last_name,
                        $position,
                        $facilitymanager,
                        $facilityemail,
                        $facilityphone,
                        $headofficeemail,
                        $headofficefname,
                        $headofficelname,
                        $email_reviews_facility,
                        $email_reviews_head_office,
                        $agree_terms,
                        $headofficephone 
                    ), $message );
                    if(!$possible_spam){    $result  = @mail( $to,"This facility is currently not responding" . ' ' . $facility_name . ' at ' . date( "g:i a d.m.y" ), $message, $headers );}
                        
                        if($add_subject && !$possible_spam){ $result  = @mail( 'nigel@agedadvisor.co.nz', $add_subject.$emailTemplate[ 'subject' ] . ' ' . $facility_name . ' at ' . date( "g:i a d.m.y" ), $message,"MIME-Version: 1.0\nContent-type: text/html; charset=iso-8859-1\n");}

                    }    

    
} else {
     if ( $valid_email == 1 ) {
        if ( $valid_mobile_count > 8 ) {
            if ( empty( $uemail ) || empty( $uname ) || empty( $vacanncy_tel ) ) {
                echo '<font color="#FF0000">Please fill all the fields!</font>';
            } else {                                      
                    $addNoteToEmail = get_count_of_enquiries($founduser, $prId );                    
                    $msg = $msg.$addNoteToEmail; // appending message with a note returned by get_count_of_enquiries function
                if ( $valid_email == 1 ) {
                    $to       = $user_email;
                    $headers  = "MIME-Version: 1.0\n";
                    $headers .= "Content-type: text/html; charset=iso-8859-1\n";
                    $headers .= 'From:   ';                                         
                    $headers .= "reviews@agedadvisor.co.nz";                    
                    $headers .= "\n";
                    $headers .= "Cc: $headofficeemail,reviews@agedadvisor.co.nz\r\n";
                    $headers .= "bcc: nigel@agedadvisor.co.nz\r\n";                                                      
                    $emailTemplate = emailTemplate( 'Enq-availability-nonupgraded' );                                    
                    $link_to_facility = "<a style='background-color: #F15922;border-color: #d23f0a;display: inline-block;padding: 6px 12px;margin-bottom: 0;font-size: 14px;font-weight: 400;line-height: 1.42857143;text-align: center;white-space: nowrap;vertical-align: middle;cursor: pointer;background-image: none;border: 1px solid transparent;border-radius: 4px;color: #fff;' href='https://www.agedadvisor.nz/extra-facility/?sq=".md5((int)trim( $founduser ))."&ug=".md5((int)trim( $prId ))."&sf=".md5((int)$enq_last_id)."'>Click for contact details</a>";
                    
                    
                    if ( $emailTemplate[ 'subject' ] != '' ) {
                        $sname      = substr( $uname, 0, 3 ) . '..... .......';
                        $semail     = substr( $uemail, 0, 3 ) . '.......@.....';
                        $smobile    = substr( $vacanncy_tel, 0, 5 ) . '.....';
                        $message    = str_replace( array(
                            '{username}',
                            '{facility_url}',
                            '{name}',
                            '{facility_name}',
                            '{email}',
                            '{mobile}',
                            '{plan_id}',
                            '{sname}',
                            '{semail}',
                            '{smobile}',
                            '{msg}',
                            '{link_to_facility}',
                            '{enq_id}',
                            '{plan}'
                        ), array(
                            $user_name,
                            $quick_url,
                            $uname,
                            $product_name,
                            $uemail,
                            $vacanncy_tel,
                            $upgrade,
                            $sname,
                            $semail,
                            $smobile,
                            $msg,
                            $link_to_facility,
                            $tracking_pixel_id,
                            $plan
                        ), stripslashes( $emailTemplate[ 'description' ] ) );
                    $fQuery     = "SELECT `manager`,`email`,`phone`,first_name,last_name,position FROM `ad_pro_extra_info` WHERE pro_id='$prId'";
                    $fQueryresL = mysqli_query($db->db_connect_id,  $fQuery );
                    while($fc = mysqli_fetch_array($fQueryresL)){
                       $facilitymanager = $fc[ 'manager' ];
                       $facilityemail   = $fc[ 'email' ];
                       $facilityphone   = $fc[ 'phone' ];
                       $first_name   = $fc[ 'first_name' ];
                       $last_name   = $fc[ 'last_name' ];
                       $position   = $fc[ 'position' ];
                    }
                    $message = str_replace( array(
                        '{first_name}',
                        '{last_name}',
                        '{position}',
                        '{facilitymanager}',
                        '{facilityemail}',
                        '{facilityphone}',
                        '{headofficeemail}',
                        '{headofficefname}',
                        '{headofficelname}',
                        '{email_reviews_facility}',
                        '{email_reviews_head_office}',
                        '{agree_terms}',
                        '{headofficephone}',
                        '{plan}' 
                    ), array(
                        $first_name,
                        $last_name,
                        $position,
                        $facilitymanager,
                        $facilityemail,
                        $facilityphone,
                        $headofficeemail,
                        $headofficefname,
                        $headofficelname,
                        $email_reviews_facility,
                        $email_reviews_head_office,
                        $agree_terms,
                        $headofficephone ,
                        $plan
                    ), $message );
                    if(!$possible_spam){    @mail( $to, $emailTemplate[ 'subject' ] . ' ' . $facility_name . ' at ' . date( "g:i a d.m.y" ), $message, $headers );  }                                              

                        if($add_subject && !$possible_spam){ @mail( 'nigel@agedadvisor.co.nz', $add_subject.$emailTemplate[ 'subject' ] . ' ' . $facility_name . ' at ' . date( "g:i a d.m.y" ), $message,"MIME-Version: 1.0\nContent-type: text/html; charset=iso-8859-1\n");}
                    }                        
                }
                if ( $valid_email == 1 ) {
                    $to      = 'reviews@agedadvisor.co.nz';
                    $headers = "MIME-Version: 1.0\n";
                    $headers .= "Content-type: text/html; charset=iso-8859-1\n";
                    $headers .= 'From:   ';
                    if ( $uemail == 'n/a' ) {
                        $headers .= "reviews@agedadvisor.co.nz";
                    } else {
                        $headers .= "$uemail";
                    }                
                    $headers .= "\n";
                    $emailTemplate = emailTemplate( 'Enq-availability' );
                    $link_to_facility = "<a style='background-color: #F15922;border-color: #d23f0a;display: inline-block;padding: 6px 12px;margin-bottom: 0;font-size: 14px;font-weight: 400;line-height: 1.42857143;text-align: center;white-space: nowrap;vertical-align: middle;cursor: pointer;background-image: none;border: 1px solid transparent;border-radius: 4px;color: #fff;' href='https://www.agedadvisor.nz/extra-facility/?sq=".md5((int)trim( $founduser ))."&ug=".md5((int)trim( $prId ))."&sf=".md5((int)$enq_last_id)."'>Click for contact details</a>";
                    if ( $emailTemplate[ 'subject' ] != '' ) {
                        $message    = str_replace( array(
                            '{username}',
                            '{facility_url}',
                            '{name}',
                            '{facility_name}',
                            '{email}',
                            '{mobile}',
                            '{msg}',
                            '{link_to_facility}',
                            '{plan_id}' ,
                            '{plan}'
                        ), array(
                            $user_name,
                            $quick_url,
                            $uname,
                            $product_name,
                            $uemail,
                            $vacanncy_tel,
                            $msg,
                            $link_to_facility,
                            $upgrade ,
                            $plan
                        ), stripslashes( $emailTemplate[ 'description' ] ) );
                    $fQuery     = "SELECT `manager`,`email`,`phone`,first_name,last_name,position FROM `ad_pro_extra_info` WHERE pro_id='$prId'";
                    $fQueryresL = mysqli_query($db->db_connect_id,  $fQuery );
                    
                    while($fc = mysqli_fetch_array($fQueryresL)){
                        $facilitymanager = $fc[ 'manager' ];
                        $facilityemail   = $fc[ 'email' ];
                        $facilityphone   = $fc[ 'phone' ];
                        $first_name   = $fc[ 'first_name' ];
                        $last_name   = $fc[ 'last_name' ];
                        $position   = $fc[ 'position' ];
                    }
                    $message = str_replace( array(
                        '{first_name}',
                        '{last_name}',
                        '{position}',
                        '{facilitymanager}',
                        '{facilityemail}',
                        '{facilityphone}',
                        '{headofficeemail}',
                        '{headofficefname}',
                        '{headofficelname}',
                        '{email_reviews_facility}',
                        '{email_reviews_head_office}',
                        '{agree_terms}',
                        '{headofficephone}' ,
                        '{plan}'
                    ), array(
                        $first_name,
                        $last_name,
                        $position,
                        $facilitymanager,
                        $facilityemail,
                        $facilityphone,
                        $headofficeemail,
                        $headofficefname,
                        $headofficelname,
                        $email_reviews_facility,
                        $email_reviews_head_office,
                        $agree_terms,
                        $headofficephone ,
                        $plan
                    ), $message );
                        $result  = @mail( $to, $emailTemplate[ 'subject' ] . ' ' . $facility_name . ' at ' . date( "g:i a d.m.y" ), $message, $headers );
                        
                        if($add_subject && !$possible_spam){ $result  = @mail( 'nigel@agedadvisor.co.nz', $add_subject.$emailTemplate[ 'subject' ] . ' ' . $facility_name . ' at ' . date( "g:i a d.m.y" ), $message,"MIME-Version: 1.0\nContent-type: text/html; charset=iso-8859-1\n");}

                    }
                }
                if ( $valid_email == 1 && $uemail != 'n/a' ) {
                    $to      = $uemail;
                    $headers = "MIME-Version: 1.0\n";
                    $headers .= "Content-type: text/html; charset=iso-8859-1\n";
                    $headers .= 'From:   ';
                    $headers .= "reviews@agedadvisor.co.nz";
                    $headers .= "\n";
                    $headers .= "Cc: reviews@agedadvisor.co.nz\r\n";
                    if ( $hasupgraded ) {
                        $emailTemplate = emailTemplate( 'Enq-availability-thanks' );
                    } else {
                        $emailTemplate = emailTemplate( 'Enq-availability-thanks-nonupgraded' );
                    }
                    if ( $emailTemplate[ 'subject' ] != '' ) {
                        $message = str_replace( array(
                            '{username}',
                            '{facility_url}',
                            '{name}',
                            '{facility_name}',
                            '{email}',
                            '{mobile}',
                            '{plan_id}' ,
                            '{plan}'
                        ), array(
                            $user_name,
                            $quick_url,
                            $uname,
                            $product_name,
                            $uemail,
                            $vacanncy_tel,
                            $upgrade ,
                            $plan
                        ), stripslashes( $emailTemplate[ 'description' ] ) );
                        if ( $hasupgraded == FALSE ) {                            
                            $message = str_replace( '{you-may-also-like}', $you_may_also_like, $message );
                        }
                        if(!$possible_spam){$result = @mail( $to, $emailTemplate[ 'subject' ] . ' ' . $facility_name . ' at ' . date( "g:i a d.m.y" ), $message, $headers ); }                       
                    }
                }
            }
        } else {
            echo "Please enter your phone number<br>(with area code)";
        }
    } else {
        echo 'Please enter a valid email address.';
    }
}

?>