<div class="panel panel-default feed">
    <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style="width:100%; margin:10px 0px;">
        <div class="col-md-2 col-sm-2 col-xs-12 padding_none"><strong style="font-size:15px;">Pros</strong></div> 
        <div class="col-md-10 col-sm-10 col-xs-12 padding_left"><?php echo ((empty($record['pros'])) ? 'N/A' : $record['pros']) ?></div> 
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style="width:100%; margin-bottom:10px;">
        <div class="col-md-2 col-sm-2 col-xs-12 padding_none"><strong style="font-size:15px;">Cons</strong></div> 
        <div class="col-md-10 col-sm-10 col-xs-12 padding_left"><?php echo ((empty($record['cons'])) ? 'N/A' : $record['cons']) ?></div> 
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style="width:100%; margin-bottom:10px;">
        <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style=" margin-bottom: 10px;"><strong style="font-size:15px">Suggestions to Management or General Comment</strong></div> 
        <div class="col-md-12 col-sm-12 col-xs-12 padding_none"><?php echo $record['feedback'] ?></div> 
    </div>     
    <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left scroll" style="width:100%; margin-bottom:10px;">
        <table class="table table-bordered table_bord">                   
            <thead>
                <tr>
                    <th width="" class="th_text">Categories</th>
                    <th width="" class="th_text">Rating</th>
                    <th width="" class="th_text">Categories</th>
                    <th width="" class="th_text">Rating</th>
                </tr>
            </thead> 
            <tbody>
                <tr>
                    <td>Quality of Care</td>
                    <td>
                        <div class="text-center"><?php echo getStars($record['quality_of_care']); ?></div>
                    </td>
                    <td>Caring/Helpful Staff</td>
                    <td>
                        <div class="text-center"><?php echo getStars($record['caring_staff']) ?></div>
                    </td>
                </tr>
                <tr>
                    <td>Responsive Management</td>
                    <td>
                        <div class="text-center"><?php echo getStars($record['responsive_management']) ?></div>
                    </td>
                    <td>Trips/Outdoor Activities</td>
                    <td>
                        <div class="text-center"><?php echo getStars($record['trips_outdoor_activities']) ?></div>
                    </td>
                </tr>
                <tr>
                    <td>Indoor Entertainment</td>
                    <td>
                        <div class="text-center"><?php echo getStars($record['indoor_entertainment']) ?></div>
                    </td>
                    <td>Social Atmosphere</td>
                    <td>
                        <div class="text-center"><?php echo getStars($record['social_atmosphere']) ?></div>
                    </td>
                </tr>
                <tr>
                    <td>Enjoyable Food</td>
                    <td>
                        <div class="text-center"><?php echo getStars($record['enjoyable_food']) ?></div>
                    </td>
                    <td>OVERALL RATING</td>
                    <td>
                        <div class="text-center"><?php echo getStars($record['overall_rating']) ?></div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>     
    <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style="width:100%; margin-bottom:10px;">
        <div class="col-md-12 col-sm-12 col-xs-12 pull-left padding_none" style=" margin-bottom: 10px;"><strong style="font-size:15px">How do you know about this place?</strong></div> 
        <div class="col-md-12 col-sm-12 col-xs-12 padding_none"><?php echo ((empty($record['know_me'])) ? 'N/A' : $knowMe[$record['know_me']]) ?></div> 
    </div> 

    <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style="width:100%; margin:10px 0px;">
        <div class="col-md-12 col-sm-12 col-xs-12 pull-left padding_none" style=" margin-bottom: 10px;"><strong style="font-size:15px">I have lived at / visited this facility for</strong></div> 
        <div class="col-md-12 col-sm-12 col-xs-12 padding_none"><?php echo ((empty($record['visit_duration'])) ? 'N/A' : $durationVisit[$record['visit_duration']]) ?></div> 
    </div>                    
    <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style="width:100%; margin:10px 0px;">
        <div class="col-md-12 col-sm-12 col-xs-12 pull-left padding_none" style=" margin-bottom: 10px;"><strong style="font-size:15px">I last lived at / visited this facility</strong></div> 
        <div class="col-md-12 col-sm-12 col-xs-12 padding_none"><?php echo ((empty($record['last_visit'])) ? 'N/A' : $lastVisit[$record['last_visit']]) ?></div> 
    </div>                    
    <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style="width:100%; margin:10px 0px;">
        <div class="col-md-12 col-sm-12 col-xs-12 pull-left padding_none" style=" margin-bottom: 10px;"><strong style="font-size:15px">Would you recommend this facility to a friend?</strong></div> 
        <div class="col-md-12 col-sm-12 col-xs-12 padding_none"><?php echo ((empty($record['recommended'])) ? 'N/A' : $recommended[$record['recommended']]) ?></div> 
    </div>  
</div>