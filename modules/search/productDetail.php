<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false&amp;libraries=places"></script>
<?php
/*
 * Objective: Listing of all the coupons for new Clients
 * Filename: addProduct.php
 * Created By : Vinod Kumar Singh <vinod.kumar@ilmp-tech.com>
 * Modified by : Ravinder pal
 * modified on : 23 sep 2014
 */
?>
<?php
global $db;
$useId = $_SESSION['userId'];
$supplier_name = $_SESSION['username'];
$query = "SELECT id, title,file,type FROM " . _prefix("galleries") . "   "
        . " where deleted = 0  AND user_id=" . $useId . " AND type=0  ORDER BY id";
$city = '';
$res = $db->sql_query($query);
$data = $db->sql_fetchrowset($res);
$image = '';
$qryKnowMe = "select id, title from " . _prefix('know_me') . ' where  status=1 AND deleted=0';
$resultKnowMe = $db->sql_query($qryKnowMe);
$knowMeData = $db->sql_fetchrowset($resultKnowMe);
foreach ($knowMeData as $key => $record) {
    $knowMe[] = $record['title'];
}
$qryDuration = "select id, title from " . _prefix('durations') . ' where  status=1 AND deleted=0';
$resultDuration = $db->sql_query($qryDuration);
$durationVisitData = $db->sql_fetchrowset($resultDuration);
foreach ($durationVisitData as $key => $record) {
    $durationVisit[] = $record['title'];
}
$lastVisit = array('Within the last 12 months', '1 - 3 years ago', 'More than 3 years ago');
$recommended = array('No', 'Yes', 'Not sure');
//include_once("./fckeditor/fckeditor.php");
if ($_SERVER['HTTP_X_FORWARDED_FOR']) {
    $IPaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
} else {
    $IPaddress = $_SERVER['REMOTE_ADDR'];
}

//$geoData = unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip=' . $IPaddress));
//$lat = $geoData['geoplugin_latitude'];
//$long = $geoData['geoplugin_longitude'];

/* Default value */
$lat = -41.211722;
$long = 175.517578;
$zip = 5792;

$id = $_GET['id'];
$sql_query = "SELECT fdbk.*, pr.*,pr.id AS proId ,pr.supplier_id,facility.name as facility_type,city.title as city_name, "
        . "suburb.title as suburb_name,restCare.title as restcare_name,pr.id as pr_id, extinfo.*, "
        . "extinfo.id as ex_id  FROM " . _prefix("feedbacks") . " AS fdbk "
        . " Left join " . _prefix("products") . " AS pr ON pr.id=fdbk.product_id"
//        . " Left join " . _prefix("services") . " AS sr ON pr.facility_type=sr.id"
        . " Left join " . _prefix("cities") . " AS city ON pr.city_id=city.id"
        . " Left join " . _prefix("suburbs") . " AS suburb ON pr.suburb_id=suburb.id"
        . " Left join " . _prefix("rest_cares") . " AS restCare ON pr.restcare_id=restCare.id"
        . " Left join " . _prefix("services") . " AS facility ON pr.facility_type=facility.id"
        . " Left join " . _prefix("extra_facility") . " AS extinfo ON extinfo.product_id =pr.id"
        . " where md5(fdbk.id)='$id'";
$res = $db->sql_query($sql_query);
$record = $db->sql_fetchrow($res);
//prd($records);
if (count($record)) {
//    foreach ($records as $record) {
    //prd($record['pros']);
    $proId = $record['proId'];
    $supplier_id = $record['supplier_id'];
    $ex_id = $record['ex_id'];
    $pr_id = $record['pr_id'];
    $title = $record['title'];
    $description = $record['description'];
    $keyword = $record['keyword'];
    $image = $record['image'];
    $ext_url = $record['ext_url'];
    $youtube_video = $record['youtube_video'];
    $facility = $record['facility_type'];
    $city = $record['city_name'];
    $suburb = $record['suburb_name'];
    $restcare = $record['restcare_name'];
    $zip = $record['zip'];
    $staff_comment = $record['staff_comment'];
    $management_comment = $record['management_comment'];
    $activities_comment = $record['activity_comment'];
    $staff_image = $record['staff_image'];
    $management_image = $record['management_image'];
    $activities_image = $record['activity_image'];
    $extraInfo1 = $record['extra_info1'];
    $extraInfo2 = $record['extra_info2'];
    $extraInfo3 = $record['extra_info3'];
    $brief = $record['brief'];
    $noOfRooms = $record['no_of_room'];
    $noOfBeds = $record['no_of_beds'];
    $long = $record['longitude'];
    $lat = $record['latitude'];


    if (empty($lat) && empty($long)) {
        $lat = -41.211722;
        $long = 175.517578;
    }
    ?>
    <div class="container">
        <div class="row articles_container">
            <h1 class="theme_color_inner"  style="padding-left:5px;margin-bottom: 5px;">Details of <?php echo isset($title) ? stripslashes($title) : ''; ?><a href="<?php echo HOME_PATH . 'rating?spId=' . base64_encode($supplier_id) . '&prId=' . base64_encode($proId) ?>"><span class="btn btn-primary pull-right">Write a review</span></a></h1>
            <div class="col-md-12 col-sm-12 col-xs-12 container_wrapper">
                <form novalidate name="addLogo" id="addLogo" action="" method="POST" class="form-horizontal form_payment" role="form" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 list_container">
                            <div class="form-group" style="margin-bottom:6px;">
                                <label class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration">Geographical Presence </label>
                                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12" style="overflow: hidden;">
                                        <span class="pull-left right_td">
                                            <div id="map-canvas" class="img-responsive" style="width:350px;height:200px; margin-top:10px;"></div>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group" style="margin-bottom:6px;">
                                <label for="title" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration">Product/Service Name </label>
                                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                    <div class="input_fild_details"><?php echo isset($title) ? stripslashes($title) : 'N/A'; ?></div>
                                </div>
                            </div>
                            <div class="form-group" style="margin-bottom:6px;">
                                <label for="title" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration">Rating </label>
                                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                    <div class="input_fild_details">
                                        <span class="pull-left right_td"><?php echo overAllRatingProduct($record['pr_id']); ?></span>|
                                        <span class="">
                                            <?php if (overAllRatingProductCount($record['pr_id']) > 0) { ?>
                                                <a href="<?php echo HOME_PATH; ?>search/viewReview?id=<?php echo md5($record['pr_id']) ?>"><i class="fa fa-users"></i>&nbsp;<?php echo overAllRatingProductCount($record['pr_id']); ?></a>
                                            <?php } else { ?>
                                                <i class="fa fa-users"></i>&nbsp;<?php echo overAllRatingProductCount($record['pr_id']); ?>
                                            <?php } ?>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <?php
                            foreach ($data as $key => $record1) {
                                $fileImage = ADMIN_PATH . 'files/gallery/images/thumb_' . $record1['file'];
                                $fileImageSrc = HOME_PATH . 'admin/files/gallery/images/thumb_' . $record1['file'];
                                $fileImageView = HOME_PATH . 'admin/files/gallery/images/' . $record1['file'];
                                ;
                            }
                            if (@getimagesize(HOME_PATH . 'admin/files/gallery/images/' . $image)) {
                                ?>
                                <div style="margin-bottom:6px;" class="form-group">
                                    <label class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration" for="zip"></label>
                                    <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                        <img width="100px;" height="100px;" src="<?php echo HOME_PATH . 'admin/files/gallery/images/' . $image; ?>" title="Image">
                                    </div>
                                </div>
                            <?php } ?>
                            <div style="margin-bottom:6px;" class="form-group">
                                <label class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration" for="description">Images</label>
                                <?php echo getProductImages($id); ?>

                            </div>
                            <div style="margin-bottom:6px;" class="form-group">
                                <label class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration" >YouTube Video</label>
                                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                    <div class="discription_fild"><?php echo isset($youtube_video) ? $youtube_video : 'N/A'; ?></div>
                                </div>
                            </div>
                            <div class="form-group" style="margin-bottom:6px;">
                                <label for="extUrl" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration">External URL</label>
                                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                    <div class="input_fild_details"><?php echo isset($ext_url) ? '<a target="_blank" href="' . $ext_url . '">' . $ext_url . '</a>' : 'N/A'; ?></div>
                                </div>
                            </div>
                            <div style="margin-bottom:6px;" class="form-group">
                                <label class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration" for="description">Description</label>
                                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                    <div class="discription_fild"><?php echo isset($description) ? stripslashes($description) : 'N/A'; ?></div>
                                </div>
                            </div>
                            <div style="margin-bottom:6px;" class="form-group">
                                <label class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration" for="facility_type_id">Facility Type </label>
                                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                    <div class="discription_fild"><?php echo getServices($proId); ?><?php // echo isset($facility) ? stripslashes($facility) : 'N/A';                         ?></div>
                                    <!--<div class="input_fild_details"><?php // echo $proId.getServices($proId);                         ?><?php // echo isset($facility) ? stripslashes($facility) : 'N/A';                         ?></div>-->
                                </div>
                            </div>
                            <div style="margin-bottom:6px;" class="form-group">
                                <label class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration" for="city">City </label>
                                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                    <div class="input_fild_details"><?php echo isset($city) ? stripslashes($city) : ''; ?></div>
                                </div>
                            </div>
                            <div style="margin-bottom:6px;" class="form-group">
                                <label class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration" for="suburb">Suburb</label>
                                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                    <div class="input_fild_details"><?php echo isset($suburb) ? stripslashes($suburb) : 'N/A'; ?></div>
                                </div>
                            </div>
                            <div style="margin-bottom:6px;" class="form-group">
                                <label class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration" for="restcare">Rest Care</label>
                                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                    <div class="input_fild_details"><?php echo isset($restcare) ? stripslashes($restcare) : 'N/A'; ?></div>
                                </div>
                            </div>
                            <div style="margin-bottom:6px;" id="zipid" class="form-group">
                                <label class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration" for="zip">Zip Code </label>
                                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                    <div class="input_fild_details"><?php echo isset($zip) ? stripslashes($zip) : 'N/A'; ?></div>
                                </div>
                            </div>

                            <div class="panel-group" id="accordion" style="padding-top:3%;">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Facility Description</a></h4>
                                    </div>
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            <div style="width:95%; margin:0px auto;" class="form-group">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="col-md-12">
                                                        <label class="col-lg-3 col-md-3 col-sm-9 col-xs-12 label_list " for="brief">Brief Description</label>
                                                        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                                            <div class="discription_fild"><?php echo isset($brief) ? stripslashes($brief) : 'N/A'; ?></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                            <label class="col-lg-6 col-md-6 col-sm-10 col-xs-12 label_list text-left duration" for="noOfRooms">No. of Dwellings</label>
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <div class="input_fild_details"><?php echo isset($noOfRooms) ? stripslashes($noOfRooms) : 'N/A'; ?></div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 pull-right">
                                                            <label class="col-lg-6 col-md-6 col-sm-10 col-xs-12 label_list text-left duration" for="noOfBeds">No. of Beds</label>
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <div class="input_fild_details"><?php echo isset($noOfBeds) ? stripslashes($noOfBeds) : 'N/A'; ?></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <label class="col-lg-3 col-md-3 col-sm-9 col-xs-12 label_list " for="brief">Extra Info.</label>
                                                        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                                            <div class="discription_fild"><?php echo isset($extraInfo1) ? stripslashes($extraInfo1) : 'N/A'; ?> <?php echo isset($extraInfo2) ? stripslashes($extraInfo2) : ''; ?></div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Staff</a></h4>
                                    </div>
                                    <div id="collapseTwo" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            <div style="width:95%; margin:0px auto;" class="form-group">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="col-md-12">
                                                        <label class="col-lg-3 col-md-3 col-sm-9 col-xs-12 label_list" for="comment">Description</label>
                                                        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                                            <div class="discription_fild"><?php echo!empty($staff_comment) ? stripslashes($staff_comment) : 'N/A'; ?></div>
                                                        </div>
                                                    </div>
                                                    <?php
                                                    if (@getimagesize(MAIN_PATH . '/files/product/staff/' . $staff_image)) {
                                                        ?>
                                                        <div class="col-md-12">
                                                            <label class="col-lg-3 col-md-3 col-sm-9 col-xs-12 label_list text-left duration" for="image">Image</label>
                                                            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">

                                                                <img width="100px;" height="100px;" src="<?php echo MAIN_PATH . '/files/product/staff/' . $staff_image ?>" title="Staff Image">
                                                            </div>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">Management</a>
                                        </h4>
                                    </div>
                                    <div id="collapseThree" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            <div style="width:95%; margin:0px auto;" class="form-group">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="col-md-12">
                                                        <label class="col-lg-3 col-md-3 col-sm-9 col-xs-12 label_list" for="comment">Description</label>
                                                        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                                            <div class="discription_fild"><?php echo!empty($management_comment) ? stripslashes($management_comment) : 'N/A'; ?></div>
                                                        </div>
                                                    </div>
                                                    <?php
                                                    if (@getimagesize(MAIN_PATH . 'files/product/management/' . $management_image)) {
                                                        ?>
                                                        <div class="col-md-12">
                                                            <label class="col-lg-3 col-md-3 col-sm-9 col-xs-12 label_list text-left duration" for="image" >Image</label>
                                                            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">

                                                                <img width="100px;" height="100px;" src="<?php echo MAIN_PATH . 'files/product/management/' . $management_image ?>" title="Management Image">
                                                            </div>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">Activities</a></h4>
                                    </div>
                                    <div id="collapseFour" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div style="width:95%; margin:0px auto;" class="form-group">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="col-md-12">
                                                        <label class="col-lg-3 col-md-3 col-sm-9 col-xs-12 label_list" for="comment">Description</label>
                                                        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                                            <div class="discription_fild"><?php echo!empty($activities_comment) ? stripslashes($activities_comment) : 'N/A'; ?></div>
                                                        </div>
                                                    </div>
                                                    <?php
                                                    if (@getimagesize(MAIN_PATH . 'files/product/activity/' . $activities_image)) {
                                                        ?>
                                                        <div class="col-md-12">
                                                            <label class="col-lg-3 col-md-3 col-sm-9 col-xs-12 label_list text-left duration" for="image">Image</label>
                                                            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                                                <img width="100px;" height="100px;" src="<?php echo MAIN_PATH . 'files/product/activity/' . $activities_image ?>" title="Activity Image">
                                                            </div>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 20px;" id="pageData"></div>

                                <div id="lazyload" style="margin-top:20px;"><span class="col-md-4 bg-info"> Click here to view feedback..</span></div>    
                                
                            </div>
                            <script type="text/javascript">
                                var address = '<?php echo!empty($city) ? $city : 'Auckland'; ?>' + ', New Zealand';
                                var map = new google.maps.Map(document.getElementById('map-canvas'), {
                                    mapTypeId: google.maps.MapTypeId.TERRAIN,
                                    zoom: 15
                                });
                                var geocoder = new google.maps.Geocoder();
                                geocoder.geocode({
                                    'address': address
                                },
                                function(results, status) {
                                    if (status == google.maps.GeocoderStatus.OK) {
                                        new google.maps.Marker({
                                            position: results[0].geometry.location,
                                            map: map
                                        });
                                        map.setCenter(results[0].geometry.location);
                                        var infowindow = new google.maps.InfoWindow(
                                                {content: '<b>' + address + '</b>',
                                                    size: new google.maps.Size(500, 273)
                                                });
                                        var marker = new google.maps.Marker({
                                            position: results[0].geometry.location,
                                            map: map,
                                            title: address
                                        });
                                        google.maps.event.addListener(marker, 'click', function() {
                                            infowindow.open(map, marker);
                                        });
                                    }
                                });
                                $(document).ready(function() {
    //                                    $('.read_more').click(function() {
    //                                        $(this).text() == 'Hide Feedbacks...' ? $(this).text('Click here to view feedbacks..') : $(this).text('Hide Feedbacks...');
    //                                    });
                                   
                                    $("#lazyload").click(function() {
                                        var image = '<?php echo ADMIN_IMAGE; ?>' + 'approval.gif';
                                        var page = 0;
                                        var li = '0_no';
                                        var fid="<?php echo !empty($_GET['fid'])?$_GET['fid']:''; ?>";
                                        var data = '&id=<?php echo md5($record['pr_id']) ?>';
                                        $('#pageData').html('<div align="center"><img src=' + image + ' ></div>');
                                        var exAction = function() {
                                            if(fid!='') {
                                                $('html, body').animate({
                                                    scrollTop: $("#"+fid).offset().top 
                                                }, 'fast');
                                            }
                                        }
                                        changePagination(page, li, 'viewFeedback', data, exAction);
                                        $(function() {
                                            $("div.load").lazyload({
                                                event: "sporty"
                                            });
                                        });

                                        $(window).bind("load", function() {
                                            var timeout = setTimeout(function() {
                                                $("div.load").trigger("sporty")
                                            }, 5000);
                                        });
                                    });
                                    <?php 
                                    if(isset($_GET['fid'])) { ?>
                                        $("#lazyload").click();
                                   <?php } ?>

                                });
                                $(function() {
                                    $('#collapseFour').collapse({
                                        toggle: false
                                    })
                                });
                                $(function() {
                                    $('#collapseTwo').collapse('show')
                                });
                                $(function() {
                                    $('#collapseThree').collapse('toggle')
                                });
                                $(function() {
                                    $('#collapseOne').collapse('hide')
                                });
                            </script>
                        </div>
                </form>
            </div>
        </div>
    </div>
    </div>
    <!--      </div>-->

    <?php
//    }
//    die();
}
?>




<style>
    th,td,tr{border:0px!important; }
    .input{background-color:white;color: black;}
    label.error{
        color:red;
        font-size: 10px;

    }
    .redCol{
        color:red;
    }

</style>

