<?php
include("../application_top.php");

if ($_REQUEST['submit']) {
    $useremail = $_REQUEST['user_email'];
    $sql_query = "select * from " . _prefix("users") . " where email='$useremail'";
    $res = $db->sql_query($sql_query);
    $records = $db->sql_fetchrowset($res);
    if (count($records)) {
        $to = $records[0]['email'];
        $random = Random_Password(6);
        $new_password = md5($random);
        $fields = array('password' => $new_password);
        $where = "where email='$to'";
        $update_result = $db->update(_prefix('users'), $fields, $where);
        if ($update_result) {
            $email = emailTemplate('customer_forgot-password');

            if ($email['subject'] != '') {
                $message = str_replace(array('{forgotpass}'), array($random), $email['description']);
                $send = sendmail($to, $email['subject'], $message);
                if ($send) {
                    
                    $msg = common_message_supplier(1, constant('CUSTOMER_FORGOT_PASSWORD'));
                    $_SESSION['forgot_msg'] = $msg;
                    redirect_to(HOME_PATH . "forgotPassword");
                }
            }
        }
    } else {
        $errors = '';
        foreach ($response as $key => $message) {
            $error = true;
            $errors .= $message . "<br>";
        }
    }
}
?>
<div class="row">
    <div class="col-sm-offset-2 col-sm-6 success">
        <?php if (isset($error)) { ?>
            <div class="errorForm" id="error" style="display: block;">
                <img align="absmiddle" src="<?php echo HOME_PATH . '/images/error.png'; ?>">&nbsp;<?php echo $errors; ?>.<br>
            </div>
            <?php
        }
        if (isset($_SESSION['forgot_msg']) && !empty($_SESSION['forgot_msg'])) {
            ?>
            <?php echo $_SESSION['forgot_msg']; ?><br>
            <?php
            unset($_SESSION['forgot_msg']);
        }
        ?>
    </div>
</div>
 

<div class="user-login-section section-padding bg-fixed" style="margin-top:80px;">
            <div class="container">
                <div class="row">
                    <div class="col-md-10 offset-md-1  text-center">
                        <div class="login-wrapper">
                        <h1 class="theme_color">Forgot Password?</h1>
                            <div class="ui-dash tab-content">
                                <div class="tab-pane fade show active" id="login" role="tabpanel">
                                     <form name="form1" action="" method="post" id="forget_Password" >
                                        
                                        <div class="form-group">
                                          <input type="text" placeholder="Write Email *" name="user_email"  id="user_email"  class=" form-control  filter-input input_fild">
                                        </div>
                                        
                                        <div class="res-box text-center mar-top-30">
                                         <input type="submit" name="submit" class="btn btn-danger center-block btn_search" value="Submit">
                                        </div>
                                    </form>
                                     
                                </div>
                               
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>






<script type="text/javascript">
    $(document).ready(function() {
        setTimeout(function() {
            $("#success").hide('slow');
            $('#error').hide('slow');
        }, 15000);
        var path = '<?php echo HOME_PATH; ?>admin/';
        $('#forget_Password').validate({
            rules: {
                user_email: {
                    required: true,
                    email: true,
                    remote: {
                        url: path + 'ajax.php',
                        type: 'POST',
                        data: {action: "unquieCustomerEmail", email: function() {
                                return $('#user_email').val();
                            }
                        }
                    }
                }

            },
            messages: {
                user_email: {
                    required: "This field is required",
                    email: "Please enter a valid email address.",
                    remote: "Email address does not exist"
                }

            }
        });
    });
</script>
<style type="text/css">
    label.error{
        color:red;
        font-size:10px;
    }
</style>