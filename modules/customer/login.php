<?php
global $db;
include("../application_top.php");
require_once ('modules/fblogin/facebook-sdk-v5/autoload.php');
    
$fb = new Facebook\Facebook([
  'app_id' => '1409054779407381', // Replace {app-id} with your app id
  'app_secret' => 'e7dcb280e81129960a9a99d60b91c3f9',
  'default_graph_version' => 'v2.8'
]);

$helper = $fb->getRedirectLoginHelper();

$permissions = ['email','user_location','public_profile']; // Optional permissions
$loginUrl = $helper->getLoginUrl('https://www.agedadvisor.nz/facebook-login/', $permissions);
$loginUrl = htmlspecialchars($loginUrl);
$_SESSION['back_url'] = 'home';
    


if ($return) {
    $time = time() + 7 * 24 * 3600;
    setcookie('back_url', $_SERVER['HTTP_REFERER'], $time);
    if(isset($msgR)&&!empty($msgR)){
        $_SESSION['msg'] = base64_decode($msgR);
    }
}

    if (isset($_POST['user_id']) && $_POST['user_id']) {

    if (isset($_SESSION['userId'])) {
        $msg = "First you logOut from the Supplier and then logIn";
        $_SESSION['msg'] = $msg;
        $url = HOME_PATH . 'login';
        redirect_to($url);
        // break;
    }
    $fields_array = customerLogin();
    $response = Validation::_initialize($fields_array, $_POST);


    if (isset($response['valid']) && $response['valid'] > 0) {
        $qryCheck = 'select * from ' . _prefix('users') . ' where validate=3 AND status=1 AND deleted= 0 AND user_name="' . $user_id . '" and password="' . md5($password) . '" and (user_type=0 )';
        $resultCheck = $db->sql_query($qryCheck);
        $dataCheck = $db->sql_fetchrow($resultCheck);
        if ($db->sql_numrows($resultCheck) > 0) {
                $_SESSION = array();
        	$_SESSION['csUserFullName'] = $dataCheck['first_name'] . " " . $dataCheck['last_name'];
        	$_SESSION['csEmail'] = $dataCheck['email'];
        	$_SESSION['csUserName'] = $dataCheck['user_name'];
        	$_SESSION['csId'] = $dataCheck['id'];
            redirect_to(HOME_PATH . 'changePassword?id=' . md5($dataCheck['id']));
        }
        $qry = "select * from " . _prefix('users') . ' where (validate=2 AND status=1 AND deleted= 0 AND user_name="' . $user_id . '" AND password="' . md5($password) . '" AND user_type=0)';
        $result = $db->sql_query($qry);
        $data = $db->sql_fetchrow($result);
        if ($db->sql_numrows($result) > 0) {
            /* To destroy the session */
//            session_unset();
            //  session_destroy();
            $_SESSION = array();
            $_SESSION['csUserFullName'] = $data['first_name'] . " " . $data['last_name'];
            $_SESSION['csEmail'] = $data['email'];
            $_SESSION['csUserName'] = $data['user_name'];
            $_SESSION['csId'] = $data['id'];
            $user_type = 0;
            if ($_POST['checkbox'] == 'on') {
                $time = time() + 7 * 24 * 3600;
                setcookie('csRemember', '1', $time);
                setcookie('csUserFullName', $_POST['user_id'], $time);
                setcookie('csPassword', $_POST['password'], $time);
                setcookie('csCheckBox', $_POST['checkbox'], $time);
                setcookie('AutoLoginId', $_POST['user_id'], $time);
                setcookie('loginUserType', $user_type, $time);
            } else {
                if (isset($_COOKIE['csRemember'])) {
                    $past = time() - 100;
                    setcookie('csRemember', '', $past);
                    setcookie('AutoLoginId', '', $past);
                    setcookie('csUserFullName', '', $past);
                    setcookie('password', '', $past);
                }
                setcookie('csCheckBox', '', $past);
            }
            if (isset($_COOKIE[back_url])) {
                $url = $_COOKIE[back_url];
                $past = time() - 100;
                setcookie('back_url', '', $past);
                redirect_to($url);
            } else {
                redirect_to(HOME_PATH);
            }
        } else{
            $qryCheck = 'select * from ' . _prefix('users') . ' where validate=3 AND status=1 AND deleted= 0 AND user_name="' . $user_id . '" and password="' . md5($_POST['password']) . '" and (user_type=1 )';

            $resultCheck = $db->sql_query($qryCheck);
            $dataCheck = $db->sql_fetchrow($resultCheck);
           
//            $qry = "select * from " . _prefix('users') . ' where validate=2 AND status=1 AND deleted= 0 AND ((user_name="' . $user_id . '" and password=")OR (email=))' . md5($password) . '" and (user_type=1 )';
            $qry = "select user.*, member.video_enable, member.youtube_video, member.image_enable, member.image_limit, "
                    . " member.ext_url_enable, member.type from " . _prefix('users') . " AS user "
                    . " Left join " . _prefix("membership_prices") . " AS member ON member.id=user.account_type  "
                    . "where ((user.validate=2 OR user.validate=3) AND user.status=1 AND user.deleted= 0 AND user.user_name='" . $user_id . "' AND user.password='" . md5($password) . "' AND user.user_type=1)";
            $result = $db->sql_query($qry);
            $data = $db->sql_fetchrow($result);
            if ($db->sql_numrows($result) > 0) {
                // session_destroy();
                $_SESSION['username'] = $data['first_name'] . " " . $data['last_name'];
                $_SESSION['userEmail'] = $data['email'];
                $_SESSION['userId'] = $data['id'];
                $_SESSION['id'] = $data['id'];
                $_SESSION['SupUserName'] = $data['user_name'];
                $_SESSION['SupImage'] = $data['user_image'];
                $_SESSION['SupValidate'] = $data['validate'];
//                $_SESSION['videoEnable'] = $data['video_enable'];
//                $_SESSION['youtubeEnable'] = $data['youtube_video'];
//                $_SESSION['imageEnable'] = $data['image_enable'];
//                $_SESSION['mediaLimit'] = $data['image_limit'];
//                $_SESSION['extUrlEnable'] = $data['ext_url_enable'];
//                $_SESSION['planType'] = $data['type'];

                

                if ($_POST['checkbox'] == 'on') {
                    $time = time() + 7 * 24 * 3600;
                    setcookie('remember', '1', $time);
                    setcookie('userName', $_POST['user_id'], $time);
                    setcookie('password', $_POST['password'], $time);
                    setcookie('checkBox', $_POST['checkbox'], $time);
                    setcookie('AutoLoginId', $_POST['user_id'], $time);
                    setcookie('loginUserType', $user_type, $time);
                } else {
                    if (isset($_COOKIE['remember'])) {
                        $past = time() - 100;
                        setcookie('remember', '', $past);
                        setcookie('AutoLoginId', '', $past);
                        setcookie('userName', '', $past);
                        setcookie('password', '', $past);
                    }
                    setcookie('checkBox', '', $past);
                }
                if ($db->sql_numrows($resultCheck) > 0) {
//                prd($db->sql_numrows($resultCheck));
                $_SESSION['xlsUser'] = true;
                redirect_to(HOME_PATH . 'supplier/changePassword?id=' . md5($dataCheck['id']));
            }

                redirect_to(HOME_PATH . 'supplier/productList');
            } else {
                $msg = "Invalid User Name and Password";
                $_SESSION['msg'] = $msg;
                redirect_to(HOME_PATH . 'login');
            }
        }
    } else {
        $errors = '';
        foreach ($response as $key => $message) {
            $error = true;
            $errors .= $message . "<br>";
        }
    }
}
?>

<script type="text/javascript">
    $(document).ready(function() {

        $('#CustomerLogin').validate();
        setTimeout(function() {
            $('#error').hide('slow');
        }, 5000);

 
    });
</script>
<style type="text/css">
    label.error{
        color:red;
        font-size: 14px;font-weight: bold;
    }
</style>

    <!--=== Content Part ===-->
  <!--  <div class="container content">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
                
                
                
                        <?php if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
                        ?>
                        <div class="alert alert-warning alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <?php echo $_SESSION['msg']; ?>
                        </div>
                        <?php
                        unset($_SESSION['msg']);
                    }
                     if (isset($errors) && !empty($errors)) {
                        ?>
                        <div class="alert alert-warning alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <?php echo $errors; ?>
                        </div>
                        <?php
                    }
                    ?>

                
                
                
                
                <form method="POST" action="" id="CustomerLogin" name="CustomerLogin">
                    <div class="reg-header">
                        <h2 class="text-center">Login to your account</h2>
                        <p class="text-center">Don't have an account? <a href="<?php echo HOME_PATH; ?>register" class="color-green">Create a new account</a></p>
                        <p class="text-center">Login using your <a href="login-twitter.php">Twitter</a> or <a href="<?php echo $loginUrl?>">Facebook</a> details.<br>(NOTE: We will not display your photo or contact details)</p>
                        <p class="text-center">
						<a href="login-twitter.php">
						<img style="width: 141px;height: 29px;" src="<?php echo HOME_PATH; ?>images/sign_in_with_twitter.png" alt="">
						</a>&nbsp;
						<a href="<?php echo $loginUrl?>">
						<img style="width: 141px;height: 29px;" src="<?php echo HOME_PATH; ?>images/sign_in_with_facebook.png" alt="">
						</a></p>
                        <div class="heading heading-v4"><h2>or</h2></div>
                        <p class="text-center">Enter your login details below.</p>
                    
                    </div>

                    <input type="text" placeholder="User Name" name="user_id" id="user_id" class="form-control input_fild required" value="<?php
                            if (isset($_COOKIE['csRemember'])) {
                                echo $_COOKIE['csUserFullName'];
                            }
                            else if
                            (ISSET($_POST['user_id'])){echo cleanthishere($_POST['user_id']);}
                            ?>">

                    <input type="password" placeholder="Password" id="password" name="password" class="form-control input_fild required margin-top-20"  value="<?php  if(ISSET($_POST['password'])){echo cleanthishere($_POST['password']);}?>">
                    <div class="row margin-top-20">            
                        <div class="col-md-7 col-sm-7 col-xs-12 padding_left chechbox_text">
                                    <input id="rememberMe" type="checkbox" class="chechbox_remember" name="checkbox" <?php
                                    if (isset($_COOKIE['csRemember'])) {
                                        echo 'checked="checked"';
                                    } else {
                                        echo '';
                                    }
                                    ?>><label for="rememberMe" style="display: inline;font-weight: normal;"> <span>Remember me</span></label>
                        </div>
                        <div class="col-md-5 col-sm-5 col-xs-12 padding_left padding_right text-right forgot_password"><a href="<?php echo HOME_PATH; ?>forgotPassword">Forgot Password?</a>
                        </div>
                    </div>
                    
                            <button type="button" id="submit_button" class="btn btn-danger center-block inner_btn" style="margin-bottom:0px; padding:9px 10%; font-size:18px;">LOGIN</button>                     
                
                
                <p class="text_login text-center margin-top-20">Are you a Retirement Village, AgedCare Owner, or Facility Manager ? <a href="<?php echo HOME_PATH . 'supplier/register'; ?>" style="color:#f15922;">Then click here</a></p>
                </div>
                </form>
            </div>
        </div>
    </div>-->
	
	
	<div class="user-login-section section-padding bg-fixed">
            <div class="container " style=" ">
			
			 
                
                        <?php if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
                        ?>
                        <div class="alert alert-warning alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <?php echo $_SESSION['msg']; ?>
                        </div>
                        <?php
                        unset($_SESSION['msg']);
                    }
                     if (isset($errors) && !empty($errors)) {
                        ?>
                        <div class="alert alert-warning alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <?php echo $errors; ?>
                        </div>
                        <?php
                    }
                    ?>
			
			
                <div class="row">
                    <div class="col-md-10 offset-md-1  text-center">
                        <div class="login-wrapper1">
                            <ul class="ui-list nav nav-tabs justify-content-center mar-bot-30" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active"  href="<?php echo HOME_PATH;?>login" role="tab" aria-selected="true">Login</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link"   href="<?php echo HOME_PATH; ?>register"  >Register</a>
                                </li>
                            </ul>
							
							  <p class="text-center">Don't have an account? <a href="<?php echo HOME_PATH; ?>register" class="color-green">Create a new account</a></p>
                        <p class="text-center">Login using your <a href="login-twitter.php">Twitter</a> or <a href="<?php echo $loginUrl?>">Facebook</a> details.<br>(NOTE: We will not display your photo or contact details)</p><br>
							
                            <div class="ui-dash tab-content">
                                <div class="tab-pane fade show active" id="login" role="tabpanel">
                                   <form method="POST" action="" id="CustomerLogin" name="CustomerLogin">
                                        <div class="form-group">
                                        <input type="text" placeholder="User Name" name="user_id" id="user_id" class="form-control filter-input input_fild required" value="<?php
                            if (isset($_COOKIE['csRemember'])) {
                                echo $_COOKIE['csUserFullName'];
                            }
                            else if
                            (ISSET($_POST['user_id'])){echo cleanthishere($_POST['user_id']);}
                            ?>">
                                        </div>
                                        <div class="form-group">
                                           <input type="password" placeholder="Password" id="password" name="password" class="form-control filter-input input_fild required margin-top-20"  value="<?php  if(ISSET($_POST['password'])){echo cleanthishere($_POST['password']);}?>">
                                        </div>
                                        <div class="row mar-top-20">
                                            <div class="col-md-6 col-12 text-left">
                                                <div class="res-box">
                                                    <input id="rememberMe" type="checkbox" class="chechbox_remember" name="checkbox" <?php
                                    if (isset($_COOKIE['csRemember'])) {
                                        echo 'checked="checked"';
                                    } else {
                                        echo '';
                                    }
                                    ?>>
                                                    <label for="remember">Remember Me</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12 text-right">
                                                <div class="res-box sm-left">
                                                    <a href="<?php echo HOME_PATH; ?>forgotPassword">Forgot Password?</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="res-box text-center mar-top-30">
                                            <button type="submit" id="submit_button"  class="btn v3"><i class="ion-log-in"></i> Log In</button>
                                        </div>
										
										  <p class="text_login text-center margin-top-20">Are you a Retirement Village, AgedCare Owner, or Facility Manager ? <a href="<?php echo HOME_PATH . 'supplier/register'; ?>" style="color:#f15922;">Then click here</a></p>
                                   
                                    <div class="social-profile-login text-center mar-top-30">
                                        <h5>or Login with</h5>
                                        <ul class="social-btn">
                                            <li class="bg-fb"><a href="<?php echo $loginUrl?>"><i class="ion-social-facebook"></i></a></li>
                                            <li class="bg-tt"><a href="login-twitter.php"><i class="ion-social-twitter"></i></a></li>
                                            <li class="bg-ig"><a href="#"><i class="ion-social-instagram"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                 </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	
	
	
	
	
     <!--=== End Content Part ===-->
<script type="text/javascript">
    			$('#submit_button').click(function(e) {
					e.preventDefault();
					$('#CustomerLogin').submit();
				});
</script>
                              
                       
                                
 
