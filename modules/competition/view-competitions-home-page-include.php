<?php
ini_set('display_errors', '1');

function adjustcompetitions() {
  global $db;
  $notify_message='';
  $sql = "select * from";
  $sql .= " competition where status in (0,1,2,3) order by status,enddate desc";

  
  $result = mysqli_query($db->db_connect_id,$sql ) or die( "Error getting list" );
  
  while ( $myrow = mysqli_fetch_row( $result ) ) {
    $strID = $myrow[ 0 ];
    $strname = $myrow[ 1 ];
    $strdescription = $myrow[ 3 ];
    $strdescription = str_replace( "\n", "<br>", $strdescription );

    $strstartdate = $myrow[ 4 ];
    $strenddate = $myrow[ 5 ];
    $strvotedate = $myrow[ 6 ];
    $mediaallowed = $myrow[ 7 ];
    $strstatus = $myrow[ 8 ];
    $newstatus = $strstatus;
      if (time() <= (strtotime( "$strstartdate" ))) {
        $newstatus = 0;
      }

    if ($strstatus == 0) { // Has it started?
      if (time() > (strtotime( "$strstartdate" ))) {
        $newstatus = 1;
      }
    } // end of has it started
    if ($strstatus == 1) { // voting?
      if (time() > (strtotime( "$strvotedate" ) + 86400)) {
        $newstatus = 2;
      }
    } // end of start of voting
    if ($strstatus == 2) { // voting closed?
        $time_left_to_vote=((strtotime( "$strenddate" ) + 86400)-time())/60;//minutes
        if($time_left_to_vote<5000){$notify_message="Voting for $strname finishes in ".floor($time_left_to_vote/1440)." days.";}
        if($time_left_to_vote<1440){$notify_message="Voting for $strname finishes in ".floor($time_left_to_vote/60)." hours.";}
        if($time_left_to_vote<60){$notify_message="Voting for $strname finishes in ".ceil($time_left_to_vote)." minutes.";}
        
        
        
      if (time() > (strtotime( "$strenddate" ) + 86400)) {
        $newstatus = 3;
      }
    } // end of stop voting



    if ($strstatus !== $newstatus) { // we must update to new status
      
      $sqlinc = "update competition set status='$newstatus' where ID='$strID'";
      mysqli_query( $db->db_connect_id,$sqlinc ) or die( "Error updating status of competition" );

    } //end of update the status
  } // on to next competition
  return $notify_message;
} // end of function adjustcompetitions



function howmanyentries( $competition ) {
  global $db;
  $numresults = 0;
  // find out how many times anyone has entered this competition
  $sql = "select ID ";
  $sql .= "FROM competition_media ";
  $sql .= "WHERE competitionID=$competition";
  
  $result = mysqli_query( $db->db_connect_id,$sql );
  if ($result) { //  if (mysqli_num_rows($result)>0){
    $numresults = mysqli_num_rows( $result );
  }
 		$sql="update `competition` set number_of_entries = '$numresults' WHERE ID='$competition' ";
		
		mysqli_query($db->db_connect_id,$sql) or die("<br>$sql<br>Error updating table");
  
  
 
  return $numresults;
}


function Numbervotes( $competition ) {
  global $db;
  $sql = "Select SUM(votes) from competition_media where competitionID='$competition'";
  $result = mysqli_query($db->db_connect_id, $sql ) or die( "$sql Error checking number of Entry" );
  if ($result) { //  if (mysqli_num_rows($result)>0){
    $myrow = mysqli_fetch_row( $result );
    $numvotes = $myrow[ 0 ];
  }
  if ($numvotes > 0) {
    $back = "Votes: $numvotes";
    return $back;
  } else {
    return;
  }
}


function printdate( $strdate ) {
  return date( "d F Y", strtotime( "$strdate" ) );
}




// If they are not logged in then they
// get set to the guest group

$strDate = date( 'F Y' );



$notify_message= adjustcompetitions();

$header_title="List of current competitions";

$responsivetabs=FALSE;
$home_banner=FALSE;
$feature_slider=FALSE;
$feature_slider=FALSE;


?>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">&nbsp;
	                        
                        </div>
                    </div>
<?php 
  // Cycle through competitions here



  $sql = "select * from";
  $sql .= " competition where status in (1,2,3) order by status,enddate desc  limit 0,2";

  
  $result = mysqli_query($db->db_connect_id, $sql ) or die( "Error getting list" );
  ;
  while ( $myrow = mysqli_fetch_row( $result ) ) {
    $strID = $myrow[ 0 ];
    $strname = $myrow[ 1 ];
    $strname = str_replace( '"', "&quot;", $strname );
    $strdescription = $myrow[ 3 ];
    $strdescription = str_replace( "\n", "<br>", $strdescription );
    $strdescription = str_replace( "\r", '', $strdescription );

    $strstartdate = $myrow[ 4 ];
    $strenddate = $myrow[ 5 ];
    $strvotedate = $myrow[ 6 ];
    $mediaallowed = $myrow[ 7 ];
    $strstatus = $myrow[ 8 ];
 		$small_advert = "http://localhost/".$myrow[12];
		
 		// remove http
		
 		$small_advert = str_ireplace('http:', '', $small_advert);
		
		$banner_advert = $myrow[13];
		$finished_announcement = $myrow[14];
		$meta_tags = $myrow[15];
		$number_of_entries = $myrow[16];
   $strmedia = explode( ",", $mediaallowed );
    
      $text_color= '';$text_color_close='';$image_fade='';

    if ($strstatus == "3") {
      $text_color= '<span style="color:#B0B0B0;">';$text_color_close='</span>';$image_fade='style= "filter: alpha(opacity=40); opacity: 0.4;" ';
    }
    else{$number_of_entries = howmanyentries( $strID );}
   
 $img='';   

if($small_advert){$img= '<a href="https://www.agedadvisor.nz/view-all-entries/' . $strID . '">'."<img class=\"img-thumbnail\" src=\"$small_advert\" alt=\"$strname\" $image_fade></a>";}
$status='';

    if ($strstatus == "1") {
      $status.= "Active, voting starts ";
      $status.= printdate( $strvotedate );
    }
    if ($strstatus == "2") {
      $status.= "Voting, late entries still accepted, Voting until ";
      $status.= printdate( $strenddate );
    }
    if ($strstatus == "3") {
      $status.= "Finished: ";
      $status.= printdate( $strenddate );
    }
    $status.='.';

    if ($number_of_entries >2) {$status.= " We have $number_of_entries ";
    if ($number_of_entries == 1) {
      $status.= "entry.";
    } else {
      $status.= "entries.";
    }
    }
    $how_to_enter='';
    if ($strstatus == "1") {
      $how_to_enter.= '<a style="margin: 5px;" class="btn btn-danger" role="button" href="https://www.agedadvisor.nz/new-media/' . $strID . '">Upload your entry here</a>&nbsp; ';
    }
    if ($number_of_entries > 0 && $strstatus == "1") {
      $how_to_enter.=  '<a style="margin: 5px;" class="btn btn-info" role="button"   href="https://www.agedadvisor.nz/view-all-entries/' . $strID . '">View Entries</a>';
    }
    if ($strstatus == "2") {
      $how_to_enter.=  '<a style="margin: 5px;" class="btn btn-danger" role="button" href="https://www.agedadvisor.nz/new-media/' . $strID . '">Upload your entry here</a>&nbsp; 
      <a style="margin: 5px;" class="btn btn-info" role="button"  href="view-all-entries/' . $strID . '">View entries and vote here</a> ' . Numbervotes( $strID );
    }
    if ($strstatus == "3") {
      $how_to_enter.=  '<a style="margin: 5px;" class="btn btn-info" role="button"  href="https://www.agedadvisor.nz/view-all-entries/' . $strID . '">View entries and results here</a>';
    }
$how_to_enter.='<br>';
?>
                    <div class="row" style="margin-bottom: 10px;">
                        <div class="col-md-3">
                        <?php echo $img?>
                        </div>
                        <div class="col-md-9">
                         <?php echo $text_color?><h2><?php echo $strname?></h2>
	                    <?php echo $strdescription?><br>
	                    <b>Status:</b> <?php echo $status?>
	                    <div style="margin-top: 6px;"><?php echo $how_to_enter?> </div>  
                        <?php echo $text_color_close?>
                        
                        </div>
                    </div>
  
<?php

  }
  // End of cycle through competitions
  ?>

                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
	                        <!--<a href="https://www.agedadvisor.nz/view-competitions">View Previous Competitions</a>-->
                        </div>
                    </div>



