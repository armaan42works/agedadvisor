<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');
global $db;
$facility_name = '';

function dbconnecti() {
    global $db;
    return $db;
}

// Set USERS, LANGUAGES, CURRENCIES, BASKET

if (isset($_SESSION['userId']) && !empty($_SESSION['userId'])) {
    $userId = $_SESSION['userId'];
} else if (isset($_SESSION['csId']) && !empty($_SESSION['csId'])) {
    $userId = $_SESSION['csId'];
}

$usernumber = $userId;
$boolError = '';
$alert_message = '';
$notify_message = '';


$allowededit = FALSE;
$item = $_REQUEST["item"];
if (!is_numeric($item)) {
    echo"Access denied - incorrect data";
    exit;
}
$comp = $_REQUEST["comp"];
if (!is_numeric($item)) {
    echo"Access denied - incorrect data";
    exit;
}

$GLOBALS['DB'] = dbconnecti();
$sql = "SELECT * FROM competition where ID='$comp' AND status in (1,2,3)";
$result = mysqli_query($db->db_connect_id, $sql) or die("Error getting competition details");
if ($result) {
    $myrow = mysqli_fetch_row($result);
    $name = $myrow[1];

    $strstatus = $myrow[8];
}

// If they are not logged in then they
// get set to the guest group
if (!$usernumber) {
    echo"Access denied members only";
    exit;
}

if ($strstatus > 2 && !$allowededit) {
    echo"Sorry competition Finished";
    exit;
}



// Clean and test variables in Header for security


if (!is_numeric($item)) {
    echo"Access denied";
    exit;
}//
// Are they allowed to edit this name?
$GLOBALS['DB'] = dbconnecti();
$sql = "SELECT ID,title,userID,description,file_medium FROM competition_media where ID='$item'";
$result = mysqli_query($db->db_connect_id, $sql) or die("Error finding Owner of media");
if ($result) {
    $myrow = mysqli_fetch_row($result);
    $medianame = $myrow[1];
    $mediaowner = $myrow[2];
    $media_description = $myrow[3];
    $image_small = $myrow[4];
}


$strImage = "https://www.agedadvisor.nz/images/competition/" . $image_small;

$strThumb = "<img class= \"img-responsive\" src =\"" . $strImage . "\" alt=\"" . $medianame . "\" title=\"" . $medianame . "\" border=\"0\">";




if ($mediaowner == $usernumber || $allowededit) {
    
} else {
    echo"Access denied";
    exit;
}







// sset defaults
$alert_message = '';
$notify_message = '';
$success_message = '';
$danger_message = '';

function show_messages($alert_message = '', $notify_message = '', $success_message = '', $danger_message = '') {
// remove the last <br>
    $banner = '';

    if (trim($alert_message)) {
        echo '<div class="row">
                        <div class="col-md-12">
							<div class="alert alert-info">
							<button type="button" class="close" data-dismiss="alert">&times;</button>' . $alert_message . '</div>
						</div>
					</div>';
    }

    if (trim($danger_message)) {
        echo '<div class="row">
                        <div class="col-md-12">
							<div class="alert alert-danger">
							<button type="button" class="close" data-dismiss="alert">&times;</button>' . $danger_message . '</div>
						</div>
					</div>';
    }


    if (trim($success_message)) {
        echo '<div class="row">
                        <div class="col-md-12">
							<div class="alert alert-success">
							<button type="button" class="close" data-dismiss="alert">&times;</button>' . $danger_message . '</div>
						</div>
					</div>';
    }


    if (trim($notify_message)) {
        $GLOBALS['bluebox'] = TRUE;
        echo '<div class="row">
                        <div class="col-md-12">
							<div class="alert alert-info">
							<button type="button" class="close" data-dismiss="alert">&times;</button>' . $notify_message . '</div>
						</div>
					</div>';
    }
}

// end function

function create_ul_list($text) {
    $list = '<ul class="list-style">';
    $words = explode('<br>', $text);
    foreach ($words as $item) {
        $item = trim($item);
        if ($item) {
            $list .= '<li>' . $item . '</li>';
        }
    }
    $list = $list . '</ul>';

    return $list;
}

function getnamestring($userID) {
    return getubbname($userID);
}

function getubbname($userID) {
    global $db;

    $sql_query = "select first_name   from ad_users where id='$userID' ";
//echo "$sql_query";
    $res = mysqli_query($db->db_connect_id, $sql_query);

    $records = mysqli_fetch_assoc($res);
    $rows = mysqli_num_rows($res);
    if ($rows > 0) {
        $name = $records['first_name'];
    }

    return $name;
}

function edititem($item, $usernumber, $competition = "") {
    if (!is_numeric($competition)) {
        echo"Access denied - incorrect data";
        exit;
    }
    if (!is_numeric($item)) {
        echo"Access denied - incorrect data";
        exit;
    }

//  Check if not blank
    $title = cleanthishere($_POST['name']);
    $media_description = cleanthishere($_POST['media_description']);

    $media_description = preg_replace("'<script[^>]*?>.*?</script>'si", "", $media_description);
    $media_description = str_replace("<", "&lt;", $media_description);
    $media_description = str_replace(">", "&gt;", $media_description);
    $media_description = cleanthishere($media_description);

    $title = preg_replace("'<script[^>]*?>.*?</script>'si", "", $title);
    $title = str_replace("<", "&lt;", $title);
    $title = str_replace(">", "&gt;", $title);
    $title = cleanthishere($title);

    $title = htmlspecialchars($title);
    $media_description = htmlspecialchars($media_description);



    if (stristr($title, '"') !== FALSE) {
        echo"Invalid characters use only 0-9 a-z ";
        exit;
    }

    if ($title !== "") {
//OK to update the name

        $sqlupdate = "update competition_media set title='" . cleanthishere($title) . "',description='" . cleanthishere($media_description) . "' WHERE ID='$item'";

        mysqli_query($db->db_connect_id, $sqlupdate) or die("Error updating the item name");
        if ($competition) {
            session_write_close();
            header('Location: https://www.agedadvisor.nz/view-all-entries/' . $competition . '-' . $item);
            exit;
        }

        session_write_close();
        header('Location: https://www.agedadvisor.nz//view-competitions');
        exit;
    }
}

function deleteitem($item, $usernumber, $competition = "") {
    if (!is_numeric($competition)) {
        echo"Access denied - incorrect data";
        exit;
    }
    if (!is_numeric($item)) {
        echo"Access denied - incorrect data";
        exit;
    }

    $GLOBALS['DB'] = dbconnecti();
    $sql = "delete from competition_media where ID=$item";
    mysqli_query($db->db_connect_id, $sql) or die("Error deleting media");
    if ($competition) {
        session_write_close();
        header('Location: https://www.agedadvisor.nz/view-all-entries/' . $competition);
        exit;
    }


    session_write_close();
    header('Location: https://www.agedadvisor.nz/view-competitions');
    exit;
}

// Lets find out what they want to do
if (ISSET($_REQUEST["func"])) {
    $function = $_REQUEST["func"];
} else {
    $function = '';
}
if (ISSET($_REQUEST["item"])) {
    $item = cleanthishere($_REQUEST["item"]);
} else {
    $item = '';
}
if (ISSET($_REQUEST["comp"])) {
    $competition = cleanthishere($_REQUEST["comp"]);
} else {
    $competition = '';
}

if ($function == "edit") {
    edititem($item, $usernumber, $competition);
}
if ($function == "delete") {
    deleteitem($item, $usernumber, $competition);
}
$alert_message .= $boolError;



$header_title = "Edit Entry";

$responsivetabs = FALSE;
$home_banner = FALSE;
$feature_slider = FALSE;
$feature_slider = FALSE;


$header_description = "";

// Bread crumbs and title
?>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12"><br>&nbsp;<br>
        <h3>Edit your entry</h3>
    </div>
</div>
<div class="row">
    <div class="col-md-12">


        <?php

        show_messages($alert_message,$notify_message)// Show any regular or important messages
        ?></div></div>
<div class="row">
    <div class="col-md-12">
        <form class="form-horizontal"  enctype="multipart/form-data" action="edit_media?func=edit&amp;item=<?php echo  $item ?>&amp;comp=<?php echo  $competition ?>" method="post" name="edit">

            <div class="form-group">
                <label class="col-sm-2 control-label">User</label>
                <div class="col-sm-10">
                    <p class="form-control-static"><?php echo  getubbname($usernumber) ?></p>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Facility</label>
                <div class="col-sm-10">
                    <input type="text" name="facility_name" value="<?php echo  $facility_name ?>">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Title of entry</label>
                <div class="col-sm-10">
                    <input type="text" name="name" value="<?php echo  $medianame ?>">
                </div>
            </div>  
            <div class="form-group">
                <label class="col-sm-2 control-label">Description</label>
                <div class="col-sm-10">
                    <textarea rows="6" name="media_description" ><?php echo  $media_description ?></textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">&nbsp;</label>
                <div class="col-sm-10">
                    <div class="text-center"><input style="width:300px;" class="btn btn-danger"  type="submit" value="Update"></div>
                </div>
            </div>
        </form>
    </div></div>

<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <label class="col-sm-2 control-label">&nbsp;</label>
            <div class="col-sm-10"><p class="text-center">or</p>
            </div></div>

        <form class="form-horizontal"  enctype="multipart/form-data" action="edit_media?func=delete&amp;item=<?php echo  $item ?>&amp;comp=<?php echo  $competition ?>" method="post" name="delete">


            <div class="form-group">
                <label class="col-sm-2 control-label">&nbsp;</label>
                <div class="col-sm-10">
                    <div class="text-center"><input style="width:300px;" class="btn btn-danger" type="submit" value="REMOVE from competition"></div>
                </div>
            </div>
        </form>




    </div></div>
<div class="row">    
    <div class="col-md-12">
<?php echo  $strThumb ?>
    </div
</div>







