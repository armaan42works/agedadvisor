<?php
require_once ('modules/fblogin/facebook-sdk-v5/autoload.php');

$fb = new Facebook\Facebook([
    'app_id' => '1409054779407381', // Replace {app-id} with your app id
    'app_secret' => 'e7dcb280e81129960a9a99d60b91c3f9',
    'default_graph_version' => 'v2.8'
        ]);

$helper = $fb->getRedirectLoginHelper();

$url = $_SERVER[REQUEST_URI];
$exploded = explode('/', $url);
$postfix = $exploded[2];

$permissions = ['email', 'user_location', 'public_profile']; // Optional permissions
$loginUrl = $helper->getLoginUrl('https://www.agedadvisor.nz/facebook-login/', $permissions);
$loginUrl = htmlspecialchars($loginUrl);
$_SESSION['back_url'] = 'newmedia-' . $postfix;

error_reporting(E_ALL);
ini_set('display_errors', '1');
global $db;

function dbconnecti() {
    global $db;
    return $db;
}

$title = '';
$media_description = '';
$name = '';
$filesize = 0;

// Set USERS, LANGUAGES, CURRENCIES, BASKET
$GLOBALS['goodurl'] = FALSE;
$additional_jquery = '';
$facility_name = '';


if (isset($_SESSION['userId']) && !empty($_SESSION['userId'])) {
    $userId = $_SESSION['userId'];
} else if (isset($_SESSION['csId']) && !empty($_SESSION['csId'])) {
    $userId = $_SESSION['csId'];
}


$usernumber = $userId;


if ($usernumber > 1) {
    $canenter = TRUE;
} else {
    $canenter = FALSE;
}






// default settings for security
$maxfilesize = 0;
$competition = 0;
$filetype = 0;
$allowededit = FALSE;
$allowedcreate = FALSE;

$boolError = '';


// sset defaults
$alert_message = '';
$notify_message = '';
$success_message = '';
$danger_message = '';

function show_messages($alert_message = '', $notify_message = '', $success_message = '', $danger_message = '') {
// remove the last <br>
    $banner = '';

    if (trim($alert_message)) {
        echo '<div class="row">
                        <div class="col-md-12">
							<div class="alert alert-info">
							<button type="button" class="close" data-dismiss="alert">&times;</button>' . $alert_message . '</div>
						</div>
					</div>';
    }

    if (trim($danger_message)) {
        echo '<div class="row">
                        <div class="col-md-12">
							<div class="alert alert-danger">
							<button type="button" class="close" data-dismiss="alert">&times;</button>' . $danger_message . '</div>
						</div>
					</div>';
    }


    if (trim($success_message)) {
        echo '<div class="row">
                        <div class="col-md-12">
							<div class="alert alert-success">
							<button type="button" class="close" data-dismiss="alert">&times;</button>' . $danger_message . '</div>
						</div>
					</div>';
    }


    if (trim($notify_message)) {
        $GLOBALS['bluebox'] = TRUE;
        echo '<div class="row">
                        <div class="col-md-12">
							<div class="alert alert-info">
							<button type="button" class="close" data-dismiss="alert">&times;</button>' . $notify_message . '</div>
						</div>
					</div>';
    }
}

// end function

function printdate($strdate) {
    echo date("d F Y", strtotime("$strdate"));
}

function showimagesbox($usernumber) {
    $insert = '';
    $competition = '';
    $filetype = 0;
    $focus = 0;
    $html = '';
    $strstatus = '';
//Show list of media files that can be downloaded/viewed
    $GLOBALS['DB'] = dbconnecti();
    $imbeddedimage = "";
    $output = "";
    $counting = 0;
    $strtable = 1;



    $sql = "SELECT ID,title,userID,competitionID,votes,views,file,dateadded,length,type,file_small,approved,file_medium FROM competition_media WHERE approved>0 AND userID='$usernumber' ORDER BY views ASC,ID desc";
    if ($strstatus == 3) {
        $sql = "SELECT ID,title,userID,competitionID,votes,views,file,dateadded,length,type,file_small,approved,file_medium FROM competition_media  WHERE approved>0 AND userID='$usernumber'  ORDER BY votes desc";
    }

//echo $sql;exit;

    $result = mysqli_query($db->db_connect_id, $sql) or die("Error finding entries");
    if ($result) {
        $c = 1;
        $html .= "<div class=\"row\">";
        if (mysqli_num_rows($result) > 0) {
            $html .= '<div class="col-md-12"><h3>Previous Photo Competition Entries</h3>Note: Photo submitted must not already have been in one of our previous competitions.</div></div><div class="row">';
            $mediatype = "";
            while ($myrow = mysqli_fetch_row($result)) {
                if ($strstatus == 3) {
                    $counting = $counting + 1;
                    $insert = "No. $counting<br>";
                }
                $approved = $myrow[11];
                $votes = $myrow[4];
                $competition = $myrow[3];
                $strImage = "https://www.agedadvisor.nz/images/competition/" . $myrow[10];
                $strImage_medium = "https://www.agedadvisor.nz/images/competition/" . $myrow[12];
                $ajax_id = 'https://www.agedadvisor.nz/modules/competition/view-all-entries-modal.php/' . $competition . '-' . $myrow[0];
                $normal_href = 'https://www.agedadvisor.nz/view-all-entries/' . $competition . '-' . $myrow[0];
                $strName = $myrow[1];

                $strThumb = "<img class=\"img-responsive img-thumbnail\" src =\"" . $strImage . "\" alt=\"" . $strName . "\" title=\"" . $strName . "\" src=\"\">";


//if ($strtable==1){echo "<div class=\"row\">";}
                if ($imbeddedimage == "" && $filetype == 0) {
                    $imbeddedimage = "Found";
                    $filetype = $myrow[0];
                }

                $highlight = '';
                if ($filetype == $myrow[0]) {
                    if ($focus) {
                        $highlight = ' bgcolor="#fefefe"';
                    }
                }
                $html .= '<div class="col-md-2 col-sm-4 col-xs-6"><div class="thumbnail" style="margin:10px auto;"><div class="text-center">';
                $html .= $insert;
                $html .= '<a class="fancybox" rel="lightbox" href="' . $normal_href . '" data-fancybox-href="' . $strImage_medium . '" data-ajax-id="' . $ajax_id . '" title="">';
                $html .= $strThumb;
                $html .= "</a>";
                if ($votes > 0 && $strstatus == '2') {
                    $html .= "<br>" . $votes . "&nbsp;Votes.";
                }
                if ($votes == 0 && $strstatus == '2') {
                    $html .= "<br>&nbsp;";
                }



                if (!$approved) {
                    $html .= "<br>Awaiting Approval";
                }
                $html .= '</div></div></div>';

                $strtable = $strtable + 1;
                $c = $c + 1;
                //echo"\n";
            }// end of while get colours
// Now tidy up the end table
            $html .= "</div>"; // end of row
        } else {
            $html .= '';
        }
        $html .= "</div>";
    }

    return $html;
}

// end function show all images in table	
















$competition = $childModule;


// Clean and test variables in Header for security
if (!is_numeric($competition) || $competition > 9999) {
    header("HTTP/1.0 404 Not Found");
    die;
}// Limited to 99 tools
// Show system variables
//echo " $ competition=".$competition." $ usernumber=".$usernumber."<br>";
if ($competition !== 0) {
    $GLOBALS['DB'] = dbconnecti();
    $sql = "SELECT * FROM competition where ID=$competition AND status IN (1,2)";
    $result = mysqli_query($db->db_connect_id, $sql) or die("Error getting competition details");
    if ($result) {
        $myrow = mysqli_fetch_row($result);
        $name = $myrow[1];
        $description = $myrow[3];
        $description = str_replace("\n", "<br>", $description);
        $description = str_replace("\r", "", $description);

        $moredescription = $myrow[11];
        $moredescription = str_replace("\n", "<br>", $moredescription);
        $moredescription = str_replace("\r", "", $moredescription);

        $entriesclose = $myrow[6];
        $enddate = $myrow[5];
        $mediaallowed = $myrow[7];
        // echo $mediaallowed;
        if ($mediaallowed == ',10') {
            $otherfiles = ",.jpeg,.png,.gif";
        }
        $rules = $myrow[9];
        $banner_advert = $myrow[13];
        $meta_tags = $myrow[15];
        $maximumentries = $myrow[10];
    }
}
if (!$name) {
    header("HTTP/1.0 404 Not Found");

    die;
}//
// find out how many times they havew entered this competition
$sql = "select distinct(ID) ";
$sql .= "FROM competition_media ";
$sql .= "WHERE competitionID=$competition AND userID=$usernumber ";
$GLOBALS['DB'] = dbconnecti();
$result = mysqli_query($db->db_connect_id, $sql);
$numresults = 0;
if ($result) {  //  if (mysqli_num_rows($result)>0){
    $numresults = mysqli_num_rows($result);
}
if ($numresults < $maximumentries && $usernumber > 1) {
// Yes they can still enter
    $canenter = TRUE;
} else if ($usernumber > 1) {
// They have reached their maximum entries
    $danger_message = "You have now entered the maximum times allowed";
    $canenter = FALSE;
}
//echo 'here';exit;
$mediaallowed = ".jpg";

$GLOBALS['DB'] = dbconnecti();

///echo "- $mediaallowed";exit;
//====================================================
//Function To shorten a string to X length
//====================================================
function shorten($str, $num = 75) {
    if (strlen($str) > $num)
        $str = substr($str, 0, $num) . "...";
    return $str;
}

function getubbname($userID) {
    global $db;

    $sql_query = "select first_name from ad_users where id='$userID' ";
//echo "$sql_query";
    $res = mysqli_query($db->db_connect_id, $sql_query);

    $records = mysqli_fetch_assoc($res);
    $rows = mysqli_num_rows($res);
    if ($rows > 0) {
        $name = $records['first_name'];
    }

    return $name;
}

$strAction = '';
//Form Being Submitted
//====================================================
if (ISSET($_POST['function'])) {
    $strAction = $_POST['function'];
}
//====================================================
//Updating the Lesson
//====================================================
if (isset($strAction) && $strAction == "upload") {
//echo '$strAction='."$strAction";exit;
    $title = ($_POST['title']);
    $product_id = ($_POST['restcareid']);
    $facility_name = ($_POST['cityLable']);
    $media_description = ($_POST['media_description']);
// --------------------------------------
// Let's see if we want this type of file


    if (($_FILES['userfile']['name'] != "none") && ($_FILES['userfile']['name'])) {
//**************************************************************************IMAGE START
// Let's see if we want this type of file
        $Error = 0;

        if ($meta_tags && ($_FILES['userfile']['name'] != "none") && ($_FILES['userfile']['name'])) {
//      echo "checking";exit;
            if (preg_match("/\.(php|php3|php4|cgi|pl|exe|bat|reg|jpeg|png|gif)/i", $_FILES['userfile']['name'])) {
                $boolError .= "Sorry that type of file is not allowed. Must be .jpg<br>";
            }

// Now check with accepted file types

            $checkfile = str_replace(",", "|", $mediaallowed);
            if (!preg_match("/($checkfile)$/i", $_FILES['userfile']['name'])) {
                $boolError .= "Sorry that type of file is not allowed. Must be .jpg<br>";
                $Error = 1;
            }
// Now check size of file
            $filesize = $_FILES['userfile']['size'];
            if (( $filesize > 30000000)) {
                $boolError .= "Sorry that type of file is too big<br>";
                $Error = 1;
            }
        } else {
            $Error = 1;
        }



        if (!$Error && $filesize && $meta_tags) {// Yes we can upload this into the site
            $saveas = str_replace(" ", "-", $facility_name . ' ' . $name . '.jpg');
            $saveas = preg_replace("/[^A-Za-z0-9. ]/", '', $saveas);
            $saveas = str_replace(",", "-", $saveas);
            $saveas = str_replace("+", "-", $saveas);
            $saveas = str_replace("_", "-", $saveas);
            $saveas = str_replace("'", "", $saveas);
            $saveas = str_replace("(", "", $saveas);
            $saveas = str_replace(")", "", $saveas);
            $saveas = str_replace('"', "", $saveas);
            $saveas = str_replace("'", "", $saveas);
            $saveas = str_replace(";", "", $saveas);
            $saveas = str_replace(":", "", $saveas);
            $saveas = str_replace("[", "", $saveas);
            $saveas = str_replace("]", "", $saveas);
            $saveas = str_replace("{", "", $saveas);
            $saveas = str_replace("}", "", $saveas);
            $saveas = str_replace("%", "", $saveas);
            $saveas = str_replace("|", "", $saveas);
            $saveas = str_replace('$', "", $saveas);

            $saveas = str_ireplace(".jpg", "", $saveas);


//Now check if filename already exists before saving
/// If it does not exist create it
            // Now save the file
            $FileName = "$saveas";


            $FileFullPath = $_SERVER['DOCUMENT_ROOT'] . "/images/competition/" . $FileName . ".jpg";
            $FileFullPath = $_SERVER['DOCUMENT_ROOT'] . "/images/competition/" . $FileName . '' . "_medium.jpg";
            $FileFullPath_medium = $_SERVER['DOCUMENT_ROOT'] . "/images/competition/" . $FileName . '' . "_medium.jpg";
            $FileFullPath_thumb = $_SERVER['DOCUMENT_ROOT'] . "/images/competition/" . $FileName . '' . "_thumb.jpg";
            //  echo $FileFullPath;exit;     
            //***********Find them a valid filename by adding a number to the end of the chosen file
            $found = FALSE;
            //$count = "";
            $count = "";
            while (!$found) {
                if (is_file($_SERVER['DOCUMENT_ROOT'] . "/images/competition/" . $FileName . '' . $count . "_medium.jpg")) {
                    // Nope someone else using this uname
                    $found = FALSE;
                    $count = $count + 1;
                } else {
                    $found = TRUE;
                    $FileFullPath = $_SERVER['DOCUMENT_ROOT'] . "/images/competition/" . $FileName . '' . $count . "_medium.jpg"; // this is the same as below file so only stores one medium version and the thumb below
                    $FileFullPath_medium = $_SERVER['DOCUMENT_ROOT'] . "/images/competition/" . $FileName . '' . $count . "_medium.jpg";
                    $FileFullPath_thumb = $_SERVER['DOCUMENT_ROOT'] . "/images/competition/" . $FileName . '' . $count . "_thumb.jpg";
                }
                //***********Found them a valid file name
            } // end while


            $file_tmp = $_FILES['userfile']["tmp_name"];
            move_uploaded_file($file_tmp, $FileFullPath);







//if($boolError){echo $boolError;exit;}

            $media_description = preg_replace("'<script[^>]*?>.*?</script>'si", "", $media_description);
            $media_description = str_replace("<", "&lt;", $media_description);
            $media_description = str_replace(">", "&gt;", $media_description);
            $media_description = cleanthishere($media_description);

            $title = preg_replace("'<script[^>]*?>.*?</script>'si", "", $title);
            $title = str_replace("<", "&lt;", $title);
            $title = str_replace(">", "&gt;", $title);
            $title = cleanthishere($title);


            if (stristr($title, '"') !== FALSE) {
                $boolError .= "Invalid characters use only 0-9 and a-z<br>";
            }

            if (strlen($title) > 255) {
                $boolError .= "Title is too long. Maximum 255 characters<br>";
            }
            if (strlen($title) < 1) {
                $boolError .= "Title is missing<br>";
            }
            if (strlen($media_description) < 1) {
                $boolError .= "Description is missing<br>";
            }
            if (!$product_id) {
                $boolError .= "Please enter facility name<br>";
            }


            if (!$boolError) {// Yes we can upload this into the site
                $date = date("Y-m-d", time());
                ;
                $title = htmlspecialchars($title);
                $media_description = htmlspecialchars($media_description);



                $raw_filename = $FileFullPath;

                $ext = "jpg";
                $target_path = $FileFullPath;
                $target_path_thumb = $FileFullPath_thumb;
                $target_path_medium = $FileFullPath_medium;
//echo "$FileName   <hr>here";exit;
///RESIZE CODE

                list($save_strwidth, $save_strheight) = getimagesize($raw_filename);
//echo "here is size of raw image $save_strwidth, $save_strheight";
// Make the thumb Image size for zoom
                $strwidth = 200;
                $strheight = 200;

//echo "Doing resize $target_path, $target_path_thumb, $strwidth, $strheight, $ext";
                ak_img_thumb($target_path, $target_path_thumb, $strwidth, $strheight, $ext);
//echo "end resize ";exit;
// Make the medium Image size for zoom

                $maxsquareheight = 800;
                $maxsquarewidth = 800;
                $strwidth = $save_strwidth;
                $strheight = $save_strheight;
                if ($strheight > $maxsquareheight) {
                    $strfactor = $maxsquareheight / $strheight;
                    $strheight = round($strheight * $strfactor);
                    $strwidth = round($strwidth * $strfactor);
                }

                if ($strwidth > $maxsquarewidth) {
                    $strfactor = $maxsquarewidth / $strwidth;
                    $strheight = round($strheight * $strfactor);
                    $strwidth = round($strwidth * $strfactor);
                }


                ak_img_resize($target_path, $target_path_medium, $strwidth, $strheight, $ext);




                $type = 10;


// Raw image
                $Filename_q = addslashes(str_ireplace($_SERVER['DOCUMENT_ROOT'] . "/images/competition/", "", $FileFullPath));
// medium         
                $file_medium = addslashes(str_ireplace($_SERVER['DOCUMENT_ROOT'] . "/images/competition/", "", $FileFullPath_medium));
// thumb        
                $file_small = addslashes(str_ireplace($_SERVER['DOCUMENT_ROOT'] . "/images/competition/", "", $FileFullPath_thumb));


                $GLOBALS['DB'] = dbconnecti();
                $sql = "insert into competition_media (competitionID,userID,title,description,type,file,length,dateadded,file_medium,file_small,productID) values ('$competition','$usernumber','" . cleanthishere($title) . "','" . cleanthishere($media_description) . "','$type','$Filename_q','$filesize','$date','$file_medium','$file_small', '$product_id')";

//echo $sql;exit;

                mysqli_query($db->db_connect_id, $sql) or die("$sql Error adding to the media list");

//************************************************
//Email a copy of the entry
//************************************************
                $strMessage = "";
                //====================================================
                $strMessage = "User: " . getubbname($usernumber) . "\n\n";
                $GLOBALS['DB'] = dbconnecti();
                $sql = "select ID from competition_media WHERE file='$Filename_q'";
                $result = mysqli_query($db->db_connect_id, $sql) or die("Error getting users name");
                $myrow = mysqli_fetch_row($result);
                $ID = $myrow[0];


                $strMessage .= "\nHas uploaded into competition the following entry\n";
                $strMessage .= "Link: https://www.agedadvisor.nz/Malcolm/CompetitionAdmin/view_all_entries.php/" . $competition . "_" . $ID . "\n";


// HOMEOFPOI	SendEmail($strSenderName,$strSenderEmail,$strToName,$strToEmail,$strSubject,$strMessage);

                sendmail('nigel@agedadvisor.co.nz', 'New Entry to Photo Competition', $strMessage, '');
//SendEmail('Competition','server@homeofpoi.com','Malcolm','office@homeofpoi.com','Entry into competition',$strMessage);
//SendEmail('Competition','server@homeofpoi.com','Chris','chris@homeofpoi.com','Entry into competition',$strMessage);


                session_write_close();
                header('Location: https://www.agedadvisor.nz/view-all-entries/' . $competition . '-' . $ID);
                exit;
            }
        }

//====================================================
//End of Uploading the lesson
//====================================================
    }
}
?>
<script type="text/javascript">
    $(function () {
        $('#cityLable').focus(function () {
            $(this).val('');
            $("#city,#cityid,#suburbid,#restcareid,#supplierId").val('');
        });

        $('#cityLable').focusout(function () {
            //restcareid
            var restcare_id = $("#restcareid").val();
            if (restcare_id) {
            } else
            {
                $(this).val('');
                $("#city,#cityid,#suburbid,#restcareid,#supplierId").val('');
            }

        });

        var offset = $('#cityLable').offset();
        var autoL = offset.left;
        var autoT = parseInt(offset.top) + parseInt(47);
        autoL = autoL + 'px';
        autoT = autoT + 'px';

        var path = '<?php echo HOME_PATH; ?>';

        $("#cityLable").autocomplete({
            source: function (request, response) {
                $.ajax({
                    minLength: 3,
                    method: "post",
                    url: path + 'ajaxFront.php/' + new Date().getTime(),
                    data: {
                        'action': 'getSupplierProList', 'maxRows': 12,
                        'name': request.term
                    },
                    success: function (result) {
                        var data = $.parseJSON(result);
                        if (data === '') {
                            return {
                                label: '',
                                value: '',
                                // title = location,restcare=supplier product                                                       value: item.title + ', '+item.suburb+ ', '+item.restCare,
                                cityId: '',
                                suburbId: '',
                                restCareId: ''
                            };
                        }
                        response($.map(data, function (item) {
                            var restCare = (item.restCare === null) ? '' : item.restCare;
                            var suburb = (item.suburb === null) ? '' : ', ' + item.suburb;
                            var location = (item.cty === null) ? '' : ', ' + item.cty;
                            var servType = '';
                            if (item.servType == "Aged Care") {
                                servType = '[AC]';
                            } else if (item.servType == "Retirement Village") {
                                servType = '[RV]';
                            } else {
                                servType = '[HS]';
                            }
                            return {
                                label: restCare + '' + suburb + '' + location + '' + servType,
                                value: restCare + '' + suburb + '' + location + '' + servType,
                                // title = location,restcare=supplier product                                                       value: item.title + ', '+item.suburb+ ', '+item.restCare,
                                cityId: item.id,
                                suburbId: item.suburbId,
                                restCareId: item.restCareId,
                                supplierId: item.supplierId
                            };
                        }));
                    },
                    error: function (e) {
                        alert('Error');
                    }
                });
            },
            select: function (event, ui) {
                $this = $(this);
                setTimeout(function () {
                    $('#cityid').val(ui.item.cityId);
                    $('#suburbid').val(ui.item.suburbId);
                    $('#restcareid').val(ui.item.restCareId);
                    $('#supplierId').val(ui.item.supplierId);
                    $('#cityLable').val(ui.item.label);
                    $('#facilityType').focus();
                    $this.blur();
                }, 1);
            },
            open: function (event, ui) {
                $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
                $('ul.ui-widget-content').css({"z-index": "9999", "display": "block", "top": autoT, "left": autoL});
            },
            close: function () {
                $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
            }
        });
    });
</script>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12"><br>&nbsp;<br>
        <h3>Enter Photo into this competition</h3>
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">


<?php
if (!$usernumber) {
    echo '<div class="alert alert-info">Anyone can vote. However only members can enter competitions below this line. Members must <a href="/login">login</a> first. You can <a href="/register">become a member</a> for free or <br><a href="https://www.agedadvisor.nz/login-twitter.php"><img class="btn" src="/images/sign_in_with_twitter.png" alt="twitter Login"/></a> &nbsp; &nbsp; &nbsp;<a href="' . $loginUrl . '"><img class="btn" src="/images/sign_in_with_facebook.png" alt="Facebook Login"/></a></div>';
}
?></div></div><?php
            $alert_message .= $boolError;

            $header_title = "$name";

            $responsivetabs = FALSE;
            $home_banner = FALSE;
            $feature_slider = FALSE;
            $feature_slider = FALSE;
?>
<div class="row">
    <div class="col-md-12">
        <?php
        show_messages($alert_message, $notify_message, $success_message, $danger_message)// Show any regular or important messages
        ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">

        <div class="text-center">
        <?php if ($banner_advert) {
            echo "<img class=\"img-responsive\" src=\"$banner_advert\" alt=\"Upload Entry\" >";
        } ?></div>
        <h3 id="what">What can you upload?</h3>

        <p><strong><?php echo  $name ?></strong><br><?php echo  $description ?><br><br><strong>Voting starts:</strong> <?php echo  printdate($entriesclose) ?><br><strong>Late entries:</strong> are accepted.<br><strong>Voting stops:</strong> <?php echo  printdate($enddate) ?></p>
<?php
//  Add rules in here if there are any
if ($rules) {
    $rules = str_replace("\n\r\n", "</li></ul><ul><li>", $rules);
    $rules = str_replace("\n", "</li><li>", $rules);
    $rules = str_replace("\r", "", $rules);
    echo "<h3 id=\"rules\">Rules:</h3>";
    echo "<ul><li>" . $rules . "</li>";
    echo "</ul>";
}

//if($moredescription){echo '<h3 id="info">Additional Information:</h3><p>'.$moredescription.'</p>';}


if ($canenter) { // Yes they can enter
    echo showimagesbox($usernumber);
    ?>

            <hr>
            <h3 id="upload">Upload Entry</h3>
            <form class="form-horizontal" enctype="multipart/form-data" action="https://www.agedadvisor.nz/new-media/<?php echo  $competition ?>" method="post" name="add" id="form_upload">
                <input type="hidden" name="function" value="upload">

                <div class="form-group">
                    <label class="col-sm-2 control-label">User</label>
                    <div class="col-sm-10">
                        <p class="form-control-static"><?php echo  getubbname($usernumber) ?></p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Facility</label>
                    <div class="col-sm-10">
                      <!--<input type="text" name="facility_name" value="<?php echo  $facility_name ?>">-->
                        <div class="form-groups">
                            <input type="hidden" name="cityid" id="cityid">
                            <input type="hidden" name="suburbid" id="suburbid">
                            <input type="hidden" name="restcareid" id="restcareid"/>
                            <input type="hidden" name="supplierId" id="supplierId"/>
                            <div class="error"></div>                        
                            <input style="border-radius: 0px !important;" type="text" class="input-lg ui-autocomplete-input ui-widget required" required autocomplete="off" name="cityLable" id="cityLable" onfocus="this.value = '';"  placeholder="Start typing facility name here and then select from displayed list">           
                        </div>          
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Title of entry</label>
                    <div class="col-sm-10">
                        <input type="text" name="title" value="<?php echo  $title ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label"><?if($moredescription){echo $moredescription;}else{echo "Description";}?></label>
                    <div class="col-sm-10">
                        <textarea rows="6" name="media_description"><?php echo  $media_description ?></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Files allowed</label>
                    <div class="col-sm-10">
                        <p class="form-control-static"><?php echo  $mediaallowed ?></p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">File to upload *max 20Mb</label>
                    <div class="col-sm-10">
                        <input type="file" name="userfile" accept="*" class="formboxes" size="40"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">&nbsp;</label>
                    <div class="col-sm-10">
                        <div id="uploadingpic" class="hide"><b>Please wait uploading <img src="/images/interstitial-animated-dots.gif" alt=""></b></div><div class="text-center"><input style="width:300px;" class="btn btn-danger"  id="button_upload" type="submit" value="Upload Entry"></div>
                    </div>
                </div>
            </form>
            <?php }?>




        </div>
    </div>




    <?php
    $additional_jquery .= '$("#button_upload").click(function(o){o.preventDefault(),$("#button_upload").hide(),$("#uploadingpic").show(),$("#form_upload").submit()});';
    ?><script type="text/javascript"><?php echo  $additional_jquery ?></script>


