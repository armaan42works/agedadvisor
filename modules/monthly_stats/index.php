<?php
$prev_month = date_create("last day of -1 month")->format('m');
(int) $prev_month;
$curr_year = (new DateTime)->format("Y");
(int) $curr_year;

$sp = $_GET['sp'];

function ordinal($number) {
    $ends = array('th','st','nd','rd','th','th','th','th','th','th');
    if ((($number % 100) >= 11) && (($number % 100) <= 13)){
        return $number. 'th';
    }else{
        return $number. $ends[$number % 10];
    }
}

$dateObj = DateTime::createFromFormat('!m', $prev_month);
$monthName = $dateObj->format('F');

$nonupgraded_fac_count = "SELECT COUNT(pro.id) AS non_upgraded_count FROM ad_products AS pro LEFT JOIN ad_pro_plans AS proplans ON pro.id = proplans.pro_id WHERE proplans.plan_id < 2 AND proplans.current_plan = 1 AND pro.deleted = 0 GROUP BY pro.id";
$nonupfac_count_res = $db->sql_query($nonupgraded_fac_count);
$non_count_data = $db->sql_fetchrowset($nonupfac_count_res);
$non_upgrade_count = $db->sql_numrows($nonupfac_count_res);


$upgraded_fac_count = "SELECT COUNT(pro.id) AS upgraded_count FROM ad_products AS pro LEFT JOIN ad_pro_plans AS proplans ON pro.id = proplans.pro_id WHERE proplans.plan_id > 1 AND proplans.current_plan = 1 AND pro.deleted = 0 GROUP BY pro.id";
$upfac_count_res = $db->sql_query($upgraded_fac_count);
$count_data = $db->sql_fetchrowset($upfac_count_res);
$upgrade_count = $db->sql_numrows($upfac_count_res);

$upgraded_fac_sum = "SELECT SUM(rank_rating_nation) AS upgraded_rank_sum FROM ad_products AS pro LEFT JOIN ad_pro_plans AS proplans ON pro.id = proplans.pro_id WHERE proplans.plan_id > 1 AND proplans.current_plan = 1 AND pro.deleted = 0";
$upfac_sum_res = $db->sql_query($upgraded_fac_sum);
$upfac_sum_data = $db->sql_fetchrowset($upfac_sum_res);

foreach ($upfac_sum_data as $key => $record) {
    $upfac_rank_sum = $record['upgraded_rank_sum'];
}

$nonup_fac_sum = "SELECT SUM(rank_rating_nation) AS nonup_rank_sum FROM ad_products AS pro LEFT JOIN ad_pro_plans AS proplans ON pro.id = proplans.pro_id WHERE proplans.plan_id < 2 AND proplans.current_plan = 1 AND pro.deleted = 0";
$nonupfac_sum_res = $db->sql_query($nonup_fac_sum);
$nonupfac_sum_data = $db->sql_fetchrowset($nonupfac_sum_res);

foreach ($nonupfac_sum_data as $key => $record) {
    $nonupfac_rank_sum = $record['nonup_rank_sum'];
}

$nonupavgrank = round($nonupfac_rank_sum / $non_upgrade_count);
$upgradeavgrank = round($upfac_rank_sum / $upgrade_count);


$upfac_enq_sum = "SELECT SUM(total_no_of_enq) AS upfac_enq_sum FROM ad_data_history AS dh LEFT JOIN ad_pro_plans AS proplans ON dh.id = proplans.pro_id WHERE proplans.plan_id > 1 AND proplans.current_plan = 1 AND dh.month = $prev_month";
$upfac_enq_res = $db->sql_query($upfac_enq_sum);
$upfac_enq_data = $db->sql_fetchrowset($upfac_enq_res);

foreach ($upfac_enq_data as $key => $record) {
    $upfac_enq = $record['upfac_enq_sum'];
}

$upgraded_view_sum = "SELECT SUM(total_no_of_views) AS upfac_view_sum FROM ad_data_history AS dh LEFT JOIN ad_pro_plans AS proplans ON dh.id = proplans.pro_id WHERE proplans.plan_id > 1 AND proplans.current_plan = 1 AND dh.month = $prev_month";
$upfac_view_res = $db->sql_query($upgraded_view_sum);
$upfac_view_data = $db->sql_fetchrowset($upfac_view_res);

foreach ($upfac_view_data as $key => $record) {
    $upfac_view = $record['upfac_view_sum'];
}

$upgradeavgview = round($upfac_view / $upgrade_count);
$upgradeavgenq = round($upfac_enq / $upgrade_count);

function uReviews($id) {
    global $db;
    $query = " SELECT fdbk.* , pds.title  FROM " . _prefix("feedbacks") . " AS fdbk   "
            . " LEFT JOIN " . _prefix("products") . " AS pds ON  fdbk.product_id= pds.id "
            . " WHERE fdbk.product_id =" . $id . " AND fdbk.status = 1 AND fdbk.deleted = 0 ORDER BY fdbk.created DESC";
    $res = $db->sql_query($query);
    $count = $db->sql_numrows($res);
    return $count;
}

function uRating($id) {
    $query = " SELECT fdbk.* , pds.title  FROM ad_feedbacks AS fdbk LEFT JOIN ad_products AS pds ON  fdbk.product_id= pds.id WHERE fdbk.product_id =" . $id . " AND fdbk.deleted = 0 ORDER BY fdbk.created DESC";
    $res = mysqli_query($db->db_connect_id, $query);
    $TotalRatingOverAll = 0;
    $divide_by = 0;
    while ($data = mysqli_fetch_array($res)) {
        if ($data['overall_rating'] > 0) {
            $divide_by = $divide_by + 1;
            $TotalRatingOverAll = ($TotalRatingOverAll + $data['overall_rating']);
        }
    }
    if ($divide_by > 0) {
        $RatingOverAll = ($TotalRatingOverAll / $divide_by);
    }

    return round($RatingOverAll * 20, 1);
}

$count_qry = "SELECT COUNT(pro.id) as fac_count, pro.id, usr.email, usr.last_name, usr.first_name, pro.supplier_id FROM ad_products as pro
        LEFT JOIN ad_users AS usr ON usr.id = pro.supplier_id WHERE pro.deleted = 0 AND md5(pro.supplier_id)='$sp' GROUP BY supplier_id";
$qryres = mysqli_query($db->db_connect_id, $count_qry);
while ($data = mysqli_fetch_array($qryres)) {

    $sup_id = $data['supplier_id'];
    $fac_email = $data['email'];
    $fac_count = $data['fac_count'];

    $fac_name = $data['first_name'] . ' ' . $data['last_name'];
    if ($fac_count > 0) {
        $prod_qry = "SELECT id FROM ad_products WHERE deleted = 0 and supplier_id = $sup_id";
        $prodres = mysqli_query($db->db_connect_id, $prod_qry);        
        $output = "<table>
            <tr>
                <td style='font-weight:bold;font-size:120%;border:1px solid;background-color:yellow;'>Facility Name</td>
                <td style='font-weight:bold;font-size:120%;border:1px solid;background-color:yellow;'>Rating</td>
                <td style='font-weight:bold;font-size:120%;border:1px solid;background-color:yellow;'>Reviews</td>
                <td style='font-weight:bold;font-size:120%;border:1px solid;background-color:yellow;'>Views</td>                
                <td style='font-weight:bold;font-size:120%;border:1px solid;background-color:yellow;'>Enquiries made</td>
                <td style='font-weight:bold;font-size:120%;border:1px solid;background-color:yellow;'>Responded to</td>
                <td style='font-weight:bold;font-size:120%;border:1px solid;background-color:yellow;'>Rating in(local)</td>
                <td style='font-weight:bold;font-size:120%;border:1px solid;background-color:yellow;'>Rating in NZ</td>
                <td style='font-weight:bold;font-size:120%;border:1px solid;background-color:yellow;'>Page Views in(local)</td>
                <td style='font-weight:bold;font-size:120%;border:1px solid;background-color:yellow;'>Page Views in NZ</td>
                <td style='font-weight:bold;font-size:120%;border:1px solid;background-color:yellow;'>Reviews in(local)</td>
                <td style='font-weight:bold;font-size:120%;border:1px solid;background-color:yellow;'>Reviews in NZ</td>      
            <tr>";
        
        while ($pData = mysqli_fetch_array($prodres)) {
            $prod_id = $pData['id'];
            $query_ranking = "SELECT propla.plan_id,prd.id, prd.title, prd.supplier_id, prd.address_city, rank_rating_city, rank_rating_nation, rank_page_view_city, rank_page_view_nation, rank_reviews_city, rank_reviews_nation, "
                    . "(SELECT SUM(visited_before) FROM ad_analytics aa WHERE aa.prod_id = prd.id AND YEAR(date) = $curr_year AND MONTH(date) = $prev_month GROUP BY aa.prod_id) AS no_of_views, "
                    . "(SELECT COUNT(aenq.id) FROM ad_enquiries aenq WHERE aenq.prod_id = prd.id AND YEAR(time_of_enquiry) = $curr_year AND MONTH(time_of_enquiry) = $prev_month) AS no_of_enq, "
                    . "(SELECT COUNT(aenq.facility_responded) FROM ad_enquiries aenq WHERE aenq.prod_id = prd.id AND YEAR(time_of_enquiry) = $curr_year AND MONTH(time_of_enquiry) = $prev_month AND facility_responded = 'Y') AS no_of_responses "
                    . " FROM " . _prefix("products") . " AS prd "
                    . " LEFT JOIN ad_pro_plans as propla ON propla.pro_id = prd.id "
                    . " WHERE prd.id = $prod_id AND current_plan = 1";           
            
            $ranking_res = $db->sql_query($query_ranking);
            $count = $db->sql_numrows($ranking_res);
            $data = $db->sql_fetchrowset($ranking_res);

            foreach ($data as $key => $record) {                               
                $city = $record['address_city'];
                $id = $record['id'];
                $per = "%";
                $fOverall_rating1 = uRating($id);
                if($fOverall_rating1>0){
                $fOverall_rating = $fOverall_rating1 . $per;
                }else{
                    $fOverall_rating="Not Ranked Yet";
                }
                
                $reviewall = uReviews($id);
                
                $rank_page_view_city = (($record['rank_page_view_city'] === NULL) || ($record['rank_page_view_city'] < 1)) ? 'Not Ranked for '.$monthName.'' : ordinal($record['rank_page_view_city']);

                $rank_page_view_nation = (($record['rank_page_view_nation'] === NULL) || ($record['rank_page_view_nation'] < 1)) ? 'Not Ranked for '.$monthName.'' : ordinal($record['rank_page_view_nation']);

                $rank_reviews_city = (($record['rank_reviews_city'] === NULL) || ($record['rank_reviews_city'] < 1)) ? 'Not Ranked for '.$monthName.' in '.ucfirst(strtolower($city)).'' : ordinal($record['rank_reviews_city']);
                
                $rank_reviews_nation = (($record['rank_reviews_nation'] === NULL) || ($record['rank_reviews_nation'] < 1)) ? 'Not Ranked for '.$monthName.'' : ordinal($record['rank_reviews_nation']);
                
                $rank_rating_city = (($record['rank_rating_city'] === NULL) || ($record['rank_rating_city'] < 1)) ? 'Not Ranked for '.$monthName.' in '.ucfirst(strtolower($city)).'' : ordinal($record['rank_rating_city']);
                
                $rank_rating_nation = (($record['rank_rating_nation'] === NULL) || ($record['rank_rating_nation'] < 1)) ? 'Not Ranked for '.$monthName.'' : ordinal($record['rank_rating_nation']);
                
                $output .= '
                    <td style="width:325px; border:1px solid;">' . $record['title'] . '</td>
                    <td style="border:1px solid;">' . $fOverall_rating . '</td>
                    <td style="border:1px solid;">' . $reviewall . '</td>
                    <td style="border:1px solid;">' . $record['no_of_views'] . '</td>                        
                    <td style="border:1px solid;">' . $record['no_of_enq'] . '</td>
                    <td style="border:1px solid;">' . $record['no_of_responses'] . '</td>
                    <td style="width:300px; border:1px solid;">' . $rank_rating_city . '</td>
                    <td style="border:1px solid;">' . $rank_rating_nation . '</td>
                    <td style="border:1px solid;">' . $rank_page_view_city . '</td>
                    <td style="border:1px solid;">' . $rank_page_view_nation . '</td>
                    <td style="width:300px; border:1px solid;">' . $rank_reviews_city  . '</td>
                    <td style="border:1px solid;">' . $rank_reviews_nation . '</td>
                    <td></td>';
            }
            $output .= "</tr>";            
        }
        $output .= "</table>";
    }
    ob_end_clean();
    ob_clean();
    header("Content-disposition: attachment; filename=Monthly Statistics - ".$monthName.".xls");
    header("Content-type: excel/ms-excel; name=Monthly Statistics - ".$monthName.".xls");    
    echo $output;    
    exit;
}