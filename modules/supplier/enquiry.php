
<script type="text/javascript">
    $(document).ready(function() {
        var page = 0;
        var li = '0_no';
        var data = '&id=54';
        changePagination(page, li, 'enquiry', data);
    });
</script>
<style>
    .btn-dangers:hover, .btn-dangers:focus, .btn-dangers:active, .btn-dangers.active {
        color: #fff;
        background-color: #333 !important;
    }
    .divider {
        text-align: center;
        margin-bottom: 30px;
    }
    .divider hr {
        margin-left: auto;
        margin-right: auto;
        width: 25%;
    }
    .right {
        float: right;
    }
    .left {
        float: left;
    }
    hr {
        margin: 15px 0;
    }
    .provider-compair-headings {
        position: relative;
        border-bottom: 1px solid #ddd;
        padding: 8px 15px;
        box-shadow: 0 -3px 3px rgba(0,0,0,.2);
    }
    .images {
       display: inline;
        margin: 5px;
        padding: 10px;
        vertical-align: middle;
        width: 185px;
    }
    .enquiries-box{
            position:relative;
            padding:15px 0;
    }
    .enquiries-box p{
            margin:0 5px 15px;
    }
</style>
<div class="row">
    <div class="dashboard_container">
    <?php require_once 'includes/sp_left_navigation.php';?>
        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
            <div class="dashboard_right_col">                
                <h2 class="hedding_h2"><i class="fa fa-pencil-square-o"></i> <span>Enquiries</span> </h2>
                <h4 class="logo_bottom_color font_size_16">Facility Name: <?php echo $_SESSION['proName']; ?></h4>
                
                                
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pdding_none scroll" id="pageData" ></div>
            </div>
        </div>
    </div>
</div>