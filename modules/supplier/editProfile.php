<?php
//ini_set('error_reporting', E_ALL);
ini_set("display_errors", 0);

// Make sure all have appropriate QUICK URL

function pre_check_before_update($table, $product_missing, $productWhere) {
    global $db;

    $return = FALSE;
    $sql_queryL = "SELECT * FROM " . $table . " $productWhere ";
    //echo $sql_queryL;
    $resL = $db->sql_query($sql_queryL);
    while ($missing = $db->sql_fetchrow($resL)) {
        $return = TRUE;
    }

    return $return;
}

function does_quick_url_exist($name = '', $id = '') {
    global $db;

    $return = FALSE;
    $sql_queryL = "SELECT id,quick_url FROM " . _prefix("products") . " WHERE quick_url = '$name'  AND id != $id ";
    //echo $sql_queryL;
    $resL = $db->sql_query($sql_queryL);
    while ($missing = $db->sql_fetchrow($resL)) {
        $return = TRUE;
    }

    return $return;
}

function make_url_clean($url) {
    $url = trim(str_replace(',', " ", $url));
    $url = trim(str_replace('/', " ", $url));

    $url = trim(str_replace("&reg;", "", $url));
    $url = trim(str_replace("&amp;", " ", $url));

    $url = preg_replace("/\<.*?\>/", "", $url);
    $url = preg_replace('/[^a-z 0-9*]/i', '', $url);
    $url = trim(str_replace("  ", " ", $url));
    $url = trim(str_replace("  ", " ", $url));
    $url = trim(str_replace("  ", " ", $url));
    // Finished clean of funny characters
    $url = str_replace(" ", "-", $url);
    //echo "$url<br>";
    return $url;
}

function find_missing_quick_url($thisone = '') {
    global $db;

    if ($thisone == '') {
        $sql_queryL = "SELECT id,title,certification_service_type,address_suburb,address_city FROM " . _prefix("products") . " WHERE quick_url = '' ";
    } else {
        $sql_queryL = "SELECT id,title,certification_service_type,address_suburb,address_city FROM " . _prefix("products") . " WHERE id = '$thisone' ";
    }
//    $sql_queryL = "SELECT id,title,certification_service_type,address_suburb,address_city FROM " . _prefix("products");
    $resL = $db->sql_query($sql_queryL);
    while ($missing = $db->sql_fetchrow($resL)) {

        $id = $missing['id'];
        $title = make_url_clean(ucwords(strtolower($missing['title'])));
        $address_suburb = make_url_clean($missing['address_suburb']);
        $address_city = make_url_clean($missing['address_city']);
        $certification_service_type = make_url_clean(strtolower($missing['certification_service_type']));
        $url = 'search/' . $certification_service_type . '/' . $title . '-' . $address_suburb . '-' . $address_city;

        //***********Find them a valid quickurl or add number to the end of the chosen name
        $found = FALSE;
        $count = "";
        while (!$found) {
            if (does_quick_url_exist($url . $count, $id)) {
                // Nope someone else using this uname
                $found = FALSE;
                $count = $count + 1;
            } else {
                $found = TRUE;
                $url = "$url$count";
            }
            //***********Found them a valid quick_url
        } // end while
        //echo "$url<br>";
        //echo "$url<br>";

        $product_missing = array(
            'quick_url' => "$url"
        );
        $productWhere = "where id= '$id'";
        if (pre_check_before_update(_prefix('products'), $product_missing, $productWhere)) {
            $product_update = $db->update(_prefix('products'), $product_missing, $productWhere);
        } else {
            sendmail('Nigel@agedadvisor.co.nz', 'Major Error', 'NO AUTO REPAIR On page /modules/editAccountInfo.php Products entry is missing. Notify Web Admin: accounts@agedadvisor.co.nz', '');

            echo "Products entry is missing. Notify Web Admin: accounts@agedadvisor.co.nz";
            exit;
        }
    }// next while product
}

// end find missing quick_url  
// make sure no one is missing a quick_url   
find_missing_quick_url();




$id = $_SESSION['id'];
$sql_query = "SELECT * FROM " . _prefix('users') . " WHERE id= $id";

$res = $db->sql_query($sql_query);
$record = $db->sql_fetchrow($res);
//$first_name = $record['first_name'];
//$last_name = $record['last_name'];
$user_name = $record['user_name'];
$company = $record['company'];
$contactPerson = $record['contact_person'];
$accounts_person = $record['accounts_person'];
$accounts_email = $record['accounts_email'];
$website = $record['website'];
$email = $record['email'];
$address = $record['address'];
$physicaladdress = $record['physical_address'];
$address_other = $record['address_other'];
$address_suburb = $record['address_suburb'];
$address_city = $record['address_city'];
$postal_address_suburb = $record['postal_address_suburb'];
$postal_address_city = $record['postal_address_city'];
$postal_address_zip = $record['postal_address_zip'];
$zipcode = $record['zipcode'];
$IPaddress = $record['ip_address'];
$phone = $record['phone'];
$amount = $record['price'];
$user_image = $record['user_image'];
$lat = $record['latitude'];
$long = $record['longitude'];
$allow_emails_reviews = $record['allow_emails_reviews'];
$email_reviews_facility = $record['email_reviews_facility'];
$email_reviews_head_office = $record['email_reviews_head_office'];
$allow_emails_updates = $record['allow_emails_updates'];
$email_updates_facility = $record['email_updates_facility'];
$email_updates_head_office = $record['email_updates_head_office'];

// Review Email Settings

if (isset($update)) {

    if (isset($_POST['allow_emails_reviews']) && $_POST['allow_emails_reviews'] == 'on') {
        $allow_emails_reviews = 1;
    } else {
        $allow_emails_reviews = 0;
    }
    if (isset($_POST['email_reviews_facility']) && $_POST['email_reviews_facility'] == 'on') {
        $email_reviews_facility = 1;
    } else {
        $email_reviews_facility = 0;
    }
    if (isset($_POST['email_reviews_head_office']) && $_POST['email_reviews_head_office'] == 'on') {
        $email_reviews_head_office = 1;
    } else {
        $email_reviews_head_office = 0;
    }

// Updates Email settings

    if (isset($_POST['allow_emails_updates']) && $_POST['allow_emails_updates'] == 'on') {
        $allow_emails_updates = 1;
    } else {
        $allow_emails_updates = 0;
    }
    if (isset($_POST['email_updates_facility']) && $_POST['email_updates_facility'] == 'on') {
        $email_updates_facility = 1;
    } else {
        $email_updates_facility = 0;
    }
    if (isset($_POST['email_updates_head_office']) && $_POST['email_updates_head_office'] == 'on') {
        $email_updates_head_office = 1;
    } else {
        $email_updates_head_office = 0;
    }


    $fields_array = account_info();
    $response = Validation::_initialize($fields_array, $_POST);

    if (isset($response['valid']) && $response['valid'] > 0) {
        if ($_FILES['user_image']['error'] == 0) {
            $ImageExt = array("JPG", "PNG", "JPEG", "GIF", "jpg", "png", "jpeg", "gif");
            $ext = end(explode('.', $_FILES['user_image']['name']));
            if (in_array($ext, $ImageExt)) {
//                prd($_FILES);
                $file_tmp = $_FILES['user_image']["tmp_name"];

                $target_pathL = ADMIN_PATH . 'files/user/';
                $date = new DateTime();
                $timestamp = $date->getTimestamp();
                $logoName = $timestamp . '_' . imgNameSanitize(basename($_FILES['user_image']['name']), 20);
                $target_path = $target_pathL . $logoName;
                $target_path_thumb = $target_pathL . 'thumb_' . $logoName;
                //prd($target_path_thumb);
                if (move_uploaded_file($file_tmp, $target_path)) {
                    //if(file_exists($target_path)){echo "oK";exit;}
                    ak_img_resize($target_path, $target_path_thumb, 150, 60, $ext);
                    $user_images = $logoName;
                }
            } else {
                $_SESSION['msg'] = "Please select a valid type of Image";
            }
        }
        if ($user_images == "" || empty($user_images)) {
            $user_images = $user_image;
        } else {
            @unlink(ADMIN_PATH . 'files/user/' . $user_image);
            @unlink(ADMIN_PATH . 'files/user/thumb_' . $user_image);
        }

        $emails = $_POST["email"];
        if ($_POST["email1"] && $_POST["email"]) {
            $emails .= ',' . $_POST["email1"];
        }
        if ($_POST["email1"] && !$_POST["email"]) {
            $emails .= $_POST["email1"];
        }
        if ($_POST["email2"] && ($_POST["email"] || $_POST["email1"])) {
            $emails .= ',' . $_POST["email2"];
        }


        $fields = array(
            'user_image' => $user_images,
            'first_name' => '',
            'last_name' => '',
            'address' => $_POST["address"],
            'user_name' => $_POST["user_name"],
            'company' => $_POST['company'],
            'physical_address' => $_POST['physical_address'],
            'address_other' => $_POST['address_other'],
            'address_suburb' => $_POST['address_suburb'],
            'address_city' => $_POST['address_city'],
            'postal_address_suburb' => $_POST['postal_address_suburb'],
            'postal_address_city' => $_POST['postal_address_city'],
            'postal_address_zip' => $_POST['postal_address_zip'],
            'email' => $emails,
            'contact_person' => $_POST['contact_person'],
            'accounts_person' => $_POST['accounts_person'],
            'accounts_email' => $_POST['accounts_email'],
            'website' => $_POST['website'],
            'zipcode' => $_POST["zipcode"],
            'phone' => $_POST["phone"],
            'latitude' => $_POST["latitude"],
            'longitude' => $_POST["longitude"],
            'allow_emails_reviews' => $allow_emails_reviews,
            'email_reviews_facility' => $email_reviews_facility,
            'email_reviews_head_office' => $email_reviews_head_office,
            'allow_emails_updates' => $allow_emails_updates,
            'email_updates_facility' => $email_updates_facility,
            'email_updates_head_office' => $email_updates_head_office
        );

        $where = "where id= '$id'";
        $update_result = $db->update(_prefix('users'), $fields, $where);
        if ($update_result) {
            // Message for insert
            $msg = common_message(1, constant('UPDATE'));
            $_SESSION['msg'] = $msg;
            //ends here
            redirect_to(HOME_PATH . "supplier/profile");
        }
    } else {
        $errors = '';
        foreach ($response as $key => $message) {
            $error = true;
            $errors .= $message . "<br>";
        }
    }
}

function fList() {
    global $db;
    $id = $_SESSION['id'];
    $sql_queryCategory = "SELECT id, name FROM " . _prefix("services") . " where status = 1 AND deleted = 0";
    $resCategory = $db->sql_query($sql_queryCategory);
    $CategoryRecords = $db->sql_fetchrowset($resCategory);

    $sql_queryFacility = "SELECT facility_type FROM " . _prefix("sp_facility_type") . " where status = 1 AND deleted = 0 AND user_id = '$id'";
    $resFacility = $db->sql_query($sql_queryFacility);
    $ListFacilitys = $db->sql_fetchrowset($resFacility);
    $facility = array();
    foreach ($ListFacilitys as $ListFacility) {
        $facility[] = $ListFacility['facility_type'];
    }
    foreach ($CategoryRecords as $categoryrecord) {
        if (in_array($categoryrecord['id'], $facility)) {
            $selected = "selected";
        } else {
            $selected = '';
        }
        $option .= '<option value="' . $categoryrecord['id'] . '"' . $selected . '>' . $categoryrecord['name'] . ' </option>';
    }
    return $option;
}

function cityListselect() {
    global $db;
    $id = $_SESSION['id'];
    $cityList = array();
    $sql_queryCity = "select id, title from " . _prefix('cities') . " ORDER BY title ASC";
    $resCity = $db->sql_query($sql_queryCity);
    $CityRecords = $db->sql_fetchrowset($resCity);
    $sql_queryL = "SELECT locations FROM " . _prefix("service_locations") . " where deleted=0 AND user_id ='$id'";
    $resL = $db->sql_query($sql_queryL);
    $LocationRecords = $db->sql_fetchrowset($resL);
    $city = array();
    foreach ($LocationRecords as $LocationRecord) {
        $city[] = $LocationRecord['locations'];
    }
    foreach ($CityRecords as $cityrecord) {
        if (in_array($cityrecord['id'], $city)) {
            $selected = "selected";
        } else {
            $selected = '';
        }
        $option .= '<option value="' . $cityrecord['id'] . '"' . $selected . '>' . $cityrecord['title'] . ' </option>';
    }
    return $option;
    // return options($cityList, $selected);
}
?>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
<script type="text/javascript">
    var map;
    var marker;
    var lat = <?php echo $lat; ?>;
    var lang = <?php echo $long; ?>;
    var myLatlng = new google.maps.LatLng(lat, lang);
    var geocoder = new google.maps.Geocoder();
    var infowindow = new google.maps.InfoWindow();
    function initialize() {
        var mapOptions = {
            zoom: 7,
            center: myLatlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        map = new google.maps.Map(document.getElementById("map"), mapOptions);

        marker = new google.maps.Marker({
            map: map,
            position: myLatlng,
            draggable: true
        });

        geocoder.geocode({'latLng': myLatlng}, function (results, status) {
            //console.log(results);
            if (status == google.maps.GeocoderStatus.OK) {
                if (results[0]) {
                    $('#address').val(results[0].formatted_address);
                    $('#latitude').val(marker.getPosition().lat());
                    $('#longitude').val(marker.getPosition().lng());
                    infowindow.setContent(results[0].formatted_address);
                    infowindow.open(map, marker);
                }
            }
        });


        google.maps.event.addListener(marker, 'dragend', function () {

            geocoder.geocode({'latLng': marker.getPosition()}, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        $('#address').val(results[0].formatted_address);
                        $('#latitude').val(marker.getPosition().lat());
                        $('#longitude').val(marker.getPosition().lng());
                        var latlng = new google.maps.LatLng($('#latitude').val(), $('#longitude').val());
                        geocoder = new google.maps.Geocoder();
                        geocoder.geocode({'latLng': latlng}, function (results, status) {
//                            if (status == google.maps.GeocoderStatus.OK) {
//                                if (results[0]) {
//                                    for (j = 0; j < results[0].address_components.length; j++) {
//                                        if (results[0].address_components[j].types[0] == 'postal_code') {
//                                            $('#zipcodeValue').val(results[0].address_components[j].short_name);
//                                        }
//                                    }
//                                }
//                            }
                        });


                        infowindow.setContent(results[0].formatted_address);
                        infowindow.open(map, marker);
                    }
                }
            });
        });

    }

    google.maps.event.addDomListener(window, 'load', initialize);







</script>
 <div class="container">
   <div style="    padding: 70px;">
<div class="row">
    <div class="col-sm-offset-2 col-sm-6 success">
<?php if (isset($error)) { ?>
            <div class="errorForm" id="error" style="display: block;">
                <img align="absmiddle" src="<?php echo HOME_PATH . '/images/error.png'; ?>">&nbsp;<?php echo $errors; ?>.<br>
            </div>
            <!--echo '<div style="color:#FF0000">' . $errors . '</div>';-->
    <?php
}
if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
    ?>
            <div class="errorForm" id="error" style="display: block;">
                <img align="absmiddle" src="<?php echo HOME_PATH . '/images/error.png'; ?>">&nbsp;<?php echo $_SESSION['msg']; ?>.<br>
            </div>
    <?php
    unset($_SESSION['msg']);
}
?>
    </div>
</div>

<!--<div class="row">
    <div class="dashboard_container">
<?php // require_once 'includes/sp_left_navigation.php';    ?>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="dashboard_right_col">
                <h2 class="hedding_h2"><i class="fa fa-book"></i> <span>Update Head Office / Owner Profile</span></h2>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left">
                        <form method="POST" name="accountAddress" id="accountAddress" action=""  class="form-horizontal" role="form" enctype="multipart/form-data">
                            <input type="hidden" id="address" value=""/>
                            <input type="hidden" id="latitude" name="latitude" value="<?php echo $lat; ?>"/>
                            <input type="hidden" id="longitude" name="longitude" value="<?php echo $long; ?>" />
                            <div class="margin-bottom-20">
                                <div class="" style="width:100%;"><button type="submit" id="submit1" name="update" class="btn btn-danger btn_search btn_pay margin-bottom-20 pull-right" style="margin: 0 0 0 5px;">update</button><a class="pull-right btn btn-info" href="<?php echo HOME_PATH ?>supplier/productList">Go Back</a></div>
                                <div class="clearfix"></div>
                            </div>



                            <ul class="list-group">
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                            <span class="pull-left left_td">Upload Supplier logo</span>
                                        </div>    
                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                            <input name="user_image" id="image" class="form-control_browse input_fild_img_browse" type="file">
                                        </div>
                                        <div class="col-md-12 text-center ">
                                            <?php if (file_exists(DOCUMENT_PATH . 'admin/files/user/' . $user_image) && $user_image != '') { ?>
                                                <img title="User Image" src="<?php echo MAIN_PATH . '/files/user/thumb_' . $user_image ?>"/>
                                             <?php } ?>
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                            <span class="pull-left left_td">User Name (Login Name)</span>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                            <input type="text" name="user_name" id="username" maxlength="50" class="form-control required noSpace" value="<?php echo isset($user_name) ? stripslashes($user_name) : ''; ?>">
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                            <span class="pull-left left_td">Organisation</span>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                            <input type="text" name="company" id="company" maxlength="60" class="form-control" value="<?php echo isset($company) ? stripslashes($company) : ''; ?>">
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                            <span class="pull-left left_td">User Email (enter up to three)</span>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
<?php
$email1 = '';
$email2 = '';
if ($email) {
    $emails = explode(',', $email);
    $email = $emails[0];
    if (ISSET($emails[1])) {
        $email1 = $emails[1];
    }
    if (ISSET($emails[2])) {
        $email2 = $emails[2];
    }
}
?>
                                            <input type="text" name="email" id="email" class="form-control" value="<?php echo isset($email) ? stripslashes($email) : ''; ?>">
                                            <input type="text" name="email1" id="email1" class="form-control" value="<?php echo isset($email1) ? stripslashes($email1) : ''; ?>">
                                            <input type="text" name="email2" id="email2" class="form-control" value="<?php echo isset($email2) ? stripslashes($email2) : ''; ?>">
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                            <span class="pull-left left_td">Email Settings</span>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">

                                            <div id="show_change_reviews" style="margin: 0px 0px 10px;"><strong>When someone posts a review...</strong><br>
                                                <input id="email_reviews_facility" type="checkbox" class="margin-top-20" name="email_reviews_facility" value="on" <?php
                                                if (isset($email_reviews_facility) && $email_reviews_facility) {
                                                    echo 'checked="checked"';
                                                } else {
                                                    echo '';
                                                }
                                                ?>><label for="email_reviews_facility" style="display: inline;font-weight: normal;"> <span>Email a copy to the Facility.</span></label><br>
                                                <input id="email_reviews_head_office" type="checkbox" class="margin-top-20" name="email_reviews_head_office" value="on" <?php
                                                if (isset($email_reviews_head_office) && $email_reviews_head_office) {
                                                    echo 'checked="checked"';
                                                } else {
                                                    echo '';
                                                }
                                                ?>>
												<label for="email_reviews_head_office" style="display: inline;font-weight: normal;"> <span>Email a copy to Head Office.</span></label><br>
                                               </div>

                                            <div id="show_change_updates" style="margin: 0px 0px 10px;"><strong>For important news or updates...</strong><br>
                                                <input id="email_updates_facility" type="checkbox" class="margin-top-20" name="email_updates_facility" value="on" <?php
                                                if (isset($email_updates_facility) && $email_updates_facility) {
                                                    echo 'checked="checked"';
                                                } else {
                                                    echo '';
                                                }
                                                ?>><label for="email_updates_facility" style="display: inline;font-weight: normal;"> <span>Email a copy to the Facility.</span></label><br>
                                                <input id="email_updates_head_office" type="checkbox" class="margin-top-20" name="email_updates_head_office" value="on" <?php
                                                if (isset($email_updates_head_office) && $email_updates_head_office) {
                                                    echo 'checked="checked"';
                                                } else {
                                                    echo '';
                                                }
                                                ?>><label for="email_updates_head_office" style="display: inline;font-weight: normal;"> <span>Email a copy to Head Office.</span></label><br>
                                            </div>

                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                            <span class="pull-left left_td">Contact Person</span>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                            <input type="text" name="contact_person" id="contact_person" class=" form-control" value="<?php echo isset($contactPerson) ? stripslashes($contactPerson) : ''; ?>">
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                            <span class="pull-left left_td">Phone</span>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                            <input type="text" name="phone" id="phone" class=" form-control" value="<?php echo isset($phone) ? stripslashes($phone) : ''; ?>">
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                            <span class="pull-left left_td">Accounts Person<br>(if different to contact person)</span>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                            <input type="text" name="accounts_person" id="accounts_person" class=" form-control" value="<?php echo isset($accounts_person) ? stripslashes($accounts_person) : ''; ?>">
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                            <span class="pull-left left_td">Accounts Email<br>(if different to contact email)</span>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                            <input type="text" name="accounts_email" id="accounts_email" class=" form-control" value="<?php echo isset($accounts_email) ? stripslashes($accounts_email) : ''; ?>">
                                        </div>
                                    </div>
                                </li>




                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                            <span class="pull-left left_td">Website</span>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                            <input type="text" name="website" id="website" class=" form-control" value="<?php echo isset($website) ? stripslashes($website) : ''; ?>">
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                            <span class="pull-left left_td">Physical Address</span>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                            <span class="pull-left right_td"><textarea rows="4" cols="39" maxlength="255"  name="physical_address" class="form-control input_fild_1" id="description"><?php
                                                if ((isset($physicaladdress)) && !empty($physicaladdress)) {
                                                    echo stripslashes($physicaladdress);
                                                }
                                                ?></textarea></span>
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                            <span class="pull-left left_td">Address Suburb/Road</span>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                            <input type="text" name="address_suburb" id="website" class=" form-control" value="<?php echo isset($address_suburb) ? stripslashes($address_suburb) : ''; ?>">
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                            <span class="pull-left left_td">Address Town/City</span>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                            <input type="text" name="address_city" id="website" class=" form-control" value="<?php echo isset($address_city) ? stripslashes($address_city) : ''; ?>">
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                            <span class="pull-left left_td">Address Post Code/Zip</span>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                            <span class="pull-left right_td"><input id="zipcodeValue" type='text'class="col-sm-4 form-control"  value='<?php echo $zipcode; ?>' name='zipcode' maxlength="14" ></span>
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                            <span class="pull-left left_td">Postal Address</span>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                            <span class="pull-left right_td"><textarea rows="4" cols="39" maxlength="255"  name="address" class="form-control input_fild_1 required" id="description"><?php
                                                    if ((isset($address)) && !empty($address)) {
                                                        echo stripslashes($address);
                                                    }
                                                ?></textarea></span>
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                            <span class="pull-left left_td">Postal Address Suburb/Road</span>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                            <span class="pull-left right_td"><input id="zipcodeValue" type='text'class="col-sm-4 form-control"  value='<?php echo $postal_address_suburb; ?>' name='postal_address_suburb' maxlength="28" ></span>
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                            <span class="pull-left left_td">Postal Address Town/City</span>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                            <span class="pull-left right_td"><input id="zipcodeValue" type='text'class="col-sm-4 form-control"  value='<?php echo $postal_address_city; ?>' name='postal_address_city' maxlength="28" ></span>
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                            <span class="pull-left left_td">Postal Address Post Code</span>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                            <span class="pull-left right_td"><input id="zipcodeValue" type='text'class="col-sm-4 form-control"  value='<?php echo $postal_address_zip; ?>' name='postal_address_zip' maxlength="14" ></span>
                                        </div>
                                    </div>
                                </li>

                            
                            </ul>

                            <div class="" style="width:100%;"><button type="submit" id="submit1" name="update" class="pull-right btn btn-danger btn_search btn_pay" style="margin: 0 0 0 5px;">update</button><a class="pull-right btn btn-info" href="<?php echo HOME_PATH ?>supplier/productList">Go Back</a></div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


-->

<div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="db-add-list-wrap">
                                <div class="act-title">
                                    <h5><i class="ion-person"></i>Update Head Office / Owner Profile</h5>
                                </div>
								
								
								 <form method="POST" name="accountAddress" id="accountAddress" action=""  class="form-horizontal" role="form" enctype="multipart/form-data">
                                <div class="db-add-listing">
                                    <div class="row">
                                        <div class="col-md-4 offset-md-4">
                                            <!-- Avatar -->
                                            <div class="edit-profile-photo">
                                                                    <?php if (file_exists(DOCUMENT_PATH . 'admin/files/user/' . $user_image) && $user_image != '') { ?>
                                                <img style="margin-bottom: -47px;" title="User Image" src="<?php echo MAIN_PATH . '/files/user/thumb_' . $user_image ?>"/>
<?php } ?>
                                                <div class="change-photo-btn">
                                                    <div class="contact-form__upload-btn xs-left">
                                                       
														 <input name="user_image" id="image" class="contact-form__input-file" type="file">
                                                        <span>
                                                            <i class="icofont-upload-alt"></i>
                                                            Upload Photos
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>User Name (Login Name)</label>
                                                         <input type="text" name="user_name" id="username"  class="form-control filter-input" value="<?php echo isset($user_name) ? stripslashes($user_name) : ''; ?>">
                                                    </div>
                                                </div>
										 
												
												
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Organisation</label>
                                                         <input type="text" name="company" id="company"   class="form-control filter-input" value="<?php echo isset($company) ? stripslashes($company) : ''; ?>">
                                                    </div>
                                                </div>
												
                                               
												
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>User Email (enter up to three</label>
                                                       <?php
$email1 = '';
$email2 = '';
if ($email) {
    $emails = explode(',', $email);
    $email = $emails[0];
    if (ISSET($emails[1])) {
        $email1 = $emails[1];
    }
    if (ISSET($emails[2])) {
        $email2 = $emails[2];
    }
}
?>
                                            <input type="text" name="email" id="email" class="form-control filter-input" value="<?php echo isset($email) ? stripslashes($email) : ''; ?>"><br>
                                            <input type="text" name="email1" id="email1" class="form-control filter-input" value="<?php echo isset($email1) ? stripslashes($email1) : ''; ?>"><br>
                                            <input type="text" name="email2" id="email2" class="form-control filter-input" value="<?php echo isset($email2) ? stripslashes($email2) : ''; ?>">

                                                    </div>
                                                </div>
												
												 <div class="col-md-6">
                                                    <div class="form-group">
										     <label>Email Settings</label>
												 <div id="show_change_reviews" style="margin: 0px 0px 10px;"><strong>When someone posts a review...</strong><br>
                                                <input id="email_reviews_facility" type="checkbox" class="margin-top-20" name="email_reviews_facility" value="on" <?php
                                                if (isset($email_reviews_facility) && $email_reviews_facility) {
                                                    echo 'checked="checked"';
                                                } else {
                                                    echo '';
                                                }
                                                ?>><label for="email_reviews_facility" style="display: inline;font-weight: normal;"> <span>Email a copy to the Facility.</span></label><br>
                                                <input id="email_reviews_head_office" type="checkbox" class="margin-top-20" name="email_reviews_head_office" value="on" <?php
                                                if (isset($email_reviews_head_office) && $email_reviews_head_office) {
                                                    echo 'checked="checked"';
                                                } else {
                                                    echo '';
                                                }
                                                ?>><label for="email_reviews_head_office" style="display: inline;font-weight: normal;"> <span>Email a copy to Head Office.</span></label><br>
                                            </div>
                                            </div>
                                            </div>
											 <div class="col-md-6">
                                                    <div class="form-group">
											 <div id="show_change_updates" style="margin: 0px 0px 10px;"><strong>For important news or updates...</strong><br>
                                                <input id="email_updates_facility" type="checkbox" class="margin-top-20" name="email_updates_facility" value="on" <?php
                                                if (isset($email_updates_facility) && $email_updates_facility) {
                                                    echo 'checked="checked"';
                                                } else {
                                                    echo '';
                                                }
                                                ?>><label for="email_updates_facility" style="display: inline;font-weight: normal;"> <span>Email a copy to the Facility.</span></label><br>
                                                <input id="email_updates_head_office" type="checkbox" class="margin-top-20" name="email_updates_head_office" value="on" <?php
                                                if (isset($email_updates_head_office) && $email_updates_head_office) {
                                                    echo 'checked="checked"';
                                                } else {
                                                    echo '';
                                                }
                                                ?>><label for="email_updates_head_office" style="display: inline;font-weight: normal;"> <span>Email a copy to Head Office.</span></label><br>
                                            </div>
                                            </div>
                                            </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Contact Person</label>
                                                         <input type="text" name="contact_person" id="contact_person" class=" form-control filter-input" value="<?php echo isset($contactPerson) ? stripslashes($contactPerson) : ''; ?>">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Phone</label>
                                                        <input type="text" name="phone" id="phone" class="form-control filter-input" value="<?php echo isset($phone) ? stripslashes($phone) : ''; ?>">
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Accounts Person
                                                       (if different to contact person)</label>
                                                         <input type="text" name="accounts_person" id="accounts_person" class="form-control filter-input" value="<?php echo isset($accounts_person) ? stripslashes($accounts_person) : ''; ?>">
                                                    </div>
                                                </div>

                                         <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Accounts Email
                                                       (if different to contact email)</label>
                                                         <input type="text" name="accounts_email" id="accounts_email" class=" form-control filter-input" value="<?php echo isset($accounts_email) ? stripslashes($accounts_email) : ''; ?>">
                                                    </div>
                                                </div>


                                            <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Website</label>
                                                          <input type="text" name="website" id="website" class=" form-control filter-input" value="<?php echo isset($website) ? stripslashes($website) : ''; ?>">
                                                    </div>
                                                </div>
												
												 <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Physical Address</label>
                                                         <span class="pull-left right_td"><textarea rows="4" cols="39" maxlength="255"  name="address" class="form-control input_fild_1 required" id="description"><?php
                                                    if ((isset($address)) && !empty($address)) {
                                                        echo stripslashes($address);
                                                    }
                                                ?></textarea></span>
                                                    </div>
                                                </div>
												
												<div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Address Suburb/Road</label>
                                                         <input type="text" name="address_suburb" id="website" class=" form-control filter-input" value="<?php echo isset($address_suburb) ? stripslashes($address_suburb) : ''; ?>">
                                                    </div>
                                                </div>
												<div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Address Town/City</label>
                                                         <input type="text" name="address_city" id="website" class=" form-control filter-input" value="<?php echo isset($address_city) ? stripslashes($address_city) : ''; ?>">
                                                    </div>
                                                </div>
												
												<div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Address Post Code/Zip</label>
                                                         <input id="zipcodeValue" type='text'class="form-control filter-input"  value='<?php echo $zipcode; ?>' name='zipcode'   > 
                                                    </div>
                                                </div>
												
												<div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Postal Address Post Code</label>
                                                         <input id="zipcodeValue" type='text'class="form-control filter-input"  value='<?php echo $postal_address_zip; ?>' name='postal_address_zip'   >
                                                    </div>
                                                </div>
												
												<div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>Postal Address</label>
                                                         <span class="pull-left right_td"><textarea rows="4" cols="39" maxlength="255"  name="address" class="form-control input_fild_1 required" id="description"><?php
                                                    if ((isset($address)) && !empty($address)) {
                                                        echo stripslashes($address);
                                                    }
                                                ?></textarea></span>
                                                    </div>
                                                </div>
												
												<div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Postal Address Suburb/Road</label>
                                                         <input id="zipcodeValue" type='text'class="form-control filter-input"  value='<?php echo $postal_address_suburb; ?>' name='postal_address_suburb'   >
                                                    </div>
                                                </div>
												<div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Postal Address Town/City</label>
                                                         <input id="zipcodeValue" type='text'class="form-control filter-input"  value='<?php echo $postal_address_city; ?>' name='postal_address_city'   >
                                                    </div>
                                                </div>
												

                                                <div class="col-md-12">
                                                  <button type="submit" id="submit1" name="update" class="pull-right btn v3" style="margin: 0 0 0 5px;">update</button><a style="    padding: 9px 20px;
                                                  border-radius: 50px;"class="pull-right btn btn-info" href="<?php echo HOME_PATH ?>supplier/productList">Go Back</a>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
									
									
									
                                </div>
								
								</form>
								
                            </div>
                        </div>
                  
                   
                    </div>
                </div>
                </div>
                </div>
				<br>
<style type="text/css">
    label.error{
        color:red;
        font-size: 10px;
    }
    input{
        color:black;
        font-size: 15px;
    }

</style>
<script type="text/javascript">

    $("#allow_emails_reviews").change(function () {
        if ($(this).is(':checked')) {
            $('#show_change_reviews').slideDown('slow');
        } else {
            $('#show_change_reviews').slideUp('slow');
        }
    });

    $("#allow_emails_updates").change(function () {
        if ($(this).is(':checked')) {
            $('#show_change_updates').slideDown('slow');
        } else {
            $('#show_change_updates').slideUp('slow');
        }
    });




    $(document).ready(function () {
        var path = "<?php echo HOME_PATH; ?>admin/";
        jQuery.validator.addMethod("alphaNumSpL", function (value, element) {
            return this.optional(element) || /^[a-zA-Z\'\.\-\ ]+$/.test(value);
        }, "Please enter valid last name");

        jQuery.validator.addMethod("noSpace", function (value, element) {
            return value.indexOf(" ") < 0 && value != "";
        }, "Space is not allowed");

        $('#accountAddress').validate({
            rules: {
                address: {
                    required: true,
                    minlength: 3,
                    maxlength: 255
                },
                email: {
                    // required: true,
                    email: true,
                    remote: {
                        url: path + 'ajax.php',
                        type: 'POST',
                        data: {action: "Max2Email", email: function () {
                                return $('#email').val();
                            }
                        }
                    }
                },
                email1: {
                    // required: true,
                    email: true,
                    remote: {
                        url: path + 'ajax.php',
                        type: 'POST',
                        data: {action: "Max2Email", email: function () {
                                return $('#email1').val();
                            }
                        }
                    }
                },
                email2: {
                    // required: true,
                    email: true,
                    remote: {
                        url: path + 'ajax.php',
                        type: 'POST',
                        data: {action: "Max2Email", email: function () {
                                return $('#email2').val();
                            }
                        }
                    }
                },
                user_name: {
                    required: true,
                    minlength: 6,
                    maxlength: 30,
                    remote: {
                        url: path + 'ajax.php',
                        type: 'POST',
                        data: {action: "lookUnquieUsername", username: function () {
                                return $('#username').val();
                            }
                        }
                    }
                }
            },
            messages: {
                address: {
                    required: "This field is required",
                    minlength: "Address must be contain at least 3 chars",
                    maxlength: "Address must be contain maximum 255 chars"
                }, email: {
                    required: "This field is required",
                    email: "Please enter a valid email address.",
                    remote: "Email address already in use. Please use another email."
                },
                user_name: {
                    required: "This field is required",
                    remote: "User name already in use. Please use other User name",
                    minlength: "User name  must contain at least 6 chars",
                    maxlength: "User name  should not be more than 30 characters"
                }
            },
            submitHandler: function (form) {
                $('#submit').attr('disabled', 'disabled');
                form.submit();
            }

        });
    });
</script>
