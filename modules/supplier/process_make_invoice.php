<?php
ini_set('display_errors', '1');
require_once (__DIR__ .'../../../application_top.php');
global $db;
function get_facility_name($pr_id) {
    global $db;
    $query = " SELECT title,address_city  FROM " . _prefix("products") . " WHERE id='" . $pr_id . "' ";
    $res = $db->sql_query($query);
    $data = $db->sql_fetchrow($res);
    return $data[0].'/'.$data[1];
}

function get_plan_name($plan_id) {
    global $db;
    $query = " SELECT title,description  FROM " . _prefix("membership_prices") . " WHERE id='" . $plan_id . "' ";
    $res = $db->sql_query($query);
    $data = $db->sql_fetchrow($res);
    return $data[0].' '.$data[1];
}


function make_invoice($our_order_number,$sp_id,$submit_type="direct_credit"){

global $db;

$id = $sp_id;
$sql_query = "SELECT * FROM " . _prefix('users') . " WHERE id= $id";
$res = $db->sql_query($sql_query);
$record = $db->sql_fetchrow($res);
$first_name = $record['first_name'];
$last_name = $record['last_name'];
$user_name = $record['user_name'];
$company = $record['company'];
$contactPerson = $record['contact_person'];
$website = $record['website'];
$email = $record['email'];
$address = $record['address'];
$physicaladdress = $record['physical_address'];
$address_other = $record['address_other'];
$address_suburb = $record['address_suburb'];
$address_city = $record['address_city'];
$postal_address_suburb = $record['postal_address_suburb'];
$postal_address_city = $record['postal_address_city'];
$postal_address_zip = $record['postal_address_zip'];
$zipcode = $record['zipcode'];
$IPaddress = $record['ip_address'];
$user_image = $record['user_image'];

//echo $sql_query;exit;
//Listing of products of current supplier
$query = "SELECT * FROM " . _prefix("direct_credit") . " WHERE our_order_number='$our_order_number' LIMIT 0,1";

$res = $db->sql_query($query);
$data = $db->sql_fetchrowset($res);  
  
  
// Items Lines
foreach ($data as $record) {
$created=$record['created'];// date made in table
$order_no=$record['order_no'];// their order reference
}// end each data as record
  
		$date=date("F j, Y",strtotime( $created)-24*60*60); 

	if(!stristr($our_order_number, '.')){$order_no='';}
	
	$invoice='<table border="0" cellspacing="0" cellpadding="5" width="100%" class="table">
  <tr>
    <td colspan="2"><center><img src="http://www.agedadvisor.nz/images/find_agedadvisor_retirement_villages_agedcare_logo.png" align="top" alt="AgedAdvisor.nz">   
    </center></td>
  </tr>
  <tr>
    <td colspan="2"><center><h3>Tax/Invoice Number: '.$our_order_number.'<small><br>';
    
 if($order_no){$invoice.='Your Reference: <strong>'.$order_no.'</strong>';}
 
 $invoice.='&nbsp;<br>Dated: '.$date.'</small></h3></center></td>
  </tr>
  <tr>
    <td colspan="2"><h2>From:</h2>Aged Advisor (NZ) Ltd<br>Private Bag 4707<br>Christchurch 8014<br>New Zealand<br>Phone: 0800 243 323<br><br><font size=+1>GST No. 114-919-330</td>
  </tr>
  <tr>
    <td><h2>Account holder details</h2></td>
  </tr>
  <tr>
    <td>Attn: '.$first_name.' '.$last_name.'<br>'.$company.'<br>'.$address.'<br>'.$postal_address_suburb.'<br>'.$postal_address_city.' '.$postal_address_zip.'<br>New Zealand</td>
  </tr>
  <tr>
    <td colspan="2"><h2>Order Items</h2></td>
      </tr>
      <tr>
    <td colspan="2"><table border=0 cellspacing=0 cellpadding=5>
  <tr>
    <td>Qty</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>Item Name</td>
    <td>&nbsp</td>
    <td nowrap>Price</td>
  </tr>';
  
  
//Listing of products of current supplier
$query = "SELECT * FROM " . _prefix("direct_credit") . " WHERE our_order_number='$our_order_number' AND deleted = 0 ORDER BY id ASC";

$res = $db->sql_query($query);
$data = $db->sql_fetchrowset($res);  
  $total_amount=0;
  
// Items Lines
if($data){
foreach ($data as $record) {

$product_id=$record['product_id'];
$plan_id=$record['plan_id'];
$amount=$record['amount'];
$approve_status=$record['approve_status'];
$created=$record['created'];
$order_no=$record['order_no'];
$discount=$record['discount'];
$our_order_number=$record['our_order_number'];
$total_amount=$total_amount+$amount;
$invoice.='  <tr>
    <td>1</td>
      <td> x </td>
      <td nowrap>
       </td>
      <td>'.get_facility_name($product_id).'<br>(';
if(stristr($our_order_number, 'PN')){$invoice.="Payment for instant access to enquiry, without upgrading";$discount=0;}else{
$invoice.=get_plan_name($plan_id);
    }  
$invoice.=      ')</td>
      <td>&nbsp;</td>
      <td align="right">$'.number_format($amount-($amount*3/23),2).'</td>

  </tr>';
 }// end each data as record
  }// end if data
$invoice.='
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right"><strong>Subtotal<strong>&nbsp;&nbsp;</td>
    <td align="right"><strong>$'.number_format($total_amount-($total_amount*3/23),2).'</strong></td>
  </tr>';
	 $preGST_subtotal=$total_amount-($total_amount*3/23);
	 $GST_amount=round($preGST_subtotal*0.15,2);
 if(ISSET($discount) && $discount>0){
	 $discount_amount= $preGST_subtotal/100 * $discount;
	 $preGST_subtotal = round(($preGST_subtotal - ($preGST_subtotal/100 * $discount)),2);
	 $GST_amount=round($preGST_subtotal*0.15,2);
	 $total_amount=$preGST_subtotal+ $GST_amount;
	 
	 
	 
$invoice.='  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right"><strong>Less Special Discount of '.$discount.'%<strong>&nbsp;&nbsp;</td>
    <td align="right"><strong>($'.number_format($discount_amount,2).')</strong></td>
  </tr>
    <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right"><strong>Total (including discount)<strong>&nbsp;&nbsp;</td>
    <td align="right"><strong>$'.number_format($preGST_subtotal,2).'</strong></td>
  </tr>';
 } 
$invoice.='  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right"><strong>GST (@15%)<strong>&nbsp;&nbsp;</td>
    <td align="right"><strong>$'.number_format(($GST_amount),2).'</strong></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right"><strong>Grand Total (incl. GST)&nbsp;&nbsp;<strong></td>
    <td align="right"><strong>$'.number_format($total_amount,2).'</strong></td>
  </tr>
</table>
</td>
  </tr>';
        
        
        
//        if($submit_type=='direct_credit'){
if(!stristr($our_order_number, 'PN')){       
			$invoice.='
			  <tr>
    <td colspan="2"><strong>Payment Details</strong></td>
      </tr>
      <tr>
        <td colspan="2">
          <strong>If paying by direct credit, please ensure payment is made within 2 working days, to;<br><br>
			Aged Advisor</strong><br>
			ANZ Bank account: <strong>0108110284756 00</strong><br>
			Please include company name AND our ref/inv no.(as stated above) in the particulars when paying. 
			        </td>
      </tr>';
//		}
}

$invoice.='         
  <tr>
    <td>&nbsp;<br>&nbsp;<br></td>
    <td>&nbsp;</td>
  </tr>

</table>';
	
	
	
	
	
	
return $invoice;	
	
}

function update_status_of_invoice($our_order_number,$sp_id,$action='PAID'){

global $db;

//Listing of products of current supplier
$query = "SELECT * FROM " . _prefix("direct_credit") . " WHERE our_order_number='$our_order_number' LIMIT 0,1";

$res = $db->sql_query($query);
$data = $db->sql_fetchrowset($res);  
  
  
// Items Lines
foreach ($data as $record) {
$created=$record['created'];// date made in table
$order_no=$record['order_no'];// their order reference
}// end each data as record
  
		$date=date("F j, Y",strtotime( $created)); 

	
  
//Listing of products of current supplier
$query = "SELECT * FROM " . _prefix("direct_credit") . " WHERE our_order_number='$our_order_number' ORDER BY id ASC";

$res = $db->sql_query($query);
$data = $db->sql_fetchrowset($res);  
  $total_amount=0;
  
// Items Lines
foreach ($data as $record) {

$product_id=$record['product_id'];
$plan_id=$record['plan_id'];
$amount=$record['amount'];
$approve_status=$record['approve_status'];
$created=$record['created'];
$order_no=$record['order_no'];
$total_amount=$total_amount+$amount;
$modified = date('Y-m-d', time());
// Update old plans as inactive
        $product = array(
            'current_plan' => '0',
            'modified' => $modified
        );
        $productWhere = "where pro_id= '$product_id'";
        $product_update = $db->update(_prefix('pro_plans'), $product, $productWhere);



if($action!=='PAID'){
        $payments = array(
            'status' => 'Cancelled',
            'canceldate' => $modified
        );
        $productWhere = "where items= '$our_order_number'";
        $product_update = $db->update(_prefix('payments_test'), $payments, $productWhere);
}


// Now insert the plan as paid
if($action=='PAID'){
	$sql_insert ="insert into ad_pro_plans set supplier_id='".$sp_id."', plan_id = '".$plan_id."', pro_id = '".$product_id."', plan_start_date = '".date('Y-m-d')."', plan_end_date = '".date('Y-m-d H:i:s',mktime(23, 59, 59, date("m")+1, 0,   date("Y")))."', current_plan = '1', status='1', deleted='0', created='".date('Y-m-d')."'";
	$insert_result = mysqli_query($db->db_connect_id, $sql_insert) ;
}// end if PAID
//echo $sql_insert;exit;


 }// end each data as record
  	
}


