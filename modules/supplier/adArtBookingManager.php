<style>
    th,td,tr{border:0px!important; }
    .input{background-color:white;}
    label.error{
        color:red;
        font-size: 10px;
    }
    .redCol{
        color:red;
    }
    label{
        margin-left: 50px;
    }
    #group{
        border: 1px solid green;
    }

</style>
<?php
    global $db;
    $description = '';
    include_once("./fckeditor/fckeditor.php");
    $sql_query = "SELECT price  FROM " . _prefix("space_prices") . " where id=1";
    $res = $db->sql_query($sql_query);
    $priceRecord = $db->sql_fetchrow($res);
    $pro_id=$_SESSION['proId'];
    $pro_name=$_SESSION['proName'];

    if (isset($submit1) && $submit1 == 'submit') {
        $sp_id = $_SESSION['id'];
        $fields_array = ads_masters();
        $response = Validation::_initialize($fields_array, $_POST);
        if (isset($response['valid']) && $response['valid'] > 0) {
            if (isset($_FILES['image']['name'])) {

                if ($_FILES['image']['error'] == 0) {
                    $ext = end(explode('.', $_FILES['image']['name']));
                    $target_pathL = ADMIN_PATH . 'files/ads_image/';
                    $date = new DateTime();
                    $timestamp = $date->getTimestamp();
                    $adsImage = $timestamp . '_' . imgNameSanitize(basename($_FILES['image']['name']), 20);
                    $target_path = $target_pathL . $adsImage;
                    $target_path_thumb = $target_pathL . 'thumb_' . $adsImage;
                    move_uploaded_file($_FILES['image']["tmp_name"], $target_path);
                    ak_img_resize($target_path, $target_path_thumb, 50, 50, $ext);
                    list($w_orig, $h_orig)=getimagesize($target_path);
                    if($w_orig>1280 || $h_orig > 720 ) {
                    ak_img_resize($target_path, $target_path, 1280, 720, $ext);
                    }
                    $_POST['image'] = $adsImage; 
                }
            }

            $getMax = "SELECT max(order_by) as count from " . _prefix("ads_masters") . " WHERE status = 1 && deleted = 0";
            $resMax = $db->sql_query($getMax);
            $dataMax = mysqli_fetch_assoc($resMax);
            $order = ($dataMax['count'] == NULL) ? 1 : $dataMax['count'] + 1;
            $fields = array(
                'title' => trim($_POST['title']),
                'from_date' => trim($_POST['start_date']),
                'to_date' => trim($_POST['end_date']),
                'price' => trim($_POST['price']),
                'description' => trim($_POST['description']),
                'image' => $_POST["image"],
                'space_id' => trim($_POST["space_id"]),
                'supplier_id' => $sp_id,
                'ads_mode' => trim($_POST["ads_mode"]),
                'service_category_id' => trim($_POST["service_category_id"]),
                'created' => date('Y/m/d h:i:s', time())
            );
            $insert_result = $db->insert(_prefix('ads_masters'), $fields);
            if ($insert_result) {
                // Message for insert
                $msg = common_message(1, constant('INSERT'));
                $_SESSION['msg'] = $msg;
                //ends here
                redirect_to(HOME_PATH . "supplier/add-manager");
            }
        } else {
            $errors = '';
            foreach ($response as $key => $message) {
                $error = true;
                $errors .= $message . "<br>";
            }
        }
    }
    if (key($_REQUEST) == 'edit') {
        $id = $_GET['id'];
        $sql_query = "SELECT * FROM " . _prefix("ads_masters") . " where md5(id)='$id'";
        $res = $db->sql_query($sql_query);
        $records = $db->sql_fetchrowset($res);
        if (count($records)) {
            foreach ($records as $record) {

                $title = $record['title'];
                $price = $record['price'];
                $description = $record['description'];
                $image = $record['image'];
                $space_id = $record['space_id'];
                $supplier_id = $record['supplier_id'];
                $ads_mode = $record['ads_mode'];
                $service_category_id = $record['service_category_id'];
                $start_date = $record['from_date'];
                $end_date = $record['to_date'];
            }
        }
    }
    if (isset($update) && $update == 'update') {

        $fields_array = ads_masters();
        $response = Validation::_initialize($fields_array, $_POST);
        if (isset($response['valid']) && $response['valid'] > 0) {
            if (isset($_FILES['image']['name'])) {

                if ($_FILES['image']['error'] == 0) {
                    $ext = end(explode('.', $_FILES['image']['name']));
                    $target_pathL = ADMIN_PATH . 'files/ads_image/';
                    $date = new DateTime();
                    $timestamp = $date->getTimestamp();
                    $adsImage = $timestamp . '_' . imgNameSanitize(basename($_FILES['image']['name']), 20);
                    $target_path = $target_pathL . $adsImage;
                    $target_path_thumb = $target_pathL . 'thumb_' . $adsImage;
                    move_uploaded_file($_FILES['image']["tmp_name"], $target_path);
                    ak_img_resize($target_path, $target_path_thumb, 50, 50, $ext);
                    $_POST['image'] = $adsImage;
                }
            }

            $title = $_POST['title'];
            $price = $_POST['price'];
            $description = $_POST['description'];
            $image = $_POST['image'];
            $space_id = $_POST['space_id'];
            $supplier_id = $_POST["supplier_id"];
            $ads_mode = $_POST["ads_mode"];
            $service_category_id = $_POST["service_category_id"];
            $fields = array('title' => trim($title),
                'description' => trim($description),
                'space_id' => $space_id,
                'supplier_id' => $_SESSION['id'],
                'ads_mode' => $ads_mode,
                'service_category_id' => $service_category_id,
                'from_date' => trim($_POST['start_date']),
                'to_date' => trim($_POST['end_date']),
                'price' => $price,
                'modified' => date('Y/m/d h:i:s', time())
            );
            if ($_POST['image'] != '') {
                $fields['image'] = $image;
            }
            $where = "where md5(id)= '$id'";
            $update_result = $db->update(_prefix('ads_masters'), $fields, $where);
            if ($update_result) {
                // Message for insert
                $msg = common_message(1, constant('UPDATE'));
                $_SESSION['msg'] = $msg;
                //ends here
                redirect_to(HOME_PATH . "supplier/add-manager");
            }
        } else {
            $errors = '';
            foreach ($response as $key => $message) {
                $error = true;
                $errors .= $message . "<br>";
            }
        }
    }
?>

<div class="row">
    <div class="col-sm-offset-2 col-sm-6 success">
        <?php if (isset($error)) { ?>
                <div class="errorForm" id="error" style="display: block;">
                    <img align="absmiddle" src="<?php echo HOME_PATH . '/images/error.png'; ?>">&nbsp;<?php echo $errors; ?>.<br>
                </div>
                <!--echo '<div style="color:#FF0000">' . $errors . '</div>';-->
                <?php
            }
            if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
                ?>
                <br><div class="errorForm" id="error" style="display: block;">
                    <img align="absmiddle" src="<?php echo HOME_PATH . '/images/error.png'; ?>">&nbsp;<?php echo $_SESSION['msg']; ?>.<br>
                </div>
                <?php
                unset($_SESSION['msg']);
            }
        ?>
    </div>
</div>

<div class="container" style="padding:0px 3% 0">
    <div class="row">
        <div class="dashboard_container">
            <?php require_once 'includes/sp_left_navigation.php'; ?>
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                <div class="dashboard_right_col">
                    <h2 class="hedding_h2"><i class="fa fa-file-text-o"></i> <span><?php echo (key($_REQUEST) == 'edit') ? 'Edit Uploaded' : 'Upload'; ?> Artwork</span></h2>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 thumbnail adbooking_container">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <form name="addAds" id="addAds" method="POST" action="addBookingPaymentProcess"  class="form-horizontal form_payment" role="form" enctype="multipart/form-data" style="padding-top:20px;">
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 list_container">
                                            <div class="form-group" style="margin-bottom:6px;">
                                                <label for="title" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration">Title&nbsp;<span class="redCol">* </span></label>
                                                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                                    <input type="text" maxlength="50" class="required form-control input_fild_1" name="title" id="title"  value="<?php echo isset($title) ? stripslashes($title) : ''; ?>"/>
                                                </div>
                                            </div>
                                            <div class="form-group" style="margin-bottom:6px;">
                                                <label for="content" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration">Content&nbsp;</label>
                                                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                                    <textarea rows="4" cols="50" maxlength="500" name="description" class="form-control input_fild_1" id=""><?php echo (isset($description) && !empty($description) ) ? stripslashes($description) : ''; ?></textarea>
                                                </div>
                                            </div>

                                            <div class="form-group" style="margin-bottom:6px;">
                                                <label for="firstname" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration">Duration <span class="redCol">* </span></label>
                                                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 form_file">
                                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padding_left from_main_bg">
                                                        <label for="firstname" class="col-lg-3 col-md-3 col-sm-10 col-xs-12 label_list text-left from from_lable">From</label>
                                                        <div class="col-lg-9 col-md-9 col-sm-10 col-xs-12 padding_right">
                                                            <input type="text"  name="start_date" id="start_date" class="required form-control input_fild_1" value="<?php echo isset($start_date) ? stripslashes($start_date) : ''; ?>">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 pull-right padding_right t_text">
                                                        <label for="firstname" class="col-lg-3 col-md-3 col-sm-10 col-xs-12 label_list text-right to to_lable">To</label>
                                                        <div class="col-lg-9 col-md-9 col-sm-10 col-xs-12 padding_right">
                                                            <input type="text"  name="end_date" id="end_date" value="<?php echo isset($end_date) ? stripslashes($end_date) : ''; ?>" class="required form-control input_fild_1">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group" style="margin-bottom:6px;">
                                                <label for="service_category_id" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration">Facility Name&nbsp;<span class="redCol">* </span></label>
                                                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                                    <input type="text" maxlength="50" class="required form-control input_fild_1" name="facility" readonly="true" id="facility"  value="<?php echo isset($pro_name) ? stripslashes($pro_name) : ''; ?>"/>
                                                </div>
                                            </div>
                                            <!--                        <div class="form-group">
                                                                        <label for="space_id" class="col-sm-2 control-label">Page Space Position<span class="redCol">* </span></label>
                                                                        <div class="col-sm-8">
                                                                            <select id="space_id" class="required space_id form-control" name="space_id" style="width:235px;"><?php echo spaceList($space_id); ?></select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                            <?php $select_type = array("0" => "Normal", "1" => "Rotational", "2" => "Random"); ?>
                                                                        <label for="ads_mode" class="col-sm-2 control-label">Page Space Type<span class="redCol">* </span></label>
                                                                        <div class="col-sm-8">
                                                                            <select name="ads_mode" id='ads_mode' class="required form-control"  style="width:235px;"><?php
                                                $ads_mode = $ads_mode != '' ? $ads_mode : $_POST['plan_type'];
                                                echo options($select_type, $ads_mode);
                                            ?></select>
                                                                        </div>
                                                                    </div>-->
                                            <div class="form-group" style="margin-bottom:6px;">
                                                <label for="image" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration">Upload Artwork&nbsp;<span class="redCol"><?php echo isset($image) ? '' : '*'; ?> </span></label>
                                                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                                    <input type="file" name="image" id="image" class="form-control_browse input_fild_img_browse <?php echo isset($image) ? '' : 'required'; ?>">
                                                </div>
                                            </div>
                                            <div class="form-group" style="margin-bottom:6px;">
                                                <div class="col-sm-12 text-center">
                                                    <?php
                                                        if (file_exists(ADMIN_PATH . 'files/ads_image/' . $image) && $image != '' && key($_REQUEST) == 'edit') {
                                                            ?>
                                                            <img width="100px;" height="100px;" title="feature" src="<?php echo MAIN_PATH . '/files/ads_image/' . $image ?>">
                                                        <?php } ?>
                                                </div>
                                            </div>
                                            <div class="form-group" style="margin-bottom:6px;">
                                                <label for="price" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration">Price</label>
                                                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                                    <input type="text" maxlength="50" readonly="true" class="number required form-control input_fild_1" name="price" id="price"  value="<?php echo isset($price) ? stripslashes($price) : ''; ?>"></div>
                                            </div>


                                            <div class="form-group" style="margin-bottom:6px;">
                                                <div class="col-sm-offset-2 col-sm-8">
                                                    
                                                    <input type=hidden name='red_page' value='editArtAddsBooking'>                                               
                                                    <input type=hidden name='payment_type' value='2'>
                                                    <?php
                                                        if (key($_REQUEST) == 'edit') {
                                                            ?>
                                                            <button type="submit" value='update' name="update" id="submit" class="btn btn-danger center-block btn_search">UPDATE PURCHASE</button>
                                                            <?php
                                                        } else {
                                                            ?>
                                                            <button type="submit" value='submit' name="submit2" id="submit2" class="btn btn-danger center-block btn_search">PURCHASE</button>
                                                            <input type="hidden" name="submit1" value="Submit" />
                                                            <?php
                                                        }
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('#start_date').datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true,
        minDate: 0,
        onSelect: function(selected) {
            $("#end_date").datepicker("option", "minDate", selected)
            var dayFrom = $("#start_date").datepicker('getDate');
            var dayTo = $("#end_date").datepicker('getDate');

            if (dayFrom && dayTo) {
                var days = calcDaysBetween(dayFrom, dayTo);
                $.ajax({
                    url: '<?php echo HOME_PATH; ?>ajaxFront.php',
                    type: 'POST',
                    data: {action: 'price', days: days},
                    success: function(response) {
                        $('#price').val(response);
                    }
                });
            }

            function calcDaysBetween(startDate, endDate) {
                return (endDate - startDate) / (1000 * 60 * 60 * 24);
            }
        }
    });
    $('#end_date').datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true,
        minDate: 0,
        onSelect: function(selected) {
            $("#start_date").datepicker("option", "maxDate", selected)

            var dayFrom = $("#start_date").datepicker('getDate');
            var dayTo = $("#end_date").datepicker('getDate');

            if (dayFrom && dayTo) {
                var days = calcDaysBetween(dayFrom, dayTo);

                $.ajax({
                    url: '<?php echo HOME_PATH; ?>ajaxFront.php',
                    type: 'POST',
                    data: {action: 'price', days: days},
                    success: function(response) {
                        $('#price').val(response);
                    }
                });
            }

            function calcDaysBetween(startDate, endDate) {
                return (endDate - startDate) / (1000 * 60 * 60 * 24);
            }

        },
    });



    $(document).ready(function() {
    setTimeout(function() {
                $("#success").hide('slow');
                $('#error').hide('slow');
            }, 5000);
        $('input[type="submit"]').focus();
    });
    $('#addAds').validate({
    submitHandler: function(form) {
                    $('#submit2').attr('disabled', 'disabled');
                    form.submit();
}
            });
<?php if (file_exists(DOCUMENT_PATH . 'admin/files/ads_image/' . $image) && $image != '' && key($_REQUEST) == 'edit') { ?>
            $('#image').removeClass('required');
    <?php } ?>

</script>