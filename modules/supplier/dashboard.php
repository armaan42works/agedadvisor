<div class="container">
    <div class="row row_col">
        <h1 class="theme_color_inner">Welcome <?php echo $_SESSION['username']; ?></h1>
        <div class=" col-md-12 col-sm-12 col-xs-12 registration_container account_container" style="margin-top:1%;">
            <div class="row">
                <div class="col-md-4">
                    <a href="<?php echo HOME_PATH; ?>supplier/accountInfo">
                        <div class="widget style1 account-bg">
                            <div class="row">
                                <div class="col-xs-12 text-center">
                                    <img src="<?php echo HOME_PATH; ?>images/accountinformation_icon.png" alt="">
                                </div>
                                <div class="col-xs-12 text-center">
                                    <span>Account Information</span>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-4">
                    <a href="<?php echo HOME_PATH . 'supplier/payment' ?>">
                        <div class="widget style1 payment-bg">
                            <div class="row">
                                <div class="col-xs-12 text-center">
                                    <img src="<?php echo HOME_PATH; ?>images/payment_icon.png" alt="">
                                </div>
                                <div class="col-xs-12 text-center">
                                    <span>Payment</span>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-4">
                    <a href="<?php echo HOME_PATH . 'supplier/order-manager' ?>">
                        <div class="widget style1 order-bg">
                            <div class="row">
                                <div class="col-xs-12 text-center">
                                    <i class="fa fa-list-alt"></i>
                                </div>
                                <div class="col-xs-12 text-center">
                                    <span>Order Information</span>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <a href="<?php echo HOME_PATH . 'supplier/add-manager' ?>">
                        <div class="widget style1 ad-booking-bg">
                            <div class="row">
                                <div class="col-xs-12 text-center">
                                    <i class="fa fa-file-text-o"></i>
                                </div>
                                <div class="col-xs-12 text-center">
                                    <span>Ad Booking Manager</span>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-4">
                    <a href="<?php echo HOME_PATH; ?>supplier/gallery-manager">
                        <div class="widget style1 gallery-manager-bg">
                            <div class="row">
                                <div class="col-xs-12 text-center">
                                    <i class="fa fa-picture-o"></i>
                                </div>
                                <div class="col-xs-12 text-center">
                                    <span>Gallery Manager</span>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-4">
                    <a href="<?php echo HOME_PATH; ?>supplier/feedback-center">
                        <div class="widget style1 feedback-center-bg">
                            <div class="row">
                                <div class="col-xs-12 text-center">
                                    <img src="<?php echo HOME_PATH; ?>images/feedback_center_icon.png" alt="">
                                </div>
                                <div class="col-xs-12 text-center">
                                    <span>Feedback Center</span>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <a href="<?php echo HOME_PATH . 'supplier/artwork-manager' ?>">
                        <div class="widget style1 artwork-manager-bg">
                            <div class="row">
                                <div class="col-xs-12 text-center">
                                    <i class="fa fa-suitcase"></i>
                                </div>
                                <div class="col-xs-12 text-center">
                                    <span>Artwork Manager</span>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-4">
                    <a href="<?php echo HOME_PATH . 'supplier/product-manager' ?>">
                        <div class="widget style1 product-listing-bg">
                            <div class="row">
                                <div class="col-xs-12 text-center">
                                    <img src="<?php echo HOME_PATH; ?>images/services_icon.png" alt="">
                                </div>
                                <div class="col-xs-12 text-center">
                                    <span>Service/Product Listing</span>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
