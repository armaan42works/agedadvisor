
<script>
    function filter(item) {
        dataString = "value=" + item;
        //alert(dataString);
        $.ajax({
            type: "POST",
            url: "/modules/supplier/getvisited.php",
            data: dataString,
            success: function (data) {
                $("#results").html(data);
            }
        });
    }
</script>

<?php
// datebase coonection
session_start();
ini_set('display_errors', '1');
include("../../application_top.php");
$supplierID = $_SESSION['userId'];

$analyticsQuery = "SELECT * from ad_products WHERE supplier_id ={$supplierID}  AND `deleted` = 0";

$fetch_analyticsQuery_data = mysqli_query($db->db_connect_id,$analyticsQuery);

// $_SERVER['REMOTE_ADDR'];
?>

<style>
    table{margin:50px 0!important}
    thead tr th{height:60px; text-align:center}
    tbody tr td{padding:7px!important; line-height: 1.42857143!important; height:60px; text-align:center}

    /*.table > tbody > tr > td, .table > tfoot > tr > td {
        padding: 6px;
        line-height: 1.42857143;
        vertical-align:top;
        border-top: 1px solid #ddd;
    }*/
    .table > thead > tr > th{
        padding: 6px;
        line-height: 1.42857143;
        /*vertical-align:top;*/
        border-top: 1px solid #ddd;
    }
    tbody tr td {
        padding: 7px !important;
        line-height: 1.42857143 !important;
        height: 62px;
        text-align: center;
    }
    .table-bordered1{
        border-top:1px solid #ddd;
        border-bottom:2px solid #ddd;
        border-right:2px solid #ddd;
    }

</style>

<div class="col-md-12" style="padding-top:15px;">
    <div class="row">
        <div class="col-md-12 col-lg-5 col-sm-12">
            <h3 style="color: #f25822;"><i class="fa fa-list-alt"></i>&nbsp;Statistics Listing</h3>
        </div>
        <div class="col-md-12 col-lg-7 col-sm-12">
            <div class="facility-list-btn-box"><?php  if ($_SESSION['fac_pass']) {

} else {
    ?>
                    <a href="<?php echo HOME_PATH . 'supplier/editProfile' ?>" class="btn btn-info">Edit Head Office Details</a>
                    <a class="btn btn-danger" style="margin: 0px 5px;" href="<?php echo HOME_PATH . 'supplier/addNewProduct' ?>">Add Facility</a>
<?php  } ?>

                <a href="<?php echo HOME_PATH . 'supplier/addEventList' ?>" class="btn btn-info" style="margin-right:5px;">EventList</a>
                <a class="btn btn-danger" style="margin-right:5px;" href="<?php echo HOME_PATH . 'supplier/addEvent' ?>">Add Event</a>
                <a class="btn btn-info" href="<?php echo HOME_PATH . 'supplier/productList' ?>">Go Back</a>

            </div>
        </div>

    </div>

</div>


<div class="table-responsive analytics-table-responsive">
    <div class="analytics-table-box">
        <table class="table table-bordered table_bord table-hover recordtable margin_bottom wd-80">
            <thead>
                <tr>
                    <th rowspan="2" class="col-md-3 header">Facility Name</th>
                     <!--<th rowspan="2" class="col-md-3 header">Current Plan</th>-->
                    <th rowspan="2" class="col-md-2 header">Total Appointments</th>
                    <th rowspan="2" class="col-md-2 header">Shortlist Facility Count</th>
                    <th rowspan="2" class="col-md-2 header">Shown Phone<br>(since July 2017)</th>
                    <th rowspan="2" class="col-md-1 header">Total Users</th>
                    <th rowspan="2" class="col-md-1 header">Total Visits</th>

                </tr>
            </thead>
            <tbody>


                <?php
                while ($fetched_analytics_data = mysqli_fetch_assoc($fetch_analyticsQuery_data)) {

                    $prodid = $fetched_analytics_data['id'];
                    $prodTitle = $fetched_analytics_data['title'];

                    $appointmentAnalyticsQuery = "SELECT count(product_id) as appointmentCount from  ad_appintment WHERE product_id = $prodid";
                    $appointmentAnalyticsQueryResult = mysqli_query($db->db_connect_id,$appointmentAnalyticsQuery);

                    if (mysqli_num_rows($appointmentAnalyticsQueryResult) > 0) {
                        while ($appointmentAnalyticsData = mysqli_fetch_assoc($appointmentAnalyticsQueryResult)) {

                            $appointmentCount = $appointmentAnalyticsData['appointmentCount'];
                        }
                    } else {

                        $appointmentCount = 0;
                    }
                    
                    $phoneCountQuery = "SELECT SUM(show_phone_count) AS total from  ad_analytics WHERE prod_id = $prodid";
                    $phoneCountQueryResult = mysqli_query($db->db_connect_id,$phoneCountQuery);                    
                    if (mysqli_num_rows($phoneCountQueryResult) > 0) {
                        while ($phoneCountData = mysqli_fetch_assoc($phoneCountQueryResult)) {
                            $phoneCount = $phoneCountData['total'];
                        }
                    }
                    
                    //echo "<br>";

                    $shortlistAnalyticsQuery = "SELECT count(product_id) as shortlistCount from  ad_myshortlist WHERE product_id = $prodid";
                    $shortlistAnalyticsQueryResult = mysqli_query($db->db_connect_id,$shortlistAnalyticsQuery);
                    if (mysqli_num_rows($shortlistAnalyticsQueryResult) > 0) {
                        while ($shortlistAnalyticsQueryData = mysqli_fetch_assoc($shortlistAnalyticsQueryResult)) {

                            $shortlistCount = $shortlistAnalyticsQueryData['shortlistCount'];
                        }
                    } else {

                        $shortlistCount = 0;
                    }

                    //echo "<br>";

                    $thirtydayCountAnalyticsQuery = "SELECT sum(visited_before) as thirtydayCount from  ad_analytics WHERE `date` BETWEEN CURDATE() - INTERVAL 7 DAY AND CURDATE() AND `prod_id` = $prodid";

                    $thirtydayCountAnalyticsQueryResult = mysqli_query($db->db_connect_id,$thirtydayCountAnalyticsQuery);


                    if (mysqli_num_rows($thirtydayCountAnalyticsQueryResult) > 0) {
                        while ($thirtydayCountAnalyticsData = mysqli_fetch_assoc($thirtydayCountAnalyticsQueryResult)) {

                            if ($thirtydayCountAnalyticsData['thirtydayCount'] == NULL) {

                                $thirtydayCount = 0;
                            } else {
                                $thirtydayCount = $thirtydayCountAnalyticsData['thirtydayCount'];
                            }
                        }
                    } else {

                        $thirtydayCount = 0;
                    }

                    //echo "<br>";

                    $sixtydayCountAnalyticsQuery = "SELECT sum(visited_before) as sixtydayCount from  ad_analytics WHERE `date` BETWEEN CURDATE() - INTERVAL 30 DAY AND CURDATE() AND `prod_id` = $prodid";

                    $sixtydayCountAnalyticsQueryResult = mysqli_query($db->db_connect_id,$sixtydayCountAnalyticsQuery);

                    if (mysqli_num_rows($sixtydayCountAnalyticsQueryResult) > 0) {
                        while ($sixtydayCountAnalyticsData = mysqli_fetch_assoc($sixtydayCountAnalyticsQueryResult)) {

                            if ($sixtydayCountAnalyticsData['sixtydayCount'] == NULL) {

                                $sixtydayCount = 0;
                            } else {
                                $sixtydayCount = $sixtydayCountAnalyticsData['sixtydayCount'];
                            }
                        }
                    } else {

                        $sixtydayCount = 0;
                    }
                    //echo "<br>";

                    $ninetydayCountAnalyticsQuery = "SELECT sum(visited_before) as ninetydayCount from  ad_analytics WHERE `date` BETWEEN CURDATE() - INTERVAL 90 DAY AND CURDATE() AND `prod_id` = $prodid";

                    $ninetydayCountAnalyticsQueryResult = mysqli_query($db->db_connect_id,$ninetydayCountAnalyticsQuery);

                    if (mysqli_num_rows($ninetydayCountAnalyticsQueryResult) > 0) {
                        while ($ninetydayCountAnalyticsData = mysqli_fetch_assoc($ninetydayCountAnalyticsQueryResult)) {

                            if ($ninetydayCountAnalyticsData['ninetydayCount'] == NULL) {

                                $ninetydayCount = 0;
                            } else {
                                $ninetydayCount = $ninetydayCountAnalyticsData['ninetydayCount'];
                            }
                        }
                    } else {

                        $ninetydayCount = 0;
                    }
                    
                    $currentplan_querys = "SELECT  member.title FROM ad_pro_plans
            AS pln LEFT JOIN ad_membership_prices AS member ON member.id=pln.plan_id WHERE pln.pro_id =" . $prodid . "
            AND pln.current_plan = 1 AND pln.status = 1 AND pln.deleted = 0 ORDER BY pln.id DESC ";


                    $currentplan_querys_excute = mysqli_query($db->db_connect_id,$currentplan_querys);


                    if (mysqli_num_rows($currentplan_querys_excute) > 0) {
                        while ($currentplanData = mysqli_fetch_assoc($currentplan_querys_excute)) {

                            $currentplantitle = $currentplanData['title'];
                        }
                    } else {

                        $currentplantitle = 0;
                    }
                    
                    $totalvisite = "SELECT count(prod_id), sum(visited_before)  from  ad_analytics WHERE  `prod_id` ='$prodid'";
                    $totalvisited_query = mysqli_query($db->db_connect_id,$totalvisite) or die("query problem");
                    if (mysqli_num_rows($totalvisited_query) > 0) {
                        while ($fetch_data_visit = mysqli_fetch_assoc($totalvisited_query)) {
                            $Total_visit = $fetch_data_visit['sum(visited_before)'];
                            $Total_user = $fetch_data_visit['count(prod_id)'];
                            if ($Total_visit != '' || $Total_visit != NULL)
                                $Total_visit_count = $Total_visit;
                            else
                                $Total_visit_count = 0;

                            if ($Total_user != '' || $Total_user != NULL)
                                $Total_user_count = $Total_user;
                            else
                                $Total_user_count = 0;
                        }
                    }
                    /*                     * **********************end of code for Total Page Visit************* */
                    ?>
                    <tr>
                        <td><span style="color:#e67e22"><?php echo  $prodTitle ?></span></td>
                          <!--<td><//?= $currentplantitle ?></td> -->
                        <td><?php echo  $appointmentCount ?></td>
                        <td><?php echo  $shortlistCount ?></td>
                        <td><?php echo  $phoneCount ?></td>
                        <td><?php echo  $Total_user_count ?></td>
                        <td><?php echo  $Total_visit_count ?></td>

                    </tr>


                </tbody>
<?php } ?>



        </table>

        <table class="table table-bordered1 table_bord table-hover visitertable margin_bottom">
            <thead>
                <tr>
                    <th rowspan="2" class="col-md-2 header">
                        <span>No. of visits </span>
                        <select name="filter" onchange="filter(this.value)">
                            <option value="7">Last 7 Days</option>
                            <option value="30">Last 30 Days</option>
                            <option value="90">Last 90 Days</option>
                        </select>

                    </th>
                </tr>
            </thead>
            <tbody id="results">

<?php
$analyticsQuerys = "SELECT `id` from ad_products WHERE supplier_id ={$supplierID} AND `deleted` = 0";

$fetch_analyticsQuery_datas = mysqli_query($db->db_connect_id,$analyticsQuerys);

while ($fetched_analytics_datas = mysqli_fetch_assoc($fetch_analyticsQuery_datas)) {

    $prodid = $fetched_analytics_datas['id'];

    $sevendayCountsAnalyticsQuery = "SELECT sum(visited_before) as sevendayCounts from  ad_analytics WHERE `date` BETWEEN CURDATE() - INTERVAL 7 DAY AND CURDATE() AND `prod_id` = $prodid";

    $sevendayCountsAnalyticsQueryResult = mysqli_query($db->db_connect_id,$sevendayCountsAnalyticsQuery);



    if (mysqli_num_rows($sevendayCountsAnalyticsQueryResult) > 0) {
        while ($sevendayCountsAnalyticsData = mysqli_fetch_assoc($sevendayCountsAnalyticsQueryResult)) {

            if ($sevendayCountsAnalyticsData['sevendayCounts'] == NULL) {

                $visitedCounts = 0;
            } else {
                $visitedCounts = $sevendayCountsAnalyticsData['sevendayCounts'];
            }
        }
    } else {

        $visitedCounts = 0;
    }
    ?>
                    <tr><td><?php echo  $visitedCounts; ?></td></tr>
                <?php } ?>


            </tbody>
        </table>
    </div>
</div>



