<script type="text/javascript">
    $(document).ready(function() {
        setTimeout(function() {

            $("#success").hide('slow');
            $('#error').hide('slow');
        }, 5000);
        var page = 0;
        var li = '0_no';
        var data = '&start_date=&search_type=&end_date=';
        changePagination(page, li, 'adsBookingList', data);
        $('#search_form').submit(function(e) {
            $('#search_form').validate();
            e.preventDefault();
            if (($('#search_type').val() == 'approved') || ($('#search_type').val() == 'disapproved')) {
                var data = '&search_input=1&search_type=' + $('#search_type').val();
            } else {
                var data = '&search_input=' + $.trim($('#search_input').val()) + '&search_type=' + $('#search_type').val();
            }
            changePagination1(page, li, 'adsBookingList', data);
        });
    });
</script>
<div class="row">
    <div class="col-sm-offset-2 col-sm-6 success">
        <?php if (isset($error)) { ?>
                <div class="errorForm" id="error" style="display: block;">
                    <img align="absmiddle" src="<?php echo HOME_PATH . '/images/error.png'; ?>">&nbsp;<?php echo $errors; ?>.<br>
                </div>
                <!--echo '<div style="color:#FF0000">' . $errors . '</div>';-->
                <?php
            }
            if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
                ?>
                <?php echo $_SESSION['msg']; ?><br>
                <?php
                unset($_SESSION['msg']);
            }
        ?>
    </div>
</div>
<div class="container" style="padding:0px 3% 0">
    <div class="row">
        <div class="dashboard_container">
            <?php require_once 'includes/sp_left_navigation.php'; ?>
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12" id="group">
                <div class="dashboard_right_col  thumbail">
                    <div class="row">
                        <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12 padding_none">
                            <h2 class="hedding_h2"><i class="fa fa-file-text-o"></i> <span>Ads Booking Manager List</span></h2>
                        </div>
                        <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12" style="padding-right:0px;">
                            <div class="btn_listing pull-right text-right btn_new_btn_1" style="margin-right:0%; display:inline-block;"><a style="padding-left:9%;" href="editAddsBooking" class="btn btn-danger center-block btn_search btn_pay">Upload Ad.</a></div>
                            <div class="btn_listing pull-right text-right btn_new_btn" style="margin-right:2%; display:inline-block;"><a style="padding-left:9%;"  href="editArtAddsBooking" class="btn btn-danger center-block btn_search btn_pay">Upload Artwork</a></div>
                            <!--                <div class="col-md-3 pull-right" id="link">
                                                <a href="<?php echo HOME_PATH; ?>supplier/editAddsBooking">Add Ads Booking</a>
                                            </div>-->
                        </div>
                    </div>
                    <h4 class="logo_bottom_color font_size_16">Facility Name: <?php echo $_SESSION['proName']; ?></h4>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left scroll_2" id="pageData">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style type="text/css">

    #link{
        border: 2px solid rgb(22, 36, 194);
        padding: 3px;
        width: 170px;
        border-radius: 5px;
        font-weight: bold;
        /* background-color: graytext; */
        text-align: center;
    }
    #group{
        /*border: 1px solid green;*/
    }

</style>