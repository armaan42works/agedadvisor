 <div class="call-to-action pad-tb-70" style="background-image: url(<?php echo HOME_PATH;?>/modules/pages/images/bg/memphis-colorful.png); height:250px;">
            <div class="overlay op-5"></div>
            <div class="container">
                <div class="row align-items-center ">
                    <div class="col-md-6 col-12">
                        <div class="breadcrumb-menu">
                            <h2  class="page-title" style="color:#141414;">Compare National/Multiple Facility Providers</h2>
                        </div>
                    </div>
                    <div class="col-md-6 col-12">
                        <div class="breadcrumb-menu text-right sm-left">
                            <ul>
                                <li class="active"><a class="detail1 no-arrow" href="<?php echo HOME_PATH;?>">Home</a></li>
                                <li><a class="detail1 no-arrow"  href="<?php echo HOME_PATH;?>supplier/ranking">Compare</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

 
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
 
 
<!------ Include the above in your HEAD tag ---------->

<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
 
<style>

 
a.detail1.no-arrow {
    color: #f15922 !important;
}
.panel-table .panel-body{
  padding:0;
}

.panel-table .panel-body .table-bordered{
  border-style: none;
  margin:0;
}

.panel-table .panel-body .table-bordered > thead > tr > th:first-of-type {
    text-align:center;
    width: 100px;
}

.panel-table .panel-body .table-bordered > thead > tr > th:last-of-type,
.panel-table .panel-body .table-bordered > tbody > tr > td:last-of-type {
  border-right: 0px;
}

.panel-table .panel-body .table-bordered > thead > tr > th:first-of-type,
.panel-table .panel-body .table-bordered > tbody > tr > td:first-of-type {
  border-left: 0px;
}

.panel-table .panel-body .table-bordered > tbody > tr:first-of-type > td{
  border-bottom: 0px;
}

.panel-table .panel-body .table-bordered > thead > tr:first-of-type > th{
  border-top: 0px;
}

.panel-table .panel-footer .pagination{
  margin:0; 
}

/*
used to vertically center elements, may need modification if you're not using default sizes.
*/
.panel-table .panel-footer .col{
 line-height: 34px;
 height: 34px;
}

.panel-table .panel-heading .col h3{
 line-height: 30px;
 height: 30px;
}

.panel-table .panel-body .table-bordered > tbody > tr > td{
  line-height: 34px;
}


</style>
<div class="container">
<h2 class="headline shortlist-heading"></h2>
<?php
global $db;

function star($overallRating){
	switch ($overallRating) {
		case 1:
			$all_Rating = '<i class="fa fa-star star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>';
			break;
		case 2:
			$all_Rating = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>';
			break;
		case 3:
			$all_Rating = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>';
			break;
		case 4:
			$all_Rating = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star"></i>';
			break;
		case 5:
			$all_Rating = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i>';
			break;
		default:
			$all_Rating = 'Not Rated';
			break;
	
	}
	$rattingpercent = $overallRating*20;
	$str = '<div class="star-inactive"><div class="star-active" style="width:'.$rattingpercent.'%;"></div></div>';
	return $str;
}

$orderby = 'order by facility desc';
if($_REQUEST['sortby']=='name_asc'){
	$orderby = 'order by company asc';
}
else if($_REQUEST['sortby']=='name_desc'){
	$orderby = 'order by company desc';
}
else if($_REQUEST['sortby']=='topfacility'){
	$orderby = 'order by facility desc';
}
else 
	$orderby = 'order by facility desc';

$arr =array();
$q1 = 'SELECT sup.id,user_name,count(prd.supplier_id) as facility FROM ad_users as sup LEFT JOIN ad_products as prd ON sup.id = prd.supplier_id WHERE user_type=1 GROUP BY prd.supplier_id HAVING count(prd.supplier_id)>='.$num_of_facility.' order by facility desc';
$res1 = $db->sql_query($q1);
$records1 = $db->sql_fetchrowset($res1);
$totalrecord = count($records1);
$num_of_record_per_page = 10;
$num_of_page = $totalrecord/$num_of_record_per_page;
if($totalrecord%$num_of_record_per_page !=0 )
	$num_of_page=intval($num_of_page)+1;
$p = $_REQUEST['p'];
if($p==''){$p=1;}
$startfrom = ($p-1)*$num_of_record_per_page;
$query = 'SELECT sup.id,user_name,user_image,trim(company) as company,count(prd.supplier_id) as facility FROM ad_users as sup LEFT JOIN ad_products as prd ON sup.id = prd.supplier_id WHERE user_type=1 GROUP BY prd.supplier_id HAVING count(prd.supplier_id)>=3 '.$orderby;
$res = $db->sql_query($query);
$records = $db->sql_fetchrowset($res);
//echo $totalrecord1 = count($records);
foreach($records as $record){
	
	$suplierDetail = new stdClass();
	$suplierDetail->suplierID = $record['id'];
	$suplierDetail->suplierName = $record['company'];
	$suplierDetail->facilityCount = $record['facility'];
	$suplierDetail->user_image = $record['user_image'];
	//echo $queryProduct = 'SELECT prd.id,prd.title,prd.quick_url,ct.title as city,no_of_room as room,no_of_beds as bed,count(feed.product_id) as review,AVG(feed.overall_rating) as rating FROM ad_feedbacks as feed LEFT JOIN ad_products as prd ON prd.id = feed.product_id LEFT JOIN ad_cities as ct on ct.id = prd.city_id LEFT JOIN ad_extra_facility as extf on extf.product_id = prd.id  WHERE prd.supplier_id='.$record['id'].' GROUP BY feed.product_id order by rating desc';
	$queryProduct = 'SELECT prd.id,prd.title,prd.quick_url,ct.title as city,no_of_room as room,no_of_beds as bed,count(feed.product_id) as review,AVG(feed.overall_rating) as rating FROM ad_products as prd LEFT JOIN ad_feedbacks as feed ON prd.id = feed.product_id LEFT JOIN ad_cities as ct on ct.id = prd.city_id LEFT JOIN ad_extra_facility as extf on extf.product_id = prd.id WHERE prd.supplier_id='.$record['id'].' group by prd.id order by rating desc ';
	
	$resProduct = $db->sql_query($queryProduct);
	$recordsProduct = $db->sql_fetchrowset($resProduct);
	//avg code for facility wise
	$sumofRating = 0;
	$toReview = 0;
	$sumofBeds = 0;
	$sumofRooms = 0;
	foreach($recordsProduct as $rec){
		$sumofBeds += $rec['bed'];
		$sumofRooms += $rec['room'];
		$sumofRating += $rec['rating']*$rec['review'];
		$toReview += $rec['review'];
	}
	$totAvgrating = $sumofRating/$toReview;
	$suplierDetail->supAvgRating = $totAvgrating;
	$suplierDetail->totReview = $toReview;
	$suplierDetail->totReviewPerfacility = $toReview/$suplierDetail->facilityCount;
	$suplierDetail->totReviewPerRoom = $toReview/$sumofBeds;
	$suplierDetail->details = $recordsProduct;
	$suplierDetail->roomsByBed = $sumofRooms.'/'.$sumofBeds;
	//$suplierDetail->id = 
	$arr[] = $suplierDetail;
	//print_r($_SERVER);
	
}
 usort($arr, 'my_sort_function');



function my_sort_function($a, $b)
{
	switch($_REQUEST['sortby']){
	case 1:
		return $a->suplierName > $b->suplierName;
	case 2:
		return $a->supAvgRating < $b->supAvgRating;
	case 3:
		return $a->totReviewPerRoom < $b->totReviewPerRoom;
        case 4:
		return $a->supAvgRating > $b->supAvgRating;
	case 5:
		return $a->totReviewPerRoom > $b->totReviewPerRoom;
	default:
		return $a->suplierName > $b->suplierName;
		
	}	
		
		
}

//print_r($arr);
?>
<div class="facilityp">
    <div class="row">
        <!---left section starts-->
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 pull-left">
       <ul class="pagination"><?php for($i=1;$i<=$num_of_page;$i++){
        	$mstr='';
        if($_REQUEST['sortby']!=''){
        	$mstr = '&sortby='.$_REQUEST['sortby'];
        }
        
        	?>
        	<li <?php if($_REQUEST['p']==$i || (!isset($_REQUEST['p']) && $i==1)) echo 'class="active"';?> ><a href="<?php echo HOME_PATH;?>supplier/ranking/?p=<?php echo $i.$mstr;?>"><?php echo $i;?></a></li>
        <?php }?></ul>
        <div class="pull-right">Sort By:
	        <select class="sortby form-control" name="sortby">
	        	<option>Sort results</option>
	        	<option value="1" <?php if($_REQUEST['sortby']==1)echo "selected";?>>Sort Alphabetically</option>
	        	<option value="2" <?php if($_REQUEST['sortby']==2)echo "selected";?>>Sort by Overall Rating (Highest to Lowest)</option>
	        	<option value="4" <?php if($_REQUEST['sortby']==4)echo "selected";?>>Sort by Overall Rating (Lowest to Highest)</option>
	        	<option value="3" <?php if($_REQUEST['sortby']==3)echo "selected";?>>Sort by Reviews per room/bed (Highest to Lowest)</option>
	        	<option value="5" <?php if($_REQUEST['sortby']==5)echo "selected";?>>Sort by Reviews per room/bed (Lowest to Highest)</option>
	        </select>
        </div>
        <div class="table-responsive table_responsive_new tab_table">
			<table border="1" class="table table-bordered">
			<thead class="bg_theme" style="border-top: 1px #dddddd solid;">
				<tr>
					<th>Facility Provider</th><th>Overall Avg. Rating</th><th>No. of Dwellings / Beds</th><!--  <th>No. Of Reviews</th>--><th style="position:relative;">Reviews per Room/Bed <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="This figure relates to the proportion of reviews a facility receives compared to their size. eg. A 20 Bed facility with 10 reviews will have a score of .5 while a 100 Bed facility with 30 reviews will have a score of .3.  The higher this figure is, the more accurate their rating is likely to reflect all users."><i class="fa fa-question-circle"></i>
					</a></th><th>Compare</th>
				</tr>
				</thead>
				<?php $k=1;
				if(isset($_REQUEST['p']))
				$pagenum = $_REQUEST['p'];
				else 
					$pagenum = 1;
				$initalCount = ($pagenum-1)*10;
				$finalCount = $pagenum*10;
				for($i=$initalCount;$i<$finalCount;$i++){ $recordList = $arr[$i];
                 if(isset($arr[$i])){
                ?>
					<tr class="maintab <?php if($k%2==0)echo "evenrow";?>">
					
						<td class="  lead" width="40%" style="min-width:270px;"><a class="detail fa fa-chevron-right" href="javascript:void(0);"data-ttr="child-<?php echo $recordList->suplierID;?>"id="child-<?php echo $recordList->suplierID;?>"></a><div style="float: right; width: 86%; white-space: normal;    min-width: 215px;" ><?php echo $recordList->suplierName;if($recordList->user_image){?><br><img src="<?php echo MAIN_PATH . 'files/user/thumb_'.$recordList->user_image;?>" alt="suplier Images" /><?php }  ?> <br><a class="detail1 no-arrow" href="javascript:void(0);" data-ttr="child-<?php echo $recordList->suplierID;?>" >(<?php echo $recordList->facilityCount;?> Facilities)</a></div></td>
						<td class=" " width="30%"><?php echo round($recordList->supAvgRating*20, 0)."% from ".$recordList->totReview." Review(s)<br>";echo star(round($recordList->supAvgRating, 1));?></td>
						<td class=" " width="15%"><?php echo $recordList->roomsByBed;?></td>
						<!--<td class=" " width="15%"><?php echo $recordList->totReview;?></td>  -->
						<td class=" " width="15%"><?php echo round($recordList->totReviewPerRoom,3);?></td>
						<td><input type="checkbox" class="form-control" disabled /></td>
						
					</tr>
								<tr class="child child-<?php echo $recordList->suplierID;?> show-facility">
						
									<th>City | Facility name</th><th>Rating</th><th>No. of Dwellings / Beds</th><th>Reviews per Dwelling / Bed</th><th><button class="btn btn-warning compareButton" id="compareButton" type="button" style="margin-top: 0px;">Compare</button></th>
								</tr>
								</thead>
								<?php $cn = 1;
								foreach($recordList->details as $facilityDetail){?>
								<tr class="child child-<?php echo $recordList->suplierID;?> <?php if($cn<=5)echo 'show-facility';?> <?php if($cn%2==0)echo "evenrow";?>" >
									<td class="  lead" ><a href="<?php echo HOME_PATH.$facilityDetail['quick_url'];?>" target="_blank"><?php echo $facilityDetail['city'];?> | <?php echo $facilityDetail['title'];?></a></td><td class=" " width="30%"><?php echo round($facilityDetail['rating']*20, 0).'% from '.$facilityDetail['review'].' Review(s)';echo '<br>'.star(round($facilityDetail['rating'], 1))?></td><td><?php echo (int)$facilityDetail['room'].'/'.(int)$facilityDetail['bed'];?></td><td><?php echo round((int)$facilityDetail['review']/(int)$facilityDetail['bed'],3);?></td><td><input type="Checkbox" name="product[]" id="product-<?php echo $facilityDetail['id']; ?>" value="<?php echo $facilityDetail['id']; ?>" class="pull-right form-control checkbox_check"></td>
									
								</tr>	
								<?php 
								$cn++;
								}
								if($cn>6){?>
									<tr class="child child-<?php echo $recordList->suplierID;?> show-facility">
										<td colspan="5"><a href="javascript:void(0);" data-attr="child-<?php echo $recordList->suplierID;?>" class="makexpand" >View More</a></td>
									</tr>
								<?php }?>
								
						
					
				<?php $k++; }}?>
			</table>
			
			</div>
			<ul class="pagination"><?php for($i=1;$i<=$num_of_page;$i++){
        	$mstr='';
        if($_REQUEST['sortby']!=''){
        	$mstr = '&sortby='.$_REQUEST['sortby'];
        }
        
        	?>
        	<li <?php if($_REQUEST['p']==$i) echo 'class="active"';?>><a href="<?php echo HOME_PATH;?>supplier/ranking/?p=<?php echo $i.$mstr;?>"><?php echo $i;?></a></li>
        <?php }?></ul>
			</div>
			
        </div>
        </div>
        </div>
       <script type="text/javascript">
       $( document ).ready(function() {
           $('.sortby').change(function(){
				var link = window.location.href.split('?');
				window.location.href=link[0]+'?p=<?php echo $_REQUEST['p']? $_REQUEST['p']:'1';?>&sortby='+$(this).val();
				
               });
    	    $('tr.child').hide();
    	    $('.makexpand').click(function(){
					var mattr = $(this).attr('data-attr');
					if($(this).text()=='View More'){
						$(this).text('Close');
						}
					else{
						$(this).text('View More');
						}
					$('#'+mattr).focus();
					$('tr.'+mattr).each(function(){
							if(!$(this).hasClass('show-facility')){
									if(!$(this).hasClass('expand')){
										$(this).addClass('expand');
										}
									else{
										$(this).removeClass('expand');
										}
									$(this).slideToggle('slow');
									
								}
						});
        	    });
    	    $('a.detail,a.detail1').click(function(){
        	    if($(this).hasClass('detail1')){
						if($(this).parent().parent().find('.detail').hasClass('fa-chevron-right')){
							$(this).parent().parent().find('.detail').removeClass('fa-chevron-right');
							$(this).parent().parent().find('.detail').addClass('fa-chevron-down');
							}
						else{
							$(this).parent().parent().find('.detail').removeClass('fa-chevron-down');
							$(this).parent().parent().find('.detail').addClass('fa-chevron-right');
							}
            	    }
        	    	if($(this).hasClass('fa-chevron-right')){
        	    		$(this).removeClass('fa-chevron-right');
        	    		$(this).addClass('fa-chevron-down');
            	    	}
        	    	else if($(this).hasClass('fa-chevron-down')){
        	    		$(this).removeClass('fa-chevron-down');
        	    		$(this).addClass('fa-chevron-right');
            	    	}
					$('.'+$(this).attr('data-ttr')+'.show-facility,.'+$(this).attr('data-ttr')+'.expand').slideToggle('slow');
        	    })
				$("input:checkbox").change(function() {
            var value = $(this).val();
            var n = $("input:checked").length;
            if (n > 3) {
                alert('Please select no more than 3 products/services to compare.');
                $(this).prop("checked", false);
            } else if ($(this).prop("checked") == true) {
                var id = 'productList-' + $(this).val();
                $("#" + id).val(value);
            } else if ($(this).prop("checked") == false) {
                var id = 'productList-' + $(this).val();
                $("#" + id).val('');
            }
        });
        	    $('.compareButton').click(function() {
                    var length = 0;
                    $('#productForm input[type=hidden]').each(function() {
                        if ($(this).val() != '') {
                            length = length + 1;
                        }
                    });
                    if (length == 0) {
                        alert('Please select products/services to compare');
                    } else if (length == 1) {
                        alert('Please select more product/services to compare');
                    } else {
                        $('#productForm').submit();
                    }
                });
    	});
     
       $(document).ready(function(){
           $('[data-toggle="tooltip"]').tooltip();
       });
       
       </script>
       <style>
.detail, .detail:hover {
    background:#f69c7f none repeat scroll 0 0;
    border-radius: 4px;
    color: #000;
    cursor: pointer;
    display: inline-block;
    float: left;
    font-size: 13px;
    height: 20px;
    line-height: 20px;
    margin-bottom: 15px;
    margin-right: 10px;
    margin-top: 5px;
    text-align: center;
    text-decoration: none;
    width: 20px;
}
</style>
<!--

//-->

<form id="productForm"  name="productForm" action="<?php echo HOME_PATH . 'compare'; ?>" method="post">
                <?php foreach($arr as $recordList){
                 foreach($recordList->details as $facilityDetail){?>
                	
                    <input type = "hidden" name = "productList[]" id = "productList-<?php echo $facilityDetail['id']; ?>" value = ""/>
                <?php }}
                ?>
</form>
<?php //print_r($records);