<?php
$sid = $_SESSION['id'];
$p_id = $_SESSION['proId'];

//Finding the plan type of current supplier for the current services
$querys = "SELECT pro.*,exinfo.*,member.title as plan,
            member.logo_enable,
            member.map_enable,
            member.address_enable,
            member.phone_number_enable,
            member.video_enable,
            member.video_limit AS video_limit_enable,
            member.youtube_video AS youtube_video_enable,
            member.image_enable,
            member.facility_desc_enable,
            member.staff_enable,
            member.management_enable,
            member.activity_enable,
            member.ext_url_enable,
            member.image_limit,
            member.product_limit,
            member.number_of_review_response,
exfaci.no_of_beds,exfaci.no_of_room,exfaci.extra_info1,exfaci.brief FROM " . _prefix("products") . " AS pro"
        . " LEFT JOIN " . _prefix("pro_extra_info") . " AS exinfo ON exinfo.pro_id=pro.id"
        . " LEFT JOIN " . _prefix("extra_facility") . " AS exfaci ON exfaci.product_id=pro.id"
        . " LEFT JOIN " . _prefix("pro_plans") . " AS pln ON pln.pro_id=pro.id"
        . " LEFT JOIN " . _prefix("membership_prices") . " AS member ON member.id=pln.plan_id "
        . " WHERE pro.id =" . $p_id . " AND pln.supplier_id=" . $sid . " AND pln.status = 1 AND pln.deleted = 0 ORDER BY pln.id DESC";
$planres = $db->sql_query($querys);
//echo $querys;

$dataplans = $db->sql_fetchrow($planres);
$logo_enable = $dataplans['logo_enable'];
$number_of_review_response = $dataplans['number_of_review_response'];


$querys = " SELECT * FROM " . _prefix("feedbacks") . " WHERE md5(id) ='" . $id . "' ";
//echo $querys;exit;
$res = $db->sql_query($querys);
$record = $db->sql_fetchrow($res);
//print_r($record);



$feedback_id = $record['id'];
$feedback_reply = $record['feedback_reply'];
$Admin_FINAL_REPLY = $record['Admin_FINAL_REPLY'];
$reply = $feedback_reply;
//echo '___'.$feedback_id.'____';


if (isset($_POST['feedback_id']) && $_POST['feedback_id']) {
    // update review


    $feedback_reply = $_POST['feedback_reply'];
    $reply = $feedback_reply;

    //  if ($update_result) {
    // Message for insert
    if ($feedback_reply) {
        $que = $db->sql_query("select response_of_review from " . _prefix("pro_plans") . "  where supplier_id = " . $sid . " AND pro_id = $p_id");
        $response_of_review = $db->sql_fetchrow($que);
        $num_reviews = $response_of_review['response_of_review'];
        //exit("++++++++++++++++++++++");
        if ($num_reviews >= 1) {
            $msg = "Your reply has been added to the review";
            $planID = $db->sql_query("select ad_pro_plans.pro_id, ad_pro_plans.plan_id, ad_membership_prices.id FROM ad_pro_plans inner join ad_membership_prices on ad_pro_plans.plan_id = ad_membership_prices.id where ad_pro_plans.pro_id = " . $p_id . "");
            $record2 = $db->sql_fetchrow($planID);
            //echo ($num_reviews - 1);						 
            $query_update = "UPDATE " . _prefix("pro_plans") . " SET response_of_review =" . ($num_reviews - 1) . " WHERE supplier_id = " . $sid . " AND pro_id = $p_id";
            $res1 = $db->sql_query($query_update);
            $record1 = $db->sql_fetchrow($res1);
            //echo '<br/>===='.($number_of_review_response-1);
            $fields = array(
                'feedback_reply' => $feedback_reply,
                'Feedback_reply_UNEDITED' => $feedback_reply
            );
            $where = "where id= '$feedback_id'";
            $update_result = $db->update(_prefix('feedbacks'), $fields, $where);
            $num_reviews = $num_reviews - 1;
        } else {
            /*             * *****************for update responce****************** */
            $query_update = "UPDATE " . _prefix("pro_plans") . " SET response_of_review =" . ($num_reviews - 1) . " WHERE supplier_id = " . $sid . " AND pro_id = $p_id";
            $res1 = $db->sql_query($query_update);
            $record1 = $db->sql_fetchrow($res1);
            //echo '<br/>===='.($number_of_review_response-1);
            $fields = array(
                'feedback_reply' => $feedback_reply,
                'Feedback_reply_UNEDITED' => $feedback_reply
            );
            $where = "where id= '$feedback_id'";
            $update_result = $db->update(_prefix('feedbacks'), $fields, $where);


            $msg = "Your maximum number of responses for this month have been reached. To add more now, simply upgrade.";
        }
    } else {
        $msg = "Your reply has been removed from the review";
    }
    $_SESSION['msg'] = $msg;




    //}
}
?>

<script type="text/javascript">
    $(document).ready(function () {
        var page = 0;
        var li = '0_no';
        var data = '&id=<?php echo $id ?>';
        changePagination(page, li, 'viewFeedbackdDetail', data);
    });
</script>

<div class="row">
    <div class="dashboard_container">
        <?php require_once 'includes/sp_left_navigation.php'; ?>
        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
            <div class="dashboard_right_col">
                <h2 class="hedding_h2 pull-left"><i class="fa fa-twitch"></i> <span>View Feedback</span> </h2>
                <div class="pull-right margin-bottom-20">
                    <div style="width:100%;">
                        <?php if (!$reply) { ?>
                        <a class="btn btn-danger pull-right showreply" style="margin: 0 0 0 5px;" href="#reply_here">Reply to this review</a>
                        <?php } ?>
                        <a class="btn btn-info pull-right" href="<?php echo HOME_PATH; ?>supplier/feedback-center">&lt; Go Back</a>
                    </div>
                </div>
                <?php if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) { ?>
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <?php echo $_SESSION['msg']; ?>.
                </div>
                <?php  unset($_SESSION['msg']); } ?>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 show_details" style="<?php if (!$reply) { echo 'display: none'; } ?>"><a name="reply_here"></a>
                        <form method="POST" name="accountAddress" id="accountAddress" action="?id=<?php echo $id ?>">
                        <?php if ($number_of_review_response) { ?>
                            <label for="description"><i>Your Reply</i>&nbsp;&nbsp;
                                <?php if (!$Admin_FINAL_REPLY) { ?>
                                    <a class="clickedit btn btn-danger btn-xs" href="#">Edit</a>
                                <?php } else {
                                    echo "(NOTE: This reply has been moderated. Please contact AgedAdvisor for further edits)";
                                } ?>
                            </label>
                            <p class="showhtml" style="<?php if (!$reply) { echo 'display: none;'; } ?>"><i><?php echo str_ireplace("\n", '<br>', $reply) ?> </i></p>
                            <textarea rows="4" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 margin-bottom-20 editbox" placeholder="Type your comments here..." style="font-style: italic;display: none;" name="feedback_reply" id="description"><?php echo $reply ?></textarea><input type="hidden" value="<?php echo $feedback_id ?>" name="feedback_id">
                            <div class="reply">
                                <div class="editbox" style="width:100%;display: none;"><input type="submit" id="submit1" value="<?php if (!$reply) { ?>Insert Reply<?php } else { ?>Update Reply<?php } ?>" name="update" class="pull-right btn btn-danger btn_search btn_pay margin-bottom-20">

                                </div>
                            </div>
                        <?php } else { ?>
                                <h2>Upgrade required</h2>
                                <p>You must <a href="<?php echo HOME_PATH; ?>supplier/payment">Upgrade Listing</a> to enable this option to reply to reviews.<br>
                                    Reply will be shown below the review.
                                </p>
                        <?php } ?>
                        </form>
                    </div>
                </div>
                <!--                    <div class="col-sm-offset-2 col-sm-8 margin">
                                                                <span>Overall Rating: <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i></span>
                                                        </div>-->
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pdding_none scroll" id="pageData" >

                </div>

                <div class="pull-right margin-bottom-20">
                    <div style="width:100%;">
                        <a class="btn btn-danger pull-right showreply" style="margin: 0 0 0 5px;" href="#reply_here">Reply to this review</a>
                        <a class="btn btn-info pull-right" href="<?php echo HOME_PATH; ?>supplier/feedback-center">&lt; Go Back</a>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>

<style type="text/css">
    #fancybox-overlay{z-index: 999999999 !important;}
    #fancybox-wrap{z-index: 2147483647 !important;}
</style>


<script type="text/javascript">
    $(".clickedit").click(function () {
        $(".show_details").show("slow");
        $(".editbox").show("slow");
        $(".showhtml").hide();
    });


    $(".showreply").click(function () {
        $(".show_details").show("slow");
        $(".editbox").show("slow");
        $(".showreply").hide();
    });
</script>