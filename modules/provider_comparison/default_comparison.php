<script src="https://www.agedadvisor.nz/assets/plugins/maskedinput/jquery.mask.js"></script>
<script src="https://www.agedadvisor.nz/js/validations.js"></script>
<script type="text/javascript" src="<?php echo HOME_PATH; ?>js/bootstrap-multiselect.js"></script>

<?php	
$msg='';$city_name='';$sortbythis='feedbackcount desc';



//session_start();

function array_to_string($a){
	$return='';
	if(is_array($a)){
	while (list ($key, $val) = each ($a) ) $return=$return.$val.',';
	$return=rtrim($return, ",");
	}
	return $return;
}

function string_to_array($a){
	$return_array=array();
	if(!is_array($a)){
	$return_array=explode(",",$a); 
	}
	return $return_array;
}


function get_services_name($id){
global $db;
	$name ='&nbsp;';	
	$sql_query = "SELECT name from ad_services where id='$id'";

    $res = mysqli_query($db->db_connect_id, $sql_query);
    
    while ($records = mysqli_fetch_assoc($res)) {
	$name = $records['name'];
	}
    return $name;
}
?>	
    </div>
</div>
</div>
<!--
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
 <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.js"></script>  -->
<script>
	$(document).ready(function(e) {
		// Tooltip Start
	$('[data-toggle="tooltip"]').tooltip();
        
        $('#masked_amt').mask('###,##0,000', {reverse: true});
         // Edit Quote Start
	$(document).on('click','#edit-quoteHide', function(e) {
            $('.provider-compair-left').removeClass('in');
            $('.provider-compair-right').removeClass('in');
            $('#edit-quoteShow').addClass('active')
            $('.carousel-btn-prev').css('left','240px');
        });
	$(document).on('click','#edit-quoteShow', function(e) {
            $('.provider-compair-left').addClass('in');
            $('.provider-compair-right').addClass('in');
            $('#edit-quoteShow').removeClass('active')
            $('.carousel-btn-prev').css('left','540px');
        });

        $('#select_city').on('change', function() {
               $( "#suburbType-remove").html('Please wait...');
           $('#submitbutton').click();
        });
	// Drag & Drop Start
        $( function() {
            $( ".provider-slider-box").sortable({
                    axis: 'x',
                    handle: ".provider-heading-head"
            });
            $( ".provider-slider-box").disableSelection();
        });
	
	// Collapse Provider Compair Section Start
        $('.provider-compair-update-box .provider-compair-update-list .provider-section-list li.provider-section-age span.drop').on('click', function(e) {
			$(this).children('.fa-caret-right').toggleClass('fa-caret-down');
           	$('.provider-compair-update-box .provider-compair-update-list .provider-section-list li.provider-section-age').children('ul').slideToggle('slow');
        });
		
		$('.provider-compair-update-box .provider-compair-update-list .provider-section-list li.provider-section-capital span.drop').on('click', function(e) {
			$(this).children('.fa-caret-right').toggleClass('fa-caret-down');
           	$('.provider-compair-update-box .provider-compair-update-list .provider-section-list li.provider-section-capital').children('ul').slideToggle('slow');
        });
		
		// Carousel Start
		CarouselFunction();
		
		ResizeAddRemoveClass();
    });
	
	$(window).resize(function(e) {
        // Carousel Start
		CarouselFunction();
		
		ResizeAddRemoveClass();
		
    });
	
	function ResizeAddRemoveClass(){
		var winWidth = $(window).width();
		if(winWidth >= 767){
			$('.provider-compair-left').addClass('in');
			$('.provider-compair-right').addClass('in');
			$('#edit-quoteShow').removeClass('active');
		}else{
			$('.provider-compair-left').removeClass('in');
			$('.provider-compair-right').removeClass('in');
			$('#edit-quoteShow').addClass('active');
		}
	}
	// Carousel Start
	function CarouselFunction (){
		var winWidth = $(window).width();
		var coverwidth = $('.provider-slider-cover').width();
		var item = $('.provider-compair-update-box.provider-slide');
		var leftSpace = winWidth-coverwidth;
		var itemWidth = coverwidth/5;
		var itemLength = item.length;
		var coverBoxwidth = itemLength*itemWidth;
		var ButtonPrev = $('.carousel-btn-prev');
		var ButtonNext = $('.carousel-btn-next');
		
		ButtonPrev.css('left',leftSpace);
		
		// Carousel Button Events 
		function carouselButtons(){
			if(itemExtralength > 0){
				var counter = 0;
				var maxcunter = 0;
				ButtonNext.addClass('in');
				
				ButtonNext.click(function(e) {
					
					if(++counter){
						
							ButtonPrev.addClass('in');
							
						if(counter <= itemExtralength){
							
							maxcunter = counter;
							
							var LeftMove = itemWidth*counter;
							
							$('.provider-slider-box').css({
								left: -LeftMove
							});
							$('.provider-compair-update-box.provider-slide.active').next('.provider-compair-update-box.provider-slide').addClass('active');
							$('.provider-compair-update-box.provider-slide.active').prev('.provider-compair-update-box.provider-slide').removeClass('active');
						}
						if(counter == itemExtralength){
							counter = itemExtralength;
							maxcunter = counter;
						
							ButtonNext.removeClass('in');
						}
					}
				});
				
				ButtonPrev.click(function(e) {
					if(maxcunter>0){
					
						maxcunter--;
						ButtonNext.addClass('in');
						var LeftMove = itemWidth*maxcunter;
							$('.provider-slider-box').css({
								left: -LeftMove
							});
							$('.provider-compair-update-box.provider-slide.active').prev('.provider-compair-update-box.provider-slide').addClass('active');
							$('.provider-compair-update-box.provider-slide.active').next('.provider-compair-update-box.provider-slide').removeClass('active');
						counter = maxcunter;
					}
					if(maxcunter==0){
						counter= 0;
						ButtonPrev.removeClass('in');
											}
				});
			}
		}
			
		if(winWidth >= 767){
			
			
			if(coverwidth >= 670){
				var itemWidth = coverwidth/5;
				item.css({
					width: 	itemWidth
				});
				
				var itemExtralength = itemLength-5;
				
				carouselButtons();
				
			}
			else if(coverwidth >= 500){
				var itemWidth = coverwidth/4-2;
				item.css({
					width: 	itemWidth
				});
				
				var itemExtralength = itemLength-4;
				
				carouselButtons();
				
			}
			else if(coverwidth >= 400){
				var itemWidth = coverwidth/3-2;
				item.css({
					width: 	itemWidth
				});
				
				var itemExtralength = itemLength-3;
				
				carouselButtons();
				
			}
			else if(coverwidth >= 300){
				var itemWidth = coverwidth/2-2;
				item.css({
					width: 	itemWidth
				});
				
				var itemExtralength = itemLength-2;
				
				carouselButtons();
				
			}
			else if(coverwidth >= 150){
				var itemWidth = coverwidth/1-2;
				item.css({
					width: 	itemWidth
				});
				
				var itemExtralength = itemLength-1;
				
				carouselButtons();
				
			}
			
		}
	}
</script>

<?php
global $db;
 
$counter=1;
 
       $sql_query = "SELECT cpi_value FROM " . _prefix("cpi_value") . " where id = 1";
        $res = mysqli_query($db->db_connect_id, $sql_query);
        $cpi_value= mysqli_fetch_assoc($res);
        $cpi=$cpi_value['cpi_value'];

        $cities = array();
        $sql_query = "SELECT id, title FROM ad_cities where status =1 AND title <> '' AND comparisonpage_deleted=0 order by title ";
        $res = mysqli_query($db->db_connect_id, $sql_query);
        while ($records = mysqli_fetch_assoc($res)) {
           $cities[]=$records;
        }		
        
        if(isset($_POST['submit']))
        {
         $amount=$_POST['amount'];
         $amount = str_replace( ',', '', $amount );
         
         if(!is_numeric($amount)){$amount=0;}
         /*if(isset($_SESSION['city']) && ($_SESSION['city'] != $_POST['city'])){
             unset($_SESSION['session_compare_product']);
             unset($_SESSION['session_compare_count']);        
         }else{
             $_SESSION['city'] = $_POST['city'];             
         }*/
         $city=$_POST['city'];      
         $sortby = $_POST['sortby'];
         //echo $sortby;exit;
         $sortbythis ='prd.'.$sortby;
         if($sortby=="deff_fee"){$sortbythis='deff_sort';}
         if($sortby=="weekly_fee"){$sortbythis='prd.convert_to_weekly';}
         if($sortby=="mostReviews"){$sortbythis='feedbackcount desc';}
         if($sortby=="star_rating"){$sortbythis='rating desc, feedbackcount desc';}
         if($sortby=="max_deff_sort"){$sortbythis='max_deff_sort desc';}
         if($sortby=="max_weekly_fee_sort"){$sortbythis='weekly_fee desc';}
         if($sortby=="max_entry_sort"){$sortbythis='max_entry_value desc';}  
         if($sortby=="min_age_sort"){$sortbythis='min_age_sort asc';}  
         //print_r($suburbType);
         if(isset($suburbType)){$suburb=mysqli_escape_string(array_to_string($suburbType));}else{$suburb='';}
         

        $suburblist = array();
         $sql_query = "SELECT DISTINCT address_suburb FROM ad_products where status =1 AND address_suburb <> '' AND deleted=0 AND status=1 AND city_id=$city order by address_suburb";
        $res = mysqli_query($db->db_connect_id, $sql_query);
        while ($records = mysqli_fetch_assoc($res)) {
            $suburblist[$records['address_suburb']] = trim(ucwords($records['address_suburb']));
        }

$suburblist = array_unique($suburblist);
asort($suburblist);

/* For filtering with suburb
       $sql_query ="select prd.*, city.id, usr.user_name, usr.user_image, extra.no_of_room as rooms  from ad_products as prd, ad_users as usr , ad_cities as city, ad_extra_facility as extra WHERE prd.supplier_Id =usr.id AND $amount>=prd.min_entry_value AND $amount<=prd.max_entry_value AND prd.city_id=$city AND  extra.product_id=prd.id AND prd.address_suburb='$suburb' AND city.id=prd.city_id order by prd.$sortby ";
*/
$amount_search=$amount*1.1;// Adds 10 percent
     $sql_query ="select (select count(*) from ad_feedbacks as FD where FD.product_id=prd.id and FD.deleted=0 group by FD.product_id) as feedbackcount, (select avg(overall_rating) from ad_feedbacks as FD where FD.product_id=prd.id and FD.overall_rating>0 group by FD.product_id) as rating, prd.*,prd.id AS mypr_id, prd.min_deff_sort AS 'deff_sort', city.id, usr.user_name, usr.user_image, extra.no_of_room as rooms  from ad_products as prd, ad_users as usr , ad_cities as city, ad_extra_facility as extra WHERE prd.supplier_Id =usr.id ";
if($amount>1){     
    $sql_query .=" AND $amount_search>=prd.min_entry_value  ";
    
}else{
    $sql_query .=" AND 0<=prd.max_entry_value ";
}
$sub_string = "";$suburb_extra= "";

if(ISSET($suburbType)){$sub_string = implode("','",$suburbType);
	if($sub_string){
	    $suburb_extra= " AND ( prd.address_suburb IN ('$sub_string') ) ";
	}
}

    $sql_query .=" AND prd.city_id=$city AND  extra.product_id=prd.id AND city.id=prd.city_id AND prd.certification_service_type LIKE 'Ret%' order by $sortbythis";    
      $res = mysqli_query($db->db_connect_id, $sql_query)or die($sql_query);

      $total= mysqli_num_rows($res);
    
	if($total==0){
        $msg="Sorry, no results found. Please try a different search";
      }
 }else if( isset ($_GET['city']) && isset ($_GET['sort']) ){
     
    if(isset ($_GET['amt'])){
        $amount=$_GET['amt']; 
    }else{
        $amount= 0;
    }     
    $city=$_GET['city'];      
    $sortby = $_GET['sort'];
    
    
    if(isset($suburbType)){$suburb=mysqli_escape_string(array_to_string($suburbType));}else{$suburb='';}
         

        $suburblist = array();
         $sql_query = "SELECT DISTINCT address_suburb FROM ad_products where status =1 AND address_suburb <> '' AND deleted=0 AND status=1 AND city_id=$city order by address_suburb";
        $res = mysqli_query($db->db_connect_id, $sql_query);
        while ($records = mysqli_fetch_assoc($res)) {
            $suburblist[$records['address_suburb']] = trim(ucwords($records['address_suburb']));
        }

$suburblist = array_unique($suburblist);
asort($suburblist);
$amount_search=$amount*1.1;// Adds 10 percent

$sql_query ="select (select count(*) from ad_feedbacks as FD where FD.product_id=prd.id and FD.deleted=0 group by FD.product_id) as feedbackcount, (select avg(overall_rating) from ad_feedbacks as FD where FD.product_id=prd.id and FD.overall_rating>0 group by FD.product_id) as rating, prd.*,prd.id AS mypr_id, prd.min_deff_sort AS 'deff_sort', city.id, usr.user_name, usr.user_image, extra.no_of_room as rooms  from ad_products as prd, ad_users as usr , ad_cities as city, ad_extra_facility as extra WHERE prd.supplier_Id =usr.id ";
if($amount>1){     
    $sql_query .=" AND $amount_search>=prd.min_entry_value  ";
    
}else{
    $sql_query .=" AND 0<=prd.max_entry_value ";
}
$sub_string = "";$suburb_extra= "";

if(ISSET($suburbType)){$sub_string = implode("','",$suburbType);
    if($sub_string){
        $suburb_extra= " AND ( prd.address_suburb IN ('$sub_string') ) ";
    }
}

$sql_query .=" AND prd.city_id=$city AND  extra.product_id=prd.id AND city.id=prd.city_id AND prd.certification_service_type LIKE 'Ret%' order by $sortbythis";    
$res = mysqli_query($db->db_connect_id, $sql_query)or die($sql_query);

$total= mysqli_num_rows($res);

if($total==0){
    $msg="Sorry, no results found. Please try a different search";
}     
    ?>
<script>
    $(document).ready(function(){        
        var sorted =  $('#sorting-providers-sidebar').val("<?php echo $sortby?>");
        var city = $('#select_city').val("<?php echo $city?>");
    })
</script>
 <?php }
 else {
    $amount=0;
    $sortby="deff_sort";
    $sql_query = "select prd.* ,prd.id AS mypr_id, min_deff_sort AS 'deff_sort', usr.user_name, usr.user_image,extra.no_of_room as rooms from ad_products as prd, ad_users as usr, ad_extra_facility as extra  WHERE prd.supplier_Id=usr.id AND extra.product_id=prd.id AND prd.min_entry_value>$amount AND prd.certification_service_type LIKE 'Ret%' order by prd.$sortbythis ";   
    $res = mysqli_query($db->db_connect_id, $sql_query);
 }
if($counter==1){ ?>
 <script>
    $(document).ready(function(){
        $("#provider1").addClass('selected');
    });
 </script>
 <?php } ?>
<div class="provider-compair-cover" style="    margin-top: 71px;">
    <div class="provider-compair-left in">
    	<div class="blue-light-gradient">
        
    	<div class="provider-compair-heading">
        	<button type="button" class="btn btn-info pull-right" id="edit-quoteHide">Hide</button>
        	<h2><i class="fa fa-pencil"></i> Edit Estimate</h2>
                 
       </div>
        
        <div class="provider-compair-form">
        	<form name="quote" id="quotemain" method="post">
              <div class="form-group">
                <div class="input-group">
                  <div class="input-group-addon">$</div>
                  <input type="text" class="form-control" name="amount" value="<?php echo $amount;?>" placeholder="Amount" id="masked_amt">
                </div>
              </div>
              <div class="form-group">

                <select class="form-control" name="city" id="select_city" required>
                   <option value="">Choose a City/Town</option>
                   <?php 
                   foreach ($cities as $value) {
                    ?>
                   	<option value="<?php echo $value['id'];?>" <?php if($value['id']==$city){echo "selected";}?>> <?php echo $value['title'];?></option>
                  <?php } ?>
                </select>
               
              </div>
<?php if(ISSET($suburblist) && count($suburblist)>1){?>
    <div class="form-group" id="suburbType-remove">
<label for="suburbType" class="label_text">Filter by Suburb:</label>
    <select class="form-control" name="suburbType[]" multiple="true" id="suburbType">
        <?php echo optionsService($suburblist,$suburb); ?>
    </select>
    <script type="text/javascript">
                $(document).ready(function() {
                    $('#suburbType').multiselect();
                })
            </script>
    </div>
<?php }?>

              
              
              <div class="form-group">
                  <select class="form-control" name="sortby" id="sorting-providers-sidebar">
                  
                  <option value="mostReviews"<?php if($sortby=="mostReviews"){echo "selected";}?>>Most Reviews</option>
                  <!--<option value="mostInfo"  <?php if($sortby=="mostInfo"){echo "selected";}?>>Most Info.</option>-->
                  <option value="star_rating"<?php if($sortby=="star_rating"){echo "selected";}?>>Highest Star Rating</option>
                  <option value="min_age_sort"<?php if($sortby=="min_age_sort"){echo "selected";}?>>Lowest Entry Age</option>
                  <!--<option value="most_facilities" <?php if($sortby=="most_facilities"){echo "selected";}?>>Most Facilities</option>-->
                  <option value="min_deff_sort" <?php if($sortby=="min_deff_sort"){echo "selected";}?>>Lowest Deferred Mgmt Fee</option>
                  <option value="max_deff_sort" <?php if($sortby=="max_deff_sort"){echo "selected";}?>>Highest Deferred Mgmt Fee</option>
                  <option value="weekly_fee_sort"<?php if($sortby=="weekly_fee_sort"){echo "selected";}?>>Lowest Maintenance Fee </option>
                  <option value="max_weekly_fee_sort"<?php if($sortby=="max_weekly_fee_sort"){echo "selected";}?>>Highest Maintenance Fee </option>
                  <option value="min_entry_sort"<?php if($sortby=="min_entry_sort"){echo "selected";}?>>Lowest Purchase Price</option>
                  <option value="max_entry_sort"<?php if($sortby=="max_entry_sort"){echo "selected";}?>>Highest Purchase Price</option>
                  <!--<option value="capital_gain"<?php if($sortby=="capital_gain"){echo "selected";}?>>Capital Gain</option>-->
                   <option value="title"<?php if($sortby=="title"){echo "selected";}?>>List A to Z</option>
                </select>
              </div>
           
              
              <button type="submit" id="submitbutton" name ="submit" class="btn btn-danger"><i class="fa fa-refresh"></i> Update</button>
            </form>
        </div>
        </div>
        
        <div class="provider-compair-form">
            <div id="short-div">
                <button type="button" class="btn btn-info" id="show-shortlist">Compare Checked Facilities  
                    <span  class="numbers" id="shortlist_id">
                    <?php if(isset($_SESSION['shortlist_compare_count'])) {
                            echo $_SESSION['shortlist_compare_count'];
                        }else{
                            echo "0";
                        }
                    ?>                        
                    </span>
                </button>
            
                <?php if(isset($_SESSION['shortlist_compare_count']) && $_SESSION['shortlist_compare_count'] >0) { ?>
                <button type="button" class="link" id="reset-shortlist">Reset Compared Facilities</button>
                <?php }else{ ?>
                    <button type="button" class="link reset-shortlist" id="reset-shortlist"  style="display:none;">Reset Compared Facilities</button>
                <?php } ?>
            
            </div>
            <div id="showall-div" style="display:none;">
                <button type="button" class="btn btn-info" id="hide-shortlist">Show All Facilities</button>
                
                <?php if(isset($_SESSION['shortlist_compare_count']) && $_SESSION['shortlist_compare_count'] >0) { ?>
                <button type="button" class="link" id="reset-shortlst">Reset Compared Facilities</button>
                <?php }else{ ?>
                    <button type="button" class="link reset-shortlist" id="reset-shortlst" style="display:none;">Reset Compared Facilities</button>
                <?php } ?>
            </div>
        </div>
       
        <?php echo getRightBannerNEW1($city_name);  ?>
        
    </div>
    <div class="provider-compair-right in">
    	<div class="provider-compair-heading-box">
        	<button type="button" class="btn btn-info" id="edit-quoteShow"><i class="fa fa-pencil"></i>Edit Estimate</button>
        </div>
        <div class="clearfix"></div>
        <?php if($msg){ ?>
        <div id="no-results" style="display: none;">
        	<h4 class="alert alert-danger" style="text-align:center;"><?php echo $msg ;?></h4>
        	</div>
        	<div class="clearfix"></div>
                <div class="provider-compair-update-box" style="display:none;">
                    <p>&nbsp;</p>
                </div>
               
                <div class="col-md-10 col-md-offset-1">
                    <div class="google-add-box"> <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Comparison-page-dec22 -->
<ins class="adsbygoogle"
     style="display:inline-block;width:728px;height:90px"
     data-ad-client="ca-pub-2800188040402843"
     data-ad-slot="9008759012"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script><!--<img src="images/google-ads.jpg">--></div>
                </div>
                <div class="clearfix"></div>
        	<?php } ?>

        <div class="provider-compair-update-cover">
        <?php if(!$msg) { ?>
            <div style="display: none;">
            	<h5 class="alert pdtb-0" style="text-align:center;">Note: Figures shown are based on provider / resident feedback and are indicative only. They are not to be used as the primary basis for making any financial decisions. We recommend contacting any providers directly to get a copy of their latest disclosure statements.</h5>
            </div>
            <div class="provider-compair-update-box" style="display:none">
                <p>&nbsp;</p>
            </div>
            <div class="col-md-10 col-md-offset-1">
            	<div class="google-add-box"><script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Comparison-page-dec22 -->
<ins class="adsbygoogle"
     style="display:inline-block;width:728px;height:90px"
     data-ad-client="ca-pub-2800188040402843"
     data-ad-slot="9008759012"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script> <!--<img src="images/google-ads.jpg">--> </div>
            </div>
            <div class="clearfix"></div>
            <?php } ?>
        	<div class="provider-compair-update-box">
            	<ul class="provider-compair-update-list">
                	<li class="provider-list-heading">
                    	<ul class="provider-heading-list">
                            <li class="provider-heading-head"></li>
                            <li class="provider-heading-rating" style="margin-top: 25px;"> <i class="fa fa-question-circle" data-toggle="tooltip" title="On non-mobile devices, you can move the columns into any specific order you like, by click and holding the cursor on the column header and then dragging it either left or right."></i> Provider </li>
                            <li class="provider-heading-rating" style="margin-top: 15px;min-height: 55px;">Village Name</li>
                            <li class="provider-main-minmum-rating" ></li>
                            <li class="provider-heading-rating" style="margin-top: -25px;"><i class="fa fa-question-circle" data-toggle="tooltip" title="This is the providers current average rating based on all their facilities, as rated on AgedAdvisor."></i> Average Rating<div style=" ">on AgedAdvisor</div></li>
                             
                            <li class="provider-main-minmum-rating">&nbsp;</li>
                            <li class="provider-heading-rating" style="margin-top: 70px;margin-bottom: 8px;"><i class="fa fa-question-circle" data-toggle="tooltip" title="ORA/LTO - Occupational Rights Agreement / Licence To Occupy is the most popular model currently used in NZ, where you pay an upfront amount and then receive this back less a deferred fee. Other models include Outright Purchase or Rental."></i>Fee model</li>

                            
                        </ul>
                    </li>
                    <li class="provider-list-main" style="padding-top: 5px;padding-bottom: 5px;">
                    	<ul class="provider-main-list">
                        	<li class="provider-main-minmum-age"><i class="fa fa-question-circle" data-toggle="tooltip" title="Each village normally has a minimum age to be a resident. The age stated is based on a single person. Where a 'couple' (two people) has one person over the minimum age and another under, then we recommend you contact the provider directly"></i> Minimum Age of Entry </li>
                           
                        </ul>
                    </li>
                    <li class="provider-list-section">
                    	<ul class="provider-section-list">
                        	<li class="provider-section-age" style="padding-top: 5px;padding-bottom: 5px;min-height: 85px;"><span class="m-pvd-heading-full drop" style="min-height: 75px;"><i class="fa fa-question-circle" data-toggle="tooltip" title="This is the maximum amount of money that will be deducted from the purchase price of the property and kept by the provider on exit or sale of your unit / townhouse. This fee is usually calculated over 2 or more years and pro-rata'd if exiting during a year"></i> <i class="fa fa-caret-right"></i>Max. Deferred Fee </span>

                            	<ul class="provider-section-drop-list" style="display: none;">
                                    <li class="m-hide" style="padding: 5px;">1 Year</li>
                                    <li class="m-hide" style="padding: 5px;">2 Year</li>
                                    <li class="m-hide" style="padding: 5px;">3 Year</li>
                                    <li class="m-hide" style="padding: 5px;">4 Year</li>
                                    <li class="m-hide" style="padding: 5px;">5 Year</li>
                                </ul>
                            </li>
                             <li class="provider-heading-name" style="height: 86px;">Lowest Known Price<div style="margin-top:-25px;">&nbsp;</div></li>
                             
                              <li class="provider-heading-name" style="height: 86px;">Highest Known Price<div style="margin-top:-25px;">&nbsp;</div></li>
                              <li class="provider-heading-rating" style="height: 71px;">Village Name</li>
                               <li class="provider-main-capital" style="height: 61px;"><i class="fa fa-question-circle" data-toggle="tooltip" title="Who receives the increase between the purchase price of the property and the sale price on exiting. Most retirement villages do not pass on any capital gain, however some do."></i> Capital Gain </li>
                               <li class="provider-main-capital" style="height: 61px;"><i class="fa fa-question-circle" data-toggle="tooltip" title="Who is liable for any loss in capital value on sale of unit."></i> Capital Loss (if any) </li>
                            <li class="provider-section-capital" style="padding-top: 5px;padding-bottom: 5px;min-height: 86px;"><i class="fa fa-question-circle" data-toggle="tooltip" title="This is the maintenance that will apply Weekly"></i><span class="drop"><i class="fa fa-caret-right"></i></span>Maintenance Fees<div style=" ">Starting from</div>
                            <div class="clearfix"></div>
                            <ul class="provider-section-drop-list">
                                	<li style="height: 60px">Yearly Maintenance Cost</li>
                                   
                                </ul>
                            </li>
                            <li class="provider-heading-name" style="height: 60px;">Maintenance Fee Increases?</li>
                        </ul>
                    </li>
                    <li class="provider-list-footer">
                    	<ul class="provider-footer-list">
                    	<li class="provider-footer-facilties" style="height: 61px;">Extra Costs/Fees</li>
                            <li class="provider-footer-facilties" style="height: 61px;"><i class="fa fa-question-circle" data-toggle="tooltip" title="This is the total number of studio apartments, apartments, villas or townhouses that this village comprises of."></i> No. of dwellings in village</li>
                            <li class="provider-footer-star" style="height: 61px;">Data current as of</li>
                            <li class="provider-footer-providers" style="height: 61px;">Data supplied by</li>
                            <li class="provider-compair-form">                            
                            <button type="button" class="btn btn-info" id="show-shortlistb">Compare Checked Facilities</button>
                            <?php //if(isset($_SESSION['shortlist_compare_count']) && $_SESSION['shortlist_compare_count'] >0) { ?>
                            <!--<button type="button" class="link" id="reset-shortlistb">Reset Compared Facilities</button>
                            <?php// }else{ ?>
                                <button type="button" class="link" id="reset-shortlistb"  style="display:none;">Reset Compared Facilities</button>-->
                            <?php //} ?>
                            <button type="button" class="btn btn-info"  id="hide-shortlistb" style="display:none;">Show All Facilities</button>
                        </li>
                        </ul>
                    </li>
                </ul>
            </div>

            <div class="provider-slider-cover">
               <div class="row" id="loading-gif">
                        <div class="col-sm-12">

                            <div id="loading">
                                <ul class="bokeh">
                                    <li></li>
                                    <li></li>
                                    <li></li>
                                </ul>
                                <h3 style="text-align:center;">Just a moment while we create your village profile...</h3>
                            </div>
                        </div>
                </div>
            	<button type="button" class="carousel-btn-prev"><i class="fa fa-chevron-left"></i></button>
                <button type="button" class="carousel-btn-next"><i class="fa fa-chevron-right"></i></button>
                
                <div class="provider-slider-box" style="display: none;" id="main-content-loading">
                <?php 
	                
	                
	                $collecting_data = '<span><i class="fa fa-question-circle" style="float: none;width:auto;" data-toggle="tooltip" title="The provider has not yet supplied this information."></i></span>';
	                
	                
	                
	                 while($records = mysqli_fetch_assoc($res)){
                	
                            $get_review="select count(product_id) from  ad_feedbacks  WHERE product_id=".$records['pro_extra_info'];
                            $totalreview=mysqli_fetch_row(mysqli_query($db->db_connect_id, $get_review));
                            
                            $sql_query_country = "select  count(supplier_Id) from  ad_products  WHERE supplier_Id=".$records['supplier_id']." AND `certification_service_type` = 'Retirement Village'";
                            
                            $totalInCountry=mysqli_fetch_row(mysqli_query($db->db_connect_id, $sql_query_country));
                           
                ?>
                    <div class="provider-compair-update-box provider-slide active" data-providername="provider1" id="provider1">
                        <div class="provider-compair-update-box-in <?php if((($amount < $records['min_entry_value']) || ($records['min_entry_sort'] == 999999999)) && ($amount > 0)){ echo "provider-compare-column-border";} ?>">
                            <ul class="provider-compair-update-list <?php if((($amount < $records['min_entry_value']) || ($records['min_entry_sort'] == 999999999)) && ($amount > 0)){ echo "provider-compare-column-backg"; }?> ">
                                <li class="provider-list-heading">
                                    <ul class="provider-heading-list">
                                        <li class="provider-heading-head <?php if((($amount < $records['min_entry_value']) || ($records['min_entry_sort'] == 999999999)) && ($amount > 0)){ echo "provider-compare-column-back"; }?>">
                                        	<span class="catg"><?php if($counter==1){ 
                                        		if($sortby=='weekly_fee_sort') { 

                                        			echo "Lowest Weekly Fee";
                                        		 }
                                                         else if($sortby=='max_weekly_fee_sort'){
                                                             
                                                                echo "Highest Weekly Fee";
                                                         }
                                        		 else if($sortby=='capital_gain') {

                                        		 	echo "Capital Gain"; 
                                        		 }
                                        		 else if($sortby=='min_entry_sort') {

                                        		 	echo "Lowest Purchase Price"; 
                                        		 }
                                                         else if($sortby == 'max_entry_sort'){
                                                             
                                                                echo "Highest Purchase Price";
                                                         }
							 else if($sortby=='title') {

                                        		 	echo "Sorted A - Z"; 
                                        		 }
                                                         else if($sortby == 'mostReviews'){
                                                             
                                                                echo "Most Reviews";
                                                         }
                                                         else if($sortby == 'star_rating'){
                                                             
                                                                echo "Highest Star Rating";
                                                         }
                                                         else if($sortby == 'min_deff_sort'){
                                                             
                                                                echo "Lowest Def. Mgmt. Fee";
                                                         }
                                                         else if($sortby == 'max_deff_sort'){
                                                             
                                                                echo "Highest Def. Mgmt. Fee";
                                                         }
                                                        else if($sortby == 'min_age_sort'){
                                                             
                                                                echo "Lowest Entry Age";
                                                         }
                                                         else{
                                        		 	echo "Best Def. Mgmt Fee";
                                        		 }
                                        	$counter++; }?></span> 
                                            <span class="glyphicon glyphicon-move" aria-hidden="true" style="color: #f5f5f5;"></span></li>
                                        <li class="provider-heading-logo"><div class="logo"><img src =<?php  if($records['user_image']){ echo "https://www.agedadvisor.nz/admin/files/user/".$records['user_image'];}else { echo "https://www.agedadvisor.nz/images/notlogo-pc-page.png"; } ?>  width="100%" height="auto"></div></li>
                                        <li class="provider-heading-name" style="min-height: 60px;">
                            <span><?php echo "(".$totalInCountry[0]." village";if($totalInCountry[0]>1){echo"s";} echo " in NZ)";?></span></br><strong>
                            <a href="<?php echo HOME_PATH. $records['quick_url']?>" target="_blank" title="<?php
                                             echo $records['title'];?>"><?php
                                             echo $records['title'];?></a>
                                             
                                         </strong></li>
                                         <li class="provider-heading-rating">
                                            <span class="start-box">
                                             <?php 
                                              //echo overAllRating($records['supplier_id']);
                                           if(simlilarFacilityRating($records['mypr_id'])==0)
                        {?>
                            Not Rated<div  style=" "><?php echo"(".$totalreview[0]." review";if($totalreview[0]!=1){echo's';}echo")";?></div>
                        <?php } 
                        else
                        {?>
                       <?php echo OverAllNEWRatingProduct($records['mypr_id']);?><div  style="margin-top:-25px;"><?php echo simlilarFacilityRating($records['mypr_id'])?>% <?php echo"(".$totalreview[0]." review";if($totalreview[0]!=1){echo's';}echo")";?></div>
                        <?php }  ?>       
                                            </span>
                                        </li>
                                       <li class="provider-main-minmum-age">
                                            <button type="button" class="btn enquire-btn" onclick="location.href='<?php echo HOME_PATH. $records['quick_url']?>#enquire';" >
                                                <a href="<?php echo HOME_PATH. $records['quick_url']?>#enquire" target="_blank">Enquire Now</a>
                                            </button>
                                        </li>
                                        <li class="provider-main-minmum-age" style="padding: 0px; min-height: 30px; line-height: 30px;"> <button type="button" value="<?php echo $records['mypr_id']?>" class="btn btn-info report-button">Print Report</button> </li>
                                        
                                         <li class="provider-heading-name" style="height:101px;">
                                         <strong>
                                         <?php if($records['fee_model']=='ORA / LTO'){
	                                         $decimalplaces=2;
	                                         if($records['deff_fee']*10 == ceil($records['deff_fee']*10)){$decimalplaces=1;}
	                                         if($records['deff_fee'] == ceil($records['deff_fee'])){$decimalplaces=0;}
                                         	echo $records['fee_model']." <div>".number_format($records['deff_fee'],$decimalplaces)."% per year, for ".$records['max_year']." years";
                                         	if($records['initial_value']==0 && $records['admin_fee_exit']==0){echo ". ";}
                                         	if($records['initial_value']>0){ echo" + ". $records['initial_value']."% upfront";}
                                         	if(!$records['admin_fee_exit'] && $records['initial_value']){echo ". ";}
                                         	if($records['admin_fee_exit']>0){echo" + ". $records['admin_fee_exit']."% on exit.";}
                                         	 
                                         	if($records['deff_fee']>0){echo "<br>Maximum ".ceil($records['deff_fee']*$records['max_year']+$records['initial_value']+$records['admin_fee_exit'])."%";}
                                         	echo "</div>";
                                         } else{
                                         	echo $records['fee_model'];
                                         }?></strong></li>
                                        
                                    </ul>
                                </li>
                                <li class="provider-list-main">
                                    <ul class="provider-main-list">
                                        <li class="provider-main-minmum-age">
                                        	<span class="m-pvd-heading m-show">Minimum Age</span>
                                            <span class="m-pvd-cont min-age"><?php if($records['min_age']>0){echo $records['min_age'];}else{echo "Undisclosed $collecting_data";} ?></span>
                                        </li>
                                        
                                    </ul>
                                </li>
                                <li class="provider-list-section">
                                    <ul class="provider-section-list">
                                        <li class="provider-section-age">
                                        	<span class="m-pvd-heading-full drop">
                                            	<span class="m-pvd-heading m-show">Max. Deferred Mgmt Fee</span>
												
                                                <span class="m-pvd-cont"><i class="fa fa-caret-right fa-caret-down m-show"></i>
                                                    <?php if($amount>0){?>
                                                        <strong>
                                                            <?php 
                                                            if($records['deff_fee']){$max_deff_fee= (($records['initial_value']/100)+($records['deff_fee']/100)*$records['max_year'])*$amount+($records['admin_fee_exit']/100*$amount);
                                                            echo "$".number_format($max_deff_fee);
                                                            ?>
                                                        </strong><div style="margin-top:-25px;">(based on <?php echo "$".number_format($amount);?>)</div><?php }else{echo '...</strong><div style="margin-top:-25px;">...</div>';}}else{?>Please Edit Estimate<div style="margin-top:-25px;">(using form above)</div>
                                                    <?php } ?>
                                                </span>
                                            </span>
                                            <div class="clearfix"></div>
                                            <ul class="provider-section-drop-list">
                                                <?php
                                                 for($year=1;$year<=$records['max_year']; $year++){
                                                    $max_deff_fee_by_year= (($records['initial_value']/100)+($records['deff_fee']/100)*$year)*$amount;
                                                    ?>        
                                                    <li>
                                                    	<span class="m-pvd-heading m-show"><?php echo $year."year"; ?></span>
                                                		<span class="m-pvd-cont"><?php echo "$".number_format($max_deff_fee_by_year); ?></span>
                                                    </li>
                                               <?php }
                                               if($records['max_year']<5){
                                               for($l=$records['max_year'];$l<5;$l++) { ?>
                                               <li class="m-hide"><?php echo "$".number_format($max_deff_fee_by_year); ?></li>
                                               <?php }}?>
                                            </ul>
                                        </li>
                                        
                                        <li> <span class="m-pvd-heading m-show">Lowest known price to purchase</span><strong><?php 
	                                        if($records['min_entry_poa']=='1'){?><a href="<?php echo HOME_PATH. $records['quick_url']?>#enquire" target="_blank">POA - Enquire Now</a><?php }else

	                                        
	                                        if($records['min_entry_value']>0){echo "$".number_format($records['min_entry_value']);}else{echo "Undisclosed $collecting_data";}?></strong><?php if($records['dwelling_low_price']){echo '<div style="margin-top:-25px;">'.get_services_name($records['dwelling_low_price']).'</div>';}else{echo '<div style="margin-top:-25px;">...</div>';}?></li>
                                        
                                        
                                        <li> <span class="m-pvd-heading m-show">Highest known price to purchase</span><strong><?php
	                                        if($records['max_entry_poa']=='1'){?><a href="<?php echo HOME_PATH. $records['quick_url']?>#enquire" target="_blank">POA - Enquire Now</a><?php } else
	                                        
	                                          if($records['max_entry_value']>0){echo "$".number_format($records['max_entry_value']);}else{echo "Undisclosed $collecting_data";} ?></strong><?php if($records['dwelling_high_price']){echo '<div style="margin-top:-25px;">'.get_services_name($records['dwelling_high_price']).'</div>';}else{echo '<div style="margin-top:-25px;">...</div>';}?></li>
                                         
                                        <li style="padding-top: 0px;"> 
                                            <ul class="provider-heading-list">
                                                
                                                <li class="provider-heading-name"  style="height: 65px;">
                                                    <strong>
                                                        
                                                        <a href="<?php echo HOME_PATH. $records['quick_url']?>" target="_blank" title="<?php
                                                        echo $records['title'];?>"><?php
                                                        echo $records['title'];?></a>

                                                    </strong><p>&nbsp;</p>
                                                </li>
                                            </ul>
                                        </li>
                                         
                                         <li class="provider-main-capital">
                                        	<span class="m-pvd-heading m-show">Capital Gain</span>
                                            <span class="m-pvd-cont capital">&nbsp;<?php echo $records['capital_gain']; ?>&nbsp;</span>
                                        </li>
                                         <li class="provider-main-capital">
                                        	<span class="m-pvd-heading m-show">Capital Loss (if any)</span>
                                            <span class="m-pvd-cont capital">&nbsp;<?php echo $records['capital_loss']; ?>&nbsp;</span>
                                        </li>

                                        <li class="provider-section-capital">
											<span class="m-pvd-heading-full drop">
                                            	<span class="m-pvd-heading m-show">Maintenance Fees from</span>
                                                <span class="m-pvd-cont"><i class="fa fa-caret-right fa-caret-down m-show"></i><?php
	                                             if($records['weekly_fee_poa']=='1'){?><strong><a href="<?php echo HOME_PATH. $records['quick_url']?>#enquire" target="_blank">POA - Enquire Now</a></strong><div style="margin-top:-25px;">&nbsp;</div><?php } else{
   
	                                                $fee_how_often=$records['fee_how_often'];
	                                                $fee=$records['weekly_fee'];$period='<div style="margin-top:-25px;">(Paid Weekly)</div>';
	                                                if($fee_how_often=='Fortnightly'){$fee=$records['weekly_fee']/2;$period='<div style="margin-top:-25px;">(Paid Fortnightly)</div>';}
	                                                if($fee_how_often=='Monthly'){$fee=$records['weekly_fee']*12/52;$period='<div style="margin-top:-25px;">(Paid Monthly)</div>';}
	                                                if($fee){
		                                                $decimalplaces=2;
														if($fee == ceil($fee)){$decimalplaces=0;}
		                                                echo "<strong>$".number_format($fee,$decimalplaces)." per week";?></strong><br><?php echo $period?><?php }else
		                                                {echo 'Undisclosed '.$collecting_data.'<br><div style="margin-top:-25px;">&nbsp;</div>';}
		                                                }
	                                                ?></span>
                                            </span>
                                            <div class="clearfix"></div>
                                            <ul class="provider-section-drop-list">
                                                <?php 
                                                	
                                                 if($records['fixed']==$records['fixed']){
                                                	 $fee=$records['convert_to_weekly'];
                                                	 $fees=52*$fee;
                                                	for($year=1; $year<2; $year++){
                                                		if($year==1){
                                                			$totalf=$fees;
                                                		}else{
                                                	   $fee = ($fees * ($cpi/100))+$fees;
                                                	    $fees=$fee;
                                                	    $totalf=$totalf+$fees;

                                                	}
                                                	   ?>
                                                	   <li>
                                                	<span class="m-pvd-heading m-show"><?php echo "Yearly Maintenance Cost";?></span>
                                                	<span class="m-pvd-cont"><?php if($totalf){echo "$".number_format($totalf, 2, '.', ',');}else{echo '&nbsp;';} ?></span>
                                                	
                                                	 </li>
                                                	
                                               <?php }
                                               if($records['max_year']=="OFF"){
                                               for($l=$records['max_year'];$l<2;$l++) { ?>
                                               <li class="m-hide"><?php echo "$".number_format($totalf, 2, '.', ','); ?></li>
                                               <?php }}
                                                }
                                                else
                                                	{
                                                		for($year=1; $year<2; $year++){
                                                	   ?>
                                                	   <li>
                                                	<span class="m-pvd-heading m-show"><?php echo $year." Year";?></span>
                                                	<span class="m-pvd-cont"><?php echo "---" ?></span>
                                                	 </li>
                                               <?php }
                                                } ?>
                                               
                                            </ul>
                                        </li>
                                        <li>
                                                    <span class="m-pvd-heading-full">
                                                        <span class="m-pvd-heading m-show">Any Maintenance Fee Increases?</span>
                                                        <span class="m-pvd-cont">&nbsp;<?php 
                                                        if($records['fixed']=='Undisclosed'){
                                                        	echo $records['fixed']." $collecting_data (CPI used)";} else{
                                                        		echo $records['fixed'];
                                                        	}?>&nbsp;</span>
                                                    </span>
                                                </li>
                                    </ul>
                                </li>
                                <li class="provider-list-footer">
                                    <ul class="provider-footer-list">
                                    <li class="provider-footer-facilties">
                                        	<span class="m-pvd-heading m-show">Extra Costs/Fees</span>
                                            <span class="m-pvd-cont">&nbsp;<?php echo $records['extra_fee']; ?></span>
                                        </li>
                                       
                                        <li class="provider-footer-facilties">
                                        	<span class="m-pvd-heading m-show">Number of dwellings in village</span>
                                            <span class="m-pvd-cont">&nbsp;<?php if($records['rooms']>0){echo $records['rooms'];}else{echo "Undisclosed $collecting_data";} ?></span>
                                        </li>
                                        
                                        <li class="provider-footer-star">
                                        	<span class="m-pvd-heading m-show">Data current as of:</span>
                                            <span class="m-pvd-cont">
                                                <span class="start-box">
                                                   <?php if($records['last_update']=='0000-00-00'){
                                              echo "Unknown"; 
                                           }else{
                                           	  echo date('j M, Y',strtotime($records['last_update']));
                                           }
                                        
                                         ?>
                                                </span>
                                            </span>
                                        </li>
                                         <li class="provider-footer-providers">
                                            <span class="m-pvd-heading m-show"> Data supplied by</span>
                                            <span class="m-pvd-cont">&nbsp;<?php echo $records['info_by']; ?></span>
                                        </li>
                                        <li class="provider-footer-providers">
                                            <span class="m-pvd-heading m-show"> Compare</span>
                                            <span class="m-pvd-cont">
                                                <input type="checkbox" id="comparison_shortlist<?php echo $records['mypr_id']?>" value="<?php echo $records['mypr_id']?>" class="compare_shortlist" <?php if(isset($_SESSION['session_compare_product'][$records['mypr_id']])){?> checked <?php }?>>
                                                Compare                                            
                                            </span>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <?php } ?>
                  
                </div>
            </div>
            <div>
                <h3>&nbsp;</h3>
            	<h5 class="alert pdtb-0" style="text-align:center;">Note: Figures shown are based on provider / resident feedback and are indicative only. They are not to be used as the primary basis for making any financial decisions. We recommend contacting any providers directly to get a copy of their latest disclosure statements.</h5>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>

            
<div id="myModal" class="modal fade" role="dialog">
  	<div class="modal-dialog">
    
    	<div class="modal-content blue-light-gradient" id="data">
        
            <div class="provider-compair-heading">
                <h2><i class="fa fa-pencil"></i>Discover Your Village Options</h2>
            </div>
                                                
            <div class="provider-compair-form">
                <form name="quote" method="post">
                  <div class="form-group"><strong>Select the $ range that you would consider spending to buy or licence to occupy a unit (eg. studio, apartment, villa, townhouse etc.) in a retirement village.</strong><br>
                    <!--<div class="input-group">
                      <div class="input-group-addon">$</div>-->
                    <select style="margin-top:10px;" class="form-control" name="amount">
                      <option value="0" <?php if($amount=="0"){echo "selected";}?>>Please select</option>
                      <option value="150000" <?php if($amount=="150000"){echo "selected";}?>>$0 - $150,000</option>
                      <option value="300000" <?php if($amount=="300000"){echo "selected";}?>>$150,001 - $300,000</option>
                      <option value="450000" <?php if($amount=="450000"){echo "selected";}?>>$300,001 - $450,000</option>
                      <option value="750000" <?php if($amount=="750000"){echo "selected";}?>>$450,001 - $750,000</option>
                      <option value="1000000" <?php if($amount=="1000000"){echo "selected";}?>>$750,001 - $1,000,000</option>
                      <option value="1500000" <?php if($amount=="1500000"){echo "selected";}?>>$1,000,001 - $1,500,000</option>    
                    </select>                   
                    <!--</div>--><span style="color: grey;font-size:12px;">You may leave this blank, but we will be unable to calculate any fees.</span>
                  </div>
                  <div class="form-group"><strong>Select the town / city that you're interested in.</strong>
                    <select style="margin-top:10px;"  class="form-control" name="city" id="select_city" required>
                       <option value="">Choose a City/Town</option>
                       <?php foreach ($cities as $value) { ?>
                         <option value="<?php echo $value['id'];?>" <?php if($value['id']==$city){echo "selected";}?>> <?php echo $value['title'];?></option>
                       <?php } ?>
                    </select>                   
                  </div>                     
                               
                  <div class="form-group" style="margin-bottom:-5px;"><strong>Select how would you like your results sorted?</strong>
                      <select style="margin-top:10px;"  class="form-control" name="sortby" id="sorting-providers-fancy" required>
                      
                      <option value="mostReviews"<?php if($sortby=="mostReviews"){echo "selected";}?>>Most Reviews</option>
                      <!--<option value="mostInfo"  <?php if($sortby=="mostInfo"){echo "selected";}?>>Most Info.</option>-->
                      <option value="star_rating"<?php if($sortby=="star_rating"){echo "selected";}?>>Highest Star Rating</option>
                      <option value="min_age_sort"<?php if($sortby=="min_age_sort"){echo "selected";}?>>Lowest Entry Age</option>

                      <!--<option value="most_facilities" <?php if($sortby=="most_facilities"){echo "selected";}?>>Most Facilities</option>-->
                      <option value="min_deff_sort" <?php if($sortby=="min_deff_sort"){echo "selected";}?>>Lowest Deferred Mgmt Fee</option>
                      <option value="max_deff_sort" <?php if($sortby=="max_deff_sort"){echo "selected";}?>>Highest Deferred Mgmt Fee</option>
                      <option value="weekly_fee_sort"<?php if($sortby=="weekly_fee_sort"){echo "selected";}?>>Lowest Maintenance Fee </option>
                      <option value="max_weekly_fee_sort"<?php if($sortby=="max_weekly_fee_sort"){echo "selected";}?>>Highest Maintenance Fee </option>
                      <option value="min_entry_sort"<?php if($sortby=="min_entry_sort"){echo "selected";}?>>Lowest Purchase Price</option>
                      <option value="max_entry_sort"<?php if($sortby=="max_entry_sort"){echo "selected";}?>>Highest Purchase Price</option>
                       <!--<option value="capital_gain"<?php if($sortby=="capital_gain"){echo "selected";}?>>Capital Gain</option>-->
                    </select>
                    
                    <br>
                    <span style="color: grey; display: none;">Note: Figures shown are based on provider / resident feedback and are indicative only. They are not to be used as the primary basis for making any financial decisions. We recommend contacting any providers directly to get a copy of their latest disclosure statements.</span>
                  </div>
               
                  
               <input type="checkbox" name="agree" value="yes" id="agreement"> I understand that any figures shown will be indicative only and should not be used as the primary basis for any decisions. <br>&nbsp;<br>
               <button type="submit"  name ="submit" class="btn btn-danger" disabled id="submit-fancy"><i class="fa fa-refresh"></i>Show Results</button>
                </form><div style='height:20px;'>&nbsp;</div>
               
            </div>
    	</div>

  	</div>
</div>
 
 <div id="mailModal" class="modal fade" role="dialog">
  	<div class="modal-dialog">
    
    	<div class="modal-content blue-light-gradient" id="data">
        
            <div class="provider-compair-heading">
                <h2><i class="fa fa-pencil"></i>Download your free trial report link for you to print.</h2>
            </div>
                                                
            <div class="provider-compair-form">                                   
                <div class="form-group" style="margin-bottom:-5px;"><strong>To ensure this information is not misused, report downloads are limited. Please enter your email address and we will send you a link to access and print the report.</strong>
                      <input type="email" placeholder="Please enter your e-mail" id="email-report">                   
                </div>               
                <input type="hidden" value="" name="mypr_id" id="report_mypr_id">               
                <button type="button"  name ="submit" class="btn btn-danger" id="submit-report" disabled><i class="fa fa-refresh"></i>Email me the Report Link</button>
                <button type="button" class="btn btn-info" data-dismiss="modal" style="margin-left:15px;" id="cancel-button">Cancel</button>
                <div style='height:20px;'>&nbsp;</div>
               
            </div>
    	</div>

  	</div>
</div>
         
<style>
.enquire-btn {    
    background-color: #f15922;
    border-color: #f12222;
}
.enquire-btn a {    
   color: #fff !important;
}
</style>

<script type="text/javascript">
	$(document).ready(function(){
            $('#email-report').on('keypress keydown blur keyup change mouseleave mouseenter',function() {
                if(!$.trim(this.value).length) { // zero-length string AFTER a trim
                    $("#submit-report").attr('disabled', 'disabled');
                }else{
                    $("#submit-report").removeAttr('disabled');
                }                     
           });
           
            $('#submit-report').click(function(){ 
                var page_id = $(this).parent().find("#report_mypr_id").val();
                var emailr = $(this).parent().find("#email-report").val();
                $.ajax({
                    type: 'POST',
                    url: '../../includes/ajax.php',
                    data: {action: 'mailReport', page_id : page_id, emailr: emailr},
                    success: function(response) {
                        alert(response);
                        $('#cancel-button').trigger('click');
                    }
                });
            });
            
            $(".report-button").click(function(){
                $("#report_mypr_id").val($(this).val());
                $('#mailModal').modal({
                        backdrop: 'static',
                        keyboard: true
                });
            })
            
            $('.compare_shortlist').click(function(){
                if ($('.compare_shortlist:checkbox:checked').length > 0)
                {
                    $('.reset-shortlist').show();
                    $('#reset-shortlistb').show();
                }
                else
                {
                   $('.reset-shortlist').hide();
                   $('#reset-shortlistb').hide();
                }
            })
            
            $('#show-shortlist').click(function(){
                $("#short-div").hide();
                $("#showall-div").show();
                $('.provider-slider-box').css({
                        left: 0
                });
                $('.provider-slide').hide();                
                $('#show-shortlistb').hide();                
                $('#hide-shortlistb').show();
                $('.compare_shortlist:checkbox:checked').each(function () {                
                    $(this).parent().parent().parent().parent().parent().parent().parent().show();
                });
            });
            
            $('#hide-shortlist').click(function(){
                $("#short-div").show();
                $("#showall-div").hide();
                $('.provider-slide').show();                
                $('#hide-shortlistb').hide();                
                $('#show-shortlistb').show();
            });
            
            $('#show-shortlistb').click(function(){
                $('.provider-slider-box').css({
                    left: 0
                });
                $('.provider-slide').hide();
                $(this).hide();
                $("#short-div").hide();
                $("#showall-div").show();                    
                $('#show-shortlistb').hide();
                $('#hide-shortlistb').show();                        
                $('.compare_shortlist:checkbox:checked').each(function () {                          
                    $(this).parent().parent().parent().parent().parent().parent().parent().show();
                });
            });
            
            $('#hide-shortlistb').click(function(){
                $('.provider-slide').show();
                $(this).hide();
                $("#short-div").show();
                $("#showall-div").hide();                               
                $('#hide-shortlistb').hide();                
                $('#show-shortlistb').show();
            });
            
            setTimeout(function () {
                $('#main-content-loading').slideDown(300);                 
            }, 3000);
            setTimeout(function () {
                $('#no-results').slideDown(300);
            }, 3000);
            setTimeout(function () {
                $('#loading-gif').slideUp();
            }, 2500);
            $('#agreement').click(function() {
                if ($(this).is(':checked')) {
                    $('#submit-fancy').removeAttr('disabled');
                } else {
                    $('#submit-fancy').attr('disabled', 'disabled');
                }
            });
            
            var sorted = $('#sorting-providers-sidebar').val();
            var city = $('#select_city').val();
            
            if((city == "" || sorted == "")){
               
                $('#myModal').modal({
                        backdrop: 'static',
                        keyboard: false
                });
            }
            
            $("#select_city").change(function (){
                var id=$(this).val();
                
                if(id==''){
                    $("#suburb").prop('disabled', true);
                }
                else{
                    $("#suburb").prop('disabled', false);
                }
            });
	});
</script>

<script>
fbq('track', 'ComparisonPage', {
<city>: <<?php echo $city?>>,
<amount>: <<?php echo $amount?>>,
<sortby>: <<?php echo $sortby?>>
});
</script>