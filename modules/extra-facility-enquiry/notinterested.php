<?php
include( "../../application_top.php" );
global $db;

$lat = $_GET['lat'];

$long = $_GET['long'];

$fAddress_suburb = $_GET['sub'];

$fAddress_city = $_GET['city'];

$fZip = $_GET['zip'];

$pr_id = $_GET['pid'];

$user_id = $_GET['uid'];

$userQuery = "SELECT `id`, `first_name`,`last_name`,`email`,`phone` FROM `ad_users` WHERE md5(id)='$user_id'";
$userQueryRes = $db->sql_query($userQuery);
$userData = $db->sql_fetchrowset($userQueryRes);

foreach($userData as $user){
    $user_id    = $user['id'];
    $first_name = $user['first_name'];
    $last_name  = $user['last_name'];
    $useremail  = $user['email'];            
    $userphone  = $user['phone'];
    $username   = $first_name.' '.$last_name; 
}

$addressQuery ="SELECT `id`,`certification_service_type`,`facility_type`,`address_suburb`,`address_city`,`zip`, `latitude`, `longitude`, `facility_link` FROM `ad_products` "
        . "WHERE md5(id)='$pr_id'";
    
    $addressQueryRes = $db->sql_query($addressQuery);
    $addressData = $db->sql_fetchrowset($addressQueryRes);
    
    foreach($addressData as $addData){
        $pr_id                      = $addData['id'];
        $fCertification_service_type= $addData['certification_service_type'];
        $fFacility_type             = $addData['facility_type'];
        $fAddress_suburb            = $addData['address_suburb'];
        $fAddress_city              = $addData['address_city'];
        $fZip                       = $addData['zip'];
        $other_facility_link        = $addData['facility_link'];
        $lat                        = $addData['latitude'];
        $long                       = $addData['longitude'];        
    }

             
    $similarFacilityQuery = 'SELECT gall.file, fd.feedback, COUNT(fd.feedback) as count_feedback , pr.id, pr.quick_url,pr.title, pr.address_suburb, '
            . 'pr.address_city, fd.overall_rating,gall.file,gall.type FROM `ad_products` AS pr LEFT JOIN `ad_feedbacks` AS fd ON pr.id = fd.product_id '
            . 'LEFT JOIN `ad_galleries` as gall ON pr.id = gall.pro_id WHERE (`address_suburb` = "'.$fAddress_suburb.'" OR '
            . '`address_city` = "'.$fAddress_city.'" OR `zip` = "'.$fZip.'") AND  gall.type=0 AND gall.file '
            . 'IS NOT NULL AND prod.id NOT IN($pr_id) AND pr.deleted=0 GROUP BY pr.id HAVING count_feedback >= 3 ORDER BY fd.overall_rating DESC LIMIT 5';

    $similarFacilityQueryResult = mysqli_query($db->db_connect_id, $similarFacilityQuery);
    if(mysqli_num_rows($similarFacilityQueryResult) <= 0){
    $similarFacilityQuery="SELECT gall.file, fd.feedback, COUNT(fd.feedback) as count_feedback , pr.id, pr.quick_url,pr.title, pr.address_suburb, "
            . "pr.address_city,pr.deleted,pr.latitude, pr.longitude, SQRT(POW(69.1 * (pr.latitude - ('$lat')), 2) + POW(69.1 * (('$long') - pr.longitude) * COS(pr.latitude / 57.3), 2)) "
            . "AS `distance`, fd.overall_rating,gall.file,gall.type FROM `ad_products` AS pr LEFT JOIN `ad_feedbacks` AS fd ON pr.id = fd.product_id "
            . "LEFT JOIN `ad_galleries` as gall ON pr.id = gall.pro_id GROUP BY pr.id HAVING distance < 500 and distance > 0  AND count_feedback >= 3 "
            . "AND pr.deleted = 0 AND gall.type=0 AND gall.file IS NOT NULL ORDER BY distance LIMIT 5";
    }
    
    $similarFacilityQueryresL = $db->sql_query($similarFacilityQuery);  
    $similarFacility = $db->sql_fetchrowset($similarFacilityQueryresL);
    
    if(count($similarFacility>1)){?>                                  
    <div class="right-photo-gallery mt-30">
        <div class="headline">
            <a name="images"></a>
            <h4>More facilities in same area...</h4>
        </div>
        <ul class="facilities-list img-rtng">
    <?php if(count($similarFacility)){
        foreach($similarFacility as $sFac){
            $fID = $sFac['id'];
            $fQuick_url = $sFac['quick_url'];
            $fTitle = $sFac['title'];

            if(isset($sFac['file']) && !empty($sFac['file'])){
                $fImageSet = $_SERVER['DOCUMENT_ROOT'].'/admin/files/gallery/images/'.$sFac['file'];            
                $fImage = $sFac['file'];
            }
            $fSuburb = $sFac['address_suburb'];
            $fCity = $sFac['address_city'];
            $fOverall_rating = simlilarFacilityRating($fID);
            if (file_exists($fImageSet)) {
            $similarFacilityHtml = '<li>
                <img src="'.HOME_PATH. 'admin/files/gallery/images/'.$fImage.'" alt="" title="" />
                <div class="facility-rating">

                    <a href="'.HOME_PATH. $fQuick_url.'"><span class="prcnt-icon">'.round($fOverall_rating).'%</span>'.$fTitle.'<br/>
                    <span>'.$fSuburb.', '.$fCity.'</span>
                    <div class="star-inactive">
                        <div class="star-active" style="width:'.$fOverall_rating.'%;"></div>
                    </div>                    
                    </a>
                    <div>
                        <button type="button" class="open btn btn-danger" style="display: inline-block; float:right">Click to contact Facility</button>
                        <input type="hidden" class="pid" value="'.$fID.'">
                    </div>
                </div> 
            </li>';               
            }else { $similarFacilityHtml = ''; }            
            echo $similarFacilityHtml;      
        }    
    } 
}     
?>
<div class="dashboard_right_col">                
    <div id="userdetails" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content blue-light-gradient">             
                <div class="provider-compair-heading">
                    <h2><i class="fa fa-hand-o-right" style="margin-right: 30px;"></i>Please Confirm the contact details <i class="fa fa-times-circle" aria-hidden="true"style="float: right; cursor: pointer;" data-dismiss="modal"></i></h2>
                </div>
                <div class="provider-compair-form">
                    <form name="restartEnquiry" id="restartEnquiry">
                        <div class="form-group">
                            <label class="col-sm-2 col-form-label" style="margin-bottom: 15px;">Name</label>
                            <div class="col-sm-10" style="margin-bottom: 15px;">
                                <input type="text" value="<?php echo $username;?>" class="form-control" id="enquirer_name" name="enquirer_name" placeholder="Name" required="">
                            </div>                        
                        </div>                                         
                        <div class="form-group">
                            <label class="col-sm-2 col-form-label" style="margin-bottom: 15px;">Email</label>
                            <div class="col-sm-10" style="margin-bottom: 15px;">
                                <input type="text" value="<?php echo $useremail;?>" class="form-control" id="enquirer_email" name="enquirer_email" placeholder="Email" required="">
                            </div>
                        </div>                                       
                        <div class="form-group">                                    
                            <label class="col-sm-2 col-form-label" style="margin-bottom: 15px;">Phone</label>
                            <div class="col-sm-10" style="margin-bottom: 15px;">
                                <input type="text" value="<?php echo $userphone;?>" class="form-control" id="enquirer_tel" name="enquirer_tel" placeholder="Phone" required="">
                            </div>
                        </div>
                        <div class="form-group">                                    
                            <label class="col-sm-2 col-form-label" style="margin-top: 15px;">Message</label>
                            <div class="col-sm-10" style="margin-bottom: 15px;">
                            <textarea style="margin-bottom: 0px;" rows="4" cols="38" id="enq_specifics" name="enq_specifics" placeholder="Enquiry Specifics (if any)"></textarea>
                            </div>
                        </div>
                        <div class="form-group">                        
                            <button type="submit" class="btn btn-danger" style="width:112px;">Submit </button>
                            <input type="hidden" name="enquirer_id" id="enquirer_id" value="<?php echo $user_id;?>">
                            <input type="hidden" name="prod_id" id="prod_id" value="<?php echo $pr_id;?>">
                        </div>                                                            
                        <div style='height:0px;'>&nbsp;</div>
                    </form>
                </div>
            </div>
        </div>
    </div>                
</div>
            
<script>
    $(document).ready(function(){        
        $('.open').on('click',function(){
            $("#prod_id").val($(this).parent().find(".pid").val());
            setTimeout(function(){
                $('#userdetails').modal({
                    backdrop: 'static',
                    keyboard: false
                });
             },35);              
        });
        
        $("#restartEnquiry").submit(function(event){                                                      
            $.ajax({
                type: "post",
                url: '<?php echo  HOME_PATH ?>/modules/search/ajax_vacancy.php', 
                data: $("#restartEnquiry").serialize(),
                success: function(result)  
                {  console.log(result) }
            });
        event.preventDefault();
    });
})
</script>
  