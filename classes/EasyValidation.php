<?php
/*
 * Author: Marvin Petker
 * Version: 1.0
 * Date: 2014-09-23
 * License: GNU General Public License 3.0
 * License URL: http://www.gnu.org/licenses/gpl-3.0.txt
 * 
 * Description: This class is used for easy validation of input data.
 * 
 * Note: The Idea for that class came from phpacademy.org and their youtube channel.
 * I've changed some things but I recommend you to watch their tutorials because they're very good and
 * can give beginners an understanding of oop and other stuff.
 * 
 */

class EasyValidation {
    
    //This Array will hold all Error messages supplied to the constructor
    protected $_errorMessages;
    
    //This Array hold all occured errors during validation
    protected $_errors;
    
    /*
     * Constructor
     * @Param1: Array $errorMessages
     * @Return: void;
     */
    public function __construct(Array $errorMessages){
        $this->_errorMessages = $errorMessages;
    }
    
    /*
     * Method to invoke for checking given values with given rules
     * @Param1: Array $data; Data to validate.
     * @Param2: Array $rules; Specified rules for $data.
     * @Return: void
     */
    public function check(Array $data, Array $rules){
        foreach( $data as $field => $value ){
            if( empty($rules[$field]) ){
                continue;
            }
            $this->validate($field, $value, $rules[$field]);
        }
    }
    
    /*
     * This function invokes the validation methods and call the errorHandler
     * method on failure.
     * @Param1: string $field; Name of the input field
     * @Param2: mixed $value; The value of the input field
     * @Param3: array $rules; All rules that apply to given $field
     * @Return: void
     */
    protected function validate($field, $value, Array $rules){
        foreach( $rules as $rule => $satisfier ){
           if( !method_exists($this, $rule) ){
               throw new Exception("Unknown rule: {$rule}.");
           }
           if( $rule == "validate" || $rule == "check" ){
               throw new Exception("Rule {$rule} is not allowed.");
           }
           
           if(is_bool($satisfier) ){
               $params = array($value);
           }
           else{
               $params = array($value, $satisfier);
           }
           
           if( !call_user_func_array(array($this, $rule), $params) ){
                $this->handleErrors($field, $rule, $satisfier);
           }
        }
    }
    
    /*
    * HandleErrors adds error messages to the _errors array
     * @Param1: string $field; Name of the input field
     * @Param2: string $rule; The rule that creates the message
     * @Param3: mixed $value; The satisfier of the $rule.
     * @Return: void;
     */
    protected function handleErrors($field, $rule, $value){
        foreach( $this->_errorMessages as $n => $msgs ){
            if( $this->pcre($n, "/^\#[a-zA-Z0-9]+$/") && ltrim($n, "#") == $field){
                if( !empty($msgs[$rule]) ){
                    $msg = $msgs[$rule];
                    break;
                }
            }
            else{
                if( empty($this->_errorMessages[$rule]) ){
                    throw new Exception("Error message for rule: {$rule} is not given.");
                }
                $msg = $this->_errorMessages[$rule];
            }
        }
        
        if( empty($this->_errors[$field]) || !is_array($this->_errors[$field]) ){
            $this->_errors[$field] = array();
        }
        $msg = str_replace(array(":field", ":value"), array($field, $value), $msg);
        $this->_errors[$field][$rule] = $msg;
    }
    
    /*
    * Get all error messages
     * @Param: void;
     * @Return Array $this->_errors;
     */
    public function getErrors(){
        return $this->_errors;
    }
    
    /*
     * Adds an error message
     * @Param1: string $rule, The rule the message applies to.
     * @Param2: string $msg, The message. Can contain :field and :value placeholders
     */
    public function addErrorMsg($rule, $msg){
        $this->_errorMessages[$rule] = $msg;
    }
    
    /*
     * VALIDATION METHODS
     * Each method represents a possible rule.
     * On checks for types etc. no second parameter is needed.
     * But if your satisfier is diffrent to (bool)true you need a second argument.
     */
    
    protected function required($value){
        return !empty($value);
    }
    
    protected function numeric($value){
        return is_numeric($value);
    }
    
    protected function alnum($value){
        return ctype_alnum($value);
    }
    
    protected function alpha($value){
        return ctype_alpha($value);
    }
    
    protected function digit($value){
        return ctype_digit($value);
    }
    
    protected function int($value){
        return is_int($value);
    }
    
    protected function float($value){
        return is_float($value);
    }
    
    protected function string($value){
        return is_string($value);
    }
    
    protected function email($value){
        return filter_var($value, FILTER_VALIDATE_EMAIL);
    }
    
    protected function pcre($value, $pattern){
        return preg_match($pattern, $value);
    }
    
    protected function maxLength($value, $length){
        return strlen($value) < $length;
    }
    
    protected function minLength($value, $length){
        return strlen($value) > $length;
    }
    
}
