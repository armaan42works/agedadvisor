<?php

    /*     * *************************************************************************
     *                         mainClass.class.php
     *                            -------------------
     *   begin                : Saturday, July 22, 2014
     *   copyright            : (C) 2014 The Ilmp Tech pvt.ltd
     *   email                : dev@ilmp-tech.com
     *
     * ************************************************************************* */

    /*     * *************************************************************************
     *
     *   This class responsible to manage all the operations
     *
     * ************************************************************************* */
    global $db;
    class mainClass extends sql_db {

        public function insert($table,$sql_qry) {
            $db = new sql_db(HOST, DB_USER_NAME, DB_PASSWORD, DB_NAME);
            
            try {
                $val_v = '';
                $key_v = "";
                if (count($sql_qry) > 0) {
                    foreach ($sql_qry as $key => $val) {
                        $key_v.="," . $key;
                        $val_v.=",'" . mysqli_real_escape_string($db-db_connect_id,htmlspecialchars_decode(htmlentities($val, ENT_QUOTES), ENT_QUOTES)) . "'";
                    }
                    $sql = "insert into " . $table . "  (" . substr($key_v, 1) . ")" . "  values (" . substr($val_v, 1) . ")";
                }
                if ($db->sql_query($sql)) {
                    return true;
                } else {
                    die(mysqli_error('There is an error in query'));
                }
            } catch (Exception $ex) {
                echo "Some Exception Occured " . $ex;
            }
        }
        
        
        public  function redirect($url){
	    header("location:".$url);
	 }

    }
    

    // class sql_db

    if (!isset($main_obj)) {
        $main_obj = new mainClass($DBHOST,$DBNAME,$UNAME,$DBPASSWORD);
    }
?>