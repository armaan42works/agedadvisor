<?php
if (!defined("SQL_LAYER")) {

    define("SQL_LAYER", "mysqli");

    class sql_db {

        var $db_connect_id;
        var $query_result;
        var $row = array();
        var $rowset = array();
        var $num_queries = 0;

        function __construct($sqlserver, $sqluser, $sqlpassword, $database, $persistency = true) {
            $this->persistency = $persistency;
            $this->user = $sqluser;
            $this->password = $sqlpassword;
            $this->server = $sqlserver;
            $this->dbname = $database;

            $this->db_connect_id = ($this->persistency) ? @mysqli_connect("p:" . $this->server, $this->user, $this->password) : @mysqli_connect($this->server, $this->user, $this->password);

            if ($this->db_connect_id && $this->dbname != '') {
                if (@mysqli_select_db($this->db_connect_id, $this->dbname)) {
                    return $this->db_connect_id;
                }
            }
        }

        function sql_close() {
            if ($this->db_connect_id) {
                if ($this->query_result) {
                    @mysqli_free_result($this->query_result);
                }
                $result = @mysqli_close($this->db_connect_id);
                return $result;
            } else {
                return false;
            }
        }

        function sql_query($query = "", $transaction = FALSE) {
            // Remove any pre-existing queries
            unset($this->query_result);
            if ($query != "") {
                if (DEBUG_QUERY === true)
                    echo '<br>' . $query . '<br>';
                $time = microtime(true);
                $this->query_result = mysqli_query($this->db_connect_id, $query);
                $totaltime = microtime(true) - $time;
                if ($totaltime > 1) {
                    error_log("Slow Query: $totaltime" . "s $query");
                }
            }
            if ($this->query_result) {
                unset($this->row[$this->query_result]);
                unset($this->rowset[$this->query_result]);
                return $this->query_result;
            } else {
                return ( $transaction == END_TRANSACTION ) ? true : false;
            }
        }

        function sql_numrows($query_id = 0) {
            if (!$query_id) {
                $query_id = $this->query_result;
            }
            if ($query_id) {
                $result = @mysqli_num_rows($query_id);
                return $result;
            } else {
                return false;
            }
        }

        function sql_affectedrows() {
            if ($this->db_connect_id) {
                $result = @mysqli_affected_rows($this->db_connect_id);
                return $result;
            } else {
                return false;
            }
        }

        function sql_numfields($query_id = 0) {
            if (!$query_id) {
                $query_id = $this->query_result;
            }
            if ($query_id) {
                $result = @mysqli_num_fields($query_id);
                return $result;
            } else {
                return false;
            }
        }

        function sql_fieldname($offset, $query_id = 0) {
            if (!$query_id) {
                $query_id = $this->query_result;
            }
            if ($query_id) {
                $result = @mysqli_field_name($query_id, $offset);
                return $result;
            } else {
                return false;
            }
        }

        function sql_fieldtype($offset, $query_id = 0) {
            if (!$query_id) {
                $query_id = $this->query_result;
            }
            if ($query_id) {
                $result = @mysqli_field_type($query_id, $offset);
                return $result;
            } else {
                return false;
            }
        }

        function sql_fetchrow($query_id = 0) {

            if (!$query_id) {
                $query_id = $this->query_result;
            }
            if ($query_id) {
                $id = (int) $query_id;
                $this->row[$id] = @mysqli_fetch_array($query_id);
                return $this->row[$id];
            } else {
                return false;
            }
        }

        function sql_fetchrowset($query_id = 0) {
            if (!$query_id) {
                $query_id = $this->query_result;
            }
            if ($query_id) {
                $id = (int) $query_id;

                unset($this->rowset[$id]);
                unset($this->row[$id]);
                while ($this->rowset[$id] = @mysqli_fetch_array($query_id)) {
                    $result[] = $this->rowset[$id];
                }
                if (isset($result) && count($result) > 0) {
                    return $result;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }

        function sql_fetchfield($field, $rownum = -1, $query_id = 0) {
            if (!$query_id) {
                $query_id = $this->query_result;
            }
            if ($query_id) {
                if ($rownum > -1) {
                    $result = @mysqli_result($query_id, $rownum, $field);
                } else {
                    if (empty($this->row[$query_id]) && empty($this->rowset[$query_id])) {
                        if ($this->sql_fetchrow()) {
                            $result = $this->row[$query_id][$field];
                        }
                    } else {
                        if ($this->rowset[$query_id]) {
                            $result = $this->rowset[$query_id][$field];
                        } else if ($this->row[$query_id]) {
                            $result = $this->row[$query_id][$field];
                        }
                    }
                }
                return $result;
            } else {
                return false;
            }
        }

        function sql_rowseek($rownum, $query_id = 0) {
            if (!$query_id) {
                $query_id = $this->query_result;
            }
            if ($query_id) {
                $result = @mysqli_data_seek($query_id, $rownum);
                return $result;
            } else {
                return false;
            }
        }

        function sql_nextid() {
            if ($this->db_connect_id) {
                $result = @mysqli_insert_id($this->db_connect_id);
                return $result;
            } else {
                return false;
            }
        }

        function sql_freeresult($query_id = 0) {
            if (!$query_id) {
                $query_id = $this->query_result;
            }

            if ($query_id) {
                unset($this->row[$query_id]);
                unset($this->rowset[$query_id]);

                @mysqli_free_result($query_id);

                return true;
            } else {
                return false;
            }
        }

        function sql_error($query_id = 0) {
            $result["message"] = @mysqli_error($this->db_connect_id);
            $result["code"] = @mysqli_errno($this->db_connect_id);

            return $result;
        }

        public function insert($table, $sql_qry) {            
            try {
                $val_v = '';
                $key_v = "";
                if (count($sql_qry) > 0) {
                    foreach ($sql_qry as $key => $val) {
                        $key_v .= "," . $key;
                        $val_v .= ",'" . mysqli_real_escape_string($this->db_connect_id,(htmlspecialchars_decode(htmlentities($val, ENT_QUOTES), ENT_QUOTES))) . "'";
                    }
                    $sql = "insert into " . $table . "  (" . substr($key_v, 1) . ")" . "  values (" . substr($val_v, 1) . ")";                    
                }                
                if (@mysqli_query($this->db_connect_id,$sql)) {
                    return true;
                } else {
                    die(mysqli_error('There is an error in query'));
                }
            } catch (Exception $ex) {
                echo "Some Exception Occured " . $ex;
            }
        }

        public function delete($table, $sql_qry, $where = '') {     
            try {
                $val_v = '';
                $key_v = "";
                if (count($sql_qry) > 0) {
                    foreach ($sql_qry as $key => $val) {
                        $key_v .= ", " . $key . "='" . $val . "' ";
                    }
                    $sql = "delete from " . $table . "" . $where;
                    if (@mysqli_query($this->db_connect_id,$sql)) {
                        return true;
                    } else {
                        die(mysqli_error("There is an error in query"));
                    }
                }
            } catch (Exception $ex) {
                echo "Some Exception Occured " . $ex;
            }
        }

        public function fetch_all($field, $table, $where = '', $limit = '', $order = '') {
            try {
                ////  mysql  fetch  Query
                $num_res = "select " . $field . " from " . $table . " " . $where . " " . $order;

                $total = mysqli_query($num_res);


                $total_num = mysqli_num_rows($total);
                $this->total_record = $total_num;
                $sql = "select " . $field . " from " . $table . " " . $where . " " . $order . " " . $limit;

                $pro = mysqli_query($sql);
                if ($pro) {
                    return true;
                } else {
                    return false;
                }
            } catch (Exception $ex) {
                echo "Some Exception Occured" . $ex;
            }
        }

        public function update($table, $sql_qry, $where = '') {
            ////  mysql  insert Query
            $val_v = '';
            $key_v = "";
            if (count($sql_qry) > 0) {
                foreach ($sql_qry as $key => $val) {
                    $key_v .= ", " . $key . "='" . mysqli_real_escape_string($this->db_connect_id,(htmlspecialchars_decode(htmlentities($val, ENT_QUOTES), ENT_QUOTES))) . "' ";
                }
                $sql = "update " . $table . " set  " . substr($key_v, 1) . " " . " " . $where;               
                if (@mysqli_query($this->db_connect_id,$sql)) {
                    return true;
                } else {
                    die(mysqli_error('There is an error in query'));
                }
            }
        }

        public function last_id() {
            if ($id = @mysqli_insert_id($this->db_connect_id)) {
                return $id;
            } else {
                return false;
            }
        }

    }

}
?>