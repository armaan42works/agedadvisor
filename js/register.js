/*
 * Objective: JS for register page
 * filename : register.js
 * Created By: Sanket Khanna <sanket.khanna@ilmp-tech.com>
 * Created On : 01 September 2014
 */

$(document).ready(function() {


    $('.refferal').hide();

    $('.sp_reffer').click(function() {
        var refferType = $(this).val();
        if (parseInt(refferType) == 1) {
            $('.refferal').show();
            $('.cp_sales_id').hide();
            $('#cp_sales_id').val('');
            $('#cp_sales_id').removeClass('required');
            $('#cp_client_id').addClass('required');
        } else {
            $('.refferal').hide();
            $('.cp_sales_id').show();
            $('#cp_sales_id').addClass('required');
            $('#cp_client_id').val('');
            $('#cp_client_id').removeClass('required');
        }
    });

    $('.cp_reffer').click(function() {
        var refferType = $(this).val();
        if (parseInt(refferType) == 3) {
            $('.cp_client_id').hide();
            $('#cp_client_id').val('');
            $('#cp_client_id').removeClass('required');
        } else {
            $('.cp_client_id').show();
            $('#cp_client_id').addClass('required');
        }
    });
});
$(document).ready(function() {
    $('#refresh-captcha').on('click', function() {
        $('#captcha').attr("src", "admin/captcha/captcha.php?rnd=" + Math.random());
    })
    $('#clientForm').validate({
        rules: {
            "captcha": {
                "required": true,
                "remote":
                        {
                            url: 'admin/captcha/checkCaptcha.php',
                            type: "post",
                            data:
                                    {
                                        code: function()
                                        {
                                            return $('#captcha-text').val();
                                        }
                                    }
                        }
            },
            "c_email": {
                "required": true,
                "email": true,
                "remote": {
                    url: 'admin/ajax.php',
                    type: 'POST',
                    data: {action: "unquieEmail", email: function() {
                            return $('#c_email').val();
                        }
                    }
                }
            }, "cp_sales_id": {
                "remote": {
                    url: 'admin/ajax.php',
                    type: 'POST',
                    data: {action: "validSalesId"
                    }
                }
            }, "cp_client_id": {
                "remote": {
                    url: 'admin/ajax.php',
                    type: 'POST',
                    data: {action: "validClientId"
                    }
                }
            }
        },
        messages: {
            "captcha": {
                "required": "Please enter the verifcation code.",
                "remote": "Verication code incorrect, please try again."
            },
            "c_email": {
                "required": "This field is required.",
                "email": "Please enter a valid email address.",
                "remote": "Email Id already exists."
            },
            "cp_sales_id": {
                "remote": "Referral Id is not valid."
            },
            "cp_client_id": {
                "remote": "Referral Id is not valid."
            }
        }
    });
    if ($('#c_country').val() == 229) {
        $('input[name="c_zipcode"]').rules('remove');
        $('input[name="c_zipcode"]').rules('add', {
            digits: true,
            required: true,
            maxlength: 5
        });
    }

    $('#c_country').on('change', function() {
        var coun_id = $(this).val();
        if (coun_id == 229) {
            $('input[name="c_zipcode"]').rules('remove');
            $('input[name="c_zipcode"]').rules('add', {
                digits: true,
                required: true,
                maxlength: 5
            });
        } else {
            $('input[name="c_zipcode"]').rules('remove');
            $('input[name="c_zipcode"]').rules('add', {
                required: true
            });
        }
    });
});
