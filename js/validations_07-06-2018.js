$(document).ready(function() {
    $('#embankment').blur(function() {
        alert('here');
    });
});
$(document).ready(function() {
    /*
     *  Order for Services
     */
    $('.order').live('click', function() {
        var split = $(this).attr('id').split('-');

        var id = split['1'];
        var order = split['0'];
        if ($(this).hasClass('down')) {
            var type = 'down';
        } else {
            type = 'up';
        }
        $.ajax({
            url: 'ajax.php',
            type: 'POST',
            data: {action: 'order', id: id, order: order, type: type},
            success: function(response) {
                window.location.reload();
            }
        });
    });
    /*
     *  Order for Category
     */
    $('.orderCat').live('click', function() {
        var split = $(this).attr('id').split('-');

        var id = split['1'];
        var order = split['0'];
        if ($(this).hasClass('down')) {
            var type = 'down';
        } else {
            type = 'up';
        }
        $.ajax({
            url: 'ajax.php',
            type: 'POST',
            data: {action: 'orderCat', id: id, order: order, type: type},
            success: function(response) {
                window.location.reload();
            }
        });
    });
    /*
     * Delete Services and  reset the ordering
     */
    $('.delete-service').live('click', function() {
        var sure = confirm('Are you sure to delete?');
        if (sure) {
            var split = $(this).attr('id').split('-');
            var rec_id = split['1'];
            var order = split['0'];
            $.ajax({
                type: 'POST',
                url: 'ajax.php',
                data: {action: 'deleteService', id: rec_id, order: order},
                success: function() {
                    window.location.reload();
                }
            });
        }


    });
    /*
     * Delete Services and  reset the ordering
     */
    $('.delete-categories').live('click', function() {
        var sure = confirm('Are you sure to delete?');
        if (sure) {
            var split = $(this).attr('id').split('-');
            var rec_id = split['1'];
            var order = split['0'];
            $.ajax({
                type: 'POST',
                url: 'ajax.php',
                data: {action: 'deleteCatogory', id: rec_id, order: order},
                success: function() {
                    window.location.reload();
                }
            });
        }


    });
    /*
     * Paid/ not Paid
     */

    $('.payment').on('change', function() {
        alert("hi");
        var details = $(this).attr('id').split('-');
        var table = details[0];
        var id = details[1];
        var paid = details[2];
        $.ajax({
            type: 'POST',
            url: 'ajax.php',
            data: {action: 'paid', id: id, table: table, paid: paid},
            success: function(response) {
                $('#paid-' + id).html(response);
            }
        });
    });
    /*
     * Code for status change
     */

    $('.status').live('click', function() {
        var details = $(this).attr('id').split('-');
        var table = details[0];
        var id = details[1];
        var status = details[2];
        $.ajax({
            type: 'POST',
            url: 'ajax.php',
            data: {action: 'status', id: id, table: table, status: status},
            success: function(response) {
                $('.status-' + id).html($.trim(response));
            }
        });
    });
 
    $('.abuse').live('click', function() {
        var details = $(this).attr('id').split('-');
        var table = details[0];
        var id = details[1];
        var abuse = details[2];
        alert(id);die;
        $.ajax({
            type: 'POST',
            url: 'ajax.php',
            data: {action: 'abuse', id: id, table: table, abuse: disabuse},
            success: function(response) {
                $('.abuse-' + id).html($.trim(response));
            }
        });
    });


    $('.approval').live('click', function() {
        var details = $(this).attr('id').split('--');
        var table = details[0];
        var id = details[1];
        var approval = details[2];
        if (approval == 0) {
            $.ajax({
                type: 'POST',
                url: 'ajax.php',
                data: {action: 'approval', id: id, table: table, approval: approval},
                success: function(response) {
                    $('.approval-' + id).html($.trim(response));
                }
            });
        } else
            return;
    });
    $('.verified').live('click', function() {
        var details = $(this).attr('id').split('--');
        var table = details[0];
        var id = details[1];
        var verified = details[2];
        if (verified == 0) {
            $.ajax({
                type: 'POST',
                url: 'ajax.php',
                data: {action: 'verified', id: id, table: table, verified: verified},
                success: function(response) {
                    $('.verified-' + id).html($.trim(response));
                }
            });
        } else
            return;
    });


    $('.archive').live('click', function() {
        var details = $(this).attr('id').split('--');
        var table = details[0];
        var id = details[1];
        var archive = details[2];
        if (archive == 0) {
            $.ajax({
                type: 'POST',
                url: 'ajax.php',
                data: {action: 'archive', id: id, table: table, archive: archive},
                success: function(response) {
                    $('.archive-' + id).html($.trim(response));
                }
            });
        } else
            return;
    });
    /*
     * Code for Delete
     */

    $('.delete').live('click', function() {
        var sure = confirm('Are you sure, you want to delete?');
        if (sure) {
            var details = $(this).attr('id').split('-');
            var table = details[1];
            var id = details[2];
            $.ajax({
                type: 'POST',
                url: 'ajax.php',
                data: {action: 'delete', id: id, table: table},
                success: function() {
                    $('.row_' + id).hide();
                }
            });
        }
    });

    /*
     * Closing Quotes
     */

    $('.close').live('click', function() {
        var sure = confirm('Are you sure, you want to close?');
        if (sure) {
            var details = $(this).attr('id').split('-');
            var table = details[1];
            var id = details[2];
            $.ajax({
                type: 'POST',
                url: 'ajax.php',
                data: {action: 'close', id: id, table: table},
                success: function() {
                    $('#close_' + id).html('Closed');
                    $('#closequote_' + id).hide();
                    $('#convert_' + id).hide();
                }
            });
        }
    });

    /*
     * Convert Quote to Project
     */

    $('.convert').live('click', function() {
        var sure = confirm('Are you sure, you want to convert this quote to project?');
        if (sure) {
            var details = $(this).attr('id').split('-');
            var table = details[1];
            var id = details[2];
            $.ajax({
                type: 'POST',
                url: 'ajax.php',
                data: {action: 'convert', id: id, table: table},
                success: function() {
                    $('#convert_' + id).hide();
                    $('#close_' + id).html('yes');
                }
            });
        }
    });

    /*
     * Project Completed
     */

    $('.complete').live('click', function() {
        var sure = confirm('Are you sure, you want to Complete this project?');
        if (sure) {
            var details = $(this).attr('id').split('-');
            var table = details[1];
            var id = details[2];
            $.ajax({
                type: 'POST',
                url: 'ajax.php',
                data: {action: 'complete', id: id, table: table},
                success: function() {
                    $('#complete-quotes-' + id).hide();
                    $('#updateProgress-' + id).hide();
                }
            });
        }
    });
    /*
     * State Listing
     */
    $('.country').on('change', function() {
        var country = $(this).val();
        $('#state_id').val('');
        $.ajax({
            type: 'POST',
            url: 'ajax.php',
            data: {action: 'stateList', country: country},
            success: function(response) {
                $('#state_id').html($.trim(response));

            }
        });
    });
    /*
     * Provider-Comparison Shortlist
     */
    $('.compare_shortlist').click(function(){
        if($(this).is(":checked")){
        var shortlisted_item = $(this).val();
        $.ajax({
            type: 'POST',
            url: 'includes/ajax.php',
            data: {action: 'compareShortList', id: shortlisted_item, flag: 1},
            success: function(response) {
                $('#shortlist_id').html(response);

            }
        });
        }else{
            var shortlisted_item = $(this).val();
            $.ajax({
                type: 'POST',
                url: 'includes/ajax.php',
                data: {action: 'compareShortList', id: shortlisted_item, flag: 2},
                success: function(response) {
                    $('#shortlist_id').html(response);

                }
            }); 
        }
    });
    
    $('#reset-shortlist, #reset-shortlst').click(function(){ 
        $.ajax({
            type: 'POST',
            url: 'includes/ajax.php',
            data: {action: 'resetShortList'},
            success: function(response) {
                window.location.reload(true);
            }
        });
    });

    /*
     * Add Rule for Phone validation
     */
    jQuery.validator.addMethod("phoneUS", function(phone_number, element) {
        phone_number = phone_number.replace(/\s+/g, "");
        return this.optional(element) || phone_number.length > 9 &&
                phone_number.match(/^(1-?)?(\([2-9]\d{2}\)|[2-9]\d{2})-?[2-9]\d{2}-?\d{4}$/);
    }, "Please specify a valid phone number");
    $('#country_id').on('change', function() {
        $('input[name="zipcode"]').rules('remove');
        if ($(this).val() == 229) {  // for %
            $('input[name="zipcode"]').rules('add', {
                digits: true,
                required: true,
                maxlength: 5
            });
        }
    });

    /*
     * Add Rule for file extension
     */
    jQuery.validator.addMethod("extension", function(value, element, param) {
        param = typeof param === "string"? param.replace(/,/g, '|') : "png|jpe?g|gif";
        return this.optional(element) || value.match(new RegExp(".(" + param + ")$", "i"));
    }, jQuery.format("Please enter a value with a valid extension."));


    $('#start_date').datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true,
        onSelect: function(selected) {
            $("#end_date").datepicker("option", "minDate", selected)
        }
    });
    $('#end_date').datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true,
        onSelect: function(selected) {
            $("#start_date").datepicker("option", "maxDate", selected)
        }
    });

});

$(document).ready(function() {
    setTimeout(function() {

        $("#success").hide('slow');
        $('#error').hide('slow');
    }, 5000);
});