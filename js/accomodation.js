 $(document).ready(function() {
	 
	 $('ul.tabs li').click(function() {
		 $('article').hide();
		 $('ul.tabs li').removeClass('active');
		 var curIndex = $(this).index();
		 $('article').eq(curIndex).show();
		 $(this).addClass('active');
		if($('select').is(':visible')){
			 if(curIndex==1)
		 {
			$('article select#select1').jqTransSelect({imgPath:'common/images/img/'});
			selectBoxUlHide();
		
		}else if(curIndex==3){
			
			$('article select#select3').jqTransSelect({imgPath:'common/images/img/'});
			selectBoxUlHide();
			}else if(curIndex==4){
			
			$('article select#select4').jqTransSelect({imgPath:'common/images/img/'});
			selectBoxUlHide();
			}
		}
		 return false;
		 	 
	});
	$('div.partner_slider').cycle({
		fx: 'scrollHorz',
		next: '.nexx_btn',
		prev: '.prvv_btn'
	});
	
	
	 $('ul.lower_tabs li').click(function() {
		 
		 $('div.featured_slider').hide();
		 $('ul.lower_tabs li').removeClass('active');
		 var curIndex = $(this).index();
		 $('div.featured_slider').eq(curIndex).show();
		 $(this).addClass('active');
			$('div.tab_slider').cycle({
				fx: 'scrollHorz',
				timeout: 0,
				next: '.next_button',
				prev: '.prev_button'
				
			});
		 return false;
		 	 
	});
	
	$('div.share_sec').click(function() {
		
		if ($(this).hasClass('active')) {
			
			$('div.share_sec').css({'z-index':'999'}).animate({'right' : '0px'}, 1000);
			$('div.share_details').css({'z-index':'999'}).animate({width: 'toggle'}, 1000);
			$(this).removeClass('active');
			
		}
		else {
			
			
			if ($('div.finder_sec').hasClass('active')) {
				
				$('div.finder_sec').css({'z-index':'999'}).animate({'right' : '0px'}, 1000);
				$('div.finder_details').css({'z-index':'999'}).animate({width: 'toggle'}, 1000);
				$('div.finder_sec').removeClass('active');
				
			}
			
			if ($('div.wishlist_sec').hasClass('active')) {
				
				$('div.wishlist_sec').css({'z-index':'999'}).animate({'right' : '0px'}, 1000);
				$('div.wishlist_details').css({'z-index':'999'}).animate({width: 'toggle'}, 1000);
				$('div.wishlist_sec').removeClass('active');
			
			}
			
			$('div.share_sec').css({'z-index':'9999'}).animate({'right' : '643px'}, 1000);
			$('div.share_details').css({'z-index':'9999'}).animate({width: 'toggle'}, 1000);
			$(this).addClass('active');

		
		}
		
		return false;
		
	});
	
	$('div.finder_sec').click(function() {
		
		if ($(this).hasClass('active')) {
			
			$('div.finder_sec').css({'z-index':'999'}).animate({'right' : '0px'}, 1000);
			$('div.finder_details').css({'z-index':'999'}).animate({width: 'toggle'}, 1000);
			$(this).removeClass('active');
				destiSelect(false);
				 holidaySelect(false);
				acordianFlyOut(false);
				holidaySelectHide(false);
		
		}
		else {
			
			if ($('div.wishlist_details').hasClass('active')) {
				
				$('div.wishlist_sec').css({'z-index':'999'}).animate({'right' : '0px'}, 1000);
				$('div.wishlist_details').css({'z-index':'999'}).animate({width: 'toggle'}, 1000);
				$('div.wishlist_sec').removeClass('active');
			
			}
			
			if ($('div.share_sec').hasClass('active')) {
				
				$('div.share_sec').css({'z-index':'999'}).animate({'right' : '0px'}, 1000);
				$('div.share_details').css({'z-index':'999'}).animate({width: 'toggle'}, 1000);
				$('div.share_sec').removeClass('active');
			
			}
			
			$('div.finder_sec').css({'z-index':'9999'}).animate({'right' : '621px'}, 1000);
			$('div.finder_details').css({'z-index':'9999'}).animate({width: 'toggle'}, 1000);
			$(this).addClass('active');
			
		$('div.finder_details input[type=radio]').jqTransRadio({imgPath:'common/images/img/'});
		$('div.finder_details input[type=checkbox]').jqTransCheckBox({imgPath:'common/images/img/'});
		destiSelect(true);
		 holidaySelect(true);
		acordianFlyOut(true);
		holidaySelectHide(true);
		
		}
		
		return false;
		
	});
	
	$('div.wishlist_sec').click(function() {
		
		if ($(this).hasClass('active')) {
			
			$('div.wishlist_sec').css({'z-index':'999'}).animate({'right' : '0px'}, 1000);
			$('div.wishlist_details').css({'z-index':'999'}).animate({width: 'toggle'}, 1000);
			$(this).removeClass('active');
			$('#wish_list_details').jScrollPaneRemove();
		
		}
		else {
			
			if ($('div.share_sec').hasClass('active')) {
				
				$('div.share_sec').css({'z-index':'999'}).animate({'right' : '0px'}, 1000);
				$('div.share_details').css({'z-index':'999'}).animate({width: 'toggle'}, 1000);
				$('div.share_sec').removeClass('active');
			
			}
			
			if ($('div.finder_sec').hasClass('active')) {
				
				$('div.finder_sec').css({'z-index':'999'}).animate({'right' : '0px'}, 1000);
				$('div.finder_details').css({'z-index':'999'}).animate({width: 'toggle'}, 1000);
				$('div.finder_sec').removeClass('active');
			
			}
			
			$('div.wishlist_sec').css({'z-index':'9999'}).animate({'right' : '681px'}, 1000);
			$('div.wishlist_details').css({'z-index':'9999'}).animate({width: 'toggle'}, 1000);
			$(this).addClass('active');
			
			$('#wish_list_details').jScrollPane({ scrollbarWidth: 10});
		
		}
		
		return false;
		
	});
	
	 $('div.finder_details input[type=radio]').jqTransRadio({imgPath:'common/images/img/'});
	 //$('div.finder_details select').jqTransSelect({imgPath:'common/images/img/'});
	 $('ul.tabs li').eq(0).trigger('click');
	 $('ul.lower_tabs li').eq(1).trigger('click');
	 
	 
});

function acordianFlyOut(flag){
	if(flag==true){
$('.destinations_drop ul li strong a').click(function(){
		 $(this).parent().next().next().slideToggle();
		 $(this).parent().toggleClass('active');
		return false;
	});
	}else{$('.destinations_drop ul li strong a').unbind('click');}
}
	
function destiSelect(flag){
	if(flag==true){
	
	$('.destinations ul li a').click(function(){
			if($(this).hasClass('active')){
				$('.destinations_drop').hide('slide',{direction:"up"},200);
				$(this).removeClass('active')
			}else{
		$('#destScroll').jScrollPaneRemove();
		$('.destinations_drop').show('slide',{direction:"up"},200);
		$(this).addClass('active')
		if($('.destinations_drop').is(':visible')){
			$('#destScroll').jScrollPane({ scrollbarWidth: 10});
		}
	}
		return false;
		});
		
	}else{
		$('.destinations ul li a').unbind('click');
	}

}	

function holidaySelect(flag){
		if(flag==true){
	$('.holiday ul li a').click(function(){
		$('.holiday_drop').toggle('slide',{direction:"up"},200);
		return false;
		});
}else{$('.holiday ul li a').unbind('click');}
}	
	
function holidaySelectHide(flag)
{
		if(flag==true){
$(document).click(function(e) {
     	if($('.destinations_drop').is(':visible')){
		$('.destinations_drop').hide('slide',{direction:"up"},200);
		$('.destinations ul li a').removeClass('active');
		}
		if($('.holiday_drop').is(':visible')){
		$('.holiday_drop').hide('slide',{direction:"up"},200);
		}		
	});
	}else{$(document).unbind('click');}
	$('.destinations_drop').click(function(e) {e.stopPropagation();});
	//$('.destinations ul li a').click(function(e) {e.stopPropagation();});
	$('.holiday_drop').click(function(e) {e.stopPropagation();});
}

function selectBoxUlHide()
{
$(document).click(function(e) {
    var clicked = e.target.className;
    if($.trim(clicked) != 'jqTransformSelectWrapper') {
     	$(".jqTransformSelectWrapper ul").hide();
		}	
 		e.stopPropagation();
		if($('.price_list').is(':visible')){
		$('.price_list').hide('slide',{direction:"up"},200);
}
	});
}