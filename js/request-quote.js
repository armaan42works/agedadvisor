  /*
     * Objective: JS for request a free quote
     * filename : request-quote.js
     * Created By: Sanket Khanna <sanket.khanna@ilmp-tech.com>
     * Created On : 01 September 2014
   */

 $(document).ready(function() {
        $("#noAcc").click(function() {
            //Scrolling the website to request a free quote form on click of No
            $('html, body').animate({
                scrollTop: $("#scrollDiv").offset().top
            }, 2000);
        });
    });

    $(document).ready(function() {
        $('#service').on('change', function() {
            var service = $(this).val();
            if (service == 0) {
                $('.other').show();
                $('#other_services').addClass('required');
            } else {
                $('.other').hide();
                $('#other_services').removeClass('required');
            }
        });

        $('.dataanalyze').on('click', function() {
            var data = $(this).val();
            if (parseInt(data) == 0) {
                $('.filesize').show();
                $('#small').attr('checked', true);
            } else {
                $('.filesize').hide();
                $('#small').attr('checked', false);
            }
        });

        $('.budget').on('click', function() {
            var budget = $(this).val();
            if (budget == 0) {
                $('#max_amount').hide();
                $('#max_amount').val('');
                $('#min_amount').val('');
                $('#min_amount').show();
                $('#min_amount').addClass('required');
                $('#min_amount').removeClass('required');
                $('#and').hide();
                $("label[for='max_amount']").hide();
            } else if (budget == 1) {
                $('#min_amount').show();
                $('#max_amount').show();
                $('#min_amount').addClass('required');
                $('#max_amount').addClass('required');
                $('#max_amount').val('');
                $('#min_amount').val('');
                $('#and').show();
            } else if (budget == 2) {
                $('#min_amount').hide();
                $('#min_amount').val();
                $('#max_amount').show();
                $('#max_amount').addClass('required');
                $('#min_amount').removeClass('required');
                $('#max_amount').val('');
                $('#min_amount').val('');
                $('#and').hide();
                $("label[for='min_amount']").hide();
            }
        });

        $('.promo').on('click', function() {
            var promo = $(this).val();
            if (promo == 0 || promo == 1) {
                $('.promocode').show();
                $('#promocode').val('');
                 $('input[name="promocode]').rules('remove');
                $('#promocode').addClass('required');
            } else if (promo == 2) {
                $('.promocode').hide();
                $('#promocode').val('');
                $('input[name="promocode]').rules('remove');
                $('#promocode').removeClass('required');

            }
        });

        $('.alert').on('click', function() {
            var alert = $(this).val();
            var country = $('#c_country').val();
            if (alert == 0) {
                $('.smsnumber').show();
                $('#cellphone').show();
                $('#countryCode').val('');
                $('#cellphone').val('');
                $('#countryCode').addClass('required');
                $('#cellphone').addClass('required');
                if (country != 229) {
                    $('#countryCode').show();
                }
            } else {
                $('.smsnumber').hide();
                $('#cellphone').show();
                $('#countryCode').val('');
                $('#cellphone').val('');
                $('#countryCode').removeClass('required');
                $('#cellphone').removeClass('required');
            }
        });

    });