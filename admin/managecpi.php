<?php
include("../application_top.php");
include(INCLUDE_PATH . "header.php");
global $db;

$sql_query = "SELECT cpi_value FROM " . _prefix("cpi_value") . " where id = 1";
    $res = mysqli_query($db->db_connect_id,$sql_query);
    $records =mysqli_fetch_assoc($res);
    if(isset($_POST['submit'])){
        $cpi_value= $_POST['cpi'];
       $sql_query= "UPDATE ad_cpi_value SET cpi_value=$cpi_value WHERE id=1";
        $exe_query=mysqli_query($db->db_connect_id,$sql_query);
        $sql_query = "SELECT cpi_value FROM " . _prefix("cpi_value") . " where id = 1";
        $res = mysqli_query($db->db_connect_id,$sql_query);
        $records =mysqli_fetch_assoc($res);

    }
?>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tbody>
        <tr>
            <td class="lt_nav" height="455" valign="top" width="15%">
                <!-- left navigation -->
                <?php include(INCLUDE_PATH . "left_navigation.php"); ?>
                <!-- left navigation ends here -->
            </td>
            <td style="vertical-align: 0;">
<div style="margin-top: 10px;">
                    <div class="breadcrumb">
                        <ul>
                            <li><a href="<?php echo HOME_PATH_URL; ?>admin.php"><i class="fa fa-home"></i>Home </a>>><a href="<?php echo HOME_PATH_URL . "managecpi.php" ?>"> Manage Cpi </a>>></li>
                        </ul>
                    </div>
    
    <div class="container" style="width: 95%; border: 1px solid rgb(204, 204, 204);">
        <div class="row" style="margin-bottom: 0px; border-bottom: 1px solid rgb(204, 204, 204);">
            <div class="col-sm-6">
                <h2 class=""> Manage CPI</h2>
            </div>

            <div class="col-sm-6">
                <h2 style="float:right;" class="redCol small">* fields required</h2>
            </div>
        </div>
        <p class="clearfix"></p>
        <form name="addCity" id="addCity" action="" method="POST" class="form-horizontal" role="form" enctype="multipart/form-data">
            <div class="form-group">
                <label for="title" class="col-sm-2 control-label">CPI Value<span class="redCol">*</span></label>
                <div class="col-sm-4">
                <div class="form-group">
                <div class="input-group">
                 <div class="input-group-addon">%</div>
                
                    <input type="text"  class="required form-control" name="cpi" id="cpi"  maxlength="50" value="<?php echo $records['cpi_value'];?>"/>
                </div>
                </div>
                <div class="form-group">
                <div class="col-sm-offset-2 col-sm-8">
                 <input type="submit" value="submit" name="submit" class="submit_btn btn" style="margin:15px 0px 0px 10px">
                 </div>
                 </div>
            </div></form>
            </div>
            </div>
            </td>
    </tr>
</tbody>
</table>
