<?php
/* * ******************************************
 * Admin login page
 * ****************************************** */
include("../application_top.php");

if ($_REQUEST['submit']) {

    $useremail = $_REQUEST['username'];
    $sql_query = "select * from " . _prefix("users") . " where email='$useremail'";
    $res = $db->sql_query($sql_query);
    $records = $db->sql_fetchrowset($res);
    if (count($records)) {
        $to = $records[0]['email'];
        $new_password = Random_Password(6);

        $fields = array('password' => $new_password
        );
        $where = "where email='$to'";
        $update_result = $db->update(_prefix('users'), $fields, $where);
        if ($update_result) {
            $email = emailTemplate('admin_forgot-password');

            if ($email['subject'] != '') {
                $message = str_replace(array('{forgotpass}'), array($new_password), $email['description']);

                $send = sendmail($to, $email['subject'], $message);
                if ($send) {
                    $_SESSION['forgot_password']='forgot_passowrd';
                    $main_obj->redirect("index.php");
                }
            }
        }
    }
}
?>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>

        <link rel="stylesheet" type="text/css" href="style.css">
        <script src="admin/js/jquery-1.9.1.min.js" type="text/javascript"></script>
        <script src="admin/js/jquery-migrate-1.1.1.min.js" type="text/javascript"></script>
        <script src="admin/js/jquery.validate.js" type="text/javascript"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <title><?php echo "Aged Advisor --- ADMIN PANEL"; ?></title>
    </head>
    <body>
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tbody>
                <tr>
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tbody>
                                <tr>
                                    <td></td>
                                    <td bgcolor="#000000" height="10" width="223"><img src="<?php echo  ADMIN_IMAGE; ?>sp.gif" alt=""></td>
                                    <td width="1"><img src="<?php echo  ADMIN_IMAGE; ?>sp.gif"></td>
                                    <td bgcolor="#5fadfa"><img src="<?php echo  ADMIN_IMAGE; ?>sp.gif" alt="Data Anaytics"></td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="450">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tbody>
                                <tr>
                                    <td>
                                        <table align="center" border="0" cellpadding="0" cellspacing="0"
                                               width="376">
                                            <tbody>

                                                <tr>
                                                    <td>
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                            <tbody>

                                                                <tr>
                                                                    <td width="11"><img
                                                                            src="<?php echo  ADMIN_IMAGE; ?>login_box_left_cr.gif" height="37"
                                                                            width="11"></td>
                                                                    <td class="bgCol fs15">&nbsp; <b>Forget Password</b></td>
                                                                    <td width="11"><img
                                                                            src="<?php echo  ADMIN_IMAGE; ?>login_box_right_cr.gif" height="37"
                                                                            width="11"></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td class="loginBg">
                                                        <table border="0" cellpadding="16" cellspacing="0" width="100%">
                                                            <tbody>
                                                                <tr>
                                                                    <td>
                                                                        <table border="0" cellpadding="0" cellspacing="0"
                                                                               width="100%">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td colspan="2">
                                                                                        <div style="color: #FF0000"></div>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>

                                                                                    <td valign="bottom">

                                                                                        <form name="form1" id="forgetPassword" action="" method="post" >
                                                                                            <table
                                                                                                style="border: 1px solid rgb(184, 184, 184); background-color: rgb(255, 251, 248); border-radius: 5px;"
                                                                                                border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                                <tbody>


                                                                                                    <tr>
                                                                                                        <td rowspan="8" width="15">&nbsp;</td>
                                                                                                        <td height="16">&nbsp;</td>
                                                                                                    </tr>

                                                                                                    <tr>
                                                                                                        <td height="16" valign="top" class="login_style">User Id:</td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td><div class="input text"><input type="email" name="username" id="username" size="30" maxlength="30"></div></td>
                                                                                                    </tr>


                                                                                                    <tr>
                                                                                                        <td height="30" valign="bottom">
                                                                                                            <table cellspacing="0" cellpadding="0">
                                                                                                                <tr>
                                                                                                                    <td>
                                                                                                                        <input type="submit" value="submit" name="submit" class="submit_btn">
                                                                                                                    </td>


                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td class="copy" height="15" valign="bottom">&nbsp;</td>
                                                                                                    </tr>

                                                                                                    <tr>
                                                                                                        <td>&nbsp;</td>
                                                                                                    </tr>
                                                                                                </tbody>
                                                                                            </table>
                                                                                        </form>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td height="10"><img
                                                            src="<?php echo  ADMIN_IMAGE; ?>login_box_bottom_cr.gif" height="10" width="376"></td>
                                                </tr>

                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </body>
</html>
<script type="text/javascript">
    $(document).ready(function() {
        $('#forgetPassword').validate({
            rules: {
                "username": {
                    required: true,
                    email: true,
                    remote: {
                        url: '<?php echo HOME_PATH ?>admin/ajax.php',
                        type: 'POST',
                        data: {action: "adminEmail"
                        }
                    }
                }
            },
            message: {
                "username": {
                    "remote": "Email Address is not valid"
                }
            }
        })
    });
</script>

