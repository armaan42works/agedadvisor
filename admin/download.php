<?php
    include("../includes/constant.php"); 
function output_file($file, $name, $mime_type='')
{
 
            $filename = realpath($file);

            $file_extension = strtolower(substr(strrchr($file,"."),1));

            switch ($file_extension) {
                case "pdf": $ctype="application/pdf"; break;
                case "exe": $ctype="application/octet-stream"; break;
                case "zip": $ctype="application/zip"; break;
                case "doc": $ctype="application/msword"; break;
                case "xls": $ctype="application/vnd.ms-excel"; break;
                case "ppt": $ctype="application/vnd.ms-powerpoint"; break;
                case "gif": $ctype="image/gif"; break;
                case "png": $ctype="image/png"; break;
                case "jpe": case "jpeg":
                case "jpg": $ctype="image/jpg"; break;
                default: $ctype="asspplication/force-download";
            }

            if (!file_exists($filename)) {
                die("NO FILE HERE");
            }

            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private",false);
            header("Content-Type: $ctype");
            header("Content-Disposition: attachment; filename=\"".basename($file)."\";");
            header("Content-Transfer-Encoding: binary");
            header("Content-Length: ".@filesize($file));
            set_time_limit(0);
            @readfile("$file") or die("File not found.");

}
//Set the time out
set_time_limit(0);

//path to the file
if(isset($_REQUEST['type'])){
    $file_path=DOCUMENT_PATH.'admin/files/'.$_REQUEST['type'].'/'.$_REQUEST['file'];
} else {
    $file_path=DOCUMENT_PATH.'admin/files/'.$_REQUEST['file'];
}



//Call the download function with file path,file name and file type
output_file($file_path, ''.$_REQUEST['file'].'');
die();
?>