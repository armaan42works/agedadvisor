/* 
 * All ajax calling function here
 * @author: Pramod Kumar
 * Date:30 July 2014.
 */
/*
 /* 
 * Manage coupon:show listing with pagination
 * @author:pramod kumar
 * @Date:30 July 2014
 */
function changePagination(pageId, liId, module,data) {

    $(".flash").show();
    $(".flash").fadeIn(400).html('Loading <img src="images/loading.gif" />');
    var dataString = 'pageId=' + pageId+data;
    if (module == 'history') {
        var model = 'newsletter';
    } else if(module == 'quote' || module == 'sprecord'){
        model = 'project';
    } else if(module == 'customer' || module == 'supplier'|| module=='supplierDetail'){
        model = 'user';
    } else if(module == 'procom'){
        model = 'provider_comparison';
    } else if((module == 'bprice')||(module == 'mprice')||(module == 'comboprice')){
        model = 'price';
    } else if((module == 'artwork')||(module == 'partwork')){
        model = 'artwork';
    }  else if((module == 'blog')||(module == 'comment')){
        model = 'blog';
    }else if((module == 'forum')||(module == 'forumComments')){
        model = 'forum';
    }else if(module == 'adsList'){
        model = 'ads';
    }else if((module == 'location')||(module == 'suburb')||(module == 'restcare')){
        model = 'location';
    }else if(module == 'application'){
        model = 'position';
    } else if(module == 'detail'){
        model = 'payment';
    }else if((module == 'duration')||(module == 'visit')){
        model = 'pages';
    }else if(module == 'faqs'){
        model = 'cms';
    }else if(module == 'abusedreview'){
        model = 'review';
    }else if(module == 'paymentapproval'){
        model = 'paymentapproval';
    }else {
        model = module;

    }
    $.ajax({
        type: "POST",
        url: model + ".php?action=" + module,
        data: dataString,
        cache: false,
        success: function(result) {
            $(".flash").hide();
            $(".link a").removeClass("In-active current");
            $("#" + liId + " a").addClass("In-active current");
            $("#pageData").html(result);
        }
    });
}


