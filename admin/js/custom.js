$(function() {
    $("#selectall").live("click",function(){$(".case").attr("checked",this.checked);if($(".case").prop("checked")){$(".case").closest("tr").addClass("selected")}else{$(".case").closest("tr").removeClass("selected")}});
    $(".case").live("click",function(){$(this).closest("tr").toggleClass("selected");if($(".case").length==$(".case:checked").length){$("#selectall").attr("checked","checked")}else{$("#selectall").removeAttr("checked")}});
    $(".case:checked").each(function(){$(this).closest("tr").addClass("selected")});
    $("a.colorbox").live("click",function(){$.colorbox({href:$(this).attr("href"),width:"80%",height:"80%",overlayClose:false});return false});
//    $('a.colorbox').colorbox({width: "60%", height: "75%", 'overlayClose': false});
    
    $("input.phone").mask("999-999-9999");

    $(".close").live("click",function(){$(this).parent().fadeTo(400,0,function(){$(this).slideUp(400)});return false});
    setTimeout(function(){$(".close").parent().slideUp(400)},2500);
    var showtree = true;
    $("#showtree").live("click",function(){if(showtree){showtree=false;$("#show_tree").slideDown("");$(this).attr("src","/img/li_cat_hide.png")}else{showtree=true;$("#show_tree").slideUp("");$(this).attr("src","/img/li_cat_show.png")}});
});
function checkboxCount(){if($(".case:checked").length<=0){alert("Please select atleast one record  by clicking checkbox !!");return false}};

jQuery.validator.setDefaults({
    errorElement: "div",
    //    errorClass: "error-message",
    //    wrapper: "div",
    errorPlacement: function(error, element) {
        if (element.is(":radio")) {
            error.insertAfter(element.closest('ul.radio'));
            //        error.addClass('error_class');
        } else if (element.is(":checkbox")) {
            error.insertAfter(element.closest('ul.checkbox'));
            //        error.addClass('error_class');
        } else {
            error.insertAfter(element);
            //        error.addClass('error_class');
        }
    }
});

jQuery.validator.addMethod("greaterthanzero",function(a){return parseFloat(a)>0},"Value should be greater than zero");
jQuery.validator.addMethod("charactersonly",function(b,a){return this.optional(a)||/^[a-z ]+$/i.test(b)},"Only Characters allowed");
jQuery.validator.addMethod("charactersonly_numbers_sp_char",function(b,a){return this.optional(a)||b==b.match(/^[-a-zA-Z0-9_ ]+$/)},"Character, Numbers & Space/underscore Allowed.");
jQuery.validator.addMethod("charactersonly_nospace",function(b,a){return this.optional(a)||b==b.match(/^[a-zA-Z]+$/)},"Only Characters allowed without spaces.");
jQuery.validator.addMethod("charactersonly_numbers",function(b,a){return this.optional(a)||b==b.match(/^[a-z0-9A-Z ]+$/)},"Only Characters & Numbers Allowed.");
jQuery.validator.addMethod("numbers",function(b,a){return this.optional(a)||b==b.match(/^[0-9]+$/)},"Only Numeric value Allowed.");
jQuery.validator.addMethod("decimal_numbers",function(b,a){return this.optional(a)||b==b.match(/^\d+(?:\.\d\d?)?$/)},"Only  Decimal Digit Are Allowed.");
jQuery.validator.addMethod("decimal_numbers_nozero",function(b,a){return this.optional(a)||b==b.match(/^(?:\.\d\d?)?$/)},"Only Two Decimal Digit Are Allowed.");
jQuery.validator.addMethod("range12",function(b,a){return this.optional(a)||b==b.match(/^[1-9]|[1-2][0-9]|3[0-1]$/)},"Number should be between 0 to 31.");
jQuery.validator.addMethod("contactNumber",function(b,a){return this.optional(a)||/^[0-9\-()+]+$/i.test(b)},"Please provide mobile number.");
$.validator.addMethod("greaterThan",function(c,a,d){var b=$(d);if(this.settings.onfocusout){b.off(".validate-greaterThan").on("blur.validate-greaterThan",function(){$(a).valid()})}return parseInt(c)>parseInt(b.val())},"Highr price  be greater than base price");
jQuery.validator.addMethod("ip_address",function(b,a){return this.optional(a)||b==b.match(/^(?:25[0-5]|2[0-4]\d|1\d\d|[1-9]\d|\d)(?:[.](?:25[0-5]|2[0-4]\d|1\d\d|[1-9]\d|\d)){3}$/)},"Only this format( 255.255.255.255 )Allowed.");
jQuery.validator.addMethod("notEqualTo",function(c,b,d){var a=true;c=$.trim(c);for(i=0;i<d.length;i++){if(c==$.trim($(d[i]).val())){a=false}}return this.optional(b)||a},"Please enter a diferent value...");
jQuery.validator.addMethod("filesize",function(b,a,c){return this.optional(a)||(a.files[0].size<=c)});

function areAnyChecked(a){return !!$("#"+a+" input[type=checkbox]:checked").length};

$.fn.singleDotHyphen = function() {
    return this.each(function() {
        var a = $(this);
        a.val(function() {
            return a.val().replace(/\.{2,}/g, ".").replace(/\'{2,}/g, "'").replace(/\,{2,}/g, ",").replace(/\\{2,}/g, " ").replace(/\,\n{2,}/g, "\n").replace(/\n{3,}/g, "\n\n").replace(/\[/g, "").replace(/]/g, "").replace(/ {2,}/g, " ").replace(/ \,/g, ",").replace(/\, \n/g, ",\n").replace(/-{2,}/g, "-").replace(/ \n/g, "\n").replace(/ \n{2,}/g, "\n")
        })
    })
};

$(document).ready(function() {
    $("ul#navigation").superfish({delay: 100, animation: {opacity: "show", height: "show"}, speed: "fast", autoArrows: true, dropShadows: false});
    $("ul#navigation li").hover(function() {
        $(this).addClass("sfHover2")
    }, function() {
        $(this).removeClass("sfHover2")
    })
});
