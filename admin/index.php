<?php
if($_SERVER['REMOTE_ADDR']=="188.138.188.34" ||  $_SERVER['REMOTE_ADDR']=="188.138.188.34"){   
    die();
}

    /*     * ******************************************
     * Admin login page
     * ****************************************** */
    include("../application_top.php");
    if (isset($_SESSION['Admin']) && !empty($_SESSION['Admin'])) {
        $main_obj->redirect('admin.php');
    }
?>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="style.css">
        <script src="js/jquery-1.9.1.min.js" type="text/javascript"></script>
        <script src="js/jquery-migrate-1.1.1.min.js" type="text/javascript"></script>
        <script src="js/jquery-ui-1.8.23.custom.min.js" type="text/javascript"></script>
        <script src="js/jquery.validate.js" type="text/javascript"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <title><?php echo "Aged Advisor --- ADMIN PANEL"; ?></title>
    </head>
    <body>
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tbody>
                <tr>
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tbody>
                                <tr>
                                    <td></td>
                                    <td bgcolor="#000000" height="10" width="223"><img src="<?php echo  ADMIN_IMAGE; ?>sp.gif" alt=""></td>
                                    <td width="1"><img src="<?php echo  ADMIN_IMAGE; ?>sp.gif"></td>
                                    <td bgcolor="#5fadfa"><img src="<?php echo  ADMIN_IMAGE; ?>sp.gif" alt="Data Anaytics"></td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="450">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tbody>
                                <tr>
                                    <td>
                                        <table align="center" border="0" cellpadding="0" cellspacing="0"
                                               width="376">
                                            <tbody>
                                                <tr>
                                                    <td align="center" height="40">
                                                        <h1>Welcome to Administration!</h1>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                            <tbody>

                                                                <tr>
                                                                    <td width="11"><img
                                                                            src="<?php echo  ADMIN_IMAGE; ?>login_box_left_cr.gif" height="37"
                                                                            width="11"></td>
                                                                    <td class="bgCol fs15">&nbsp; <b>LOGIN PANEL</b></td>
                                                                    <td width="11"><img
                                                                            src="<?php echo  ADMIN_IMAGE; ?>login_box_right_cr.gif" height="37"
                                                                            width="11"></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td class="loginBg">
                                                        <table border="0" cellpadding="16" cellspacing="0" width="100%">
                                                            <tbody>
                                                                <tr>
                                                                    <td>
                                                                        <table border="0" cellpadding="0" cellspacing="0"
                                                                               width="100%">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td colspan="2">
                                                                                        <div style="color: #FF0000"></div>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td valign="top" width="45%">
                                                                                        <table border="0" cellpadding="0" cellspacing="0"
                                                                                               width="100%">
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <img src="<?php echo  ADMIN_IMAGE; ?>login_pic.gif" height="65" width="65">
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="login_style"> Use a valid username and
                                                                                                        password to gain access to
                                                                                                        the administration console.</td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </td>
                                                                                    <td valign="bottom">
                                                                                        <form name="form1"  id="form1" action="login.php" method="post" >
                                                                                            <table
                                                                                                style="border: 1px solid rgb(184, 184, 184); background-color: rgb(255, 251, 248); border-radius: 5px;"
                                                                                                border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                                <tbody>
                                                                                                    <?php if (key($_REQUEST) == 'msg') { ?>
                                                                                                            <tr>
                                                                                                                <td colspan="30" valign="top"><?php echo common_message(0, constant($msg)); ?></td>
                                                                                                            </tr>
                                                                                                        <?php }else if($_SESSION['forgot_password']){?>
                                                                                                            <tr>
                                                                                                                <td colspan="30" valign="top">
                                                                                                                    <div class="errorForm" id="error">
                                                                                                                        <br>
                                                                                                                    <?php echo FORGET_PASSWORD; ?></div></td>
                                                                                                            </tr>
                                                                                                            
                                                                                                       <?php unset($_SESSION['forgot_password']);} ?>

                                                                                                    <tr>
                                                                                                        <td rowspan="8" width="15">&nbsp;</td>
                                                                                                        <td height="16">&nbsp;</td>
                                                                                                    </tr>

                                                                                                    <tr>
                                                                                                        <td height="16" valign="top" class="login_style">User Id:</td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td><div class="input text"><input type="text" class="required" name="useid" id="username" maxlength="30"></div></td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td height="16" valign="top" class="login_style">Password:</td>

                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td><div class="input password"><input type="password" class="required" name="password" id="password" maxlength="30"></div></td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <span class="forgot_text">
                                                                                                                <a href="<?php echo HOME_PATH_URL; ?>forget_password.php">I forgot my Password</a><br>
                                                                                                            </span>
                                                                                                        </td>

                                                                                                    </tr>


                                                                                                    <tr>
                                                                                                        <td height="30" valign="bottom">
                                                                                                            <table cellspacing="0" cellpadding="0">
                                                                                                                <tr>
                                                                                                                    <td><input type="image" src="<?php echo ADMIN_IMAGE; ?>submit.gif" name="submit"> </td>
                                                                                                                    <td>&nbsp;</td>

                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td class="copy" height="15" valign="bottom">&nbsp;</td>
                                                                                                    </tr>

                                                                                                    <tr>
                                                                                                        <td>&nbsp;</td>
                                                                                                    </tr>
                                                                                                </tbody>
                                                                                            </table>
                                                                                        </form>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td height="10"><img src="<?php echo  ADMIN_IMAGE; ?>login_box_bottom_cr.gif" height="10" width="376"></td>
                                                </tr>

                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </body>
</html>
<script type="text/javascript">
        $(document).ready(function() {
        $('#form1').validate();
    });
    </script>