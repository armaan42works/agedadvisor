<?php
/* * ******************************* */
//Header Navigtion page of Admin
if (!isset($_SESSION['Admin'])) {
    $url = HOME_PATH_URL . 'index.php';
    redirect_to($url);
    exit();
}
?>
<?php require_once("common_ajaxPaging.php"); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<head>
    <link rel="stylesheet" type="text/css" href="css/style.css"/>
    <link rel="stylesheet" type="text/css" href="css/calendar.css"/>
    <link rel="stylesheet" href="css/jquery.ui.css"/>
    <link rel="stylesheet" href="css/colorbox.css"/>
    <link rel="stylesheet" href="css/font-awesome-4.0.3/css/font-awesome.min.css"/>
    <link href="<?php echo HOME_PATH; ?>css/jRating.jquery.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="js/jquery-migrate-1.1.1.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui-1.8.23.custom.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui-timepicker-addon.js" type="text/javascript"></script>
    <script src="js/jquery.validate.js" type="text/javascript"></script>
    <script src="js/jquery.colorbox-min.js" type="text/javascript"></script>
    <script src="js/jquery.maskedinput.min.js" type="text/javascript"></script>
    <!--<script src="js/developer-script.js" type="text/javascript"></script>-->
    <script type="text/javascript" src="<?php echo HOME_PATH; ?>js/bootstrap-multiselect.js"></script>
    <script src="js/validations.js" type="text/javascript"></script>
    <script src="js/commonAjax.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/jqueryfileDownload.js"></script>
    <!--<script type="text/javascript" src="js/jqueryfileDownload.js"></script>-->
    <script src="<?php echo HOME_PATH; ?>js/jRating.jquery.min.js"></script>
    <!-- code for boostrap start here -->

    <!--     Latest compiled and minified CSS
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">

             Optional theme
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">

                 Latest compiled and minified JavaScript
                <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>-->
    <!-- code for boostrap end here -->

    <link href="<?php echo HOME_PATH; ?>css/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css" />
    <script src="<?php echo HOME_PATH; ?>css/bootstrap/js/bootstrap.js"></script>
    <?php ?>
    <title><?php echo "Aged Advisor --- ADMIN PANEL"; ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=windows-1252"/>
    <meta content="MShtml 6.00.2600.0" name="GENERATOR"/>
</head>
<body>

    <div class="header_data">
        <div class="header_left_logo"><a href='<?php echo HOME_PATH_URL . 'admin.php' ?>'><img src="<?php echo IMAGES . 'logo.png' ?>"></a></div>
        <div class="header_right_welcome">WELCOME ADMIN  &nbsp;&nbsp; <a  style="color:#F06D3A" href="<?php echo HOME_PATH_URL; ?>logout.php">LOGOUT</a></div>
    </div>
