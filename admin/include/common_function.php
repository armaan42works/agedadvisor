<?php
function display_list($sql,$arr_flds,$mode,$id_fld,$display='')
   {
	  global $obj_mysql,$obj_paging,$obj_common;
	  
		  if($_GET['page_no'])
		     $page_no=$_GET['page_no'];
	      else
		     $page_no=0;
			 
		  /*--------------------if page_no alreay exists please remove them ---------------------------*/
			$query_str=$_SERVER['QUERY_STRING'];
			$str_pos=strpos($query_str,'&page_no');
			if($str_pos>0)
			$query_str=substr($query_str,0,$str_pos); 	 		
		  /*------------------------------------------------------------------------------------------*/
		  $total_num=$obj_mysql->get_num_rows($sql);	
		 // $sql.= " LIMIT $obj_paging->lower,$obj_paging->limit";
		  $str=display_top($arr_flds,$mode);
		  $colspan=count($arr_flds)+2;
		 
		  if($total_num=='0')
		  $str.='<tr><td align=center class="NormalRedText" colspan="'.$colspan.'" height="30">No record available!</td></tr>';
		  $res=$obj_mysql->exec_query($sql);
		  $count=0; 
		  while($rec=mysqli_fetch_assoc($res))
			{
				$table_id=$rec[$id_fld];
				if($count%2==0)
					$class='AlTr1';
				else
					$class='AlTr2';	
			    $str.=display_row($rec,$class,$arr_flds,$mode,$table_id,$display);
		  		$count++;	
			}
			
		  $str.="</table>";
			$str.="<table cellpadding=0 cellspacing=0 width=100%><tr><td>";
			//$str.=$obj_paging->next_pre($page_no,$total_num,"index.php?$query_str&",'NormalBlueText','NormalRedText');
			$str.="</td></tr></table>";
		  
		  $str.="</form>";
		  
return $str;
}

function display_top($arr_flds)
  {
    
    
	 $str.="<form name='frm_list' method='post'>
	 <input type='hidden' name='Action' id='Action'>
	 <table cellpadding=0 class='TableBox' cellspacing=0 width=100% >";
	 $str.="$colspan<tr class='MainTr'>";
	 if($mode==1)
	 	$str.="<td width=2%> </td>";
	  else
	   $str.="<td width=2% align='center'><input type='checkbox' name='main' onClick=\"checkAll(document.frm_list,this.checked)\" > </td>";	
	 foreach($arr_flds as $key=>$val)
	 {
     	 $str.="<td>$key</td>";
	 }
	$str.="<td>Action</td>";	 	
  	$str.="</tr>";
	
	return $str;	
  }
  
function display_row($rec,$class,$arr_flds,$mode,$table_id,$display)
  {
    
  global $obj_common;

 	$query_str=$_SERVER['QUERY_STRING'];	
	  $str="<tr class=\"$class\">";
	 if($mode==1)
		 $str.="<td align='center'><input type='checkbox' name='items[]' id='items' value=\"$table_id\"> </td>";
	foreach($arr_flds as $key=>$val)
     { 
	 	$value=$rec[$val];
		 	
	if($val=='fld_status' || $val=='fld_userActive')
		  $value=get_status($value);
		  
	if($display=='sub_category')
	{
	if($val=='fld_category_id')
		$value=$obj_common->category_name($value);
	}
	
	if($display=='sub_state')
	{
	if($val=='fld_state_id')
		$value=$obj_common->state_name($value);
	}
	if($display=='sub_country')
	{
	if($val=='fld_country_id')
		$value=$obj_common->country_name($value);
	}
	
	if($display=='user_manager')
	{
	if($val=='fld_level')
		$value=$obj_common->get_level($value);
	}	

	
	if($display=='user_report' || $display=='rating_manager' )
	{
	if($val=='fld_project_id')
		$value=$obj_common->get_progectName($value);
	}	
	
		if( $display=='rating_manager' )
	{
	if($val=='fld_programmer_id' || $val=='fld_dept')
		$value=$obj_common->get_programmerName($value);
	}		
	if( $display=='rating_manager' )
	{
	if($val=='fld_module_id')
		$value=$obj_common->get_moduleName($value);
	}

	if(strlen($value)>22)
	 	    $value=substr($value,0,22)."...";

		$str.="<td>$value</td>";
 }
	  $query_str.="&rec_id=".md5($table_id);
	  $query_str1=str_replace('action=Edit','action=ViewDetails',$query_str);
	  $query_str2=str_replace('action=Edit','action=Access',$query_str);
	  $query_str3=str_replace('action=ViewAssignProject','action=ViewModule',$query_str);
          $passwordChangeQuery=str_replace('action=Edit','action=change_password',$query_str);

if($display=='admin_manager')
{ $query_edit=str_replace('action=ViewAllProject','action=EditProject',$query_str);
	  $query_view=str_replace('action=ViewAllProject','action=ViewModule',$query_str);
	  $query_add=str_replace('action=ViewAllProject','action=addModule',$query_str);
	  if($_SESSION['type']==1)
	  {
	  $str.="<td><a href=\"index.php?$query_edit\" class=\"NormalBlueText\"><u>Edit Project</u></a> |
	  <a href=\"index.php?$query_view\" class=\"NormalBlueText\"><u>View Module</u></a> | 
	  <a href=\"index.php?$query_add\" class=\"NormalBlueText\"><u>Add Module</u></a> ";
	  }
	   else if($_SESSION['dept']==3)
	  {
	   $str.="<td>
	  <a href=\"index.php?$query_view\" class=\"NormalBlueText\"><u>View Module</u></a> | 
	  <a href=\"index.php?$query_add\" class=\"NormalBlueText\"><u>Add Module</u> </a>|
	 <a href=\"#\" class=\"NormalBlueText\" onclick=\"return call('$table_id');\"><u>Reset Task </u></a>";

	  }
	  else
	  {
	//index.php?$query_add	 
	  $str.="<td>
	  <a href=\"index.php?$query_view\" class=\"NormalBlueText\"><u>View Module </u></a> | 
	  <a href=\"index.php?$query_add\" class=\"NormalBlueText\"><u>Add Module</u>";

	  }
	}

elseif($display=='user_report')
	 $str.="<td><a href=\"index.php?$query_str3\" class=\"NormalBlueText\"><u>View Module</u></a>";
	 
elseif( $display=='optionalleave_management' )
{
$str.="<td><a href=\"index.php?$query_str\" class=\"NormalBlueText\"><u>Edit</u></a> | <a href=\"#\" id=\"tbl_optionleave-$table_id\" class=\"NormalBlueText delete_record\"><u>Delete</u></a>";
}
elseif( $display=='leave_management' )
{
   if($rec['fld_title']=='Pending'){
$str.="<td><a href=\"index.php?$query_str\" class=\"NormalBlueText\"><u>Approve/Cancel</u></a>";
   }else{
       $str.="<td>";
   }
}

		 
	 
else
{
		$str.="<td><a href=\"index.php?$query_str\" class=\"NormalBlueText\"><u>Edit</u></a> 
		<!----<a href=\"index.php?$query_str1\" class=\"NormalBlueText\"><u>View Details</u></a>---->";
		if($display=='user_manager')
                    $str.="| <a href=\"index.php?$query_str2\" class=\"NormalBlueText\"><u>Access Key</u></a>";	
                if($display != 'leave_management')
                    $str.="| <a href=\"index.php?$passwordChangeQuery\" class=\"NormalBlueText\"><u>Changepassword</u></a></td>";	
                }
	
	  $str.="</tr>";
	  
	return $str;
  }
  
function get_status($status)
  {
  	if($status==3)
		$str= "Admin";
  	elseif($status==2)
		$str= "Finished";
  	else if($status==1)
	  $str = "Active";
	else if($status=='0')
	  $str = "Inactive" ;
	else 
	  $str = "Active";
	return $str;
  }

?>

<script>
       function call(id)
	   {
		var cmd=confirm('do you want to reset the tast of this project');
	    if(cmd){	
		window.location="export.php?rec_id="+id;
		 return true;
		}
		else{ 
			return false;
		}
	   }
		</script>