<?php /*
     * Objective  : Used for left navigation links
     * Filename :   left_navigation.php
     * Created By : Vinod Kumar Singh <vinod.kumar@ilmp-tech.com>
     * Created On : 01 August 2014
     */
?>
<script>
    $(document).ready(function() {
        $('#nav > li > a').click(function() {
            if ($(this).attr('class') != 'active') {
                $('#nav li ul').slideUp();
                $(this).next().slideToggle();
                $('#nav li a').removeClass('active');
                $(this).addClass('active');
            }
        });
    });
</script>
<style>
    #nav {
        width: 217px;
        list-style: none;
        padding-left: 0;

    }
    #nav li a {
        display: block;
        padding: 10px 15px;
        background: none repeat scroll 0 0 #29599A;
        border-top: 1px solid #4B99E6;
        border-bottom: 1px solid #4B99E6;
        text-decoration: none;
        color: #fff;
        list-style:none;
        font: 400 14px 'PT Sans Narrow', sans-serif;
    }
    #nav li a:hover, #nav li a.active {
        background: #4B99E6;
        color: #fff;
    }
    #nav li ul {
        display: none; 
        list-style: none;
    }
    #nav li ul li a {
        padding: 10px 25px;
        background: #ededed;
        border: 1px solid #ccc;
        font: 400 12px 'PT Sans Narrow', sans-serif;
        color:#000;
    }

    #nav li ul li a:hover{background: #929292; color:#000;}
    .subactive{background: #bcbcbc !important; color:#000 !important;}
    .title{
        list-style:none;
        padding-left: 0;
    }
</style>
<?php
    $nav_url = $_SERVER['REQUEST_URI'];
    $subactive = key($_REQUEST);
?>
<ul id="nav">

    <li><a href="javascript:void(0);" class="<?php echo strpos($nav_url, 'ads.php') > 0 ? 'active' : ''; ?>">Manage Ads</a>
        <ul class="title" style="<?php echo strpos($nav_url, 'ads.php') > 0 ? 'display:block;' : ''; ?>">
            <li><a href="<?php echo HOME_PATH_URL; ?>ads.php?manage" class="<?php echo strpos($nav_url, 'ads.php') > 0 && $subactive == 'manage' ? 'subactive' : '' ?>">Manage Ads</a></li>
            <li><a href="<?php echo HOME_PATH_URL; ?>ads.php?add" class="<?php echo strpos($nav_url, 'ads.php') > 0 && $subactive == 'add' ? 'subactive' : '' ?>">Add Ads</a></li>
        </ul>
    </li>
    
    <li><a href="javascript:void(0);" class="<?php echo strpos($nav_url, 'freeimages.php') > 0 ? 'active' : ''; ?>">Manage Image</a>
        <ul class="title" style="<?php echo strpos($nav_url, 'freeimages.php') > 0 ? 'display:block;' : ''; ?>">
            <li><a href="<?php echo HOME_PATH_URL; ?>freeimages.php?manage" class="<?php echo strpos($nav_url, 'freeimages.php') > 0 && $subactive == 'manage' ? 'subactive' : '' ?>">Manage Image</a></li>
            <li><a href="<?php echo HOME_PATH_URL; ?>freeimages.php?add" class="<?php echo strpos($nav_url, 'freeimages.php') > 0 && $subactive == 'add' ? 'subactive' : '' ?>">Add Image</a></li>
        </ul>
    </li>
    
    <li><a href="javascript:void(0);" class="<?php echo strpos($nav_url, 'blog.php') > 0 ? 'active' : ''; ?>">Manage Blog</a>
        <ul class="title" style="<?php echo strpos($nav_url, 'blog.php') > 0 ? 'display:block;' : ''; ?>">
            <li><a href="<?php echo HOME_PATH_URL; ?>blog.php?manage" class="<?php echo strpos($nav_url, 'blog.php') > 0 && $subactive == 'manage' ? 'subactive' : '' ?>">Manage Blog</a></li>
            <li><a href="<?php echo HOME_PATH_URL; ?>blog.php?add" class="<?php echo strpos($nav_url, 'blog.php') > 0 && $subactive == 'add' ? 'subactive' : '' ?>">Add Blog</a></li>
        </ul>
    </li>
    <li><a href="javascript:void(0);" class="<?php echo strpos($nav_url, 'forum.php') > 0 ? 'active' : ''; ?>">Manage Forum</a>
        <ul class="title" style="<?php echo strpos($nav_url, 'forum.php') > 0 ? 'display:block;' : ''; ?>">
            <li><a href="<?php echo HOME_PATH_URL; ?>forum.php?manage" class="<?php echo strpos($nav_url, 'forum.php') > 0 && $subactive == 'manage' ? 'subactive' : '' ?>">Manage Forum</a></li>
            <li><a href="<?php echo HOME_PATH_URL; ?>forum.php?add" class="<?php echo strpos($nav_url, 'forum.php') > 0 && $subactive == 'add' ? 'subactive' : '' ?>">Add Forum</a></li>
        </ul>
    </li>
    <li><a href="javascript:void(0);" class="<?php echo strpos($nav_url, 'clogo.php') > 0 ? 'active' : ''; ?>">Manage Company Logo</a>
        <ul class="title" style="<?php echo strpos($nav_url, 'clogo.php') > 0 ? 'display:block;' : ''; ?>">
            <li><a href="<?php echo HOME_PATH_URL; ?>clogo.php?manage" class="<?php echo strpos($nav_url, 'clogo.php') > 0 && $subactive == 'manage' ? 'subactive' : '' ?>">Manage Company Logo</a></li>
            <li><a href="<?php echo HOME_PATH_URL; ?>clogo.php?add" class="<?php echo strpos($nav_url, 'clogo.php') > 0 && $subactive == 'add' ? 'subactive' : '' ?>">Add Logo</a></li>
        </ul>
    </li>
    <li><a href="javascript:void(0);" class="<?php echo strpos($nav_url, 'slider.php') > 0 ? 'active' : ''; ?>">Manage Slider Images</a>
        <ul class="title" style="<?php echo strpos($nav_url, 'slider.php') > 0 ? 'display:block;' : ''; ?>">
            <li><a href="<?php echo HOME_PATH_URL; ?>slider.php?rightBanner" class="<?php echo strpos($nav_url, 'slider.php') > 0 && $subactive == 'rightBanner' ? 'subactive' : '' ?>">Manage Right Panel Banner</a></li>
            <li><a href="<?php echo HOME_PATH_URL; ?>slider.php?manage" class="<?php echo strpos($nav_url, 'slider.php') > 0 && $subactive == 'manage' ? 'subactive' : '' ?>">Manage Slider Images</a></li>
            <li><a href="<?php echo HOME_PATH_URL; ?>slider.php?add" class="<?php echo strpos($nav_url, 'slider.php') > 0 && $subactive == 'add' ? 'subactive' : '' ?>">Add Slider Image</a></li>
        </ul>
    </li>
    <li><a href="javascript:void(0);" class="<?php echo strpos($nav_url, 'price.php') > 0 ? 'active' : ''; ?>">Manage Plan & Price</a>
        <ul class="title" style="<?php echo strpos($nav_url, 'price.php') > 0 ? 'display:block;' : ''; ?>">
            <li><a href="<?php echo HOME_PATH_URL; ?>price.php?bprice" class="<?php echo strpos($nav_url, 'price.php') > 0 && $subactive == 'bprice' ? 'subactive' : '' ?>">Manage Normal Ad Price</a></li>
            <li><a href="<?php echo HOME_PATH_URL; ?>price.php?comboprice" class="<?php echo strpos($nav_url, 'price.php') > 0 && $subactive == 'comboprice' ? 'subactive' : '' ?>">Manage Combo Ad Price</a></li>
            <li><a href="<?php echo HOME_PATH_URL; ?>price.php?add" class="<?php echo strpos($nav_url, 'price.php') > 0 && $subactive == 'add' ? 'subactive' : '' ?>">Add ad Price</a></li>
            <li><a href="<?php echo HOME_PATH_URL; ?>price.php?mprice" class="<?php echo strpos($nav_url, 'price.php') > 0 && $subactive == 'mprice' ? 'subactive' : '' ?>">Manage Membership Plan</a></li>
        </ul>
    </li>
    <li><a href="javascript:void(0);" class="<?php echo strpos($nav_url, 'artwork.php') > 0 ? 'active' : ''; ?>">Manage Artwork</a>
        <ul class="title" style="<?php echo strpos($nav_url, 'artwork.php') > 0 ? 'display:block;' : ''; ?>">
            <li><a href="<?php echo HOME_PATH_URL; ?>artwork.php?manage" class="<?php echo strpos($nav_url, 'clogo.php') > 0 && $subactive == 'manage' ? 'subactive' : '' ?>">Manage Artwork</a></li>
        </ul>
    </li>
    <li><a href="javascript:void(0);" class="<?php echo strpos($nav_url, 'service.php') > 0 ? 'active' : ''; ?>">Manage Categories (Facility Type)</a>
        <ul class="title" style="<?php echo strpos($nav_url, 'service.php') > 0 ? 'display:block;' : ''; ?>">
            <li><a href="<?php echo HOME_PATH_URL; ?>service.php?manage" class="<?php echo strpos($nav_url, 'service.php') > 0 && $subactive == 'manage' ? 'subactive' : '' ?>">Manage Service Categories</a></li>
            <li><a href="<?php echo HOME_PATH_URL; ?>service.php?add" class="<?php echo strpos($nav_url, 'service.php') > 0 && $subactive == 'add' ? 'subactive' : '' ?>">Add Service Categories</a></li>
        </ul>
    </li>
    <li><a href="javascript:void(0);" class="<?php echo (strpos($nav_url, 'pages.php') > 0 || strpos($nav_url, 'review.php') > 0 ) ? 'active' : ''; ?>">Manage Content</a>
        <ul class="title" style="<?php echo (strpos($nav_url, 'pages.php') > 0 || strpos($nav_url, 'review.php') > 0 ) ? 'display:block;' : ''; ?>">
            <li><a href="<?php echo HOME_PATH_URL; ?>pages.php?homekey" class="<?php echo strpos($nav_url, 'pages.php') > 0 && $subactive == 'homekey' ? 'subactive' : '' ?>">Manage Home Page Keys</a></li>
            <li><a href="<?php echo HOME_PATH_URL; ?>review.php?review" class="<?php echo strpos($nav_url, 'review.php') > 0 && $subactive == 'review' ? 'subactive' : '' ?>">Manage Reviews</a></li>
            <li><a href="<?php echo HOME_PATH_URL; ?>review.php?abused_review" class="<?php echo strpos($nav_url, 'review.php') > 0 && $subactive == 'abused_review' ? 'subactive' : '' ?>">Manage Reported Reviews</a></li>
            <li><a href="<?php echo HOME_PATH_URL; ?>pages.php?pages" class="<?php echo strpos($nav_url, 'pages.php') > 0 && $subactive == 'pages' ? 'subactive' : '' ?>">Manage Article</a></li>
            <li><a href="<?php echo HOME_PATH_URL; ?>pages.php?add" class="<?php echo strpos($nav_url, 'pages.php') > 0 && $subactive == 'add' ? 'subactive' : '' ?>">Add Article</a></li>
            <!--<li><a href="<?php echo HOME_PATH_URL; ?>review.php?add" class="<?php echo strpos($nav_url, 'review.php') > 0 && $subactive == 'add' ? 'subactive' : '' ?>">Add Review</a></li>-->
            <li><a href="<?php echo HOME_PATH_URL; ?>pages.php?setRating" class="<?php echo strpos($nav_url, 'pages.php') > 0 && $subactive == 'setRating' ? 'subactive' : '' ?>">Set Rating Weightage</a></li>
            <li><a href="<?php echo HOME_PATH_URL; ?>pages.php?manageDuration" class="<?php echo strpos($nav_url, 'pages.php') > 0 && $subactive == 'manageDuration' ? 'subactive' : '' ?>">Manage Duration</a></li>
            <li><a href="<?php echo HOME_PATH_URL; ?>pages.php?manageVisit" class="<?php echo strpos($nav_url, 'pages.php') > 0 && $subactive == 'manageVisit' ? 'subactive' : '' ?>">Manage Visits</a></li>
        </ul>
    </li>
    <li><a href="javascript:void(0);" class="<?php echo strpos($nav_url, 'email.php') > 0 ? 'active' : ''; ?>">Email Template Management</a>
        <ul class="title" style="<?php echo strpos($nav_url, 'email.php') > 0 ? 'display:block;' : ''; ?>">
            <li><a href="<?php echo HOME_PATH_URL; ?>email.php?manage" class="<?php echo strpos($nav_url, 'email.php') > 0 && $subactive == 'manage' ? 'subactive' : '' ?>">Manage Email Templates</a></li>
            <li><a href="<?php echo HOME_PATH_URL; ?>email.php?add" class="<?php echo strpos($nav_url, 'email.php') > 0 && $subactive == 'add' ? 'subactive' : '' ?>">Add Email Templates</a></li>
        </ul>
    </li>
    <li><a href="javascript:void(0);" class="<?php echo strpos($nav_url, 'paymentapproval.php') > 0 ? 'active' : ''; ?>">Payment Management</a>
        <ul class="title" style="<?php echo strpos($nav_url, 'paymentapproval.php') > 0 ? 'display:block;' : ''; ?>">
            <li><a href="<?php echo HOME_PATH_URL; ?>paymentapproval.php?manage" class="<?php echo strpos($nav_url, 'paymentapproval.php') > 0 && $subactive == 'manage' ? 'subactive' : '' ?>">Manage Requested Payment</a></li>
            <li><a href="<?php echo HOME_PATH_URL; ?>paymentapproval.php?dcHeader" class="<?php echo strpos($nav_url, 'paymentapproval.php') > 0 && $subactive == 'dcHeader' ? 'subactive' : '' ?>">Direct Credit Header</a></li>
        </ul>
    </li>
    <li><a href="javascript:void(0);" class="<?php echo strpos($nav_url, 'user.php') > 0 ? 'active' : ''; ?>">Manage User</a>
        <ul class="title" style="<?php echo strpos($nav_url, 'user.php') > 0 ? 'display:block;' : ''; ?>">
            <li><a href="<?php echo HOME_PATH_URL; ?>user.php?customer" class="<?php echo strpos($nav_url, 'user.php') > 0 && $subactive == 'customer' ? 'subactive' : '' ?>">Manage Customer</a></li>
            <li><a href="<?php echo HOME_PATH_URL; ?>user.php?supplier" class="<?php echo strpos($nav_url, 'user.php') > 0 && ($subactive == 'supplier' || $subactive == 'supplierDetail' || $subactive == 'extraInfo') ? 'subactive' : '' ?>">Manage Supplier</a></li>
            <li><a href="<?php echo HOME_PATH_URL; ?>user.php?xlsUpload" class="<?php echo strpos($nav_url, 'user.php') > 0 && $subactive == 'xlsUpload' ? 'subactive' : '' ?>">Supplier Upload</a></li>
            <!--<li><a href="<?php echo HOME_PATH_URL; ?>user.php?add" class="<?php echo strpos($nav_url, 'user.php') > 0 && in_array($subactive, array('add', 'edit')) ? 'subactive' : '' ?>">Add Customer/Supplier </a></li>-->
        </ul>
    </li>
    <li><a href="javascript:void(0);" class="<?php echo strpos($nav_url, 'admin.php') > 0 && in_array($subactive, array('change_password', 'manage_address', 'change_email')) ? 'active' : ''; ?>">Manage Account</a>
        <ul class="title" style="<?php echo strpos($nav_url, 'admin.php') > 0 && in_array($subactive, array('change_password', 'manage_address', 'change_email')) ? 'display:block;' : ''; ?>">
            <li><a href="<?php echo HOME_PATH_URL; ?>admin.php?change_password" class="<?php echo strpos($nav_url, 'admin.php') > 0 && $subactive == 'change_password' ? 'subactive' : '' ?>">Change Password</a></li>
            <!--<li><a href="<?php echo HOME_PATH_URL; ?>admin.php?manage_address" class="<?php echo strpos($nav_url, 'admin.php') > 0 && $subactive == 'manage_address' ? 'subactive' : '' ?>">Manage Address</a></li>-->
            <!--<li><a href="<?php echo HOME_PATH_URL; ?>admin.php?change_email" class="<?php echo strpos($nav_url, 'admin.php') > 0 && $subactive == 'change_email' ? 'subactive' : '' ?>">Change Email</a></li>-->
            <li><a href="<?php echo HOME_PATH_URL; ?>logout.php">Logout</a></li>
        </ul>
    </li>

    <li><a href="javascript:void(0);" class="<?php echo strpos($nav_url, 'location.php') > 0 ? 'active' : ''; ?>">Manage Location</a>
        <ul class="title" style="<?php echo strpos($nav_url, 'location.php') > 0 ? 'display:block;' : ''; ?>">
            <li><a href="<?php echo HOME_PATH_URL; ?>location.php?manageCity" class="<?php echo strpos($nav_url, 'location.php') > 0 && $subactive == 'manageCity' ? 'subactive' : '' ?>">Manage City</a></li>
            <li><a href="<?php echo HOME_PATH_URL; ?>location.php?addCity" class="<?php echo strpos($nav_url, 'location.php') > 0 && $subactive == 'addCity' ? 'subactive' : '' ?>">Add City</a></li>
            <li><a href="<?php echo HOME_PATH_URL; ?>location.php?manageSuburb" class="<?php echo strpos($nav_url, 'location.php') > 0 && $subactive == 'manageSuburb' ? 'subactive' : '' ?>">Manage Suburb</a></li>
            <li><a href="<?php echo HOME_PATH_URL; ?>location.php?addSuburb" class="<?php echo strpos($nav_url, 'location.php') > 0 && $subactive == 'addSuburb' ? 'subactive' : '' ?>">Add Suburb</a></li>
            <li><a href="<?php echo HOME_PATH_URL; ?>location.php?manageRestcare" class="<?php echo strpos($nav_url, 'location.php') > 0 && $subactive == 'manageRestcare' ? 'subactive' : '' ?>">Manage Restcare</a></li>
            <li><a href="<?php echo HOME_PATH_URL; ?>location.php?addRestcare" class="<?php echo strpos($nav_url, 'location.php') > 0 && $subactive == 'addRestcare' ? 'subactive' : '' ?>">Add Restcare</a></li>
        </ul>
    </li>
    <li><a href="javascript:void(0);" class="<?php echo strpos($nav_url, 'cms.php') > 0 ? 'active' : ''; ?>">Manage CMS</a>
        <ul class="title" style="<?php echo strpos($nav_url, 'cms.php') > 0 ? 'display:block;' : ''; ?>">
            <li><a href="<?php echo HOME_PATH_URL; ?>cms.php?about_us" class="<?php echo strpos($nav_url, 'cms.php') > 0 && $subactive == 'about_us' ? 'subactive' : '' ?>">Manage About Us</a></li>
            <li><a href="<?php echo HOME_PATH_URL; ?>cms.php?contact_us" class="<?php echo strpos($nav_url, 'cms.php') > 0 && $subactive == 'contact_us' ? 'subactive' : '' ?>">Manage Contact Us</a></li>

                        <li><a href="<?php echo HOME_PATH_URL; ?>cms.php?Complaint" class="<?php echo strpos($nav_url, 'cms.php') > 0 && $subactive == 'Complaint' ? 'subactive' : '' ?>">Manage Voice a Complaint</a></li>
			<li><a href="<?php echo HOME_PATH_URL; ?>cms.php?sponsors_advertising" class="<?php echo strpos($nav_url, 'cms.php') > 0 && $subactive == 'sponsors_advertising' ? 'subactive' : '' ?>">Manage Advertising</a></li>            
            <li><a href="<?php echo HOME_PATH_URL; ?>cms.php?term_con" class="<?php echo strpos($nav_url, 'cms.php') > 0 && $subactive == 'term_con' ? 'subactive' : '' ?>">Manage Term&amp;Condition </a></li>
            <li><a href="<?php echo HOME_PATH_URL; ?>cms.php?manage_FAQ" class="<?php echo strpos($nav_url, 'cms.php') > 0 && $subactive == 'manage_FAQ' ? 'subactive' : '' ?>">Manage FAQs</a></li>
            <li><a href="<?php echo HOME_PATH_URL; ?>cms.php?add_Faq" class="<?php echo strpos($nav_url, 'cms.php') > 0 && $subactive == 'add_Faq' ? 'subactive' : '' ?>">Add FAQ</a></li>
            <li><a href="<?php echo HOME_PATH_URL; ?>cms.php?awards" class="<?php echo strpos($nav_url, 'cms.php') > 0 && $subactive == 'awards' ? 'subactive' : '' ?>">Awards</a></li>
        </ul>
    </li>
	  <?php 
	/*
	* code for Manage community
	* from admin section 
	*/
	?>
    <!-- code for Mange Community page from admin section -->
    <li><a href="javascript:void(0);" class="<?php echo strpos($nav_url, 'community_logo.php') > 0 ? 'active' : ''; ?>">Manage Community</a>
        <ul class="title" style="<?php echo strpos($nav_url, 'community_logo.php') > 0 ? 'display:block;' : ''; ?>">
        <li><a href="<?php echo HOME_PATH_URL; ?>community_logo.php?manage" class="<?php echo strpos($nav_url, 'community_logo.php') > 0 && $subactive == 'manage' ? 'subactive' : '' ?>">Manage Community Logo</a></li>
            <li><a href="<?php echo HOME_PATH_URL; ?>community_logo.php?add" class="<?php echo strpos($nav_url, 'community_logo.php') > 0 && $subactive == 'add' ? 'subactive' : '' ?>">Add Community Logo</a></li>
        </ul>
    </li>
    
    
    
        <li><a href="javascript:void(0);" class="<?php echo strpos($nav_url, 'sponsors.php') > 0 ? 'active' : ''; ?>">Manage Sponsors</a>
        <ul class="title" style="<?php echo strpos($nav_url, 'sponsors.php') > 0 ? 'display:block;' : ''; ?>">
        <li><a href="<?php echo HOME_PATH_URL; ?>sponsors.php?manage" class="<?php echo strpos($nav_url, 'sponsors.php') > 0 && $subactive == 'manage' ? 'subactive' : '' ?>">Manage Sponsors Logo</a></li>
            <li><a href="<?php echo HOME_PATH_URL; ?>sponsors.php?add" class="<?php echo strpos($nav_url, 'sponsors.php') > 0 && $subactive == 'add' ? 'subactive' : '' ?>">Add Sponsors Logo</a></li>
        </ul>
    </li>
    
     <li><a href="javascript:void(0);" class="<?php echo strpos($nav_url, 'event.php') > 0 ? 'active' : ''; ?>">Manage Events</a>
        <ul class="title" style="<?php echo strpos($nav_url, 'event.php') > 0 ? 'display:block;' : ''; ?>">
        <li><a href="<?php echo HOME_PATH_URL; ?>event.php?manage" class="<?php echo strpos($nav_url, 'event.php') > 0 && $subactive == 'manage' ? 'subactive' : '' ?>">Manage Events</a></li>
            <li><a href="<?php echo HOME_PATH_URL; ?>event.php?add" class="<?php echo strpos($nav_url, 'event.php') > 0 && $subactive == 'add' ? 'subactive' : '' ?>">Add Events</a></li>
        </ul>
    </li>
     <!---code for category list -->
    <li><a href="javascript:void(0);" class="<?php echo strpos($nav_url, 'category_list.php') > 0 ? 'active' : ''; ?>">Manage Consumer Information</a>
        <ul class="title" style="<?php echo strpos($nav_url, 'category_list.php') > 0 ? 'display:block;' : ''; ?>">
        <li><a href="<?php echo HOME_PATH_URL; ?>category_list.php?manage" class="<?php echo strpos($nav_url, 'category_list.php') > 0 && $subactive == 'manage' ? 'subactive' : '' ?>">Manage Consumer Information</a></li>
            <li><a href="<?php echo HOME_PATH_URL; ?>category_list.php?add" class="<?php echo strpos($nav_url, 'category_list.php') > 0 && $subactive == 'add' ? 'subactive' : '' ?>">Add Consumer Information</a></li>
        </ul>
    </li>

     <!--end of function code for category list -->
    
    
    <!--end code for Mange Community page from admin section-->
    <li><a href="javascript:void(0);" class="<?php echo strpos($nav_url, 'reminder.php') > 0 ? 'active' : ''; ?>">Manage Suppliers Reminder Email</a>
        <ul class="title" style="<?php echo strpos($nav_url, 'cms.php') > 0 ? 'display:block;' : ''; ?>">
            <li><a href="<?php echo HOME_PATH_URL; ?>reminder.php?send_mail" class="<?php echo strpos($nav_url, 'reminder.php') > 0 && $subactive == 'send_mail' ? 'subactive' : '' ?>">Manage Suppliers Reminder Email</a></li>

        </ul>
    </li>
    <li><a href="<?php echo HOME_PATH_URL; ?>managecpi.php" class="<?php echo strpos($nav_url, 'managecpi.php') > 0 ? 'active' : ''; ?>">Manage CPI</a>
        
    </li>
    
    <li><a href="javascript:void(0);" class="<?php echo strpos($nav_url, 'provider_comparison.php') > 0 ? 'active' : ''; ?>">Provider Comparison</a>
        <ul class="title" style="<?php echo strpos($nav_url, 'provider_comparison.php') > 0 ? 'display:block;' : ''; ?>">
            <li><a href="<?php echo HOME_PATH_URL; ?>provider_comparison.php?procom" class="<?php echo strpos($nav_url, 'provider_comparison.php') > 0 && $subactive == 'procom' ? 'subactive' : '' ?>">Manage Provider Comparison</a></li>
            
        </ul>
    </li>
    
   

</ul>
