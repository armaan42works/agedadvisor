<?php
/*********************Manage Demand Page**********************
***Find Total record
***Find Cubic Calculations
***Decide no of records show per page
***Edit Demand Soil 
***Delete Demand Soil
***User Detail regarding Soil 
***************************************************************/
 global $db;
    //pr($_SESSION);
    ini_set('display_errors', 0);
    switch ($action) {
        case 'leave':
            leavedata($pageNo, $extra);
            break;
        case 'absent':
            absent_emp($pageNo, $extra);
            break;
        case 'present':
            present_emp($pageNo, $extra);
            break;
        case 'userdata':
            ajax_data($pageNo, $extra);
            break;
        case 'searchuserdata':
            ajax_search_data($pageNo, $extra);
            break;
        case 'change_authority':
            change_authority();
            break;
         case 'change_flag':
            change_flag();
            break;
        case 'change_status' :
            change_status();
            break;
            
    }    
    
$number_of_results = 5;
$total_record			=	mysqli_query($db->db_connect_id,"Select ud.* , ams.soil_type as st , uf.used_for as used_for_s from user_demand as ud left join admin_manage_cms as ams on ud.soil_type = ams.manage_id left join used_for as uf on ud.used_for = uf.id")or die(__FILE__.mysqli_errno($db->db_connect_id));
$totals					=	mysqli_num_rows($total_record)."<br>";
$cubic_result			=	mysqli_query($db->db_connect_id,"select sum(embankment)as emb,sum(surface) as surface from user_demand")or die(__FILE__.mysqli_errno($db->db_connect_id));
$count					= 	mysqli_num_rows($total_record);
$cubic_data				= 	mysqli_fetch_array($cubic_result);
$cubic					=	($cubic_data['emb'])*($cubic_data['surface']);
	
/************************************************************/?>
<table align="center" border="0" cellpadding="0" cellspacing="0"
	width="95%">
	<tbody>
		<tr>
			<td height="10">&nbsp;</td>

		</tr>
		<tr>
			<td>
			<div class="breadcrumb"><a href="<?php echo HOME_PATH_URL;?>admin.php">Home </a>>><b>Manage user</b></div>
			</td>
		</tr>
		
		<tr>

			<td align="left">&nbsp;</td>
		</tr>
		
		
		
		<tr>
			<td bgcolor="#dedede" height="1"><img src="<?php echo ADMIN_IMAGE;?>sp.gif"
				height="1" width="1"></td>
		</tr>
		
		<tr>

			<td align="left">Total Records:<?php echo $totals;?></td>
		</tr>
		
		

		<tr>
			<td>
			<div style="color: #FF0000"></div>

			</td>
		</tr>
		

		<tr>
			<td height="1"><img src="<?php echo ADMIN_IMAGE;?>sp.gif" height="1" width="1"></td>

		</tr>
		
		
		<tr>
			<td class="bdrTab">
			<table border="0" cellpadding="3" cellspacing="1" width="100%">
				<tbody>
					<tr>
		<td  class="bgCol blu bgBlk" width="25%">
		<a href="<?php echo MAIN_PATH."/admin.php?demand&name=soil_type&order=".($_REQUEST['order']!=''?($_REQUEST['order']=="DESC"?"ASC":"DESC"):"DESC");?>">Soil Type</a>
		</td>
		<td class="bgCol blu bgBlk">
		<a href="<?php echo MAIN_PATH."/admin.php?demand&name=used_for&order=".($_REQUEST['order']!=''?($_REQUEST['order']=="DESC"?"ASC":"DESC"):"DESC")?>">Used For</a>
		</td>
		<td  class="bgCol blu bgBlk" width="25%">
		<a href="<?php echo MAIN_PATH."/admin.php?demand&name=del_address&order=".($_REQUEST['order']!=''?($_REQUEST['order']=="DESC"?"ASC":"DESC"):"DESC");?>">Delivery Address</a>
		</td>
		<td class="bgCol blu bgBlk">
		<a href="<?php echo MAIN_PATH."/admin.php?demand&name=del_date_from&order=".($_REQUEST['order']!=''?($_REQUEST['order']=="DESC"?"ASC":"DESC"):"DESC");?>">Delivery Date</a>
		</td>

 		<td class="bgCol blu">Actions</td>
 	</tr>
					<!-- sorting starts here -->
				<?php 
			if(isset($_SESSION['name'])&&!empty($_SESSION['name'])){
					
				$name	=$_SESSION['name'];
				$order	=$_SESSION['order'];
					
			}
			else{
					$name	=del_date_from;
					$order	='DESC';	
			}
			/************sorting end here********************/
			/************Paging starts here******************/
			if($_REQUEST['page']>1){
				$limit	="limit ".($page-1)*$number_of_results.","."$number_of_results";
			}
			else{
				$limit	="limit 0,".$number_of_results;
			}
			/************Paging ends here******************/
            $query = mysqli_query($db->db_connect_id,"Select ud.* , ams.soil_type as st , uf.used_for as used_for_s from user_demand as ud left join admin_manage_cms as ams on ud.soil_type = ams.manage_id left join used_for as uf on ud.used_for = uf.id order by $name $order $limit");         
			while($result=mysqli_fetch_array($query))
		   {	
		   	
		   	?>
				
 	 						<tr class="r1">
					
							<td><?php echo $result['st'];?></td>
							<td><?php echo $result['used_for_s'];?></td>
							<td><?php echo $result['del_address'];?></td>
							<td><?php echo $result['del_date_from'];?></td>
							<td><a href="javascript:confirmdelete('admin.php?delete_demand=<?php echo $result['id'];?>')">Delete</a>
							/ <a href="admin.php?edit_demand=<?php echo $result['id'];?>">Edit</a> / 
							<a href="<?php echo HOME_PATH_URL;?>admin.php?user_detail&user_id=<?php echo $result['user_id'];?>">User_detail</a>
							</td>
						</tr>
				<?php }?>
						 									 							
			</td>
				</tbody>
			</table>

			<table class="" border="0" cellpadding="0" cellspacing="6">
				<tbody>
				<?php 
				$count	=	mysqli_num_rows($total_record);
				$page	=	ceil($count/$number_of_results);
				?>
					<tr>
				<?php for($i=1;$i<=$page;$i++){?>
						<td class=""><span>
						<?php 
							if($_GET['page'] == $i)
							{
								echo "<b><i>$i</i></b> |";
							}
							else
							{
							?>
							<a href="<?php echo MAIN_PATH."/admin.php?demand&page=".$i?>"><u><?php echo $i;?></u></a>|
						</span>
							<?php 	
							}
						?>
						</td>
						<?php }?>
					</tr>
				</tbody>
			</table>
			</td>
		</tr>
	</tbody>
</table>

