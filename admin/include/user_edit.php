<?php 
/*****************************************
 * User edit page 
 *****************************************/
if(isset($_REQUEST['profile'])){
		$id			=	$_REQUEST['id'];
		$name		=	$_REQUEST['name'];
		$address	=	$_REQUEST['address'];
		$postcode	=	$_REQUEST['postcode'];
		$telecom	=	$_REQUEST['telecom'];
		$fax		=	$_REQUEST['fax'];
		$email		=	$_REQUEST['email'];
		$password	=	$_REQUEST['password'];
		$new_password	=	$_REQUEST['new_password'];
		$email_check= 	mysqli_query($db->db_connect_id,"select *from user_registration where email='{$email}' and id!='{$id}'");
		$result		=	mysqli_fetch_array($email_check);
		
		if((mysqli_num_rows($email_check)<1)&&($result['password']!=$password))
		{	
			$query	=	"update user_registration set name='{$name}',address='{$address}',postcode='{$postcode}',telecom='{$telecom}',fax='{$fax}',email='{$email}',password='{$new_password}' where id='{$id}'";
			$update	=	mysqli_query($db->db_connect_id,$query);
			header("Location:user.php?profile_edit_msg");
		}
		else{
			$duplicate	=	"Please use different email or current password does not match!";
		}
}
		$result_data	=	mysqli_query($db->db_connect_id,"select *from user_registration where email='{$_SESSION['email']}'");
		$result			=	mysqli_fetch_array($result_data);?>
			
<form name="form1" action="" method="post" onsubmit="return user_edit();">
<table cellpadding="0" cellspacing="0" width="90%" >
<tbody>
<?php if(isset($duplicate)&&!empty($duplicate)){?>
<div style="font-family: sans-serif;size: 14px;color: green;"><?php echo $duplicate;?></div>
<?php }?>
<input type="hidden" name="id" value="<?php echo $result['id'];?>">
<tr><td><span class="redCol">* </span><?php echo $value = translation('Name',$lang_file);?></td>
<td><input type="text" name="name" id="name" value="<?php echo $result['name'];?>" maxlength="30" size="30" class="loginTextbox"/></td></tr>

<tr><td><span class="redCol">* </span><?php echo $value = translation('Address',$lang_file);?></td>
<td><input type="text" name="address" id="address" value="<?php echo $result['address'];?>" maxlength="50" size="30" class="loginTextbox"/></td></tr>

<tr><td><span class="redCol">* </span><?php echo $value = translation('Postcode',$lang_file);?></td>
<td><input type="text" name="postcode" id="postcode" value="<?php echo $result['postcode'];?>" maxlength="30" size="30" class="loginTextbox"/></td></tr>

<tr><td><span class="redCol">* </span><?php echo $value = translation('Telecom',$lang_file);?></td>
<td><input type="text" name="telecom" id="telecom" value="<?php echo $result['telecom'];?>" maxlength="30" size="30" class="loginTextbox"/></td></tr>

<tr><td><span class="redCol">* </span><?php echo $value = translation('Fax',$lang_file);?></td>
<td><input type="text" name="fax" id="fax" value="<?php echo $result['fax'];?>" maxlength="30" size="30" class="loginTextbox"/></td></tr>

<tr><td><span class="redCol">* </span><?php echo $value = translation('Email',$lang_file);?></td>
<td><input type="text" name="email" id="email" value="<?php echo $result['email'];?>" maxlength="30" size="30" class="loginTextbox"/></td></tr>

<tr><td><span class="redCol">* </span><?php echo $value = translation('Current Password',$lang_file);?></td>
<td><input type="password" name="password" id="password" maxlength="30" size="30" class="loginTextbox"/></td></tr>

<tr><td><span class="redCol">* </span><?php echo $value = translation('New Password',$lang_file);?></td>
<td><input type="password" name="new_password" id="new_password"  maxlength="30" size="30" class="loginTextbox"/></td></tr>

<tr><td><span class="redCol">* </span><?php echo $value = translation('Confirm Password',$lang_file);?></td>
<td><input type="password" name="confirm_password" id="confirm_password" maxlength="30" size="30" class="loginTextbox"/></td></tr>

<tr><td>&nbsp;</td><td>&nbsp;</td></tr>
<tr><td>&nbsp;&nbsp;</td><td><input type="submit" name="profile" value="submit" class="button1"></td></tr>
</tbody>
</table>
</form>