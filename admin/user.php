<?php
/* * **************ADMIN HOME PAGE*****************************
  MODULE INCLUDED
  1.MANAGE OFFER
  2.MANAGE DEMAND
  3.CHANGE PASSWORD
  4.LOGOUT
 * ********************************************* */
?>
<?php
include("../application_top.php");
include(INCLUDE_PATH . "header.php");
?>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tbody>
        <tr>
            <td class="lt_nav" valign="top" width="17%">
                <!-- left navigation -->
                <?php include(INCLUDE_PATH . "left_navigation.php"); ?>
                <!-- left navigation ends here -->
            </td>
            <td valign="top">
                <!-- content area here -->
                <div id="body_content">
                    <?php
                    switch (key($_REQUEST)) {
                        case 'add' :
                            include(MODULE_PATH . "user/add.php");
                            break;
                        case 'customer' :
                            include(MODULE_PATH . "user/customer.php");
                            break;
                        case 'supplier' :
                            include(MODULE_PATH . "user/supplier.php");
                            break;
                        case 'supplierDetail' :
                            include(MODULE_PATH . "user/supplier_detail.php");
                            break;
                        case 'edit' :
                            include(MODULE_PATH . "user/add.php");
                            break;
                        case 'editProduct' :
                            include(MODULE_PATH . "user/addProduct.php");
                            break;
                        case 'editSupplier' :
                            include(MODULE_PATH . "user/editUsers.php");
                            break;
                        case 'editCustomer' :
                            include(MODULE_PATH . "user/editUsers.php");
                            break;
                        case 'xlsUpload' :
                            include(MODULE_PATH . "user/xlsUpload.php");
                            break;
                        case 'extraInfo' :
                            include(MODULE_PATH . "user/extraInfo.php");
                            break;
                        default :
                            break;
                    }
                    ?>
                </div>
                <!-- content area ends here -->
            </td>
        </tr>
    </tbody>
</table>


