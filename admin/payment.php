<?php
    /*
     * Objective : Redirection of the p[ages related to payment
     * Filename : payment.php
     * Created By : Sanket Khanna <sanket.khanna@ilmp-tech.com>
     * Created On : 21 August 2014
     */
    include("../application_top.php");
    include(INCLUDE_PATH . "header.php");
?>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tbody>
        <tr>
            <td class="lt_nav" height="455" valign="top" width="17%">
                <!-- left navigation -->
                <?php include(INCLUDE_PATH . "left_navigation.php"); ?>
                <!-- left navigation ends here -->
            </td>
            <td valign="top">
                <!-- content area here -->
                <div id="body_content">
                    <?php
                        switch (key($_REQUEST)) {
                           case 'manage':
                                include(MODULE_PATH . "payment/manage.php");
                                break;
                            case 'detail':
                                include(MODULE_PATH . "payment/detail.php");
                                break;
                            default:
                                include(MODULE_PATH . "payment/manage.php");
                                break;
                        }
                    ?>
                </div>
                <!-- content area ends here -->
            </td>
        </tr>
    </tbody>
</table>

