<?php

    /*
     * Objective : Check Captcha
     * Filename : captcha.php
     * Created By : Sanket Khanna <sanket.khanna@ilmp-tech.com>
     * Created On : 13 August 2014
     */
    session_start();

    if (isset($_REQUEST['code'])) {
        echo json_encode(strtolower($_REQUEST['code']) == strtolower($_SESSION['captcha']));
    } else {
        echo 0; // no code
    }
?>