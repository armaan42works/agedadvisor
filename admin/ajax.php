<?php

/*
 * Objective : Using this file for all the ajax call
 * Filename : ajax.php
 * Created By : Sanket Khanna <sanket.khanna@ilmp-tech.com>
 * Created On : 4 August 2014
 * Modified : 14 August 2014
 */
include("../application_top.php");
global $db;
$sp_id = $_SESSION['id'];

switch ($_POST['action']) {
    //geenerate Coupon Code for gift card in admin coupon section
    case 'generateGiftCode':
        //geenerate Coupon Code
        echo generateGiftCode();
        break;
    // state list on front/admin
    case 'status' :
        //use this for status change
        echo status($table, $id, $status);
        break;
    case 'abuse' :
        //use this for status change
        echo abuse($table, $id, $abuse);
        break;
    case 'approval' :
        //use this for approved
        echo approval($table, $id, $approval);
        break;
    case 'verified' :
        //use this for approved
        echo verified($table, $id, $verified);
        break;
    case 'archive' :
        //use this for archive
        echo archive($table, $id, $archive);
        break;
    // state list on front/admin
    case "stateList" :
        // state list
        echo state($country);
        break;
    case "suburbList" :
        // state list
        echo suburb($city);
        break;
    // Delete record in admin panel
    case "delete" :
        echo delete($table, $id);
        break;
    // Remove Flagged Remark
    case "removeFlag" :
        echo removeFlag($table, $id);
        break;
    //Convert the Quotes to Project in manage quotes
    case "convert" :
        echo convertQuotetoProject($table, $id);
        break;
    // Close the Quotes
    case "close":
        echo close($table, $id);
        break;
    // Update The Progress of Project
    case "updateProgress":
        echo updateProgress($id, $progress);
        break;
    // Chnage the Project to Complete
    case "complete":
        echo complete($table, $id);
        break;
    // Get the record of the coupon
    case "checkCoupon":
        echo checkCoupon($value);
        break;
    // Check whether the coupon code exist or not
    case "checkCouponCode":
        echo checkCouponCode($code);
        break;
    // Change the order of the services
    case "order":
        echo serviceOrder($table, $id, $order, $type);
        break;
    case "orderCat":
        echo categoryOrder($id, $order, $type);
        break;
    //Delete services
    case "deleteService":
        echo deleteService($id, $order);
        break;
    case "deleteCatogory":
        echo deleteCatogory($id, $order);
        break;
    //Check whether the Email exist or not
    case "sendMail":
        echo sendMail($id);
        break;
    //Check whether the Email exist or not
    case "unquieEmail":
        echo unquieEmail($email);
        break;
    //Check whether the Phone exist or not
    case "uniquePhone":
         echo uniquePhone($phone);
         break; 
    //Look for new email that does not exist.
    case "lookUnquieEmail":
        echo lookUnquieEmail($email);
        break;
    //Look for new email that does not exist.
    case "Max2Email":
        echo Max2Email($email);
        break;
    //Look for new user name that does not exist.
    case "lookUnquieUsername":
        echo lookUnquieUsername($username);
        break;
    //Check whether the Supplier Email exist or not
    case "unquieSupplierEmail":
        echo unquieSupplierEmail($email);
        break;
    //Check whether the Customer Email exist or not
    case "unquieCustomerEmail":
        echo unquieCustomerEmail($email);
        break;
    //Check whether the User name exist or not
    case "unquieUsername":
        echo unquieUsername($username);
        break;
    //Check whether the Admin Email exist or not
    case "adminEmail":
        echo adminEmail($username);
        break;
    //change Paid unpaid
    case "paid":
        echo payment($table, $id, $paid);
        break;
    //Check minimum budget
    case "minBudget":
        echo minBudget($min_amount);
        break;
    //Check maximum budget
    case "maxBudget":
        echo maxBudget($max_amount);
        break;
    //check coupon exist or not
    case "couponExist":
        echo couponExist($promocode, $type);
        break;
    //check sales id is valid or not
    case "validSalesId":
        echo validSalesId($cp_sales_id);
        break;
    //check client id is valid or not
    case "validClientId":
        echo validClientId($cp_client_id);
        break;
    //check email id exist is valid or not
    case "emailExist":
        echo emailExist($email);
        break;
    //delete admin quotes uploaded file
    case "deleteFileName":
        echo deleteFileName($id);
        break;
    //delete client quotes uploaded file
    case "deleteFile":
        echo deleteFile($id);
        break;
    case "deletePC":
        echo deletePC($id);
        break;
    case "productList":
        echo products($supplier);
        break;
    case 'payment_approval' :
        //use this for payment_approval by admin
        echo payment_approval($table, $id, $plan_id, $product_id, $user_id, $amount, $approval);
        break;
}

function generateGiftCode() {
    global $db;
    $characters = 'abcdefghijklmnopqrstuvwyzABCDEFGHIJKLMNOPQRSTUVWYZ0123456';
    $randomString = '';
    for ($i = 0; $i < 10; $i++) {
        $randomString .= $characters[rand(0, strlen($characters) - 1)];
    }
    $number = "0123456";
    for ($i = 0; $i < 3; $i++) {
        $randomString .= $number[rand(0, strlen($number) - 1)];
    }
    $checkCode = "SELECT code FROM " . _prefix("coupons") . " where code = '$randomString'";
    $result = $db->sql_query($checkCode);
    $num = $db->sql_numrows($result);
    if ($num > 0) {
        generateGiftCode();
    }
    return 'KA' . $randomString;
}

function status($table, $id, $status) {
    global $db;
    $changeStatus = $status == 1 ? 0 : 1;
    $UpdateSql = "UPDATE " . _prefix("$table") . " SET status = '$changeStatus' WHERE id= $id";
    $updateResult = $db->sql_query($UpdateSql);
    if ($updateResult) {
        $CurrentStatus = $changeStatus == 0 ? '<img src="' . ADMIN_IMAGE . 'deactivate.png" title="Inactive" />' : '<img src="' . ADMIN_IMAGE . 'activate.png" title="active"/>';
        echo "<a href='javascript:void(0);' id='$table-" . $id . "-" . $changeStatus . "' class='status'>$CurrentStatus</a>";
    }
}

function abuse($table, $id, $abuse) {
    global $db;
    $changeStatus = $abuse == 1 ? 0 : 1;
    $UpdateSql = "UPDATE " . _prefix("$table") . " SET abused = '$changeStatus' WHERE id= $id";
    $updateResult = $db->sql_query($UpdateSql);
    if ($updateResult) {
        $CurrentStatus = $changeStatus == 0 ? '<img src="' . ADMIN_IMAGE . 'deactivate.png" title="Inactive" />' : '<img src="' . ADMIN_IMAGE . 'activate.png" title="active"/>';
        echo "<a href='javascript:void(0);' id='$table-" . $id . "-" . $changeStatus . "' class='abuse'>$CurrentStatus</a>";
    }
}

function approval($table, $id, $approval) {
    echo $table;
    echo $id;
    echo $approval;
    global $db;
    $changeStatus = $approval == 0 ? 1 : 1;
    $UpdateSql = "UPDATE " . _prefix("$table") . " SET approve_status = '$changeStatus' WHERE id= $id";
    $updateResult = $db->sql_query($UpdateSql);
    if ($updateResult) {
        $CurrentStatus = $approved == 1 ? 'Disapproved <img src="' . ADMIN_IMAGE . 'deactivate.png" title="Disapproved" />' : 'Approved <img src="' . ADMIN_IMAGE . 'activate.png" title="Approved"/>';
        echo "<a href='javascript:void(0);' id='$table-" . $id . "-" . $changeStatus . "' class='approval'>$CurrentStatus</a>";
    }
}

function verified($table, $id, $verified) {
    global $db;
    $changeStatus = $verified == 0 ? 1 : 1;
    $UpdateSql = "UPDATE " . _prefix("$table") . " SET approve_status = '$changeStatus' WHERE id= $id";
    $updateResult = $db->sql_query($UpdateSql);
    if ($updateResult) {
        $CurrentStatus = $verified == 1 ? 'Unverified<img src="' . ADMIN_IMAGE . 'deactivate.png" title="Unverified" />' : 'Verified <img src="' . ADMIN_IMAGE . 'activate.png" title="Verified"/>';
        echo "<a href='javascript:void(0);' id='$table--" . $id . "--" . $changeStatus . "' class='verified'>$CurrentStatus</a>";
    }
}

function archive($table, $id, $archive) {
    global $db;
    $changeStatus = $archive == 0 ? 1 : 1;
    $UpdateSql = "UPDATE " . _prefix("$table") . " SET archive = '$changeStatus' WHERE id= $id";
    $updateResult = $db->sql_query($UpdateSql);
    if ($updateResult) {
        $CurrentStatus = $archive == 0 ? ' <i style="font-size:20px;" title="Archieved" class="fa fa-archive"></i>' : '<img src="' . ADMIN_IMAGE . 'activate.png" title="Archive"/>';
        echo "<a href='javascript:void(0);' id='$table--" . $id . "--" . $changeStatus . "' class='archive'>$CurrentStatus</a>";
    }
}

function state($counrty) {
    echo stateList($counrty);
    die();
}

function suburb($city) {
    echo suburbList($city);
    die();
}

function delete($table, $id) {
    global $db;
    $updateSql = "UPDATE " . _prefix("$table") . " SET deleted = '1' WHERE id= $id";
    $updateResult = $db->sql_query($updateSql);
    if ($table == 'point_earning_setting') {
        $insert_data = "UPDATE " . _prefix("$table") . " SET referral_point='',referral_value = '', social_point = '',social_value = '', "
                . "min_value = '', client_point = '', client_value = '' WHERE id = $id";
        $db->sql_query($insert_data);
    }
}

function removeFlag($table, $id) {
    global $db;
    $updateSql = "UPDATE " . _prefix("$table") . " SET abused_request = 0 AND abuse_reason ='' AND abuse_date='0000-00-00 00:00:00' WHERE id= $id";
    $updateResult = $db->sql_query($updateSql);
}

function close($table, $id) {
    global $db;
    $updateSql = "UPDATE " . _prefix("$table") . " SET status = '3' WHERE id= $id";
    $updateResult = $db->sql_query($updateSql);
}

function convertQuotetoProject($table, $id) {
    global $db;
    $updateSql = "UPDATE " . _prefix("$table") . " SET status  = '1', modified=NOW() WHERE id= $id";
    $updateResult = $db->sql_query($updateSql);
}

function updateProgress($id, $progress) {
    global $db;
    $sql_query = "update " . _prefix("quotes") . " set progress = '$progress', progress_modified = now() where id = '$id'";
    $res = $db->sql_query($sql_query);
}

function complete($table, $id) {
    global $db;
    $updateSql = "UPDATE " . _prefix("$table") . " SET status  = '2', modified=NOW() WHERE id= $id";
    $updateResult = $db->sql_query($updateSql);
}

function checkCoupon($value) {
    global $db;
    $sql_query = "select * from " . _prefix("coupons") . " where id = '$value'";
    $res = $db->sql_query($sql_query);
    if ($db->sql_numrows($res) > 0) {
        $data = sql_fetchrow($res);
        $record = $data['subtype'] . '#@#' . $data['value'] . '#@#' . date('M d, Y', strtotime($data['expire_date']));
    }
    echo $record;
}

function checkCouponCode($code) {
    global $db;
    $sql_query = "select code from " . _prefix("coupons") . " where code = '$code'";
    $res = $db->sql_query($sql_query);
    if ($db->sql_numrows($res) > 0) {
        echo '1';
    } else {
        echo '0';
    }
}

function serviceOrder($table, $id, $order, $type) {
    global $db;
    if ($type == 'down') {
        $changeOrder = $order - 1;
        $updateOrder = "UPDATE " . _prefix("$table") . " SET order_by='$order' WHERE order_by = '$changeOrder'";
        $db->sql_query($updateOrder);
        $updateOrderCurrent = "UPDATE " . _prefix("$table") . " SET order_by = '$changeOrder' WHERE id = '$id'";
        $db->sql_query($updateOrderCurrent);
    }
    if ($type == 'up') {
        $changeOrder = $order + 1;

        $updateOrder = "UPDATE " . _prefix("$table") . " SET order_by='$order' WHERE order_by = '$changeOrder'";
        $db->sql_query($updateOrder);
        $updateOrderCurrent = "UPDATE " . _prefix("$table") . " SET order_by = '$changeOrder' WHERE id = '$id'";
        $db->sql_query($updateOrderCurrent);
    }
}

function categoryOrder($id, $order, $type) {
    global $db;
    if ($type == 'down') {
        $changeOrder = $order + 1;
        $updateOrder = "UPDATE " . _prefix("categories") . " SET order_by='$order' WHERE order_by = '$changeOrder'";
        $db->sql_query($updateOrder);
        $updateOrderCurrent = "UPDATE " . _prefix("categories") . " SET order_by = '$changeOrder' WHERE id = '$id'";
        $db->sql_query($updateOrderCurrent);
    }
    if ($type == 'up') {
        $changeOrder = $order - 1;

        $updateOrder = "UPDATE " . _prefix("categories") . " SET order_by='$order' WHERE order_by = '$changeOrder'";
        $db->sql_query($updateOrder);
        $updateOrderCurrent = "UPDATE " . _prefix("categories") . " SET order_by = '$changeOrder' WHERE id = '$id'";
        $db->sql_query($updateOrderCurrent);
    }
}

function deleteService($id, $order) {
    global $db;
    $deleteRecord = "UPDATE " . _prefix("services") . " SET deleted = '1', order_by = '0' WHERE id = '$id'";
    if ($db->sql_query($deleteRecord)) {
        $getMax = "SELECT MAX(order_by) as count FROM " . _prefix("services") . " WHERE status = 1 && deleted = 0";
        $resMax = $db->sql_query($getMax);
        $dataMax = $db->sql_fetchrow($resMax);
        for ($i = $order; $i <= $dataMax['count']; $i++) {
            $changeOrder = $i - 1;
            $updateOrder = "UPDATE " . _prefix("services") . " SET order_by = '$changeOrder' WHERE order_by = '$i'";
            $db->sql_query($updateOrder);
        }
    }
}

function deleteCatogory($id, $order) {
    global $db;
    $deleteRecord = "UPDATE " . _prefix("categories") . " SET deleted = '1', order_by = '0' WHERE id = '$id'";
    if ($db->sql_query($deleteRecord)) {
        $getMax = "SELECT MAX(order_by) as count FROM " . _prefix("categories") . " WHERE status = 1 && deleted = 0";
        $resMax = $db->sql_query($getMax);
        $dataMax = $db->sql_fetchrow($resMax);
        for ($i = $order; $i <= $dataMax['count']; $i++) {
            $changeOrder = $i - 1;
            $updateOrder = "UPDATE " . _prefix("categories") . " SET order_by = '$changeOrder' WHERE order_by = '$i'";
            $db->sql_query($updateOrder);
        }
    }
}

function uniquePhone($phone) {
    global $db;
    $checkPhone = "Select id from " . _prefix("users") . " where phone like '$phone' AND password !='' ";
    $resCheckPhone = $db->sql_query($checkPhone);
    if ($db->sql_numrows($resCheckPhone) > 0) {
        
        return 0;
    } else {
       
        return json_encode($phone == $phone);
    }
}

function unquieEmail($email) {
    global $db;
    $checkEmail = "Select id from " . _prefix("users") . " where email like '$email' AND password !=''";
    $resCheckEmail = $db->sql_query($checkEmail);
    if ($db->sql_numrows($resCheckEmail) > 0) {
        return 0;
    } else {
        return json_encode($email == $email);
    }
}

//Look for a new email that does not exist
function lookUnquieEmail($email) {
    $sp_id = $_SESSION['id'];
    global $db;
    $checkEmail = "Select id from " . _prefix("users") . " where (email like '$email' AND deleted=0 AND id!=$sp_id)";
    $resCheckEmail = $db->sql_query($checkEmail);
    if ($db->sql_numrows($resCheckEmail) > 0) {
        return 0;
    } else {
        return json_encode($email == $email);
    }
}

function Max2Email($email) {
    $sp_id = $_SESSION['id'];
    global $db;
    $checkEmail = "Select id from " . _prefix("users") . " where (email like '$email' AND deleted=0 AND id!=$sp_id)";
    $resCheckEmail = $db->sql_query($checkEmail);
    if ($db->sql_numrows($resCheckEmail) > 2) {
        return 0;
    } else {
        return json_encode($email == $email);
    }
}

//Unique Supplier Email
function unquieSupplierEmail($email) {
    global $db;
    $checkEmail = "Select id from " . _prefix("users") . " where deleted=0 AND email like '$email'";
    $resCheckEmail = $db->sql_query($checkEmail);
    if ($db->sql_numrows($resCheckEmail) > 0) {
        return json_encode($email == $email);
    } else {
        return 0;
    }
}

//Unique Customer Email
function unquieCustomerEmail($email) {
    global $db;
    $checkEmail = "Select id from " . _prefix("users") . " where  deleted=0 AND email like '$email'";
    $resCheckEmail = $db->sql_query($checkEmail);
    // return $db->sql_numrows($resCheckEmail);
    if ($db->sql_numrows($resCheckEmail) > 0) {
        return json_encode($email == $email);
    } else {
        return 0;
    }
}

//Check for unique user name
function unquieUsername($username) {
    global $db;
    $checkUsername = "Select id from " . _prefix("users") . " where user_name like '$username' AND password!=''";
    $resCheckUsername = $db->sql_query($checkUsername);
    if ($db->sql_numrows($resCheckUsername) > 0) {
        return 0;
    } else {
        return json_encode($username == $username);
    }
}

//Look for a new user name that does not exist
function lookUnquieUsername($username) {
    $sp_id = $_SESSION['id'];
    global $db;
    $checkUsername = "Select id from " . _prefix("users") . " where user_name like '$username' AND deleted=0 AND id!=$sp_id";
    $resCheckUsername = $db->sql_query($checkUsername);
    if ($db->sql_numrows($resCheckUsername) > 0) {
        return 0;
    } else {
        return json_encode($username == $username);
    }
}

function checkCaptcha($data) {
    require_once dirname(__FILE__) . '/captch/securimage.php';
    $securimage = new Securimage();
    if ($securimage->check($data) == false) {
        echo 0;
    } else {
        echo 1;
    }
}

function adminEmail($username) {
    global $db;
    $checkEmail = "Select id from " . _prefix("users") . " where email like '$username'";
    $resCheckEmail = $db->sql_query($checkEmail);
    if ($db->sql_numrows($resCheckEmail) > 0) {
        return json_encode($username == $username);
    } else {
        return 0;
    }
}

function payment($table, $id, $paid) {
    global $db;
    $date = date('Y-m-d H:i:s');
    $UpdateSql = "UPDATE " . _prefix("$table") . " SET payment_status = '$paid', paid_on = '$date' WHERE id= $id";
    $updateResult = $db->sql_query($UpdateSql);
    if ($updateResult) {
        echo "Paid (" . date('M d, Y', strtotime($date)) . ")";
    }
    die();
}

function minBudget($min_amount) {
    global $db;
    $sql_query = "SELECT * FROM " . _prefix("budget") . " WHERE amount <= '$min_amount'";
    $res = $db->sql_query($sql_query);
    $num = $db->sql_numrows($res);
    if ($num > 0) {
        return json_encode($min_amount == $min_amount);
    } else {
        return 0;
    }
}

function maxBudget($max_amount) {
    global $db;
    $sql_query = "SELECT * FROM " . _prefix("budget") . " WHERE amount <= '$max_amount'";
    $res = $db->sql_query($sql_query);
    $num = $db->sql_numrows($res);
    if ($num > 0) {
        return json_encode($max_amount == $max_amount);
    } else {
        return 0;
    }
}

function couponExist($promocode, $type) {
    global $db;
    $promocode = trim($promocode);
    $sql_query = "SELECT * FROM " . _prefix("coupons") . " WHERE code like '$promocode' "
            . "&& client_type = '$type' && status = '1' && code NOT IN (SELECT coupon_id FROM " . _prefix("quotes") . " WHERE coupon_id IS NOT NULL)";
    $res = $db->sql_query($sql_query);
    $num = $db->sql_numrows($res);
    if ($num > 0) {
        return json_encode($promocode == $promocode);
    } else {
        return 0;
    }
}

function validSalesId($cp_sales_id) {
    global $db;
    $sql_query = "SELECT * FROM " . _prefix("users") . " Where unique_id = '$cp_sales_id' && user_type = '1' && status = '1' && deleted = '0'";
    $res = $db->sql_query($sql_query);
    $num = $db->sql_numrows($res);
    if ($num > 0) {
        return json_encode($cp_sales_id == $cp_sales_id);
    } else {
        return 0;
    }
}

function validClientId($cp_client_id) {
    global $db;
    $sql_query = "SELECT * FROM " . _prefix("users") . " Where unique_id = '$cp_client_id' && user_type = '0' && status = '1' && deleted = '0'";
    $res = $db->sql_query($sql_query);
    $num = $db->sql_numrows($res);
    if ($num > 0) {
        return json_encode($cp_client_id == $cp_client_id);
    } else {
        return 0;
    }
}

function emailExist($email) {
    global $db;
    $checkEmail = "Select id from " . _prefix("users") . " where email like '$email'";
    $resCheckEmail = $db->sql_query($checkEmail);
    if ($db->sql_numrows($resCheckEmail) > 0) {
        return json_encode($email == $email);
    } else {
        return 0;
    }
}

function deleteFileName($id) {
    global $db;
    $UpdateSql = "UPDATE " . _prefix("admin_quotes") . " SET filename = NULL WHERE id= '$id'";
    $updateResult = $db->sql_query($UpdateSql);
    if ($updateResult) {
        return 1;
    } else {
        return 0;
    }
}

function deleteFile($id) {
    global $db;
    $UpdateSql = "UPDATE " . _prefix("quotes") . " SET file_name = NULL WHERE id= '$id'";
    $updateResult = $db->sql_query($UpdateSql);
    if ($updateResult) {
        return 1;
    } else {
        return 0;
    }
}

function products($supplier) {
    echo productList($supplier);
    die();
}

function payment_approval($table, $id, $plan_id, $product_id, $user_id, $amount, $approval) {
    global $db;
    $sql_queryP = "SELECT plan.duration FROM " . _prefix("membership_prices") . " AS plan  WHERE plan.id=" . $plan_id . " ";
    $resP = $db->sql_query($sql_queryP);
    $planInfo = $db->sql_fetchrow($resP);
    $plan_month = "+ " . $planInfo['duration'] . " Months";
    $start_time = date('Y/m/d h:i:s', time());
    $end_time = date('Y/m/d h:i:s', strtotime($plan_month, time()));

    $fields = array(
        'user_id' => $user_id,
        'payment_type' => 4,
        'amount' => $amount,
        'product_id' => $product_id,
        'payment_date' => date('Y/m/d h:i:s', time())
    );
    if (planExist($product_id)) {
        //update current_plan fields of current supplier to 0
        $update_fields = array(
            'current_plan' => 0
        );
        $where = "where supplier_id= '$user_id' AND pro_id='$product_id'";
        $update_plan = $db->update(_prefix('pro_plans'), $update_fields, $where);
    }
    $plan_fields = array(
        'supplier_id' => $user_id,
        'plan_id' => $plan_id,
        'pro_id' => $product_id,
        'plan_start_date' => $start_time,
        'plan_end_date' => $end_time,
        'current_plan' => 1
    );
    $CurrentStatus='';
    $sp_id = $user_id;
    $changeStatus = $approval == 0 ? 1 : 1;
    $UpdateSql = "UPDATE " . _prefix("$table") . " SET approve_status = '$changeStatus' WHERE id= $id";
    $updateResult = $db->sql_query($UpdateSql);
    if ($updateResult) {
        $insert_pro_plans = $db->insert(_prefix('pro_plans'), $plan_fields);
        $insert_result = $db->insert(_prefix('sp_payments'), $fields);
        if ($insert_pro_plans && $insert_result) {
            $sql_guery = "select user.email,CONCAT(IFNULL(user.first_name,''),' ',IFNULL(user.last_name,'')) AS supplierName  FROM " . _prefix("users") . " AS user where user.id='$sp_id' ";
            $res = $db->sql_query($sql_guery);
            $user_data = $db->sql_fetchrowset($res);
            $to = $user_data[0]['email'];
            $supplier_name = $user_data[0]['supplierName'];
            $email = emailTemplate('approved_payment');
            if ($email['subject'] != '') {
                $message = str_replace(array('{user_name}', '{plan_id}'), array($supplier_name, substr($plan_month, 2)), $email['description']);
                //$send = sendmail($to, $email['subject'], $message);
                //if ($send) {
                    $CurrentStatus = ($approved == 1) ? 'Not Approved <img src="' . ADMIN_IMAGE . 'deactivate.png" title="Pending Payment" />' : 'Approved <img src="' . ADMIN_IMAGE . 'activate.png" title="Paid"/>';
                    echo $CurrentStatus;
               // }
            }
        }
    }
    
}
function deletePC($id) {
    global $db;
    
    
     
        
    
    $deleteRecord = "UPDATE " . _prefix("products") . " SET min_age_new = min_age,capital_gain_new = capital_gain,
                deff_fee_new = deff_fee,
                initial_value_new=initial_value, 
                max_year_new=max_year,
                min_entry_value_new=  min_entry_value,
                max_entry_value_new=max_entry_value,
                fixed_new=fixed,
                fee_model_new=fee_model,
                extra_fee_new=extra_fee,
                info_by_new=info_by,
                weekly_fee_new=weekly_fee,
                last_update_new=last_update,
                fee_how_often_new=fee_how_often,
                dwelling_low_price_new=dwelling_low_price,
                dwelling_high_price_new=dwelling_high_price,
                admin_fee_exit_new=admin_fee_exit,
                capital_loss_new=capital_loss, "
            . "admin_removed = 1, provider_compare_updated = NULL, provider_compare_declined = NULL WHERE id = '$id'";
    if ($db->sql_query($deleteRecord)) {
        /*$getMax = "SELECT MAX(order_by) as coad_productsunt FROM " . _prefix("products") . " WHERE status = 1 && deleted = 0";
        $resMax = $db->sql_query($getMax);
        $dataMax = $db->sql_fetchrow($resMax);
        for ($i = $order; $i <= $dataMax['count']; $i++) {
            $changeOrder = $i - 1;
            $updateOrder = "UPDATE " . _prefix("services") . " SET order_by = '$changeOrder' WHERE order_by = '$i'";
            $db->sql_query($updateOrder);
        }*/
    }
}
