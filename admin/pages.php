<?php
    /*     * **************ADMIN HOME PAGE*****************************
      MODULE INCLUDED
      1.MANAGE OFFER
      2.MANAGE DEMAND
      3.CHANGE PASSWORD
      4.LOGOUT
     * ********************************************* */
?>
<?php
    include("../application_top.php");
    include(INCLUDE_PATH . "header.php");
?>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tbody>
        <tr>
            <td class="lt_nav" height="455" valign="top" width="17%">
                <!-- left navigation -->
                <?php include(INCLUDE_PATH . "left_navigation.php"); ?>
                <!-- left navigation ends here -->
            </td>
            <td valign="top">
                <!-- content area here -->
                <div id="body_content">
                    <?php
                        switch (key($_REQUEST)) {
                            case 'add':
                                include(MODULE_PATH . "pages/add.php");
                                break;
                            case 'homekey':
                                include(MODULE_PATH . "pages/homekey.php");
                                break;
                            case "pages" :
                                include(MODULE_PATH . "pages/manage.php");
                                break;
                            case "edit" :
                                include(MODULE_PATH . "pages/add.php");
                                break;
                            case "addDuration" :
                                include(MODULE_PATH . "pages/addduration.php");
                                break;
                            case "editDuration" :
                                include(MODULE_PATH . "pages/addduration.php");
                                break;
                            case "manageDuration" :
                                include(MODULE_PATH . "pages/manageduration.php");
                                break;
                            case "addVisit" :
                                include(MODULE_PATH . "pages/addvisit.php");
                                break;
                            case "editVisit" :
                                include(MODULE_PATH . "pages/addvisit.php");
                                break;
                            case "manageVisit" :
                                include(MODULE_PATH . "pages/managevisit.php");
                                break;
                            case "setRating" :
                                include(MODULE_PATH . "pages/setrating.php");
                                break;
                            default:
                                include(MODULE_PATH . "pages/manage.php");
                                break;
                        }
                    ?>
                </div>
                <!-- content area ends here -->
            </td>
        </tr>
    </tbody>
</table>

