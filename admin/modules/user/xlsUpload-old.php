<?php
    /*
     * Objective : Add Customer details on admin Panel
     * Filename : add.php
     * Created By : Vinod Kumar singh<vinod.kumar@ilmp-tech.com>
     * Created On : 2 September  2014
     * Modified : 2 September 2014
     */
?>
<?php
    require_once ADMIN_PATH . 'include/excel_reader2.php';
    global $db;
    $records = array();
    $disable = '';
    if (isset($submit) && $submit == 'Submit') {
        if (isset($_FILES['xls_file']['name'])) {
            if ($_FILES['xls_file']['error'] == 0) {
                $target_path = '../admin/files/xlsUpload/';
                $date = new DateTime();
                $timestamp = $date->getTimestamp();
                $fileName = $timestamp . '_' . imgNameSanitize(basename($_FILES['xls_file']['name']), 20);
                $target_path = $target_path . $fileName;
                move_uploaded_file($_FILES['xls_file']["tmp_name"], $target_path);
            }
        }
        $dataXLS = new Spreadsheet_Excel_Reader($target_path);
        $result = $dataXLS->sheets[0]['cells'];
        $succesCount = 0;
        $errorCount = 0;
        foreach ($result as $mainKey => $data) {
            if ($mainKey == 1) {
                continue;
            } else {
                $xlsField = array(
                    'user_type' => 1,
                    'first_name' => trim($data[1]),
                    'last_name' => trim($data[2]),
                    'user_name' => trim($data[3]),
                    'company' => trim($data[4]),
                    'trade' => trim($data[5]),
//                'website' => trim($data[5]),
                    'phone' => $data[6],
                    'address' => ' ',
                    'email' => $data[7],
                    //'country_id' => $country_id,
                    //'state_id' => $state_id,
                    'zipcode' => ' ',
//                'phone' => $data[8],
                    //'ip_address' => $_SERVER['REMOTE_ADDR'],
                    'source_type' => 1,
                    'first_login' => 0,
                    'validate' => 3,
                    'created' => date('Y-m-d H:i:s'),
                        // 'password' => md5(time())
                );
                $fields_array = userXLS();
                $response = Validation::_initialize($fields_array, $xlsField);
                if (isset($response['valid']) && $response['valid'] > 0) {
                    $query = "SELECT user_name, email from " . _prefix('users') . " WHERE user_name= '" . $xlsField['user_name'] . "' OR email= '" . $xlsField['email'] . "'";
                    $res = mysqli_query($db->db_connect_id,$query);
                    $num = mysqli_num_rows($res);
                    $insert_result = '';
                    if ($num == 0) {
                        $rl_password = time();
                        $xlsField['password'] = md5($rl_password);
                        $insert_result = $db->insert(_prefix('users'), $xlsField);
                        $id = $db->last_id();
                    }
                    if ($insert_result) {
                        $to = trim($xlsField['email']);
                        // $rl_password = trim($password);
                        $name = ucwords($xlsField['first_name'] . " " . $xlsField['last_name']);
                        $username = $xlsField['user_name'];

                        $url = ' <a href="' . HOME_PATH . 'supplier/login?id=' . md5($id) . '">Click me</a> ';
                        $email = emailTemplate('Supplier_change_password');
                        if ($email['subject'] != '') {
                            $message = str_replace(array('{password}', '{name}', '{username}', '{url}'), array($rl_password, $name, $username, $url), $email['description']);
                            // prd($message);
                            $send = sendmail($to, $email['subject'], $message);
//                            if ($send) {
//                                // Message for insert
//                                $msg = common_message(1, constant('Successfully Registered'));
//                                $_SESSION['msg'] = $msg;
//                                //ends here
//                                // redirect_to(HOME_PATH . 'supplier/register');
//                            } else {
//                                // $msg = common_message(1, constant('INSERT'));
//                                $_SESSION['msg'] = 'Mail has not been sent to your email Id';
//                                //ends here
//                            }
                        }
                        $succesCount++;
                    } else {
                        $error[] = $xlsField['first_name'] . ' ' . $xlsField['last_name'] . ' was failed to register</br>';
                    }
                } else {
                    $errorCount++;
                }
            }
        }
        $msg = common_message(1, $succesCount . constant('Successfully Registered'));
        $_SESSION['msg'] = $msg;
        foreach ($error as $err) {
            $_SESSION['msg_err'] .= $err;
        }
        
        
        
        
        
        
        
        
        
        
        // $_SESSION['msg'] = 'Succesfull:' . $succesCount . ',&nbsp;Failure:' . $errorCount;
    }
?>

<table align="center" border="0" cellpadding="0" cellspacing="0" width="95%">
    <tbody>
        <tr style="background:#4b99e6;">
            <td>
                <div class="breadcrumb">
                    <ul>
                        <li><a href="<?php echo HOME_PATH_URL; ?>admin.php"><i class="fa fa-home"></i>Home </a>>>Supplier Upload</li>
                    </ul>
                </div>
            </td>
        </tr>
    </tbody>
</table>


<div class="row" style="margin-bottom: 20px; border-bottom: 1px solid rgb(204, 204, 204);">
    <div class="col-sm-offset-2 col-sm-6 success">
<?php if (isset($_SESSION['msg_err'])) { ?>
                <div class="errorForm" id="error" style="display: block;">
                    <img align="absmiddle" src="<?php echo HOME_PATH . '/images/error.png'; ?>">&nbsp;<?php echo $_SESSION['msg_err']; ?>
                </div>
        <?php
        unset($_SESSION['msg_err']);
    }
    if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
        ?>
                <!--                <div class="successForm" id="success" style="display: block;">-->
                                    <!--<img align="absmiddle" src="<?php // echo HOME_PATH . '/images/success.png';             ?>">-->
                &nbsp;<?php echo $_SESSION['msg']; ?>
                <!--                </div>-->
        <?php
        unset($_SESSION['msg']);
    }
?>
    </div>
</div>
<div class="container" style="width: 95%; border: 1px solid rgb(204, 204, 204);">
    <div class="row" style="margin-bottom: 20px; border-bottom: 1px solid rgb(204, 204, 204);">
        <div class="col-sm-6">
            <h2 class="">Supplier Upload</h2>
        </div>

        <div class="col-sm-6">
            <h2 style="float:right;" class="redCol small">* fields required</h2>
        </div>
    </div>
    <p class="clearfix"></p>
    <form name="xlsFile" id="xlsFile" action="" method="POST" class="form-horizontal" role="form" enctype="multipart/form-data">

        <div class="form-group">
            <label for="xls_file" class="col-sm-2 control-label">Upload xls file<span class="redCol">* </span></label>
            <div class="col-sm-8">
                <input type="file" name="xls_file" id="xls_file" class="required">
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-8">
                <input type="submit" value="Submit" name="submit" class="submit_btn btn">
            </div>
        </div>
    </form>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('input[type="submit"]').focus();
    });
    $('#xlsFile').validate();
</script>