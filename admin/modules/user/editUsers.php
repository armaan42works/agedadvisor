<?php
/*
 * Objective : Add Customer details on admin Panel
 * Filename : add.php
 * Created By : Vinod Kumar singh<vinod.kumar@ilmp-tech.com>
 * Created On : 2 September  2014
 * Modified : 2 September 2014
 */
?>
<?php
global $db;
if (isset($submit) && $submit == 'Update') {
    // prd($user_type);
    if ($user_type == 0) {
        $fields_array = editCustomer();
    } else {
        $fields_array = editSupplier();
    }
    // prd($fields_array);
    $response = Validation::_initialize($fields_array, $_POST);
    if (isset($response['valid']) && $response['valid'] > 0) {

        if ($user_type == 0) {
            $fields = array(
                'first_name' => trim($first_name),
                'last_name' => trim($last_name),
                'phone' => trim($phone),
                'country_id' => trim($country_id),
                'modified' => date('Y-m-d h:i:s', time())
            );
        } else {
            $fields = array(
                'first_name' => trim($first_name),
                'last_name' => trim($last_name),
                'phone' => trim($phone),
                'company' => trim($company),
                'trade' => trim($trade),
                'zipcode' => trim($zipcode),
                'modified' => date('Y-m-d h:i:s', time())
            );
        }
       // prd($fields);
        $where = "where md5(id)= '$id'";
        $update_result = $db->update(_prefix('users'), $fields, $where);

        if ($update_result) {
            if ($user_type == 0) {
                // Message for insert
                $msg = common_message(1, constant('UPDATE'));
                $_SESSION['msg'] = $msg;
                //ends here                     
                redirect_to(MAIN_PATH . "/user.php?customer");
            } else {
                // Message for insert
                $msg = common_message(1, constant('UPDATE'));
                $_SESSION['msg'] = $msg;
                //ends here 
                redirect_to(MAIN_PATH . "/user.php?supplier");
            }
            $msg = common_message(1, constant('UPDATE'));
            foreach ($_POST as $key => $value) {
                $key = '';
            }
        }
    } else {
        $errors = '';
        foreach ($response as $key => $message) {
            $error = true;
            $errors .= $message . "<br>";
        }
    }
}

if (key($_REQUEST) == 'editSupplier' || key($_REQUEST) == 'editCustomer') {
    $sql_query = "SELECT * FROM " . _prefix("users") . " WHERE md5(id)='$id' AND (user_type=1 OR user_type=0)";
    $res = $db->sql_query($sql_query);
    $records = $db->sql_fetchrow($res);
    extract($records);
    if ($user_type == 0) {
        $user = 'Customer';
    } else {
        $user = 'Supplier';
    }
    //  prd($records);
    $editAct = 1;
    $disable = "disabled = 'disabled'";
}
?>  
<table align="center" border="0" cellpadding="0" cellspacing="0" width="95%">
    <tbody>
        <tr style="background:#4b99e6;">
            <td>
                <div class="breadcrumb">
                    <ul>
                        <li><a href="<?php echo HOME_PATH_URL; ?>admin.php"><i class="fa fa-home"></i>Home </a>>><a href="<?php echo HOME_PATH_URL . "user.php?" . strtolower($user) ?>">Manage Users</a>>> Edit <?php echo $user; ?></li>
                    </ul>
                </div>
        </tr>
    </tbody>
</table>
<div class="row">
    <div class="col-sm-offset-2 col-sm-6 success">
        <?php if (isset($error)) { ?>
            <div class="errorForm" id="error" style="display: block;">
                <img align="absmiddle" src="<?php echo HOME_PATH . 'admin/images/error.png'; ?>">&nbsp;<?php echo $errors; ?>.<br>
            </div>
            <!--echo '<div style="color:#FF0000">' . $errors . '</div>';-->
            <?php
        }
        if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
            ?>
            <div class="errorForm" id="error" style="display: block;">
                <img align="absmiddle" src="<?php echo HOME_PATH . 'admin/images/error.png'; ?>">&nbsp;<?php echo $_SESSION['msg']; ?>.<br>
            </div>
            <?php
            unset($_SESSION['msg']);
        }
        ?>
    </div>
</div>

<div class="container" style="width: 95%; border: 1px solid rgb(204, 204, 204);">
    <div class="row" style="margin-bottom: 20px; border-bottom: 1px solid rgb(204, 204, 204);">
        <div class="col-sm-6">
            <h2 class="">Edit <?php echo $user; ?></h2>
        </div>

        <div class="col-sm-6">
            <h2 style="float:right;" class="redCol small">* fields required</h2>
        </div>
    </div>
    <p class="clearfix"></p>
    <form name="editSupplier" id="editSupplier" method="POST" action=""  class="form-horizontal" role="form" enctype="multipart/form-data">
        <div class="form-group">
            <label for="first_name" class="col-sm-2 control-label">First Name&nbsp;<span class="redCol">* </span></label>
            <div class="col-sm-8">
                <input type="text" minlength="2" maxlength="50" class="required form-control alphaNumSpF" name="first_name" id="title"  value="<?php echo isset($first_name) ? stripslashes($first_name) : ''; ?>"/>
            </div>
        </div>
        <div class="form-group">
            <label for="last_name" class="col-sm-2 control-label">Last Name&nbsp;<span class="redCol">* </span></label>
            <div class="col-sm-8">
                <input type="text" minlength="2" maxlength="50" class="required form-control alphaNumSpF" name="last_name" value="<?php echo isset($last_name) ? stripslashes($last_name) : ''; ?>"/>
            </div>
        </div>
        <div class="form-group">
            <label for="email" class="col-sm-2 control-label">Email</label>
            <div class="col-sm-8" style="margin-top: 8px;">
                <span><?php echo isset($email) ? stripslashes($email) : ''; ?></span>
            </div>
        </div>
        <div class="form-group" >
            <label for="last_name" class="col-sm-2 control-label">User Name</label>
            <div class="col-sm-8" style="margin-top: 8px;">
                <span><?php echo isset($user_name) ? stripslashes($user_name) : ''; ?></span>
            </div>
        </div>
        <div class="form-group">
            <label for="phone" class="col-sm-2 control-label">Phone Number&nbsp;<span class="redCol">* </span></label>
            <div class="col-sm-8">
                <input type="text" minlength="2" maxlength="50" class="required form-control phonePattern"  name="phone"  value="<?php echo isset($phone) ? stripslashes($phone) : ''; ?>"/>
            </div>
        </div>
        <?php if ($user_type == 0) { ?>
            <div class="form-group">
                <label for="country" class="col-sm-2 control-label">Country&nbsp;<span class="redCol">* </span></label>
                <div class="col-sm-8">
                    <select class="form-control select_option_1 input_fild pull-left" name="country_id" ><?php echo countryLists($country_id); ?></select>
                </div>
            </div>
        <?php } else { ?>
            <div class="form-group">
                <label for=company" class="col-sm-2 control-label">Company Name&nbsp;<span class="redCol">* </span></label>
                <div class="col-sm-8">
                    <input type="text" minlength="2" maxlength="50" class="required form-control" name="company"  value="<?php echo isset($company) ? stripslashes($company) : ''; ?>"/>
                </div>
            </div>        
            <div class="form-group">
                <label for=trade_name" class="col-sm-2 control-label">Trade Name&nbsp;<span class="redCol">* </span></label>
                <div class="col-sm-8">
                    <input type="text" minlength="2" maxlength="50" class="required form-control" name="trade"  value="<?php echo isset($trade) ? stripslashes($trade) : ''; ?>"/>
                </div>
            </div>        
            <div class="form-group">
                <label for="zipcode" class="col-sm-2 control-label">Zipcode&nbsp;<span class="redCol">* </span></label>
                <div class="col-sm-8">
                    <input type="text" minlength="2" maxlength="50" class="required form-control alphaNumeric" name="zipcode"  value="<?php echo isset($zipcode) ? stripslashes($zipcode) : ''; ?>"/>
                </div>
            </div>  
        <?php } ?>

        <input type="hidden" name="user_type" value="<?php echo $user_type; ?>"/>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-8">
                <input type="submit" value="Update" name="submit" class="submit_btn btn"  />
            </div>
        </div>
    </form>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        var path = "<?php echo HOME_PATH; ?>admin/";
        jQuery.validator.addMethod("phonePattern", function(value, element) {
            return this.optional(element) || /^[0-9\+\- ]+$/.test(value);
        }, "Enter valid phone number.");
        jQuery.validator.addMethod("alphaNumeric", function(value, element) {
            return this.optional(element) || /^[0-9a-zA-Z]+$/.test(value);
        }, "Only  Alphanumeric.");
        jQuery.validator.addMethod("NumericSpace", function(value, element) {
            return this.optional(element) || /^[0-9 ]+$/.test(value);
        }, "Only  Numeric.");
        jQuery.validator.addMethod("alphaNumSpF", function(value, element) {
            return this.optional(element) || /^[a-zA-Z\'\.\-\ ]+$/.test(value);
        }, "Please enter valid first name");
        jQuery.validator.addMethod("alphaNumSpL", function(value, element) {
            return this.optional(element) || /^[a-zA-Z\'\.\-\ ]+$/.test(value);
        }, "Please enter valid last name");

        jQuery.validator.addMethod("noSpace", function(value, element) {
            return value.indexOf(" ") < 0 && value != "";
        }, "Space is not allowed");

        $('#editSupplier').validate({
            rules: {
                zipcode: {
                    required: true,
                    minlength: 3,
                    maxlength: 10
                },
                phone: {
                    required: true,
                    minlength: 7,
                    maxlength: 19
                }

            },
            messages: {
                zipcode: {
                    required: "This field is required ",
                    minlength: "Zipcode must contain at least 3 chars",
                    maxlength: "Zipcode should not be more than 10 characters"
                },
                phone: {
                    required: "This field is required ",
                    minlength: "Phone must contain at least 7 chars",
                    maxlength: "Phone should not be more than 19 characters"
                }
            },
            submitHandler: function(form) {
                $('#editSupplier').attr('disabled', 'disabled');
                form.submit();

            }
        });

    });
</script>
<style type="text/css">
    label.error{
        color:red;
        font-size: 10px;
    }
    input{
        color:black;
        font-size: 15px;
    }

</style>