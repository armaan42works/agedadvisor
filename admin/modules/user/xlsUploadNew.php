<?php
require_once ADMIN_PATH . 'include/excel_reader2.php';
global $db;
$records = array();
$disable = '';
$useId = $_SESSION['userId'];
if (isset($submit) && $submit == 'submit') {
//      prd($_FILES);
    if (isset($_FILES['xls_file']['name'])) {
        if ($_FILES['xls_file']['error'] == 0) {
            $target_path = ADMIN_PATH . 'files/xlsUpload/';
            $date = new DateTime();
            $timestamp = $date->getTimestamp();
            $fileName = $timestamp . '_' . imgNameSanitize(basename($_FILES['xls_file']['name']), 20);
            $target_path = $target_path . $fileName;
            // prd($target_path);
            move_uploaded_file($_FILES['xls_file']["tmp_name"], $target_path);
            $dataXLS = new Spreadsheet_Excel_Reader($target_path);
            $result = $dataXLS->sheets[0]['cells'];
            $succesCount = 0;
            $errorCount = 0;
            foreach ($result as $mainKey => $data) {
                if ($mainKey == 1) {
                    continue;
                } else {
                    $xlsField = array(
                        'user_type' => 1,
                        'first_name' => trim($data[16]),
                        'last_name' => '',
                        'user_name' => trim($data[16]),
                        'company' => trim($data[16]),
                        'trade' => trim($data[2]),
//                'website' => trim($data[5]),
                        'phone' => trim($data[29]),
                        'address' => '',
                        'email' => trim($data[28]),
                        //'country_id' => $country_id,
                        //'state_id' => $state_id,
//                        'zipcode' => ' ',
//                      'phone' => $data[8],
//                      'ip_address' => $_SERVER['REMOTE_ADDR'],
                        'source_type' => 1,
                        'first_login' => 0,
                        'validate' => 3,
                        'created' => date('Y-m-d H:i:s'),
                            // 'password' => md5(time())
                    );
//                    if (isset($response['valid']) && $response['valid'] > 0) {
                    $query = "SELECT id, user_name, email from " . _prefix('users') . " WHERE first_name= '" . $xlsField['first_name'] . "' OR email= '" . $xlsField['email'] . "'";
                    $res = mysqli_query($db->db_connect_id,$query);
                    $num = mysqli_num_rows($res);
                    $dataUser = mysqli_fetch_assoc($res);
                    $insert_result = '';
                    /* Check for new user start*/
                    if ($num == 0) {
                        $rl_password = time();
                        $xlsField['password'] = md5($rl_password);
                        $insert_result = $db->insert(_prefix('users'), $xlsField);
                        $spId = $db->last_id();

                        /* Send mail to new a/c start */
                        if ($insert_result) {
                            $to = trim($xlsField['email']);
//                          $rl_password = trim($password);

                            $name = ucwords($xlsField['first_name'] . " " . $xlsField['last_name']);
                            $username = $xlsField['user_name'];
                            $url = ' <a href="' . HOME_PATH . 'supplier/login?id=' . md5($spId) . '">Click me</a> ';
                            $email = emailTemplate('Supplier_change_password');
                            if ($email['subject'] != '') {
                                $message = str_replace(array('{password}', '{name}', '{username}', '{url}'), array($rl_password, $name, $username, $url), $email['description']);
// prd($message);
                                $send = sendmail($to, $email['subject'], $message);
//                            if ($send) {
//                                // Message for insert
//                                $msg = common_message(1, constant('Successfully Registered'));
//                                $_SESSION['msg'] = $msg;
//                                //ends here
//                                // redirect_to(HOME_PATH . 'supplier/register');
//                            } else {
//                                // $msg = common_message(1, constant('INSERT'));
//                                $_SESSION['msg'] = 'Mail has not been sent to your email Id';
//                                //ends here
//                            }
                            }
                            $succesCount++;
                        }
                        /* Send mail to new a/c end */
                    } else { // user is existing
                        $spId = $dataUser['id'];
                    }
                    /* Check for new user end*/
                    $allservice = explode(',', trim($data[3]));
                    $servicesIds = ''; // seperated by ','
                    foreach ($allservice as $servise) {
                        $servicesIds .= ($servicesIds == '') ? serviceCategoryId(trim($servise)) : ', ' . serviceCategoryId(trim($servise));
                    }
                    $fields = array(
                        'title' => trim($data[1]),
                        'description' => trim($data[1]),
                        'keyword' => trim($data[1]),
                        'city_id' => getCityId(trim($data[9])),
                        'suburb_id' => trim($data[8]),
                        'restcare_id' => trim($data[7]),
                        'facility_type' => $servicesIds,
                        'zip' => trim($data[10]),
                        'supplier_id' => $spId,
                        //'latitude' => $latitude,
                        //'longitude' => $longitude,
                        //'image' => $_POST['image'],
                        'created' => date('Y-m-d h:i:s', time()),
                            //'staff_comment' => trim($data[11]),
                            //'staff_image' => trim($data[1]),
                            //'management_comment' => trim($data[12]),
                            // 'management_image' => trim($data[1]),
                            //'activity_comment' => trim($data[13]),
                            // 'activity_image' => trim($data[1]),
                    );

                    $extra_field = array(
                        'sp_id' => $spId,
                        'brief' => trim($data[1]),
                        'no_of_beds' => trim($data[4]),
                        'created' => date('Y-m-d h:i:s', time())
                    );

                    $fields_array = product();
                    $response = Validation::_initialize($fields_array, $fields);
                    if (isset($response['valid']) && $response['valid'] > 0) {
                        $query = "SELECT title from " . _prefix('products') . " WHERE title = '" . $fields['title'] . "'";
                        $res = mysqli_query($db->db_connect_id,$query);
                        $num = mysqli_num_rows($res);

                        $insert_result = '';
                        $insert_resultPro = $db->insert(_prefix('products'), $fields);
                        $last_product_id = $db->last_id();
                        $insert_result = $db->insert(_prefix('extra_facility'), $extra_field);

                        $queryDel = "DELETE FROM " . _prefix("pro_services") . " WHERE product_id = '$last_product_id'";
//                          Inserting services type ids in pro_services table
                        foreach ($allservice as $servise) {
                            $services_field = array(
                                'supplier_id' => $spId,
                                'product_id' => $last_product_id,
                                'service_id' => serviceCategoryId(trim($servise)),
                            );
//                          Message for insert
                            $insert_service = $db->insert(_prefix('pro_services'), $services_field);
                        }
                        $fieldsExtra = array(
                            'pro_id' => $last_product_id,
                            'premises_name' => trim($data[1]),
                            'certification_service_type' => trim($data[2]),
                            'service_types' => $insert_service,
                            'premises_website' => trim($data[5]),
                            'premises_address_other' => trim($data[6]),
                            'dhb_name' => trim($data[11]),
                            'certificate_name' => trim($data[12]),
                            'certification_period' => trim($data[13]),
                            'certificate_license_end_date' => trim($data[14]),
                            'current_auditor' => trim($data[15]),
                            'legal_name' => trim($data[16]),
                            'l_e_address_other' => trim($data[17]),
                            'l_e_address' => trim($data[18]),
                            'l_e_address_suburb' => trim($data[19]),
                            'l_e_address_city' => trim($data[20]),
                            'l_e_address_post_code' => trim($data[21]),
                            'l_e_address_postal' => trim($data[22]),
                            'l_e_address_post_suburb' => trim($data[23]),
                            'l_e_address_post_city' => trim($data[24]),
                            'l_e_postal_address_post_code' => trim($data[25]),
                            'l_e_website' => trim($data[26]),
                            'manager' => trim($data[27]),
                            'email' => trim($data[28]),
                            'phone' => trim($data[29]),
                            'last_name' => trim($data[30]),
                            'position' => trim($data[31]),
                            'old_email' => trim($data[32]),
                            'created' => date('Y-m-d h:i:s', time())
                        );
                        $insert_result = $db->insert(_prefix('pro_extra_info'), $fieldsExtra);
                        if ($insert_result) {
                            $field = array(
                                'pro_extra_info' => mysqli_insert_id($db->db_connect_id)
                            );
                            $where = "where id=" . $last_product_id . " ";
                            $update_result = $db->update(_prefix('products'), $field, $where);
                        }
                        if ($insert_resultPro) {
                            $succesCount++;
                        } else {
                            $error[] = $fields['title'] . ' was failed to save</br>';
                        }
                    } else {
                        $errorCount++;
                    }
                }
            }
        $msg = common_message(1, $succesCount . constant('Successfully Registered'));
        $_SESSION['msg'] = $msg;
        foreach ($error as $err) {
            $_SESSION['msg_err'] .= $err;
        }
     }else {

        $msg = common_message(0, 'Error in parsing xls file. Please provide another one.');
        $_SESSION['msg'] = $msg;
    }
//            $msg = common_message(1, $succesCount . constant('Successfully Registered'));
//            $_SESSION['msg'] = $msg;
//            foreach ($error as $err) {
//                $_SESSION['msg_err'] .= $err;
//            }
//prd($target_path);

    }
}
//}
?>


<div class="row" style="margin-bottom: 20px; border-bottom: 1px solid rgb(204, 204, 204);">
    <div class="col-sm-offset-2 col-sm-6 success">
<?php if (isset($_SESSION['msg_err'])) { ?>
            <div class="errorForm" id="error" style="display: block;">
                <img align="absmiddle" src="<?php echo HOME_PATH . '/images/error.png'; ?>">&nbsp;<?php echo $_SESSION['msg_err']; ?>
            </div>
    <?php
    unset($_SESSION['msg_err']);
}
if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
    ?>
            <!--                <div class="successForm" id="success" style="display: block;">-->
                                <!--<img align="absmiddle" src="<?php // echo HOME_PATH . '/images/success.png';                                       ?>">-->
            &nbsp;<?php echo $_SESSION['msg']; ?>
            <!--                </div>-->
            <?php
            unset($_SESSION['msg']);
        }
        ?>
    </div>
</div>


<div class="container" style="padding:0px 3% 0">
    <div class="row">
        <div class="dashboard_container">
<?php require_once 'includes/sp_left_navigation.php'; ?>
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                <div class="dashboard_right_col">
                    <h2 class="hedding_h2"><i class="fa fa-pencil-square-o"></i> <span>Upload Premises</span></h2>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 thumbnail adbooking_container">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <form name="xlsFile" id="xlsFile" action="" method="POST"  class="form-horizontal form_payment" role="form" enctype="multipart/form-data" style="padding-top:20px;">
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 list_container">

                                            <div class="form-group" style="margin-bottom:6px;">
                                                <label for="xls_file" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration">Upload xls file&nbsp;<span class="redCol"> *</span></label>
                                                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                                    <input type="file" name="xls_file" id="xls_file" class="form-control_browse input_fild_img_browse required">
                                                </div>
                                            </div>
                                            <!--                                            <div class="form-group" style="margin-bottom:6px;">
                                                                                            <label for="xls_file" class="form-control_browse input_fild_img_browse required">Upload xls file<span class="redCol">* </span></label>
                                                                                            <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                                                                                <input type="file" name="xls_file" id="xls_file" class="required">
                                                                                            </div>
                                                                                        </div>-->

                                            <div class="form-group" style="margin-bottom:6px;">
                                                <div class="col-sm-offset-2 col-sm-8">

                                                    <button type="submit" value='submit' name="submit" class="btn btn-danger center-block btn_search">Upload</button>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<script type="text/javascript">
    $(document).ready(function() {
        $('input[type="submit"]').focus();
    });
    $('#xlsFile').validate();
</script>