<style>
    th,td,tr{border:0px!important;}
    .input{background-color:white;}
</style>
<?php
global $db;
$description = '';
include_once("./fckeditor/fckeditor.php");

if (isset($submit) && $submit == 'Submit') {
    $timestamp="";
    $fields_array = freeimages();
    $response = Validation::_initialize($fields_array, $_POST);
    if (isset($response['valid']) && $response['valid'] > 0) {
        if (isset($_FILES['file']['name'])) {
            if ($_FILES['file']['error'] == 0) {
                $ext = end(explode('.', $_FILES['file']['name']));
                $target_pathL = '../admin/files/gallery/images/';
                $freeImage = $timestamp . '_' . imgNameSanitize(basename($_FILES['file']['name']), 20);
                $target_path = $target_pathL . $freeImage;
                $target_path_thumb = $target_pathL . 'thumb_' . $freeImage;
                move_uploaded_file($_FILES['file']["tmp_name"], $target_path);
                ak_img_resize($target_path, $target_path_thumb, 50, 50, $ext);
                $_POST['file'] = $freeImage;
            }
        }
         $title=trim($_POST['title']);
         $image= $_POST["file"];
        $fields = array(
            'title' => trim($_POST['title']),
            'image' => $_POST["file"],
             
        );
        $sql="insert into ad_galleries (title,file,status,imageflg) VALUES ('$title','$image',1,1)"; 
        $insert_result = $db->sql_query($sql);
        
        if ($insert_result) {
            // Message for insert
           $msg = common_message(1, constant('INSERT'));
           $_SESSION['msg'] = $msg;
            //ends here
            redirect_to(MAIN_PATH . "freeimages.php?manage");
        }
    } else {
        $errors = '';
        foreach ($response as $key => $message) {
            $error = true;
            $errors .= $message . "<br>";
        }
    }
}
if (key($_REQUEST) == 'edit') {
    $id = $_GET['id'];
    $sql_query = "SELECT * FROM " . _prefix("galleries") . " where md5(id)='$id' and imageflg=1"; 
    $res = $db->sql_query($sql_query);
    $records = $db->sql_fetchrowset($res);
   // echo"<pre>";print_r($records);die();
    if (count($records)) {
        foreach ($records as $record) {
            $title = $record['title'];
            $image = $record['file'];
        }
    }
}
if (isset($update) && $update == 'Update') {
   // echo"<pre>";print_r($_GET);die();
    $fields_array = freeimages();
    $response = Validation::_initialize($fields_array, $_POST);
    if (isset($response['valid']) && $response['valid'] > 0) {
        if (isset($_FILES['file']['name'])) {
            if ($_FILES['file']['error'] == 0) {
                $ext = end(explode('.', $_FILES['file']['name']));
                $target_pathL = '../admin/files/gallery/images/';
                $freeImage = $timestamp . '_' . imgNameSanitize(basename($_FILES['file']['name']), 20);
                $target_path = $target_pathL . $freeImage;
                $target_path_thumb = $target_pathL . 'thumb_' . $freeImage;
                move_uploaded_file($_FILES['file']["tmp_name"], $target_path);
                ak_img_resize($target_path, $target_path_thumb, 50, 50, $ext);
                $_POST['file'] = $freeImage;
                $title=$_POST["title"];

            } else{
                $id=$_GET['id'];
                $sql="select id,file,title from ad_galleries where md5(id)='$id' and imageflg=1";
                $result=$db->sql_query($sql);
                $data=$db->sql_fetchrow($result);
                $freeImage= $data["file"];
                $title=$_POST["title"];    
                  }
       
            }
       
        
        $where = "where md5(id)= '$id' and imageflg=1";
//            prd(get_class_methods($db));
        $sql="update ad_galleries set title='$title',file='$freeImage' $where ";
        $update_result=$db->sql_query($sql);
        if ($update_result) {
            // Message for insert
            $msg = common_message(1, constant('UPDATE'));
            $_SESSION['msg'] = $msg;
            //ends here
            redirect_to(MAIN_PATH . "freeimages.php?manage");
        }
    } else {
        $errors = '';
        foreach ($response as $key => $message) {
            $error = true;
            $errors .= $message . "<br>";
        }
    }
}
?>
<script type="text/javascript">
    $(function() {
    
  // var availableTags ='search.php';
    $("#city").autocomplete({

        //source:availableTags,
        //focus: true,
        
        source: function (request,response) {
        $.ajax({
            method: "post",
            url:'/ajaxFront.php',
            minlength:3,
            data: {
                'action':'citiesname', 'maxRows': 12,
                'name': request.term
                },
            success: function (result) {
                var data = $.parseJSON(result);
                     response($.map(data, function(item)
                     {
                        return {
                            label: item.label,
                            value: item.value
                     }
            }));
            
        }
    });
    },
        select: function(event, ui) {
                              $this = $(this);
                             setTimeout(function(){
          
            $("#city").val(ui.item.label); 
            $("#city_id").val(ui.item.value); 
            });
    },
        open: function(event, ui) {
        $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
         $('ul.ui-widget-content').css({"z-index": "9999", "display": "block", "top": autoT, "left": autoL});
          },
        close: function() {
        $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
        }
});
});
</script>




<div style="margin-top: 10px;">
    <table align="center" border="0" cellpadding="0" cellspacing="0" width="95%">
        <tbody>
            <tr style="background:#4b99e6;">
                <td>
                    <div class="breadcrumb">
                        <ul>
                            <li><a href="<?php echo HOME_PATH_URL; ?>admin.php"><i class="fa fa-home"></i>Home </a>>><a href="<?php echo HOME_PATH_URL . "freeimages.php?manage" ?>">Manage FreeImage</a>>> <?php echo (key($_REQUEST) == 'edit') ? 'Edit' : 'Add'; ?></li>
                        </ul>
                    </div>
            </tr>
        </tbody>
    </table>
    <div class="row">
        <div class="col-sm-offset-2 col-sm-6 success">
            <?php if (isset($error)) { ?>
                <div class="errorForm" id="error" style="display: block;">
                    <img align="absmiddle" src="<?php echo HOME_PATH . 'admin/images/error.png'; ?>">&nbsp;<?php echo $errors; ?>.<br>
                </div>
                <!--echo '<div style="color:#FF0000">' . $errors . '</div>';-->
                <?php
            }
            if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
                ?>
                <div class="errorForm" id="error" style="display: block;">
                    <img align="absmiddle" src="<?php echo HOME_PATH . 'admin/images/error.png'; ?>">&nbsp;<?php echo $_SESSION['msg']; ?>.<br>
                </div>
                <?php
                unset($_SESSION['msg']);
            }
            ?>
        </div>
    </div>

    <div class="container" style="width: 95%; border: 1px solid rgb(204, 204, 204);">
        <div class="row" style="margin-bottom: 20px; border-bottom: 1px solid rgb(204, 204, 204);">
            <div class="col-sm-6">
                <h2 class=""><?php echo (key($_REQUEST) == 'edit') ? 'Edit' : 'Add'; ?> Free Images Add</h2>
            </div>

            <div class="col-sm-6">
                <h2 style="float:right;" class="redCol small">* fields required</h2>
            </div>
        </div>
        <p class="clearfix"></p>
        <form name="freeimages" id="freeimages" method="POST" action=""  class="form-horizontal" role="form" enctype="multipart/form-data">
            <div class="form-group">
                <label for="title" class="col-sm-2 control-label">Title<span class="redCol">* </span></label>
                <div class="col-sm-8">
                    <input type="text" minlength="2" maxlength="50" class="required form-control" name="title" id="title"  value="<?php echo isset($title) ? stripslashes($title) : ''; ?>"/>
                </div> 
            </div>
            
            <div class="form-group">
                <label for="file" class="col-sm-2 control-label">Upload Image<span class="redCol"><?php echo isset($image) ? '' : '*'; ?> </span></label>
                <div class="col-sm-8">
                    <input type="file" name="file" id="file"  class="<?php echo isset($image) ? '' : 'required'; ?>">
                </div>
            </div>
              <div class="form-group">
                <div class="col-sm-offset-2 col-sm-8">
                    <?php if (file_exists(DOCUMENT_PATH . 'admin/files/gallery/images/' . $image) && $image != '' && key($_REQUEST) == 'edit') { ?>
                        <img width="100px;" height="100px;" title="feature" src="<?php echo MAIN_PATH . '/files/gallery/images/' . $image ?>">
                    <?php } ?>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-8">
                    <?php
                    if (key($_REQUEST) == 'edit') {
                        ?>
                        <input type="submit" value="Update" name="update" class="submit_btn btn" src="<?php echo ADMIN_IMAGE; ?>submit.gif">

                        <?php
                    } else {
                        ?>
                        <input type="submit" value="Submit" name="submit" class="submit_btn btn" id="btnadd">
                        <?php
                    }
                    ?>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('input[type="submit"]').focus();
    });
    $('#freeimages').validate();
<?php if (file_exists(DOCUMENT_PATH . 'admin/files/gallery/images/' . $image) && $image != '' && key($_REQUEST) == 'edit') { ?>
        $('#image').removeClass('required');
<?php } ?>
</script>
