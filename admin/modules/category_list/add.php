<style>
    th,td,tr{border:0px!important; }
    .input{background-color:white;}
    #category_content{height: auto;}

</style>
<?php
global $db;
$description = '';
$image = '';
include_once("./fckeditor/fckeditor.php");

$category_name=$_POST['title'];
$category_topic=$_POST['category'];
$category_content=$_POST['category_content1'];
$content = $_POST["content"];
$content_link = $_POST["content_link"];
 $article_type = $_POST["article_type"];





/*******************************for submit*************************************/
if (isset($submit) && $submit == 'Submit') {
   $fields_array = consumer_info();
    $response = Validation::_initialize($fields_array, $_POST);
    if (isset($_FILES['banner_image']['name'])) {

        if ($_FILES['banner_image']['error'] == 0) {


            $image_info = getimagesize($_FILES["banner_image"]["tmp_name"]);
            $image_width = $image_info[0];
            $image_height = $image_info[1];

            if ($image_height < 225) {
                $response['size'] = 'Please upload image of minimum height 225.';
            }
            
            if ($image_width < 260) {
                $response['size'] = 'Please upload image of minimum width 260.';
            }
             if ($image_height > 450) {
                $response['size'] = 'Please upload image of maximum height 450.';
            }
            
            if ($image_width > 520) {
                $response['size'] = 'Please upload image of maximum width 520.';
            }
        }
    }
   /* if (isset($_FILES['article_pdf']['name'])) {
           $allowedExts = array("doc", "docx", "pdf", "odt");
             echo $pdf_name=$_FILES['article_pdf']['name'];
            $extension_name=end(explode('.',$pdf_name));
              
              if(in_array($pdf_name,$allowedExts)){
                   if ($_FILES['article_pdf']['error'] == 0) {

                    $response['type']="Please upload only doc,docx,pdf,odt";
                   }
               }
           }*/





    if (empty($response['size']) && isset($response['valid']) && $response['valid'] > 0) {
        if (isset($_FILES['banner_image']['name'])) {

            if ($_FILES['banner_image']['error'] == 0) {
                $ext = end(explode('.', $_FILES['banner_image']['name']));
                $target_pathL = '../admin/files/pages/banner/';
                $date = new DateTime();
                $timestamp = $date->getTimestamp();
                $category_image_name = $timestamp . '_' . imgNameSanitize(basename($_FILES['banner_image']['name']), 20);
                $target_path = $target_pathL . $category_image_name;
                $target_path_thumb = $target_pathL . 'thumb_' . $category_image_name;
                move_uploaded_file($_FILES['banner_image']["tmp_name"], $target_path);
                if ($ext == 'tif') {
                    copy($target_path, $target_path_thumb);
                } else {
                    ak_img_resize($target_path, $target_path_thumb, 30, 30, $ext);
                }
                $_POST['banner_image'] = $category_image_name;
            }
        }
         
        /****for article pdf code****/

       if (isset($_FILES['article_pdf']['name'])) {
          // $allowedExts = array("doc", "docx", "pdf", "odt");
             //echo $pdf_name=$_FILES['article_pdf']['name'];
            //$extension_name=end(explode('.',$pdf_name));
              
             // if(in_array($pdf_name,$allowedExts)){
                   if ($_FILES['article_pdf']['error'] == 0) {

                        //$target_paths = '../admin/files/pages/feature_image/';
                        $target_paths = '../admin/files/pages/consumer_article_pdf/';

                        $date = new DateTime();
                        $timestamp = $date->getTimestamp();
                        $ext = end(explode('.', $_FILES['article_pdf']['name']));
                        $articleName =basename($_FILES['article_pdf']['name']);
                        $target_path = $target_paths . $articleName;
                        $target_path_thumb = $target_paths . 'thumb_' . $articleName;
                        move_uploaded_file($_FILES['article_pdf']["tmp_name"], $target_path);
                        echo $_POST['article_pdf'] = $articleName;
                    }
                //}
        }



        $getMax = "SELECT max(order_by) as count from " . _prefix("category_content_list") . " WHERE status = 1 && deleted = 0";
        $resMax = $db->sql_query($getMax);
        $dataMax = mysqli_fetch_assoc($resMax);
        $order = ($dataMax['count'] == NULL) ? 1 : $dataMax['count'] + 1;
        $fields = array(
            'title' => trim($category_name),
            'category ' => $category_topic,
            'category_content' => trim($content),
            'category_link' => trim($content_link),
            'category_img_name' => $_POST['banner_image'],
            'category_article_pdf' => $_POST['article_pdf'],
           'category_article_type' => trim($article_type)
            
        );
        $fields2 = array(
            'name' => trim($category_topic)
           
        );


        $insert_result = $db->insert(_prefix('category_content_list'), $fields);

         /****************check category name is exists*******************/
         if($insert_result){
             $cat_name_query="select name from ad_category_list where name ='$category_topic'";
             $res_cate=$db->sql_query($cat_name_query);
             $row=mysqli_num_rows($res_cate);
             if($row>0)
             {
                echo "Category name is already exists";
             }
             else
             {
                 $insert_category_list = $db->insert(_prefix('category_list'), $fields2);
             }
        }
         /*****************end of code of category name is exists***********/

        if ($insert_result) {
            // Message for insert
            $msg = common_message(1, constant('INSERT'));
            $_SESSION['msg'] = $msg;
            //ends here
            redirect_to(MAIN_PATH . "category_list.php?manage");
        }
    } else {
        $errors = '';
        foreach ($response as $key => $message) {
            $error = true;
            if ($message != '1.' && $message != '.') {
                $errors .= $message . "<br>";
            }
        }
    }
}




$imageName = '';
$image_path = '';
$banner_image = '';
$bannerName = '';



if (key($_REQUEST) == 'edit') {
    $id = $_GET['id'];
    $sql_query = "SELECT * FROM  ad_category_content_list where id='$id'";
    $res = $db->sql_query($sql_query);
    $records = $db->sql_fetchrowset($res); 
    if (count($records)) {
        foreach ($records as $record) {
                $id = $record['id'];
                $category_name2 = $record['title'];
                $category_topic2 = $record['category'];
                $category_content2 = $record['category_content'];
                $category_category_img_name2 = $record['category_img_name']; 
                $content_link2 = $record['category_link'];
                $category_article_type =$record['category_article_type']; 
                $article_pdf=$record['category_article_pdf'];           
        }
    }    
}


/***************************for update****************************************/
if (isset($update) && $update == 'Update') {
    $fields_array =consumer_info();
    $response = Validation::_initialize($fields_array, $_POST);
    if (isset($response['valid']) && $response['valid'] > 0) {

        if (isset($_FILES['banner_image']['name'])) {

            if ($_FILES['banner_image']['error'] == 0) {
                $ext = end(explode('.', $_FILES['banner_image']['name']));
                $target_pathL = '../admin/files/pages/banner/';
                $date = new DateTime();
                $timestamp = $date->getTimestamp();
                $image_name = $timestamp . '_' . imgNameSanitize(basename($_FILES['banner_image']['name']), 20);
                $target_path = $target_pathL . $image_name;
                $target_path_thumb = $target_pathL . 'thumb_' . $image_name;
                move_uploaded_file($_FILES['banner_image']["tmp_name"], $target_path);
                ak_img_resize($target_path, $target_path_thumb, 50, 50, $ext);
                $_POST['banner_image'] = $image_name;
            }
        }


        if (isset($_FILES['article_pdf']['name'])) {


            if ($_FILES['article_pdf']['error'] == 0) {

                //$target_paths = '../admin/files/pages/feature_image/';
                $target_paths = '../admin/files/pages/article_pdf/';

                $date = new DateTime();
                $timestamp = $date->getTimestamp();
                $ext = end(explode('.', $_FILES['article_pdf']['name']));
                $articleName =basename($_FILES['article_pdf']['name']);
                $target_path = $target_paths . $articleName;
                $target_path_thumb = $target_paths . 'thumb_' . $articleName;
                move_uploaded_file($_FILES['article_pdf']["tmp_name"], $target_path);
                $_POST['article_pdf'] = $articleName;
            }
        }

        
        $category_article_type = $_POST["category_article_type"];
        $category_name=$_POST['title'];
        $category_topic=$_POST['category'];
        //$category_content=$_POST['category_content1'];
        $content = $_POST["content"];
        $content_link = $_POST["content_link"];
        $image1=$_POST['banner_image'];

         if($image1=='')
         {
             $image2= $category_category_img_name2;
         }

         else
         {

             $image2=$image1;
         }

        $fields = array(
            'title' => trim($category_name),
            'category' => $category_topic,
            'category_content' => trim($content),
            'category_link' => trim($content_link),
            'category_img_name' => $image2,
            'category_article_type'=>$category_article_type,
            'category_article_pdf'=>$_POST['article_pdf']
        );
         $fields2 = array(
            'name' => trim($category_topic)
           
        );


        /*echo "<pre>";
        print_r($fields);
        echo "</pre>";
        /* if ($_POST['banner_image'] != '') {
            echo "+++++".$fields['banner_image'] = $image1;
        }*/

        $where = "where id= '$id'";
        $update_result = $db->update(_prefix('category_content_list'), $fields, $where);
         /****************check category name is exists*******************/
         if($update_result){
             $cat_name_query="select name from ad_category_list where name ='$category_topic'";
             $res_cate=$db->sql_query($cat_name_query);
             $row=mysqli_num_rows($res_cate);
             if($row>0)
             {
                //echo "Category name is already exists";
             }
             else
             {
                 $insert_category_list = $db->insert(_prefix('category_list'), $fields2);
             }
         }


         /*****************end of code of category name is exists***********/


        if ($update_result) {
            // Message for insert
            $msg = common_message(1, constant('UPDATE'));
            $_SESSION['msg'] = $msg;
            //ends here
            redirect_to(MAIN_PATH . "category_list.php?manage");
        }
    } else {
        $errors = '';
        foreach ($response as $key => $message) {
            $error = true;
            $errors .= $message . "<br>";
        }
    }
}


/**************************end for code update**********************************/

/*if (isset($update) && $update == 'Update') {
    /*if (isset($_FILES['banner_image']['name'])) 
    {
        if ($_FILES['banner_image']['error'] == 0) 
        {*/
            /* $target_paths = '../admin/files/pages/banner/';

               
                $ext =  $_FILES['banner_image']['name'];
                $bannerName = basename($_FILES['banner_image']['name']);
                $target_path = $target_paths . $bannerName;
                $target_path_thumb = $target_paths . 'thumb_' . $bannerName;
                move_uploaded_file($_FILES['banner_image']["tmp_name"], $target_path);
                ak_img_resize($target_path, $target_path_thumb, 100, 100, $ext);
                $_POST['banner_image'] = $bannerName;
                
                if($bannerName!='')
                {
                     $bannerName_img=$bannerName;
                }
                else
                {

                     $bannerName_img=$category_category_img_name2;
                }


         
                   $category_update_query='update  ad_category_content_list  set  category_name="'.$category_name.'" ,category_topic="'.$category_topic.'"  ,category_img_name="'.$bannerName_img.'", category_content="'.$content.'" where id="'.$id.'" '; 

                 $category_up_query=mysqli_query($db->db_connect_id,$category_update_query);  
           
    
                if($category_up_query)  
                {
                    echo "<script> alert('category data successfully update'); window.location='category_list.php?manage';</script>";  
                }
                 else
                {
                    echo "<script> alert('Please Try Again');</script>";
                }
        /*}
     }*/
   
//}

?>

<div style="margin-top: 10px;">
    <table align="center" border="0" cellpadding="0" cellspacing="0" width="95%">
        <tbody>
            <tr style="background:#4b99e6;">
                <td>
                    <div class="breadcrumb">
                        <ul>
                           <li><a href="<?php echo HOME_PATH_URL; ?>admin.php"><i class="fa fa-home"></i>Home </a>>><a href="<?php echo HOME_PATH_URL . "category_list.php?manage" ?>"> Manage Consumer Information </a>>> <?php echo (key($_REQUEST) == 'edit') ? 'Edit' : 'Add'; ?></li>
                        </ul>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
    <div class="row">
        <div class="col-sm-offset-2 col-sm-6 success">
            <?php if (isset($error)) { ?>
                <div class="errorForm" id="error" style="display: block;">
                    <img align="absmiddle" src="<?php echo HOME_PATH . 'admin/images/error.png'; ?>">&nbsp;<?php echo $errors; ?>.<br>
                </div>
                <!--echo '<div style="color:#FF0000">' . $errors . '</div>';-->
                <?php
            }
            if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
                ?>
                <div class="errorForm" id="error" style="display: block;">
                    <img align="absmiddle" src="<?php echo HOME_PATH . 'admin/images/error.png'; ?>">&nbsp;<?php echo $_SESSION['msg']; ?>.<br>
                </div>
                <?php
                unset($_SESSION['msg']);     
            }
            ?>
        </div>
    </div>
    <div class="container" style="width: 95%; border: 1px solid rgb(204, 204, 204);">
        <div class="row" style="margin-bottom: 20px; border-bottom: 1px solid rgb(204, 204, 204);">
            <div class="col-sm-6">
                <h2 class=""><?php echo (key($_REQUEST) == 'edit') ? 'Edit' : 'Add'; ?> Consumer Information </h2>
            </div>

            <div class="col-sm-6">
                <h2 style="float:right;" class="redCol small">* fields required</h2>
            </div>
        </div>
        <p class="clearfix"></p>
        <form name="addLogo" id="addLogo" action="" method="POST" class="form-horizontal" role="form" enctype="multipart/form-data">
            <div class="form-group">
                <label for="title" class="col-sm-2 control-label">Title<span class="redCol">*</span></label>
                <div class="col-sm-8">
                    <input type="text"  class="required form-control" name="title" id="category_name"  value="<?php echo isset($category_name2) ? stripslashes($category_name2) : ''; ?>"/>
                </div>
            </div>
            <!--*******************for article pdf**********************************-->
          <div class="form-group">
                <label for="article_path" class="col-sm-2 control-label">Upload Article PDF <span class="redCol"></span></label>
                <div class="col-sm-8">
                    <input type="file" name="article_pdf" id="pdf_upload">
                    <label> Uploaded File:<?php if($article_pdf){ echo $article_pdf;} else {echo '';} ?></label>
                </div>
            </div>

            <div class="form-group">
                <label for="article_type" class="col-sm-2 control-label"> Article Type <span class="redCol"></span></label>
                <div class="col-sm-8">
                   <select name= "category_article_type">
                       <option value="">Choose article type</option>
                       <option value="Checklist"<?php if($category_article_type== 'Checklist'){ echo 'selected'; }?>>Checklist</option>
                       <option value="Free Report" <?php if($category_article_type== 'Free Report'){ echo 'selected'; }?> >Free Report</option>
                       <option value="Article for only $4.95" <?php if($category_article_type=='Article for only $4.95'){ echo 'selected'; }?> >Article for only $4.95</option>
                   </select>
                </div>
            </div>
            <!-- ************************************end of code for article*****************-->
             <!-- ********************************category List*************************************************-->
              <?php 
                    $select_category="select * from ad_category_list";
                    $select_category_list=mysqli_query($db->db_connect_id,$select_category);
                    $row=mysqli_num_rows($select_category_list);
                    if($row>0){
                ?>
             <div class="form-group"  id="category_list_data">
                <label for="title" class="col-sm-2 control-label">Category List<span class="redCol">*</span></label>
                <div class="col-sm-8">
                  <select name="category_list" class="form-control" id="category_list">
                  <option value="">Select Category</option>
                    <?php
                    while($fetch_category_list=mysqli_fetch_array($select_category_list))
                    {   
                        $category_id=$fetch_category_list['id'];
                        $category_name=$fetch_category_list['name'];
                  ?>
                    <option value="<?php echo $category_name;?>"><?php echo $category_name;?></option>
                <?php }?>
                 </select>
                </div>
            </div>
            <?php } ?>
             <!-- *************************end of code for category List*************************************************-->
            



            <div class="form-group" id="cat_topic">
                <label for="title" class="col-sm-2 control-label">Category<span class="redCol">*</span></label>
                <div class="col-sm-8">
                    <input type="type" id="category" class="required form-control" name="category" value="<?php echo isset($category_topic2) ? stripslashes($category_topic2) : ''; ?>"/>
                </div>
            </div>
            
            <!--<div class="form-group"> 
                <label for="title" class="col-sm-2 control-label"> Content<span class="redCol">*</span></label>
                <div class="col-sm-8">
                    
                    <textarea  class="required form-control" name="category_content1" rows="10" id="category_content"><?php //echo isset($category_content2) ? //stripslashes($category_content2) : ''; ?></textarea>
                </div>
            </div>-->
              <?php
            if (isset($_POST['content'])) {

                 $content = $_POST['content'];
            } elseif (isset($content)) {
                $content = $content;
            } else {
                'empty';
                $content = '';
            }
            ?> 


          <div class="form-group">
                <label for="content" class="col-sm-2 control-label">Content<span class="redCol">* </span></label>
                <div class="col-sm-8">
                    <?php
                    $oFCKeditor = new FCKeditor('content');
                    $oFCKeditor->BasePath = './fckeditor/';
                    $oFCKeditor->Height = '400px';
                    $oFCKeditor->Value = $category_content2;
                    $oFCKeditor->class = "form-control";
                    $oFCKeditor->Create();
                    ?>
                </div>
            </div>
              <div class="form-group">
                <label for="title" class="col-sm-2 control-label">Category Link<!--<span class="redCol">*</span>--></label>
                <div class="col-sm-8">
                    <input type="type" class="form-control" name="content_link" value="<?php echo isset($content_link2) ? stripslashes($content_link2) : ''; ?>"/>
                </div>
            </div>

            <div class="form-group">
                <label for="image_path" class="col-sm-2 control-label">Upload Image<span class="redCol"></span></label>
                <div class="col-sm-8">
                    <input type="file" name="banner_image" id="image_upload">
                </div>
            </div>
            <div class="form-group">
                <!--<label for="banner_image" class="col-sm-2 control-label">Upload Banner<span class="redCol">* </span></label>-->
                <div class="col-sm-offset-2 col-sm-8">
                    <?php
                    if ($type == 0) {

                        

                        if (file_exists(ADMIN_PATH . 'files/pages/banner/' . $category_category_img_name2) && $category_category_img_name2 != '' && key($_REQUEST) == 'edit') {  
                            ?>
                            <img width="100px;" height="100px;" title="feature" src="<?php echo MAIN_PATH . '/files/pages/banner/' . $category_category_img_name2 ?>">
                            <?php
                        }
                    } else {

                        if (file_exists(ADMIN_PATH . 'files/pages/feature_image/' . $banner_image) && $banner_image != '' && key($_REQUEST) == 'edit') {  
                            ?>
                            <img width="100px;" height="100px;" title="feature" src="<?php echo MAIN_PATH . '/files/pages/feature_image/' . $category_category_img_name2 ?>">
                            <?php
                        }
                    }
                    ?>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-8">
                    <?php
                    if (key($_REQUEST) == 'edit') {
                        ?>
                        <input type="submit" value="Update" name="update" class="submit_btn btn" src="<?php echo ADMIN_IMAGE; ?>submit.gif">

                        <?php
                    } else {
                        ?>
                        <input type="submit" value="Submit" name="submit" class="submit_btn btn">
                        <?php
                    }
                    ?>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('input[type="submit"]').focus();
    });
    $('#addLogo').validate();
<?php if (file_exists(DOCUMENT_PATH . 'admin/files/community_logos/' . $image) && $image != '' && key($_REQUEST) == 'edit') { ?>
        $('#image').removeClass('required');
<?php } ?>
</script>

<script type="text/javascript">
    $(document).ready(function() {

        $('input[type="submit"]').focus();
        $('.imageupload').hide();

        $('#type').change(function() {
            var value = $(this).val();

            if (parseInt(value) == 1) {
                $('.imageupload').show();
                $('#image_upload').addClass('required');
            } else {
                $('.imageupload').hide();
                $('#image_upload').removeClass('required');
            }
        });

<?php if ((isset($type) && $type == 1) || (isset($_POST['type']) && $_POST['type'] == 1)) {
    ?>
            $('.imageupload').show();
<?php }
?>
        $('#addPage').validate({
            rules: {
                image_path: {
                    extension: "jpeg|png|gif|jpg"
                },
                banner_image: {
                    extension: "jpeg|png|gif|jpg"
                },
                article_pdf: {
                    extension: "pdf"
                }
            },
            messages: {
                image_path: {
                    extension: "Please upload valid file formats"
                },
                banner_image: {
                    extension: "Please upload valid file formats"
                },
                article_pdf: {
                    extension: "Please upload valid file formats"
                }
            }
        });


    });




</script>
<script type="text/javascript">
$(document).ready(function(){
var get_value_data=$("#category").val();
//alert(get_value_data);

$("#category_list").change(function(){
var get_value=$("#category_list").val();
        if(get_value!='')
        {
             $("#category").attr('readonly','readonly');
            var category_data=$("#category").val(get_value);
            var category_data1=$("#category").val();
            //alert(category_data1);
            $("#cat_topic").hide(200);
            
        }
        else
        {
              if(get_value==''|| get_value==null)
              { 
                    if($('#category').is('[readonly]')){ 
                       $("#category").removeAttr('readonly');
                       var cat_data =$("#category").val(get_value_data);
                        $("#cat_topic").show(200);
                      // alert(cat_data);
                    }
                    else
                    {
                       var cat_data=$("#category").val(get_value_data); 
                       //alert(cat_data);
                    }      
              }
        }
    });
    
});
</script>



