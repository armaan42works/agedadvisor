<script type="text/javascript">
    $(document).ready(function() {
        var page = 0;
        var li = '0_no';
        var data = '&start_date=&search_type=&end_date=&forumId=<?php echo $id; ?>';
        changePagination(page, li, 'forumComments', data);
        $('#search_form').submit(function(e) {
            $('#search_form').validate();
            e.preventDefault();
            data = '&start_date=' + $('#start_date').val() + '&search_type=' + $('#search_type').val() + '&end_date=' + $('#end_date').val();
            changePagination(page, li, 'forumComments', data);
        });
    });
</script>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="95%">
    <tbody>
        <tr style="background:#4b99e6;">
            <td>
                <div class="breadcrumb">
                    <ul>
                        <li><a href="<?php echo HOME_PATH_URL; ?>admin.php"><i class="fa fa-home"></i>Home </a>>><a href="<?php echo HOME_PATH_URL . "forum.php?manage" ?>">Manage Forum</a>>>Manage Comment</li>
                    </ul>
                </div>
            </td>
        </tr>
    <div class="sublinks" height="40" valign="bottom" style="margin-bottom: 10px;">
        <?php
            if (isset($error)) {
                echo '<div style="color:#FF0000">' . $errors . '</div>';
            }
            if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
                echo $_SESSION['msg'];
                unset($_SESSION['msg']);
            }
        ?>
    </div>
    <tbody>
        <tr>
            <td>
                <table class="table table-striped table-hover">

                    <tbody  id="pageData">

                        <?php // echo '<div id="pageData"></div>'; ?>
                    </tbody>

                </table>
            </td>
        </tr>
    </tbody></tbody></table>
<table class="ds_box" cellpadding="0" cellspacing="0" id="ds_conclass" style="display: none;">
    <tr><td id="ds_calclass"></td></tr></table>
