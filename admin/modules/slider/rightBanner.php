<style>
    th,td,tr{border:0px!important; }
    .input{background-color:white;}
</style>
<?php
    global $db;
    $description = '';
    $image = '';
    if (isset($submit) && $submit == 'Submit') {
        $fields_array = slider();
        $response = Validation::_initialize($fields_array, $_POST);
        if (isset($response['valid']) && $response['valid'] > 0) {
            if (isset($_FILES['image']['name'])) {

                if ($_FILES['image']['error'] == 0) {
                    $ext = end(explode('.', $_FILES['image']['name']));
                    $target_pathL = '../admin/files/slider/';
                    $date = new DateTime();
                    $timestamp = $date->getTimestamp();
                    $logoName = $timestamp . '_' . imgNameSanitize(basename($_FILES['image']['name']), 20);
                    $target_path = $target_pathL . $logoName;
                    $target_path_thumb = $target_pathL . 'thumb_' . $logoName;
                    move_uploaded_file($_FILES['image']["tmp_name"], $target_path);
                    ak_img_resize($target_path, $target_path_thumb, 50, 50, $ext);
                    $_POST['image'] = $logoName;
                }
            }

            $fields = array(
                'title' => trim($title),
                'description' => trim($_POST['description']),
                'image' => $_POST["image"],
                'created_by' => $_SESSION['Aid'],
                'created' => date('Y-m-d h:i:s', time())
            );


            $insert_result = $db->insert(_prefix('right_banner'), $fields);
            if ($insert_result) {
                // Message for insert
                $msg = common_message(1, constant('INSERT'));
                $_SESSION['msg'] = $msg;
                //ends here
                redirect_to(MAIN_PATH . "/slider.php?rightBanner");
            }
        } else {
            $errors = '';
            foreach ($response as $key => $message) {
                $error = true;
                $errors .= $message . "<br>";
            }
        }
    }
    if (key($_REQUEST) == 'rightBanner') {
//    $id = $_GET['id'];
        $sql_query = "SELECT * FROM " . _prefix("right_banner") . "   order by id desc limit 1";
        $res = $db->sql_query($sql_query);
        $records = $db->sql_fetchrowset($res);
        if (count(array_filter($records))) {
            $idBan = $records[0]['id'];
            $title = $records[0]['title'];
            $description = $records[0]['description'];
            $image = $records[0]['image'];
        } else {
            $emptyFlag = true;
        }
    }
    if (isset($update) && $update == 'Update') {

        $fields_array = slider();
        $response = Validation::_initialize($fields_array, $_POST);
        if (isset($response['valid']) && $response['valid'] > 0) {

            if (isset($_FILES['image']['name'])) {

                if ($_FILES['image']['error'] == 0) {
                    $ext = end(explode('.', $_FILES['image']['name']));
                    $target_pathL = '../admin/files/slider/';

                    $date = new DateTime();
                    $timestamp = $date->getTimestamp();
                    $logoName = $timestamp . '_' . imgNameSanitize(basename($_FILES['image']['name']), 20);
                    $target_path = $target_pathL . $logoName;
                    $target_path_thumb = $target_pathL . 'thumb_' . $logoName;
                    move_uploaded_file($_FILES['image']["tmp_name"], $target_path);
                    ak_img_resize($target_path, $target_path_thumb, 50, 50, $ext);
                    $_POST['image'] = $logoName;
                }
            }
            $title = trim($_POST["title"]);
            $description = trim($_POST["description"]);
            $image = $_POST["image"];
            $fields = array('title' => trim($title),
                'description' => trim($description),
            );
            if ($_POST['image'] != '') {
                $fields['image'] = $image;
            }
            $where = "where id = " . $_POST['id'] . " ";
            $update_result = $db->update(_prefix('right_banner'), $fields, $where);
            if ($update_result) {
                // Message for insert
                $msg = common_message(1, constant('UPDATE'));
                $_SESSION['msg'] = $msg;
                //ends here
                redirect_to(MAIN_PATH . "/slider.php?rightBanner");
            }
        } else {
            $errors = '';
            foreach ($response as $key => $message) {
                $error = true;
                $errors .= $message . "<br>";
            }
        }
    }
?>

<div class="row" style="margin-bottom: 20px; border-bottom: 1px solid rgb(204, 204, 204);">
    <div class="col-sm-offset-2 col-sm-6 success">
        <?php if (isset($_SESSION['msg_err'])) { ?>
                <div class="errorForm" id="error" style="display: block;">
                    <img align="absmiddle" src="<?php echo HOME_PATH . '/images/error.png'; ?>">&nbsp;<?php echo $_SESSION['msg_err']; ?>
                </div>
                <?php
                unset($_SESSION['msg_err']);
            }
            if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
                ?>
                <!--                <div class="successForm" id="success" style="display: block;">-->
                                    <!--<img align="absmiddle" src="<?php // echo HOME_PATH . '/images/success.png';             ?>">-->
                &nbsp;<?php echo $_SESSION['msg']; ?>
                <!--                </div>-->
                <?php
                unset($_SESSION['msg']);
            }
        ?>
    </div>
</div>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="95%">
    <tbody>
        <tr style="background:#4b99e6;">
            <td>
                <div class="breadcrumb">
                    <ul>
                        <li><a href="<?php echo HOME_PATH_URL; ?>admin.php"><i class="fa fa-home"></i>Home </a>>><a href="<?php echo HOME_PATH_URL . "slider.php?rightBanner" ?>"> Manage Right panel banner</a>>> <?php echo (key($_REQUEST) == 'edit') ? 'Edit' : 'Add'; ?></li>
                    </ul>
                </div>
            </td>
        </tr>
    </tbody>
</table>

<div class="container" style="width: 95%; border: 1px solid rgb(204, 204, 204);">
    <div class="row" style="margin-bottom: 20px; border-bottom: 1px solid rgb(204, 204, 204);">
        <div class="col-sm-6">
            <h2 class=""><?php echo (key($_REQUEST) == 'edit') ? 'Edit' : 'Add'; ?> Right panel banner</h2>
        </div>

        <div class="col-sm-6">
            <h2 style="float:right;" class="redCol small">* fields required</h2>
        </div>
    </div>
    <p class="clearfix"></p>
    <form name="addSiler" id="addSiler" action="" method="POST" class="form-horizontal" role="form" enctype="multipart/form-data">
        <div class="form-group">
            <label for="title" class="col-sm-2 control-label">Title<span class="redCol">*</span></label>
            <div class="col-sm-8">
                <input type="text"  class="required form-control" name="title" minlength="2" maxlength="50" id="title"  value="<?php echo isset($title) ? stripslashes($title) : ''; ?>"/>
            </div>
        </div>
        <div class="form-group">
            <label for="description" class="col-sm-2 control-label">Description<span class="redCol">* </span></label>
            <div class="col-sm-8">
                <textarea rows="4" cols="50" minlength="4" maxlength="500" name="description" class="required form-control" id=""><?php
                        if (isset($description)) {
                            echo stripslashes($description);
                        }
                    ?></textarea>
            </div>
        </div>
        <div class="form-group">
            <label for="image" class="col-sm-2 control-label">Upload image<span class="redCol">*</span><h2 class="redCol small pull-left"  style="float:left;margin-top: 2px;">[Image should be 351x293 pixel's size]</h2> </label>
            <div class="col-sm-8">
                <input type="file" name="image" id="image" class="required">
                <input type="hidden" name="id" id="id" value="<?php echo $idBan; ?>"/>
            </div>
        </div>
        <div class="form-group">
            <!--<label for="image" class="col-sm-2 control-label">Slider image<span class="redCol">* </span></label>-->
            <div class="col-md-2"></div>
            <div class="col-sm-8">
                <?php // if (file_exists(DOCUMENT_PATH . 'admin/files/slider/' . $image) && $image != '') { ?>
                <img width="100px;" height="100px;" title="feature" src="<?php echo MAIN_PATH . '/files/slider/' . $image ?>"/>
                <?php // } ?>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-8">
                <?php
                    if (!$emptyFlag) {
                        ?>
                        <input type="submit" value="Update" name="update" class="submit_btn btn" src="<?php echo ADMIN_IMAGE; ?>submit.gif">

                        <?php
                    } else {
                        ?>
                        <input type="submit" value="Submit" name="submit" class="submit_btn btn">
                        <?php
                    }
                ?>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript">
    $(document).ready(function() {

        $('input[type="submit"]').focus();
    });
    $('#addSiler').validate();
<?php if (file_exists(DOCUMENT_PATH . 'admin/files/slider/' . $image) && $image != '' && key($_REQUEST) == 'edit') { ?>
            $('#image').removeClass('required');
    <?php } ?>
</script>



