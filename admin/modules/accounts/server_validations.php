<?php

    /* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function change_password(){
    $fields_array = array('method' => 'REQUEST','rules' =>array(
                            'username' => array('required' => true),
                            'old_password' => array('required' => true),
                            'new_password' => array('required' => true,'minLength'=>6, 'maxLength'=>30),
                            'confirm_password' => array('required' => true,'minLength'=>6,'maxLength'=>30)),
                        'messages' => array(
                          'username' => array('required' => 'Please enter username'),
                          'old_password' => array('required' => 'Please enter old password'),
                          'new_password' => array('required' => 'Please enter new password'),
                          'confirm_password' => array('required' => 'Please enter confirm password'))
                      );
    
    return $fields_array;
}


