<script type="text/javascript">
    $(document).ready(function() {
        var page = 0;
        var li = '0_no';
        var data = '&start_date=&search_type=&end_date=&id=<?php echo $id; ?>';
        changePagination(page, li, 'partwork', data);
        $('#search_form').submit(function(e) {
            $('#search_form').validate();
            e.preventDefault();
            data = '&start_date=' + $('#start_date').val() + '&search_type=' + $('#search_type').val() + '&end_date=' + $('#end_date').val();
            changePagination(page, li, 'partwork', data);
        });
    });
</script>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="95%">

    <tbody>


        <tr style="background:#4b99e6;">
            <td>
                <div class="breadcrumb">
                    <ul>
                        <li><a href="<?php echo HOME_PATH_URL; ?>admin.php"><i class="fa fa-home"></i>Home </a>>>Manage Artworks </li>
                    </ul>
                </div>
            </td>
        </tr>

    <div class="sublinks" height="40" valign="bottom" style="margin-bottom: 10px;">
        <?php
            if (isset($error)) {
                echo '<div style="color:#FF0000">' . $errors . '</div>';
            }
            if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
                echo $_SESSION['msg'];
                unset($_SESSION['msg']);
            }
        ?>
    </div>

    <tr>
        <!----------------------------- End here --------------------------->
        <?php
            $search_array = array('created' => 'Date Issued', 'expire' => 'Expiration Date');
        ?>
        <td class="sublinks" height="40" valign="bottom">
            <form id="search_form" method="post" action="">
                <input type="text" name="start_date" id="start_date" value="<?php echo date('Y-m-d'); ?>" placeholder="Start Date">
                <input type="text" name="end_date" id="end_date" value="<?php echo date('Y-m-d'); ?>" placeholder="End Date">
                <select name="search_type" id="search_type">
                    <?php
                        $option = '<option value="">------ Choose Option-----</option>';
                        foreach ($search_array as $key => $value) {
                            $option .="<option value='$key'>$value</option>";
                        }
                        echo $option;
                    ?>
                </select>
                <input type="submit" value="Search">
            </form>
        </td>
    </tr>
    <tbody>
        <tr>
            <td>
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                        <?php echo '<div id="pageData"></div>'; ?>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody></tbody></table>
<table class="ds_box" cellpadding="0" cellspacing="0" id="ds_conclass" style="display: none;">
    <tr><td id="ds_calclass"></td></tr></table>

