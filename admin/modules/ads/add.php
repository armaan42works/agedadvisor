<style>
    th,td,tr{border:0px!important;}
    .input{background-color:white;}
</style>
<?php
global $db;
$description = '';
include_once("./fckeditor/fckeditor.php");

if (isset($submit) && $submit == 'Submit') {
    $fields_array = ads_masters();
    $response = Validation::_initialize($fields_array, $_POST);
    if (isset($response['valid']) && $response['valid'] > 0) {
        if (isset($_FILES['image']['name'])) {

            if ($_FILES['image']['error'] == 0) {
                $ext = end(explode('.', $_FILES['image']['name']));
                $target_pathL = '../admin/files/ads_image/';
                $date = new DateTime();
                $timestamp = $date->getTimestamp();
                $adsImage = $timestamp . '_' . imgNameSanitize(basename($_FILES['image']['name']), 20);
                $target_path = $target_pathL . $adsImage;
                $target_path_thumb = $target_pathL . 'thumb_' . $adsImage;
                move_uploaded_file($_FILES['image']["tmp_name"], $target_path);
                ak_img_resize($target_path, $target_path_thumb, 50, 50, $ext);
                $_POST['image'] = $adsImage;
            }
        }

        $getMax = "SELECT max(order_by) as count from " . _prefix("ads_masters") . " WHERE status = 1 && deleted = 0";
        $resMax = $db->sql_query($getMax);
        $dataMax = mysqli_fetch_assoc($resMax);
        $order = ($dataMax['count'] == NULL) ? 1 : $dataMax['count'] + 1;
        if (!filter_var($_POST["url_link"], FILTER_VALIDATE_URL) === false) {
            $url_link = $_POST["url_link"];
        }else{
            $url_link = NULL;
        }
        /******code for city and national****/
        if($_POST['city']=='')
         {
            echo $_POST['city']=$_POST['national'];
            echo $_POST['city_id']=-1;
        }
      /******end of code for city and national***/
        $fields = array(
            'title' => trim($_POST['title']),
            'description' => trim($_POST['description']),
            'image' => $_POST["image"],
            'url_link' => $url_link,
            'space_id' => trim($_POST["space_id"]),
             'city_id'=>$_POST['city_id'],
            'city_name'=>strtolower($_POST['city']),
            'supplier_id' => $_SESSION['Aid'],
            'ads_mode' => trim($_POST["ads_mode"]),
            'service_category_id' => trim($_POST["service_category_id"]),
            'created' => date('Y/m/d h:i:s', time())
        );
        $insert_result = $db->insert(_prefix('ads_masters'), $fields);
        if ($insert_result) {
            // Message for insert
            $msg = common_message(1, constant('INSERT'));
            $_SESSION['msg'] = $msg;
            //ends here
            redirect_to(MAIN_PATH . "/ads.php?manage");
        }
    } else {
        $errors = '';
        foreach ($response as $key => $message) {
            $error = true;
            $errors .= $message . "<br>";
        }
    }
}
if (key($_REQUEST) == 'edit') {
    $id = $_GET['id'];
    $sql_query = "SELECT * FROM " . _prefix("ads_masters") . " where md5(id)='$id'";
    $res = $db->sql_query($sql_query);
    $records = $db->sql_fetchrowset($res);
    if (count($records)) {
        foreach ($records as $record) {
            $url_link = $record['url_link'];
            $title = $record['title'];
            $description = $record['description'];
            $image = $record['image'];
            $space_id = $record['space_id'];
            $supplier_id = $record['supplier_id'];
            $city_id = $record['city_id'];
            $city = $record['city_name'];
            $ads_mode = $record['ads_mode'];
            $service_category_id = $record['service_category_id'];
        }
    }
}
if (isset($update) && $update == 'Update') {

    $fields_array = ads_masters();
    $response = Validation::_initialize($fields_array, $_POST);
    if (isset($response['valid']) && $response['valid'] > 0) {
        if (isset($_FILES['image']['name'])) {

            if ($_FILES['image']['error'] == 0) {
                $ext = end(explode('.', $_FILES['image']['name']));
                $target_pathL = '../admin/files/ads_image/';
                $date = new DateTime();
                $timestamp = $date->getTimestamp();
                $adsImage = $timestamp . '_' . imgNameSanitize(basename($_FILES['image']['name']), 20);
                $target_path = $target_pathL . $adsImage;
                $target_path_thumb = $target_pathL . 'thumb_' . $adsImage;
                move_uploaded_file($_FILES['image']["tmp_name"], $target_path);
                ak_img_resize($target_path, $target_path_thumb, 50, 50, $ext);
                $_POST['image'] = $adsImage;
            }
        }
        if (!filter_var($_POST["url_link"], FILTER_VALIDATE_URL) === false) {
            $url_link = $_POST["url_link"];
        }else{
            $url_link = NULL;
        }
        $title = $_POST['title'];
        $description = $_POST['description'];
        $image = $_POST['image'];
        $space_id = $_POST['space_id'];
        //$supplier_id = $_POST["supplier_id"];
         $supplier_id = 1;

        $city_id = $_POST["city_id"];

        $city_id = $_POST["city_id"];
        
        
          if($_POST['city']=='')
         {
             $_POST['city']=$_POST['national'];
            $city_id=-1;
        }
        $city = strtolower($_POST["city"]);
        $ads_mode = $_POST["ads_mode"];
        $service_category_id = $_POST["service_category_id"];
        $fields = array('title' => trim($title),
            'description' => trim($description),
            'space_id' => $space_id,
            //'supplier_id' => $_SESSION['id'],
            'supplier_id' =>  $supplier_id,
            'url_link' => $url_link,
            'ads_mode' => $ads_mode,
            'city_id'=>$city_id,
            'city_name'=>$city,
            'service_category_id' => $service_category_id,
            'modified' => date('Y-m-d h:i:s', time())
        );
        if ($_POST['image'] != '') {
            $fields['image'] = $image;
        }
        $where = "where md5(id)= '$id'";
//            prd(get_class_methods($db));
        $update_result = $db->update(_prefix('ads_masters'), $fields, $where);

        if ($update_result) {
            // Message for insert
            $msg = common_message(1, constant('UPDATE'));
            $_SESSION['msg'] = $msg;
            //ends here
            redirect_to(MAIN_PATH . "/ads.php?manage");
        }
    } else {
        $errors = '';
        foreach ($response as $key => $message) {
            $error = true;
            $errors .= $message . "<br>";
        }
    }
}
?>
<script type="text/javascript">
    $(function() {
    
  // var availableTags ='search.php';
    $("#city").autocomplete({

        //source:availableTags,
        //focus: true,
        
        source: function (request,response) {
        $.ajax({
            method: "post",
            url:'/ajaxFront.php',
            minlength:3,
            data: {
                'action':'citiesname', 'maxRows': 12,
                'name': request.term
                },
            success: function (result) {
                var data = $.parseJSON(result);
                     response($.map(data, function(item)
                     {
                        return {
                            label: item.label,
                            value: item.value
                     }
            }));
            
        }
    });
    },
        select: function(event, ui) {
                              $this = $(this);
                             setTimeout(function(){
          
            $("#city").val(ui.item.label); 
            $("#city_id").val(ui.item.value); 
            });
    },
        open: function(event, ui) {
        $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
         $('ul.ui-widget-content').css({"z-index": "9999", "display": "block", "top": autoT, "left": autoL});
          },
        close: function() {
        $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
        }
});
});
</script>




<div style="margin-top: 10px;">
    <table align="center" border="0" cellpadding="0" cellspacing="0" width="95%">
        <tbody>
            <tr style="background:#4b99e6;">
                <td>
                    <div class="breadcrumb">
                        <ul>
                            <li><a href="<?php echo HOME_PATH_URL; ?>admin.php"><i class="fa fa-home"></i>Home </a>>><a href="<?php echo HOME_PATH_URL . "ads.php?manage" ?>">Manage Ads</a>>> <?php echo (key($_REQUEST) == 'edit') ? 'Edit' : 'Add'; ?></li>
                        </ul>
                    </div>
            </tr>
        </tbody>
    </table>
    <div class="row">
        <div class="col-sm-offset-2 col-sm-6 success">
            <?php if (isset($error)) { ?>
                <div class="errorForm" id="error" style="display: block;">
                    <img align="absmiddle" src="<?php echo HOME_PATH . 'admin/images/error.png'; ?>">&nbsp;<?php echo $errors; ?>.<br>
                </div>
                <!--echo '<div style="color:#FF0000">' . $errors . '</div>';-->
                <?php
            }
            if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
                ?>
                <div class="errorForm" id="error" style="display: block;">
                    <img align="absmiddle" src="<?php echo HOME_PATH . 'admin/images/error.png'; ?>">&nbsp;<?php echo $_SESSION['msg']; ?>.<br>
                </div>
                <?php
                unset($_SESSION['msg']);
            }
            ?>
        </div>
    </div>

    <div class="container" style="width: 95%; border: 1px solid rgb(204, 204, 204);">
        <div class="row" style="margin-bottom: 20px; border-bottom: 1px solid rgb(204, 204, 204);">
            <div class="col-sm-6">
                <h2 class=""><?php echo (key($_REQUEST) == 'edit') ? 'Edit' : 'Add'; ?> Supplier Ads</h2>
            </div>

            <div class="col-sm-6">
                <h2 style="float:right;" class="redCol small">* fields required</h2>
            </div>
        </div>
        <p class="clearfix"></p>
        <form name="addAds" id="addAds" method="POST" action=""  class="form-horizontal" role="form" enctype="multipart/form-data">
            <div class="form-group">
                <label for="title" class="col-sm-2 control-label">Title<span class="redCol">* </span></label>
                <div class="col-sm-8">
                    <input type="text" minlength="2" maxlength="50" class="required form-control" name="title" id="title"  value="<?php echo isset($title) ? stripslashes($title) : ''; ?>"/>
                </div> 
            </div>
            <div class="form-group">
                <label for="content" class="col-sm-2 control-label">Content<span class="redCol">* </span></label>
                <div class="col-sm-8">
                    <textarea rows="4" cols="50" maxlength="500" name="description" class="required form-control" id=""><?php echo (isset($description) && !empty($description) ) ? stripslashes($description) : ''; ?></textarea>
                </div>
            </div>
            <div class="form-group">
                <label for="content" class="col-sm-2 control-label">Url Link<span class="redCol">* </span></label>
                <div class="col-sm-8">
                    <input type="text" class="required form-control" name="url_link" id="url_link"  value="<?php echo isset($url_link) ? stripslashes($url_link) : ''; ?>"/>
                </div>
            </div>
            <!--        <div class="form-group">
                        <label for="supplier_id" class="col-sm-2 control-label">Supplier<span class="redCol">* </span></label>
                        <div class="col-sm-8">
                            <select id="supplier_id" class="required supplier_id form-control" name="supplier_id" style="width:235px;"><?php // echo supplierList($supplier_id);                           ?></select>
                        </div>
                    </div>-->
                <!--********************************code for city name and id ******************-->
               <!--**************************select location****************************-->
                 <div class="form-group">
                        <label for="supplier_id" class="col-sm-2 control-label">Select Location<span class="redCol">* </span></label>
                        <div class="col-sm-8">
                            
                            <strong>Local City</strong>&nbsp; &nbsp;&nbsp; <input type="radio" name="location" id ="local" class="local">&nbsp; &nbsp;&nbsp;
                            <strong>National</strong>&nbsp; &nbsp;&nbsp;<input type="radio" name="location" id ="national" class="national">
                        </div>
                    </div>
                <!-- *****************************end of select location*************-->
                <div class="form-group" id="city_id_detail">
                <label for="location_id" class="col-sm-2 control-label">City<span class="redCol">* </span></label>
                <div class="col-sm-8">
                   <!-- <select id="location_id" class="required location_id form-control" name="location_id" style="width:235px;"><?php //echo serviceCategoryAdmin($location_id); ?></select>-->
                     <input type="hidden"  id="city_id" name="city_id">
                    <?php if (key($_REQUEST) == 'edit' && $city_id==-1){ ?>
                   <input type="text" minlength="2" maxlength="50" class="required form-control" name="city" id="city"  value=""/>
                    <?php } else {?>
                   <input type="text" minlength="2" maxlength="50" class="required form-control" name="city" id="city"  value="<?php echo isset($city) ? stripslashes($city) : ''; ?>"/>
                   <?php }?>
                </div>
            </div>
            <!-- ************************************end of code for city name and id************************-->

             <!--**************************************code for national ******************************-->
           <div class="form-group" id="national_id">
                <label for="location_id" class="col-sm-2 control-label">National<span class="redCol">* </span></label>
                <div class="col-sm-8">
                  
                   
                   <input type="text" minlength="2" maxlength="50" class="required form-control" name="national" id="national_val"  value="New Zealand"/>
                   <?php 
                    if(key($_REQUEST) == 'edit' && $city=='New Zealand' ){?>
                     <input type="text" minlength="2" maxlength="50" class="required form-control" name="national" id="national_val"  value="New Zealand"/> 
                   <?php }?>
                </div>
            </div>
            <!--***********************************end of code for national*******************************-->



            <div class="form-group">
                <label for="service_category_id" class="col-sm-2 control-label">Service Category<span class="redCol">* </span></label>
                <div class="col-sm-8">
                    <select id="service_category_id" class="required service_category_id form-control" name="service_category_id" style="width:235px;"><?php echo serviceCategoryAdmin($service_category_id); ?></select>
                </div>
            </div>
            <!--        <div class="form-group">
                        <label for="space_id" class="col-sm-2 control-label">Page Space Position<span class="redCol">* </span></label>
                        <div class="col-sm-8">
                            <select id="space_id" class="required space_id form-control" name="space_id" style="width:235px;"><?php echo spaceList($space_id); ?></select>
                        </div>
                    </div>-->
            <div class="form-group">
                <?php // $select_type = array("0" => "Normal", "1" => "Rotational", "2" => "Random"); ?>
                <?php $select_type = array("0" => "Normal", "2" => "Random"); ?>
                <label for="ads_mode" class="col-sm-2 control-label">Page Space Type<span class="redCol">* </span></label>
                <div class="col-sm-8">
                    <select name="ads_mode" id='ads_mode' class="required form-control"  style="width:235px;"><?php
                        $ads_mode = $ads_mode != '' ? $ads_mode : $_POST['plan_type'];
                        echo options($select_type, $ads_mode);
                        ?></select>
                </div>
            </div>
            <div class="form-group">
                <label for="image" class="col-sm-2 control-label">Upload Image<span class="redCol"><?php echo isset($image) ? '' : '*'; ?> </span></label>
                <div class="col-sm-8">
                    <input type="file" name="image" id="image" class="<?php echo isset($image) ? '' : 'required'; ?>">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-8">
                    <?php if (file_exists(DOCUMENT_PATH . 'admin/files/ads_image/' . $image) && $image != '' && key($_REQUEST) == 'edit') { ?>
                        <img width="100px;" height="100px;" title="feature" src="<?php echo MAIN_PATH . '/files/ads_image/' . $image ?>">
                    <?php } ?>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-8">
                    <?php
                    if (key($_REQUEST) == 'edit') {
                        ?>
                        <input type="submit" value="Update" name="update" class="submit_btn btn" src="<?php echo ADMIN_IMAGE; ?>submit.gif">

                        <?php
                    } else {
                        ?>
                        <input type="submit" value="Submit" name="submit" class="submit_btn btn">
                        <?php
                    }
                    ?>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('input[type="submit"]').focus();
    });
    $('#addAds').validate();
<?php if (file_exists(DOCUMENT_PATH . 'admin/files/ads_image/' . $image) && $image != '' && key($_REQUEST) == 'edit') { ?>
        $('#image').removeClass('required');
<?php } ?>


$(document).ready(function(){
var city_val=$("#city").val();
var national_val=$("#national_val").val();
$("#national_id").hide();
$("#local").prop('checked', true); 
$("#national").click(function(){
   $("#city_id_detail").hide();
   $("#city").removeClass("required");
   $('#city').attr('value', ''); 
   //$("#national_val").val(national_val);
   $("#national_id").show();
});

$("#local").click(function(){
   $("#national_id").hide();
    $("#national").removeClass("required");
    $('#city').val(city_val); 
   $("#city_id_detail").show();
});
<?php
 if(key($_REQUEST) == 'edit' && $city_id==-1){?> 
    $("#city_id_detail").hide();
    $("#national_id").show();
    $("#national").prop('checked', true); 
 <?php } 
  elseif(key($_REQUEST) == 'edit' && $city_id!=-1)
{?>
    $("#national_id").hide();
    $("#city_id_detail").show();
    $("#local").prop('checked', true); 
<?php } ?>
});

</script>
