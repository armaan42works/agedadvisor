<?php
$getSalesperson = "SELECT du.*, dc.name as countryname FROM " . _prefix("users") . " as du inner join " . _prefix("countries") . " as dc on du.country_id = dc.id  WHERE md5(du.id) = '{$id}'";
$res = $db->sql_query($getSalesperson);
$data = $db->sql_fetchrow($res);
?>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="95%">

    <tbody>
        <tr>
            <td height="10">&nbsp;</td>
        </tr>

        <tr>
            <td>
                <div class="breadcrumb"><a href="<?php echo HOME_PATH_URL; ?>admin.php"><i class="fa fa-home"></i>Home </a>>><a href="<?php echo HOME_PATH_URL . "user.php?sp" ?>">Manage Sales Person</a>>><b>Detail</b></div>
            </td>
        </tr>
        <tr>

            <td align="left">&nbsp;</td>
        </tr>
        <tr>
            <td bgcolor="#dedede" height="1"><img src="<?php echo ADMIN_IMAGE; ?>sp.gif"
                                                  height="1" width="1"></td>
        </tr>
        <tr>
            <td class="sublinks" height="40" valign="bottom">
                <?php
                if (isset($error)) {
                    echo '<div style="color:#FF0000">' . $errors . '</div>';
                }
                if (isset($msg) && !empty($msg)) {
                    echo $msg;
                }
                ?>
            </td>
        </tr>
        <tr>
            <td class="sublinks" height="40" valign="bottom"></td>
        </tr>
        <tr>

            <td height="1"><img src="<?php echo ADMIN_IMAGE; ?>sp.gif" height="1" width="1"></td>
        </tr>
        <tr>
            <td class="bdrTab">
                <table style="border: 1px solid rgb(184, 184, 184);" border="0"
                       cellpadding="5" cellspacing="0" width="100%">
                    <tbody>
                        <tr>
                            <td>
                                <table align="center" border="0" cellpadding="0" cellspacing="0"
                                       width="98%">

                                    <tbody>
                                        <tr>
                                            <td colspan="2" class="bdr" height="25">
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">

                                                    <tbody>
                                                        <tr>
                                                            <td width="27%">
                                                                <h2>Sales Person Details: </h2>

                                                            </td>

                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>

                                        <tr>
                                    <input type="hidden" id="spid" value="<?php echo $data['id']; ?>">
                                    <td class="marginleft" align="right" height="26"><?php echo stripslashes($data['name']); ?></td>
                                    <td class="marginleft"><div class="input text"></div></td>
                        </tr>
                        <tr>
                            <td class="marginleft" align="right" height="26"> Salesperson ID : </td>
                            <td class="marginleft"><div class="input text">
                                    <?php echo 'KAS' . $data['unique_id']; ?>
                                </div></td>
                        </tr>
                        <tr>
                            <td class="marginleft" align="right" height="26"> Email Address : </td>
                            <td class="marginleft"><div class="input text">
                                    <?php echo $data['email']; ?>
                                </div></td>
                        </tr>
                        <tr class="business">

                            <td class="marginlef" align="right" height="26" width="31%"><span
                                    class="redCol"></span>Phone Number : </td>
                            <td class="marginleft" width="69%">
                                <div class="input text">
                                    <?php echo $data['phone']; ?>
                                </div></td>
                        </tr>
                        <tr class="business">

                            <td class="marginlef" align="right" height="26" width="31%"><span
                                    class="redCol"></span>Cell Phone:( if any) </td>
                            <td class="marginleft" width="69%">
                                <div class="input text">
                                    <?php echo $data['cellphone']; ?>
                                </div></td>
                        </tr>
                        <tr class="business">

                            <td class="marginlef" align="right" height="26" width="31%"><span
                                    class="redCol"></span>Street Address : </td>
                            <td class="marginleft" width="69%">
                                <div class="input text">
                                    <?php echo $data['address'], ', ' . $data['zipcode'], ', ' . $data['countryname']; ?>
                                </div></td>
                        </tr>
                        <tr class="business">

                            <td class="marginlef" align="right" height="26" width="31%"><span
                                    class="redCol"></span>Joined On : </td>
                            <td class="marginleft" width="69%">
                                <div class="input text">
                                    <?php echo $data['modified'] == '0000-00-00 00:00:00' ? 'N/A' : date('M d, Y', strtotime($data['modified'])); ?>
                                </div></td>
                        </tr>
                        <tr class="business">

                            <td class="marginlef" align="right" height="26" width="31%"><span
                                    class="redCol"></span>commission Formula : </td>
                            <td class="marginleft" width="69%">
                                <div class="input text">
                                    <?php echo "Formula"; ?>
                                </div></td>
                        </tr>


                    </tbody>

                </table>
            </td>
        </tr>
        <tr>
            <td height="10">&nbsp;</td>
        </tr>
        <tr>
            <td bgcolor="#dedede" height="1"><img src="<?php echo ADMIN_IMAGE; ?>sp.gif"
                                                  height="1" width="1"></td>
        </tr>
        <tr>
            <td width="27%">
                <h2>Payment Details: </h2>

            </td>

        </tr>
        <tr>

            <td align="left">&nbsp;</td>
        </tr>


        <tr>
            <td class="bdrTab">
                <table style="border: 1px solid rgb(184, 184, 184);" border="0"
                       cellpadding="5" cellspacing="0" width="100%">
                    <tbody>
                        <tr>
                            <td>
                                <table  align="center" border="0" cellpadding="0" cellspacing="0"
                                        width="98%">

                                    <tbody>
                                        <tr>
                                            <td colspan="2" class="bdr" height="25">

                                            </td>
                                        </tr>
                                        <?php
                                        echo "<div id='pageData'></div>"
                                        ?>    
                                    </tbody>
                                    <tbody>

                                        <tr class="business">

                                            <td class="marginlef" align="right" height="26" width="31%"><span
                                                    class="redCol"></span>Total of pending payment : </td>
                                            <td class="marginleft" width="69%">
                                                <div class="input text">
                                                    <?php echo '$439.00'; ?>
                                                </div></td>
                                        </tr>
                                        <tr class="business">

                                            <td class="marginlef" align="right" height="26" width="31%"><span
                                                    class="redCol"></span>Point Redeemed ($) : </td>
                                            <td class="marginleft" width="69%">
                                                <div class="input text">
                                                    <?php echo '$439.00'; ?>
                                                </div></td>
                                        </tr>
                                        <tr class="business">

                                            <td class="marginlef" align="right" height="26" width="31%"><span
                                                    class="redCol"></span>Total Amount Paid ($) : </td>
                                            <td class="marginleft" width="69%">
                                                <div class="input text">
                                                    <?php echo '$439.00'; ?>
                                                </div></td>
                                        </tr>
                                        <tr class="business">

                                            <td class="marginlef" align="right" height="26" width="31%"><span
                                                    class="redCol"></span> Balance : </td>
                                            <td class="marginleft" width="69%">
                                                <div class="input text">
                                                    <?php echo '$439.00'; ?>
                                                </div></td>
                                        </tr>



                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody></table></td></tr>
    </tbody></table>
</td></tr>





</tbody>
</table>
<script type="text/javascript">
    $(document).ready(function() {
        var page = 0;
        var li = '0_no';
        var data = "&id=" + $('#spid').val();
        changePagination(page, li, 'sprecord', data);
    });
</script>