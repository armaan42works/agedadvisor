
<script type="text/javascript">
    $(document).ready(function() {
        var page = 0;
        var li = '0_no';
        var data = '&start_date=&search_type=&end_date=';
        changePagination(page, li, 'quote', data);
        $('#search_form').submit(function(e) {
            $('#search_form').validate();
            e.preventDefault();
            var data = '&start_date=' + $('#start_date').val() + '&search_type=' + $('#search_type').val() + '&end_date=' + $('#end_date').val();
            changePagination(page, li, 'quote', data);
        });

    });

    $(document).ready(function() {
        // module document downlaod
        $('.getfile').live('click', function() {
            var filename = $(this).attr('id');
            //filename = 'Document1.docx';
            $.fileDownload('<?php echo HOME_PATH_URL ?>files/' + filename + '');
        });
    });
</script>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="95%">

    <tbody>


        <tr style="background:#4b99e6;">
            <td>
                <div class="breadcrumb">
                    <ul>
                        <li><a href="<?php echo HOME_PATH_URL; ?>admin.php"><i class="fa fa-home"></i>Home </a>>>Manage Quotes</li>
                    </ul>
                </div>
            </td>
        </tr>

        <tr>
            <?php
            $search_array = array('created' => 'Created', 'admin_quote' => 'Admin Quoted');
            ?>
            <td class="sublinks" height="40" valign="bottom">
                <form id="search_form" method="post" action="">
                    <input type="text" name="start_date" id="start_date" placeholder="Start Date" value="<?php echo date('Y-m-d'); ?>">
                    <input type="text" name="end_date" id="end_date" value="<?php echo date('Y-m-d'); ?>" placeholder="End Date">
                    <select name="search_type" id="search_type">
                        <?php
                        $option = '<option value="">------ Choose Option-----</option>';
                        foreach ($search_array as $key => $value) {
                            $option .="<option value='$key'>$value</option>";
                        }
                        echo $option;
                        ?>
                    </select>
                    <input type="submit" value="Search">
                </form>
            </td>
        </tr>


    <tbody>
        <tr>
            <td>
                <table  align="center" border="0" cellpadding="0" cellspacing="0"
                        width="98%">

                    <tbody>

                        <?php
                        echo "<div id='pageData'></div>"
                        ?>    
                    </tbody>

                </table>
            </td>
        </tr>
    </tbody></table>
<table class="ds_box" cellpadding="0" cellspacing="0" id="ds_conclass" style="display: none;">
    <tr><td id="ds_calclass"></td></tr></table>

