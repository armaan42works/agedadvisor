<style>
    th,td,tr{border:0px!important; }
    .input{background-color:white;}
</style>
<!--<script src="http://localhost/Aged_Advisor/admin/ckeditor/ckeditor.js" type="text/javascript"></script>-->
<!--<script src="//cdn.ckeditor.com/4.4.4/standard/ckeditor.js"></script>-->
<!--<script src="//cdn.ckeditor.com/4.4.4/standard-all/ckeditor.js"></script>-->
<!--<script src="//tinymce.cachefly.net/4.1/tinymce.min.js"></script>-->

<?php
    global $db;

    include_once("./fckeditor/fckeditor.php");
//make filled fields

    $sql_query = "SELECT * FROM " . _prefix("home_keys");
    $res = $db->sql_query($sql_query);
    $records = $db->sql_fetchrowset($res);
    $Id = $records[0]['id'];
    $meta_title = $records[0]['title'];
    $keyword = $records[0]['keyword'];
    $description = $records[0]['description'];
    $modified = $records[0]['modified'];


//update home key

    if (isset($update) && $update == 'update') {
        $fields_array = add_homekey();
        $response = Validation::_initialize($fields_array, $_POST);
        if (isset($response['valid']) && $response['valid'] > 0) {
            $date = new DateTime();
            $timestamp = $date->format('Y-m-d H:i:s');
            $fields = array(
                'title' => trim($_POST['meta_title']),
                'keyword' => trim($_POST['keyword']),
                'description' => trim($_POST['description']),
                'modified' => $timestamp
            );
            $where = 'where id= ' . $Id;
            $update_result = $db->update(_prefix('home_keys'), $fields, $where);
            if ($update_result) {
                // Message for insert
                $msg = common_message(1, constant('UPDATE'));
                $_SESSION['msg'] = $msg;
                //ends here
                redirect_to(MAIN_PATH . "/pages.php?homekey");
            }
        } else {
            $errors = '';
            foreach ($response as $key => $message) {
                $error = true;
                $errors .= $message . "<br>";
            }
        }
    }
?>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="95%">
    <tbody>
        <tr style="background:#4b99e6;">
            <td>
                <div class="breadcrumb">
                    <ul>
                        <li><a href="<?php echo HOME_PATH_URL; ?>admin.php"><i class="fa fa-home"></i>Home </a>>>Update Home Key Pages</li>
                    </ul>
                </div>
            </td>
        </tr>
    </tbody>
</table>
<div class="col-sm-offset-2 col-sm-6 success">
    <?php if (isset($_SESSION['msg_err'])) { ?>
            <div class="errorForm" id="error" style="display: block;">
                <img align="absmiddle" src="<?php echo HOME_PATH . '/images/error.png'; ?>">&nbsp;<?php echo $_SESSION['msg_err']; ?>
            </div>
            <?php
            unset($_SESSION['msg_err']);
        }
        if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
            ?>
            <!--                <div class="successForm" id="success" style="display: block;">-->
                                <!--<img align="absmiddle" src="<?php // echo HOME_PATH . '/images/success.png';              ?>">-->
            &nbsp;<?php echo $_SESSION['msg']; ?>
            <!--                </div>-->
            <?php
            unset($_SESSION['msg']);
        }
    ?>
</div>
<div class="container" style="width: 95%; border: 1px solid rgb(204, 204, 204);">
    <div class="row" style="margin-bottom: 20px; border-bottom: 1px solid rgb(204, 204, 204);">
        <div class="col-sm-6">
            <h2 class="">Update Home Key Page</h2>
        </div>

        <div class="col-sm-6">
            <h2 style="float:right;" class="redCol small">* fields required</h2>
        </div>
    </div>
    <p class="clearfix"></p>
    <form name="addPage" id="addPage"  enctype="multipart/form-data"  method="POST" action=""  class="form-horizontal" role="form">
        <div class="form-group">
            <label for="meta_title" class="col-sm-2 control-label">Meta Title<span class="redCol">* </span></label>
            <div class="col-sm-8">
                <input type="text"  class="required form-control" name="meta_title" minlength="2" maxlength="50" id="meta_title"  value="<?php echo isset($meta_title) ? stripslashes($meta_title) : ''; ?>"/>
            </div>
        </div>
        <div class="form-group">
            <label for="keyword" class="col-sm-2 control-label">Meta Keyword<span class="redCol">* </span></label>
            <div class="col-sm-8">
                <input type="text"  class="required form-control" name="keyword" id="keyword"  maxlength="50" value="<?php echo isset($keyword) ? stripslashes($keyword) : ''; ?>"/>
            </div>
        </div>
        <div class="form-group">
            <label for="description" class="col-sm-2 control-label">Meta Description<span class="redCol">* </span></label>
            <div class="col-sm-8">
                <textarea  rows="4" cols="50" maxlength="500" class="required form-control" minlength="4" name="description" id="description"  ><?php echo isset($description) ? stripslashes($description) : ''; ?></textarea>
            </div>
        </div>
        <div class="form-group">
            <label for="description" class="col-sm-2 control-label">Last Modified:&nbsp;</label>
            <div class="col-sm-8" style="margin-top: 6px;font-family: sans-serif; font-size: 15px;">
                <?php echo isset($modified) ? date('M d, Y', strtotime($modified)) : 'N/A'; ?>
            </div>
        </div>


        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-8">
                <input type="submit" value="update" name="update" class="submit_btn btn" src="<?php echo ADMIN_IMAGE; ?>submit.gif">
            </div>
        </div>
    </form>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('#addPage').validate();
    });




</script>








