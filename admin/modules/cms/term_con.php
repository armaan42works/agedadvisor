<style>
    th,td,tr{border:0px!important; }
    .input{background-color:white;}
</style>

<?php
global $db;
include_once("./fckeditor/fckeditor.php");
$description = '';
if (isset($submit) && $submit == 'Save' && $id == '') {

    //$response = Validation::_initialize($fields_array, $_POST);
    if ($id == '') {
        $tmp = strip_tags($_POST["content"], '&nbsp;');
        $_POST['content'] = trim(trim($tmp, '&nbsp;')) == '' ? '' : $_POST["content"];
        $fields = array(
            'title' => trim($_POST['title']),
            'content' => trim($_POST['content']),
            'created' => date('Y-m-d h:i:s', time()),
            'created_by' => $_SESSION['id']
        );
        // prd($fields);

        $insert_result = $db->insert(_prefix('about_us'), $fields);
        if ($insert_result) {
            // Message for insert
            $msg = common_message(1, constant('INSERT'));
            $_SESSION['msg'] = $msg;
            //ends here
            redirect_to(MAIN_PATH . "cms.php?term_con");
        }
    } else {
        $errors = '';
        foreach ($response as $key => $message) {
            $error = true;
            $errors .= $message . "<br>";
        }
    }
}
$sql_query = "SELECT * FROM " . _prefix("about_us") . " where id=3";
$res = $db->sql_query($sql_query);
$records = $db->sql_fetchrowset($res);
if (count($records)) {
    foreach ($records as $record) {
        $id = $record['id'];
        $title = $record['title'];
        $content = $record['content'];
    }
}
if (isset($submit) && $submit == 'Save' && $id == '3') {

    $title = $_POST['title'];
    $content = $_POST['content'];
    $fields = array(
        'title' => trim($_POST['title']),
        'content' => trim(trim($content)),
        'modified ' => date('Y-m-d h:i:s', time()),
        'modified_by' => $_SESSION['id']
    );
    // prd($fields);
    $where = "where id=3";
    $update_result = $db->update(_prefix('about_us'), $fields, $where);
    if ($update_result) {
        // Message for insert
        $msg = common_message(1, constant('UPDATE'));
        $_SESSION['msg'] = $msg;
        //ends here
        redirect_to(MAIN_PATH . "cms.php?term_con");
    }
}
?>
<div class="row sublinks" style="margin-bottom: 20px; border-bottom: 1px solid rgb(204, 204, 204);">
    <div class="col-sm-offset-2 col-sm-6 success">
        <?php if (isset($error)) { ?>
            <div class="errorForm" id="error" style="display: block;">
                <img align="absmiddle" src="<?php echo HOME_PATH . '/images/error.png'; ?>">&nbsp;<?php echo $errors; ?>.<br>
            </div>
            <?php
        }
        if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
            echo $_SESSION['msg'];
            unset($_SESSION['msg']);
        }
        ?>
    </div>
</div>

<table align="center" border="0" cellpadding="0" cellspacing="0" width="95%">
    <tbody>
        <tr style="background:#4b99e6;">
            <td>
                <div class="breadcrumb">
                    <ul>
                        <li><a href="<?php echo HOME_PATH_URL; ?>admin.php"><i class="fa fa-home"></i>Home </a>>><a href="<?php echo HOME_PATH_URL . "cms.php?term_con" ?>">Supplier T&C</a>>> Edit</li>
                    </ul>
                </div>
        </tr>
    </tbody>
</table>


<div class="container" style="width: 95%; border: 1px solid rgb(204, 204, 204);">
    <div class="row" style="margin-bottom: 20px; border-bottom: 1px solid rgb(204, 204, 204);">
        <div class="col-sm-6">
            <h2 class="">Supplier T&C</h2>
        </div>

        <div class="col-sm-6">
            <h2 style="float:right;" class="redCol small">* fields required</h2>
        </div>
    </div>
    <p class="clearfix"></p>
    <form name="form1" id="addblogs" method="POST" action=""  class="form-horizontal" role="form">
        <div class="form-group">
            <label for="title" class="col-sm-2 control-label"> Title<span class="redCol">* </span></label>
            <div class="col-sm-8">
                <input type='hidden' name='id' value='<?php echo isset($id) ? stripslashes($id) : ''; ?>'>
                <input type="text"  class="required form-control" name="title" maxlength="50" id="title"  value="<?php echo isset($title) ? stripslashes($title) : ''; ?>"/>
            </div>
        </div>
        <?php
        if (isset($_POST['content'])) {

            $content = $_POST['content'];
        } elseif (isset($content)) {
            $content = $content;
        } else {
            'empty';
            $content = '';
        }
        ?>
        <div class="form-group">
            <label for="content" class="col-sm-2 control-label">Content<span class="redCol">* </span></label>
            <div class="col-sm-8">
                <?php
                $oFCKeditor = new FCKeditor('content');
                $oFCKeditor->BasePath = './fckeditor/';
                $oFCKeditor->Height = '400px';
                $oFCKeditor->Value = "$content";
                $oFCKeditor->class = "form-control";
                $oFCKeditor->Create();
                ?>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-8">
                <input type="submit" value="Save" name="submit" class="submit_btn btn">
            </div>
        </div>
    </form>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('input[type="submit"]').focus();
    });
    $('#addblogs').validate();
</script>

