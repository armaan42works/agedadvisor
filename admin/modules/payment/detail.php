<?php
    /*
     * Objective : Detail activity of the Salesperson related project and payment
     * Fimename : detail.php
     * Created By : Sanket Khanna <sanket.khanna@ilmp-tech.com>
     * Created On : 21 August 2014
     */
?>
<script type="text/javascript">
    $(document).ready(function() {
        var page = 0;
        var li = '0_no';
        var data = '&start_date=""&search_type=""&end_date=""&id=<?php echo $id; ?>';
        changePagination(page, li, 'detail', data);
        $('#search_form').submit(function(e) {
            $('#search_form').validate();
            e.preventDefault();
            var data = '&start_date=' + $('#start_date').val() + '&search_type=' + $('#search_type').val() + '&end_date=' + $('#end_date').val() + '&id=<?php echo $id; ?>';
            changePagination(page, li, 'detail', data);
        });
    });
</script>     
<table align="center" border="0" cellpadding="0" cellspacing="0" width="95%">

    <tbody>

        <tr style="background:#4b99e6;">
            <td>
                <div class="breadcrumb">
                    <ul>
                        <li><a href="<?php echo HOME_PATH_URL; ?>admin.php"><i class="fa fa-home"></i>Home </a>>><a href="<?php echo HOME_PATH_URL . "payment.php?manage" ?>">Salesperson Performance Record</a>>>Detail</li>
                    </ul>
                </div>
            </td>
        </tr>

    <tbody>
        <tr>
            <td>
                <table align="center" border="0" cellpadding="0" cellspacing="0"
                       width="100%">

                    <tbody>

                    <tbody>
                        <tr>
                            <td colspan="2" class="bdr" height="25">

                                <h2>Sales Person Profile</h2>

                            </td>
                        </tr>

                    </tbody>
                    <?php
                        $sql_query = "SELECT du.*,dc.name as country_name, ds.name as state_name "
                                . "FROM " . _prefix("users") . " as du "
                                . "LEFT JOIN " . _prefix("countries") . " as dc ON dc.id = du.country_id "
                                . "LEFT JOIN " . _prefix("states") . " as ds ON ds.id = du.state_id "
                                . "  WHERE md5(du.id) = '$id'";
                        $res = $db->sql_query($sql_query);
                        $data = $db->sql_fetchrow($res);
                    ?>

                    <tr>
                        <td class="marginleft" align="right" height="26"><?php echo $data['name']; ?></td>

                    </tr>
                    <tr>
                        <td class="marginleft" align="right" height="26">Salesperson ID : <?php echo 'KAS' . $data['unique_id']; ?></td>

                    </tr>
                    <tr>
                        <td class="marginleft" align="right" height="26">Email Address : <?php echo $data['email']; ?></td>

                    </tr>
                    <tr>
                        <td class="marginleft" align="right" height="26">Phone Number : <?php echo $data['phone']; ?></td>

                    </tr>
                    <tr>
                        <td class="marginleft" align="right" height="26">Cell Phone Number (if any) : <?php
                                echo $data['cellphone'] == '' ? 'N/A' : $data['cellphone'];
                                ;
                            ?></td>

                    </tr>
                    <tr>
                        <td class="marginleft" align="right" height="26">Street Address : <?php echo $data['address'] . ' ' . $data['zipcode'] . ', ' . $data['state_name'] . ', ' . $data['country_name']; ?></td>

                    </tr>
                    <tr>
                        <td class="marginleft" align="right" height="26">Joined On : <?php echo 'N/A'; ?></td>

                    </tr>
                    <tr>
                        <td class="marginleft" align="right" height="26">Bank Information : <?php echo 'N/A'; ?></td>

                    </tr>
                    <tr>
                        <td class="marginleft" align="right" height="26">Minimum to meet to get paid : <?php echo 'N/A'; ?></td>

                    </tr>
                    <tr>
                        <td class="marginleft" align="right" height="26">Commission formula : <?php echo 'N/A'; ?></td>

                    </tr>

    </tbody>

</table>
</td>
</tr>
</tbody></tbody></table>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="95%" style="margin-bottom: 2%;margin-top: 2%">
    <tbody>
        <tr>
            <td colspan="2" class="bdr" height="25">

                <h2>Projects related to salesperson</h2>

            </td>
        </tr>

    </tbody>
    <tbody>
    <div class="sublinks" height="40" valign="bottom" style="margin-bottom: 10px;">
        <?php
            if (isset($error)) {
                echo '<div style="color:#FF0000">' . $errors . '</div>';
            }
            if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
                echo $_SESSION['msg'];
                unset($_SESSION['msg']);
            }
        ?>
    </div>

    <tr>
        <!----------------------------- End here --------------------------->
        <?php
            $search_array = array('created' => 'Added On', 'paid' => 'Paid On');
        ?>
        <td class="sublinks" height="40" valign="bottom">
            <form id="search_form" method="post" action="">
                <input type="text" name="start_date" id="start_date" value="<?php echo date('Y-m-d'); ?>" placeholder="Start Date">
                <input type="text" name="end_date" id="end_date" value="<?php echo date('Y-m-d'); ?>" placeholder="End Date">
                <select name="search_type" id="search_type">
                    <?php
                        $option = '<option value="">------ Choose Option-----</option>';
                        foreach ($search_array as $key => $value) {
                            $option .="<option value='$key'>$value</option>";
                        }
                        echo $option;
                    ?>
                </select>
                <input type="submit" value="Search">
            </form>
        </td>
    </tr>


    <tbody>
        <tr>
            <td>
                <table align="center" border="0" cellpadding="0" cellspacing="0"
                       width="100%">

                    <tbody>

                        <?php echo '<div id="pageData"></div>'; ?>    
                    </tbody>

                </table>
            </td>
        </tr>
    </tbody></tbody></table>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="95%" style="margin-bottom: 20%;">

    <tbody>

<?php
    $sql_query = "SELECT sum(earnings) as total FROM  " . _prefix("sp_performance") . " WHERE md5(sp_id) = '$id' && payment_status = 0";
    $res = $db->sql_query($sql_query);
    $total = $db->sql_fetchrow($res);
    $total = ($total['total'] == '') ? '0' : $total['total'];
    $sql_amount = "SELECT sum(earnings) as total FROM " . _prefix("sp_performance") . " WHERE md5(sp_id) = '$id' && payment_status = 1";
    $res_amount = $db->sql_query($sql_amount);
    $paid = $db->sql_fetchrow($res_amount);
    $paid = ($paid['total'] == '') ? '0' : $paid['total'];
    $sql_maxamount = "SELECT sum(earnings) as total FROM " . _prefix("sp_performance") . " WHERE md5(sp_id) = '$id'";
    $res_maxamount = $db->sql_query($sql_maxamount);
    $maxamount = $db->sql_fetchrow($res_maxamount);
    $maxamount = ($maxamount['total'] == '') ? '0' : $maxamount['total'];
    $bal = $maxamount - $paid;
    ?>

    <tbody>
        <tr>
            <td>
                <table align="center" border="0" cellpadding="0" cellspacing="0"
                       width="100%">

                    <tbody>

                        <tr>
                            <td class="marginleft" align="right" height="26">Total Pending Amount : <?php echo '$'.$total; ?></td>

                        </tr>
                        <tr>
                            <td class="marginleft" align="right" height="26">Point Redeemed ($) : <?php echo 'N/A'; ?></td>

                        </tr>
                        <tr>
                            <td class="marginleft" align="right" height="26">Total Amount Paid ($) : <?php echo '$'.$paid; ?></td>

                        </tr>
                        <tr>
                            <td class="marginleft" align="right" height="26">Balance : <?php echo '$'.$bal; ?></td>

                        </tr>
                    </tbody>

                </table>
            </td>
        </tr>
    </tbody></tbody></table>
<table class="ds_box" cellpadding="0" cellspacing="0" id="ds_conclass" style="display: none;">
    <tr><td id="ds_calclass"></td></tr></table>


