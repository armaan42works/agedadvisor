<!-----------------
Objective : Listing of all the Job application on Admin Panel
Filename : application.php
Created By: Sanket Khanna <sanket.khanna@ilmp-tech.com>
Created On: 19th August 2014
------------------->
<script type="text/javascript">
    $(document).ready(function() {
        var page = 0;
        var li = '0_no';
        changePagination(page, li, 'application');
    });
</script> 
<table align="center" border="0" cellpadding="0" cellspacing="0" width="95%">

    <tbody>
        <?php
            global $db;
            if (isset($submit) && $submit == 'submit') {
               $send =sendmail($email, $subject, nl2br($message));
                if ($send) {
                    $sql_query = "UPDATE " . _prefix("job_application") . " SET status = '$jobAction', modified = '" . date('Y-m-d h:i:s') . "' WHERE md5(id) = '$id'";
                    $db->sql_query($sql_query);
                    if ($jobAction == 2) {
                        $_SESSION['msg'] = common_message(1, constant('JOB_APPROVED'));
                    } else {
                        $_SESSION['msg'] = common_message(1, constant('JOB_DENIED'));
                    }
                }
            }
            if (isset($Send_Mail) && $Send_Mail == 'Send Mail') {
                $verify = VerifyUser($id);
                if ($verify) {
                    $_SESSION['msg'] = common_message(1, constant('ACCOUNT_VERIFY'));
                } else {
                    $_SESSION['msg'] = common_message(1, constant('ACCOUNT_NOT_VERIFY'));
                }
            }
        ?>

        <tr style="background:#4b99e6;">
            <td>
                <div class="breadcrumb">
                    <ul>
                        <li><a href="<?php echo HOME_PATH_URL; ?>admin.php"><i class="fa fa-home"></i>Home </a>>>Manage jobs Application</li>
                    </ul>
                </div>
            </td>
        </tr>
    <div class="sublinks" height="40" valign="bottom" style="margin-bottom: 10px;">
        <?php
            if (isset($error)) {
                echo '<div style="color:#FF0000">' . $errors . '</div>';
            }
            if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
                echo $_SESSION['msg'];
                unset($_SESSION['msg']);
            }
        ?>
    </div>  


    
                <tbody>
                    <tr>
                        <td>
                            <table  align="center" border="0" cellpadding="0" cellspacing="0"
                                    width="100%">

                                <tbody>


                                    <?php
                                        echo '<div id="pageData"></div>';
                                    ?>    
                                </tbody>

                            </table>
                        </td>
                    </tr>
                </tbody></tbody></table>
<table class="ds_box" cellpadding="0" cellspacing="0" id="ds_conclass" style="display: none;">
    <tr><td id="ds_calclass"></td></tr></table>

