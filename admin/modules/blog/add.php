<style>
    th,td,tr{border:0px!important; }
    .input{background-color:white;}
</style>

<?php
/*****************************
*page: add.php of blog		*
*							*
*task: for uploading image 	*
*							*
*	updated on: 29-1-16		*
*							*
*							*
*****************************/
error_reporting(E_ALL);
ini_set("display_errors", 1);
global $db;
include_once("./fckeditor/fckeditor.php");
$description = '';




if (isset($submit) && $submit == 'Submit') {
		
		if(isset($_FILES['blog_image']['name']))
		{ 
			$uploaddir ='../admin/files/pages/image/';
			$uploadfile = $uploaddir . basename($_FILES['blog_image']['name']);
			$move=  @move_uploaded_file($_FILES['blog_image']['tmp_name'], $uploadfile);
		    
			
		}
    $fields_array = blog();
    $response = Validation::_initialize($fields_array, $_POST);
    if (isset($response['valid']) && $response['valid'] > 0) {

        $getMax = "SELECT max(order_by) as count from " . _prefix("blogs") . " WHERE status = 1 && deleted = 0 && type = 0";
        $resMax = $db->sql_query($getMax);
        $dataMax = mysqli_fetch_assoc($resMax);
        $order = ($dataMax['count'] == NULL) ? 1 : $dataMax['count'] + 1;
        $tmp = strip_tags($_POST["content"], '&nbsp;');
        $_POST['content'] = trim(trim($tmp, '&nbsp;')) == '' ? '' : $_POST["content"];
        $fields = array(
            'title' => trim($_POST['title']),
            'content' => trim($_POST['content']),
			'img_name'=>$_FILES['blog_image']['name'],
            'type' => 0,
            'created' => date('Y-m-d h:i:s', time()),
            'created_by' => $_SESSION['Aid']
        );
        $insert_result = $db->insert(_prefix('blogs'), $fields);
        if ($insert_result) {
            // Message for insert
            $msg = common_message(1, constant('INSERT'));
            $_SESSION['msg'] = $msg;
            //ends here
            redirect_to(MAIN_PATH . "/blog.php?manage");
        }
    } else {
        $errors = '';
        foreach ($response as $key => $message) {
            $error = true;
            $errors .= $message . "<br>";
        }
    }
}
if (key($_REQUEST) == 'edit') {
    $id = $_GET['id'];
    $sql_query = "SELECT * FROM " . _prefix("blogs") . " where md5(id)='$id'";
    $res = $db->sql_query($sql_query);
    $records = $db->sql_fetchrowset($res);
    if (count($records)) {
        foreach ($records as $record) {

            $title = $record['title'];
            $content = $record['content'];
        }
    }
}
if (isset($update) && $update == 'Update') {

    $fields_array = blog();
    $response = Validation::_initialize($fields_array, $_POST);
    if (isset($response['valid']) && $response['valid'] > 0) {
        $title = $_POST['title'];
        $content = $_POST['content'];
        $fields = array(
            'title' => trim($_POST['title']),
            'content' => trim(trim($content)),
			'img_name'=>$_FILES['blog_image']['name'],
        );

        $where = "where md5(id)= '$id'";
        $update_result = $db->update(_prefix('blogs'), $fields, $where);
        if ($update_result) {
            // Message for insert
            $msg = common_message(1, constant('UPDATE'));
            $_SESSION['msg'] = $msg;
            //ends here
            redirect_to(MAIN_PATH . "/blog.php?manage");
        }
    } else {
        $errors = '';
        foreach ($response as $key => $message) {
            $error = true;
            $errors .= $message . "<br>";
        }
    }
}
?>
<div style="margin-top: 10px;">

    <table align="center" border="0" cellpadding="0" cellspacing="0" width="95%">
        <tbody>
            <tr style="background:#4b99e6;">
                <td>
                    <div class="breadcrumb">
                        <ul>
                            <li><a href="<?php echo HOME_PATH_URL; ?>admin.php"><i class="fa fa-home"></i>Home </a>>><a href="<?php echo HOME_PATH_URL . "blog.php?manage" ?>">Manage Blog</a>>> <?php echo (key($_REQUEST) == 'edit') ? 'Edit' : 'Add'; ?></li>
                        </ul>
                    </div>
            </tr>
        </tbody>
    </table>
    <div class="row">
        <div class="col-sm-offset-2 col-sm-6 success">
            <?php if (isset($error)) { ?>
                <div class="errorForm" id="error" style="display: block;">
                    <img align="absmiddle" src="<?php echo HOME_PATH . 'admin/images/error.png'; ?>">&nbsp;<?php echo $errors; ?>.<br>
                </div>
                <?php
            }
            if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
                ?>
                <div class="errorForm" id="error" style="display: block;">
                    <img align="absmiddle" src="<?php echo HOME_PATH . 'admin/images/error.png'; ?>">&nbsp;<?php echo $_SESSION['msg']; ?>.<br>
                </div>
                <?php
                unset($_SESSION['msg']);
            }
            ?>
        </div>
    </div>

    <div class="container" style="width: 95%; border: 1px solid rgb(204, 204, 204);">
        <div class="row" style="margin-bottom: 20px; border-bottom: 1px solid rgb(204, 204, 204);">
            <div class="col-sm-6">
                <h2 class=""><?php echo (key($_REQUEST) == 'edit') ? 'Edit' : 'Add'; ?> Blog</h2>
            </div>

            <div class="col-sm-6">
                <h2 style="float:right;" class="redCol small">* fields required</h2>
            </div>
        </div>
        <p class="clearfix"></p>
        <form name="form1" id="addblogs" method="POST" action=""  class="form-horizontal" role="form" enctype="multipart/form-data">
            <div class="form-group">
                <label for="title" class="col-sm-2 control-label">Blog Title<span class="redCol">* </span></label>
                <div class="col-sm-8">
                    <input type="text"  class="required form-control" name="title" minlength='2' maxlength="50" id="title"  value="<?php echo isset($title) ? stripslashes($title) : ''; ?>"/>
                </div>
            </div>
            <?php
            if (isset($_POST['content'])) {

                $content = $_POST['content'];
            } elseif (isset($content)) {
                $content = $content;
            } else {
                'empty';
                $content = '';
            }
            ?>
            <div class="form-group">
                <label for="content" class="col-sm-2 control-label">Content<span class="redCol">* </span></label>
                <div class="col-sm-8">
                    <?php
                    $oFCKeditor = new FCKeditor('content');
                    $oFCKeditor->BasePath = './fckeditor/';
                    $oFCKeditor->Height = '400px';
                    $oFCKeditor->Value = "$content";
                    $oFCKeditor->class = "form-control";
                    $oFCKeditor->Create();
                    ?>
                </div>
            </div>
            <!-------------------------code for the select image from browse button------------------------------------->
            <div class="form-group">
                <label for="title" class="col-sm-2 control-label">Select Image<span class="redCol">* </span></label>
                <div class="col-sm-8">
                    <input type="file"  class="required form-control" name="blog_image"  id="image"  value="<?php echo isset($title) ? stripslashes($title) : ''; ?>"/>
                </div>
            </div>
            
            <!----------------------end code for the select image from browse button------------------------------------>
            
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-8">
                    <?php
                    if (key($_REQUEST) == 'edit') {
                        ?>
                        <input type="submit" value="Update" name="update" class="submit_btn btn" src="<?php echo ADMIN_IMAGE; ?>submit.gif">

                        <?php
                    } else {
                        ?>
                        <input type="submit" value="Submit" name="submit" class="submit_btn btn">
                        <?php
                    }
                    ?>
                </div>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('input[type="submit"]').focus();
    });
    $('#addblogs').validate();
</script>

