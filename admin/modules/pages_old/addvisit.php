<style>
    th,td,tr{border:0px!important; }
    .input{background-color:white;}
</style>
<?php
global $db;

if (key($_REQUEST) == 'editVisit') {
    $id = $_GET['id'];
    $sql_query = "SELECT * FROM " . _prefix("know_me") . " where md5(id)='$id'";
    $res = $db->sql_query($sql_query);
    $records = $db->sql_fetchrowset($res);
    // prd($records);
    if (count($records)) {
        foreach ($records as $record) {
            $title = $record['title'];
            $date = $record['created'];
        }
    }
}

if (isset($submit) && $submit == 'Submit') {

    $fields_array = durations();
    $response = Validation::_initialize($fields_array, $_POST);
    if (isset($response['valid']) && $response['valid'] > 0) {
        $date1 = str_replace("/", "-", $_POST['date']);
        $created_date = date('Y-m-d', strtotime($date1));
        $getMax = "SELECT max(order_by) as count from " . _prefix("know_me") . " WHERE status = 1 && deleted = 0";
        $resMax = $db->sql_query($getMax);
        $dataMax = mysqli_fetch_assoc($resMax);
        $order = ($dataMax['count'] == NULL) ? 1 : $dataMax['count'] + 1;

        $fields = array(
            'title' => trim($title),
            'created' => trim($created_date),
            'order_by' => $order
        );
        $insert_result = $db->insert(_prefix('know_me'), $fields);

        if ($insert_result) {
            // Message for insert
            $msg = common_message(1, constant('INSERT'));
            $_SESSION['msg'] = $msg;
            //ends here
            redirect_to(MAIN_PATH . "/pages.php?manageVisit");
        }
    } else {
        $errors = '';
        foreach ($response as $key => $message) {
            $error = true;
            $errors .= $message . "<br>";
        }
    }
}

if (isset($update) && $update == 'Update') {

    $fields_array = durations();
    $response = Validation::_initialize($fields_array, $_POST);
    if (isset($response['valid']) && $response['valid'] > 0) {
        $date1 = str_replace("/", "-", $_POST['date']);
        $created_date = date('Y-m-d', strtotime($date1));

        $fields = array(
            'title' => trim($_POST['title']),
            'created' => trim($created_date),
        );
        $where = "where md5(id)= '$id'";
        $update_result = $db->update(_prefix('know_me'), $fields, $where);

        if ($update_result) {
            // Message for insert
            $msg = common_message(1, constant('UPDATE'));
            $_SESSION['msg'] = $msg;
            //ends here
            redirect_to(MAIN_PATH . "/pages.php?manageVisit");
        }
    } else {
        $errors = '';
        foreach ($response as $key => $message) {
            $error = true;
            $errors .= $message . "<br>";
        }
    }
}
?>
<div class="row" style="margin-bottom: 20px; border-bottom: 1px solid rgb(204, 204, 204);">
    <div class="col-sm-offset-2 col-sm-6 success">
        <?php if (isset($error)) { ?>
            <div class="errorForm" id="error" style="display: block;">
                <img align="absmiddle" src="<?php echo HOME_PATH . 'images/error.png'; ?>">&nbsp;<?php echo $errors; ?>.<br>
            </div>
            <!--echo '<div style="color:#FF0000">' . $errors . '</div>';-->
            <?php
        }
        if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
            ?>
            <div class="errorForm" id="error" style="display: block;">
                <img align="absmiddle" src="<?php echo HOME_PATH . 'images/error.png'; ?>">&nbsp;<?php echo $_SESSION['msg']; ?>.<br>
            </div>
            <?php
            unset($_SESSION['msg']);
        }
        ?>
    </div>
</div>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="95%">
    <tbody>
        <tr style="background:#4b99e6;">
            <td>
                <div class="breadcrumb">
                    <ul>
                        <li><a href="<?php echo HOME_PATH_URL; ?>admin.php"><i class="fa fa-home"></i>Home </a>>><a href="<?php echo HOME_PATH_URL . "pages.php?manageVisit" ?>">Manage Visit</a>>><?php echo (key($_REQUEST) == 'editVisit') ? 'Edit' : 'Add'; ?></li>
                    </ul>
                </div>
            </td>
        </tr>
    </tbody>
</table>

<div class="container" style="width: 95%; border: 1px solid rgb(204, 204, 204);">
    <div class="row" style="margin-bottom: 20px; border-bottom: 1px solid rgb(204, 204, 204);">
        <div class="col-sm-6">
            <h2 class=""><?php echo (key($_REQUEST) == 'editVisit') ? 'Edit' : 'Add'; ?> Visit Title</h2>
        </div>

        <div class="col-sm-6">
            <h2 style="float:right;" class="redCol small">* fields required</h2>
        </div>
    </div>
    <p class="clearfix"><?php
        if (isset($error)) {
//                echo '<div style="color:#FF0000">' . $errors . '</div>';
        }
        ?></p>
    <p class="clearfix"></p>
    <form name="addPage" id="addPage"  enctype="multipart/form-data"  method="POST" action=""  class="form-horizontal" role="form">
        <div class="form-group">
            <label for="title" class="col-sm-2 control-label">Title<span class="redCol">* </span></label>
            <div class="col-sm-8">
                <input type="text"  class="required form-control" name="title" id="title" minlength="2" maxlength="50" value="<?php echo isset($title) ? stripslashes($title) : ''; ?>"/>
            </div>
        </div>
        <div class="form-group">
            <label for="date" class="col-sm-2 control-label">Date<span class="redCol">* </span></label>
            <div class="col-sm-8">
                <input type="text"  class="required form-control" name="date" id="created_date"  value="<?php echo isset($date) ? stripslashes($date) : ''; ?>"/>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-8">
                <?php
                if (key($_REQUEST) == 'editVisit') {
                    ?>
                    <input type="submit" value="Update" name="update" class="submit_btn btn" src="<?php echo ADMIN_IMAGE; ?>submit.gif">

                    <?php
                } else {
                    ?>
                    <input type="submit" value="Submit" name="submit" class="submit_btn btn">
                    <?php
                }
                ?>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#created_date').datepicker({
            dateFormat: 'dd/mm/yy',
            changeMonth: true,
            changeYear: true,
            yearRange: '2010',
            minDate: '0'
        });

        $('input[type="submit"]').focus();

        $('#addPage').validate({
        });

    });

</script>

