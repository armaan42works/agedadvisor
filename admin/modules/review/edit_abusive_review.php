<style>
    th,td,tr{border:0px!important; }
    .input{background-color:white;}
</style>
<?php
    global $db;

    include_once("./fckeditor/fckeditor.php");


    $imageName = '';
    $image_path = '';
    $banner_image = '';
    $bannerName = '';
//    if (key($_REQUEST) == 'edit') {
    $id = $_GET['id'];
    $sql_query = "SELECT * FROM " . _prefix("feedbacks") . " where md5(id)='$id'";
    $res = $db->sql_query($sql_query);
    $records = $db->sql_fetchrowset($res);
    if (count($records)) {

        foreach ($records as $record) {
            $feedback = $record['feedback'];
            $pros = $record['pros'];
            $cons = $record['cons'];
            $feedback_reply = $record['feedback_reply'];
            $title = $record['title'];
            $yes_no_planto = $record['yes_no_planto'];
            $who_did_you_tell = $record['who_did_you_tell'];
            $abuse_reason = $record['abuse_reason'];
            $what_response = $record['what_response'];
            $unedited_feedback  = $record['unedited_feedback'];
        }
    }
    
    
    if(!$unedited_feedback){
	    
	    $unedited_feedback = $title."<hr>".$pros."<hr>".$cons."<hr>".$feedback."<hr>";	    
        $fields = array(
            'unedited_feedback' => trim($unedited_feedback)
        );
            

        $where = "where md5(id)= '$id'";
        $update_result = $db->update(_prefix('feedbacks'), $fields, $where);
	    
	    
	    
    }
    
//    }
//    if (isset($submit) && $submit == 'Submit') {
//
//        $fields_array = add_review();
//        $response = Validation::_initialize($fields_array, $_POST);
//        if (isset($response['valid']) && $response['valid'] > 0) {
//
//
//            if (isset($_FILES['banner_image']['name'])) {
//                if ($_FILES['banner_image']['error'] == 0) {
//                    $ext = end(explode('.', $_FILES['banner_image']['name']));
//                    $target_pathL = '../admin/files/pages/banner/';
//                    $date = new DateTime();
//                    $timestamp = $date->getTimestamp();
//                    $bannerName = $timestamp . '_' . imgNameSanitize(basename($_FILES['banner_image']['name']), 20);
//                    $target_path = $target_pathL . $bannerName;
//                    $target_path_thumb = $target_pathL . 'thumb_' . $bannerName;
//                    move_uploaded_file($_FILES['banner_image']["tmp_name"], $target_path);
//                    ak_img_resize($target_path, $target_path_thumb, 100, 100, $ext);
//                    $_POST['banner_image'] = $bannerName;
//                }
//            }
//
//            $content = $_POST["content"];
//
//            $fields = array(
//                'title' => trim($title),
//                'url' => trim($url),
//                'meta_title' => trim($meta_title),
//                'keyword' => trim($keyword),
//                'description' => trim($description),
//                'short_description' => trim($short_description),
//                'content' => trim($content),
//                'banner_image' => trim($_POST['banner_image']),
//            );
//
//            $insert_result = $db->insert(_prefix('reviews'), $fields);
//
//
//            if ($insert_result) {
//                // Message for insert
//                $msg = common_message(1, constant('INSERT'));
//                $_SESSION['msg'] = $msg;
//                //ends here
//                redirect_to(MAIN_PATH . "/review.php?review");
//            }
//        } else {
//            $errors = '';
//            foreach ($response as $key => $message) {
//                $error = true;
//                $errors .= $message . "<br>";
//            }
//        }
//    }

    if (isset($update) && $update == 'Update') {

        $fields = array(
            'feedback' => trim($_POST['feedback']),
            'pros' => trim($_POST['pros']),
            'cons' => trim($_POST['cons']),
            'feedback_reply' => trim($_POST['feedback_reply']),
            'title' => trim($_POST['title']),
            'abused' => 1
                    );
            

        $where = "where md5(id)= '$id'";
        $update_result = $db->update(_prefix('feedbacks'), $fields, $where);
//prd('vinod');

        if ($update_result) {
            // Message for insert
            $msg = common_message(1, constant('UPDATE'));
            $_SESSION['msg'] = $msg;
            //ends here
            redirect_to(MAIN_PATH . "/review.php?abused_review");
        }
    }
?>

<div class="row" style="margin-bottom: 20px; border-bottom: 1px solid rgb(204, 204, 204);">
    <div class="col-sm-offset-2 col-sm-6 success">
        <?php if (isset($error)) { ?>
                <div class="errorForm" id="error" style="display: block;">
                    <img align="absmiddle" src="<?php echo HOME_PATH . 'images/error.png'; ?>">&nbsp;<?php echo $errors; ?>.<br>
                </div>
                <!--echo '<div style="color:#FF0000">' . $errors . '</div>';-->
                <?php
            }
            if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
                ?>
                <div class="errorForm" id="error" style="display: block;">
                    <img align="absmiddle" src="<?php echo HOME_PATH . 'images/error.png'; ?>">&nbsp;<?php echo $_SESSION['msg']; ?>.<br>
                </div>
                <?php
                unset($_SESSION['msg']);
            }
        ?>
    </div>
</div>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="95%">
    <tbody>
        <tr style="background:#4b99e6;">
            <td>
                <div class="breadcrumb">
                    <ul>
                        <li><a href="<?php echo HOME_PATH_URL; ?>admin.php"><i class="fa fa-home"></i>Home </a>>><a href="<?php echo HOME_PATH_URL . "review.php?manage" ?>">Manage Abused Review</a>>> Edit</li>
                    </ul>
                </div>
            </td>
        </tr>
    </tbody>
</table>



<div class="container" style="width: 95%; border: 1px solid rgb(204, 204, 204);">
    <div class="row" style="margin-bottom: 20px; border-bottom: 1px solid rgb(204, 204, 204);">
        <div class="col-sm-6">
            <h2 class="">Edit Review</h2>
        </div>

        <div class="col-sm-6">
            <h2 style="float:right;" class="redCol small">* fields required</h2>
        </div>
    </div>
    <p class="clearfix"><?php
            if (isset($error)) {
                echo '<div style="color:#FF0000">' . $errors . '</div>';
            }
        ?></p>
    <p class="clearfix"></p>
    <form name="addReview" id="addReview" enctype="multipart/form-data" method="POST" action=""  class="form-horizontal" role="form">

        <div class="form-group">
            <label for="description" class="col-sm-2 control-label">Abuse Reason</label>
            <div class="col-sm-8">
                <?php echo isset($abuse_reason) ? stripslashes($abuse_reason) : ''; ?>
            </div>
        </div>

        <div class="form-group">
            <label for="description" class="col-sm-2 control-label">Title<span class="redCol">* </span></label>
            <div class="col-sm-8">
                <textarea  rows="4" cols="50" maxlength="500" class="required form-control" name="title" id="description"  ><?php echo isset($title) ? stripslashes($title) : ''; ?></textarea>
            </div>
        </div>

        <div class="form-group">
            <label for="description" class="col-sm-2 control-label">Pros<span class="redCol">* </span></label>
            <div class="col-sm-8">
                <textarea  rows="4" cols="50" maxlength="500" class="required form-control" name="pros" id="description"  ><?php echo isset($pros) ? stripslashes($pros) : ''; ?></textarea>
            </div>
        </div>

        <div class="form-group">
            <label for="description" class="col-sm-2 control-label">Cons<span class="redCol">* </span></label>
            <div class="col-sm-8">
                <textarea  rows="4" cols="50" maxlength="500" class="required form-control" name="cons" id="description"  ><?php echo isset($cons) ? stripslashes($cons) : ''; ?></textarea>
            </div>
        </div>

        <div class="form-group">
            <label for="description" class="col-sm-2 control-label">Suggestion/General Comment<span class="redCol">* </span></label>
            <div class="col-sm-8">
                <textarea  rows="4" cols="50" maxlength="500" class="required form-control" name="feedback" id="description"  ><?php echo isset($feedback) ? stripslashes($feedback) : ''; ?></textarea>
            </div>
        </div>

        <div class="form-group">
            <label for="description" class="col-sm-2 control-label">Have you notified anyone?</label>
            <div class="col-sm-8">
                <?php echo isset($yes_no_planto) ? stripslashes($yes_no_planto) : ''; ?>
            </div>
        </div>

        <div class="form-group">
            <label for="description" class="col-sm-2 control-label">Who did you tell</label>
            <div class="col-sm-8">
                <?php echo isset($who_did_you_tell) ? stripslashes($who_did_you_tell) : ''; ?>
            </div>
        </div>
        <div class="form-group">
            <label for="description" class="col-sm-2 control-label">What was the response?</label>
            <div class="col-sm-8">
             <?php echo isset($what_response) ? stripslashes($what_response) : ''; ?>
            </div>
        </div>




        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-8">

                <input type="submit" value="Update" name="update" class="submit_btn btn" src="<?php echo ADMIN_IMAGE; ?>submit.gif">


            </div>
        </div>
    </form>
</div>

<table class="ds_box" cellpadding="0" cellspacing="0" id="ds_conclass" style="display: none;">
    <tr><td id="ds_calclass"></td></tr></table>

<script type="text/javascript">
    $(document).ready(function() {
        $('input[type="submit"]').focus();
        $('.imageupload').hide();
        $('#type').change(function() {
            var value = $(this).val();
            if (parseInt(value) == 1) {
                $('.imageupload').show();
                $('#image_upload').addClass('required');
            } else {
                $('.imageupload').hide();
                $('#image_upload').removeClass('required');
            }
        });
<?php if ((isset($type) && $type == 1) || (isset($_POST['type']) && $_POST['type'] == 1)) {
        ?>
                $('.imageupload').show();
    <?php }
?>
        $('#addReview').validate({
            rules: {
//                title: {minlength: '2', maxlength: '30'},
//                content: {maxlength: '500'},
                image_path: {
                    extension: "jpeg|png|gif|jpg"
                },
                banner_image: {
                    extension: "jpeg|png|gif|jpg"
                }
            },
            messages: {
//                title: {minlength: "Title must be greter than 1 character", maxlength: "Title must be less than 50 character"},
//                content: {maxlength: "Content must be less than or equalb 30 character"},
                image_path: {
                    extension: "Please upload valid file formats"
                },
                banner_image: {
                    extension: "Please upload valid file formats"
                }
            }
        });
    });




</script>

