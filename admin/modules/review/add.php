<style>
    th,td,tr{border:0px!important; }
    .input{background-color:white;}
</style>
<?php
global $db;
$qryDuration = "select id, title from " . _prefix('durations') . ' where  status=1 AND deleted=0';
$resultDuration = $db->sql_query($qryDuration);
$durationVisit = $db->sql_fetchrowset($resultDuration);

$qryKnowMe = "select id, title from " . _prefix('know_me') . ' where  status=1 AND deleted=0';
$resultKnowMe = $db->sql_query($qryKnowMe);
$knowMe = $db->sql_fetchrowset($resultKnowMe);
if (key($_REQUEST) == 'edit') {
    $id = $_GET['id'];
    $sql_query = "SELECT * FROM " . _prefix("feedbacks") . " where md5(id)='$id'";
    $res = $db->sql_query($sql_query);
    $records = $db->sql_fetchrowset($res);
    // prd($records);
    if (count($records)) {
        foreach ($records as $record) {
            $supplier = $record['sp_id'];
            $feedback = $record['feedback'];
            $product = $record['product_id'];
            $service = ($record['service'] * 4);
            $staff = ($record['staff'] * 4);
            $management = ($record['management'] * 4);
            $activities = ($record['activities'] * 4);
            $value_for_money = ($record['value_for_money'] * 4);
            $know_me = $record['know_me'];
            $visit_duration = $record['visit_duration'];
            $lastVisit = $record['last_visit'];
        }
    }
}


if (isset($submit) && $submit == 'Submit') {
    $fields = array(
        'cs_id' => $_SESSION['Aid'],
        'sp_id' => $supplier,
        'product_id' => $product,
        'feedback' => trim($feedback_des),
        'service' => trim($facility),
        'staff' => trim($staff),
        'management' => trim($management),
        'activities' => trim($activities),
        'value_for_money' => trim($value),
        'know_me' => trim($knowMeId),
        'visit_duration' => trim($visitDuration),
        'last_visit' => trim($lastVisit),
        'created' => date('Y/m/d h:i:s', time())
    );
    $insert_result = $db->insert(_prefix('feedbacks'), $fields);
    if ($insert_result) {
        $msg = common_message_supplier(1, constant('CLIENT_FEEDBACK'));
        $_SESSION['msg'] = $msg;
        redirect_to(MAIN_PATH . "/review.php?review");
    }
}

if (isset($update) && $update == 'Update') {

    $fields_array = add_review();
    $response = Validation::_initialize($fields_array, $_POST);
    if (isset($response['valid']) && $response['valid'] > 0) {
        $fields = array(
            'feedback' => trim($feedback_des),
            'service' => trim($_POST['facility']),
            'staff' => trim($_POST['staff']),
            'management' => trim($_POST['management']),
            'activities' => trim($_POST['activities']),
            'value_for_money' => trim($_POST['value']),
            'know_me' => trim($knowMeId),
            'visit_duration' => trim($visitDuration),
            'last_visit' => trim($lastVisit),
            'created' => date('Y/m/d h:i:s', time())
        );
        $where = "where md5(id)= '$id'";
        $update_result = $db->update(_prefix('feedbacks'), $fields, $where);

        if ($update_result) {
            // Message for insert
            $msg = common_message(1, constant('UPDATE'));
            $_SESSION['msg'] = $msg;
            //ends here
            redirect_to(MAIN_PATH . "/review.php?review");
        }
    } else {
        $errors = '';
        foreach ($response as $key => $message) {
            $error = true;
            $errors .= $message . "<br>";
        }
    }
}
?>

<div class="row" style="margin-bottom: 20px; border-bottom: 1px solid rgb(204, 204, 204);">
    <div class="col-sm-offset-2 col-sm-6 success">
        <?php if (isset($error)) { ?>
            <div class="errorForm" id="error" style="display: block;">
                <img align="absmiddle" src="<?php echo HOME_PATH . 'images/error.png'; ?>">&nbsp;<?php echo $errors; ?>.<br>
            </div>
            <!--echo '<div style="color:#FF0000">' . $errors . '</div>';-->
            <?php
        }
        if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
            ?>
            <div class="errorForm" id="error" style="display: block;">
                <img align="absmiddle" src="<?php echo HOME_PATH . 'images/error.png'; ?>">&nbsp;<?php echo $_SESSION['msg']; ?>.<br>
            </div>
            <?php
            unset($_SESSION['msg']);
        }
        ?>
    </div>
</div>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="95%">
    <tbody>
        <tr style="background:#4b99e6;">
            <td>
                <div class="breadcrumb">
                    <ul>
                        <li><a href="<?php echo HOME_PATH_URL; ?>admin.php"><i class="fa fa-home"></i>Home </a>>><a href="<?php echo HOME_PATH_URL . "review.php?manage" ?>">Manage Review</a>>> <?php echo (key($_REQUEST) == 'edit') ? 'Edit' : 'Add'; ?></li>
                    </ul>
                </div>
            </td>
        </tr>
    </tbody>
</table>



<div class="container" style="width: 95%; border: 1px solid rgb(204, 204, 204);">
    <div class="row" style="margin-bottom: 20px; border-bottom: 1px solid rgb(204, 204, 204);">
        <div class="col-sm-6">
            <h2 class=""><?php echo (key($_REQUEST) == 'edit') ? 'Edit' : 'Add'; ?> Review</h2>
        </div>

        <div class="col-sm-6">
            <h2 style="float:right;" class="redCol small">* fields required</h2>
        </div>
    </div>

    <p class="clearfix"></p>
    <form action="" id='feedback' role="form" method="POST" name='feedback' enctype="multipart/form-data" action=""  class="form-horizontal">
        <!--<form  action="" id='feedback' role="form" method="POST" name='feedback'>-->
        <div class="form-group">
            <label for="title" class="col-sm-2 control-label">Supplier<span class="redCol">* </span></label>
            <div class="col-sm-8">
                <select name="supplier" id="supplier" class="required supplier"><?php echo supplierList($supplier); ?></select>
                <!--<input type="text"  class="required form-control" name="supplier" id="supplier"  value="<?php echo isset($supplier) ? stripslashes($supplier) : ''; ?>"/>-->
            </div>
        </div>
        <div class="form-group">
            <label for="product" class="col-sm-2 control-label" class="required">Product<span class="redCol">* </span></label>
            <div class="col-sm-8">
                <select name="product" id="product_id" class="required"><?php echo productList($supplier, $product); ?></select>
                <!--<input type="text"  class="required form-control" name="product" id="product"  value="<?php echo isset($product) ? stripslashes($product) : ''; ?>"/>-->
            </div>
        </div>
        <div class="form-group">
            <table class="table table-bordered table_bord" style="width:75%; margin-left:8%;">
                <thead>
                    <tr>
                        <th class="th_text" width="80%">Categories</th>
                        <th class="th_text" width="20%">Rating</th>
                    </tr>
                </thead>

                <tbody>
                    <tr>
                        <td width="80%">Facility/Room </td>
                        <?php if (key($_REQUEST) == 'edit') { ?>
                            <td width="20%"><div class="rating" id='1'  data-average="<?php echo $service ?>" data-id="1"></div>
                            <?php } else { ?>
                            <td width="20%"><div class="rating" id='1'  data-average="0" data-id="1"></div>
                            <?php } ?>
                            <input type='hidden' class='star_1 star_rating required' name='facility' value='<?php echo $record['service']; ?>'>
                        </td>
                    </tr>

                    <tr>
                        <td width="80%">Staff </td>
                        <?php if (key($_REQUEST) == 'edit') { ?>
                            <td width="20%"><div class="rating" id='2'  data-average="<?php echo $staff ?>" data-id="2"></div>
                            <?php } else { ?>
                            <td width="20%"><div class="rating" id='2'  data-average="0" data-id="2"></div>
                            <?php } ?>
                            <input type='hidden' class='star_1 star_rating required' name='staff' value='<?php echo $record['staff']; ?>'>
                        </td>
                    </tr>

                    <tr>
                        <td width="80%">Management</td>
                        <?php if (key($_REQUEST) == 'edit') { ?>
                            <td width="20%"><div class="rating" id='3'  data-average="<?php echo $management ?>" data-id="3"></div>
                            <?php } else { ?>
                            <td width="20%"><div class="rating" id='3'  data-average="0" data-id="3"></div>
                            <?php } ?>
                            <input type='hidden' class='star_3 star_rating required' name='management' value='<?php echo $record['management']; ?>'></td>
                    </tr>

                    <tr>
                        <td width="80%">Activities</td>
                        <?php if (key($_REQUEST) == 'edit') { ?>
                            <td width="20%"><div class="rating" id='4'  data-average="<?php echo $activities ?>" data-id="4"></div>
                            <?php } else { ?>
                            <td width="20%"><div class="rating" id='4'  data-average="0" data-id="4"></div>
                            <?php } ?>
                            <input type='hidden' class='star_4 star_rating required' name='activities' value='<?php echo $record['activities']; ?>'></td>
                    </tr>

                    <tr>
                        <td width="80%">Value for money</td>
                        <?php if (key($_REQUEST) == 'edit') { ?>
                            <td width="20%"><div class="rating" id='5'  data-average="<?php echo $value_for_money ?>" data-id="5"></div>
                            <?php } else { ?>
                            <td width="20%"><div class="rating" id='5'  data-average="0" data-id="5"></div>
                            <?php } ?>
                            <input type='hidden' class='star_5 star_rating required' name='value' value='<?php echo $record['value_for_money']; ?>'></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="form-group"  style=" margin-left:8%;">
            <h4 class="comment_h4">How do you know about this place?</h4>
            <div class="form-group ">
                <?php
                foreach ($knowMe as $key => $record) {
                    if ($know_me == $record['id']) {
                        $selected = 'checked';
                    } else {
                        $selected = '';
                    }
                    ?>
                    <input type="radio" name="knowMeId" id="<?php echo 'visits-' . $record['id']; ?>" value="<?php echo $record['id']; ?>" <?php echo $selected; ?>><label style="font-weight:normal;" for="<?php echo 'visits-' . $record['id']; ?>"><?php echo '&nbsp;' . $record['title']; ?></label></br>
                <?php } ?>

            </div>
        </div>
        <div class="form-group"  style=" margin-left:8%;">
            <h4 class="comment_h4">How long did you live/visit there?</h4>
            <div class="form-group">
                <?php
                foreach ($durationVisit as $key => $record) {
                    if ($visit_duration == $record['id']) {
                        $selected = 'checked';
                    } else {
                        $selected = '';
                    }
                    ?>
                    <input type="radio" name="visitDuration" id="<?php echo 'durations-' . $record['id']; ?>"  value="<?php echo $record['id']; ?>" <?php echo $selected; ?>><label style="font-weight:normal;"  for="<?php echo 'durations-' . $record['id']; ?>"><?php echo '&nbsp;' . $record['title']; ?></label></br>
                <?php } ?>
            </div>
        </div>
        <div class="form-group"  style=" margin-left:8%;">
            <h4 class="comment_h4">When did you last live/visit there?</h4>
            <div class="form-group">
                <?php switch ($lastVisit){
                            case '1' :
                                $selected1='checked';
                                break;
                            case '2' :
                                $selected2='checked';
                                break;
                            case '3' :
                                $selected3='checked';
                                break;
                            default :
                                $selected1='';
                                $selected2='';
                                $selected3='';
                                break;
                                
                }                              ?>
                Within the last <input type="radio" name="lastVisit" value="1" <?php echo $selected1; ?>>12 mths <input type="radio" name="lastVisit" value="2" <?php echo $selected2; ?>>1-3 yrs <input type="radio" name="lastVisit" value="3" <?php echo $selected3; ?>>3+ yrs
            </div>
        </div>

        <div class="form-group">
            <label for="feedback_des" class="col-sm-2 control-label">Feedback<span class="redCol">* </span></label>
            <div class="col-sm-8">
                <textarea  rows="4" cols="50" maxlength="500" class="required form-control" name="feedback_des" id="feedback_des"  ><?php echo isset($feedback) ? stripslashes($feedback) : ''; ?></textarea>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-8">
                <?php
                if (key($_REQUEST) == 'edit') {
                    ?>
                    <input type="submit" value="Update" name="update" class="submit_btn btn" src="<?php echo ADMIN_IMAGE; ?>submit.gif">

                    <?php
                } else {
                    ?>
                    <input type="submit" value="Submit" name="submit" class="submit_btn btn">
                    <?php
                }
                ?>
            </div>
        </div>
    </form>
</div>

<table class="ds_box" cellpadding="0" cellspacing="0" id="ds_conclass" style="display: none;">
    <tr><td id="ds_calclass"></td></tr>
</table>

<script>
    $(document).ready(function() {

        setTimeout(function() {
            $("#success").hide('slow');
            $('#error').hide('slow');
        }, 5000);
        $('#feedback').validate({
            submitHandler: function() {
                var star = 0;
                $('.star_rating').each(function() {
                    var value = $(this).val();
                    if (value == '' || value == undefined) {
                        star++;
                    }
                    return star;
                });
                if (star > 0) {
                    $('label .rating_error').html('Please rate all the options');
                    alert('Please rate all the options');
                    return false;
                } else {
                    $('label .rating_error').html('');
                    form.submit();
                }

            }
        });
        $('.rating').jRating({
            step: true,
            length: 5,
            canRateAgain: true,
            nbRates: 30,
            showRateInfo: false,
            onClick: function(element, rate) {
                var id = element.id;
                $('.star_' + id).val(rate / 4);
            }
        });
    });
</script>

