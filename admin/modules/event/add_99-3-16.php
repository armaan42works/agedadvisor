<style>
    th,td,tr{border:0px!important; }
    .input{background-color:white;}
</style>
<?php
global $db;
$description = '';
$image = '';
$event_id=$_REQUEST['id'];
$event_title= $_POST['event_title'];
$start_event_time=$_POST['start_event_date_time'];
$end_event_time=$_POST['end_event_date_time'];
$event_des=$_POST['event_description'];
$event_location=$_POST['event_location'];
$link_facility=$_POST['link_to_facility'];
$cer_type=$_POST['certification_types'];


/*if (($_REQUEST['action']) == 'delete') {
	echo "<script>alert('+++++++++++++++');</script>>";
   echo  $id = $_REQUEST['id'];
   die;
    //$sql_query = "delete FROM " . _prefix("add_event") . " where id='$id'";
    //$res = $db->sql_query($sql_query);
}*/
//die;
//$path=$_SERVER['SERVER_NAME']."/modules/supplier/event_images/";
if (isset($submit) && $submit == 'Submit') {
        if (isset($_FILES["image"]["name"])) 
		{
				$target_path2 ="../modules/supplier/event_images/";
				$logoName = basename($_FILES["image"]["name"]);
				$target_path = $target_path2.$logoName;
				$move=move_uploaded_file($_FILES["image"]["tmp_name"], $target_path);
               	{  
				   if($move)
				   {     
				    $query1="insert into ad_add_event set event_title='$event_title', start_event_date_time='$start_event_time', end_event_date_time='$end_event_time' , event_description='$event_des' , event_location='$event_location',  certification_types='$cer_type', event_img_name='$logoName', event_organizer_email='nigel@agedadvisor.co.nz', event_organizer_name='admin', event_organizer_id='2'";
				     $insert_event_admin=mysqli_query($db->db_connect_id,$query1);
					 if($insert_event_admin)
					 {
					  echo "<script>
					         alert('Event has been successfully submitted');
					       </script>" ;
					      	 
					 }
					 else 
					 {
						 echo "<script>
					            alert('Please try again');
					          </script>" ;
							
					 }
				  }
			   }
     
	   }
      
}

if (key($_REQUEST) == 'edit') {
    $id = $_GET['id'];
    $sql_query = "SELECT * FROM " . _prefix("add_event") . " where id='$id'";
    $res = $db->sql_query($sql_query);
    $records = $db->sql_fetchrowset($res);
    if (count($records)) {
        foreach ($records as $record) {
            $id = $record['id'];
            $event_title = $record['event_title'];
            $start_event_date_time = $record['start_event_date_time'];
            $end_event_date_time = $record['end_event_date_time'];
            $event_description = $record['event_description'];
            $event_img_name = $record['event_img_name'];
            $event_location = $record['event_location'];
            $link_to_facility = $record['link_to_facility'];
			$certification_types = $record['certification_types'];
            
        }
    }
}
if (isset($update) && $update == 'Update') {
        if (isset($_FILES['image']['name'])) {
                $ext =  $_FILES['image']['name'];
                $target_pathL = "../modules/supplier/event_images/";;
                $logoName = basename($_FILES['image']['name']);
                 $target_path = $target_pathL . $logoName;
                 $move=move_uploaded_file($_FILES['image']["tmp_name"], $target_path);
               {
				   if($move)
				   {     $query1="update ad_add_event set event_title='$event_title', start_event_date_time='$start_event_time', end_event_date_time='$end_event_time' , event_description='$event_des' , event_location='$event_location', certification_types='$cer_type', event_img_name='$logoName' where id='$event_id'";
				        $insert_event_admin=mysqli_query($db->db_connect_id,$query1);
					    if($insert_event_admin)
					    {
					      echo "<script>
					            alert('Event successfully updated');
					          </script>" ;
							
					    }
					     else 
						 {
							  echo "<script>
					            alert('Event not updated , Please try again');
					            </script>" ;
							
						 }
				    }
			  }
     
			
        }
        
   
}

if (key($_REQUEST) == 'delete') {
   $id = $_REQUEST['id'];
   $sql_query = "delete  FROM " . _prefix("add_event") . " where id='$id'";
    $res = $db->sql_query($sql_query);
}
?>

<div style="margin-top: 10px;">
    <table align="center" border="0" cellpadding="0" cellspacing="0" width="95%">
        <tbody>
            <tr style="background:#4b99e6;">
                <td>
                    <div class="breadcrumb">
                        <ul>
                            <li><a href="<?php echo HOME_PATH_URL; ?>admin.php"><i class="fa fa-home"></i>Home </a>>><a href="<?php echo HOME_PATH_URL . "event.php?manage" ?>"> Manage Events </a>>> <?php echo (key($_REQUEST) == 'edit') ? 'Edit' : 'Add'; ?></li>
                        </ul>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
    <div class="row">
        <div class="col-sm-offset-2 col-sm-6 success">
            <?php if (isset($error)) { ?>
                <div class="errorForm" id="error" style="display: block;">
                    <img align="absmiddle" src="<?php echo HOME_PATH . 'admin/images/error.png'; ?>">&nbsp;<?php echo $errors; ?>.<br>
                </div>
                <!--echo '<div style="color:#FF0000">' . $errors . '</div>';-->
                <?php
            }
            if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
                ?>
                <div class="errorForm" id="error" style="display: block;">
                    <img align="absmiddle" src="<?php echo HOME_PATH . 'admin/images/error.png'; ?>">&nbsp;<?php echo $_SESSION['msg']; ?>.<br>
                </div>
                <?php
                unset($_SESSION['msg']);
            }
            ?>
        </div>
    </div>
    <div class="container" style="width: 95%; border: 1px solid rgb(204, 204, 204);">
        <div class="row" style="margin-bottom: 20px; border-bottom: 1px solid rgb(204, 204, 204);">
            <div class="col-sm-6">
                <h2 class=""><?php echo (key($_REQUEST) == 'edit') ? 'Edit' : 'Add'; ?> Events </h2>
            </div>

            <div class="col-sm-6">
                <h2 style="float:right;" class="redCol small">* fields required</h2>
            </div>
        </div>
        <p class="clearfix"></p>
        <form name="addLogo" id="addLogo" action="" method="POST" class="form-horizontal" role="form" enctype="multipart/form-data">
            <div class="form-group">
                <label for="title" class="col-sm-2 control-label">Event Title<span class="redCol">*</span></label>
                <div class="col-sm-8">
                    <input type="text"  class="required form-control" name="event_title" id="event_title"  value="<?php echo isset($event_title) ? stripslashes($event_title) : ''; ?>"/>
                   
                </div>
            </div>
            <div class="form-group">
                <label for="title" class="col-sm-2 control-label">Start Event Date Time<span class="redCol">*</span></label>
                <div class="col-sm-8">
                    <input type="datetime-local" class="required form-control" name="start_event_date_time" value="<?php echo isset($start_event_date_time) ? stripslashes($start_event_date_time) : ''; ?>"/></br>
                    <input type="text" class="required form-control" name="" value="<?php echo isset($start_event_date_time) ? stripslashes($start_event_date_time) : ''; ?>" disabled/>
                </div>
            </div>
            
            <div class="form-group">
                <label for="title" class="col-sm-2 control-label">Etart Event Date Time<span class="redCol">*</span></label>
                <div class="col-sm-8">
                    <input type="datetime-local" class="required form-control" name="end_event_date_time" value="<?php echo isset($end_event_date_time) ? stripslashes($end_event_date_time) : ''; ?>"/></br>
                    <input type="text" class="required form-control" name="" value="<?php echo isset($end_event_date_time) ? stripslashes($end_event_date_time) : ''; ?>" disabled/>
                </div>
            </div>
            <div class="form-group">
                <label for="title" class="col-sm-2 control-label">Event Description<span class="redCol">*</span></label>
                <div class="col-sm-8">
                 <textarea rows="7" class="required form-control" name="event_description" id="event_description" > <?php echo isset($event_description) ? stripslashes($event_description) : ''; ?></textarea>
                    <!--<input type="text"  class="required form-control" name="event_description" id="event_description"  value="<?php //echo isset($event_description) ? stripslashes($event_description) : ''; ?>"/>-->
                </div>
            </div>
             <?php  if (key($_REQUEST) == 'edit') { 
			 ?>
            
            <div class="form-group">
                <label for="title" class="col-sm-2 control-label">Event location<span class="redCol">*</span></label>
                <div class="col-sm-8">
                 <select id="event_location" name="event_location" class="required form-control">
                 <?php $cities_name11=mysqli_query($db->db_connect_id,"select * from ad_cities  where id='$event_location'");
				  if($cities_name_fetch11=mysqli_fetch_array($cities_name11))
				  { ?>
                   <option value="<?php echo $cities_name_fetch11['id'] ?>"><?php echo $cities_name_fetch11['title'] ?></option>
                  <?php } ?>
                 <?php $cities_name=mysqli_query($db->db_connect_id,"select * from ad_cities order by rand() limit 20");
						while($cities_name_fetch=mysqli_fetch_array($cities_name))
						{   ?>
                    <option value="<?php echo $cities_name_fetch['id']?>"><?php echo $cities_name_fetch['title']?></option>
                    <?php } ?>
                    </select>
                </div>
            </div>
            <?php } else { ?>
            <div class="form-group">
                <label for="title" class="col-sm-2 control-label">Event location<span class="redCol">*</span></label>
                <div class="col-sm-8">
                 <select id="event_location" name="event_location" class="required form-control">
                   <option value="">Select Event Location</option>
                
                 <?php $cities_name=mysqli_query($db->db_connect_id,"select * from ad_cities order by rand() limit 20");
						while($cities_name_fetch=mysqli_fetch_array($cities_name))
						{   ?>
                    <option value="<?php echo $cities_name_fetch['id']?>"><?php echo $cities_name_fetch['title']?></option>
                    <?php } ?>
                    </select>
                </div>
            </div>
            
            
            
            
            <?php } ?> 
            
            
            
         
            
            
             <?php  if (key($_REQUEST) == 'edit') { 
			 ?>
			<div class="form-group">
                <label for="title" class="col-sm-2 control-label">Certification Types<span class="redCol">*</span></label>
                <div class="col-sm-8">
                     <select id="certification_types" name="certification_types" class="required form-control" >
                        <option value="<?php echo $certification_types;?>"><?php echo $certification_types;?></option>
                        <option value="Retirement Village">Retirement Village</option>
                        <option value="Aged Care">Aged Care</option>
                        <option value="Home Services">Home Services</option>
                    </select>
                </div>
            </div>
			  
			 <?php } else{ ?>
            
              <div class="form-group">
                <label for="title" class="col-sm-2 control-label">Certification Types<span class="redCol">*</span></label>
                <div class="col-sm-8">
                     <select id="certification_types" name="certification_types" class="required form-control" >
                        <option value="">Select Certification Types</option>
                        <option value="Retirement Village">Retirement Village</option>
                        <option value="Aged Care">Aged Care</option>
                        <option value="Home Services">Home Services</option>
                    </select>
                </div>
            </div>
            <?php } ?>
             <?php  if (key($_REQUEST) == 'edit') { ?>
             <div class="form-group">
                <label for="image" class="col-sm-2 control-label">Event image<span class="redCol">* </span>(200*100)</label>
                <div class="col-sm-8">
                   <img  style="height:100px; width:100px;" src="https://agedadvisor.nz/modules/supplier/event_images/<?php echo $event_img_name ; ?>"/>                </div>
            </div> 
            <?php } ?>
            <div class="form-group">
                <label for="image" class="col-sm-2 control-label">Logo image<span class="redCol">* </span>(200*100)</label>
                <div class="col-sm-8">
                    <input type="file" name="image" id="image" class="required">
                </div>
            </div>
            
            <div class="form-group">
                <!--<label for="image" class="col-sm-2 control-label">Logo image<span class="redCol">* </span></label>-->
                <div class="col-md-2"></div>
                <div class="col-sm-8">
                    <?php if (file_exists(DOCUMENT_PATH . 'admin/files/community_logos/' . $image) && $image != '' && key($_REQUEST) == 'edit') { ?>
                        <img width="100px;" height="100px;" title="feature" src="<?php echo MAIN_PATH . '/files/community_logos/' . $image ?>"/>
                    <?php } ?>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-8">
                    <?php
                    if (key($_REQUEST) == 'edit') {
                        ?>
                        <input type="submit" value="Update" name="update" class="submit_btn btn" src="<?php echo ADMIN_IMAGE; ?>submit.gif">

                        <?php
                    } else {
                        ?>
                        <input type="submit" value="Submit" name="submit" class="submit_btn btn">
                        <?php
                    }
                    ?>
                </div>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('input[type="submit"]').focus();
    });
    $('#addLogo').validate();
<?php if (file_exists(DOCUMENT_PATH . 'admin/files/community_logos/' . $image) && $image != '' && key($_REQUEST) == 'edit') { ?>
        $('#image').removeClass('required');
<?php } ?>



  </script>
  
   
       
        <script> 
		/*$(document).ready(function(){
			
		$("#event_location1").keyup(function(){
		$.ajax({
		type: "POST",
		url: "readCity.php",
		data:'keyword='+$(this).val(),
		
		
		success: function(data){
			$("#suggesstion-box").show();
			$("#suggesstion-box").html(data);
			$("#event_location1").css("background","#FFF");
		}
		});
		
		});
	});   */
    
       </script>  