<?php
    global $db;
    $description = '';
    include_once("./fckeditor/fckeditor.php");


    if (key($_REQUEST) == 'view') {
        $id = $_GET['id'];
        $sql_query = "SELECT dm.*  FROM " . _prefix("messages") . " as dm   where md5(dm.id)='$id'";
        $res = $db->sql_query($sql_query);
        $records = $db->sql_fetchrowset($res);

        if (count($records)) {
            $pid = $records[0]['id'];
            $quote_id = $records[0]['quote_id'];
            $email_id = $records[0]['email_id'];
            $subject = $records[0]['subject'];
            $body = $records[0]['body'];
            $created = $records[0]['created'];
            $message_id = $records[0]['id'];
            $receiver_id = $records[0]['receiver_id'];
            $attachment = $records[0]['filename'];
        }
    }
    $mailTo = $receiver_id == 0 ? $records[0]['sender_id'] : $receiver_id;
    $getEmail = "SELECT * FROM  " . _prefix('users') . " WHERE id= '$mailTo'";
    $resEmail = $db->sql_query($getEmail);
    $dataEmail = $db->sql_fetchrow($resEmail);
    if (isset($reply) && $reply == 'Reply') {

        $fields_array = reply_message();
        $response = Validation::_initialize($fields_array, $_POST);

        if (isset($response['valid']) && $response['valid'] > 0) {
            $body = $_POST["FCKeditor1"];
            $fields = array('parent_message_id' => $pid,
                'subject' => $subject,
                'body' => $body,
                'quote_id' => $quote_id,
                'email_id' => $dataEmail['email'],
                'ip_address' => $_SERVER['REMOTE_ADDR'],
                'sender_id' => 0,
                'receiver_id' => $dataEmail['id'],
                'user_type' => 2,
                'is_read_admin' => 0);

            $insert_result = $db->insert(_prefix('messages'), $fields);
            $message_id = mysqli_insert_id($db->db_connect_id);
            $db->update(_prefix('messages'), array('is_read_client' => 0, "created" => date('Y-m-d H:i:s')), " WHERE id = '$pid'");
            $attachment = '';
            if (isset($_FILES['file_name'])) {
                for ($i = 0; $i < count($_FILES['file_name']['name']); $i++) {

                    if ($_FILES['file_name']['error'][0] == 0) {
                        $fileData = pathinfo($_FILES['file_name']["name"][$i]);
                        $target_path = '../admin/files/message/';
                        $filename = rand(1, 100000);
                        $date = new DateTime();
                        $timestamp = $date->getTimestamp();
                        $imageName = $filename . $timestamp . '_' . imgNameSanitize(basename($_FILES['file_name']['name'][$i]),20);
                        $target_path = $target_path . $imageName;
                        $message_fields = array('message_id' => $message_id, 'filename' => $imageName);
                        if ($_FILES['file_name']['name'][$i] != "") {

                            $insert_attachment = $db->insert(_prefix('attachment'), $message_fields);
                        }

                        $var = move_uploaded_file($_FILES['file_name']["tmp_name"][$i], $target_path);
                        $attachment[$i] = ADMIN_PATH . 'files/message/' . $imageName;
                    }
                }
            }
            $send = sendmail($dataEmail['email'], $subject, $body, $attachment);
            if ($insert_result) {
                $msg = common_message(1, constant('SEND_MESSAGE'));
                $_SESSION['msg'] = $msg;
            }
        } else {
            $errors = '';
            foreach ($response as $key => $message) {
                $error = true;
                $errors .= $message . "<br>";
            }
        }
    }
?>
<div class="sublinks" height="40" valign="bottom" style="margin-bottom: 10px;">
    <?php
        if (isset($error)) {
            echo '<div style="color:#FF0000">' . $errors . '</div>';
        }
        if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
            echo $_SESSION['msg'];
            unset($_SESSION['msg']);
        }
    ?>
</div>
<form method='post' id='replymail' enctype="multipart/form-data">
    <table align="center" width="95%">

        <tbody>

            <tr style="background:#4b99e6;">
                <td colspan="2">
                    <div class="breadcrumb">
                        <ul>
                            <li><a href="<?php echo HOME_PATH_URL; ?>admin.php"><i class="fa fa-home"></i>Home </a>>>Message</li>
                        </ul>
                    </div>
                </td>
            </tr>






            <tr><td><h3>Subject : <?php echo stripslashes($subject); ?></h3></td></tr>
            <tr style='display:none;' id="replyform"><td>
                    <?php
                        $oFCKeditor = new FCKeditor('FCKeditor1');
                        $oFCKeditor->BasePath = './fckeditor/';
                        $oFCKeditor->Height = '400px';
                        $oFCKeditor->Value = "";

                        $oFCKeditor->Create();
                    ?> <br>
                    <input type="hidden" name="email" id="email" value="<?php echo $dataEmail['email'] ?>">
                    <div class="input text" >
                        <input type="file" name="file_name[]" id="">
                    </div>
                    <div class="input text add" >
                        <a href="javascript:void(0);" id="0" class="addFile">+Add</a>
                    </div>
                    <br>

                    <input type='submit' value='Reply' name='reply'>

                </td> 

            </tr>
            <?php
                $getmail = "SELECT dm.*, du.email FROM " . _prefix("messages") . " as dm "
                        . "LEFT JOIN " . _prefix("users") . " as du ON du.id = dm.sender_id "
                        . "where md5(dm.parent_message_id)='$id' order by id desc";
                $resMail = $db->sql_query($getmail);
                $numMail = $db->sql_numrows($resMail);
                $allMails = $db->sql_fetchrowset($resMail);
                if ($numMail > 0) {
                    foreach ($allMails as $mail) {
                        $otherattachment = $mail['filename'] == '' ? '' : $mail['filename'];
                        if ($mail['is_read_admin'] == 0 && !isset($reply) && $reply != 'Reply') {
                            $db->sql_query("UPDATE "._prefix('messages')." SET is_read_admin = '1' WHERE `id` ='{$mail['id']}'");
                        }
                        ?>
                        <tr>
                            <td  colspan="2">
                                FROM : <?php echo $mail['sender_id'] == 0 ? 'KA Analytics' : $mail['email'] ?><br>
                                TO : <?php echo stripslashes($mail['email_id']); ?><br>
                                Date : <?php echo date('M d, Y H: i A', strtotime($mail['created'])); ?><br><br>
                                <?php echo stripslashes($mail['body']); ?><br><br>
                                <?php
                                getAllAttachment($mail['id']);
                                ?>
                            </td>

                        </tr>
                        <?php
                    }
                    ?>

                    <?php
                }
                if ($records[0]['is_read_admin'] == 0) {
                    $db->sql_query("UPDATE `da_messages` SET is_read_admin = '1' WHERE `da_messages`.`id` ='{$pid}'");
                }
            ?>

            <tr>
                <td  colspan="2">
                    FROM : <?php echo $records[0]['sender_id'] == 0 ? 'KA Analytics' : $dataEmail['name'] ?><br>
                    TO : <?php echo stripslashes($email_id); ?><br>
                    Date : <?php echo date('M d, Y H: i A', strtotime($created)); ?><br><br>
                    <?php echo stripslashes($body); ?><br>
                    <?php
                        getAllAttachment($pid);
                    ?>

                    <a href="javascript:void(0);" class="send_reply" id="send_reply"> send reply</a>  
                </td>

            </tr>
        </tbody></table>

</form>

<script type="text/javascript">

    $(document).ready(function() {
        $('#replyform').hide();

        $('.send_reply').live('click', function() {
            $('html, body').animate({scrollTop: 0}, 0);
            $('#replyform').show();
            $('#send_reply').hide();
        });

        $('.addFile').click(function() {
            var inputFile = '<div class="input text" ><input type="file" name="file_name[]" id="file_name"></div>';
            $('.add').before(inputFile);
        });
    });

</script>

