<!-----------------
Objective : Listing of all the Email Messages on Admin Panel
Filename : application.php
Created By: Yogesh Joshi <yogesh.joshi@ilmp-tech.com>
Created On: 20th August 2014
------------------->
<script type="text/javascript">
    $(document).ready(function() {
        var page = 0;
        var li = '0_no';
        var data ='&user_type=<?php echo $type; ?>'; 
        changePagination(page, li, 'message', data);
    });
</script> 
<table align="center" border="0" cellpadding="0" cellspacing="0" width="95%">

    <tbody>
        <tr style="background:#4b99e6;">
            <td>
                <div class="breadcrumb">
                    <ul>
                        <li><a href="<?php echo HOME_PATH_URL; ?>admin.php"><i class="fa fa-home"></i>Home </a>>><?php echo $type == 0 ? 'Client Messages' :'SalesPerson Messages' ?></li>
                    </ul>
                </div>
            </td>
        </tr>
        
    <div class="sublinks" height="40" valign="bottom" style="margin-bottom: 10px;">
                    <?php
                        if (isset($error)) {
                            echo '<div style="color:#FF0000">' . $errors . '</div>';
                        }
                        if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
                            echo $_SESSION['msg'];
                            unset($_SESSION['msg']);
                        }
                    ?>
                </div>
<tbody>
                    <tr>
                        <td>
                            <table  align="center" border="0" cellpadding="0" cellspacing="0"
                                    width="100%">

                                <tbody>


                                    <?php
                                        echo '<div id="pageData"></div>';
                                    ?>    
                                </tbody>

                            </table>
                        </td>
                    </tr>
                </tbody></tbody></table>
<table class="ds_box" cellpadding="0" cellspacing="0" id="ds_conclass" style="display: none;">
    <tr><td id="ds_calclass"></td></tr></table>

