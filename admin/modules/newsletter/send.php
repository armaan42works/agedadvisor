<style>
    th,td,tr{border:0px!important; }
    .input{background-color:white;}
    </style>
<?php
   
    global $db;   
    if(isset($submit) && $submit=='Submit') {      
        
      $fields_array = sendnewsletter();
      $response = Validation::_initialize($fields_array, $_POST);
      
      if(isset($response['valid']) && $response['valid']>0){
          $newsletter_id=$_POST["newsletter_id"];
          $subject =$_POST["subject"];
          if($_POST["receiver"] ==1){
              $receiver_all = 1;
              
          }
        else {$receiver_all = 0;}
          if($_POST["receiver"] ==2){
              $receiver_selected = $_POST["receiver_selected"];
              
          }
          else{
               $receiver_selected = "";
          }
              
          
        $fields = array(
                'newsletter_id' => $newsletter_id,
                'subject' => $subject,
             'receiver_all' => $receiver_all,
             'receiver_selected' => $receiver_selected);
             
  
        
$insert_result = $db->insert(_prefix('send_newsletters'), $fields);
            if ($insert_result) {
                
                   $main_obj->redirect(HOME_PATH_URL."newsletter.php?history");
            }
        }  
    else{
        $errors =   '';
        foreach($response as $key=>$message){
            $error = true;
            $errors .= $message."<br>";
        }
    }
   
    } 
    
   
?>
<form name="form1" id="sendNewsLetter" action="" method="post">
    <table align="center" border="0" cellpadding="0" cellspacing="0" width="95%">

        <tbody>
            
           <tr style="background:#4b99e6;">
            <td>
                <div class="breadcrumb">
                    <ul>
                        <li><a href="<?php echo HOME_PATH_URL; ?>admin.php"><i class="fa fa-home"></i>Home </a>>><a href="<?php echo HOME_PATH_URL . "newsletter.php?manage" ?>">Manage Newsletter</a>>>Send</li>
                    </ul>
                </div>
           </tr>
            
                        <tbody>
                            <tr>
                                <td>
                                    <table width="100%">

                                        <tbody>
                                            

                                                        <tbody>
                                                            <td colspan="2" class="bdr" height="25">
                                                   
                                                        <h2>Send Newsletter</h2><span class="redCol" style="float:right; margin-top:-10px;">* fields required</span>
                                                   
                                                </td>
                                                        </tbody>
                                                   
                                            <tr>

                                                <td  align="left" height="26" width="100%">Newsletter<span
                                                        class="redCol">* </span></td>
                                               
                                            </tr>
                                             
                                            <tr>
                                                
                                               <td width="100%">
                                                    <div class="input text">
                                                      <?php
                                                        $sql_query = "SELECT id,title FROM " . _prefix("newsletter") . " where status = 1 && deleted = 0 order by id desc";
                                                        $res = $db->sql_query($sql_query);
                                                         while($row = mysqli_fetch_assoc($res)){
                                                            $options[$row['id']] = $row['title'];
                                                        }?>
                                                        
                                                        <select name="newsletter_id" style="width:200px;" class="required">
                                                       <?php                                                        
                                                        echo options($options, $_POST['newsletter_id']);
                                                        ?>
                                                        
                                                        </select>
                                                        
                                                        
                                                    </div>
                                             </td> 
                                            </tr>

                                            <tr>
                                                <td  width="100%" height="26"> Subject<span
                                                        class="redCol">* </span></td>
                                              
                                            </tr>
                                                     
                                            <tr>
                                               <td >
                                                   <div class="input text">
                                                        <input type="text" name="subject" id="subject" maxlength="50" size="50" class="required" value="<?php  if(isset($subject)){ echo stripslashes($subject); }  ?>" />
                                                    </div>
                                                   </td> 
                                                
                                            </tr>
                                            
                                            <tr>
                                                <td  width="100%" height="26">Receiver<span
                                                        class="redCol">* </span> </td>
                                              
                                            </tr>
                                                     
                                            <tr>
                                               <td >
                                                   <div class="input text">
                                                       <label for="TheRadioField1">  <input type="radio" id="TheRadioField1" name="receiver" value="1" class="send_type" >All</label><br> <label for="TheRadioField2"><input type="radio" id="TheRadioField2" class="send_type" name="receiver" value="2" checked>Selected</label>
                                                    </div>
                                                   </td> 
                                                
                                            </tr>
                                            <tr class="emailaddress">
                                                <td  width="100%" height="26"> Email Addresses (Separated by comma)<span
                                                        class="redCol">* </span></td>
                                              
                                            </tr>
                                                     
                                            <tr class="emailaddress">
                                               <td >
                                                   <div class="input text">
                                                       <textarea name="receiver_selected" id="receiver_selected" cols="60" rows="10" class="required"></textarea>
                                                    </div>
                                                   </td> 
                                                
                                            </tr>
                                            <tr>
                                                <td class="marginleft r" align="right" height="26"><input type="submit" value="Submit" name="submit" src="<?php echo ADMIN_IMAGE; ?>submit.gif"></td></td>
                                                <td class="r marginleft">
                                                
                                            </tr>

                                            <!-- content area ends here --></td>
                                            </tr>
                                        </tbody>

                                    </table>
                                </td>
                            </tr>
                        </tbody></tbody></table></form>
<table class="ds_box" cellpadding="0" cellspacing="0" id="ds_conclass" style="display: none;">
    <tr><td id="ds_calclass"></td></tr></table>
<script>
$(document).ready(function(){
   
    
       $('#sendNewsLetter').validate();
         $('.send_type').live('click',function(){
        var value = $(this).val();
         if(value == 1){
            $('.emailaddress').hide();
            $('#receiver_selected').removeClass('required');
        } else {
            $('.emailaddress').show();
            $('#receiver_selected').addClass('required');
            
        }
    });
});

</script>
