<style>
    th,td,tr{border:0px!important; }
    .input{background-color:white;}
</style>
<?php
global $db;
// prd($_POST);
if (isset($submit) && $submit == 'Submit') {
    $fields_array = restcare();
    $response = Validation::_initialize($fields_array, $_POST);
    if (isset($response['valid']) && $response['valid'] > 0) {
        if ($suburb == '0' && $new_suburb != '') {
            $getduplicatesub = "SELECT * from " . _prefix("suburbs") . " WHERE city_id='" . $city_id . "' && title='" . $new_suburb . "' && status = 1 && deleted = 0";
            $resdupsub = $db->sql_query($getduplicatesub);
            $countdupsub = $db->sql_numrows($resdupsub);
            if ($countdupsub == 0) {
                $fields1 = array(
                    'city_id' => $city_id,
                    'title' => trim($new_suburb)
                );
                $insert_result = $db->insert(_prefix('suburbs'), $fields1);
                $lastid = mysqli_insert_id($db->db_connect_id);
                $suburb = $lastid;
            }
        }
        $getduplicate = "SELECT * from " . _prefix("rest_cares") . " WHERE suburb_id='" . $suburb . "' && title='" . $title . "' && status = 1 && deleted = 0";
        $resdup = $db->sql_query($getduplicate);
        $countdup = $db->sql_numrows($resdup);

        $fields = array(
            'title' => trim($title),
            'suburb_id' => trim($suburb),
        );
        if ($countdup == 0) {
            $insert_result = $db->insert(_prefix('rest_cares'), $fields);
            if ($insert_result) {
                // Message for insert
                $msg = common_message(1, constant('INSERT'));
                $_SESSION['msg'] = $msg;
                //ends here
                redirect_to(MAIN_PATH . "location.php?manageRestcare");
            }
        } else {
            $msg = common_message(0, constant('ALLREADY_INS_REST'));
            $_SESSION['msg'] = $msg;
        }
    } else {
        $errors = '';
        foreach ($response as $key => $message) {
            $error = true;
            $errors .= $message . "<br>";
        }
    }
}
if (key($_REQUEST) == 'editRestcare') {
    $id = $_GET['id'];
    $sql_query = "SELECT rest.*,suburb.city_id FROM " . _prefix("rest_cares") . " as rest Left join " . _prefix("suburbs") . " AS suburb ON suburb.id=rest.suburb_id where md5(rest.id)='$id'";
    $res = $db->sql_query($sql_query);
    $records = $db->sql_fetchrowset($res);
    if (count($records)) {
        foreach ($records as $record) {
            $suburb_id = $record['suburb_id'];
            $city_id = $record['city_id'];
            $title = $record['title'];
        }
    }
}
if (isset($update) && $update == 'Update') {

    $fields_array = location();
    $response = Validation::_initialize($fields_array, $_POST);
    if (isset($response['valid']) && $response['valid'] > 0) {

        $title = $_POST["title"];
        $suburb = $_POST["suburb"];
        $fields = array('title' => trim($title),
            'suburb_id' => trim($suburb),
        );
        $where = "where md5(id)= '$id'";
        $update_result = $db->update(_prefix('rest_cares'), $fields, $where);
        if ($update_result) {
            // Message for insert
            $msg = common_message(1, constant('UPDATE'));
            $_SESSION['msg'] = $msg;
            //ends here
            redirect_to(MAIN_PATH . "location.php?manageRestcare");
        }
    } else {
        $errors = '';
        foreach ($response as $key => $message) {
            $error = true;
            $errors .= $message . "<br>";
        }
    }
}
?>

<div style="margin-top: 10px;">
    <table align="center" border="0" cellpadding="0" cellspacing="0" width="95%">
        <tbody>
            <tr style="background:#4b99e6;">
                <td>
                    <div class="breadcrumb">
                        <ul>
                            <li><a href="<?php echo HOME_PATH_URL; ?>admin.php"><i class="fa fa-home"></i>Home </a>>><a href="<?php echo HOME_PATH_URL . "location.php?manageRestcare" ?>"> Manage Rest Care </a>>> <?php echo (key($_REQUEST) == 'editRestcare') ? 'Edit' : 'Add'; ?></li>
                        </ul>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
    <div class="row">
        <div class="col-sm-offset-2 col-sm-6 success">
            <?php if (isset($error)) { ?>
                <div class="errorForm" id="error" style="display: block;">
                    <img align="absmiddle" src="<?php echo HOME_PATH . 'images/error.png'; ?>">&nbsp;<?php echo $errors; ?>.<br>
                </div>
                <!--echo '<div style="color:#FF0000">' . $errors . '</div>';-->
                <?php
            }
            if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
                ?>
                <div class="errorForm" id="error" style="display: block;">
                    <img align="absmiddle" src="<?php echo HOME_PATH . 'images/error.png'; ?>">&nbsp;<?php echo $_SESSION['msg']; ?>.<br>
                </div>
                <?php
                unset($_SESSION['msg']);
            }
            ?>
        </div>
    </div>
    <div class="container" style="width: 95%; border: 1px solid rgb(204, 204, 204);">
        <div class="row" style="margin-bottom: 20px; border-bottom: 1px solid rgb(204, 204, 204);">
            <div class="col-sm-6">
                <h2 class=""><?php echo (key($_REQUEST) == 'editRestcare') ? 'Edit' : 'Add'; ?> Rest Care</h2>
            </div>

            <div class="col-sm-6">
                <h2 style="float:right;" class="redCol small">* fields required</h2>
            </div>
        </div>
        <p class="clearfix"></p>
        <form name="addRestcare" id="addCity" action="" method="POST" class="form-horizontal" role="form">
            <div class="form-group">
                <label for="city_id" class="col-sm-2 control-label">City<span class="redCol">* </span></label>
                <div class="col-sm-8">
                    <select id="city_id" class="required city form-control" name="city_id" style="width:235px;"><?php echo cityList($city_id); ?></select>
                </div>
            </div>
            <div class="form-group">
                <label for="suburb_id" class="col-sm-2 control-label">Suburb<span class="redCol">* </span></label>
                <div class="col-sm-8">
                    <select id="suburb_id" class="required suburb form-control" name="suburb" style="width:235px;"><?php echo suburbList($city_id, $suburb_id); ?></select>
                    <input type="text" name="new suburb" id="sub_id" value="" class="form-control required" minlength="2" maxlength="50">
                </div>
            </div>
            <div class="form-group">
                <label for="title" class="col-sm-2 control-label">Rest Care Name<span class="redCol">*</span></label>
                <div class="col-sm-8">
                    <input type="text"  class="required form-control" name="title" id="title" minlength="2" maxlength="50" value="<?php echo isset($title) ? stripslashes($title) : ''; ?>"/>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-8">
                    <?php
                    if (key($_REQUEST) == 'editRestcare') {
                        ?>
                        <input type="submit" value="Update" name="update" class="submit_btn btn" src="<?php echo ADMIN_IMAGE; ?>submit.gif">

                        <?php
                    } else {
                        ?>
                        <input type="submit" value="Submit" name="submit" class="submit_btn btn">
                        <?php
                    }
                    ?>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#sub_id').hide();
        $('#sub_id').removeClass('required');
        $('input[type="submit"]').focus();

        $('#suburb_id').change(function() {
            var suburbId = $('#suburb_id').val();
            if (suburbId == '0') {
                $('#sub_id').show();
                $('#sub_id').addClass('required');
                $('#suburb_id').hide();
                $('#suburb_id').removeClass('required');
            } else {
                $('#sub_id').hide();
                $('#sub_id').removeClass('required');
            }
        });
        $('#city_id').change(function() {
            $('#sub_id').hide();
            $('#sub_id').removeClass('required');
            $('#suburb_id').show();
            $('#suburb_id').addClass('required');
        });

    });
    $('#addCity').validate();
</script>
