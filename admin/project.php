<?php
    include("../application_top.php");
     include(INCLUDE_PATH . "header.php");
?>
 <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                        <tr>
                            <td class="lt_nav" height="455" valign="top" width="17%">
                                <!-- left navigation -->
                                <?php include(INCLUDE_PATH . "left_navigation.php"); ?>
                                <!-- left navigation ends here -->
                            </td>
                            <td valign="top">
                                <!-- content area here -->
                                <div id="body_content">
                                    <?php
                                        switch (key($_REQUEST)) {
                                            case 'quote':
                                                include(MODULE_PATH . "project/quote.php");
                                                break;
                                            case 'project':
                                                include(MODULE_PATH . "project/project.php");
                                                break;
                                            case 'file':
                                                include(MODULE_PATH . "project/file.php");
                                                break;
                                            case 'adminQuote':
                                                include(MODULE_PATH . "project/admin_quote.php");
                                                break;
                                            case 'sprecord':
                                                include(MODULE_PATH . "project/sp_record.php");
                                                break;
                                            default:
                                                include(MODULE_PATH . "project/project.php");
                                                break;
                                        }
                                    ?>
                                </div>
                                <!-- content area ends here -->
                            </td>
                        </tr>
                    </tbody>
                </table>
