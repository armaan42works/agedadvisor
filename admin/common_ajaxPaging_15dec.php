<?php
$action = isset($_REQUEST['action']) ? $_REQUEST['action'] : '';
$blogId = isset($_REQUEST['blogId']) ? $_REQUEST['blogId'] : '';
$forumId = isset($_REQUEST['forumId']) ? $_REQUEST['forumId'] : '';
$search_input = isset($_REQUEST['search_input']) ? $_REQUEST['search_input'] : '';
$search_type = isset($_REQUEST['search_type']) ? $_REQUEST['search_type'] : '';
global $db;
switch ($action) {
    case 'blog':
        manage_blog($search_input, $search_type);
        break;
    case 'comment':
        manage_blogComments($id);
        break;
    case 'customer':
        manage_client($search_input, $search_type);
        break;
    case 'supplier':
        manage_supplier($search_type, $search_input);
    case 'supplierDetail':
        manage_supplier_detail($id);
        break;
    case 'clogo':
        manage_flogo($search_input, $search_type);
        break;
    case 'bprice':
        manage_bannerPrice();
        break;
    case 'comboprice':
        manage_combobannerPrice();
        break;
    case 'mprice':
        manage_membershipPrice();
        break;
    case 'artwork':
        manage_artWork();
        break;
    case 'partwork':
        manage_personalArtWork($id);
        break;
    case 'cat':
        manage_categories($search_input, $search_type);
        break;
    case 'forum':
        manage_forum($search_input, $search_type);
        break;
    case 'forumComments':
        manage_forumComments($forumId);
        break;
    case 'adsList':
        manage_adsList($search_input, $search_type);
        break;
    case 'slider':
        manage_sliderImage();
        break;
    case 'newsletter':
        manage_newsletter();
        break;
    case 'pages':
        manage_pages();
        break;
    case 'duration':
        manage_duration();
        break;
    case 'visit':
        manage_visit();
        break;
    case 'review':
        manage_reviews();
        break;
    case 'abusedreview':
        manage_abused_review();
        break;
    case 'service':
        manage_service($search_input, $search_type);
        break;
    case 'position':
        manage_job();
        break;
    case 'email':
        manage_email();
        break;
    case 'location':
        manage_location();
        break;
    case 'suburb':
        manage_suburb();
        break;
    case 'restcare':
        manage_restcare();
        break;
    case 'faqs':
        manage_faqs();
        break;
    case 'paymentapproval':
        manage_paymentapproval($search_input, $search_type);
        break;
    /* front end start here */
    default :
        test();
        break;
}

function test() {
    
}

function manage_blog($search_input, $search_type) {
    global $db;
    $condition = " where bg.deleted = 0 AND bg.type= 0 ";
    if (isset($search_type) && $search_input != '') {
        switch ($search_type) {
            case "title":
                $condition .= ' && bg.title like "%' . $search_input . '%" ';
                break;
            case "author":
                $condition .= ' && user.first_name like "%' . $search_input . '%" ';
                $condition .= ' && user.last_name like "%' . $search_input . '%" ';
                break;
            default :
                $condition .= ' && ( bg.title like "%' . $search_input . '%" OR user.name like "%' . $search_input . '%" )';
        }
    }

    $query = "SELECT bg.*, CONCAT(IFNULL(user.first_name,''),' ',IFNULL(user.last_name,'')) AS author  FROM " . _prefix("blogs") . " AS bg "
            . "  LEFT JOIN " . _prefix("users") . " AS user ON user.id=bg.created_by "
            . "  LEFT JOIN " . _prefix("blog_comments") . " AS cm ON cm.blog_id=bg.id "
            . " $condition GROUP BY bg.id ORDER BY bg.id DESC ";
    $res = $db->sql_query($query);
    $count = $db->sql_numrows($res);
    $i = 1;
    if (isset($_POST['pageId']) && !empty($_POST['pageId'])) {
        $id = $_POST['pageId'];
        $i = $i + PAGE_PER_NO * $id;
    } else {
        $id = '0';
        $i = 1;
    }
    $pageLimit = PAGE_PER_NO * $id;
    $query = "SELECT bg.*, CONCAT(IFNULL(user.first_name,''),' ',IFNULL(user.last_name,'')) AS author,user.user_type, count(cm.id) AS comment FROM " . _prefix("blogs") . " AS bg "
            . "  LEFT JOIN " . _prefix("users") . " AS user ON user.id=bg.created_by "
            . "  LEFT JOIN " . _prefix("blog_comments") . " AS cm ON cm.blog_id=bg.id AND cm.deleted=0 "
            . " $condition GROUP BY bg.id ORDER BY bg.id DESC limit " . $pageLimit . ',' . PAGE_PER_NO;

    $res = $db->sql_query($query);
    $data = $db->sql_fetchrowset($res);
    if ($count > 0) {
        $output = '<tr><th align="left" width="5%">S.No</th>
                                <th align="left" width="10%">Title</th>
                                <th align="left" width="30%">Content</th>
                                <th align="left" width="10%">Author</th>
                                <th align="left" width="5%">Comments</th>
                                <th align="left" width="10%">Date</th>
                                <th align="left" width="5%">Archive</th>
                                <th align="left" width="21%">Action</th>
                            </tr>';
        foreach ($data as $key => $record) {
            $status = ($record['status'] == 1) ? 'Active' : 'InActive';
            $archive = ($record['archive'] == 1) ? 'Archive' : 'Unarchive';
            $active_status = ($record['status'] == 1) ? ADMIN_IMAGE . 'activate.png' : ADMIN_IMAGE . 'deactivate.png';
            $archive_status = ($record['archive'] == 1) ? ' <i style="font-size:20px;" title="Archieved" class="fa fa-archive"></i>' : '<img src="' . ADMIN_IMAGE . 'activate.png' . '" title="' . $archive . '" />';
            $description = strip_tags($record['content']);
            $description = (strlen($description) > 60) ? substr($description, 0, 60) . '...' : $description;
            $img = '<img src="' . $active_status . '" title="' . $status . '" />';
            $img1 = $archive_status;
            $output .= '<tr class="row_' . $record['id'] . '" style="margin-top: 10px;">';
            $output .= '<td>' . $i . '.</td>';
            $output .= '<td>' . $record['title'] . '</td>';
            $output .= '<td>' . $description . '</td>';
//            if ($record['user_type'] == 1) {
            $output .= '<td>' . $record['author'] . '</td>';
//            } else {
//                $output .= '<td>Admin</td>';
//            }
            $output .= '<td><a style="font-size:20px;line-height:1.5em;" title="view comments" href="' . HOME_PATH_URL . 'blog.php?comment&id=' . $record['id'] . '"><i class="fa fa-comment"></i>' . $record['comment'] . '</a></td>';
            $output .= '<td>' . date('M d, Y', strtotime($record['created'])) . '</td>';
            $output .= '<td><span class="archive-' . $record['id'] . '" style="margin-right:5px;"><a href="javascript:void(0);" id="blogs--' . $record['id'] . '--' . $record['archive'] . '" class="archive">' . $img1 . '</a></span></td>';
            $output .= '<td><span class="status-' . $record['id'] . '" style="margin-right:5px;"><a href="javascript:void(0);" id="blogs-' . $record['id'] . '-' . $record['status'] . '" class="status">' . $img . '</a></span>'
                    . '<span style="margin-right:5px;"><a href=' . HOME_PATH_URL . 'blog.php?edit&id=' . md5($record['id']) . '><img src="' . ADMIN_IMAGE . 'li_edit.png" title="Edit"></a></span>'
                    . '<span><a href="javascript:void(0);" class="delete" id="del-blogs-' . $record['id'] . '"><img src="' . ADMIN_IMAGE . 'li_delete.png" title="Delete" ></a> </span>';
            $output.= '</td>';
            $output .= '</tr>';

            $i ++;
        }
        $pagingCount = $count;
        if ($pagingCount > 0) {
            $data = '';
            $paginationCount = getPagination($pagingCount);
            $colspan = 6;
            $output .= get_pagination_link($paginationCount, 'blog', $data, $colspan);
        }
    } else {
        $output = '<div class="norecord">No record Found</div>';
    }
    echo $output;

    die;
}

function manage_blogComments($blogId) {
    global $db;
    $blogId = isset($_POST['blogId']) && !empty($_POST['blogId']) ? $_POST['blogId'] : $blogId;
    $query = "SELECT id FROM " . _prefix("blog_comments") . " where deleted = 0 AND blog_id=" . $blogId;
    $res = $db->sql_query($query);
    $count = $db->sql_numrows($res);
    $i = 1;
    if (isset($_POST['pageId']) && !empty($_POST['pageId'])) {
        $id = $_POST['pageId'];
        $i = $i + PAGE_PER_NO * $id;
    } else {
        $id = '0';
        $i = 1;
    }
    $pageLimit = PAGE_PER_NO * $id;
    $query = "SELECT cm.*, CONCAT(IFNULL(user.first_name,''),' ',IFNULL(user.last_name,'')) author FROM " . _prefix("blog_comments") . " AS cm "
            . "  LEFT JOIN " . _prefix("users") . " AS user ON user.id = cm.commented_by "
            . " WHERE cm.deleted = 0 AND cm.blog_id = '" . $blogId . "' "
            . " ORDER BY cm.id DESC limit " . $pageLimit . ',' . PAGE_PER_NO;
    $res = $db->sql_query($query);
    $data = $db->sql_fetchrowset($res);
    if ($count > 0) {
        $output = '<tr><td colspan="6"><div class="breadcrumb send_btn" align="right"><a href="' . HOME_PATH_URL . 'blog.php?addComment&blogId=' . $blogId . '"> Add Comment </a></div></td></tr>';
        $output .= '<tr><th align="left" width="5%">S.No</th>
                                <th align="left" width="10%">Author</th>
                                <th align="left" width="20%">Comments</th>
                                <th align="left" width="10%">Date</th>
                                <th align="left" width="21%">Action</th>
                            </tr>';
        foreach ($data as $key => $record) {
            $status = ($record['status'] == 1) ? 'Active' : 'InActive';
            $active_status = ($record['status'] == 1) ? ADMIN_IMAGE . 'activate.png' : ADMIN_IMAGE . 'deactivate.png';
            $status = ($record['status'] == 1) ? 'Active' : 'InActive';
            $comment = strip_tags($record['comment']);
            $comment = (strlen($comment) > 60) ? substr($comment, 0, 60) . '...' : $comment;
            $img = '<img src="' . $active_status . '" title="' . $status . '" />';
            $output .= '<tr class="row_' . $record['id'] . '" style="margin-top: 10px;">';
            $output .= '<td>' . $i . '.</td>';
            if ($record['user_type'] == 1) {
                $output .= '<td>' . $record['author'] . '</td>';
            } else {
                $output .= '<td>Admin</td>';
            }
            $output .= '<td><a  class="blogComment" href="' . HOME_PATH_URL . 'popup.php?comment&id=' . md5($record['id']) . '">' . $comment . '</a></td>';
            $output .= '<td>' . date('M d, Y', strtotime($record['commented_on'])) . '</td>';
            $output .= '<td><span class="status-' . $record['id'] . '" style="margin-right:5px;"><a href="javascript:void(0);" id="blog_comments-' . $record['id'] . '-' . $record['status'] . '" class="status">' . $img . '</a></span>'
                    . '<span style="margin-right:5px;"><a href=' . HOME_PATH_URL . 'blog.php?editComment&commentId=' . $record['id'] . '><img src="' . ADMIN_IMAGE . 'li_edit.png" title="Edit"></a></span>'
                    . '<span><a href="javascript:void(0);" class="delete" id="del-blog_comments-' . $record['id'] . '"><img src="' . ADMIN_IMAGE . 'li_delete.png" title="Delete" ></a> </span>';
            $output.= '</td>';
            $output .= '</tr>';

            $i ++;
        }

        //  $paging_query = "SELECT cm.*, CONCAT(IFNULL(user.first_name,''),' ',IFNULL(user.last_name,'')) AS author FROM " . _prefix("blog_comments") . " AS cm "
//                . "  LEFT JOIN " . _prefix("users") . " AS user ON user.id = cm.commented_by "
//                . " WHERE cm.deleted = 0 ORDER BY cm.id DESC ";
        // $paging_res = $db->sql_query($paging_query);
        $pagingCount = $count;
        if ($pagingCount > 0) {
            $data = '&blogId=' . $blogId;
            $paginationCount = getPagination($pagingCount);
            $colspan = 6;
            $output .= get_pagination_link($paginationCount, 'comment', $data, $colspan);
        }
    } else {
        $output = '<div class="breadcrumb send_btn" align="right"><a href="' . HOME_PATH_URL . 'blog.php?addComment&blogId=' . $blogId . '"> Add Comment </a></div>';
        $output .= '<div class="norecord">No record Found</div>';
    }
    echo $output;
    ?>
    <script>
        $(".blogComment").colorbox({
            width: '600px',
            height: '500px;'
        });

    </script>
    <?php
    die;
}

function manage_client($search_input, $search_type) {
    $clientType = array('0' => 'Customer', '1' => 'Supplier');
    $condition = ' WHERE du.deleted = 0 && du.user_type = 0 && ';
    if (isset($search_type) && $search_input != '') {
        switch ($search_type) {
            case "name":
                $condition .= "du.name like '%$search_input%' && ";
                break;
            case "client_id":
                if (substr($search_input, 3) == '') {
                    $search_input = $search_input;
                } else {
                    $search_input = substr($search_input, 3);
                }
                $condition .= "(du.unique_id like '%" . $search_input . "%') && ";
                break;
            case "referred_by":
                $condition .= ' && ((dus.first_name like "%' . $search_input . '%" ) OR (dus.last_name like "%' . $search_input . '%" ))';
                break;

            default:
                break;
        }
    }

    $condition .= '1';
    global $db;
    $query = "SELECT du.id, CONCAT(IFNULL(du.first_name,''),' ',IFNULL(du.last_name,'')) AS name, du.user_type, du.email, du.created,du.unique_id ,du.status, CONCAT(IFNULL(dus.first_name,''),' ',IFNULL(dus.last_name,'')) as referralName , dc.name as countryName FROM " . _prefix("users") . " as du "
            . "LEFT JOIN " . _prefix("countries") . " as dc ON du.country_id = dc.id "
            . "LEFT JOIN " . _prefix("users") . " as dus ON dus.id = du.refferal_id "
            . "$condition ";
    $res = $db->sql_query($query);
    $count = $db->sql_numrows($res);
    $i = 1;
    if (isset($_POST['pageId']) && !empty($_POST['pageId'])) {
        $id = $_POST['pageId'];
        $i = $i + PAGE_PER_NO * $id;
    } else {
        $id = '0';
        $i = 1;
    }

    $pageLimit = PAGE_PER_NO * $id;
    $query = "SELECT du.id, CONCAT(IFNULL(du.first_name,''),' ',IFNULL(du.last_name,'')) AS name, du.user_type, du.email, du.validate ,du.created,du.unique_id ,du.status, CONCAT(IFNULL(dus.first_name,''),' ',IFNULL(dus.last_name,'')) AS referralName , dc.name as countryName FROM " . _prefix("users") . " as du "
            . "LEFT JOIN " . _prefix("countries") . " as dc ON du.country_id = dc.id "
            . "LEFT JOIN " . _prefix("users") . " as dus ON dus.unique_id = du.refferal_id "
            . "$condition ORDER BY du.id DESC limit " . $pageLimit . ',' . PAGE_PER_NO;
    $res = $db->sql_query($query);
    $data = $db->sql_fetchrowset($res);
    if ($count > 0) {
        $output = '<tr><th align="left" width="5%">S.No</th>
                                <th align="left" width="30%">Name</th>
                                <th align="left" width="20%">Date</th>
                                <th align="left" width="21%">Action</th>
                            </tr>';

        foreach ($data as $key => $record) {
            $time = date('Y-m-d H:i:s', strtotime($record['created']) + (60 * 60 * 7));
            $currentTime = date('Y-m-d H:i:s', strtotime(date('Ymdhis')));
            $clientId = $record['unique_id'] == '' ? 'N/A' : 'KAC' . $record['unique_id'];
            $referral = $record['referralName'] == '' ? 'N/A' : $record['referralName'];
            $status = ($record['status'] == 1) ? 'Active' : 'InActive';
            $active_status = ($record['status'] == 1) ? ADMIN_IMAGE . 'activate.png' : ADMIN_IMAGE . 'deactivate.png';
            $img = '<img src="' . $active_status . '" title="' . $status . '" />';
            $output .= '<tr class="row_' . $record['id'] . '" style="margin-top: 10px;">';
            $output .= '<td>' . $i . '.</td>';
            $output .= '<td><a  class="userprofile" href="' . HOME_PATH_URL . 'popup.php?profile&id=' . md5($record['id']) . '">' . $record['name'] . '</a></td>';
            $output .= '<td>' . date('M d, Y', strtotime($record['created'])) . '</td>';
            $output .= '<td><span class="status-' . $record['id'] . '" style="margin-right:5px;"><a href="javascript:void(0);" id="users-' . $record['id'] . '-' . $record['status'] . '" class="status">' . $img . '</a></span>'
                    . '<span style="margin-right:5px;"><a href=' . HOME_PATH_URL . 'user.php?editCustomer&id=' . md5($record['id']) . '><img src="' . ADMIN_IMAGE . 'li_edit.png" title="Edit"></a></span>'
                    . '<span><a href="javascript:void(0);" class="delete" id="del-users-' . $record['id'] . '"><img src="' . ADMIN_IMAGE . 'li_delete.png" title="Delete" ></a> </span>';
            if ($record['validate'] == 0 && (strtotime($currentTime) >= strtotime($time))) {
                $output .='<span class="loader-message"><form method="POST">'
                        . '<input type=hidden name="id" value="' . $record['id'] . '">'
                        . '<input type="submit" name="Send Mail" value="Verify" class="load" id="load-' . $record['id'] . '">'
                        . '</form><div id="loading-' . $record['id'] . '" style="display:none;" class="loading-img"><img src="' . ADMIN_IMAGE . 'loading.gif"></div></span>';
            } else if ($record['validate'] == 3) {
                $output .= '<br><span> Not Verified</span>';
            }
            $output.= '</td>';
            $output .= '</tr>';

            $i ++;
        }
        $paging_query = "SELECT du.id, CONCAT(IFNULL(du.first_name,''),' ',IFNULL(du.last_name,'')) AS name, du.user_type, du.email, du.created,du.unique_id ,du.status, CONCAT(IFNULL(dus.first_name,''),' ',IFNULL(dus.last_name,'')) AS referralName , dc.name as countryName FROM " . _prefix("users") . " as du "
                . "INNER JOIN " . _prefix("countries") . " as dc ON du.country_id = dc.id "
                . "LEFT JOIN " . _prefix("users") . " as dus ON dus.id = du.refferal_id "
                . "$condition ";
        $paging_res = $db->sql_query($paging_query);
        $pagingCount = $db->sql_numrows($paging_res);
        if ($pagingCount > 0) {
            $data = '';
            $paginationCount = getPagination($pagingCount);
            $colspan = 6;
            if (isset($search_type)) {
                $data .="&search_input=$search_input&search_type=$search_type";
            }
            $output .= get_pagination_link($paginationCount, 'client', $data, $colspan);
        }
    } else {
        $output = '<div class="norecord">No record Found</div>';
    }
    echo $output;
    ?>
    <script>
        $(".userprofile").colorbox({
            width: '600px',
            height: '500px;'
        });

    </script>
    <script>
        $(document).ready(function() {
            $('.load').click(function() {
                var value = $(this).attr('id').split('-');
                $('#loading-' + value[1]).show();
            });
        });


    </script>
    <?php
    die;
}

function manage_flogo() {
    global $db;
    $condition = " where deleted = 0 ";
    $query = "SELECT id, title, description, image, status, created_on FROM " . _prefix("company_logos") . "$condition";
    $res = $db->sql_query($query);
    $count = $db->sql_numrows($res);
    $i = 1;
    if (isset($_POST['pageId']) && !empty($_POST['pageId'])) {
        $id = $_POST['pageId'];
        $i = $i + PAGE_PER_NO * $id;
    } else {
        $id = '0';
        $i = 1;
    }

    $pageLimit = PAGE_PER_NO * $id;
    $query = "SELECT id, title, description, image, status, created_on FROM " . _prefix("company_logos") . "   "
            . "$condition ORDER BY id DESC limit " . $pageLimit . ',' . PAGE_PER_NO;
    $res = $db->sql_query($query);
    $data = $db->sql_fetchrowset($res);
    if ($count > 0) {
        $output = '<tr><th align="left" width="5%">S.No</th>
                                <th align="left" width="15%">Title</th>
                                <th align="left" width="30%">Description</th>
                                <th align="left" width="10%">Image</th>
                                <th align="left" width="10%">Created On</th>
                                <th align="left" width="10%">Action</th>
                            </tr>';
        foreach ($data as $key => $record) {
            $status = ($record['status'] == 1) ? 'Active' : 'InActive';
            $active_status = ($record['status'] == 1) ? ADMIN_IMAGE . 'activate.png' : ADMIN_IMAGE . 'deactivate.png';
            $description = strip_tags($record['description']);
            $description = (strlen($description) > 60) ? substr($description, 0, 60) . '...' : $description;
            $img = '<img src="' . $active_status . '" title="' . $status . '" />';
            $output .= '<tr class="row_' . $record['id'] . '" style="margin-top: 10px;">';
            $output .= '<td>' . $i . '.</td>';
            $output .= '<td>' . $record['title'] . '</td>';
            $output .= '<td>' . $record['description'] . '</td>';
            $output .= '<td><img  title="feature" src="' . MAIN_PATH . '/files/company_logo/thumb_' . $record['image'] . '"></td>';
            $output .= '<td>' . date('M d, Y', strtotime($record['created_on'])) . '</td>';
            $output .= '<td><span class="status-' . $record['id'] . '" style="margin-right:5px;"><a href="javascript:void(0);" id="company_logos-' . $record['id'] . '-' . $record['status'] . '" class="status">' . $img . '</a></span>'
                    . '<span style="margin-right:5px;"><a href=' . HOME_PATH_URL . 'clogo.php?edit&id=' . md5($record['id']) . '><img src="' . ADMIN_IMAGE . 'li_edit.png" title="Edit"></a></span>'
                    . '<span><a href="javascript:void(0);" class="delete" id="del-company_logos-' . $record['id'] . '"><img src="' . ADMIN_IMAGE . 'li_delete.png" title="Delete" ></a> </span>';
            $output.= '</td>';
            $output .= '</tr>';

            $i ++;
        }
        $paging_query = "SELECT id, title, description, image, created_on FROM " . _prefix("company_logos") . "   " . "$condition ";
        $paging_res = $db->sql_query($paging_query);
        $pagingCount = $db->sql_numrows($paging_res);
        if ($pagingCount > 0) {
            $data = '';
            $paginationCount = getPagination($pagingCount);
            $colspan = 6;
            if (isset($search_type)) {
                $data .="&search_input=$search_input&search_type=$search_type";
            }
            $output .= get_pagination_link($paginationCount, 'clogo', $data, $colspan);
        }
    } else {
        $output = '<div class="norecord">No record Found</div>';
    }
    echo $output;
    die;
}

function manage_bannerPrice() {
    global $db;
    $condition = " where deleted = 0 ";
    $query = "SELECT id, price, description, status, created_on FROM " . _prefix("space_prices") . "$condition";
    $res = $db->sql_query($query);
    $count = $db->sql_numrows($res);
    $i = 1;
//    if (isset($_POST['pageId']) && !empty($_POST['pageId'])) {
//        $id = $_POST['pageId'];
//        $i = $i + PAGE_PER_NO * $id;
//    } else {
//        $id = '0';
////            $i = $count;
//        $i = 1;
//    }
    // $pageLimit = PAGE_PER_NO * $id;
    $query = "SELECT id, no_of_day, price, description, status, created_on FROM " . _prefix("space_prices") . "   "
            . "$condition ORDER BY id ASC limit 0,1";
    $res = $db->sql_query($query);
    $data = $db->sql_fetchrowset($res);
    if ($count > 0) {
        $output = '<tr><th align="left" width="5%">S.No</th>
                                <th align="left" width="10%">Day</th>
                                <th align="left" width="30%">Description</th>
                                <th align="left" width="10%">Price</th>
                                <th align="left" width="10%">Created On</th>
                                <th align="left" width="10%">Action</th>
                            </tr>';
        foreach ($data as $key => $record) {
            $status = ($record['status'] == 1) ? 'Active' : 'InActive';
            $active_status = ($record['status'] == 1) ? ADMIN_IMAGE . 'activate.png' : ADMIN_IMAGE . 'deactivate.png';
            $description = strip_tags($record['description']);
            $description = (strlen($description) > 40) ? substr($description, 0, 40) . '...' : $description;
            $img = '<img src="' . $active_status . '" title="' . $status . '" />';
            $output .= '<tr class="row_' . $record['id'] . '" style="margin-top: 10px;">';
            $output .= '<td>' . $i . '.</td>';
            $output .= '<td>' . $record['no_of_day'] . '</td>';
            $output .= '<td>' . $description . '</td>';
            $output .= '<td>' . $record['price'] . '</td>';
            $output .= '<td>' . date('M d, Y', strtotime($record['created_on'])) . '</td>';
            $output .= '<td><span class="status-' . $record['id'] . '" style="margin-right:5px;"><a href="javascript:void(0);" id="b_prices-' . $record['id'] . '-' . $record['status'] . '" class="status">' . $img . '</a></span>'
                    . '<span style="margin-right:5px;"><a href=' . HOME_PATH_URL . 'price.php?edit&id=' . md5($record['id']) . '><img src="' . ADMIN_IMAGE . 'li_edit.png" title="Edit"></a></span>';
            $output.= '</td>';
            $output .= '</tr>';

            $i++;
        }
//        $paging_query = "SELECT id, price, description, status, created_on FROM " . _prefix("space_prices") . "   " . "$condition ";
//        $paging_res = $db->sql_query($paging_query);
//        $pagingCount = $db->sql_numrows($paging_res);
//        if ($pagingCount > 0) {
//            $data = '';
//            $paginationCount = getPagination($pagingCount);
//            $colspan = 6;
//           // $output .= get_pagination_link($paginationCount, 'bprice', $data, $colspan);
//        }
    } else {
        $output = '<div class="norecord">No record Found</div>';
    }
    echo $output;
    die;
}

function manage_combobannerPrice() {
    global $db;
    $condition = " where deleted = 0 and id != 1 ";
    $query = "SELECT id, price, description, status, created_on FROM " . _prefix("space_prices") . "$condition";
    $res = $db->sql_query($query);
    $count = $db->sql_numrows($res);
    $i = 1;
    if (isset($_POST['pageId']) && !empty($_POST['pageId'])) {
        $id = $_POST['pageId'];
        $i = $i + PAGE_PER_NO * $id;
    } else {
        $id = '0';
//            $i = $count;
        $i = 1;
    }

    $pageLimit = PAGE_PER_NO * $id;
    echo $query = "SELECT id, no_of_day, price, description, status, created_on FROM " . _prefix("space_prices") . "   "
    . "$condition ORDER BY id DESC limit " . $pageLimit . ',' . PAGE_PER_NO;
//   die;
    $res = $db->sql_query($query);
    $data = $db->sql_fetchrowset($res);
    if ($count > 0) {
        $output = '<tr><th align="left" width="5%">S.No</th>
                                <th align="left" width="10%">Days</th>
                                <th align="left" width="20%">Description</th>
                                <th align="left" width="10%">Price</th>
                                <th align="left" width="10%">Created On</th>
                                <th align="left" width="21%">Action</th>
                            </tr>';
        foreach ($data as $key => $record) {
            $status = ($record['status'] == 1) ? 'Active' : 'InActive';
            $active_status = ($record['status'] == 1) ? ADMIN_IMAGE . 'activate.png' : ADMIN_IMAGE . 'deactivate.png';
            $description = strip_tags($record['description']);
            $description = (strlen($description) > 40) ? substr($description, 0, 40) . '...' : $description;
            $img = '<img src="' . $active_status . '" title="' . $status . '" />';
            $output .= '<tr class="row_' . $record['id'] . '" style="margin-top: 10px;">';
            $output .= '<td>' . $i . '.</td>';
            $output .= '<td>' . $record['no_of_day'] . '</td>';
            $output .= '<td>' . $description . '</td>';
            $output .= '<td>' . $record['price'] . '</td>';
            $output .= '<td>' . date('M d, Y', strtotime($record['created_on'])) . '</td>';
            $output .= '<td><span class="status-' . $record['id'] . '" style="margin-right:5px;"><a href="javascript:void(0);" id="b_prices-' . $record['id'] . '-' . $record['status'] . '" class="status">' . $img . '</a></span>'
                    . '<span style="margin-right:5px;"><a href=' . HOME_PATH_URL . 'price.php?edit&id=' . md5($record['id']) . '><img src="' . ADMIN_IMAGE . 'li_edit.png" title="Edit"></a></span>'
                    . '<span><a href="javascript:void(0);" class="delete" id="del-space_prices-' . $record['id'] . '"><img src="' . ADMIN_IMAGE . 'li_delete.png" title="Delete" ></a> </span>';
            $output.= '</td>';
            $output .= '</tr>';

            $i++;
        }
        $paging_query = "SELECT id, price, description, status, created_on FROM " . _prefix("space_prices") . "   " . "$condition ";
        $paging_res = $db->sql_query($paging_query);
        $pagingCount = $db->sql_numrows($paging_res);
        if ($pagingCount > 0) {
            $data = '';
            $paginationCount = getPagination($pagingCount);
            $colspan = 6;
            $output .= get_pagination_link($paginationCount, 'comboprice', $data, $colspan);
        }
    } else {
        $output = '<div class="norecord">No record Found</div>';
    }
    echo $output;
    die;
}

function manage_membershipPrice() {
    $type = array('Basic', 'Business', 'Premium');
    global $db;
    $condition = " where deleted = 0 ";
    $query = "SELECT id, price,title,duration, description, status, number_of_review_response,  created_on FROM " . _prefix("membership_prices") . "$condition";
    $res = $db->sql_query($query);
    $count = $db->sql_numrows($res);
    $i = 1;
    if (isset($_POST['pageId']) && !empty($_POST['pageId'])) {
        $id = $_POST['pageId'];
        $i = $i + PAGE_PER_NO * $id;
    } else {
        $id = '0';
        $i = 1;
    }

    $pageLimit = PAGE_PER_NO * $id;
    $query = "SELECT id, price,title,type,discount, duration, description, status, number_of_review_response,  created_on FROM " . _prefix("membership_prices") . "   "
            . "$condition ORDER BY id ASC limit " . $pageLimit . ',' . PAGE_PER_NO;
    $res = $db->sql_query($query);
    $data = $db->sql_fetchrowset($res);
    if ($count > 0) {
        $output = '<tr><th align="left" width="5%">S.No</th>
                                <th align="left" width="10%">Title</th>
                                <th align="left" width="20%">Description</th>
                                <th align="left" width="10%">Price</th>
                                <th align="left" width="8%">Duration (Month)</th>
				<th align="left" width="8%">Review Response</th>
                                <th align="left" width="8%">Upgrade Discount Percentage</th>
                                <th align="left" width="8%">Created On</th>
                                <th align="left" width="8%">Action</th>
                            </tr>';
        foreach ($data as $key => $record) {
            $status = ($record['status'] == 1) ? 'Active' : 'InActive';
            $active_status = ($record['status'] == 1) ? ADMIN_IMAGE . 'activate.png' : ADMIN_IMAGE . 'deactivate.png';
            $img = '<img src="' . $active_status . '" title="' . $status . '" />';
            $description = strip_tags($record['description']);
            $description = (strlen($description) > 40) ? substr($description, 0, 40) . '...' : $description;
            $output .= '<tr class="row_' . $record['id'] . '" style="margin-top: 10px;">';
            $output .= '<td>' . $i . '.</td>';
            //$output .= '<td>' . ($type[$record['type']] ? $type[$record['type']] : ' ') . '</td>';
            $output .= '<td>' . $record['title'] . '</td>';
            $output .= '<td>' . $description . '</td>';
            $output .= '<td>' . $record['price'] . '</td>';
            $output .= '<td>' . $record['duration'] . '</td>';
            $output .= '<td>' . $record['number_of_review_response'] . '</td>';
            $output .= '<td>' . $record['discount'] . '</td>';
            $output .= '<td>' . date('M d, Y', strtotime($record['created_on'])) . '</td>';
            $output .= '<td><span class="status-' . $record['id'] . '" style="margin-right:5px;"><a href="javascript:void(0);" id="m_prices-' . $record['id'] . '-' . $record['status'] . '" class="status">' . $img . '</a></span>'
                    . '<span style="margin-right:5px;"><a href=' . HOME_PATH_URL . 'price.php?medit&id=' . $record['id'] . '><img src="' . ADMIN_IMAGE . 'li_edit.png" title="Edit"></a></span>';
//                        . '<span><a href="javascript:void(0);" class="delete" id="del-membership_prices-' . $record['id'] . '"><img src="' . ADMIN_IMAGE . 'li_delete.png" title="Delete" ></a> </span>';
            $output.= '</td>';
            $output .= '</tr>';

            $i ++;
        }
        $paging_query = "SELECT id, price, description, duration, status, created_on FROM " . _prefix("membership_prices") . "   " . "$condition ";
        $paging_res = $db->sql_query($paging_query);
        $pagingCount = $db->sql_numrows($paging_res);
        if ($pagingCount > 0) {
            $data = '';
            $paginationCount = getPagination($pagingCount);
            $colspan = 6;
            $output .= get_pagination_link($paginationCount, 'mprice', $data, $colspan);
        }
    } else {
        $output = '<div class="norecord">No record Found</div>';
    }
    echo $output;
    ?>
    <script>
        $(".userprofile").colorbox({
            width: '600px',
            height: '500px;'
        });

    </script>
    <script>
        $(document).ready(function() {
            $('.load').click(function() {
                var value = $(this).attr('id').split('-');
                $('#loading-' + value[1]).show();
            });
        });


    </script>
    <?php
    die;
}

function manage_artWork() {
    global $db;
    $condition = " where deleted = 0 ";
    $query = "SELECT id  FROM " . _prefix("artwork_users") . "$condition";
    $res = $db->sql_query($query);
    $count = $db->sql_numrows($res);
    $i = 1;
    if (isset($_POST['pageId']) && !empty($_POST['pageId'])) {
        $id = $_POST['pageId'];
        $i = $i + PAGE_PER_NO * $id;
    } else {
        $id = '0';
        $i = 1;
    }

    $pageLimit = PAGE_PER_NO * $id;
    $query = "SELECT aUsers.* , CONCAT(IFNULL(users.first_name,''),'',IFNULL(users.last_name,'')) AS user_name  FROM " . _prefix("artwork_users") . " AS aUsers   "
            . "LEFT JOIN " . _prefix("users") . " AS users ON  aUsers.user_id= users.id "
            . "where aUsers.deleted=0 ORDER BY aUsers.id DESC limit " . $pageLimit . ',' . PAGE_PER_NO;
    $res = $db->sql_query($query);
    $data = $db->sql_fetchrowset($res);
    if ($count > 0) {
        $output = '<tr><th align="left" width="5%">S.No</th>
                                <th align="left" width="20%">User Name</th>
                                <th align="left" width="20%">View Requests</th>
                                <th align="left" width="15%">Total Requested</th>
                                <th align="left" width="10%">Total Completed</th>
                                <th align="left" width="10%">Pending</th>
                                <th align="left" width="10%">Last Requested</th>
                                <th align="left" width="10%">Action</th>
                            </tr>';
        foreach ($data as $key => $record) {
            $status = ($record['status'] == 1) ? 'Active' : 'InActive';
            $active_status = ($record['status'] == 1) ? ADMIN_IMAGE . 'activate.png' : ADMIN_IMAGE . 'deactivate.png';
            $img = '<img src="' . $active_status . '" title="' . $status . '" />';
            $lastChanges = ($record['modified'] == '0000-00-00 00:00:00') ? date('M d, Y', strtotime($record['created'])) : date('M d, Y', strtotime($record['modified']));
            $output .= '<tr class="row_' . $record['id'] . '" style="margin-top: 10px;">';
            $output .= '<td>' . $i . '.</td>';
            $output .= '<td>' . $record['user_name'] . '</td>';
            $output .= '<td><a href="' . HOME_PATH_URL . 'artwork.php?partwork&id=' . md5($record['id']) . '"><i class="fa fa-envelope"></i>&nbsp;View</a></td>';
            $output .= '<td>' . $record['total_requested'] . '</td>';
            $output .= '<td>' . $record['total_completed'] . '</td>';
            $output .= '<td>' . (($record['total_requested'] - $record['total_requested']) >= 0 ? ($record['total_requested'] - $record['total_requested']) : 0) . '</td>';
            $output .= '<td>' . $lastChanges . '</td>';
            $output .= '<td><span class="status-' . $record['id'] . '" style="margin-right:5px;"><a href="javascript:void(0);" id="artwork_users-' . $record['id'] . '-' . $record['status'] . '" class="status">' . $img . '</a></span>'
                    . '<span><a href="javascript:void(0);" class="delete" id="del-artwork_users-' . $record['id'] . '"><img src="' . ADMIN_IMAGE . 'li_delete.png" title="Delete" ></a> </span>';
            $output.= '</td>';
            $output .= '</tr>';

            $i ++;
        }
        $paging_query = "SELECT id, user_id, artw_user_id, client_given, admin_given, client_description, admin_description, expected_date, admin_expected_date, status, created  FROM " . _prefix("artwork_users") . "   " . "$condition ";
        $paging_res = $db->sql_query($paging_query);
        $pagingCount = $db->sql_numrows($paging_res);
        if ($pagingCount > 0) {
            $data = '';
            $paginationCount = getPagination($pagingCount);
            $colspan = 6;
            $output .= get_pagination_link($paginationCount, 'client', $data, $colspan);
        }
    } else {
        $output = '<div class="norecord">No record Found</div>';
    }
    echo $output;
    die;
}

function manage_personalArtWork($rid) {
    global $db;
    $condition = " where aUsers.deleted = 0 AND md5(aUsers.artw_user_id)='" . $rid . "' ";
    if (isset($search_type) && $search_input != '') {
        switch ($search_type) {
            case "name":
                $condition .= ' && users.first_name like "%' . $search_input . 'OR users.last_name like "%' . $search_input . '%" ';
                break;
            default :
                $condition .= ' && users.first_name like "%' . $search_input . 'OR users.last_name like "%' . $search_input . '%" ';
        }
    }

    $query = "SELECT aUsers.* , CONCAT(IFNULL(users.first_name,''),'',IFNULL(users.last_name,'')) AS user_name  FROM " . _prefix("artworks") . " AS aUsers   "
            . "LEFT JOIN " . _prefix("users") . " AS users ON  aUsers.user_id= users.id "
            . "$condition ORDER BY aUsers.id DESC";
    $res = $db->sql_query($query);
    $count = $db->sql_numrows($res);
    $i = 1;
    if (isset($_POST['pageId']) && !empty($_POST['pageId'])) {
        $id = $_POST['pageId'];
        $i = $i + PAGE_PER_NO * $id;
    } else {
        $id = '0';
        $i = 1;
    }
    $pageLimit = PAGE_PER_NO * $id;
    $query = "SELECT aUsers.* , CONCAT(IFNULL(users.first_name,''),'',IFNULL(users.last_name,'')) AS user_name  FROM " . _prefix("artworks") . " AS aUsers   "
            . "LEFT JOIN " . _prefix("users") . " AS users ON  aUsers.user_id= users.id "
            . "$condition ORDER BY aUsers.id DESC limit " . $pageLimit . ',' . PAGE_PER_NO;
    $res = $db->sql_query($query);
    $data = $db->sql_fetchrowset($res);
    if ($count > 0) {
        $output = '<tr><th align="left" width="5%">S.No</th>
                                <th align="left" width="10%">Title</th>
                                <th align="left" width="20%">Client Description</th>
                                <th align="left" width="15%">Client Expected Date</th>
                                <th align="left" width="15%">Client Given Artwork</th>
                                <th align="left" width="15%">Admin Expected Date</th>
                                <th align="left" width="15%">Admin Given Artwork</th>
                                <th align="left" width="20%">Admin Description</th>
                                <th align="left" width="10%">Status</th>
                                <th align="left" width="10%">Reply & upload</th>
                            </tr>';
        foreach ($data as $key => $record) {
            $verified_status = ($record['approve_status'] == 1) ? 'Completed' : 'Requested';
            // $active_status = ($record['approve_status'] == 1) ? ADMIN_IMAGE . 'activate.png' : ADMIN_IMAGE . 'deactivate.png';
            //$img = '<img src="' . $active_status . '" title="' . $status . '" />';
            $verified = ($record['approve_status'] == 1) ? ADMIN_IMAGE . 'activate.png' : ADMIN_IMAGE . 'deactivate.png';
            $img1 = $verified_status . ' <img src="' . $verified . '" title="' . $verified_status . '" />';
            $cDescription = strip_tags($record['client_description']);
            $cDescription = (strlen($cDescription) > 40) ? substr($cDescription, 0, 40) . '...' : $cDescription;
            $expectedDate = ($record['expected_date'] != '0000-00-00 00:00:00') ? date('M d, Y', strtotime($record['expected_date'])) : 'N/A';
            $expectedDateAdmin = ($record['admin_expected_date'] != '0000-00-00 00:00:00') ? date('M d, Y', strtotime($record['admin_expected_date'])) : 'N/A';

            $output .= '<tr class="row_' . $record['id'] . '" style="margin-top: 10px;">';
            $output .= '<td>' . $i . '.</td>';
            $output .= '<td>' . $record['title'] . '</td>';
            $output .= '<td>' . $cDescription . '</td>';
            $output .= '<td>' . $expectedDate . '</td>';
//                $output .= '<td>' . $record['client_given'] . '</td>';
            $varC = DOCUMENT_PATH . 'admin/files/artwork/' . $record['client_given'];
            if (file_exists($varC)) {
                //$output .= '<td><img src="admin/files/artwork/thumb_' . $record['client_given'] . '"/></td>';
                $output .=!empty($record['client_given']) ? '<td><a  class="image" href="' . HOME_PATH_URL . 'popup.php?client_given&id=' . md5($record['id']) . '"><img  title="View Image" src="' . MAIN_PATH . '/files/artwork/thumb_' . $record['client_given'] . '"></a><br/><br/><a href="' . MAIN_PATH . '/download.php?type=artwork&file=' . $record['client_given'] . '"><i class="fa fa-download"></i>&nbsp;Download</a></td>' : '<td>N/A</td>';
            } else {
                $output .='<td>N/A</td>';
            }
            $output .= '<td>' . $expectedDateAdmin . '</td>';
            $varA = DOCUMENT_PATH . 'admin/files/artwork/' . $record['admin_returned'];
            if (file_exists($varA)) {
                $output .=!empty($record['admin_returned']) ? '<td><img src="' . HOME_PATH . 'admin/files/artwork/thumb_' . $record['admin_returned'] . '"></td>' : '<td>N/A</td>';
//                $output .= '<td><img src="' . HOME_PATH . 'admin/files/artwork/thumb_' . $record['admin_returned'] . '"></td>';
            } else {
                $output .='<td>N/A</td>';
            }
//                <!--$output .= '<td>' . $record['admin_returned'] . '</td>';
            $output .= '<td>' . $record['admin_description'] . '</td>';
            $output .= '<td>' . $verified_status . '</td>';
            //$output .= '<td> <span class="verified-' . $record['id'] . '" style="margin-right:5px;"><a href="javascript:void(0);" id="artworks--' . $record['id'] . '--' . $record['approve_status'] . '" class="verified">' . $img1 . '</a></span></td>';
            $output .= '<td><a href="' . HOME_PATH_URL . 'artwork.php?reply&id=' . md5($record['id']) . '&pid=' . $rid . '"><img src="' . ADMIN_IMAGE . 'li_edit.png" title="Edit"></a></td>';
            $output .= '</tr>';

            $i ++;
        }
        $paging_query = "SELECT id, user_id, artw_user_id, client_given, admin_returned , client_description, admin_description, expected_date, admin_expected_date, status, created  FROM " . _prefix("artworks") . "   " . "Where deleted=0 AND md5(artw_user_id)='" . $rid . "'";
        $paging_res = $db->sql_query($paging_query);
        $pagingCount = $db->sql_numrows($paging_res);
        if ($pagingCount > 0) {
            $data = '';
            $paginationCount = getPagination($pagingCount);
            $colspan = 6;
            $output .= get_pagination_link($paginationCount, 'partwork', $data, $colspan);
        }
    } else {
        $output = '<div class="norecord">No record Found</div>';
    }
    echo $output;
    ?>
    <script>
        $(".image").colorbox({
            width: '600px',
            height: '500px;'
        });

    </script><?php
    die;
}

function manage_forum($search_input, $search_type) {
    global $db;
    $condition = " where bg.deleted = 0 AND bg.type= 1 ";
    if (isset($search_type) && $search_input != '') {
        switch ($search_type) {
            case "title":
                $condition .= ' AND bg.title like "%' . $search_input . '%" ';
                break;
            case "author":
                if ($search_input == 'Admin') {
                    $condition .= ' AND cm.user_type =0 ';
                } else {
                    $condition .= ' AND user.first_name like "%' . $search_input . 'OR user.last_name like "%' . $search_input . '%" ';
                }
                break;
            default :

                if ($search_input == 'Admin') {
                    $condition .= ' AND ( bg.title like "%' . $search_input . '%" OR cm.user_type=0 )';
                } else {
                    $condition .= ' AND ( bg.title like "%' . $search_input . '%" OR user.first_name like "%' . $search_input . '%" OR user.last_name like "%' . $search_input . '%")';
                }
        }
    }

    $query = "SELECT bg.*, CONCAT(IFNULL(user.first_name,''),' ',IFNULL(user.last_name,'')) AS author, count(cm.id) AS comment FROM " . _prefix("blogs") . " AS bg "
            . "  LEFT JOIN " . _prefix("users") . " AS user ON user.id=bg.created_by "
            . "  LEFT JOIN " . _prefix("blog_comments") . " AS cm ON cm.blog_id=bg.id "
            . " $condition GROUP BY bg.id ORDER BY bg.id DESC ";
//        prd($query);
    $res = $db->sql_query($query);
    $count = $db->sql_numrows($res);

    $i = 1;
    if (isset($_POST['pageId']) && !empty($_POST['pageId'])) {
        $id = $_POST['pageId'];
        $i = $i + PAGE_PER_NO * $id;
    } else {
        $id = '0';
        $i = 1;
    }
    $pageLimit = PAGE_PER_NO * $id;
    $query = "SELECT bg.*, CONCAT(IFNULL(user.first_name,''),' ',IFNULL(user.last_name,'')) AS author,user.user_type ,count(cm.id) AS comment FROM " . _prefix("blogs") . " AS bg "
            . "  LEFT JOIN " . _prefix("users") . " AS user ON user.id=bg.created_by "
            . "  LEFT JOIN " . _prefix("blog_comments") . " AS cm ON cm.blog_id=bg.id AND cm.deleted = 0 "
            . " $condition GROUP BY bg.id ORDER BY bg.id DESC limit " . $pageLimit . ',' . PAGE_PER_NO;
    $res = $db->sql_query($query);
    $data = $db->sql_fetchrowset($res);
    if ($count > 0) {
        $output = '<tr><th align="left" width="5%">S.No</th>
                                <th align="left" width="10%">Title</th>
                                <th align="left" width="30%">Content</th>
                                <th align="left" width="10%">Author</th>
                                <th align="left" width="5%">Comments</th>
                                <th align="left" width="10%">Date</th>
                                <th align="left" width="5%">Archive</th>
                                <th align="left" width="21%">Action</th>
                            </tr>';
        foreach ($data as $key => $record) {
            $status = ($record['status'] == 1) ? 'Active' : 'InActive';
            $archive = ($record['archive'] == 1) ? 'Archive' : 'Unarchive';
            $active_status = ($record['status'] == 1) ? ADMIN_IMAGE . 'activate.png' : ADMIN_IMAGE . 'deactivate.png';
            $archive_status = ($record['archive'] == 1) ? ' <i style="font-size:20px;" title="Archieved" class="fa fa-archive"></i>' : '<img src="' . ADMIN_IMAGE . 'activate.png' . '" title="' . $archive . '" />';
            $description = strip_tags($record['content']);
            $description = (strlen($description) > 40) ? substr($description, 0, 40) . '...' : $description;
            $img = '<img src="' . $active_status . '" title="' . $status . '" />';
            $img1 = $archive_status;
            $output .= '<tr class="row_' . $record['id'] . '" style="margin-top: 10px;">';
            $output .= '<td>' . $i . '.</td>';
            $output .= '<td>' . $record['title'] . '</td>';
            $output .= '<td>' . $description . '</td>';
            if ($record['user_type'] == 1) {
                $output .= '<td>' . $record['author'] . '</td>';
            } else {
                $output .= '<td>Admin</td>';
            }
            $output .= '<td><a style="font-size:20px;line-height:1.5em;" title="view comments"  href="' . HOME_PATH_URL . 'forum.php?comment&id=' . ($record['id']) . '"><i class="fa fa-comment"></i>' . $record['comment'] . '</a></td>';
            $output .= '<td>' . date('M d, Y', strtotime($record['created'])) . '</td>';
            $output .= '<td><span class="archive-' . $record['id'] . '" style="margin-right:5px;"><a href="javascript:void(0);" id="blogs--' . $record['id'] . '--' . $record['archive'] . '" class="archive">' . $img1 . '</a></span></td>';
            $output .= '<td><span class="status-' . $record['id'] . '" style="margin-right:5px;"><a href="javascript:void(0);" id="blogs-' . $record['id'] . '-' . $record['status'] . '" class="status">' . $img . '</a></span>'
                    . '<span style="margin-right:5px;"><a href=' . HOME_PATH_URL . 'forum.php?edit&id=' . md5($record['id']) . '><img src="' . ADMIN_IMAGE . 'li_edit.png" title="Edit"></a></span>'
                    . '<span><a href="javascript:void(0);" class="delete" id="del-blogs-' . $record['id'] . '"><img src="' . ADMIN_IMAGE . 'li_delete.png" title="Delete" ></a> </span>';
            $output.= '</td>';
            $output .= '</tr>';

            $i ++;
        }

        $paging_query = "SELECT bg.*, CONCAT(IFNULL(user.first_name,''),' ',IFNULL(user.last_name,'')) AS author, count(cm.id) AS comment FROM " . _prefix("blogs") . " AS bg "
                . "  LEFT JOIN " . _prefix("users") . " AS user ON user.id=bg.created_by "
                . "  LEFT JOIN " . _prefix("blog_comments") . " AS cm ON cm.blog_id=bg.id "
                . "WHERE bg.deleted=0 AND bg.type= 1 GROUP BY bg.id ORDER BY bg.id DESC ";

        $paging_res = $db->sql_query($paging_query);
        $pagingCount = $db->sql_numrows($paging_res);
        if ($pagingCount > 0) {
            $data = '';
            $paginationCount = getPagination($pagingCount);
            $colspan = 6;
            $output .= get_pagination_link($paginationCount, 'forum', $data, $colspan);
        }
    } else {
        $output = '<div class="norecord">No record Found</div>';
    }
    echo $output;

    die;
}

function manage_forumComments($forumId) {
    global $db;
    $forumId = isset($_POST['forumId']) && !empty($_POST['forumId']) ? $_POST['forumId'] : $forumId;
    $query = "SELECT id FROM " . _prefix("blog_comments") . " where deleted = 0 AND blog_id=" . $forumId;
    $res = $db->sql_query($query);
    $count = $db->sql_numrows($res);
    $i = 1;
    if (isset($_POST['pageId']) && !empty($_POST['pageId'])) {
        $id = $_POST['pageId'];
        $i = $i + PAGE_PER_NO * $id;
    } else {
        $id = '0';
        $i = 1;
    }
    $pageLimit = PAGE_PER_NO * $id;
    $query = "SELECT cm.*, CONCAT(IFNULL(user.first_name,''),' ',IFNULL(user.last_name,'')) AS author,user.user_type FROM " . _prefix("blog_comments") . " AS cm "
            . "  LEFT JOIN " . _prefix("users") . " AS user ON user.id = cm.commented_by "
            . " WHERE cm.deleted = 0 AND (cm.blog_id)= '" . $forumId . "' "
            . " ORDER BY cm.id DESC limit " . $pageLimit . ',' . PAGE_PER_NO;
    $res = $db->sql_query($query);
    $data = $db->sql_fetchrowset($res);
//        prd($data);
    if ($count > 0) {
        $output = '<tr><td colspan="6"><div class="breadcrumb send_btn" align="right"><a href="' . HOME_PATH_URL . 'forum.php?addComment&forumId=' . $forumId . '"> Add Comment </a></div></td></tr>';
        $output .= '<tr><th align="left" width="5%">S.No</th>
                                <th align="left" width="10%">Author</th>
                                <th align="left" width="20%">Comments</th>
                                <th align="left" width="10%">Date</th>
                                <th align="left" width="21%">Action</th>
                            </tr>';
        foreach ($data as $key => $record) {
            $status = ($record['status'] == 1) ? 'Active' : 'InActive';
            $active_status = ($record['status'] == 1) ? ADMIN_IMAGE . 'activate.png' : ADMIN_IMAGE . 'deactivate.png';
            $status = ($record['status'] == 1) ? 'Active' : 'InActive';
            $comment = strip_tags($record['comment']);
            $comment = (strlen($comment) > 40) ? substr($comment, 0, 40) . '...' : $comment;
            $img = '<img src="' . $active_status . '" title="' . $status . '" />';
            $output .= '<tr class="row_' . $record['id'] . '" style="margin-top: 10px;">';
            $output .= '<td>' . $i . '.</td>';
            if ($record['user_type'] == 1) {
                $output .= '<td>' . $record['author'] . '</td>';
            } else {
                $output .= '<td>Admin</td>';
            }
//                $output .= '<td>' . $comments . '</td>';
            $output .= '<td><a  class="forumComment" href="' . HOME_PATH_URL . 'popup.php?comment&id=' . md5($record['id']) . '">' . $comment . '</a></td>';
            $output .= '<td>' . date('M d, Y', strtotime($record['commented_on'])) . '</td>';
            $output .= '<td><span class="status-' . $record['id'] . '" style="margin-right:5px;"><a href="javascript:void(0);" id="forum_comments-' . $record['id'] . '-' . $record['status'] . '" class="status">' . $img . '</a></span>'
                    . '<span style="margin-right:5px;"><a href=' . HOME_PATH_URL . 'forum.php?editComment&commentId=' . ($record['id']) . '><img src="' . ADMIN_IMAGE . 'li_edit.png" title="Edit"></a></span>'
                    . '<span><a href="javascript:void(0);" class="delete" id="del-blog_comments-' . $record['id'] . '"><img src="' . ADMIN_IMAGE . 'li_delete.png" title="Delete" ></a> </span>';
            $output.= '</td>';
            $output .= '</tr>';

            $i ++;
        }

        $paging_query = "SELECT cm.*, CONCAT(IFNULL(user.first_name,''),' ',IFNULL(user.last_name,'')) AS author,user.user_type FROM " . _prefix("blog_comments") . " AS cm "
                . "  LEFT JOIN " . _prefix("users") . " AS user ON user.id = cm.commented_by "
                . " WHERE cm.deleted = 0 AND (cm.blog_id)= '" . $forumId . "' "
                . " ORDER BY cm.id DESC ";

        $paging_res = $db->sql_query($paging_query);
        $pagingCount = $db->sql_numrows($paging_res);
        if ($count > 0) {
            $data = '&forumId=' . $forumId;
            $paginationCount = getPagination($pagingCount);
            $colspan = 6;
            $output .= get_pagination_link($paginationCount, 'forumComments', $data, $colspan);
        }
    } else {
        $output = '<div class="breadcrumb send_btn" align="right"><a href="' . HOME_PATH_URL . 'forum.php?addComment&forumId=' . $forumId . '"> Add Comment </a></div>';
        $output .= '<div class="norecord">No record Found</div>';
    }
    echo $output;
    ?>
    <script>
        $(".forumComment").colorbox({
            width: '600px',
            height: '500px;'
        });

    </script>
    <?php
    die;
}

function manage_adsList($search_input, $search_type) {
    global $db;
    $condition = "WHERE bg.deleted = 0 ";
    if (isset($search_type) && $search_input != '') {
        switch ($search_type) {
            case "approved":
                $condition .= ' && bg.approve_status = 1 ';
                break;
            case "disapproved":
                $condition .= ' && bg.approve_status = 0 ';
                break;
            case "title":
                $condition .= ' && bg.title like "%' . $search_input . '%" ';
                break;
            case "supplier":
                $condition .= ' && ((user.first_name like "%' . $search_input . '%" ) OR (user.last_name like "%' . $search_input . '%" ))';
//                $condition .= ' && ';
                break;
            default :
                $condition .= ' && ( bg.title like "%' . $search_input . '%" OR (((user.first_name like "%' . $search_input . '%" ) OR(user.last_name like "%' . $search_input . '%" ))) )';
        }
    }

//        $query = "SELECT id FROM " . _prefix("ads_masters") . " $conditon";
    $query = "SELECT bg.id FROM " . _prefix("ads_masters") . " AS bg "
            . "  LEFT JOIN " . _prefix("users") . " AS user ON user.id=bg.supplier_id "
            . " $condition ";
    $res = $db->sql_query($query);
    $count = $db->sql_numrows($res);
    $i = 1;
//        prd($count);
    if (isset($_POST['pageId']) && !empty($_POST['pageId'])) {
        $id = $_POST['pageId'];
        $i = $i + PAGE_PER_NO * $id;
    } else {
        $id = '0';
        $i = 1;
    }
    $pageLimit = PAGE_PER_NO * $id;
    $query = "SELECT bg.*, user.email AS supplier ,CONCAT(IFNULL(user.first_name,''),' ',IFNULL(user.last_name,'')) AS supplierName  FROM " . _prefix("ads_masters") . " AS bg "
            . "  LEFT JOIN " . _prefix("users") . " AS user ON user.id=bg.supplier_id "
            . " $condition  ORDER BY bg.id DESC limit " . $pageLimit . ',' . PAGE_PER_NO;

    $res = $db->sql_query($query);
    $data = $db->sql_fetchrowset($res);
    if ($count > 0) {
        $output = '<tr><th align="left" width="5%">S.No</th>
                                <th align="left" width="20%">Title</th>
                                <th align="left" width="5%">Image</th>
                                <th align="left" width="10%">Supplier</th>
                                <th align="left" width="25%">Description</th>
                                <th align="left" width="10%">Date</th>
                                <th align="left" width="12%">Approval</th>
                                <th align="left" width="21%">Action</th>
                            </tr>';
        foreach ($data as $key => $record) {
            $status = ($record['status'] == 1) ? 'Active' : 'InActive';
            $approve_status = ($record['approve_status'] == 1) ? 'Approved' : 'Disapproved';
            $active_status = ($record['status'] == 1) ? ADMIN_IMAGE . 'activate.png' : ADMIN_IMAGE . 'deactivate.png';
            $approved = ($record['approve_status'] == 1) ? ADMIN_IMAGE . 'activate.png' : ADMIN_IMAGE . 'deactivate.png';
            $description = strip_tags($record['description']);
            $description = (strlen($description) > 40) ? substr($description, 0, 40) . '...' : $description;
            $img = '<img src="' . $active_status . '" title="' . $status . '" />';
            $img1 = $approve_status . ' <img src="' . $approved . '" title="' . $approve_status . '" />';
            $output .= '<tr class="row_' . $record['id'] . '" style="margin-top: 10px;">';
            $output .= '<td>' . $i . '.</td>';
            $output .= '<td>' . $record['title'] . '</td>';
            $fileImage = 'files/ads_image/thumb_' . $record['image'];
            if (file_exists($fileImage)) {
                $output .=!empty($record['image']) ? '<td><a  class="image" href="' . HOME_PATH_URL . 'popup.php?adsImage&id=' . md5($record['id']) . '"><img  title="View image" src="' . MAIN_PATH . '/files/ads_image/thumb_' . $record['image'] . '"></a><br/><a href="' . MAIN_PATH . '/download.php?type=ads_image&file=' . $record['image'] . '">Download<i class="fa fa-download"></i></a></td>' : '';
            } else {
                $output .= '<td><img  title="View image" src="' . HOME_PATH . 'images/notavail.gif"></a></td>';
            }
            $output .= isset($record['supplierName']) ? '<td>' . $record['supplierName'] . '</td>' : '<td class="text-center">N/A</td>';
            $output .= '<td>' . $description . '</td>';
            $output .= '<td>' . date('M d, Y', strtotime($record['created'])) . '</td>';
            $output .= '<td> <span class="approval-' . $record['id'] . '" style="margin-right:5px;"><a href="javascript:void(0);" id="ads_masters--' . $record['id'] . '--' . $record['approve_status'] . '" class="approval">' . $img1 . '</a></span></td>';
            $output .= '<td><span class="status-' . $record['id'] . '" style="margin-right:5px;"><a href="javascript:void(0);" id="ads_masters-' . $record['id'] . '-' . $record['status'] . '" class="status">' . $img . '</a></span>'
                    . '<span style="margin-right:5px;"><a href=' . HOME_PATH_URL . 'ads.php?edit&id=' . md5($record['id']) . '><img src="' . ADMIN_IMAGE . 'li_edit.png" title="Edit"></a></span>'
                    . '<span><a href="javascript:void(0);" class="delete" id="del-ads_masters-' . $record['id'] . '"><img src="' . ADMIN_IMAGE . 'li_delete.png" title="Delete" ></a> </span>';
            $output.= '</td>';
            $output .= '</tr>';
            $i ++;
        }

        $pagingCount = $count;
        if ($pagingCount > 0) {
            $data = '';
            $paginationCount = getPagination($pagingCount);
            $colspan = 6;
            $output .= get_pagination_link($paginationCount, 'adsList', $data, $colspan);
        }
    } else {
        $output = '<div class="norecord">No record Found</div>';
    }
    echo $output;
    ?>
    <script>
        $(".image").colorbox({
            width: '600px',
            height: '500px;'
        });
        $(document).ready(function() {
            $('.load').click(function() {
                var value = $(this).attr('id').split('-');
                $('#loading-' + value[1]).show();
            });
        });
    </script>
    <?php
    die;
}

function manage_sliderImage() {
    global $db;
    $condition = " where deleted = 0 ";
    $query = "SELECT id, title, description, image, status, created FROM " . _prefix("sliders") . " where deleted = 0 ";
    $res = $db->sql_query($query);
    $count = $db->sql_numrows($res);
    $i = 1;
    if (isset($_POST['pageId']) && !empty($_POST['pageId'])) {
        $id = $_POST['pageId'];
        $i = $i + PAGE_PER_NO * $id;
    } else {
        $id = '0';
        $i = 1;
    }

    $pageLimit = PAGE_PER_NO * $id;
    $query = "SELECT id, title, description, image, status, created FROM " . _prefix("sliders") . "   "
            . " where deleted = 0  ORDER BY id DESC limit " . $pageLimit . ',' . PAGE_PER_NO;
    $res = $db->sql_query($query);
    $data = $db->sql_fetchrowset($res);
    if ($count > 0) {
        $output = '<tr><th align="left" width="5%">S.No</th>
                                <th align="left" width="15%">Title</th>
                                <th align="left" width="30%">Description</th>
                                <th align="left" width="10%">Image</th>
                                <th align="left" width="10%">Created On</th>
                                <th align="left" width="10%">Action</th>
                            </tr>';
        foreach ($data as $key => $record) {
            $status = ($record['status'] == 1) ? 'Active' : 'InActive';
            $active_status = ($record['status'] == 1) ? ADMIN_IMAGE . 'activate.png' : ADMIN_IMAGE . 'deactivate.png';
            $description = strip_tags($record['description']);
            $description = (strlen($description) > 40) ? substr($description, 0, 40) . '...' : $description;
            $img = '<img src="' . $active_status . '" title="' . $status . '" />';
            $output .= '<tr class="row_' . $record['id'] . '" style="margin-top: 10px;">';
            $output .= '<td>' . $i . '.</td>';
            $output .= '<td>' . $record['title'] . '</td>';
            $output .= '<td>' . $description . '</td>';
            $fileImage = 'files/slider/thumb_' . $record['image'];
            if (file_exists($fileImage)) {
                $output .=!empty($record['image']) ? '<td><a  class="image" href="' . HOME_PATH_URL . 'popup.php?sliderImage&id=' . md5($record['id']) . '"><img  title="feature" src="' . $fileImage . '"></a></td>' : '';
            } else {
                $output .= '<td></td>';
            }
            //$output .= '<td><img  title="feature" src="' . MAIN_PATH . '/files/company_logo/thumb_' . $record['image'] . '"></td>';
            $output .= '<td>' . date('M d, Y', strtotime($record['created'])) . '</td>';
            $output .= '<td><span class="status-' . $record['id'] . '" style="margin-right:5px;"><a href="javascript:void(0);" id="sliders-' . $record['id'] . '-' . $record['status'] . '" class="status">' . $img . '</a></span>'
                    . '<span style="margin-right:5px;"><a href=' . HOME_PATH_URL . 'slider.php?edit&id=' . md5($record['id']) . '><img src="' . ADMIN_IMAGE . 'li_edit.png" title="Edit"></a></span>'
                    . '<span><a href="javascript:void(0);" class="delete" id="del-sliders-' . $record['id'] . '"><img src="' . ADMIN_IMAGE . 'li_delete.png" title="Delete" ></a> </span>';
            $output.= '</td>';
            $output .= '</tr>';

            $i ++;
        }
        $paging_query = "SELECT id, title, description, image, created_on FROM " . _prefix("sliders") . "   " . " where deleted = 0  ";
        $paging_res = $db->sql_query($paging_query);
        $pagingCount = $db->sql_numrows($paging_res);
        if ($pagingCount > 0) {
            $data = '';
            $paginationCount = getPagination($pagingCount);
            $colspan = 6;
            if (isset($search_type)) {
                $data .="&search_input=$search_input&search_type=$search_type";
            }
            $output .= get_pagination_link($paginationCount, 'slider', $data, $colspan);
        }
    } else {
        $output = '<div class="norecord">No record Found</div>';
    }
    echo $output;
    ?>
    <script>
        $(".image").colorbox({
            width: '600px',
            height: '500px;'
        });
    </script>
    <?php
    die;
}

function manage_supplier($search_type, $search_input) {

    $clientType = array('0' => 'Customer', '1' => 'Supplier');
    global $db;
    $conditon = "WHERE du.deleted = 0  && du.user_type =1";
    if (isset($search_input) && $search_input != '') {
         $conditon .= ' && ((du.first_name like "%' . $search_input . '%" ) OR (du.last_name like "%' . $search_input . '%" ))';

//        switch ($search_type) {
//             case "name":
//                $condition .= ' && ((du.first_name like "%' . $search_input . '%" ) OR (du.last_name like "%' . $search_input . '%" ))';
//                break;
//            case "sp_id":
//                if (substr($search_input, 3) == '') {
//                    $search_input = $search_input;
//                } else {
//                    $search_input = substr($search_input, 3);
//                }
//                $conditon .= "(du.unique_id like '%" . $search_input . "%') && ";
//                break;
//        }
    }
//    $conditon .= '1';
    $query = "SELECT du.id, CONCAT(IFNULL(du.first_name,''),'',IFNULL(du.last_name,'')) as fullName,du.user_name,du.unique_id, du.user_type, du.email, du.created, du.status, dc.name as countryName FROM " . _prefix("users") . " as du "
            . "LEFT JOIN " . _prefix("countries") . " as dc ON du.country_id = dc.id "
            . "$conditon";
    $res = $db->sql_query($query);
    $count = $db->sql_numrows($res);
    $i = 1;
    if (isset($_POST['pageId']) && !empty($_POST['pageId'])) {
        $id = $_POST['pageId'];
        $i = $i + PAGE_PER_NO * $id;
    } else {
        $id = '0';
        $i = 1;
    }

    $pageLimit = PAGE_PER_NO * $id;

    $query = "SELECT du.id,CONCAT(IFNULL(du.first_name,''),' ',IFNULL(du.last_name,'')) as fullName,du.unique_id, du.user_name, du.email, du.created, du.refferal_id, du.validate, du.status, dc.name as countryName FROM " . _prefix("users") . " as du "
            . "LEFT JOIN " . _prefix("countries") . " as dc ON du.country_id = dc.id "
            . "$conditon ORDER BY du.user_name ASC limit " . $pageLimit . ',' . PAGE_PER_NO;
    $res = $db->sql_query($query); //prd($query);
    $data = $db->sql_fetchrowset($res);
    if ($count > 0) {
        $output = '<tr><th align="left" width="5%">S.No</th>
                                <th align="left" width="30%">Name</th>
                                <th align="left" width="30%">User Name</th>
                                <th align="left" width="20%">Date</th>
                                <th align="left" width="20%">Action</th>
                            </tr>';

        foreach ($data as $key => $record) {

            $clientId = $record['unique_id'] == '' ? 'N/A' : 'KAS' . $record['unique_id'];
            $referral = $record['refferal_id'] == '' ? 'N/A' : $record['refferal_id'];
            $status = ($record['status'] == 1) ? 'Active' : 'InActive';
            $active_status = ($record['status'] == 1) ? ADMIN_IMAGE . 'activate.png' : ADMIN_IMAGE . 'deactivate.png';
            $img = '<img src="' . $active_status . '" title="' . $status . '" />';
            $output .= '<tr class="row_' . $record['id'] . '" style="margin-top: 10px;">';
            $output .= '<td>' . $i . '.</td>';
            $output .= '<td><a href="' . HOME_PATH_URL . 'user.php?supplierDetail&id=' . $record['id'] . '">' . $record['fullName'] . '</a></td>';
            $output .= '<td>' . $record['user_name'] . '</td>';
            $output .= '<td>' . date('M d, Y', strtotime($record['created'])) . '</td>';
            $output .= '<td><span class="status-' . $record['id'] . '" style="margin-right:5px;"><a href="javascript:void(0);" id="users-' . $record['id'] . '-' . $record['status'] . '" class="status">' . $img . '</a></span>'
                    . '<span style="margin-right:5px;"><a href=' . HOME_PATH_URL . 'user.php?editSupplier&id=' . md5($record['id']) . '><img src="' . ADMIN_IMAGE . 'li_edit.png" title="Edit"></a></span>'
                    . '<span><a href="javascript:void(0);" class="delete" id="del-users-' . $record['id'] . '"><img src="' . ADMIN_IMAGE . 'li_delete.png" title="Delete" ></a> </span>';
            $time = date('Y-m-d H:i:s', strtotime($record['created']) + (60 * 60 * 7));
            $currentTime = date('Y-m-d H:i:s', strtotime(date('Ymdhis')));
//                if ($record['validate'] == 0 && (strtotime($currentTime) >= strtotime($time))) {
            if ($record['validate'] == 0) {
                $output .='<span class="loader-message"><form method="POST">'
                        . '<input type=hidden name="id" value="' . $record['id'] . '">'
                        . '<input type="submit" name="Send Mail" value="Verify" class="load" id="load-' . $record['id'] . '">'
                        . '</form><div id="loading-' . $record['id'] . '" style="display:none;" class="loading-img"><img src="' . ADMIN_IMAGE . 'loading.gif"></div></span>';
            } else if ($record['validate'] == 3) {
                $output .= '<br><span> Not Verified</span>';
            }
            $output .= '</td>';
            $output .= '</tr>';

            $i ++;
        }

        $pagingCount = $count;
        if ($pagingCount > 0) {
            $data = '';
            $paginationCount = getPagination($pagingCount);
            $colspan = 5;
            if (isset($search_type)) {
                $data .="&search_input=$search_input&search_type=$search_type";
            }
            $output .= get_pagination_link($paginationCount, 'supplier', $data, $colspan);
        }
    } else {
        $output = '<div class="norecord">No record Found</div>';
    }
    echo $output;
    ?>
    <script>
        $(".userprofile").colorbox({
            width: '600px',
            height: '500px;'
        });

    </script>
    <script>
        $(document).ready(function() {
            $('.load').click(function() {
                var value = $(this).attr('id').split('-');
                $('#loading-' + value[1]).show();
            });
        });


    </script>
    <?php
    die;
}

function manage_supplier_detail($uid) {

    global $db;
    $condition = " where prd.deleted = 0 ";
    $query = "SELECT prd.id, prd.title, prd.supplier_id, prd.description, prd.keyword, prd.image, srv.name, prd.status, cit.title as city FROM " . _prefix("products") . " AS prd Left join " . _prefix("services") . " AS srv ON srv.id=prd.facility_type Left join " . _prefix("cities") . " AS cit ON cit.id=prd.city_id " . $condition . " && prd.supplier_id = " . $uid . "  ORDER BY prd.id DESC ";
    $res = $db->sql_query($query);
    $count = $db->sql_numrows($res);
    $i = 1;
    if (isset($_POST['pageId']) && !empty($_POST['pageId'])) {
        $id = $_POST['pageId'];
        $i = $i + PAGE_PER_NO * $id;
    } else {
        $id = '0';
        $i = 1;
    }

    $pageLimit = PAGE_PER_NO * $id;

    $query = "SELECT prd.id, prd.title, prd.description, prd.supplier_id,  prd.keyword,prd.pro_extra_info, prd.image, srv.name, prd.status, cit.title as city FROM " . _prefix("products") . " AS prd Left join " . _prefix("services") . " AS srv ON srv.id=prd.facility_type Left join " . _prefix("cities") . " AS cit ON cit.id=prd.city_id " . $condition . " && prd.supplier_id = " . $uid . "  ORDER BY prd.id DESC limit " . $pageLimit . ',' . PAGE_PER_NO;
    $res = $db->sql_query($query); //prd($query);
    $data = $db->sql_fetchrowset($res);
    if ($count > 0) {
        $output = '<table class="table table-bordered table_bord ">
                    <thead>
                    <tr>
                    <th class="col-md-1">S.No</th>
                    <th class="col-md-2">Title</th>
                    <th class="col-md-2">Description</th>
                    <th class="col-md-2">Extra Information</th>
                    <th class="col-md-1">City</th>
                    <th class="col-md-3">Action</th>
                    </tr>
                    </thead>
                    <tbody>';

        foreach ($data as $key => $record) {

            $status = ($record['status'] == 1) ? 'Active' : 'InActive';
            $active_status = ($record['status'] == 1) ? ADMIN_IMAGE . 'activate.png' : ADMIN_IMAGE . 'deactivate.png';
            $description = strip_tags($record['description']);
            $description = (strlen($description) > 60) ? substr($description, 0, 60) . '...' : $description;
            $img = '<img src="' . $active_status . '" title="' . $status . '" />';
            $output .= '<tr class="row_' . $record['id'] . '" style="margin-top: 10px;">';
            $output .= '<td>' . $i . ' .</td>';
            $output .= '<td>' . $record['title'] . '</td>';
            $output .= '<td>' . $description . '</td>';
            if (empty($record['pro_extra_info'])) {
                $output .= '<td><a href=' . HOME_PATH_URL . 'user.php?extraInfo&sp_id=' . base64_encode($record['supplier_id']) . '&pro_id=' . base64_encode($record['id']) . '&id=' . base64_encode($record['pro_extra_info']) . '>Add Extra Info</a></td>';
            } else {
                $output .= '<td><a href=' . HOME_PATH_URL . 'user.php?extraInfo&sp_id=' . base64_encode($record['supplier_id']) . '&pro_id=' . base64_encode($record['id']) . '&id=' . base64_encode($record['pro_extra_info']) . '>Edit Extra Info</a></td>';
            }
            $output .= '<td>' . $record['city'] . '</td>';
            // $output .= '<td>' . $record['name'] . '</td>';
            $output .= '<td><span class="status-' . $record['id'] . '" style="margin-right:5px;"><a href="javascript:void(0);" id="products-' . $record['id'] . '-' . $record['status'] . '" class="status">' . $img . '</a></span>'
                    . '<span style="margin-right:5px;"><a href=' . HOME_PATH_URL . 'user.php?editProduct&id=' . md5($record['id']) . '><img src="' . ADMIN_IMAGE . 'li_edit.png" title="Edit"></a></span>'
                    . '<span><a href="javascript:void(0);" class="delete" id="del-products-' . $record['id'] . '"><img src="' . ADMIN_IMAGE . 'li_delete.png" title="Delete" ></a> </span>';
            $output.= '</td>';
            $output .= '</tr>';

            $i ++;
        }
        $output .='</tbody></table>';

        $pagingCount = $count;
        if ($pagingCount > 0) {
//            $data = '';
            $paginationCount = getPagination($pagingCount);
            $colspan = 5;
                $data ="&start_date=&search_type=&end_date=&id=$uid";
            $output .= get_pagination_link($paginationCount, 'supplierDetail', $data, $colspan);
        }
    } else {
        $output = '<div class="norecord">No record Found</div>';
    }
    echo $output;
    die;
}

function manage_newsletter() {
    global $db;
    $query = "SELECT * FROM " . _prefix("newsletter") . " where deleted = 0 ORDER BY created DESC";
    $res = $db->sql_query($query);
    $count = $db->sql_numrows($res);
    $i = 1;
    if (isset($_POST['pageId']) && !empty($_POST['pageId'])) {
        $id = $_POST['pageId'];
        $i = $i + PAGE_PER_NO * $id;
    } else {
        $id = '0';
        $i = 1;
    }

    $pageLimit = PAGE_PER_NO * $id;
    $query = "SELECT * FROM " . _prefix("newsletter") . " where deleted = 0 ORDER BY id DESC limit " . $pageLimit . ',' . PAGE_PER_NO;
    $res = $db->sql_query($query);
    $data = $db->sql_fetchrowset($res);
    if ($count > 0) {
        $output = '<tr><th align="left" width="7%">S.No</th>
                                <th align="left" width="62%">Title</th>
                                <th align="left" width="25%">Created</th>
                                <th align="left" width="200px">Action</th>
                            </tr>';

        foreach ($data as $key => $record) {
            $status = ($record['status'] == 1) ? 'Active' : 'InActive';
            $active_status = ($record['status'] == 1) ? ADMIN_IMAGE . 'activate.png' : ADMIN_IMAGE . 'deactivate.png';
            $img = '<img src="' . $active_status . '" title="' . $status . '" />';
            $output .= '<tr class="row_' . $record['id'] . '" style="margin-top: 10px;">';
            $output .= '<td>' . $i . '.</td>';
            $output .= '<td>' . $record['title'] . '</td>';
            $output .= '<td>' . date('M d, Y', strtotime($record['created'])) . '</td>';
            $output .= '<td><span class="status-' . $record['id'] . '" style="margin-right:5px;"><a href="javascript:void(0);" id="newsletter-' . $record['id'] . '-' . $record['status'] . '" class="status">' . $img . '</a></span>'
                    . '<span style="margin-right:5px;"><a href=' . HOME_PATH_URL . 'newsletter.php?edit&id=' . md5($record['id']) . '><img src="' . ADMIN_IMAGE . 'li_edit.png" title="Edit"></a></span>'
                    . '<span><a href="javascript:void(0);" class="delete" id="del-newsletter-' . $record['id'] . '"><img src="' . ADMIN_IMAGE . 'li_delete.png" title="Delete" ></a> </span>'
                    . '</td>';
            $output .= '</tr>';

            $i ++;
        }
    } else {
        $output = '<div class="norecord">No record Found</div>';
    }
    echo $output;
    die;
}

function manage_pages() {
    global $db;
    $query = "SELECT * FROM " . _prefix("pages") . " where deleted = 0 ORDER BY created DESC";
    $res = $db->sql_query($query);
    $count = $db->sql_numrows($res);
    $i = 1;
    if (isset($_POST['pageId']) && !empty($_POST['pageId'])) {
        $id = $_POST['pageId'];
        $i = $i + PAGE_PER_NO * $id;
    } else {
        $id = '0';
        $i = 1;
    }

    $pageLimit = PAGE_PER_NO * $id;
    $query = "SELECT * FROM " . _prefix("pages") . " where deleted = 0 ORDER BY id DESC limit " . $pageLimit . ',' . PAGE_PER_NO;
    $res = $db->sql_query($query);
    $data = $db->sql_fetchrowset($res);
    if ($count > 0) {
        $output = '<tr><th align="left" width="7%">S.No</th>
                                <th align="left" width="50%">Article Title</th>
                                <th align="left" width="25%">Created</th>
                                <th align="left" width="30">Action</th>
                            </tr>';

        foreach ($data as $key => $record) {

            $status = ($record['status'] == 1) ? 'Active' : 'InActive';
            $active_status = ($record['status'] == 1) ? ADMIN_IMAGE . 'activate.png' : ADMIN_IMAGE . 'deactivate.png';
            $img = '<img src="' . $active_status . '" title="' . $status . '" />';
            $output .= '<tr class="row_' . $record['id'] . '" style="margin-top: 10px;">';
            $output .= '<td>' . $i . '.</td>';
            $output .= '<td>' . $record['page_title'] . '</td>';
            $output .= '<td>' . date('M d, Y', strtotime($record['created'])) . '</td>';
            $output .= '<td><span class="status-' . $record['id'] . '" style="margin-right:5px;"><a href="javascript:void(0);" id="pages-' . $record['id'] . '-' . $record['status'] . '" class="status">' . $img . '</a></span>'
                    . '<span style="margin-right:5px;"><a href=' . HOME_PATH_URL . 'pages.php?edit&id=' . md5($record['id']) . '><img src="' . ADMIN_IMAGE . 'li_edit.png" title="Edit"></a></span>'
                    . '<span><a href="javascript:void(0);" class="delete" id="del-pages-' . $record['id'] . '"><img src="' . ADMIN_IMAGE . 'li_delete.png" title="Delete" ></a> </span>'
                    . '</td>';
            $output .= '</tr>';

            $i ++;
        }
        $paging_query = "SELECT * FROM " . _prefix("pages") . " where deleted = 0 ORDER BY created DESC";
        $paging_res = $db->sql_query($paging_query);
        $pagingCount = $db->sql_numrows($paging_res);
        if ($pagingCount > 0) {
            $paginationCount = getPagination($pagingCount);

            $output .= get_pagination_link($paginationCount, 'pages', $data = '', $colspan = 4);
        }
    } else {
        $output = '<div class="norecord">No record Found</div>';
    }
    echo $output;
    die;
}

function manage_reviews() {
    global $db;
    $query = "SELECT * FROM " . _prefix("feedbacks") . " where deleted = 0 ORDER BY created DESC";
    $res = $db->sql_query($query);
    $count = $db->sql_numrows($res);
    $i = 1;
    if (isset($_POST['pageId']) && !empty($_POST['pageId'])) {
        $id = $_POST['pageId'];
        $i = $i + PAGE_PER_NO * $id;
    } else {
        $id = '0';
        $i = 1;
    }

    $pageLimit = PAGE_PER_NO * $id;
    $query = "SELECT * FROM " . _prefix("feedbacks") . " where deleted = 0 ORDER BY id DESC limit " . $pageLimit . ',' . PAGE_PER_NO;
    $res = $db->sql_query($query);
    $data = $db->sql_fetchrowset($res);
    if ($count > 0) {
        $output = '<tr><th align="left" width="7%">S.No</th>
                                <th align="left" width="50%">Review Title</th>
                                <th align="left" width="25%">Created</th>
                                <th align="left" width="30">Action</th>
                            </tr>';

        foreach ($data as $key => $record) {

            $status = ($record['status'] == 1) ? 'Active' : 'InActive';
            $active_status = ($record['status'] == 1) ? ADMIN_IMAGE . 'activate.png' : ADMIN_IMAGE . 'deactivate.png';
            $img = '<img src="' . $active_status . '" title="' . $status . '" />';
            $output .= '<tr class="row_' . $record['id'] . '" style="margin-top: 10px;">';
            $output .= '<td>' . $i . '.</td>';
            $output .= '<td>' . $record['title'] . '</td>';
            $output .= '<td>' . date('M d, Y', strtotime($record['created'])) . '</td>';
            $output .= '<td><span class="status-' . $record['id'] . '" style="margin-right:5px;"><a href="javascript:void(0);" id="feedbacks-' . $record['id'] . '-' . $record['status'] . '" class="status">' . $img . '</a></span>'
                    . '<span style="margin-right:5px;"><span style="margin-right:5px;"><a  class="views" href="' . HOME_PATH_URL . 'popup.php?reviews&id=' . md5($record['id']) . '"><img src="' . ADMIN_IMAGE . 'li_view.png" title="View"></a></span>'
                    . '<span><a href="javascript:void(0);" class="delete" id="del-feedbacks-' . $record['id'] . '"><img src="' . ADMIN_IMAGE . 'li_delete.png" title="Delete" ></a> </span>'
                    . '</td>';
            $output .= '</tr>';

            $i ++;
        }
        $paging_query = "SELECT * FROM " . _prefix("feedbacks") . " where deleted = 0 ORDER BY created DESC";
        $paging_res = $db->sql_query($paging_query);
        $pagingCount = $db->sql_numrows($paging_res);
        if ($pagingCount > 0) {
            $paginationCount = getPagination($pagingCount);

            $output .= get_pagination_link($paginationCount, 'review', $data = '', $colspan = 4);
        }
    } else {
        $output = '<div class="norecord">No record Found</div>';
    }
    echo $output;
    ?><script type="text/javascript">
        $(".views").colorbox({
            width: '600px',
            height: '700px;'
        });
    </script>

    <?php
    die;
}

function manage_abused_review() {
    global $db;
    $query = "SELECT * FROM " . _prefix("feedbacks") . " where deleted = 0 && abused_request = 1 ORDER BY created DESC";
    $res = $db->sql_query($query);
    $count = $db->sql_numrows($res);
    $i = 1;
    if (isset($_POST['pageId']) && !empty($_POST['pageId'])) {
        $id = $_POST['pageId'];
        $i = $i + PAGE_PER_NO * $id;
    } else {
        $id = '0';
        $i = 1;
    }

    $pageLimit = PAGE_PER_NO * $id;
    $query = "SELECT rev.*,CONCAT(IFNULL(user.first_name,''),' ',IFNULL(user.last_name,'')) AS cust_name FROM " . _prefix("feedbacks") . " AS rev LEFT JOIN " . _prefix("users") . " AS user on rev.abused_by=user.id where rev.deleted = 0 && rev.abused_request = 1 ORDER BY abuse_date DESC  limit " . $pageLimit . ',' . PAGE_PER_NO;
    $res = $db->sql_query($query);
    $data = $db->sql_fetchrowset($res);
    if ($count > 0) {
        $output = '<tr><th align="left" width="5%">S.No</th>
                                <th align="left" width="20%">Review Title</th>
                                <th align="left" width="15%">Flagged By</th>
                                <th align="left" width="20%">Reason for moderation</th>
                                <th align="left" width="15%">Reported Date</th>
                                <th align="left" width="10%">Set Moderate</th>
                                <th align="left" width="15%">Action</th>
                            </tr>';

        foreach ($data as $key => $record) {

            $status = ($record['abused'] == 1) ? 'Active' : 'InActive';
            $active_status = ($record['abused'] == 1) ? ADMIN_IMAGE . 'activate.png' : ADMIN_IMAGE . 'deactivate.png';
            $img = '<img src="' . $active_status . '" title="' . $status . '" />';
            $title = strip_tags($record['feedback']);
            $title = (strlen($title) > 60) ? substr($title, 0, 60) . '...' : $title;

            $output .= '<tr class="row_' . $record['id'] . '" style="margin-top: 10px;">';
            $output .= '<td>' . $i . '.</td>';
            $output .= '<td>' . $title . '</td>';
            $output .= '<td>' . $record['cust_name'] . '</td>';
            $output .= '<td>' . $record['abuse_reason'] . '</td>';
            $output .= '<td>' . date('M d, Y h:m:s', strtotime($record['abuse_date'])) . '</td>';
            $output .= '<td align="center"><span class="abuse-' . $record['id'] . '" style="margin-right:5px;"><a href="javascript:void(0);" id="feedbacks-' . $record['id'] . '-' . $record['abused'] . '" class="abuse">' . $img . '</a></span></td>';
            $output .= '<td align="center"><span style="margin-right:5px;"><a href=' . HOME_PATH_URL . 'review.php?edit_abuse&id=' . md5($record['id']) . '><img src="' . ADMIN_IMAGE . 'li_edit.png" title="Edit"></a></span>'
                    . '<span><a href="javascript:void(0);" class="removeFlag" id="del-feedbacks-' . $record['id'] . '"><img title="Click to remove Flagged Remark" src="' . ADMIN_IMAGE . 'li_delete.png" title="Delete" ></a> </span>'
                    . '</td>';
            $output .= '</tr>';

            $i ++;
        }
        $paging_query = "SELECT * FROM " . _prefix("feedbacks") . " where deleted = 0 && abused_request=1 ORDER BY created DESC";
        $paging_res = $db->sql_query($paging_query);
        $pagingCount = $db->sql_numrows($paging_res);
        if ($pagingCount > 0) {
            $paginationCount = getPagination($pagingCount);

            $output .= get_pagination_link($paginationCount, 'abusedreview', $data = '', $colspan = 7);
        }
    } else {
        $output = '<div class="norecord">No record Found</div>';
    }
    echo $output;
    die;
}

function manage_service() {
    global $db;
    $query = "SELECT * FROM " . _prefix("services") . " where deleted = 0 ORDER BY order_by DESC";
    $res = $db->sql_query($query);
    $count = $db->sql_numrows($res);
    $i = 1;
    if (isset($_POST['pageId']) && !empty($_POST['pageId'])) {
        $id = $_POST['pageId'];
        $i = $i + PAGE_PER_NO * $id;
    } else {
        $id = '0';
        $i = 1;
    }

    $pageLimit = PAGE_PER_NO * $id;
    $query = "SELECT * FROM " . _prefix("services") . " where deleted = 0 ORDER BY order_by DESC limit " . $pageLimit . ',' . PAGE_PER_NO;
    $res = $db->sql_query($query);
    $data = $db->sql_fetchrowset($res);
    $plan_type = array("0" => "Basic", "1" => "Business", "2" => "Premium");
    if ($count > 0) {
        $output = '<tr><th align="left" width="7%">S.No</th>
                                <th align="left" width="20%">Service Name</th>
                                <th align="left" width="20%">Created</th>
                                <th align="left" width="20%">Order</th>
                                <th align="left" width="17%">Action</th>
                            </tr>';
        foreach ($data as $key => $record) {

            $status = ($record['status'] == 1) ? 'Active' : 'InActive';
            $active_status = ($record['status'] == 1) ? ADMIN_IMAGE . 'activate.png' : ADMIN_IMAGE . 'deactivate.png';
            $img = '<img src="' . $active_status . '" title="' . $status . '" />';
            $output .= '<tr class="row_' . $record['id'] . '" style="margin-top: 10px;">';
            $output .= '<td>' . $i . '.</td>';
            $output .= '<td>' . $record['name'] . '</td>';
//                $output .= '<td>' . $plan_type[$record['plan_type']] . '</td>';
            $output .= '<td>' . date('M d, Y', strtotime($record['created'])) . '</td>';
            $output .= '<td>';
            if ($count > 1) {
                if ($i == 1) {
                    $output .= '<span style="margin-right:5px;"><a href=" javascript:void(0);" id="services-' . $record['order_by'] . '-' . $record['id'] . '" class="order up"><img src="' . ADMIN_IMAGE . 'arrowup.png" title="UP"></a></span>';
                } else if ($i == $count) {
                    $output .= '<span style="margin-right:5px;"><a href=" javascript:void(0);" id="services-' . $record['order_by'] . '-' . $record['id'] . '" class="order down"><img src="' . ADMIN_IMAGE . 'arrowdown.png" title="DOWN"></a></span>';
                } else {
                    $output .= '<span style="margin-right:5px;"><a href=" javascript:void(0);" id="services-' . $record['order_by'] . '-' . $record['id'] . '" class="order up"><img src="' . ADMIN_IMAGE . 'arrowup.png" title="UP"></a></span>';
                    $output .= '<span style="margin-right:5px;"><a href=" javascript:void(0);" id="services-' . $record['order_by'] . '-' . $record['id'] . '" class="order down"><img src="' . ADMIN_IMAGE . 'arrowdown.png" title="DOWN"></a></span>';
                }
            }
            $output .='</td>';
            $output .= '<td>';
            $output .='<span class="status-' . $record['id'] . '" style="margin-right:5px;"><a href="javascript:void(0);" id="services-' . $record['id'] . '-' . $record['status'] . '" class="status">' . $img . '</a></span>'
                    . '<span style="margin-right:5px;"><a href=' . HOME_PATH_URL . 'service.php?edit&id=' . md5($record['id']) . '><img src="' . ADMIN_IMAGE . 'li_edit.png" title="Edit"></a></span>'
                    . '<span><a href="javascript:void(0);" class="delete" id="del-services-' . $record['id'] . '"><img src="' . ADMIN_IMAGE . 'li_delete.png" title="Delete" ></a> </span>'
                    . '</td>';
            $output .= '</tr>';

            $i ++;
        }
        $pagingCount = $count;
        if ($pagingCount > 0) {
            $data = '';
            $paginationCount = getPagination($pagingCount);
            $colspan = 6;
            $output .= get_pagination_link($paginationCount, 'service', $data, $colspan);
        }
    } else {
        $output = '<div class="norecord">No record Found</div>';
    }
    echo $output;
    die;
}

function manage_email() {

    global $db;
    $query = "SELECT * FROM " . _prefix("email_templates") . " where deleted = 0 ORDER BY created DESC";
    $res = $db->sql_query($query);
    $count = $db->sql_numrows($res);
    $i = 1;
    if (isset($_POST['pageId']) && !empty($_POST['pageId'])) {
        $id = $_POST['pageId'];
        $i = $i + PAGE_PER_NO * $id;
    } else {
        $id = '0';
        $i = 1;
    }

    $pageLimit = PAGE_PER_NO * $id;
    $query = "SELECT * FROM " . _prefix("email_templates") . " where deleted = 0 ORDER BY id DESC limit " . $pageLimit . ',' . PAGE_PER_NO;
    $res = $db->sql_query($query);
    $data = $db->sql_fetchrowset($res);




    if ($count > 0) {
        $output = '<tr>     <th align="left" width="10%">S.No</th>
                                <th align="left" width="40%">Title</th>
                                <th align="left" width="40%">Subject</th>
                                <th align="left" width="200px">Action</th>
                            </tr>';


        foreach ($data as $key => $record) {
            $output .= '<tr class="row_' . $record['id'] . '">';
            $title = $record['title'] == '' ? 'N/A' : $record['title'];
            $subject = $record['subject'] == '' ? 'N/A' : $record['subject'];
            $status = ($record['status'] == 1) ? 'Active' : 'InActive';
            $active_status = ($record['status'] == 1) ? ADMIN_IMAGE . 'activate.png' : ADMIN_IMAGE . 'deactivate.png';
            $img = '<img src="' . $active_status . '" title="' . $status . '" />';
            $output .= '<td>' . $i . ' </td>';
            $output .= '<td>' . $title . '</td>';
            $output .= '<td>' . $subject . '</td>';

            $output .= '<td><span class="status-' . $record['id'] . '" style="margin-right:5px;"><a href="javascript:void(0);" id="email_templates-' . $record['id'] . '-' . $record['status'] . '" class="status">' . $img . '</a></span>'
                    . '<span style="margin-right:5px;"><a href=' . HOME_PATH_URL . 'email.php?edit&id=' . md5($record['id']) . '><img src="' . ADMIN_IMAGE . 'li_edit.png" title="Edit"></a></span>'
                    . '<span><a href="javascript:void(0);" class="delete" id="del-email_templates-' . $record['id'] . '"><img src="' . ADMIN_IMAGE . 'li_delete.png" title="Delete" ></a> </span>'
                    . '</td>';
            $output .= '</tr>';
            $i ++;
        }
    } else {
        $output = '<div class="norecord">No record Found</div>';
    }
    echo $output;

    die();
}

function manage_location() {
    global $db;
    $condition = " where deleted = 0 ";
    $query = "SELECT id, title, status FROM " . _prefix("cities") . "$condition";
    $res = $db->sql_query($query);
    $count = $db->sql_numrows($res);
    $i = 1;
    if (isset($_POST['pageId']) && !empty($_POST['pageId'])) {
        $id = $_POST['pageId'];
        $i = $i + PAGE_PER_NO * $id;
    } else {
        $id = '0';
        $i = 1;
    }

    $pageLimit = PAGE_PER_NO * $id;
    $query = "SELECT id, title, status FROM " . _prefix("cities") . "   "
            . "$condition ORDER BY title ASC limit " . $pageLimit . ',' . PAGE_PER_NO;
    $res = $db->sql_query($query);
    $data = $db->sql_fetchrowset($res);
    if ($count > 0) {
        $output = '<tr><th align="left" width="5%">S.No</th>
                                <th align="left" width="10%">Title</th>
                                <th align="left" width="21%">Action</th>
                            </tr>';
        foreach ($data as $key => $record) {
//            prd($record);
            $status = ($record['status'] == 1) ? 'Active' : 'InActive';
            $active_status = ($record['status'] == 1) ? ADMIN_IMAGE . 'activate.png' : ADMIN_IMAGE . 'deactivate.png';
            $description = strip_tags($record['description']);
            $description = (strlen($description) > 60) ? substr($description, 0, 60) . '...' : $description;
            $img = '<img src="' . $active_status . '" title="' . $status . '" />';
            $output .= '<tr class="row_' . $record['id'] . '" style="margin-top: 10px;">';
            $output .= '<td>' . $i . '.</td>';
            $output .= '<td>' . $record['title'] . '</td>';
            $output .= '<td><span class="status-' . $record['id'] . '" style="margin-right:5px;"><a href="javascript:void(0);" id="cities-' . $record['id'] . '-' . $record['status'] . '" class="status">' . $img . '</a></span>'
                    . '<span style="margin-right:5px;"><a href=' . HOME_PATH_URL . 'location.php?editCity&id=' . md5($record['id']) . '><img src="' . ADMIN_IMAGE . 'li_edit.png" title="Edit"></a></span>'
                    . '<span><a href="javascript:void(0);" class="delete" id="del-cities-' . $record['id'] . '"><img src="' . ADMIN_IMAGE . 'li_delete.png" title="Delete" ></a> </span>';
            $output.= '</td>';
            $output .= '</tr>';

            $i ++;
        }
        $paging_query = "SELECT id, title FROM " . _prefix("cities") . "   " . "$condition ";
        $paging_res = $db->sql_query($paging_query);
        $pagingCount = $db->sql_numrows($paging_res);
        if ($pagingCount > 0) {
            $data = '';
            $paginationCount = getPagination($pagingCount);
            $colspan = 6;
            if (isset($search_type)) {
                $data .="&search_input=$search_input&search_type=$search_type";
            }
            $output .= get_pagination_link($paginationCount, 'location', $data, $colspan);
        }
    } else {
        $output = '<div class="norecord">No record Found</div>';
    }
    echo $output;
    die;
}

function manage_suburb() {
    global $db;
    $condition = " where sub.deleted = 0 ";
    $query = "SELECT sub.* FROM " . _prefix("suburbs") . " AS sub  "
            . "LEFT JOIN " . _prefix("cities") . " AS cit ON  sub.city_id= cit.id " . "$condition";
    $res = $db->sql_query($query);
    $count = $db->sql_numrows($res);
    $i = 1;
    if (isset($_POST['pageId']) && !empty($_POST['pageId'])) {
        $id = $_POST['pageId'];
        $i = $i + PAGE_PER_NO * $id;
    } else {
        $id = '0';
        $i = 1;
    }

    $pageLimit = PAGE_PER_NO * $id;
    $query = "SELECT sub.*, cit.title as city FROM " . _prefix("suburbs") . " AS sub  "
            . "LEFT JOIN " . _prefix("cities") . " AS cit ON  sub.city_id= cit.id "
            . "$condition ORDER BY sub.title ASC limit " . $pageLimit . ',' . PAGE_PER_NO;
    $res = $db->sql_query($query);
    $data = $db->sql_fetchrowset($res);
    if ($count > 0) {
        $output = '<tr><th align="left" width="10%">S.No</th>
                                <th align="left" width="30%">City</th>
                                <th align="left" width="30%">Suburb</th>
                                <th align="left" width="30%">Action</th>
                            </tr>';
        foreach ($data as $key => $record) {
            $status = ($record['status'] == 1) ? 'Active' : 'InActive';
            $active_status = ($record['status'] == 1) ? ADMIN_IMAGE . 'activate.png' : ADMIN_IMAGE . 'deactivate.png';
            $img = '<img src="' . $active_status . '" title="' . $status . '" />';
            $output .= '<tr class="row_' . $record['id'] . '" style="margin-top: 10px;">';
            $output .= '<td>' . $i . '.</td>';
            $output .= '<td>' . $record['city'] . '</td>';
            $output .= '<td>' . $record['title'] . '</td>';
            $output .= '<td><span class="status-' . $record['id'] . '" style="margin-right:5px;"><a href="javascript:void(0);" id="suburbs-' . $record['id'] . '-' . $record['status'] . '" class="status">' . $img . '</a></span>'
                    . '<span style="margin-right:5px;"><a href=' . HOME_PATH_URL . 'location.php?editSuburb&id=' . md5($record['id']) . '><img src="' . ADMIN_IMAGE . 'li_edit.png" title="Edit"></a></span>'
                    . '<span><a href="javascript:void(0);" class="delete" id="del-suburbs-' . $record['id'] . '"><img src="' . ADMIN_IMAGE . 'li_delete.png" title="Delete" ></a> </span>';
            $output.= '</td>';
            $output .= '</tr>';

            $i ++;
        }
        $pagingCount = $count;
        if ($pagingCount > 0) {
            $data = '';
            $paginationCount = getPagination($pagingCount);
            $colspan = 6;
            if (isset($search_type)) {
                $data .="&search_input=$search_input&search_type=$search_type";
            }
            $output .= get_pagination_link($paginationCount, 'suburb', $data, $colspan);
        }
    } else {
        $output = '<div class="norecord">No record Found</div>';
    }
    echo $output;
    die;
}

function manage_restcare() {
    global $db;
    $condition = " where rest.deleted = 0 ";
    $query = "SELECT  rest.* FROM " . _prefix("rest_cares") . " AS rest  "
            . "LEFT JOIN " . _prefix("suburbs") . " AS sub ON  rest.suburb_id= sub.id " . "$condition";
    $res = $db->sql_query($query);
    $count = $db->sql_numrows($res);
//        prd($count);
    $i = 1;
    if (isset($_POST['pageId']) && !empty($_POST['pageId'])) {
        $id = $_POST['pageId'];
        $i = $i + PAGE_PER_NO * $id;
    } else {
        $id = '0';
        $i = 1;
    }

    $pageLimit = PAGE_PER_NO * $id;
    $query = "SELECT rest.*, sub.title as suburb FROM " . _prefix("rest_cares") . " AS rest  "
            . "LEFT JOIN " . _prefix("suburbs") . " AS sub ON  rest.suburb_id= sub.id "
            . "$condition ORDER BY rest.title ASC limit " . $pageLimit . ',' . PAGE_PER_NO;
    $res = $db->sql_query($query);
    $data = $db->sql_fetchrowset($res);
    if ($count > 0) {
        $output = '<tr><th align="left" width="10%">S.No</th>
                                <th align="left" width="30%">Suburb</th>
                                <th align="left" width="30%">Rest Care</th>
                                <th align="left" width="30%">Action</th>
                            </tr>';
        foreach ($data as $key => $record) {
            $status = ($record['status'] == 1) ? 'Active' : 'InActive';
            $active_status = ($record['status'] == 1) ? ADMIN_IMAGE . 'activate.png' : ADMIN_IMAGE . 'deactivate.png';
            $img = '<img src="' . $active_status . '" title="' . $status . '" />';
            $output .= '<tr class="row_' . $record['id'] . '" style="margin-top: 10px;">';
            $output .= '<td>' . $i . '.</td>';
            $output .= '<td>' . $record['suburb'] . '</td>';
            $output .= '<td>' . $record['title'] . '</td>';
            $output .= '<td><span class="status-' . $record['id'] . '" style="margin-right:5px;"><a href="javascript:void(0);" id="rest_cares-' . $record['id'] . '-' . $record['status'] . '" class="status">' . $img . '</a></span>'
                    . '<span style="margin-right:5px;"><a href=' . HOME_PATH_URL . 'location.php?editRestcare&id=' . md5($record['id']) . '><img src="' . ADMIN_IMAGE . 'li_edit.png" title="Edit"></a></span>'
                    . '<span><a href="javascript:void(0);" class="delete" id="del-rest_cares-' . $record['id'] . '"><img src="' . ADMIN_IMAGE . 'li_delete.png" title="Delete" ></a> </span>';
            $output.= '</td>';
            $output .= '</tr>';

            $i ++;
        }

        $pagingCount = $count;
        if ($pagingCount > 0) {
            $data = '';
            $paginationCount = getPagination($pagingCount);
            $colspan = 6;
            if (isset($search_type)) {
                $data .="&search_input=$search_input&search_type=$search_type";
            }
            $output .= get_pagination_link($paginationCount, 'restcare', $data, $colspan);
        }
    } else {
        $output = '<div class="norecord">No record Found</div>';
    }
    echo $output;
    die;
}

function manage_duration() {
    global $db;
    $query = "SELECT * FROM " . _prefix("durations") . " where deleted = 0 ORDER BY created DESC";
    $res = $db->sql_query($query);
    $count = $db->sql_numrows($res);
    $i = 1;
    if (isset($_POST['pageId']) && !empty($_POST['pageId'])) {
        $id = $_POST['pageId'];
        $i = $i + PAGE_PER_NO * $id;
    } else {
        $id = '0';
        $i = 1;
    }

    $pageLimit = PAGE_PER_NO * $id;
    echo $query = "SELECT * FROM " . _prefix("durations") . " where deleted = 0 ORDER BY order_by DESC limit " . $pageLimit . ',' . PAGE_PER_NO;
    $res = $db->sql_query($query);
    $data = $db->sql_fetchrowset($res);
    if ($count > 0) {
        $output = '<tr><th align="left" width="7%">S.No</th>
                                <th align="left" width="50%">Title</th>
                                <th align="left" width="15%">Created</th>
                                <th align="left" width="10%">Order</th>
                                <th align="left" width="30">Action</th>
                            </tr>';

        foreach ($data as $key => $record) {

            $status = ($record['status'] == 1) ? 'Active' : 'InActive';
            $active_status = ($record['status'] == 1) ? ADMIN_IMAGE . 'activate.png' : ADMIN_IMAGE . 'deactivate.png';
            $img = '<img src="' . $active_status . '" title="' . $status . '" />';
            $output .= '<tr class="row_' . $record['id'] . '" style="margin-top: 10px;">';
            $output .= '<td>' . $i . '.</td>';
            $output .= '<td>' . $record['title'] . '</td>';
            $output .= '<td>' . date('M d, Y', strtotime($record['created'])) . '</td>';
            $output .='<td>';

            if ($count > 1) {
                if ($i == 1) {
                    $output .= '<span style="margin-right:5px;"><a href=" javascript:void(0);" id="durations-' . $record['order_by'] . '-' . $record['id'] . '" class="order up"><img src="' . ADMIN_IMAGE . 'arrowup.png" title="UP"></a></span>';
                } else if ($i == $count) {
                    $output .= '<span style="margin-right:5px;"><a href=" javascript:void(0);" id="durations-' . $record['order_by'] . '-' . $record['id'] . '" class="order down"><img src="' . ADMIN_IMAGE . 'arrowdown.png" title="DOWN"></a></span>';
                } else {
                    $output .= '<span style="margin-right:5px;"><a href=" javascript:void(0);" id="durations-' . $record['order_by'] . '-' . $record['id'] . '" class="order up"><img src="' . ADMIN_IMAGE . 'arrowup.png" title="UP"></a></span>';
                    $output .= '<span style="margin-right:5px;"><a href=" javascript:void(0);" id="durations-' . $record['order_by'] . '-' . $record['id'] . '" class="order down"><img src="' . ADMIN_IMAGE . 'arrowdown.png" title="DOWN"></a></span>';
                }
            }
            $output .='</td>';

            $output .= '<td><span class="status-' . $record['id'] . '" style="margin-right:5px;"><a href="javascript:void(0);" id="durations-' . $record['id'] . '-' . $record['status'] . '" class="status">' . $img . '</a></span>'
                    . '<span style="margin-right:5px;"><a href=' . HOME_PATH_URL . 'pages.php?editDuration&id=' . md5($record['id']) . '><img src="' . ADMIN_IMAGE . 'li_edit.png" title="Edit"></a></span>'
                    . '<span><a href="javascript:void(0);" class="delete" id="del-durations-' . $record['id'] . '"><img src="' . ADMIN_IMAGE . 'li_delete.png" title="Delete" ></a> </span>'
                    . '</td>';
            $output .= '</tr>';

            $i ++;
        }
        $paging_query = "SELECT * FROM " . _prefix("durations") . " where deleted = 0 ORDER BY created DESC";
        $paging_res = $db->sql_query($paging_query);
        $pagingCount = $db->sql_numrows($paging_res);
        if ($pagingCount > 0) {
            $paginationCount = getPagination($pagingCount);

            $output .= get_pagination_link($paginationCount, 'duration', $data = '', $colspan = 4);
        }
    } else {
        $output = '<div class="norecord">No record Found</div>';
    }
    echo $output;
    die;
}

function manage_visit() {
    global $db;
    $query = "SELECT * FROM " . _prefix("know_me") . " where deleted = 0 ORDER BY created DESC";
    $res = $db->sql_query($query);
    $count = $db->sql_numrows($res);
    $i = 1;
    if (isset($_POST['pageId']) && !empty($_POST['pageId'])) {
        $id = $_POST['pageId'];
        $i = $i + PAGE_PER_NO * $id;
    } else {
        $id = '0';
        $i = 1;
    }

    $pageLimit = PAGE_PER_NO * $id;
    echo $query = "SELECT * FROM " . _prefix("know_me") . " where deleted = 0 ORDER BY order_by DESC limit " . $pageLimit . ',' . PAGE_PER_NO;
    $res = $db->sql_query($query);
    $data = $db->sql_fetchrowset($res);
    if ($count > 0) {
        $output = '<tr><th align="left" width="5%">S.No</th>
                                <th align="left" width="50%">Title</th>
                                <th align="left" width="15%">Created</th>
                                <th align="left" width="10%">Order</th>
                                <th align="left" width="30">Action</th>
                            </tr>';

        foreach ($data as $key => $record) {

            $status = ($record['status'] == 1) ? 'Active' : 'InActive';
            $active_status = ($record['status'] == 1) ? ADMIN_IMAGE . 'activate.png' : ADMIN_IMAGE . 'deactivate.png';
            $img = '<img src="' . $active_status . '" title="' . $status . '" />';
            $output .= '<tr class="row_' . $record['id'] . '" style="margin-top: 10px;">';
            $output .= '<td>' . $i . '.</td>';
            $output .= '<td>' . $record['title'] . '</td>';
            $output .= '<td>' . date('M d, Y', strtotime($record['created'])) . '</td>';
            $output .='<td>';

            if ($count > 1) {
                if ($i == 1) {
                    $output .= '<span style="margin-right:5px;"><a href=" javascript:void(0);" id="know_me-' . $record['order_by'] . '-' . $record['id'] . '" class="order up"><img src="' . ADMIN_IMAGE . 'arrowup.png" title="UP"></a></span>';
                } else if ($i == $count) {
                    $output .= '<span style="margin-right:5px;"><a href=" javascript:void(0);" id="know_me-' . $record['order_by'] . '-' . $record['id'] . '" class="order down"><img src="' . ADMIN_IMAGE . 'arrowdown.png" title="DOWN"></a></span>';
                } else {
                    $output .= '<span style="margin-right:5px;"><a href=" javascript:void(0);" id="know_me-' . $record['order_by'] . '-' . $record['id'] . '" class="order up"><img src="' . ADMIN_IMAGE . 'arrowup.png" title="UP"></a></span>';
                    $output .= '<span style="margin-right:5px;"><a href=" javascript:void(0);" id="know_me-' . $record['order_by'] . '-' . $record['id'] . '" class="order down"><img src="' . ADMIN_IMAGE . 'arrowdown.png" title="DOWN"></a></span>';
                }
            }
            $output .='</td>';
            $output .= '<td><span class="status-' . $record['id'] . '" style="margin-right:5px;"><a href="javascript:void(0);" id="know_me-' . $record['id'] . '-' . $record['status'] . '" class="status">' . $img . '</a></span>'
                    . '<span style="margin-right:5px;"><a href=' . HOME_PATH_URL . 'pages.php?editVisit&id=' . md5($record['id']) . '><img src="' . ADMIN_IMAGE . 'li_edit.png" title="Edit"></a></span>'
                    . '<span><a href="javascript:void(0);" class="delete" id="del-know_me-' . $record['id'] . '"><img src="' . ADMIN_IMAGE . 'li_delete.png" title="Delete" ></a> </span>'
                    . '</td>';
            $output .= '</tr>';

            $i ++;
        }
        $paging_query = "SELECT * FROM " . _prefix("know_me") . " where deleted = 0 ORDER BY created DESC";
        $paging_res = $db->sql_query($paging_query);
        $pagingCount = $db->sql_numrows($paging_res);
        if ($pagingCount > 0) {
            $paginationCount = getPagination($pagingCount);

            $output .= get_pagination_link($paginationCount, 'visit', $data = '', $colspan = 4);
        }
    } else {
        $output = '<div class="norecord">No record Found</div>';
    }
    echo $output;
    die;
}

function manage_faqs() {

    global $db;
    $query = "SELECT * FROM " . _prefix("faqs") . " where deleted = 0 ORDER BY created DESC";
    $res = $db->sql_query($query);
    $count = $db->sql_numrows($res);
    $i = 1;
    if (isset($_POST['pageId']) && !empty($_POST['pageId'])) {
        $id = $_POST['pageId'];
        $i = $i + PAGE_PER_NO * $id;
    } else {
        $id = '0';
        $i = 1;
    }

    $pageLimit = PAGE_PER_NO * $id;
    $query = "SELECT * FROM " . _prefix("faqs") . " where deleted = 0 ORDER BY order_by DESC limit " . $pageLimit . ',' . PAGE_PER_NO;
    $res = $db->sql_query($query);
    $data = $db->sql_fetchrowset($res);




    if ($count > 0) {
        $output = '<tr>     <th align="left" class="col-md-1">S.No</th>
                                <th align="left"class="col-md-3">Question</th>
                                <th align="left" class="col-md-4">Answer</th>
                                <th align="left" class="col-md-2">Order</th>
                                <th  class="col-md-2 text-center">Action</th>
                            </tr>';

        foreach ($data as $key => $record) {
            $output .= '<tr class="row_' . $record['id'] . '">';
            $question = $record['title'] == '' ? 'N/A' : $record['title'];
            $question = (strlen($question) > 50) ? substr($question, 0, 50) . '...' : $question;
            $answer = $record['content'] == '' ? 'N/A' : $record['content'];
            $answerNew = strip_tags($answer);
            $answerNew = (strlen($answerNew) > 100) ? substr($answerNew, 0, 100) . '...' : $answerNew;
            $status = ($record['status'] == 1) ? 'Active' : 'InActive';
            $active_status = ($record['status'] == 1) ? ADMIN_IMAGE . 'activate.png' : ADMIN_IMAGE . 'deactivate.png';
            $img = '<img src="' . $active_status . '" title="' . $status . '" />';
            $output .= '<td>' . $i . ' </td>';
            $output .= '<td>' . $question . '</td>';
            $output .= '<td>' . $answerNew . '</td>';
            $output .='<td>';

            if ($count > 1) {
                if ($i == 1) {
                    $output .= '<span style="margin-right:5px;"><a href=" javascript:void(0);" id="faqs-' . $record['order_by'] . '-' . $record['id'] . '" class="order up"><img src="' . ADMIN_IMAGE . 'arrowup.png" title="UP"></a></span>';
                } else if ($i == $count) {
                    $output .= '<span style="margin-right:5px;"><a href=" javascript:void(0);" id="faqs-' . $record['order_by'] . '-' . $record['id'] . '" class="order down"><img src="' . ADMIN_IMAGE . 'arrowdown.png" title="DOWN"></a></span>';
                } else {
                    $output .= '<span style="margin-right:5px;"><a href=" javascript:void(0);" id="faqs-' . $record['order_by'] . '-' . $record['id'] . '" class="order up"><img src="' . ADMIN_IMAGE . 'arrowup.png" title="UP"></a></span>';
                    $output .= '<span style="margin-right:5px;"><a href=" javascript:void(0);" id="faqs-' . $record['order_by'] . '-' . $record['id'] . '" class="order down"><img src="' . ADMIN_IMAGE . 'arrowdown.png" title="DOWN"></a></span>';
                }
            }
            $output .='</td>';

            $output .= '<td class="text-center"><span class="status-' . $record['id'] . '" style="margin-right:5px;"><a href="javascript:void(0);" id="faqs-' . $record['id'] . '-' . $record['status'] . '" class="status">' . $img . '</a></span>'
                    . '<span style="margin-right:5px;"><a href=' . HOME_PATH_URL . 'cms.php?edit_Faq&id=' . md5($record['id']) . '><img src="' . ADMIN_IMAGE . 'li_edit.png" title="Edit"></a></span>'
                    . '<span><a href="javascript:void(0);" class="delete" id="del-faqs-' . $record['id'] . '"><img src="' . ADMIN_IMAGE . 'li_delete.png" title="Delete" ></a> </span>'
                    . '</td>';
            $output .= '</tr>';
            $i ++;
        }
        $paging_query = "SELECT * FROM " . _prefix("faqs") . " where deleted = 0 ORDER BY order_by";
        $paging_res = $db->sql_query($paging_query);
        $pagingCount = $db->sql_numrows($paging_res);
        if ($pagingCount > 0) {
            $paginationCount = getPagination($pagingCount);

            $output .= get_pagination_link($paginationCount, 'faqs', $data = '', $colspan = 4);
        }
    } else {
        $output = '<div class="norecord">No record Found</div>';
    }
    echo $output;

    die();
}

function manage_paymentapproval($search_input, $search_type) {
    global $db;
    $condition = "WHERE dc.deleted = 0 ";
    if (isset($search_type) && $search_input != '') {
        switch ($search_type) {
            case "approved":
                $condition .= ' && dc.approve_status = 1 ';
                break;
            case "disapproved":
                $condition .= ' && dc.approve_status = 0 ';
                break;
            case "supplier":
                $condition .= ' && ((user.first_name like "%' . $search_input . '%" ) OR (user.last_name like "%' . $search_input . '%" ))';
//                $condition .= ' && ';
                break;
            default :
                $condition .= ' && ((((user.first_name like "%' . $search_input . '%" ) OR(user.last_name like "%' . $search_input . '%" ))) )';
        }
    }
//        $query = "SELECT id FROM " . _prefix("ads_masters") . " $conditon";
    $query = "SELECT dc.id FROM " . _prefix("direct_credit") . " AS dc "
            . "  LEFT JOIN " . _prefix("users") . " AS user ON user.id=dc.supplier_id "
            . " $condition ";
    $res = $db->sql_query($query);
    $count = $db->sql_numrows($res);
    $i = 1;
//        prd($count);
    if (isset($_POST['pageId']) && !empty($_POST['pageId'])) {
        $id = $_POST['pageId'];
        $i = $i + PAGE_PER_NO * $id;
    } else {
        $id = '0';
        $i = 1;
    }
    $pageLimit = PAGE_PER_NO * $id;
    $query = "SELECT dc.*, user.email AS supplier ,CONCAT(IFNULL(user.first_name,''),' ',IFNULL(user.last_name,'')) AS supplierName,"
            . " plan.title as planTitle , plan.duration  "
            . " FROM " . _prefix("direct_credit") . " AS dc "
            . "  LEFT JOIN " . _prefix("users") . " AS user ON user.id=dc.supplier_id "
            . "  LEFT JOIN " . _prefix("membership_prices") . " AS plan ON plan.id=dc.plan_id "
            . " $condition  ORDER BY dc.id DESC limit " . $pageLimit . ',' . PAGE_PER_NO;

    $res = $db->sql_query($query);
    $data = $db->sql_fetchrowset($res);
    //prd($data);
    if ($count > 0) {
        $output = '<tr><th align="left" width="5%">S.No</th>
                                <th align="left" width="15%">Supplier</th>
                                <th align="left" width="10%">Plan</th>
                                <th align="left" width="20%">Description</th>
                                <th align="left" width="15%">Date</th>
                                <th align="left" width="15%">Approval</th>
                                <th align="center" width="20%">Action</th>
                            </tr>';
        foreach ($data as $key => $record) {
//            if ($record['plan_id'] == 2) {
//                $plan = '12 Months';
//            } else if ($record['plan_id'] == 3) {
//                $plan = '6 Months';
//            } else if ($record['plan_id'] == 4) {
//                $plan = '3 Months';
//            } else {
//                $plan = 'N/A';
//            }
            $plan = $record['planTitle'] . '<br/>' . $record['duration'] . ' Months';
            $loading = ADMIN_IMAGE . 'approval.gif';
            $status = ($record['status'] == 1) ? 'Active' : 'InActive';
            $approve_status = ($record['approve_status'] == 1) ? 'Approved' : 'Requested';
            $active_status = ($record['status'] == 1) ? ADMIN_IMAGE . 'activate.png' : ADMIN_IMAGE . 'deactivate.png';
            $approved = ($record['approve_status'] == 1) ? ADMIN_IMAGE . 'activate.png' : ADMIN_IMAGE . 'deactivate.png';
            $description = strip_tags($record['description']);
            $description = (strlen($description) > 40) ? substr($description, 0, 40) . '...' : $description;
            $img = '<img src="' . $active_status . '" title="' . $status . '" />';
            $img1 = $approve_status . ' <img src="' . $approved . '" title="' . $approve_status . '" />';
            $output .= '<tr class="row_' . $record['id'] . '" style="margin-top: 10px;">';
            $output .= '<td>' . $i . '.</td>';
            $output .= isset($record['supplierName']) ? '<td>' . $record['supplierName'] . '</td>' : '<td class="text-center">N/A</td>';
            $output .= '<td>' . $plan . '</td>';
            $output .= '<td>' . $description . '</td>';
            $output .= '<td>' . date('M d, Y', strtotime($record['created'])) . '</td>';
            if ($record['approve_status'] == 1) {
                $output .= '<td align="center"> <span class="payment_approval-' . $record['id'] . '" style="margin-right:5px;text-align:center;">' . $img1 . '</span></td>';
            } else {
                $output .= '<td align="center"> <span class="payment_approval-' . $record['id'] . '" style="margin-right:5px;text-align:center;"><a href="javascript:void(0);" id="direct_credit--' . $record['id'] . '--' . $record['plan_id'] . '--'. $record['product_id'] . '--' . $record['supplier_id'] . '--' . $record['amount'] . '--' . $loading . '--' . $record['approve_status'] . '" class="payment_approval">' . $img1 . '</a></span></td>';
            }
            $output .= '<td align="center"><span style="margin-right:5px;"><a  class="image" href="' . HOME_PATH_URL . 'popup.php?paymentApproval&id=' . md5($record['id']) . '"><img src="' . ADMIN_IMAGE . 'li_view.png" title="View"></a></span>'
                    . '<span><a href="javascript:void(0);" class="delete" id="del-direct_credit-' . $record['id'] . '"><img src="' . ADMIN_IMAGE . 'li_delete.png" title="Delete" ></a> </span>';
            $output.= '</td>';
            $output .= '</tr>';
            $i ++;
        }

        $pagingCount = $count;
        if ($pagingCount > 0) {
            $data = '';
            $paginationCount = getPagination($pagingCount);
            $colspan = 6;
            $output .= get_pagination_link($paginationCount, 'paymentapproval', $data, $colspan);
        }
    } else {
        $output = '<div class="norecord">No record Found</div>';
    }
    echo $output;
    ?>
    <script>
        $(".image").colorbox({
            width: '600px',
            height: '330px;'
        });
        $(document).ready(function() {
            $('.load').click(function() {
                var value = $(this).attr('id').split('-');
                $('#loading-' + value[1]).show();
            });
        });
    </script>
    <?php
    die;
}
?>